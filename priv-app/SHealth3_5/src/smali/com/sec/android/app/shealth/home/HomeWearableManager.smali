.class public Lcom/sec/android/app/shealth/home/HomeWearableManager;
.super Ljava/lang/Object;
.source "HomeWearableManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HomeWearableManager"

.field public static final WEARABLE_CONNECTED:I = 0x1

.field public static final WEARABLE_DISCONNECTED:I = 0x2

.field public static final WEARABLE_SERVICE_DISCONNECTED:I = 0x3


# instance fields
.field private iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private isConnectedWearable:Z

.field private mContext:Landroid/content/Context;

.field private mSensorDeivce:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceListener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mContext:Landroid/content/Context;

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mSensorDeivce:Ljava/util/List;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->isConnectedWearable:Z

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mServiceListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mContext:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mServiceListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .line 37
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mServiceListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 38
    return-void
.end method

.method private getSyncIntent(I)Landroid/content/Intent;
    .locals 2
    .param p1, "deviceType"    # I

    .prologue
    .line 67
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 69
    .local v0, "intent":Landroid/content/Intent;
    packed-switch p1, :pswitch_data_0

    .line 87
    :pswitch_0
    const-string v1, "com.sec.shealth.UPDATE_PROFILE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    :goto_0
    return-object v0

    .line 71
    :pswitch_1
    const-string v1, "android.intent.action.PEDOMETER_SETTING_SYNC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 75
    :pswitch_2
    const-string v1, "com.samsung.android.shealth.ACTION_GEAR2_SYNC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 78
    :pswitch_3
    const-string v1, "com.samsung.android.shealth.ACTION_GEAR_SYNC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 81
    :pswitch_4
    const-string v1, "com.samsung.android.shealth.ACTION_WINGTIP_SYNC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 84
    :pswitch_5
    const-string v1, "com.samsung.android.shealth.ACTION_SBAND_SYNC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public checkConnectedWearable()I
    .locals 6

    .prologue
    const/4 v1, 0x3

    .line 42
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v3, 0x7

    const/16 v4, 0x2711

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mSensorDeivce:Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4

    .line 58
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mSensorDeivce:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mSensorDeivce:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    const-string v1, "HomeWearableManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "findConnectedWearable() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mSensorDeivce:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const/4 v1, 0x1

    .line 62
    :goto_1
    return v1

    .line 44
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e1":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 47
    .end local v0    # "e1":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 48
    .local v0, "e1":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 49
    .end local v0    # "e1":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 50
    .local v0, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 51
    .end local v0    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 52
    .local v0, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_0

    .line 53
    .end local v0    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_4
    move-exception v0

    .line 54
    .local v0, "e1":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 62
    .end local v0    # "e1":Ljava/lang/NullPointerException;
    :cond_0
    const/4 v1, 0x2

    goto :goto_1
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 118
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mContext:Landroid/content/Context;

    .line 119
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mSensorDeivce:Ljava/util/List;

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 122
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 124
    :cond_0
    return-void
.end method

.method public getSensorDeivce()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mSensorDeivce:Ljava/util/List;

    return-object v0
.end method

.method public sendSyncIntent()Z
    .locals 7

    .prologue
    .line 94
    const/4 v3, 0x0

    .line 96
    .local v3, "value":Z
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mSensorDeivce:Ljava/util/List;

    if-nez v5, :cond_0

    .line 97
    const-string v5, "HomeWearableManager"

    const-string/jumbo v6, "syncDevice start in HomeActivity but mSensorDeivce is null"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 110
    .end local v3    # "value":Z
    .local v4, "value":I
    :goto_0
    return v4

    .line 101
    .end local v4    # "value":I
    .restart local v3    # "value":Z
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mSensorDeivce:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 102
    .local v2, "sDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->getSyncIntent(I)Landroid/content/Intent;

    move-result-object v1

    .line 103
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_1

    .line 104
    const-string v5, "HomeWearableManager"

    const-string v6, "Sync broadcast sent"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/HomeWearableManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 106
    const/4 v3, 0x1

    goto :goto_1

    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "sDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :cond_2
    move v4, v3

    .line 110
    .restart local v4    # "value":I
    goto :goto_0
.end method
