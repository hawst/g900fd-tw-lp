.class public Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;
.super Ljava/lang/Object;
.source "UpgradeManager.java"

# interfaces
.implements Lcom/sec/android/app/shealth/framework/repository/common/ICompatibilityManager;


# static fields
.field private static final BIRTH_DATE:Ljava/lang/String; = "birth_date"

.field private static final BIRTH_DAY:Ljava/lang/String; = "birth_day"

.field private static final BIRTH_MONTH:Ljava/lang/String; = "birth_month"

.field private static final BIRTH_YEAR:Ljava/lang/String; = "birth_year"

.field private static final GENDER:Ljava/lang/String; = "gender"

.field private static final HEIGHT:Ljava/lang/String; = "height"

.field private static final HEIGHT_UNIT:Ljava/lang/String; = "height_unit"

.field private static final NAME:Ljava/lang/String; = "name"

.field private static final PREF_TYPE_BOOLEAN:I = 0x4

.field private static final PREF_TYPE_FLOAT:I = 0x3

.field private static final PREF_TYPE_INT:I = 0x5

.field private static final PREF_TYPE_LONG:I = 0x1

.field private static final PREF_TYPE_STRING:I = 0x2

.field private static final SECURE_BIRTH_DAY:Ljava/lang/String; = "secure_birth_day"

.field private static final SECURE_BIRTH_MONTH:Ljava/lang/String; = "secure_birth_month"

.field private static final SECURE_BIRTH_YEAR:Ljava/lang/String; = "secure_birth_year"

.field private static final SECURE_HEIGHT:Ljava/lang/String; = "secure_height"

.field private static final SECURE_PREFIX:Ljava/lang/String; = "secure_"

.field private static final SECURE_WEIGHT:Ljava/lang/String; = "secure_weight"

.field private static final TAG:Ljava/lang/String;

.field private static final TIMEZONE_KEY:Ljava/lang/String; = "fixed_time_zone_id"

.field private static final WEIGHT:Ljava/lang/String; = "weight"

.field private static final WEIGHT_UNIT:Ljava/lang/String; = "weight_unit"

.field public static mPrefsKeyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

.field private mContext:Landroid/content/Context;

.field private mIsUpgradeToMainDb:Z

.field private mPref:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    .line 77
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mPrefsKeyList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Landroid/content/Context;Z)V
    .locals 1
    .param p1, "pref"    # Landroid/content/SharedPreferences;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "isUpgradeToMainDb"    # Z

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mIsUpgradeToMainDb:Z

    .line 93
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mPref:Landroid/content/SharedPreferences;

    .line 94
    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mContext:Landroid/content/Context;

    .line 95
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mIsUpgradeToMainDb:Z

    .line 96
    return-void
.end method

.method private setActivityType(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 5
    .param p2, "healthprofile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/samsung/android/sdk/health/content/ShealthProfile;",
            ")V"
        }
    .end annotation

    .prologue
    .line 713
    .local p1, "prefset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v3, " setActivityType "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    const-string v2, "activity_type"

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 716
    const-string v2, "activity_type"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 717
    .local v0, "activity_type":I
    const/4 v1, -0x1

    .line 718
    .local v1, "k_activity_type":I
    packed-switch v0, :pswitch_data_0

    .line 736
    const/4 v1, -0x1

    .line 740
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " setActivityType activity_type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " k_activity_type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    invoke-virtual {p2, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setActivityType(I)V

    .line 743
    .end local v0    # "activity_type":I
    .end local v1    # "k_activity_type":I
    :cond_0
    return-void

    .line 721
    .restart local v0    # "activity_type":I
    .restart local v1    # "k_activity_type":I
    :pswitch_0
    const v1, 0x2bf21

    .line 722
    goto :goto_0

    .line 724
    :pswitch_1
    const v1, 0x2bf22

    .line 725
    goto :goto_0

    .line 727
    :pswitch_2
    const v1, 0x2bf23

    .line 728
    goto :goto_0

    .line 730
    :pswitch_3
    const v1, 0x2bf24

    .line 731
    goto :goto_0

    .line 733
    :pswitch_4
    const v1, 0x2bf25

    .line 734
    goto :goto_0

    .line 718
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private setBirthDate(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 11
    .param p2, "healthprofile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/samsung/android/sdk/health/content/ShealthProfile;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .local p1, "prefset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    const/4 v10, -0x1

    .line 646
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v8, " setBirthDate "

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    const/4 v2, -0x1

    .local v2, "birth_year":I
    const/4 v1, -0x1

    .local v1, "birth_month":I
    const/4 v0, -0x1

    .line 649
    .local v0, "birth_day":I
    const-string v7, "birth_year"

    invoke-interface {p1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 651
    const-string v7, "birth_year"

    invoke-interface {p1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 652
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " birth_year "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    :cond_0
    :goto_0
    const-string v7, "birth_month"

    invoke-interface {p1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 667
    const-string v7, "birth_month"

    invoke-interface {p1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 668
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " birth_month "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    :cond_1
    :goto_1
    const-string v7, "birth_day"

    invoke-interface {p1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 683
    const-string v7, "birth_day"

    invoke-interface {p1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 684
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " birth_day "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 697
    :cond_2
    :goto_2
    if-eq v2, v10, :cond_3

    if-eq v1, v10, :cond_3

    if-eq v0, v10, :cond_3

    .line 699
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 700
    .local v3, "cal":Ljava/util/Calendar;
    const/4 v7, 0x1

    invoke-virtual {v3, v7, v2}, Ljava/util/Calendar;->set(II)V

    .line 701
    const/4 v7, 0x2

    invoke-virtual {v3, v7, v1}, Ljava/util/Calendar;->set(II)V

    .line 702
    const/4 v7, 0x5

    invoke-virtual {v3, v7, v0}, Ljava/util/Calendar;->set(II)V

    .line 704
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " setting birth date : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    add-int/lit8 v7, v1, 0x1

    invoke-static {v2, v7, v0}, Lcom/sec/android/service/health/cp/common/HealthServiceUtil;->getyyyyMMddFromCalendar(III)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setBirthDate(Ljava/lang/String;)V

    .line 709
    .end local v3    # "cal":Ljava/util/Calendar;
    :cond_3
    return-void

    .line 654
    :cond_4
    const-string/jumbo v7, "secure_birth_year"

    invoke-interface {p1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 656
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v8, " SECURE_BIRTH_YEAR "

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    iget-object v8, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v7, "secure_birth_year"

    invoke-interface {p1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v9}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 658
    .local v6, "secure_birth_year":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 660
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 661
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " birth_year "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 670
    .end local v6    # "secure_birth_year":Ljava/lang/String;
    :cond_5
    const-string/jumbo v7, "secure_birth_month"

    invoke-interface {p1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 672
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v8, " SECURE_BIRTH_MONTH "

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    iget-object v8, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v7, "secure_birth_month"

    invoke-interface {p1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v9}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 674
    .local v5, "secure_birth_month":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 676
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 677
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " birth_month "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 686
    .end local v5    # "secure_birth_month":Ljava/lang/String;
    :cond_6
    const-string/jumbo v7, "secure_birth_day"

    invoke-interface {p1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 688
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v8, " SECURE_BIRTH_DAY "

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    iget-object v8, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v7, "secure_birth_day"

    invoke-interface {p1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v9}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 690
    .local v4, "secure_birth_day":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 692
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 693
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " birth_day "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private setDistanceUnit(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 5
    .param p2, "healthprofile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/samsung/android/sdk/health/content/ShealthProfile;",
            ")V"
        }
    .end annotation

    .prologue
    .line 429
    .local p1, "prefset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v3, " setDistanceUnit"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    const-string v2, "distance_unit"

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 432
    const-string v2, "distance_unit"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 433
    .local v1, "value":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " DistanceUnit "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const-string/jumbo v2, "mile"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v0, 0x29813

    .line 435
    .local v0, "unit":I
    :goto_0
    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setDistanceUnit(I)V

    .line 438
    .end local v0    # "unit":I
    .end local v1    # "value":Ljava/lang/String;
    :cond_0
    return-void

    .line 434
    .restart local v1    # "value":Ljava/lang/String;
    :cond_1
    const v0, 0x29811

    goto :goto_0
.end method

.method private setGender(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 3
    .param p2, "healthprofile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/samsung/android/sdk/health/content/ShealthProfile;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 455
    .local p1, "prefset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    const-string v1, "gender"

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 457
    const-string v1, "gender"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x2e635

    .line 459
    .local v0, "gender":I
    :goto_0
    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setGender(I)V

    .line 461
    .end local v0    # "gender":I
    :cond_0
    return-void

    .line 457
    :cond_1
    const v0, 0x2e636

    goto :goto_0
.end method

.method private setGlucoseUnit(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 442
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " set glucose unit "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    const-string/jumbo v2, "mmol/L"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v1, "mmol/L"

    .line 444
    .local v1, "unit":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 445
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "glucose_unit"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 446
    return-void

    .line 443
    .end local v0    # "sharedPref":Landroid/content/SharedPreferences;
    .end local v1    # "unit":Ljava/lang/String;
    :cond_0
    const-string/jumbo v1, "mg/dL"

    goto :goto_0
.end method

.method private setHeight(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 10
    .param p2, "healthprofile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/samsung/android/sdk/health/content/ShealthProfile;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .local p1, "prefset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    const/high16 v9, -0x40800000    # -1.0f

    .line 488
    sget-object v6, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v7, " setHeight "

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    const/high16 v0, -0x40800000    # -1.0f

    .line 497
    .local v0, "height":F
    const-string v5, "_inch"

    .line 498
    .local v5, "unit_suffix":Ljava/lang/String;
    const v4, 0x249f1

    .line 500
    .local v4, "unit":I
    const-string v6, "height_unit"

    invoke-interface {p1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 502
    const-string v6, "height_unit"

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 503
    .local v1, "height_unit":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Height Unit  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    const-string v6, "cm"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 506
    const-string v6, "height"

    invoke-interface {p1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 508
    const-string v6, "height"

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 509
    .local v2, "obj":Ljava/lang/Object;
    if-eqz v2, :cond_4

    instance-of v6, v2, Ljava/lang/Integer;

    if-eqz v6, :cond_4

    .line 510
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "obj":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->floatValue()F

    move-result v0

    .line 524
    .end local v1    # "height_unit":Ljava/lang/String;
    :cond_0
    :goto_0
    cmpl-float v6, v0, v9

    if-nez v6, :cond_2

    .line 526
    sget-object v6, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v7, " getting the inch height "

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "height"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 529
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "height"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 530
    .restart local v2    # "obj":Ljava/lang/Object;
    if-eqz v2, :cond_6

    instance-of v6, v2, Ljava/lang/Integer;

    if-eqz v6, :cond_6

    .line 531
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "obj":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->floatValue()F

    move-result v0

    .line 543
    :cond_1
    :goto_1
    cmpl-float v6, v0, v9

    if-eqz v6, :cond_2

    .line 545
    sget-object v6, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " converting height to cm height in feet = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    const v4, 0x249f2

    .line 547
    invoke-static {v0}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertInchToCm(F)F

    move-result v0

    .line 551
    :cond_2
    cmpl-float v6, v0, v9

    if-eqz v6, :cond_3

    .line 553
    sget-object v6, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Height "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    invoke-virtual {p2, v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeightUnit(I)V

    .line 555
    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeight(F)V

    .line 557
    :cond_3
    return-void

    .line 511
    .restart local v1    # "height_unit":Ljava/lang/String;
    .restart local v2    # "obj":Ljava/lang/Object;
    :cond_4
    if-eqz v2, :cond_0

    instance-of v6, v2, Ljava/lang/Float;

    if-eqz v6, :cond_0

    .line 512
    check-cast v2, Ljava/lang/Float;

    .end local v2    # "obj":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto/16 :goto_0

    .line 514
    :cond_5
    const-string/jumbo v6, "secure_height"

    invoke-interface {p1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 516
    iget-object v7, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v6, "secure_height"

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v8}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v6, v8}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 517
    .local v3, "secure_height":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 519
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    goto/16 :goto_0

    .line 532
    .end local v1    # "height_unit":Ljava/lang/String;
    .end local v3    # "secure_height":Ljava/lang/String;
    .restart local v2    # "obj":Ljava/lang/Object;
    :cond_6
    if-eqz v2, :cond_1

    instance-of v6, v2, Ljava/lang/Float;

    if-eqz v6, :cond_1

    .line 533
    check-cast v2, Ljava/lang/Float;

    .end local v2    # "obj":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto/16 :goto_1

    .line 535
    :cond_7
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "secure_height"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 537
    iget-object v7, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "secure_height"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v8}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v6, v8}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 538
    .restart local v3    # "secure_height":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 540
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    goto/16 :goto_1
.end method

.method private setMigrationTimeZone(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 319
    .local p1, "prefsSet":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v2, " setMigrationTimeZone "

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const/4 v0, 0x0

    .line 321
    .local v0, "timeZoneId":Ljava/lang/String;
    const-string v1, "fixed_time_zone_id"

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    const-string v1, "fixed_time_zone_id"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "timeZoneId":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 324
    .restart local v0    # "timeZoneId":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " timeZoneId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :cond_0
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->setSharedPrefTimeZone(Ljava/lang/String;)V

    .line 327
    return-void
.end method

.method private setName(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 4
    .param p2, "healthprofile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/samsung/android/sdk/health/content/ShealthProfile;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 470
    .local p1, "prefset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v2, " setName "

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    const-string/jumbo v1, "name"

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 474
    const-string/jumbo v1, "name"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 476
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setName(Ljava/lang/String;)V

    .line 477
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Name "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    .end local v0    # "name":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setNotification(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 415
    .local p1, "prefsSet":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v3, " setNotification "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    const/4 v1, 0x1

    .line 417
    .local v1, "value":Z
    const-string/jumbo v2, "notification_enabled"

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 419
    const-string/jumbo v2, "notification_enabled"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 421
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " setNotification value "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 423
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "notification_enabled"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 424
    return-void
.end method

.method private setPrivacy(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 408
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " setPrivacy "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 410
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "use_pedometer_ranking"

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 411
    return-void

    .line 410
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setTemperatureUnit(Ljava/util/Map;Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;)V
    .locals 5
    .param p2, "healthprofile"    # Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;",
            ")V"
        }
    .end annotation

    .prologue
    .line 305
    .local p1, "prefset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v3, " setTemperatureUnit"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    const-string/jumbo v2, "temperature_unit"

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 308
    const-string/jumbo v2, "temperature_unit"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 309
    .local v1, "value":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " TEMPERATURE_UNIT "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    const-string v2, "C"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v0, 0x27101

    .line 312
    .local v0, "unit":I
    :goto_0
    invoke-virtual {p2, v0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->setTemperatureUnit(I)V

    .line 315
    .end local v0    # "unit":I
    .end local v1    # "value":Ljava/lang/String;
    :cond_0
    return-void

    .line 311
    .restart local v1    # "value":Ljava/lang/String;
    :cond_1
    const v0, 0x27102

    goto :goto_0
.end method

.method private setWeight(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 8
    .param p2, "healthprofile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/samsung/android/sdk/health/content/ShealthProfile;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .local p1, "prefset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    const v7, 0x1fbd1

    const/high16 v6, -0x40800000    # -1.0f

    .line 566
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, " setWeight "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    const/high16 v1, -0x40800000    # -1.0f

    .line 574
    .local v1, "weight":F
    const-string/jumbo v3, "weight_unit"

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 576
    const-string/jumbo v3, "weight_unit"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 577
    .local v2, "weight_unit":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Weight Unit  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    const-string v3, "kg"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 580
    const-string/jumbo v3, "weight"

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 582
    const-string/jumbo v3, "weight"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 594
    :cond_0
    :goto_0
    cmpl-float v3, v1, v6

    if-eqz v3, :cond_1

    .line 596
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Weight in KG"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    invoke-virtual {p2, v7}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V

    .line 598
    invoke-virtual {p2, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    .line 601
    .end local v2    # "weight_unit":Ljava/lang/String;
    :cond_1
    cmpl-float v3, v1, v6

    if-nez v3, :cond_3

    .line 603
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, " Weight Unit is LB "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    const-string/jumbo v3, "weight_lb"

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 606
    const-string/jumbo v3, "weight_lb"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 616
    :cond_2
    :goto_1
    cmpl-float v3, v1, v6

    if-eqz v3, :cond_3

    .line 618
    invoke-static {v1}, Lcom/sec/android/service/health/cp/database/datahandler/UnitHelper;->convertLbToKg(F)F

    move-result v1

    .line 619
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Weight in KG "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    const v3, 0x1fbd2

    invoke-virtual {p2, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V

    .line 621
    invoke-virtual {p2, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    .line 626
    :cond_3
    cmpl-float v3, v1, v6

    if-nez v3, :cond_4

    .line 628
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v4, " Weightunit is not present default is KG "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    const-string/jumbo v3, "weight"

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 631
    const-string/jumbo v3, "weight"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 632
    invoke-virtual {p2, v7}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V

    .line 633
    invoke-virtual {p2, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    .line 637
    :cond_4
    return-void

    .line 584
    .restart local v2    # "weight_unit":Ljava/lang/String;
    :cond_5
    const-string/jumbo v3, "secure_weight"

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 586
    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v3, "secure_weight"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 587
    .local v0, "secure_weight":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 589
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto/16 :goto_0

    .line 608
    .end local v0    # "secure_weight":Ljava/lang/String;
    .end local v2    # "weight_unit":Ljava/lang/String;
    :cond_6
    const-string/jumbo v3, "secure_weight_lb"

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 610
    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    const-string/jumbo v3, "secure_weight_lb"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-virtual {v5}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Lcom/sec/android/service/health/cp/common/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 611
    .restart local v0    # "secure_weight":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 613
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto/16 :goto_1
.end method

.method private upgradeFrom1to3(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 122
    return-void
.end method

.method private upgradeFrom3to4(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 127
    return-void
.end method

.method private upgradeFrom4to5(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 132
    return-void
.end method

.method private upgradeFrom5to6(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 16
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 136
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v14, "upgradeFrom5to6"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    new-instance v4, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mIsUpgradeToMainDb:Z

    invoke-direct {v4, v13, v14}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;-><init>(Landroid/content/Context;Z)V

    .line 146
    .local v4, "hShealthProfile":Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->isExist()Z

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_1

    .line 148
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v14, " Profile is already set. Skip the profile migration process. "

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    const/4 v10, 0x0

    .line 156
    .local v10, "prefsSet":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHARED_PREF_FOLDER_NAME:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_SHARED_PREF_FILE:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 157
    .local v12, "sharedPrefFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_a

    .line 159
    const/4 v2, 0x0

    .line 160
    .local v2, "fin":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 163
    .local v7, "obIn":Ljava/io/ObjectInputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    .end local v2    # "fin":Ljava/io/FileInputStream;
    .local v3, "fin":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v8, Ljava/io/ObjectInputStream;

    invoke-direct {v8, v3}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_c
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 165
    .end local v7    # "obIn":Ljava/io/ObjectInputStream;
    .local v8, "obIn":Ljava/io/ObjectInputStream;
    :try_start_2
    invoke-virtual {v8}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v13

    move-object v0, v13

    check-cast v0, Ljava/util/Map;

    move-object v10, v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_d
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 174
    if-eqz v8, :cond_2

    .line 178
    :try_start_3
    invoke-virtual {v8}, Ljava/io/ObjectInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 185
    :cond_2
    :goto_1
    if-eqz v3, :cond_3

    .line 189
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 196
    :cond_3
    :goto_2
    new-instance v9, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "_old"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v9, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 197
    .local v9, "oldFile":Ljava/io/File;
    invoke-virtual {v12, v9}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-object v7, v8

    .end local v8    # "obIn":Ljava/io/ObjectInputStream;
    .restart local v7    # "obIn":Ljava/io/ObjectInputStream;
    move-object v2, v3

    .line 214
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "obIn":Ljava/io/ObjectInputStream;
    .end local v9    # "oldFile":Ljava/io/File;
    :cond_4
    :goto_3
    if-eqz v10, :cond_0

    .line 216
    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 218
    .local v6, "key":Ljava/lang/String;
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " key "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " value "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface {v10, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    const-string/jumbo v13, "privacy_public"

    invoke-virtual {v13, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 226
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v14, " updatePrivacy "

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    invoke-interface {v10, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v13}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setPrivacy(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_4

    .line 180
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "key":Ljava/lang/String;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "obIn":Ljava/io/ObjectInputStream;
    :catch_0
    move-exception v1

    .line 182
    .local v1, "e":Ljava/io/IOException;
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 191
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 193
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 167
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v8    # "obIn":Ljava/io/ObjectInputStream;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    .restart local v7    # "obIn":Ljava/io/ObjectInputStream;
    :catch_2
    move-exception v1

    .line 170
    .local v1, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_5
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error while restoring  the shared_pref file... "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 174
    if-eqz v7, :cond_6

    .line 178
    :try_start_6
    invoke-virtual {v7}, Ljava/io/ObjectInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 185
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_6
    if-eqz v2, :cond_7

    .line 189
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 196
    :cond_7
    :goto_7
    new-instance v9, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "_old"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v9, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 197
    .restart local v9    # "oldFile":Ljava/io/File;
    invoke-virtual {v12, v9}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto/16 :goto_3

    .line 180
    .end local v9    # "oldFile":Ljava/io/File;
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v1

    .line 182
    .local v1, "e":Ljava/io/IOException;
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 191
    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 193
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 174
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v13

    :goto_8
    if-eqz v7, :cond_8

    .line 178
    :try_start_8
    invoke-virtual {v7}, Ljava/io/ObjectInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 185
    :cond_8
    :goto_9
    if-eqz v2, :cond_9

    .line 189
    :try_start_9
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 196
    :cond_9
    :goto_a
    new-instance v9, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "_old"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v9, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 197
    .restart local v9    # "oldFile":Ljava/io/File;
    invoke-virtual {v12, v9}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 198
    throw v13

    .line 180
    .end local v9    # "oldFile":Ljava/io/File;
    :catch_5
    move-exception v1

    .line 182
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 191
    .end local v1    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 193
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 204
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "obIn":Ljava/io/ObjectInputStream;
    :cond_a
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v14, "getting shared pref from restore"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mContext:Landroid/content/Context;

    const-string/jumbo v14, "restore_com.sec.android.app.shealth_preferences"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 206
    .local v11, "restore_pref":Landroid/content/SharedPreferences;
    invoke-interface {v11}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v10

    .line 207
    if-eqz v10, :cond_b

    invoke-interface {v10}, Ljava/util/Map;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_4

    .line 210
    :cond_b
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v14, "getting shared pref from Appupgrade"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v13}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v10

    goto/16 :goto_3

    .line 229
    .end local v11    # "restore_pref":Landroid/content/SharedPreferences;
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "key":Ljava/lang/String;
    :cond_c
    const-string v13, "glucose_unit"

    invoke-virtual {v13, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 231
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v14, " update glucose_unit "

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    invoke-interface {v10, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v13}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setGlucoseUnit(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 236
    .end local v6    # "key":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setNotification(Ljava/util/Map;)V

    .line 237
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    if-nez v13, :cond_e

    .line 239
    new-instance v13, Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-direct {v13}, Lcom/sec/android/service/health/cp/common/AESEncryption;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->mAes:Lcom/sec/android/service/health/cp/common/AESEncryption;

    .line 243
    :cond_e
    :try_start_a
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setBirthDate(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    :try_end_a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_a .. :try_end_a} :catch_7

    .line 253
    :goto_b
    :try_start_b
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setName(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    :try_end_b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_b .. :try_end_b} :catch_8

    .line 263
    :goto_c
    :try_start_c
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setGender(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    :try_end_c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_c .. :try_end_c} :catch_9

    .line 273
    :goto_d
    :try_start_d
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setHeight(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    :try_end_d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_d .. :try_end_d} :catch_a

    .line 283
    :goto_e
    :try_start_e
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setWeight(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    :try_end_e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_e .. :try_end_e} :catch_b

    .line 291
    :goto_f
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setDistanceUnit(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V

    .line 293
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setTemperatureUnit(Ljava/util/Map;Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;)V

    .line 295
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setActivityType(Ljava/util/Map;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V

    .line 297
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->setMigrationTimeZone(Ljava/util/Map;)V

    .line 299
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->save()V

    goto/16 :goto_0

    .line 245
    :catch_7
    move-exception v1

    .line 247
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v14, " UnsupportedEncodingException while setting birth date"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_b

    .line 255
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_8
    move-exception v1

    .line 257
    .restart local v1    # "e":Ljava/io/UnsupportedEncodingException;
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v14, " UnsupportedEncodingException while setting Name"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_c

    .line 265
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_9
    move-exception v1

    .line 267
    .restart local v1    # "e":Ljava/io/UnsupportedEncodingException;
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v14, " UnsupportedEncodingException while setting Gender"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_d

    .line 275
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_a
    move-exception v1

    .line 277
    .restart local v1    # "e":Ljava/io/UnsupportedEncodingException;
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v14, " UnsupportedEncodingException while setting Height"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_e

    .line 285
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_b
    move-exception v1

    .line 287
    .restart local v1    # "e":Ljava/io/UnsupportedEncodingException;
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v14, " UnsupportedEncodingException while setting Weight"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_f

    .line 174
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v5    # "i$":Ljava/util/Iterator;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v7    # "obIn":Ljava/io/ObjectInputStream;
    :catchall_1
    move-exception v13

    move-object v2, v3

    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_8

    .end local v2    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "obIn":Ljava/io/ObjectInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "obIn":Ljava/io/ObjectInputStream;
    :catchall_2
    move-exception v13

    move-object v7, v8

    .end local v8    # "obIn":Ljava/io/ObjectInputStream;
    .restart local v7    # "obIn":Ljava/io/ObjectInputStream;
    move-object v2, v3

    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_8

    .line 167
    .end local v2    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    :catch_c
    move-exception v1

    move-object v2, v3

    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_5

    .end local v2    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "obIn":Ljava/io/ObjectInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "obIn":Ljava/io/ObjectInputStream;
    :catch_d
    move-exception v1

    move-object v7, v8

    .end local v8    # "obIn":Ljava/io/ObjectInputStream;
    .restart local v7    # "obIn":Ljava/io/ObjectInputStream;
    move-object v2, v3

    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_5
.end method


# virtual methods
.method public upgrade(II)Z
    .locals 2
    .param p1, "oldVersion"    # I
    .param p2, "newVersion"    # I

    .prologue
    .line 103
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 104
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    packed-switch p1, :pswitch_data_0

    .line 116
    :goto_0
    :pswitch_0
    const/4 v1, 0x1

    return v1

    .line 107
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->upgradeFrom1to3(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 109
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->upgradeFrom3to4(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 112
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->upgradeFrom4to5(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 114
    :pswitch_4
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->upgradeFrom5to6(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
