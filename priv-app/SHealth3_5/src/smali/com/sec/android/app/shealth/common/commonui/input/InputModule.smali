.class public abstract Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
.super Landroid/widget/FrameLayout;
.source "InputModule.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/input/IInputModule;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;
    }
.end annotation


# static fields
.field private static final DISABLED_VIEW_ALPHA:F = 0.4f

.field private static final ENABLED_VIEW_ALPHA:F = 1.0f

.field private static final SAVED_STATE_KEY:Ljava/lang/String; = "SAVED_STATE_KEY"

.field private static final VALUE_EDIT_TEXT_ID:Ljava/lang/String; = "VALUE_EDIT_TEXT_ID"

.field private static final sDefaultNumberSeparator:Ljava/lang/String; = "."


# instance fields
.field private mCurrentValue:F

.field private mDecButton:Landroid/widget/ImageButton;

.field private mForegroundImageView:Landroid/widget/ImageView;

.field private mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

.field private mIncButton:Landroid/widget/ImageButton;

.field private mIncDecOnClickListener:Landroid/view/View$OnClickListener;

.field private mIncDecOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mIncDecTouchListener:Landroid/view/View$OnTouchListener;

.field private mKeyListener:Landroid/view/View$OnKeyListener;

.field private mMaxInputRange:F

.field private mMaxNormalRange:Ljava/lang/Float;

.field private mMinInputRange:F

.field private mMinNormalRange:Ljava/lang/Float;

.field private mMoveDistance:F

.field mNeedToUpdateOtherView:Z

.field private mRootFocusLayout:Landroid/view/ViewGroup;

.field private mSystemNumberSeparator:Ljava/lang/String;

.field private mUnitTextView:Landroid/widget/TextView;

.field public mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

.field private onValueOutOfRangeListener:Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mNeedToUpdateOtherView:Z

    .line 72
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    .line 406
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    .line 523
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 617
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$9;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecOnClickListener:Landroid/view/View$OnClickListener;

    .line 625
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$10;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 114
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->initialize(Landroid/content/Context;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->EMPTY:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setDrawingStrategy(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mNeedToUpdateOtherView:Z

    .line 72
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    .line 406
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    .line 523
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 617
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$9;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecOnClickListener:Landroid/view/View$OnClickListener;

    .line 625
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$10;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 120
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->initialize(Landroid/content/Context;)V

    .line 121
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setDrawingStrategy(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 125
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mNeedToUpdateOtherView:Z

    .line 72
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    .line 406
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    .line 523
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 617
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$9;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecOnClickListener:Landroid/view/View$OnClickListener;

    .line 625
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$10;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 126
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->initialize(Landroid/content/Context;)V

    .line 127
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setDrawingStrategy(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 128
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->onValueOutOfRangeListener:Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
    .param p1, "x1"    # F

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->incrementDecrementValue(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->increaseDecreaseValue(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Ljava/lang/Float;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinNormalRange:Ljava/lang/Float;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Ljava/lang/Float;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxNormalRange:Ljava/lang/Float;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
    .param p1, "x1"    # F

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->changeTextColorAccordingToValue(F)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
    .param p1, "x1"    # F

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->changeForegroundAccordingToValue(F)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
    .param p1, "x1"    # F

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->updateIncDecButtons(F)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxInputRange:F

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinInputRange:F

    return v0
.end method

.method private changeForegroundAccordingToValue(F)V
    .locals 2
    .param p1, "changedNumber"    # F

    .prologue
    .line 580
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinNormalRange:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxNormalRange:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isInRange(FFF)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getNormalBackgroundDrawableID()I

    move-result v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setForegroundDrawable(I)V

    .line 582
    return-void

    .line 580
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getAbnormalBackgroundDrawableID()I

    move-result v0

    goto :goto_0
.end method

.method private changeTextColorAccordingToValue(F)V
    .locals 4
    .param p1, "changedNumber"    # F

    .prologue
    .line 589
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinNormalRange:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxNormalRange:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-static {p1, v0, v3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isInRange(FFF)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$color;->inputmodule_normal_color:I

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setTextColor(I)V

    .line 592
    return-void

    .line 589
    :cond_0
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$color;->inputmodule_abnormal_color:I

    goto :goto_0
.end method

.method private increaseDecreaseValue(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 651
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;->DIRECTION_LEFT:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->animateButtonLongpress(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;)V

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 653
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 654
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;->DIRECTION_RIGHT:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->animateButtonLongpress(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;)V

    goto :goto_0
.end method

.method private incrementDecrementValue(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 635
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->isInputWithEmptyListener()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 637
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getEmptyFieldListener()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;->processEmptyField()V

    .line 638
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    const v1, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setValue(F)V

    .line 648
    :cond_0
    :goto_0
    return-void

    .line 641
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->incrementValue()V

    goto :goto_0

    .line 645
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 646
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->decrementValue()V

    goto :goto_0
.end method

.method private initialize(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getRootLayoutId()I

    move-result v1

    invoke-static {p1, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 152
    .local v0, "rootView":Landroid/view/View;
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->inputmodule_tv_unit:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mUnitTextView:Landroid/widget/TextView;

    .line 153
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->inputmodule_btn_increase:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    .line 154
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->inputmodule_btn_decrease:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    .line 155
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->inputmodule_et_value:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setId(I)V

    .line 157
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->gradation_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    .line 158
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->foreground_image_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mForegroundImageView:Landroid/widget/ImageView;

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 173
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->registerListener()V

    .line 174
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->initAdditionalViews(Landroid/view/View;)V

    .line 175
    return-void
.end method

.method private registerListener()V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$2;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setUpdateListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnUpdateListener;)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setOnValueChangedListener(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnValueChangedListener;)V

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setOnControllerTapListener(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncDecOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 404
    return-void
.end method

.method private setDimmed(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setAlpha(F)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setAlpha(F)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mUnitTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mForegroundImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 263
    return-void
.end method

.method private setDrawingStrategy(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 131
    sget-object v2, Lcom/sec/android/app/shealth/common/commonui/R$styleable;->InputModule:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 132
    .local v1, "typedArray":Landroid/content/res/TypedArray;
    invoke-virtual {v1, v3, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    .line 133
    .local v0, "charSequence":I
    packed-switch v0, :pswitch_data_0

    .line 139
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    sget-object v3, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->NORMAL_RANGE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setDrawingStrategy(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;)V

    .line 142
    :goto_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 143
    return-void

    .line 135
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    sget-object v3, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->EMPTY:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setDrawingStrategy(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;)V

    goto :goto_0

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private setEnableBtn(Landroid/widget/ImageButton;Z)V
    .locals 0
    .param p1, "imageButton"    # Landroid/widget/ImageButton;
    .param p2, "isEnable"    # Z

    .prologue
    .line 266
    invoke-virtual {p1, p2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 267
    invoke-virtual {p1, p2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 268
    if-nez p2, :cond_0

    .line 269
    invoke-virtual {p1, p2}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 272
    :cond_0
    return-void
.end method

.method private setForegroundDrawable(I)V
    .locals 2
    .param p1, "backgroundID"    # I

    .prologue
    .line 659
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mForegroundImageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 660
    return-void
.end method

.method private setTextFilters()V
    .locals 4

    .prologue
    .line 560
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMoveDistance:F

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinInputRange:F

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxInputRange:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setIntervalValues(FFF)V

    .line 561
    return-void
.end method

.method private updateIncDecButtons(F)V
    .locals 3
    .param p1, "changedNumber"    # F

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 565
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxInputRange:F

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    .line 567
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setEnableBtn(Landroid/widget/ImageButton;Z)V

    .line 568
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setEnableBtn(Landroid/widget/ImageButton;Z)V

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 569
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinInputRange:F

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_2

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setEnableBtn(Landroid/widget/ImageButton;Z)V

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setEnableBtn(Landroid/widget/ImageButton;Z)V

    goto :goto_0

    .line 573
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setEnableBtn(Landroid/widget/ImageButton;Z)V

    .line 574
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setEnableBtn(Landroid/widget/ImageButton;Z)V

    goto :goto_0
.end method

.method private updateViewWithInputRange()V
    .locals 3

    .prologue
    .line 200
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setTextFilters()V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinInputRange:F

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxInputRange:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setInputRange(FF)V

    .line 202
    return-void
.end method


# virtual methods
.method protected abstract getAbnormalBackgroundDrawableID()I
.end method

.method public getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    return-object v0
.end method

.method protected getMinInputValue()F
    .locals 1

    .prologue
    .line 556
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinInputRange:F

    return v0
.end method

.method protected abstract getNormalBackgroundDrawableID()I
.end method

.method protected abstract getRootLayoutId()I
.end method

.method public getValue()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getFloatValue()Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public hideDecBtn(Z)V
    .locals 2
    .param p1, "isHide"    # Z

    .prologue
    .line 296
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 297
    return-void

    .line 296
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideIncBtn(Z)V
    .locals 2
    .param p1, "isHide"    # Z

    .prologue
    .line 291
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 292
    return-void

    .line 291
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideKeyboard()V
    .locals 2

    .prologue
    .line 606
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 607
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setCursorVisible(Z)V

    .line 608
    return-void
.end method

.method public hideUnit()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mUnitTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 185
    return-void
.end method

.method protected abstract initAdditionalViews(Landroid/view/View;)V
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isInputWithEmptyListener()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 276
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 278
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getEmptyFieldListener()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 284
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 278
    goto :goto_0

    .line 282
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v5, "."

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 283
    .local v0, "value":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->makeFloatValid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 284
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isOutOfRange()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getEmptyFieldListener()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    move-result-object v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public isOutOfRange()Z
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isOutOfRange()Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 689
    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    .line 690
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    const-string v2, "VALUE_EDIT_TEXT_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setId(I)V

    .line 691
    const-string v1, "SAVED_STATE_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 692
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 681
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 682
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "SAVED_STATE_KEY"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 683
    const-string v1, "VALUE_EDIT_TEXT_ID"

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 684
    return-object v0
.end method

.method protected onTouchModeEditTextColor(Z)V
    .locals 3
    .param p1, "isTaped"    # Z

    .prologue
    const/4 v2, 0x0

    .line 695
    if-eqz p1, :cond_0

    .line 696
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setCursorVisible(Z)V

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    const v1, -0xc25400

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setTextColor(I)V

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setFocusableInTouchMode(Z)V

    .line 707
    :goto_0
    return-void

    .line 703
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setTextColor(I)V

    .line 705
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setFocusableInTouchMode(Z)V

    goto :goto_0
.end method

.method public refreshView()V
    .locals 1

    .prologue
    .line 669
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->changeForegroundAccordingToValue(F)V

    .line 670
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->changeTextColorAccordingToValue(F)V

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->invalidate()V

    .line 672
    return-void
.end method

.method public setAdditionalDrawingType(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;)V
    .locals 1
    .param p1, "type"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    .prologue
    .line 676
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setDrawingStrategy(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;)V

    .line 677
    return-void
.end method

.method public setEnabled(Z)V
    .locals 4
    .param p1, "isEnable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 244
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 245
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxInputRange:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setEnableBtn(Landroid/widget/ImageButton;Z)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinInputRange:F

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_1

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setEnableBtn(Landroid/widget/ImageButton;Z)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setEnabled(Z)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setFocusable(Z)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setFocusableInTouchMode(Z)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setEnabled(Z)V

    .line 253
    if-eqz p1, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_2
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setDimmed(F)V

    .line 254
    return-void

    :cond_0
    move v0, p1

    .line 245
    goto :goto_0

    :cond_1
    move v1, p1

    .line 246
    goto :goto_1

    .line 253
    :cond_2
    const v0, 0x3ecccccd    # 0.4f

    goto :goto_2
.end method

.method public setInputRange(FF)V
    .locals 0
    .param p1, "minInputRange"    # F
    .param p2, "maxInputRange"    # F

    .prologue
    .line 194
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinInputRange:F

    .line 195
    iput p2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxInputRange:F

    .line 196
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->updateViewWithInputRange()V

    .line 197
    return-void
.end method

.method public setMoveDistance(F)V
    .locals 1
    .param p1, "distance"    # F

    .prologue
    .line 214
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMoveDistance:F

    .line 216
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->setTextFilters()V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setInterval(F)V

    .line 218
    return-void
.end method

.method public setNormalRange(FF)V
    .locals 3
    .param p1, "minNormalRange"    # F
    .param p2, "maxNormalRange"    # F

    .prologue
    .line 206
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinNormalRange:Ljava/lang/Float;

    .line 207
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxNormalRange:Ljava/lang/Float;

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinNormalRange:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxNormalRange:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setNormalRange(FF)V

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->refreshView()V

    .line 210
    return-void
.end method

.method public setOnFloatInputChangedListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;

    .prologue
    .line 599
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setOnFloatInputChangedListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;)V

    .line 600
    return-void
.end method

.method public setTextEmptyListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;)V
    .locals 1
    .param p1, "emptyFieldListener"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    .prologue
    .line 716
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setEmptyFieldListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;)V

    .line 722
    :cond_0
    return-void
.end method

.method public setUnit(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "unit"    # Ljava/lang/CharSequence;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mUnitTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    return-void
.end method

.method public setValue(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->stopInertia()V

    .line 223
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setFloatValue(F)V

    .line 225
    return-void
.end method

.method public setValueOutOfRangeListener(Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;

    .prologue
    .line 711
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->onValueOutOfRangeListener:Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;

    .line 713
    return-void
.end method

.method public showKeyboard()V
    .locals 2

    .prologue
    .line 614
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 615
    return-void
.end method

.method public showUnit()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mUnitTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    return-void
.end method

.method public stopInertia()V
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->stopInertia()V

    .line 665
    return-void
.end method
