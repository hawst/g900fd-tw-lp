.class public final enum Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
.super Ljava/lang/Enum;
.source "WalkingRequestResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

.field public static final enum CONNECTION_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

.field public static final enum DEFAULT:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

.field public static final enum FAILED_TO_GET_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

.field public static final enum INVALID_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

.field public static final enum OTHER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

.field public static final enum RESTORE_LOCAL:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

.field public static final enum SERVER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

.field public static final enum SUCCESS:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->DEFAULT:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 5
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SUCCESS:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 6
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SERVER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 7
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    const-string v1, "CONNECTION_ERROR"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->CONNECTION_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 8
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    const-string v1, "OTHER_ERROR"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->OTHER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 9
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    const-string v1, "RESTORE_LOCAL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->RESTORE_LOCAL:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 10
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    const-string v1, "INVALID_TOKEN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->INVALID_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 11
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    const-string v1, "FAILED_TO_GET_TOKEN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->FAILED_TO_GET_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 3
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->DEFAULT:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SUCCESS:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SERVER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->CONNECTION_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->OTHER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->RESTORE_LOCAL:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->INVALID_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->FAILED_TO_GET_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->$VALUES:[Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->$VALUES:[Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    return-object v0
.end method
