.class Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getSwipeDetector()Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSwipeLeft()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getPrevDate()Ljava/util/Date;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->moveLeft()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public onSwipeRight()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getNextDate()Ljava/util/Date;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->moveRight()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method
