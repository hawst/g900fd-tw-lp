.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$8;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0

    .prologue
    .line 1450
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 1452
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1453
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    check-cast p2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$WalkingMateDayStepBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$WalkingMateDayStepBinder;->getService()Landroid/app/Service;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2802(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 1455
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->registerListener()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    .line 1456
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isStartWalking()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$3000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1458
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->updateWidgets()V

    .line 1460
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateFromDB()V

    .line 1461
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 1464
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2802(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 1465
    return-void
.end method
