.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;
.super Ljava/lang/Object;
.source "ListPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalOnItemClickedListener"
.end annotation


# instance fields
.field private final index:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;I)V
    .locals 0
    .param p2, "index"    # I

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;->index:I

    .line 189
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;ILcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;

    .prologue
    .line 184
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->onItemClickedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->onItemClickedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;->index:I

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)[Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;->index:I

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;->onItemClicked(ILjava/lang/String;Landroid/widget/PopupWindow;)V

    .line 196
    :cond_0
    return-void
.end method
