.class Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;
.super Ljava/lang/Object;
.source "CignaLoadGeneralActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

.field final synthetic val$dialogTag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->val$dialogTag:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->val$dialogTag:Ljava/lang/String;

    const-string v1, "COACH_PROMPT_RESTORE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_2

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->access$002(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .line 129
    const-string v0, "COACH_PROMPT_RESTORE"

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->val$dialogTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->isInitialBackupRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->startCignaRestore(Z)V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->startCignaRestore(Z)V

    goto :goto_0

    .line 146
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->NEGATIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->dismissRestoreTriggerPopup()V

    goto :goto_0

    .line 151
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->val$dialogTag:Ljava/lang/String;

    const-string v1, "RESTORE_PROGRESS_POPUP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->val$dialogTag:Ljava/lang/String;

    const-string v1, "RESTORE_ERROR_POPUP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->val$dialogTag:Ljava/lang/String;

    const-string v1, "RESTORE_SUCCESS_POPUP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;->val$dialogTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;->onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V

    goto :goto_0
.end method
