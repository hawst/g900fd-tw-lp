.class Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;
.super Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
.source "FoodListItem.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mExtendedFoodInfo:Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

.field private mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

.field private mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;)V
    .locals 0
    .param p1, "foodInfoItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p2, "foodInfoDao"    # Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 40
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    .line 41
    return-void
.end method


# virtual methods
.method public addToFavorites()V
    .locals 3

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "added to favorite: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setFavorite(Z)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->saveToDatabase()V

    .line 59
    return-void
.end method

.method public createSelectedPanelHolder()Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    .locals 2

    .prologue
    .line 107
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 98
    instance-of v2, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;

    if-nez v2, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 101
    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;

    .line 102
    .local v0, "that":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    iget-object v3, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    if-nez v2, :cond_0

    goto :goto_1
.end method

.method protected getDescription(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getDescription(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExtendedFoodInfo()Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mExtendedFoodInfo:Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    return-object v0
.end method

.method public getListItemId()J
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getListItemType()Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->FOOD:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string v0, ""

    .end local v0    # "name":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getView(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "outState"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->setAreItemsExpandable(Z)V

    .line 46
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getView(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getFavorite()Z

    move-result v0

    return v0
.end method

.method public removeFromFavorites()V
    .locals 3

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "removed from favorite:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setFavorite(Z)V

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->saveToDatabase()V

    .line 66
    return-void
.end method

.method public removeFromMyCategory()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSourceType(I)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setFavorite(Z)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)I

    .line 115
    return-void
.end method

.method public saveToDatabase()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 81
    return-void
.end method

.method public setExtendedFoodInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V
    .locals 0
    .param p1, "mExtendedFoodInfo"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;->mExtendedFoodInfo:Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .line 85
    return-void
.end method
