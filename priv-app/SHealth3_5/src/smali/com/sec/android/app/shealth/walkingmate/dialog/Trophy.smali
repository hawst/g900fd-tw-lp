.class public abstract enum Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;
.super Ljava/lang/Enum;
.source "Trophy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

.field public static final enum FIRST:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

.field public static final enum LOOSER:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

.field public static final enum SECOND:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

.field public static final enum THIRD:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy$1;

    const-string v1, "FIRST"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->FIRST:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    .line 18
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy$2;

    const-string v1, "SECOND"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->SECOND:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    .line 29
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy$3;

    const-string v1, "THIRD"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->THIRD:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    .line 40
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy$4;

    const-string v1, "LOOSER"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->LOOSER:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    .line 6
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->FIRST:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->SECOND:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->THIRD:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->LOOSER:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->$VALUES:[Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/sec/android/app/shealth/walkingmate/dialog/Trophy$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy$1;

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getTrophy(I)Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;
    .locals 1
    .param p0, "position"    # I

    .prologue
    .line 56
    packed-switch p0, :pswitch_data_0

    .line 64
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->LOOSER:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    :goto_0
    return-object v0

    .line 58
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->FIRST:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    goto :goto_0

    .line 60
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->SECOND:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    goto :goto_0

    .line 62
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->THIRD:Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->$VALUES:[Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    return-object v0
.end method


# virtual methods
.method public abstract getDialogIcon()I
.end method

.method public abstract getItemIcon(Z)I
.end method
