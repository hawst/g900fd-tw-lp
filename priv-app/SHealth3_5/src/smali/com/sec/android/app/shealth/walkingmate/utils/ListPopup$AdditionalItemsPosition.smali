.class public final enum Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;
.super Ljava/lang/Enum;
.source "ListPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AdditionalItemsPosition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

.field public static final enum BELOW:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

.field public static final enum NONE:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

.field public static final enum TO_RIGHT_OF:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    const-string v1, "BELOW"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;->BELOW:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    const-string v1, "TO_RIGHT_OF"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;->TO_RIGHT_OF:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;->NONE:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;->BELOW:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;->TO_RIGHT_OF:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;->NONE:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;->$VALUES:[Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 95
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;->$VALUES:[Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    return-object v0
.end method
