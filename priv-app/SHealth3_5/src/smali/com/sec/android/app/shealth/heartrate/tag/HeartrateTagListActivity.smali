.class public Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HeartrateTagListActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$10;,
        Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$SpinnerCustomAdapter;,
        Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;,
        Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$DeleteDialogButtonController;
    }
.end annotation


# static fields
.field private static final DELETE_POPUP:Ljava/lang/String; = "log_delete_popup"

.field private static final SINGLE_ITEM:I = 0x1

.field private static TAG:Ljava/lang/String;


# instance fields
.field private isDeleteMode:Z

.field private isDoneMode:Z

.field private isItemClicked:Z

.field private isSelectMode:Z

.field private mCheckBox:Landroid/widget/CheckBox;

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

.field private mIconResourceIds:[Ljava/lang/Integer;

.field private mIsItemsSelected:Z

.field private mMenu:Landroid/view/Menu;

.field private mNoDataLayout:Landroid/widget/LinearLayout;

.field private mScrollPosition:I

.field private mScrollY:I

.field private mSelectDataAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectSpinner:Landroid/widget/Spinner;

.field mSelectUnselectAllListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mTagCount:I

.field private mTagIconId:[Ljava/lang/Integer;

.field private mTagListView:Landroid/widget/ListView;

.field private mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

.field private mTagNames:[Ljava/lang/String;

.field mTagsToDelete:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private onSelectAllRelClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    const-class v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 93
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isDeleteMode:Z

    .line 94
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isSelectMode:Z

    .line 95
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isItemClicked:Z

    .line 100
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    .line 101
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagCount:I

    .line 109
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollPosition:I

    .line 110
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollY:I

    .line 111
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isDoneMode:Z

    .line 122
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$1;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mDialogControllerMap:Ljava/util/Map;

    .line 454
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$7;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->onSelectAllRelClickListener:Landroid/view/View$OnClickListener;

    .line 674
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$8;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectUnselectAllListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 900
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isDeleteMode:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->showDeletePopup(Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->deleteDone()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)[Ljava/lang/Integer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagIconId:[Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateCheck(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isSelectMode:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isItemClicked:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isItemClicked:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;
    .param p1, "x1"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateSelectedCount(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->toggleSelectAllCheckBox(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->enableActionBarButtons(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    return-object v0
.end method

.method private addSelectSpinner(I)V
    .locals 5
    .param p1, "count"    # I

    .prologue
    .line 816
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030003

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectSpinner:Landroid/widget/Spinner;

    .line 817
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    .line 818
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    const v1, 0x7f090074

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 819
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090071

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 820
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090073

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_0

    .line 823
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$SpinnerCustomAdapter;

    const v1, 0x7f03021a

    const v2, 0x7f08080a

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$SpinnerCustomAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    .line 824
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x7f030219

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 827
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 829
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$9;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 858
    return-void
.end method

.method private cancelDeleteUI()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v5, 0x0

    .line 755
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollPosition:I

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 757
    .local v7, "v":Landroid/view/View;
    if-eqz v7, :cond_1

    .line 758
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollY:I

    .line 761
    :goto_0
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isDeleteMode:Z

    .line 762
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isDoneMode:Z

    .line 763
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->invalidateOptionsMenu()V

    .line 764
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagNames:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mIconResourceIds:[Ljava/lang/Integer;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Landroid/app/Activity;[Ljava/lang/String;[Ljava/lang/Integer;ZLjava/util/HashSet;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    .line 766
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 767
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 768
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagNames:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagNames:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_2

    .line 770
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mNoDataLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setVisibility(I)V

    .line 776
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollPosition:I

    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollY:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 777
    return-void

    .line 760
    :cond_1
    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollY:I

    goto :goto_0

    .line 773
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 774
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mNoDataLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method private createCustomOptionMenu()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 272
    const v0, 0x7f0207a4

    .line 274
    .local v0, "ACTION_BAR_ADD_BUTTON_SELECTOR":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 275
    new-instance v4, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$4;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V

    .line 295
    .local v4, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f0207a4

    const v6, 0x7f09003f

    invoke-direct {v1, v5, v7, v6, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    .line 297
    .local v1, "actionBarButtonAddList":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    new-array v6, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 299
    new-instance v3, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$5;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V

    .line 308
    .local v3, "actionBarButtonDeleteListener":Landroid/view/View$OnClickListener;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->getCount()I

    move-result v5

    if-lez v5, :cond_0

    .line 309
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f0207b0

    const v6, 0x7f090035

    invoke-direct {v2, v5, v7, v6, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    .line 311
    .local v2, "actionBarButtonDelete":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    new-array v6, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v2, v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 314
    .end local v2    # "actionBarButtonDelete":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->getCount()I

    move-result v5

    if-lez v5, :cond_1

    .line 315
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 318
    :goto_0
    return-void

    .line 317
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_0
.end method

.method private deleteDone()V
    .locals 3

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 383
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->deleteTag()V

    .line 384
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->resumeEdit()V

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.heartrate"

    const-string v2, "HR14"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :cond_0
    return-void
.end method

.method private disableOptionMenu()V
    .locals 2

    .prologue
    .line 947
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 948
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 949
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mMenu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 951
    :cond_0
    return-void
.end method

.method private enableActionBarButtons(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    const/4 v2, 0x0

    .line 439
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->disableOptionMenu()V

    .line 440
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isDoneMode:Z

    if-nez v0, :cond_1

    .line 441
    if-eqz p1, :cond_0

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisible(ZI)V

    .line 449
    :goto_0
    return-void

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisible(ZI)V

    goto :goto_0

    .line 447
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, p1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    goto :goto_0
.end method

.method private enableOptionMenu()V
    .locals 3

    .prologue
    .line 938
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 939
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 940
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100017

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mMenu:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 941
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 944
    :cond_0
    return-void
.end method

.method private initializeTagList()V
    .locals 10

    .prologue
    .line 690
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getCustomTags()Ljava/util/ArrayList;

    move-result-object v9

    .line 691
    .local v9, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagCount:I

    .line 692
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagCount:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagNames:[Ljava/lang/String;

    .line 693
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagCount:I

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mIconResourceIds:[Ljava/lang/Integer;

    .line 694
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagCount:I

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagIconId:[Ljava/lang/Integer;

    .line 695
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagCount:I

    if-ge v7, v0, :cond_0

    .line 696
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 697
    .local v8, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagNames:[Ljava/lang/String;

    iget-object v1, v8, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    aput-object v1, v0, v7

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mIconResourceIds:[Ljava/lang/Integer;

    iget v1, v8, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagIconId:[Ljava/lang/Integer;

    iget v1, v8, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagIconId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    .line 695
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 701
    .end local v8    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagNames:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mIconResourceIds:[Ljava/lang/Integer;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Landroid/app/Activity;[Ljava/lang/String;[Ljava/lang/Integer;ZLjava/util/HashSet;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    .line 702
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 703
    return-void
.end method

.method private removeDeleteMode()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 479
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->setDeleteMode(Z)V

    .line 480
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->setMenuDeleteMode(Z)V

    .line 481
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->setCheckAll(Z)V

    .line 482
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isDeleteMode:Z

    .line 483
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isSelectMode:Z

    .line 484
    const v1, 0x7f08054d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 485
    .local v0, "mCheckBox":Landroid/widget/CheckBox;
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 487
    const v1, 0x7f08054c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 488
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 489
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setCancelDoneVisibility(Z)V

    .line 490
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->clearCheckedItems()V

    .line 491
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 492
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->refreshAdapter()V

    .line 494
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0207a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 496
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f020023

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarBackground(I)V

    .line 498
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f09005a

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 500
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->invalidateOptionsMenu()V

    .line 502
    return-void
.end method

.method private removeSelectSpinner()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 892
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeSpinnerView()V

    .line 893
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    .line 894
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    .line 895
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 896
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 898
    :cond_0
    return-void
.end method

.method private resumeEdit()V
    .locals 2

    .prologue
    .line 390
    const v1, 0x7f08054c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 391
    .local v0, "selectAllContainer":Landroid/widget/RelativeLayout;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 393
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->removeSelectSpinner()V

    .line 394
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->removeDeleteMode()V

    .line 395
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->cancelDeleteUI()V

    .line 396
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->createCustomOptionMenu()V

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->customizeActionBar()V

    .line 399
    return-void
.end method

.method private setDeleteTagScreenUI(ZLjava/util/HashSet;)V
    .locals 8
    .param p1, "isAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "tagSel":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 780
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollPosition:I

    .line 781
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 782
    .local v7, "v":Landroid/view/View;
    if-eqz v7, :cond_0

    .line 783
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollY:I

    .line 786
    :goto_0
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagNames:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mIconResourceIds:[Ljava/lang/Integer;

    move-object v1, p0

    move-object v2, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Landroid/app/Activity;[Ljava/lang/String;[Ljava/lang/Integer;ZLjava/util/HashSet;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    .line 787
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 788
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->notifyDataSetChanged()V

    .line 789
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->notifyDataSetChanged()V

    .line 790
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollPosition:I

    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollY:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 791
    return-void

    .line 785
    :cond_0
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mScrollY:I

    goto :goto_0
.end method

.method private showDeletePopup(Z)V
    .locals 7
    .param p1, "isMultipleItems"    # Z

    .prologue
    const v6, 0x7f090035

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 402
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mIsItemsSelected:Z

    .line 404
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 406
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mIsItemsSelected:Z

    if-eqz v2, :cond_1

    .line 407
    const v2, 0x7f09011d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 408
    .local v1, "delText":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 413
    .end local v1    # "delText":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 414
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 415
    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 416
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "log_delete_popup"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 418
    return-void

    .line 410
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09011c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0
.end method

.method private toggleActionButtonsVisibilityforSelectMode(Z)V
    .locals 2
    .param p1, "visibility"    # Z

    .prologue
    .line 452
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 453
    return-void
.end method

.method private toggleSelectAllCheckBox(Z)V
    .locals 2
    .param p1, "areAllItemsChecked"    # Z

    .prologue
    const/4 v1, 0x1

    .line 423
    if-eqz p1, :cond_0

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->setCheckAll(Z)V

    .line 430
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->getSelectedLogCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateSelectedCount(I)V

    .line 432
    return-void

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method private updateCheck(ZLjava/lang/String;)V
    .locals 5
    .param p1, "isChecked"    # Z
    .param p2, "tagName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 706
    if-eqz p1, :cond_0

    .line 707
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 708
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateSelectedCount(I)V

    .line 718
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagCount:I

    if-ne v0, v1, :cond_2

    .line 719
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 726
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 727
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->enableActionBarButtons(Z)V

    .line 731
    :goto_2
    return-void

    .line 710
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 711
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 712
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 713
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectUnselectAllListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 715
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 716
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateSelectedCount(I)V

    goto :goto_0

    .line 721
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 722
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 723
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectUnselectAllListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_1

    .line 730
    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->enableActionBarButtons(Z)V

    goto :goto_2
.end method

.method private updateSelectSpinner(I)V
    .locals 7
    .param p1, "selectedCounts"    # I

    .prologue
    const v6, 0x7f090073

    const v5, 0x7f090071

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 862
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-nez v0, :cond_0

    .line 863
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->addSelectSpinner(I)V

    .line 865
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 866
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    .line 868
    :cond_1
    if-ltz p1, :cond_6

    .line 869
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setCustomViewVisibility(Z)V

    .line 870
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 871
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    const v1, 0x7f090074

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 872
    if-nez p1, :cond_4

    .line 873
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 881
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 882
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_3

    .line 883
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 889
    :cond_3
    :goto_1
    return-void

    .line 875
    :cond_4
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagCount:I

    if-ne p1, v0, :cond_5

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 877
    :cond_5
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagCount:I

    if-ge p1, v0, :cond_2

    .line 878
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 879
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 885
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setCustomViewVisibility(Z)V

    .line 886
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisible(ZI)V

    .line 887
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_1
.end method

.method private updateSelectedCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 435
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateSelectSpinner(I)V

    .line 436
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 260
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090c0a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 264
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_0
.end method

.method public deleteTag()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 609
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    if-eqz v7, :cond_a

    .line 610
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 611
    .local v3, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 612
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v7, Lcom/sec/android/app/shealth/heartrate/common/HeartrateConstants;->IS_CUSTOM_MODIFIED:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 615
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v7}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 616
    .local v5, "tag":Ljava/lang/String;
    const/4 v1, 0x0

    .line 617
    .local v1, "firstRecentTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    const/4 v4, 0x0

    .line 618
    .local v4, "secondRecentTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    const/4 v6, 0x0

    .line 620
    .local v6, "thirdRecentTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    const-string v7, "first_r_tag"

    invoke-interface {v3, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    if-lez v7, :cond_0

    .line 621
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    const-string v8, "first_r_tag"

    invoke-interface {v3, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTagByIndex(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v1

    .line 622
    :cond_0
    const-string/jumbo v7, "second_r_tag"

    invoke-interface {v3, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    if-lez v7, :cond_1

    .line 623
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    const-string/jumbo v8, "second_r_tag"

    invoke-interface {v3, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTagByIndex(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v4

    .line 624
    :cond_1
    const-string/jumbo v7, "third_r_tag"

    invoke-interface {v3, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    if-lez v7, :cond_2

    .line 625
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    const-string/jumbo v8, "third_r_tag"

    invoke-interface {v3, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTagByIndex(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v6

    .line 628
    :cond_2
    if-eqz v6, :cond_4

    iget-object v7, v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v8, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_USER_ADDED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v7, v8, :cond_4

    iget-object v7, v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 629
    const-string/jumbo v7, "third_r_tag"

    invoke-interface {v0, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 658
    :cond_3
    :goto_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 659
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->deleteCustomTag(Ljava/lang/String;)V

    goto :goto_0

    .line 631
    :cond_4
    if-eqz v4, :cond_6

    iget-object v7, v4, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v8, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_USER_ADDED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v7, v8, :cond_6

    iget-object v7, v4, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 632
    const-string/jumbo v7, "third_r_tag"

    invoke-interface {v3, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    if-lez v7, :cond_5

    .line 633
    const-string/jumbo v7, "second_r_tag"

    const-string/jumbo v8, "third_r_tag"

    invoke-interface {v3, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 637
    :goto_2
    const-string/jumbo v7, "third_r_tag"

    invoke-interface {v0, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 635
    :cond_5
    const-string/jumbo v7, "second_r_tag"

    invoke-interface {v0, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    .line 640
    :cond_6
    if-eqz v1, :cond_3

    iget-object v7, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v8, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_USER_ADDED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v7, v8, :cond_3

    iget-object v7, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 641
    const-string/jumbo v7, "second_r_tag"

    invoke-interface {v3, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    if-lez v7, :cond_8

    .line 643
    const-string v7, "first_r_tag"

    const-string/jumbo v8, "second_r_tag"

    invoke-interface {v3, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 644
    const-string/jumbo v7, "third_r_tag"

    invoke-interface {v3, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    if-lez v7, :cond_7

    .line 645
    const-string/jumbo v7, "second_r_tag"

    const-string/jumbo v8, "third_r_tag"

    invoke-interface {v3, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 656
    :goto_3
    const-string/jumbo v7, "third_r_tag"

    invoke-interface {v0, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_1

    .line 647
    :cond_7
    const-string/jumbo v7, "second_r_tag"

    invoke-interface {v0, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_3

    .line 652
    :cond_8
    const-string/jumbo v7, "second_r_tag"

    invoke-interface {v0, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 653
    const-string v7, "first_r_tag"

    invoke-interface {v0, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_3

    .line 662
    .end local v1    # "firstRecentTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .end local v4    # "secondRecentTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .end local v5    # "tag":Ljava/lang/String;
    .end local v6    # "thirdRecentTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->initializeTagList()V

    .line 664
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    :cond_a
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 506
    packed-switch p1, :pswitch_data_0

    .line 514
    :cond_0
    :goto_0
    return-void

    .line 508
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 506
    nop

    :pswitch_data_0
    .packed-switch 0x45c
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 805
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isItemClicked:Z

    .line 806
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->isMenuDeleteMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 808
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->resumeEdit()V

    .line 813
    :goto_0
    return-void

    .line 811
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 129
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 130
    const v1, 0x7f030141

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->setContentView(I)V

    .line 132
    const v1, 0x7f08054e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "log_delete_popup"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 136
    .local v0, "dialogFragment":Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 139
    :cond_0
    const v1, 0x7f08054d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectUnselectAllListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 142
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 143
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    .line 145
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->initializeTagList()V

    .line 146
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->createCustomOptionMenu()V

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$2;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 219
    const v1, 0x7f08054f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mNoDataLayout:Landroid/widget/LinearLayout;

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 222
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 928
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mMenu:Landroid/view/Menu;

    .line 929
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    if-eqz v0, :cond_0

    .line 931
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->customizeActionBar()V

    .line 934
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 245
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isItemClicked:Z

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 254
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 255
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 960
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 968
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 962
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->setUpMenuDeleteMode(Ljava/util/HashSet;)V

    goto :goto_0

    .line 960
    :pswitch_data_0
    .packed-switch 0x7f080c8b
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 226
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 227
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 955
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 979
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 980
    const-string/jumbo v1, "selected"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 981
    .local v0, "array":[Ljava/lang/String;
    new-instance v1, Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    .line 982
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 231
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagNames:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagNames:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mNoDataLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 240
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mNoDataLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 973
    const-string/jumbo v1, "selected"

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 974
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 975
    return-void
.end method

.method protected refreshAdapter()V
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->notifyDataSetChanged()V

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mNoDataLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 379
    return-void
.end method

.method protected setUpMenuDeleteMode(Ljava/util/HashSet;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "tag":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 321
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isDeleteMode:Z

    .line 322
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isDoneMode:Z

    .line 323
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    .line 324
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->setMenuDeleteMode(Z)V

    .line 325
    const v5, 0x7f08054c

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 326
    .local v2, "selectAllContainer":Landroid/widget/RelativeLayout;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->onSelectAllRelClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 328
    invoke-direct {p0, v4, p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->setDeleteTagScreenUI(ZLjava/util/HashSet;)V

    .line 330
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 333
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$6;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V

    .line 349
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f0207b0

    const v6, 0x7f090035

    invoke-direct {v1, v5, v4, v6, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    .line 350
    .local v1, "buttonDone":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    new-array v6, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v6, v4

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    const v6, 0x7f020022

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarBackground(I)V

    .line 357
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-eqz v5, :cond_0

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    .line 367
    :goto_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result v5

    if-lez v5, :cond_2

    :goto_1
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->enableActionBarButtons(Z)V

    .line 368
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateSelectedCount(I)V

    .line 369
    return-void

    .line 360
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 361
    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->addSelectSpinner(I)V

    .line 365
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    goto :goto_0

    .line 363
    :cond_1
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->addSelectSpinner(I)V

    goto :goto_2

    :cond_2
    move v3, v4

    .line 367
    goto :goto_1
.end method

.method protected setUpMenuEditMode(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "tagName"    # Ljava/lang/String;
    .param p2, "tagIconId"    # Ljava/lang/String;

    .prologue
    .line 794
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update a tag(name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", iconId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 797
    .local v0, "mEditIntent":Landroid/content/Intent;
    const-string v1, "TAG_TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 798
    const-string v1, "TAG_INDEX"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 799
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->startActivity(Landroid/content/Intent;)V

    .line 800
    return-void
.end method

.method protected setViewForDelete(Z)V
    .locals 5
    .param p1, "isChecked"    # Z

    .prologue
    .line 734
    const/4 v4, 0x0

    invoke-direct {p0, p1, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->setDeleteTagScreenUI(ZLjava/util/HashSet;)V

    .line 736
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    if-eqz v4, :cond_4

    .line 737
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagNames:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 738
    .local v3, "tag":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 739
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 737
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 741
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 742
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    goto :goto_1

    .line 746
    .end local v3    # "tag":Ljava/lang/String;
    :cond_2
    if-eqz p1, :cond_3

    .line 747
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateSelectedCount(I)V

    .line 752
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    :cond_3
    :goto_2
    return-void

    .line 750
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->cancelDeleteUI()V

    goto :goto_2
.end method
