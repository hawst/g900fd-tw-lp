.class public Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;
.super Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;
.source "SleepMonitorLogDetailView.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private Seletion_ID:Ljava/lang/String;

.field private Sleep_Rating:Landroid/widget/LinearLayout;

.field private Sleep_Rating_Divider:Landroid/view/View;

.field private TV_awakeTime:Landroid/widget/TextView;

.field private TV_sleepTime:Landroid/widget/TextView;

.field private TV_top_sleepQualityTopText:Landroid/widget/TextView;

.field private TV_top_syncedDevice:Landroid/widget/TextView;

.field private TV_totalSleep:Landroid/widget/TextView;

.field private item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

.field private mComment:Ljava/lang/String;

.field private ratingBar:Landroid/widget/RatingBar;

.field private syncTextlayout:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 182
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->customizeActionBar()V

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090d56

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 184
    return-void
.end method

.method protected getContentView()Landroid/view/View;
    .locals 28

    .prologue
    .line 79
    const-string v3, "Edit"

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mMode:Ljava/lang/String;

    .line 80
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/LayoutInflater;

    .line 81
    .local v21, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030215

    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/LinearLayout;

    .line 82
    .local v22, "layout":Landroid/widget/LinearLayout;
    const v3, 0x7f080957

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_top_syncedDevice:Landroid/widget/TextView;

    .line 83
    const v3, 0x7f080956

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->syncTextlayout:Landroid/widget/LinearLayout;

    .line 84
    const v3, 0x7f08095d

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_top_sleepQualityTopText:Landroid/widget/TextView;

    .line 85
    const v3, 0x7f080960

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_sleepTime:Landroid/widget/TextView;

    .line 86
    const v3, 0x7f080963

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_awakeTime:Landroid/widget/TextView;

    .line 87
    const v3, 0x7f08095a

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_totalSleep:Landroid/widget/TextView;

    .line 88
    const v3, 0x7f080964

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->Sleep_Rating:Landroid/widget/LinearLayout;

    .line 89
    const v3, 0x7f080966

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->Sleep_Rating_Divider:Landroid/view/View;

    .line 90
    const v3, 0x7f080965

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RatingBar;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->ratingBar:Landroid/widget/RatingBar;

    .line 91
    const v3, 0x7f080967

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 93
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->Seletion_ID:Ljava/lang/String;

    .line 94
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->Seletion_ID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 95
    .local v6, "where":Ljava/lang/String;
    const/16 v3, 0xa

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "awake_count"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "user_device__id"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "movement"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "noise"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string v4, "bed_time"

    aput-object v4, v5, v3

    const/4 v3, 0x5

    const-string/jumbo v4, "rise_time"

    aput-object v4, v5, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "quality"

    aput-object v4, v5, v3

    const/4 v3, 0x7

    const-string v4, "efficiency"

    aput-object v4, v5, v3

    const/16 v3, 0x8

    const-string v4, "comment"

    aput-object v4, v5, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "update_time"

    aput-object v4, v5, v3

    .line 107
    .local v5, "Coulm":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 109
    .local v23, "mCursor":Landroid/database/Cursor;
    if-eqz v23, :cond_1

    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_1

    .line 110
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 111
    const/16 v23, 0x0

    .line 112
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->finish()V

    .line 177
    :cond_0
    :goto_0
    return-object v22

    .line 116
    :cond_1
    if-eqz v23, :cond_0

    .line 120
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToFirst()Z

    .line 121
    new-instance v3, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    move-object/from16 v0, v23

    invoke-direct {v3, v0}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;-><init>(Landroid/database/Cursor;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    .line 123
    const-string v10, "_id = ?"

    .line 124
    .local v10, "selectionClause":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v11, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getDevice_id()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v11, v3

    .line 125
    .local v11, "mSelectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getDevice_id()Ljava/lang/String;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 126
    .local v19, "accessoryID":[Ljava/lang/String;
    const/16 v20, 0x0

    .line 128
    .local v20, "cursor2":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, v19

    array-length v3, v0

    const/4 v4, 0x2

    if-ne v3, v4, :cond_4

    .line 129
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_top_syncedDevice:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->syncTextlayout:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 133
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 134
    if-eqz v20, :cond_2

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 136
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    .line 137
    const-string v3, "custom_name"

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 138
    .local v18, "accessory":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_top_syncedDevice:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f090a8c

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v18, v7, v8

    invoke-static {v4, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    .end local v18    # "accessory":Ljava/lang/String;
    :cond_2
    :goto_1
    if-eqz v20, :cond_3

    .line 148
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 150
    :cond_3
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 152
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 153
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_top_sleepQualityTopText:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getSleep_qualty()F

    move-result v12

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getRise_time()J

    move-result-wide v13

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getBed_time()J

    move-result-wide v15

    move-object/from16 v17, p0

    invoke-static/range {v12 .. v17}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getMotionlessSleepTime(FJJLandroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, " (%d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getSleep_qualty()F

    move-result v13

    float-to-int v13, v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v9, v12

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "%)"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_sleepTime:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getBed_time()J

    move-result-wide v12

    invoke-direct {v8, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getBed_time()J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_awakeTime:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getRise_time()J

    move-result-wide v12

    invoke-direct {v8, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getRise_time()J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v27

    .line 161
    .local v27, "time":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getTotalSleepTime()J

    move-result-wide v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 163
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getTotalSleepTime()J

    move-result-wide v3

    const-wide/32 v7, 0xea60

    div-long v24, v3, v7

    .line 164
    .local v24, "minute":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_totalSleep:Landroid/widget/TextView;

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeInRequiredFormat(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getQuality()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v26, v3, v4

    .line 166
    .local v26, "rating":F
    const/4 v3, 0x0

    aget-object v3, v19, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x272e

    if-lt v3, v4, :cond_7

    const/4 v3, 0x0

    cmpl-float v3, v26, v3

    if-eqz v3, :cond_7

    .line 167
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->Sleep_Rating:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 168
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->Sleep_Rating_Divider:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 169
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->ratingBar:Landroid/widget/RatingBar;

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Landroid/widget/RatingBar;->setRating(F)V

    .line 176
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getComment()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_8

    const-string v3, ""

    :goto_4
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mComment:Ljava/lang/String;

    goto/16 :goto_0

    .line 142
    .end local v24    # "minute":J
    .end local v26    # "rating":F
    .end local v27    # "time":Ljava/util/Calendar;
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_top_syncedDevice:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->syncTextlayout:Landroid/widget/LinearLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 147
    :catchall_0
    move-exception v3

    if-eqz v20, :cond_5

    .line 148
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 150
    :cond_5
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    throw v3

    .line 155
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TV_top_sleepQualityTopText:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getSleep_qualty()F

    move-result v12

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getRise_time()J

    move-result-wide v13

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getBed_time()J

    move-result-wide v15

    move-object/from16 v17, p0

    invoke-static/range {v12 .. v17}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getMotionlessSleepTime(FJJLandroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " (%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getSleep_qualty()F

    move-result v12

    float-to-int v12, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "%)"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 172
    .restart local v24    # "minute":J
    .restart local v26    # "rating":F
    .restart local v27    # "time":Ljava/util/Calendar;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->Sleep_Rating:Landroid/widget/LinearLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 173
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->Sleep_Rating_Divider:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 176
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getComment()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4
.end method

.method protected isInputChanged()Z
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mComment:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v1, 0x7f0803e8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mComment:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mComment:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setSelection(I)V

    .line 53
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    new-instance v2, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView$1;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 68
    .local v0, "window":Landroid/view/Window;
    if-eqz v0, :cond_0

    .line 70
    const/16 v1, 0x400

    const/16 v2, 0x500

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 75
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 199
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onResume()V

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getContentView()Landroid/view/View;

    move-result-object v0

    .line 201
    .local v0, "content":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 203
    return-void
.end method

.method protected onSaveButtonSelect(Z)V
    .locals 5
    .param p1, "isAdd"    # Z

    .prologue
    .line 188
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->setComment(Ljava/lang/String;)V

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->Seletion_ID:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->item:Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->sleepItemCommitChange(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;)I

    move-result v0

    .line 190
    .local v0, "_id":I
    sget-object v2, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onSaveButtonSelect = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\t\t\t_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\t mMemoView.getText().toString()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const v2, 0x7f090835

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 192
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 193
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 194
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailView;->startActivity(Landroid/content/Intent;)V

    .line 195
    return-void
.end method
