.class Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;
.super Landroid/os/AsyncTask;
.source "SuggestedMissionsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdateListAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private coachMsg:Ljava/lang/String;

.field private goalInfo:Lcom/cigna/coach/apiobjects/GoalInfo;

.field private newListData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;)V
    .locals 1

    .prologue
    .line 316
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 318
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->coachMsg:Ljava/lang/String;

    .line 320
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->newListData:Ljava/util/List;

    return-void
.end method

.method private getCoachMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 379
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->getMissionCoachMessage(I)Ljava/lang/String;

    move-result-object v0

    .line 380
    .local v0, "coachMsg":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 381
    const-string v0, ""

    .line 383
    .end local v0    # "coachMsg":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private getListData()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 374
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->createMissionListData(Landroid/content/Context;II)Ljava/util/List;

    move-result-object v0

    .line 375
    .local v0, "listData":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;>;"
    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 316
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->getListData()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->newListData:Ljava/util/List;

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->newListData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 329
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->getCoachMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->coachMsg:Ljava/lang/String;

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->getGoalInfoForMission(I)Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->goalInfo:Lcom/cigna/coach/apiobjects/GoalInfo;

    .line 333
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 316
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 338
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 342
    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UpdateListAsyncTask list update [START]"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSuggestedView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->setPreventScroll(Z)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->newListData:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->clearAllData()V

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mListData:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->newListData:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mListData:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->setItemsList(Ljava/util/List;)V

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->notifyDataSetChanged()V

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->coachMsg:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->goalInfo:Lcom/cigna/coach/apiobjects/GoalInfo;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->updateHeaderMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/GoalInfo;)V

    .line 362
    :cond_1
    :goto_0
    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UpdateListAsyncTask list update [END]"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 371
    :cond_2
    return-void

    .line 356
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mCommentView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mNoSuggestTextView:Landroid/widget/TextView;

    const v1, 0x7f090333

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mNoSuggestDescView:Landroid/widget/TextView;

    const v1, 0x7f090332

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
