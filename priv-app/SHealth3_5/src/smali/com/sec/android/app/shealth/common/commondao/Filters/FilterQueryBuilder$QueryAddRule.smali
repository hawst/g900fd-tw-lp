.class public final enum Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;
.super Ljava/lang/Enum;
.source "FilterQueryBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QueryAddRule"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

.field public static final enum AND:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

.field public static final enum OR:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;


# instance fields
.field private operation:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    const-string v1, "OR"

    const-string v2, " OR "

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->OR:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    const-string v1, "AND"

    const-string v2, " AND "

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->AND:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    sget-object v1, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->OR:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->AND:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->$VALUES:[Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->operation:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->$VALUES:[Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->operation:Ljava/lang/String;

    return-object v0
.end method
