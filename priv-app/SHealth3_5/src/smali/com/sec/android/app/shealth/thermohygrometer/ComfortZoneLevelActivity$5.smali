.class Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;
.super Ljava/lang/Object;
.source "ComfortZoneLevelActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->initTextWachers()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 330
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->removeSpace(Landroid/text/Editable;)V
    invoke-static {v3, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$400(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;Landroid/text/Editable;)V

    .line 331
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 334
    .local v1, "currentString":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_HUMIDIY:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$1000(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    .line 335
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 339
    .local v0, "currentHumidity":F
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 340
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 344
    .local v2, "secondHumidity":F
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setButtonState()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$700(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    .line 346
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_HUMIDIY:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$1000(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v3

    cmpg-float v3, v3, v0

    if-gtz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MAXIMAL_HUMIDITY:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$1100(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v3

    cmpg-float v3, v0, v3

    if-gtz v3, :cond_0

    .line 347
    cmpg-float v3, v0, v2

    if-gez v3, :cond_3

    .line 348
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setHumidityRangeMinimal(F)V
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$1200(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V

    .line 349
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setHumidityRangeMaxial(F)V
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$1300(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V

    .line 355
    :cond_0
    :goto_2
    return-void

    .line 337
    .end local v0    # "currentHumidity":F
    .end local v2    # "secondHumidity":F
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_HUMIDIY:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$1000(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v0

    .restart local v0    # "currentHumidity":F
    goto :goto_0

    .line 342
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_HUMIDIY:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$1000(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v2

    .restart local v2    # "secondHumidity":F
    goto :goto_1

    .line 350
    :cond_3
    cmpl-float v3, v0, v2

    if-lez v3, :cond_0

    .line 351
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setHumidityRangeMaxial(F)V
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$1300(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V

    .line 352
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setHumidityRangeMinimal(F)V
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$1200(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V

    goto :goto_2
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 326
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 321
    return-void
.end method
