.class public Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;
.super Ljava/lang/Object;
.source "appSoundplayer.java"


# instance fields
.field aManager:Landroid/media/AudioManager;

.field mContext:Landroid/content/Context;

.field pool:Landroid/media/SoundPool;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->mContext:Landroid/content/Context;

    .line 18
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->initPlayer()V

    .line 19
    return-void
.end method


# virtual methods
.method public initPlayer()V
    .locals 4

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->aManager:Landroid/media/AudioManager;

    .line 23
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x1

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->pool:Landroid/media/SoundPool;

    .line 24
    return-void
.end method

.method public initPlayer(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "soundMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->initPlayer()V

    .line 28
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->loadSound(Ljava/util/Map;)V

    .line 29
    return-void
.end method

.method public loadSound(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "soundMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    .line 32
    .local v2, "soundValues":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 33
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 34
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 35
    .local v1, "soundValue":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->pool:Landroid/media/SoundPool;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    goto :goto_0

    .line 37
    .end local v1    # "soundValue":Ljava/lang/Integer;
    :cond_0
    return-void
.end method

.method public playSoundIndex(I)V
    .locals 10
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x3

    const/4 v4, 0x1

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->aManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->aManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-nez v0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->pool:Landroid/media/SoundPool;

    if-eqz v0, :cond_2

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/media/AudioManager;

    .line 46
    .local v7, "mgr":Landroid/media/AudioManager;
    invoke-virtual {v7, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    int-to-float v8, v0

    .line 47
    .local v8, "streamVolumeCurrent":F
    invoke-virtual {v7, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    int-to-float v9, v0

    .line 48
    .local v9, "streamVolumeMax":F
    div-float v2, v8, v9

    .line 50
    .local v2, "volume":F
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->pool:Landroid/media/SoundPool;

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    move v1, p1

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto :goto_0

    .line 53
    .end local v2    # "volume":F
    .end local v7    # "mgr":Landroid/media/AudioManager;
    .end local v8    # "streamVolumeCurrent":F
    .end local v9    # "streamVolumeMax":F
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->initPlayer()V

    goto :goto_0
.end method
