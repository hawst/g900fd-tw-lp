.class public Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;
.super Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;
.source "ExerciseProChartFragment.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation


# static fields
.field private static MARKING_COUNT:I

.field private static MARKING_INTERVAL:I

.field private static TIME_DEPTH_LEVEL:I


# instance fields
.field private final TAG:Ljava/lang/String;

.field private bpmUnit:Ljava/lang/String;

.field density:F

.field private elevationUnit:Ljava/lang/String;

.field public final elvChart:Z

.field exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

.field private handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

.field public final hrmChart:Z

.field kmUnit:Z

.field private mContext:Landroid/app/Activity;

.field private mGraphBackground:Landroid/graphics/Bitmap;

.field private mGraphHandler:Landroid/graphics/Bitmap;

.field private mGraphLine:Landroid/graphics/Bitmap;

.field private mHandleOverBitmapBlack:Landroid/graphics/Bitmap;

.field private mHandleOverBitmapRed:Landroid/graphics/Bitmap;

.field private mHandleOverBitmapYellowGreen:Landroid/graphics/Bitmap;

.field private mLegendView:Landroid/view/View;

.field private mNormalBitmapBlack:Landroid/graphics/Bitmap;

.field private mNormalBitmapRed:Landroid/graphics/Bitmap;

.field private mNormalBitmapYellowGreen:Landroid/graphics/Bitmap;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mRootView:Landroid/view/View;

.field private mRowID:J

.field private mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private oneDForm:Ljava/text/DecimalFormat;

.field private oneDForm2:Ljava/text/DecimalFormat;

.field private oneDForm3:Ljava/text/DecimalFormat;

.field private paceUnit:Ljava/lang/String;

.field scaledDensity:F

.field public final spdChart:Z

.field private speedUnit:Ljava/lang/String;

.field private timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 68
    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->TIME_DEPTH_LEVEL:I

    .line 69
    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->MARKING_INTERVAL:I

    .line 71
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->MARKING_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;-><init>()V

    .line 60
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 61
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 62
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mRowID:J

    .line 77
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    .line 78
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->spdChart:Z

    .line 79
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->hrmChart:Z

    .line 80
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->elvChart:Z

    .line 94
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm:Ljava/text/DecimalFormat;

    .line 95
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm2:Ljava/text/DecimalFormat;

    .line 97
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm3:Ljava/text/DecimalFormat;

    .line 111
    const-string v0, "ExerciseProChartFragment"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->TAG:Ljava/lang/String;

    .line 509
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    .line 115
    return-void
.end method

.method public constructor <init>(J)V
    .locals 4
    .param p1, "rowID"    # J

    .prologue
    const/4 v2, 0x0

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;-><init>()V

    .line 60
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 61
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 62
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mRowID:J

    .line 77
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    .line 78
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->spdChart:Z

    .line 79
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->hrmChart:Z

    .line 80
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->elvChart:Z

    .line 94
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm:Ljava/text/DecimalFormat;

    .line 95
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm2:Ljava/text/DecimalFormat;

    .line 97
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm3:Ljava/text/DecimalFormat;

    .line 111
    const-string v0, "ExerciseProChartFragment"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->TAG:Ljava/lang/String;

    .line 509
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    .line 123
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mRowID:J

    .line 124
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 4
    .param p1, "isPaceMode"    # Z

    .prologue
    const/4 v2, 0x0

    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;-><init>(Z)V

    .line 60
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 61
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 62
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mRowID:J

    .line 77
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    .line 78
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->spdChart:Z

    .line 79
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->hrmChart:Z

    .line 80
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->elvChart:Z

    .line 94
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm:Ljava/text/DecimalFormat;

    .line 95
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm2:Ljava/text/DecimalFormat;

    .line 97
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm3:Ljava/text/DecimalFormat;

    .line 111
    const-string v0, "ExerciseProChartFragment"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->TAG:Ljava/lang/String;

    .line 509
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    .line 119
    return-void
.end method

.method public constructor <init>(ZJ)V
    .locals 4
    .param p1, "isPaceMode"    # Z
    .param p2, "rowID"    # J

    .prologue
    const/4 v2, 0x0

    .line 127
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;-><init>(Z)V

    .line 60
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 61
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 62
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mRowID:J

    .line 77
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    .line 78
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->spdChart:Z

    .line 79
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->hrmChart:Z

    .line 80
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->elvChart:Z

    .line 94
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm:Ljava/text/DecimalFormat;

    .line 95
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm2:Ljava/text/DecimalFormat;

    .line 97
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm3:Ljava/text/DecimalFormat;

    .line 111
    const-string v0, "ExerciseProChartFragment"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->TAG:Ljava/lang/String;

    .line 509
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    .line 128
    iput-wide p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mRowID:J

    .line 129
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm3:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getPaceBubbleAreaUnit()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mContext:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm2:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;D)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;
    .param p1, "x1"    # D

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getPaceValue(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getSpeedBubbleAreaUnit()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getBPMBubbleAreaUnit()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getBPMBubbleAreaUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 922
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->bpmUnit:Ljava/lang/String;

    .line 923
    .local v0, "bpmUnit":Ljava/lang/String;
    return-object v0
.end method

.method private getMilesFromMeters(J)Ljava/lang/String;
    .locals 6
    .param p1, "distance"    # J

    .prologue
    .line 974
    long-to-double v2, p1

    const-wide v4, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double v0, v2, v4

    .line 975
    .local v0, "mi":D
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm2:Ljava/text/DecimalFormat;

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getPaceBubbleAreaUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 927
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->paceUnit:Ljava/lang/String;

    .line 928
    .local v0, "paceUnit":Ljava/lang/String;
    return-object v0
.end method

.method private getPaceValue(D)Ljava/lang/String;
    .locals 14
    .param p1, "time"    # D

    .prologue
    .line 960
    const-string v5, "0\'00\""

    .line 961
    .local v5, "value":Ljava/lang/String;
    const-wide/16 v7, 0x0

    cmpg-double v7, p1, v7

    if-gez v7, :cond_0

    move-object v6, v5

    .line 970
    .end local v5    # "value":Ljava/lang/String;
    .local v6, "value":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 964
    .end local v6    # "value":Ljava/lang/String;
    .restart local v5    # "value":Ljava/lang/String;
    :cond_0
    const-wide/16 v0, 0x3c

    .line 965
    .local v0, "HOUR":J
    long-to-double v7, v0

    div-double v7, p1, v7

    double-to-int v2, v7

    .line 966
    .local v2, "hour":I
    long-to-double v7, v0

    rem-double/2addr p1, v7

    .line 967
    const-string v7, "%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    double-to-int v10, p1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 968
    .local v3, "minuteText":Ljava/lang/String;
    const-string v7, "%02d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    double-to-int v10, p1

    int-to-double v10, v10

    sub-double v10, p1, v10

    const-wide/high16 v12, 0x404e000000000000L    # 60.0

    mul-double/2addr v10, v12

    double-to-int v10, v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 969
    .local v4, "secondText":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    if-lez v2, :cond_1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ":"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :goto_1
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    .line 970
    .end local v5    # "value":Ljava/lang/String;
    .restart local v6    # "value":Ljava/lang/String;
    goto :goto_0

    .line 969
    .end local v6    # "value":Ljava/lang/String;
    .restart local v5    # "value":Ljava/lang/String;
    :cond_1
    const-string v7, ""

    goto :goto_1
.end method

.method private getSpeedBubbleAreaUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 917
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->speedUnit:Ljava/lang/String;

    .line 918
    .local v0, "speedUnit":Ljava/lang/String;
    return-object v0
.end method

.method private initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 25
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 721
    new-instance v16, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 722
    .local v16, "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v20, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->scaledDensity:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 723
    const/16 v20, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 724
    const/16 v20, 0xff

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 725
    const/16 v20, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 727
    new-instance v17, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 728
    .local v17, "yLeftTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v20, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->scaledDensity:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 729
    const-string/jumbo v20, "sans-serif"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 730
    const/16 v20, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 731
    const/16 v20, 0xff

    const/16 v21, 0x8d

    const/16 v22, 0xd9

    const/16 v23, 0x31

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 732
    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 734
    new-instance v18, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 735
    .local v18, "yRightTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v20, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->scaledDensity:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 736
    const-string/jumbo v20, "sans-serif"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 737
    const/16 v20, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 738
    const/16 v20, 0xff

    const/16 v21, 0xe5

    const/16 v22, 0x47

    const/16 v23, 0x47

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 739
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 741
    new-instance v19, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 742
    .local v19, "yRightTextStyle1":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v20, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->scaledDensity:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 743
    const-string/jumbo v20, "sans-serif"

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 744
    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 745
    const/16 v20, 0xff

    const/16 v21, 0xa4

    const/16 v22, 0xa4

    const/16 v23, 0xa4

    invoke-virtual/range {v19 .. v23}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 746
    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 748
    new-instance v10, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v10}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 749
    .local v10, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v20, 0x41900000    # 18.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->scaledDensity:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 750
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 751
    const/16 v20, 0xff

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v10, v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 752
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 754
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/high16 v23, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->density:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 759
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0203b2

    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mGraphBackground:Landroid/graphics/Bitmap;

    .line 761
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mGraphBackground:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 762
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0700d8

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setChartBackgroundColor(I)V

    .line 764
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorDateFormat(Ljava/lang/String;)V

    .line 767
    const/high16 v20, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->density:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisWidth(FI)V

    .line 768
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 769
    const v20, -0xcbb1ec

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisColor(II)V

    .line 770
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 771
    const/high16 v20, 0x41f00000    # 30.0f

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextSpace(FI)V

    .line 773
    const/16 v20, 0x0

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextVisible(ZI)V

    .line 774
    const/16 v20, 0x0

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 776
    const-string v20, "HH:mm"

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 779
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorVisible(Z)V

    .line 782
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 783
    const/16 v20, 0x1

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 784
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 785
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisDirection(II)V

    .line 786
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 787
    const/16 v20, 0x1

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 788
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v5, v0, [I

    const/16 v20, 0x0

    const/16 v21, 0x0

    aput v21, v5, v20

    .line 791
    .local v5, "associatedSeriesID":[I
    const/16 v20, 0x1

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v5, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisAssociatedSeriesID(I[II)V

    .line 793
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getPaceMode()Z

    move-result v20

    if-eqz v20, :cond_4

    .line 794
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    move/from16 v20, v0

    if-eqz v20, :cond_3

    .line 795
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const v21, 0x7f09080e

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 804
    :goto_0
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 807
    const/4 v15, 0x0

    .line 808
    .local v15, "validHrData":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_1

    .line 810
    const/4 v11, 0x0

    .local v11, "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v20

    move/from16 v0, v20

    if-ge v11, v0, :cond_0

    .line 811
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getY(I)D

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmpl-double v20, v20, v22

    if-eqz v20, :cond_0

    .line 812
    const/4 v15, 0x1

    .line 815
    :cond_0
    if-eqz v15, :cond_1

    .line 816
    const/16 v20, 0x0

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 817
    const/16 v20, 0x1

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 818
    const/16 v20, 0x0

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 819
    const/16 v20, 0x1

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisDirection(II)V

    .line 820
    const/16 v20, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 821
    const/16 v20, 0x0

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 822
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v6, v0, [I

    const/16 v20, 0x0

    const/16 v21, 0x1

    aput v21, v6, v20

    .line 825
    .local v6, "associatedSeriesID1":[I
    const/16 v20, 0x1

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisAssociatedSeriesID(I[II)V

    .line 826
    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v12, v0, [F

    fill-array-data v12, :array_0

    .line 830
    .local v12, "labelOffset1":[F
    const/16 v20, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v12, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelOffsets([FI)V

    .line 831
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const v21, 0x7f0900d2

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 832
    const/16 v20, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 833
    const/high16 v20, 0x41f00000    # 30.0f

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleOffset(FI)V

    .line 839
    .end local v6    # "associatedSeriesID1":[I
    .end local v11    # "index":I
    .end local v12    # "labelOffset1":[F
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_2

    .line 840
    const/16 v20, 0x0

    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 841
    const/16 v20, 0x1

    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 842
    const/16 v20, 0x0

    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 843
    const/16 v20, 0x1

    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisDirection(II)V

    .line 844
    const/16 v20, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 845
    const/16 v20, 0x0

    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 846
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v7, v0, [I

    const/16 v20, 0x0

    const/16 v21, 0x2

    aput v21, v7, v20

    .line 849
    .local v7, "associatedSeriesID2":[I
    const/16 v20, 0x1

    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v7, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisAssociatedSeriesID(I[II)V

    .line 851
    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v12, v0, [F

    fill-array-data v12, :array_1

    .line 854
    .restart local v12    # "labelOffset1":[F
    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v13, v0, [F

    fill-array-data v13, :array_2

    .line 857
    .local v13, "labelOffset2":[F
    if-eqz v15, :cond_6

    .line 858
    const/16 v20, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v13, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelOffsets([FI)V

    .line 862
    :goto_1
    sget-object v20, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v21, "km"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_7

    const/4 v14, 0x1

    .line 863
    .local v14, "unitMile":Z
    :goto_2
    if-eqz v14, :cond_8

    .line 864
    const-string v20, "(ft)"

    const/16 v21, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 869
    :goto_3
    const/16 v20, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 870
    if-eqz v15, :cond_9

    .line 871
    const/high16 v20, 0x425c0000    # 55.0f

    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleOffset(FI)V

    .line 880
    .end local v7    # "associatedSeriesID2":[I
    .end local v12    # "labelOffset1":[F
    .end local v13    # "labelOffset2":[F
    .end local v14    # "unitMile":Z
    :cond_2
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0203c3

    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mGraphHandler:Landroid/graphics/Bitmap;

    .line 882
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mGraphHandler:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 883
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemStrokeWidth(F)V

    .line 884
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 885
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 886
    const/high16 v20, 0x426c0000    # 59.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->density:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    const/high16 v21, 0x40000000    # 2.0f

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 887
    const/high16 v20, 0x427c0000    # 63.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->density:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 888
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0a0057

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x3f000000    # 0.5f

    mul-float v20, v20, v21

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0a0058

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    add-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 890
    const/high16 v20, 0x41a00000    # 20.0f

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 891
    const-wide/16 v20, 0x7d0

    move-object/from16 v0, p1

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTimeOutDelay(J)V

    .line 893
    const-string v20, "HH:mm"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 896
    const/16 v20, 0x12c

    const/16 v21, 0x12c

    sget-object v22, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v20 .. v22}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mGraphLine:Landroid/graphics/Bitmap;

    .line 897
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0203c4

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 898
    .local v9, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v8, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mGraphLine:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v8, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 899
    .local v8, "canvas":Landroid/graphics/Canvas;
    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-virtual {v8}, Landroid/graphics/Canvas;->getWidth()I

    move-result v22

    invoke-virtual {v8}, Landroid/graphics/Canvas;->getHeight()I

    move-result v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 900
    invoke-virtual {v9, v8}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 901
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mGraphLine:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 903
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupEnable(Z)V

    .line 904
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setMarkingLineBase(I)V

    .line 905
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendVisible(Z)V

    .line 906
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->density:F

    move/from16 v20, v0

    const/high16 v21, 0x42080000    # 34.0f

    mul-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 907
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipEnable(Z)V

    .line 908
    return-void

    .line 797
    .end local v8    # "canvas":Landroid/graphics/Canvas;
    .end local v9    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v15    # "validHrData":Z
    :cond_3
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const v21, 0x7f090811

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 799
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    move/from16 v20, v0

    if-eqz v20, :cond_5

    .line 800
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const v21, 0x7f09080f

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 802
    :cond_5
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const v21, 0x7f090810

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 860
    .restart local v7    # "associatedSeriesID2":[I
    .restart local v12    # "labelOffset1":[F
    .restart local v13    # "labelOffset2":[F
    .restart local v15    # "validHrData":Z
    :cond_6
    const/16 v20, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v12, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelOffsets([FI)V

    goto/16 :goto_1

    .line 862
    :cond_7
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 866
    .restart local v14    # "unitMile":Z
    :cond_8
    const-string v20, "(m)"

    const/16 v21, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_3

    .line 873
    :cond_9
    const/high16 v20, 0x41f00000    # 30.0f

    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleOffset(FI)V

    goto/16 :goto_4

    .line 826
    nop

    :array_0
    .array-data 4
        -0x3de00000    # -40.0f
        0x0
        0x0
    .end array-data

    .line 851
    :array_1
    .array-data 4
        -0x3de00000    # -40.0f
        0x0
        0x0
    .end array-data

    .line 854
    :array_2
    .array-data 4
        0x41200000    # 10.0f
        0x42480000    # 50.0f
        0x42480000    # 50.0f
    .end array-data
.end method

.method private initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 12
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    .line 627
    new-instance v4, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;

    invoke-direct {v4}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;-><init>()V

    .line 628
    .local v4, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;
    new-instance v5, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;-><init>()V

    .line 629
    .local v5, "seriesStyle1":Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;
    new-instance v6, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;

    invoke-direct {v6}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;-><init>()V

    .line 631
    .local v6, "seriesStyle2":Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;
    const-string/jumbo v8, "speed"

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 632
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getPaceMode()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 633
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    if-eqz v8, :cond_1

    .line 634
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f09080e

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 643
    :goto_0
    const-string v8, "heart rate"

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 646
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    const/4 v9, 0x0

    iget-wide v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mRowID:J

    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getWorkoutStatics(Landroid/content/Context;ZJ)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;

    move-result-object v7

    .line 647
    .local v7, "stastics":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;
    iget-wide v0, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxAltitude:D

    .line 648
    .local v0, "mMaxElevation":D
    iget-wide v2, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->minAltitude:D

    .line 649
    .local v2, "mMinElevation":D
    const-wide v8, 0x3ff999999999999aL    # 1.6

    mul-double/2addr v8, v0

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    mul-double v0, v8, v10

    .line 650
    const-wide v8, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v8, v2

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    mul-double v2, v8, v10

    .line 657
    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    double-to-float v11, v0

    invoke-virtual {v6, v8, v9, v10, v11}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 659
    const-string v8, "elevation"

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 663
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0203c0

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mNormalBitmapYellowGreen:Landroid/graphics/Bitmap;

    .line 667
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0203c1

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mHandleOverBitmapYellowGreen:Landroid/graphics/Bitmap;

    .line 669
    const/4 v8, 0x1

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingVisible(Z)V

    .line 670
    const/4 v8, 0x2

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingVisibleOption(I)V

    .line 672
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mNormalBitmapYellowGreen:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 674
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mHandleOverBitmapYellowGreen:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 675
    const/high16 v8, 0x41700000    # 15.0f

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingSize(F)V

    .line 679
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0203be

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mNormalBitmapRed:Landroid/graphics/Bitmap;

    .line 683
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0203bf

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mHandleOverBitmapRed:Landroid/graphics/Bitmap;

    .line 685
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingVisible(Z)V

    .line 686
    const/4 v8, 0x2

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingVisibleOption(I)V

    .line 687
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mNormalBitmapRed:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 688
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mHandleOverBitmapRed:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 689
    const/high16 v8, 0x41700000    # 15.0f

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingSize(F)V

    .line 693
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0203b5

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mNormalBitmapBlack:Landroid/graphics/Bitmap;

    .line 697
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0203b6

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mHandleOverBitmapBlack:Landroid/graphics/Bitmap;

    .line 699
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingVisible(Z)V

    .line 700
    const/4 v8, 0x2

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingVisibleOption(I)V

    .line 701
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mNormalBitmapBlack:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 702
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mHandleOverBitmapBlack:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 703
    const/high16 v8, 0x41700000    # 15.0f

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingSize(F)V

    .line 705
    const v8, -0x7226b7

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setLineColor(I)V

    .line 706
    const/high16 v8, 0x41200000    # 10.0f

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setLineThickness(F)V

    .line 708
    const v8, -0x1a8e8f

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setLineColor(I)V

    .line 709
    const/high16 v8, 0x41200000    # 10.0f

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setLineThickness(F)V

    .line 711
    const v8, 0x77a4a4a4

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setAreaColor(I)V

    .line 712
    const v8, 0x77a4a4a4

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setLineColor(I)V

    .line 714
    invoke-virtual {p1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 715
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportHRM()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 716
    invoke-virtual {p1, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 717
    :cond_0
    invoke-virtual {p1, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 718
    return-void

    .line 636
    .end local v0    # "mMaxElevation":D
    .end local v2    # "mMinElevation":D
    .end local v7    # "stastics":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f090811

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 638
    :cond_2
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    if-eqz v8, :cond_3

    .line 639
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f09080f

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 641
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f090810

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0
.end method

.method private refreshChartView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 9
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 462
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->resetZoomWithoutAnimation()V

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->prepareChangeData(IILjava/util/List;)V

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v1, 0x1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->prepareChangeData(IILjava/util/List;)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v1, 0x2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->prepareChangeData(IILjava/util/List;)V

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->TIME_DEPTH_LEVEL:I

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    div-int/lit8 v3, v3, 0x4

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v2

    long-to-double v2, v2

    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->MARKING_INTERVAL:I

    sget v5, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->MARKING_COUNT:I

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 507
    :goto_0
    return-void

    .line 470
    :cond_0
    new-instance v6, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-direct {v6, v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;-><init>(II)V

    .line 472
    .local v6, "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 473
    invoke-direct {p0, v6, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 475
    new-instance v7, Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    invoke-direct {v7}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;-><init>()V

    .line 477
    .local v7, "dataSet":Lcom/samsung/android/sdk/chart/series/SchartDataSet;
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v7, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v7, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v7, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 480
    new-instance v0, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, v6, v7}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .line 482
    new-instance v8, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    invoke-direct {v8}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;-><init>()V

    .line 484
    .local v8, "interaction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setScaleZoomInteractionEnabled(Z)V

    .line 485
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInteractionEnabled(Z)V

    .line 486
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setPatialZoomInteractionEnabled(Z)V

    .line 487
    const/high16 v0, 0x40c00000    # 6.0f

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 488
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v2

    const-wide/32 v4, 0x2bf20

    sub-long/2addr v2, v4

    long-to-double v2, v2

    const/4 v4, 0x1

    const/4 v5, 0x5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v1, 0x2

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v1, 0x0

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->startGraphRevealAnimation(II)Z

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v1, 0x1

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->startGraphRevealAnimation(II)Z

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v1, 0x2

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->startGraphRevealAnimation(II)Z

    goto/16 :goto_0
.end method

.method private setUpDataSet()V
    .locals 45

    .prologue
    .line 221
    new-instance v5, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 222
    new-instance v5, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 223
    new-instance v5, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 225
    const/4 v7, 0x0

    .line 226
    .local v7, "projection":[Ljava/lang/String;
    const-string v8, "( data_type = 300001 OR data_type = 300002) AND "

    .line 228
    .local v8, "selectionClause":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "exercise__id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mRowID:J

    invoke-virtual {v5, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 229
    const/4 v7, 0x0

    .line 231
    const/16 v19, 0x0

    .line 233
    .local v19, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 234
    .local v11, "projection2":[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exercise__id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mRowID:J

    invoke-virtual {v5, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 235
    .local v12, "selectionClause2":Ljava/lang/String;
    const/4 v11, 0x0

    .line 236
    const/16 v20, 0x0

    .line 239
    .local v20, "cursor2":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    const-string v10, "ifnull(sample_time, create_time) ASC"

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 247
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    const-string v14, "ifnull(sample_time, create_time) ASC"

    invoke-virtual/range {v9 .. v14}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 256
    const-wide/16 v29, 0x0

    .line 257
    .local v29, "heartRatePerMinSave":D
    const/4 v15, 0x0

    .line 258
    .local v15, "avg_speed_save":F
    const-wide/16 v33, 0x0

    .line 259
    .local v33, "lastSampleTimeSpeed":J
    const-wide/16 v31, 0x0

    .line 260
    .local v31, "lastSampleTimeHeartrate":J
    const-wide/16 v38, 0x0

    .line 262
    .local v38, "sampleTimeTemp":J
    if-eqz v19, :cond_17

    if-eqz v20, :cond_17

    .line 263
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_10

    .line 264
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    .line 266
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v42

    .line 267
    .local v42, "start_c":Ljava/util/Calendar;
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 268
    const-string v5, "create_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v40

    .line 272
    .local v40, "startTime":J
    :goto_0
    move-object/from16 v0, v42

    move-wide/from16 v1, v40

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 274
    :goto_1
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_e

    .line 276
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 277
    const-string v5, "create_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v36

    .line 282
    .local v36, "sampleTime":J
    :goto_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 283
    .local v18, "curr_c":Ljava/util/Calendar;
    move-object/from16 v0, v18

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 284
    const/16 v5, 0xb

    const/16 v6, 0xb

    move-object/from16 v0, v42

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 285
    const/16 v5, 0xc

    const/16 v6, 0xc

    move-object/from16 v0, v42

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 286
    const/16 v5, 0xd

    const/16 v6, 0xd

    move-object/from16 v0, v42

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 287
    const/16 v5, 0xe

    const/16 v6, 0xe

    move-object/from16 v0, v42

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 288
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v36

    .line 290
    const-string/jumbo v5, "speed_per_hour"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getFloat(I)F

    move-result v5

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float v26, v5, v6

    .line 292
    .local v26, "float_avg_speed":F
    const-string v5, "heart_rate_per_min"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v27

    .line 294
    .local v27, "heartRatePerMin":D
    const/4 v5, 0x0

    cmpl-float v5, v26, v5

    if-nez v5, :cond_4

    .line 295
    const-wide/16 v5, 0x0

    cmpl-double v5, v27, v5

    if-nez v5, :cond_2

    .line 296
    const-wide/16 v31, 0x0

    .line 297
    const-wide/16 v29, 0x0

    .line 306
    :goto_3
    const-wide/16 v33, 0x0

    .line 325
    :goto_4
    new-instance v21, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 326
    .local v21, "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getPaceMode()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 327
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    if-eqz v5, :cond_a

    .line 328
    const/high16 v5, 0x40000000    # 2.0f

    cmpl-float v5, v15, v5

    if-ltz v5, :cond_7

    .line 329
    const/high16 v5, 0x45610000    # 3600.0f

    div-float/2addr v5, v15

    const/high16 v6, 0x42700000    # 60.0f

    div-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v43, v0

    .line 330
    .local v43, "tempPace":D
    move-object/from16 v0, v21

    move-wide/from16 v1, v36

    move-wide/from16 v3, v43

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 349
    .end local v43    # "tempPace":D
    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 351
    new-instance v22, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v22 .. v22}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 352
    .local v22, "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    move-object/from16 v0, v22

    move-wide/from16 v1, v36

    move-wide/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 353
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 354
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    .line 355
    move-wide/from16 v38, v36

    .line 356
    goto/16 :goto_1

    .line 270
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v22    # "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v26    # "float_avg_speed":F
    .end local v27    # "heartRatePerMin":D
    .end local v36    # "sampleTime":J
    .end local v40    # "startTime":J
    :cond_0
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v40

    .restart local v40    # "startTime":J
    goto/16 :goto_0

    .line 279
    :cond_1
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v36

    .restart local v36    # "sampleTime":J
    goto/16 :goto_2

    .line 299
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    .restart local v26    # "float_avg_speed":F
    .restart local v27    # "heartRatePerMin":D
    :cond_2
    sub-long v5, v36, v33

    const-wide/16 v9, 0x2710

    cmp-long v5, v5, v9

    if-gez v5, :cond_3

    .line 300
    move/from16 v26, v15

    goto :goto_3

    .line 302
    :cond_3
    move-wide/from16 v29, v27

    .line 303
    move-wide/from16 v31, v36

    goto/16 :goto_3

    .line 309
    :cond_4
    const-wide/16 v5, 0x0

    cmpl-double v5, v27, v5

    if-nez v5, :cond_6

    .line 310
    sub-long v5, v36, v31

    const-wide/16 v9, 0x2710

    cmp-long v5, v5, v9

    if-gez v5, :cond_5

    .line 311
    move-wide/from16 v27, v29

    .line 321
    :goto_6
    move-wide/from16 v33, v36

    .line 322
    move/from16 v15, v26

    goto/16 :goto_4

    .line 313
    :cond_5
    const-wide/16 v31, 0x0

    .line 314
    const-wide/16 v29, 0x0

    goto :goto_6

    .line 317
    :cond_6
    move-wide/from16 v29, v27

    .line 318
    move-wide/from16 v31, v36

    goto :goto_6

    .line 332
    .restart local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_7
    const-wide/high16 v5, 0x403e000000000000L    # 30.0

    move-object/from16 v0, v21

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_5

    .line 454
    .end local v15    # "avg_speed_save":F
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v26    # "float_avg_speed":F
    .end local v27    # "heartRatePerMin":D
    .end local v29    # "heartRatePerMinSave":D
    .end local v31    # "lastSampleTimeHeartrate":J
    .end local v33    # "lastSampleTimeSpeed":J
    .end local v36    # "sampleTime":J
    .end local v38    # "sampleTimeTemp":J
    .end local v40    # "startTime":J
    .end local v42    # "start_c":Ljava/util/Calendar;
    :catchall_0
    move-exception v5

    if-eqz v19, :cond_8

    .line 455
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 456
    :cond_8
    if-eqz v20, :cond_9

    .line 457
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v5

    .line 335
    .restart local v15    # "avg_speed_save":F
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    .restart local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .restart local v26    # "float_avg_speed":F
    .restart local v27    # "heartRatePerMin":D
    .restart local v29    # "heartRatePerMinSave":D
    .restart local v31    # "lastSampleTimeHeartrate":J
    .restart local v33    # "lastSampleTimeSpeed":J
    .restart local v36    # "sampleTime":J
    .restart local v38    # "sampleTimeTemp":J
    .restart local v40    # "startTime":J
    .restart local v42    # "start_c":Ljava/util/Calendar;
    :cond_a
    const v5, 0x404dfefc

    cmpl-float v5, v15, v5

    if-ltz v5, :cond_b

    .line 336
    const/high16 v5, 0x45610000    # 3600.0f

    const/high16 v6, 0x447a0000    # 1000.0f

    mul-float/2addr v6, v15

    const v9, 0x3a22e36f

    mul-float/2addr v6, v9

    div-float/2addr v5, v6

    const/high16 v6, 0x42700000    # 60.0f

    div-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v43, v0

    .line 337
    .restart local v43    # "tempPace":D
    :try_start_1
    move-object/from16 v0, v21

    move-wide/from16 v1, v36

    move-wide/from16 v3, v43

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    goto/16 :goto_5

    .line 339
    .end local v43    # "tempPace":D
    :cond_b
    const-wide/high16 v5, 0x403e000000000000L    # 30.0

    move-object/from16 v0, v21

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    goto/16 :goto_5

    .line 342
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    if-eqz v5, :cond_d

    .line 343
    float-to-double v5, v15

    move-object/from16 v0, v21

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    goto/16 :goto_5

    .line 345
    :cond_d
    float-to-long v5, v15

    const-wide/16 v9, 0x3e8

    mul-long/2addr v5, v9

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v35

    .line 346
    .local v35, "mi_speed":Ljava/lang/String;
    invoke-static/range {v35 .. v35}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    move-object/from16 v0, v21

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    goto/16 :goto_5

    .line 357
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v26    # "float_avg_speed":F
    .end local v27    # "heartRatePerMin":D
    .end local v35    # "mi_speed":Ljava/lang/String;
    .end local v36    # "sampleTime":J
    :cond_e
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 376
    .end local v40    # "startTime":J
    .end local v42    # "start_c":Ljava/util/Calendar;
    :goto_7
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 377
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->getCount()I

    move-result v5

    const/4 v6, 0x2

    if-le v5, v6, :cond_13

    .line 378
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    .line 380
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v42

    .line 381
    .restart local v42    # "start_c":Ljava/util/Calendar;
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 382
    const-string v5, "create_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v40

    .line 386
    .restart local v40    # "startTime":J
    :goto_8
    move-object/from16 v0, v42

    move-wide/from16 v1, v40

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 387
    :cond_f
    :goto_9
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_14

    .line 389
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 390
    const-string v5, "create_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v36

    .line 394
    .restart local v36    # "sampleTime":J
    :goto_a
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 395
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    move-object/from16 v0, v18

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 396
    const/16 v5, 0xb

    const/16 v6, 0xb

    move-object/from16 v0, v42

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 397
    const/16 v5, 0xc

    const/16 v6, 0xc

    move-object/from16 v0, v42

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 398
    const/16 v5, 0xd

    const/16 v6, 0xd

    move-object/from16 v0, v42

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 399
    const/16 v5, 0xe

    const/16 v6, 0xe

    move-object/from16 v0, v42

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 400
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v36

    .line 402
    const-string v5, "altitude"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v24

    .line 404
    .local v24, "elevation":D
    new-instance v23, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 406
    .local v23, "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    move-object/from16 v0, v23

    move-wide/from16 v1, v36

    move-wide/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 408
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 410
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    .line 411
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-eqz v5, :cond_f

    sub-long v5, v38, v36

    const-wide/32 v9, 0xea60

    cmp-long v5, v5, v9

    if-gez v5, :cond_f

    .line 412
    new-instance v21, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 413
    .restart local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    move-object/from16 v0, v21

    move-wide/from16 v1, v38

    move-wide/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 414
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    goto/16 :goto_9

    .line 359
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v23    # "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v24    # "elevation":D
    .end local v36    # "sampleTime":J
    .end local v40    # "startTime":J
    .end local v42    # "start_c":Ljava/util/Calendar;
    :cond_10
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 361
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    const/16 v5, 0xb

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 362
    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 363
    const/16 v5, 0xd

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 364
    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 366
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v16

    .line 367
    .local v16, "c":J
    new-instance v21, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 368
    .restart local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    new-instance v22, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v22 .. v22}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 370
    .restart local v22    # "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-wide/16 v5, 0x0

    move-object/from16 v0, v21

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 371
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 373
    const-wide/16 v5, 0x0

    move-object/from16 v0, v22

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 374
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    goto/16 :goto_7

    .line 384
    .end local v16    # "c":J
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v22    # "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .restart local v42    # "start_c":Ljava/util/Calendar;
    :cond_11
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v40

    .restart local v40    # "startTime":J
    goto/16 :goto_8

    .line 392
    :cond_12
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v36

    .restart local v36    # "sampleTime":J
    goto/16 :goto_a

    .line 418
    .end local v36    # "sampleTime":J
    .end local v40    # "startTime":J
    .end local v42    # "start_c":Ljava/util/Calendar;
    :cond_13
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 420
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    const/16 v5, 0xb

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 421
    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 422
    const/16 v5, 0xd

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 423
    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 425
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v16

    .line 426
    .restart local v16    # "c":J
    new-instance v23, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 427
    .restart local v23    # "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-wide/16 v5, 0x0

    move-object/from16 v0, v23

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 428
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 430
    .end local v16    # "c":J
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v23    # "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_14
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 454
    :goto_b
    if-eqz v19, :cond_15

    .line 455
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 456
    :cond_15
    if-eqz v20, :cond_16

    .line 457
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 459
    :cond_16
    return-void

    .line 432
    :cond_17
    :try_start_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 434
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    const/16 v5, 0xb

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 435
    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 436
    const/16 v5, 0xd

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 437
    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 439
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v16

    .line 441
    .restart local v16    # "c":J
    new-instance v21, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 442
    .restart local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-wide/16 v5, 0x0

    move-object/from16 v0, v21

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 443
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 445
    new-instance v22, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v22 .. v22}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 446
    .restart local v22    # "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-wide/16 v5, 0x0

    move-object/from16 v0, v22

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 447
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 449
    new-instance v23, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 450
    .restart local v23    # "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-wide/16 v5, 0x0

    move-object/from16 v0, v23

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 451
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_b
.end method


# virtual methods
.method public getChartView()Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    .locals 1

    .prologue
    .line 979
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    return-object v0
.end method

.method public getInformationAreaView()Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;
    .locals 1

    .prologue
    .line 987
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    return-object v0
.end method

.method public getLegendView()Landroid/view/View;
    .locals 1

    .prologue
    .line 983
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;

    return-object v0
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 6

    .prologue
    const v5, 0x7f080528

    const/16 v4, 0x8

    .line 933
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 934
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0301bf

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;

    .line 936
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 937
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;

    const v3, 0x7f0807d9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 938
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;

    const v3, 0x7f080529

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 939
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;

    const v3, 0x7f0807da

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 940
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;

    const v3, 0x7f0807dc

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 941
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;

    const v3, 0x7f0807db

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 943
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 944
    .local v0, "first_text":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getPaceMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 945
    const v2, 0x7f090a4d

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 956
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;

    return-object v2

    .line 947
    :cond_0
    const v2, 0x7f090a49

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 1
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 210
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 211
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 212
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 213
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 215
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->setUpDataSet()V

    .line 216
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->refreshChartView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    return-object v0
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 2
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 912
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    .line 913
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x7f090810

    const v2, 0x7f09080f

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->density:F

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->scaledDensity:F

    .line 172
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v0

    const-string v1, "km"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    .line 173
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    if-eqz v0, :cond_4

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->speedUnit:Ljava/lang/String;

    .line 179
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->speedUnit:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->speedUnit:Ljava/lang/String;

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->bpmUnit:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->bpmUnit:Ljava/lang/String;

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->paceUnit:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 186
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getPaceMode()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 187
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    if-eqz v0, :cond_5

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900c7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->paceUnit:Ljava/lang/String;

    .line 198
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->elevationUnit:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090a57

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->elevationUnit:Ljava/lang/String;

    .line 202
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mContext:Landroid/app/Activity;

    .line 204
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mRootView:Landroid/view/View;

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mRootView:Landroid/view/View;

    return-object v0

    .line 176
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->speedUnit:Ljava/lang/String;

    goto/16 :goto_0

    .line 190
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900cc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->paceUnit:Ljava/lang/String;

    goto :goto_1

    .line 192
    :cond_6
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->kmUnit:Z

    if-eqz v0, :cond_7

    .line 193
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->paceUnit:Ljava/lang/String;

    goto :goto_1

    .line 195
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->paceUnit:Ljava/lang/String;

    goto :goto_1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 134
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mNormalBitmapYellowGreen:Landroid/graphics/Bitmap;

    .line 137
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mHandleOverBitmapYellowGreen:Landroid/graphics/Bitmap;

    .line 140
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mNormalBitmapRed:Landroid/graphics/Bitmap;

    .line 143
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mHandleOverBitmapRed:Landroid/graphics/Bitmap;

    .line 146
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mNormalBitmapBlack:Landroid/graphics/Bitmap;

    .line 149
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mHandleOverBitmapBlack:Landroid/graphics/Bitmap;

    .line 152
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mGraphBackground:Landroid/graphics/Bitmap;

    .line 155
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mGraphHandler:Landroid/graphics/Bitmap;

    .line 158
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mGraphLine:Landroid/graphics/Bitmap;

    .line 159
    invoke-super {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->onDestroyView()V

    .line 160
    return-void
.end method
