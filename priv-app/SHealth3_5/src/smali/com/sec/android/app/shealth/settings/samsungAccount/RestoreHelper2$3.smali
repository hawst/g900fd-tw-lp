.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->startBackup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field denominatorValue:I

.field endValue:I

.field startValue:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 1

    .prologue
    .line 456
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->startValue:I

    .line 458
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->denominatorValue:I

    .line 459
    const/16 v0, 0x19

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->endValue:I

    return-void
.end method


# virtual methods
.method public onFinished(II)V
    .locals 7
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I

    .prologue
    const-wide/16 v5, 0x64

    const/4 v4, 0x0

    const/16 v3, 0x19

    .line 500
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Shealth Backup onFinished. Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    if-nez p2, :cond_1

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cignaBackupRestoreEnable:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$900(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[success] Shealth Backup onFinished. calling cigna"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const-string v1, "BACKUP"

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cignaBackupRestore(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1000(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Ljava/lang/String;)Z

    .line 544
    :goto_0
    return-void

    .line 507
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cigna is diabled and hence not calling Cigna Backup, 3.0 Backup: Backup was completed, Restore start"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->updateProgressPopupValue(I)V

    .line 509
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;)V

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 518
    :cond_1
    const/16 v0, 0x11

    if-ne p2, v0, :cond_2

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->updateProgressPopupValue(I)V

    .line 522
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3.0 Backup: Backup was completed, Restore start"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;)V

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 530
    :cond_2
    const/16 v0, 0x10

    if-ne p2, v0, :cond_3

    .line 531
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Device Storage is low"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 539
    :cond_3
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "3.0 Backup error received : else case"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NETWORK_FAILURE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onProgress(II)V
    .locals 5
    .param p1, "dataSyncType"    # I
    .param p2, "percent"    # I

    .prologue
    .line 493
    const-wide v1, 0x3fe999999999999aL    # 0.8

    iget v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->startValue:I

    int-to-double v3, v3

    mul-double/2addr v1, v3

    iget v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->denominatorValue:I

    div-int v3, p2, v3

    int-to-double v3, v3

    add-double/2addr v1, v3

    double-to-int v0, v1

    .line 494
    .local v0, "percentage":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->updateProgressPopupValue(I)V

    .line 495
    return-void
.end method

.method public onStarted(IILjava/lang/String;)V
    .locals 4
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 464
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Backup Started. Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$602(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 471
    :cond_0
    const/16 v0, 0x10

    if-ne p2, v0, :cond_2

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    .line 488
    :cond_1
    :goto_0
    return-void

    .line 475
    :cond_2
    const/16 v0, 0xb

    if-ne p2, v0, :cond_3

    .line 476
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Backup, Samsung account issue occured"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->closeProgressDialog()V

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->requestCheckValication(Landroid/content/Context;)V

    goto :goto_0

    .line 480
    :cond_3
    const/16 v0, 0x12

    if-ne p2, v0, :cond_4

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->SERVER_ERROR_MESSAGE:Ljava/lang/String;
    invoke-static {v0, p3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$802(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Ljava/lang/String;)Ljava/lang/String;

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 484
    :cond_4
    if-eqz p2, :cond_1

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NETWORK_FAILURE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0
.end method
