.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$15;
.super Ljava/lang/Object;
.source "ExerciseProDetailsActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->showResetPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V
    .locals 0

    .prologue
    .line 995
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 998
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isPhotoChanged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 999
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1000
    .local v0, "currentFragment":Landroid/support/v4/app/Fragment;
    instance-of v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 1001
    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->deleteIfExtraImagesChanged()V

    .line 1002
    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    .end local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->retainPreviousImages()V

    .line 1005
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 1006
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    .line 1007
    return-void
.end method
