.class public Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;
.super Landroid/widget/LinearLayout;
.source "CurrentMissionDetailHeaderView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView$OnDoItAgainClickListener;
    }
.end annotation


# instance fields
.field private mCurrentMissionAgainBtn:Landroid/widget/Button;

.field private mCurrentMissionExtraTxt:Landroid/widget/TextView;

.field private mCurrentMissionIconImg:Landroid/widget/ImageView;

.field private mCurrentMissionTitleTxt:Landroid/widget/TextView;

.field private mOnDoItAgainClickListener:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView$OnDoItAgainClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->initLayout()V

    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->initListener()V

    .line 38
    return-void
.end method

.method private initLayout()V
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03003a

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 44
    const v0, 0x7f080139

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->mCurrentMissionIconImg:Landroid/widget/ImageView;

    .line 45
    const v0, 0x7f08013a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->mCurrentMissionTitleTxt:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f08013c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->mCurrentMissionExtraTxt:Landroid/widget/TextView;

    .line 47
    const v0, 0x7f080140

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->mCurrentMissionAgainBtn:Landroid/widget/Button;

    .line 48
    return-void
.end method

.method private initListener()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->mCurrentMissionAgainBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 104
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 107
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->mOnDoItAgainClickListener:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView$OnDoItAgainClickListener;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->mOnDoItAgainClickListener:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView$OnDoItAgainClickListener;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView$OnDoItAgainClickListener;->onDoItAgainClick()V

    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x7f080140
        :pswitch_0
    .end packed-switch
.end method

.method public setCompleteDateText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 71
    const v0, 0x7f08013d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    return-void
.end method

.method public setEnableDoItAgainBtn(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 87
    const v0, 0x7f080140

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 88
    return-void
.end method

.method public setExtraText(Ljava/lang/String;)V
    .locals 2
    .param p1, "extraText"    # Ljava/lang/String;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->mCurrentMissionExtraTxt:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    return-void
.end method

.method public setIconImage(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "iconImage"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->mCurrentMissionIconImg:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 56
    return-void
.end method

.method public setOnDoItAgainClickListener(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView$OnDoItAgainClickListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView$OnDoItAgainClickListener;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->mOnDoItAgainClickListener:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView$OnDoItAgainClickListener;

    .line 100
    return-void
.end method

.method public setTitleText(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->mCurrentMissionTitleTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    return-void
.end method

.method public setTrackerMissionInfoText(Ljava/lang/String;)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 75
    const v0, 0x7f08013e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090347

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    return-void
.end method

.method public setVisibilityCompleteDate(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 91
    const v0, 0x7f08013d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 92
    return-void

    .line 91
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setVisibilityDoItAgain(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 83
    const v0, 0x7f08013f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 84
    return-void

    .line 83
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setVisibilityTrackerMissionInfoTxt(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 95
    const v0, 0x7f08013e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 96
    return-void

    .line 95
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setVisibilityXDayLeft(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 79
    const v0, 0x7f08013b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 80
    return-void

    .line 79
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setXDayLeftText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 67
    const v0, 0x7f08013b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    return-void
.end method
