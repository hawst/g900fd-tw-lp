.class Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$9;
.super Ljava/lang/Object;
.source "WalkingMateDayStepService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStop(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

.field final synthetic val$reStart:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;Z)V
    .locals 0

    .prologue
    .line 1402
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$9;->val$reStart:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1405
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->lock:Ljava/lang/Object;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$2000()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 1406
    :try_start_0
    const-string v1, "WalkingMateDayStepService"

    const-string v3, "OnStop"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1407
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    .line 1409
    .local v0, "deviceType":I
    const/16 v1, 0x2719

    if-ne v0, v1, :cond_0

    .line 1410
    const-string v1, "PEDOSTART"

    const-string v3, "Healthy Step, Active time start"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1411
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetActiveTime()V

    .line 1412
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetHealthyStep()V

    .line 1413
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetInactiveMonitor()V

    .line 1414
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->allStop()V

    .line 1417
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->unregisterPedometerListener()V

    .line 1419
    const-string v1, "WalkingMateDayStepService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "restart condition = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$9;->val$reStart:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1420
    invoke-static {}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->checkCoverView()V

    .line 1421
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1423
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$9;->val$reStart:Z

    if-eqz v1, :cond_1

    .line 1424
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "ShealthSensorDevice.DataListener : onStopped() - Pedometer restarted"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1425
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStart()V

    .line 1429
    :goto_0
    return-void

    .line 1421
    .end local v0    # "deviceType":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 1427
    .restart local v0    # "deviceType":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->stopAlarm(Landroid/content/Context;)V

    goto :goto_0
.end method
