.class public Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;
.super Landroid/content/ContentProvider;
.source "BaseContentProvider.java"


# static fields
.field private static final CREATETIME_URI_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private static final NONGNERIC_RETURN:I = -0x2

.field private static final TAG:Ljava/lang/String;

.field private static final THIRD_PARTY_IDENTIFIER:Ljava/lang/String; = "0000"

.field private static final UPDATETIME_URI_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private static final URI_APP_REG_REPLACE:I = 0x3e2

.field private static final URI_TYPE_APP_REGISTRY:I = 0x40c

.field private static final URI_TYPE_APP_REGISTRY_ID:I = 0x40d

.field private static final URI_TYPE_FITNESS_CHART:I = 0x43b

.field private static final URI_TYPE_FITNESS_CHART_ID:I = 0x43c

.field private static final URI_TYPE_HEALTHBOARD_CIGNA:I = 0x40a

.field private static final URI_TYPE_HEALTHBOARD_CIGNA_ID:I = 0x40b

.field private static final URI_TYPE_LONGTERM_COACH:I = 0x43d

.field private static final URI_TYPE_LONGTERM_COACH_ID:I = 0x43e

.field private static final URI_TYPE_MIGRATION:I = 0x3e3

.field private static final URI_TYPE_RAWQUERY:I = 0x3e4

.field private static final URI_TYPE_SHEALTH_DB:I = 0x3e7

.field private static final URI_TYPE_SHEALTH_SECUREDB:I = 0x3e6

.field private static final URI_TYPE_SHEALTH_SHARED_PREF_FILE:I = 0x3e5

.field private static final URI_TYPE_TRAINING_LOAD_PEAK:I = 0x40e

.field private static final URI_TYPE_TRAINING_LOAD_PEAK_ID:I = 0x40f

.field private static final sURIMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 42
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->UPDATETIME_URI_LIST:Ljava/util/ArrayList;

    .line 78
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    .line 81
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string v2, "app_registry"

    const/16 v3, 0x40c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 82
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string v2, "app_registry/#"

    const/16 v3, 0x40d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string/jumbo v2, "training_load_peak"

    const/16 v3, 0x40e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 84
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string/jumbo v2, "training_load_peak/#"

    const/16 v3, 0x40f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 86
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string v2, "Shealthdb_read"

    const/16 v3, 0x3e7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 87
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string v2, "Shealth_securedb_read"

    const/16 v3, 0x3e6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 88
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string v2, "Shared_prefs_read"

    const/16 v3, 0x3e5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string/jumbo v2, "rawquery"

    const/16 v3, 0x3e4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 91
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string v2, "fitness_chart"

    const/16 v3, 0x43b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 92
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string v2, "fitness_chart/#"

    const/16 v3, 0x43c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 93
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string v2, "longterm_coach"

    const/16 v3, 0x43d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 94
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string v2, "longterm_coach/#"

    const/16 v3, 0x43e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 96
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string/jumbo v2, "migration"

    const/16 v3, 0x3e3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 97
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    const-string v2, "app_registry_replace"

    const/16 v3, 0x3e2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 99
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/_private/BaseContract$HealthBoardCigna;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/_private/BaseContract$TrainingLoadPeak;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/_private/BaseContract$FitnessChart;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->CREATETIME_URI_LIST:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/android/sdk/health/_private/BaseContract$LongtermCoach;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private getDeviceTimeZone()I
    .locals 5

    .prologue
    .line 639
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 640
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    .line 641
    .local v2, "timeZone":Ljava/util/TimeZone;
    invoke-virtual {v2}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v1

    .line 642
    .local v1, "mGMTOffset":I
    mul-int/lit8 v3, v1, 0xa

    const v4, 0x36ee80

    div-int v1, v3, v4

    .line 643
    return v1
.end method

.method private getIdTimeStamp(Landroid/content/ContentValues;)J
    .locals 5
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 634
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private getTableNameForUri(I)Ljava/lang/String;
    .locals 3
    .param p1, "uriType"    # I

    .prologue
    .line 510
    sparse-switch p1, :sswitch_data_0

    .line 528
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown Table: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 514
    :sswitch_0
    const-string v0, "app_registry"

    .line 526
    :goto_0
    return-object v0

    .line 517
    :sswitch_1
    const-string v0, "healthboard_cigna"

    goto :goto_0

    .line 520
    :sswitch_2
    const-string/jumbo v0, "training_load_peak"

    goto :goto_0

    .line 523
    :sswitch_3
    const-string v0, "fitness_chart"

    goto :goto_0

    .line 526
    :sswitch_4
    const-string v0, "longterm_coach"

    goto :goto_0

    .line 510
    :sswitch_data_0
    .sparse-switch
        0x40a -> :sswitch_1
        0x40b -> :sswitch_1
        0x40c -> :sswitch_0
        0x40d -> :sswitch_0
        0x40e -> :sswitch_2
        0x40f -> :sswitch_2
        0x43b -> :sswitch_3
        0x43c -> :sswitch_3
        0x43d -> :sswitch_4
        0x43e -> :sswitch_4
    .end sparse-switch
.end method

.method private handleGenericDelete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    const v10, 0x29814

    .line 344
    sget-object v8, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    .line 345
    .local v6, "uriType":I
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 346
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v5

    .line 347
    .local v5, "tableName":Ljava/lang/String;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 348
    .local v7, "values":Landroid/content/ContentValues;
    const/4 v4, 0x0

    .line 349
    .local v4, "syncStatus":Ljava/lang/String;
    const/4 v3, 0x0

    .line 350
    .local v3, "rowsUpdated":I
    const/4 v2, 0x0

    .line 351
    .local v2, "rowsDeleted":I
    sparse-switch v6, :sswitch_data_0

    .line 388
    const/4 v8, -0x2

    .line 392
    :goto_0
    return v8

    .line 359
    :sswitch_0
    const-string v4, " AND sync_status = 170002"

    .line 360
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v5, v8, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 362
    const-string/jumbo v8, "sync_status"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 363
    const-string v4, " AND sync_status != 170002"

    .line 364
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v5, v7, v8, p3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    add-int v3, v2, v8

    .line 391
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v8, p1, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    move v8, v3

    .line 392
    goto :goto_0

    .line 371
    :sswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 372
    .local v1, "id":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 374
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "_id ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v5, v7, v8, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 379
    :cond_0
    const-string v4, " AND sync_status = 170002"

    .line 380
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "_id ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " and "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v5, v8, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 382
    const-string/jumbo v8, "sync_status"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 383
    const-string v4, " AND sync_status != 170002"

    .line 384
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "_id ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " and "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v5, v7, v8, p3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    add-int v3, v2, v8

    .line 386
    goto/16 :goto_1

    .line 351
    :sswitch_data_0
    .sparse-switch
        0x40a -> :sswitch_0
        0x40b -> :sswitch_1
        0x40c -> :sswitch_0
        0x40d -> :sswitch_1
        0x40e -> :sswitch_0
        0x40f -> :sswitch_1
        0x43b -> :sswitch_0
        0x43c -> :sswitch_1
        0x43d -> :sswitch_0
        0x43e -> :sswitch_1
    .end sparse-switch
.end method

.method private handleGenericInsert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v5, 0x0

    .line 288
    const-string v6, "create_time"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 290
    const-string v6, "create_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 291
    const-string/jumbo v6, "time_zone"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getDeviceTimeZone()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 293
    :cond_0
    sget-object v6, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    .line 294
    .local v4, "uriType":I
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 295
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v1, 0x0

    .line 296
    .local v1, "id":J
    sparse-switch v4, :sswitch_data_0

    .line 309
    :goto_0
    return-object v5

    .line 302
    :sswitch_0
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v3

    .line 303
    .local v3, "tableName":Ljava/lang/String;
    const-string v6, "_id"

    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getIdTimeStamp(Landroid/content/ContentValues;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 304
    invoke-virtual {v0, v3, v5, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    .line 305
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, p1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 307
    invoke-static {p1, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    goto :goto_0

    .line 296
    nop

    :sswitch_data_0
    .sparse-switch
        0x40a -> :sswitch_0
        0x40e -> :sswitch_0
        0x43b -> :sswitch_0
        0x43d -> :sswitch_0
    .end sparse-switch
.end method

.method private handleGenericQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 197
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 198
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v9

    .line 199
    .local v9, "uriType":I
    sparse-switch v9, :sswitch_data_0

    .line 228
    :goto_0
    return-object v5

    .line 206
    :sswitch_0
    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 220
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    .line 221
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 223
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v8, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    :cond_0
    move-object v5, v8

    .line 228
    goto :goto_0

    .line 213
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :sswitch_1
    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 214
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 199
    nop

    :sswitch_data_0
    .sparse-switch
        0x40a -> :sswitch_0
        0x40b -> :sswitch_1
        0x40c -> :sswitch_0
        0x40e -> :sswitch_0
        0x40f -> :sswitch_1
        0x43b -> :sswitch_0
        0x43c -> :sswitch_1
        0x43d -> :sswitch_0
        0x43e -> :sswitch_1
    .end sparse-switch
.end method

.method private validateInitializationStatus()V
    .locals 4

    .prologue
    .line 116
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "App validateInitializationStatus : mIsInitialized = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :goto_0
    sget-boolean v1, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    if-nez v1, :cond_0

    .line 119
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 121
    :try_start_0
    sget-boolean v1, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    if-eqz v1, :cond_1

    .line 122
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :cond_0
    return-void

    .line 125
    :cond_1
    :try_start_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "validateInitializationStatus : waiting..."

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    :goto_1
    :try_start_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 128
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 492
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->validateInitializationStatus()V

    .line 493
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 494
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 497
    :try_start_0
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v1

    .line 498
    .local v1, "results":[Landroid/content/ContentProviderResult;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 503
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-object v1

    .end local v1    # "results":[Landroid/content/ContentProviderResult;
    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 7
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "arg"    # Ljava/lang/String;
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 535
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->validateInitializationStatus()V

    .line 536
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "call test, method : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 539
    :cond_0
    new-instance v4, Ljava/lang/NullPointerException;

    const-string/jumbo v5, "parameter \'method\' or \'extras\' should not be null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 541
    :cond_1
    const-string v4, "key"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 542
    .local v1, "key":Ljava/lang/String;
    const-string/jumbo v4, "value"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 544
    .local v0, "defaultValue":Ljava/lang/Object;
    if-nez v1, :cond_2

    .line 546
    new-instance v4, Ljava/lang/NullPointerException;

    const-string/jumbo v5, "parameter \'key\' should not be null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 549
    :cond_2
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 550
    .local v2, "result":Landroid/os/Bundle;
    const-string v4, "key"

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const-string v4, "CONFIG_OPTION_GET"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 553
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 554
    .local v3, "value":Ljava/lang/Object;
    if-nez v3, :cond_3

    .line 556
    move-object v3, v0

    .line 559
    :cond_3
    instance-of v4, v3, Ljava/lang/Integer;

    if-eqz v4, :cond_5

    .line 561
    const-string/jumbo v4, "value"

    check-cast v3, Ljava/lang/Integer;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 585
    .end local v2    # "result":Landroid/os/Bundle;
    :cond_4
    :goto_0
    return-object v2

    .line 563
    .restart local v2    # "result":Landroid/os/Bundle;
    .restart local v3    # "value":Ljava/lang/Object;
    :cond_5
    instance-of v4, v3, Ljava/lang/Float;

    if-eqz v4, :cond_6

    .line 565
    const-string/jumbo v4, "value"

    check-cast v3, Ljava/lang/Float;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    goto :goto_0

    .line 567
    .restart local v3    # "value":Ljava/lang/Object;
    :cond_6
    instance-of v4, v3, Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 569
    const-string/jumbo v4, "value"

    check-cast v3, Ljava/lang/String;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 571
    .restart local v3    # "value":Ljava/lang/Object;
    :cond_7
    instance-of v4, v3, Ljava/lang/Boolean;

    if-eqz v4, :cond_8

    .line 573
    const-string/jumbo v4, "value"

    check-cast v3, Ljava/lang/Boolean;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 575
    .restart local v3    # "value":Ljava/lang/Object;
    :cond_8
    instance-of v4, v3, Ljava/lang/Long;

    if-eqz v4, :cond_4

    .line 577
    const-string/jumbo v4, "value"

    check-cast v3, Ljava/lang/Long;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v2, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0

    .line 581
    :cond_9
    const-string v4, "CONFIG_OPTION_PUT"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 583
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    move-result-object v4

    invoke-virtual {v4, v1, v0}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    move-object v2, p3

    .line 585
    goto :goto_0

    .line 589
    :cond_a
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "illegal method"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->validateInitializationStatus()V

    .line 318
    if-eqz p2, :cond_0

    .line 320
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 324
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "sync_status != 170004"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 325
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->handleGenericDelete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 326
    .local v0, "rowDeleted":I
    const/4 v2, -0x2

    if-eq v0, v2, :cond_1

    .line 328
    return v0

    .line 323
    .end local v0    # "rowDeleted":I
    :cond_0
    const-string p2, ""

    goto :goto_0

    .line 332
    .restart local v0    # "rowDeleted":I
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 338
    .local v1, "uriType":I
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 234
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleGenericUpdate(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const v9, 0x29813

    .line 432
    const-string v6, "create_time"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 434
    const-string v6, "create_time"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 436
    :cond_0
    const-string/jumbo v6, "update_time"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 438
    const-string/jumbo v6, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 440
    :cond_1
    sget-object v6, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    .line 441
    .local v5, "uriType":I
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 442
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    .line 443
    .local v2, "rowsUpdated":I
    const/4 v3, 0x0

    .line 444
    .local v3, "syncStatus":Ljava/lang/String;
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v4

    .line 445
    .local v4, "tableName":Ljava/lang/String;
    sparse-switch v5, :sswitch_data_0

    .line 482
    const/4 v6, -0x2

    .line 486
    :goto_0
    return v6

    .line 453
    :sswitch_0
    const-string v3, " AND sync_status = 170002"

    .line 454
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, p2, v6, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 456
    const-string/jumbo v6, "sync_status"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 457
    const-string v3, " AND sync_status != 170002"

    .line 458
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, p2, v6, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    add-int/2addr v2, v6

    .line 485
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, p1, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    move v6, v2

    .line 486
    goto :goto_0

    .line 465
    :sswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 466
    .local v1, "id":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 468
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, p2, v6, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 473
    :cond_2
    const-string v3, " AND sync_status = 170002"

    .line 474
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, p2, v6, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 476
    const-string/jumbo v6, "sync_status"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 477
    const-string v3, " AND sync_status != 170002"

    .line 478
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, p2, v6, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    add-int/2addr v2, v6

    .line 480
    goto/16 :goto_1

    .line 445
    :sswitch_data_0
    .sparse-switch
        0x40a -> :sswitch_0
        0x40b -> :sswitch_1
        0x40c -> :sswitch_0
        0x40d -> :sswitch_1
        0x40e -> :sswitch_0
        0x40f -> :sswitch_1
        0x43b -> :sswitch_0
        0x43c -> :sswitch_1
        0x43d -> :sswitch_0
        0x43e -> :sswitch_1
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v9, 0x0

    .line 242
    if-nez p2, :cond_0

    .line 244
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": Content Values null"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 246
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->validateInitializationStatus()V

    .line 248
    const-string/jumbo v7, "sync_status"

    const v8, 0x29812

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 249
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->handleGenericInsert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 250
    .local v4, "returnUri":Landroid/net/Uri;
    if-eqz v4, :cond_1

    .line 279
    .end local v4    # "returnUri":Landroid/net/Uri;
    :goto_0
    return-object v4

    .line 258
    .restart local v4    # "returnUri":Landroid/net/Uri;
    :cond_1
    const/4 v0, 0x0

    .line 259
    .local v0, "cursor":Landroid/database/Cursor;
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v7, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    .line 260
    .local v6, "uriType":I
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 261
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v2, 0x0

    .line 262
    .local v2, "id":J
    sparse-switch v6, :sswitch_data_0

    .line 282
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown URI: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 265
    :sswitch_0
    const-string v7, "app_registry"

    invoke-virtual {v1, v7, v9, p2}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 266
    invoke-static {p1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    goto :goto_0

    .line 270
    :sswitch_1
    const-string/jumbo v7, "package_name"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 271
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Package name information is missing"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 272
    :cond_2
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getTableNameForUri(I)Ljava/lang/String;

    move-result-object v5

    .line 276
    .local v5, "tableName":Ljava/lang/String;
    invoke-virtual {v1, v5, v9, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-virtual {v7, p1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 279
    invoke-static {p1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    goto :goto_0

    .line 262
    :sswitch_data_0
    .sparse-switch
        0x3e2 -> :sswitch_0
        0x40c -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 109
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 111
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/high16 v7, 0x10000000

    .line 595
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->validateInitializationStatus()V

    .line 596
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "openFile : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mode : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 598
    .local v3, "uriType":I
    packed-switch v3, :pswitch_data_0

    .line 624
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 602
    :pswitch_0
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    const-string v5, "looking for shealth2 db"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "databases"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "shealth2.db"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 604
    .local v1, "shealth_dbFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 605
    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4

    .line 606
    :cond_0
    invoke-static {v1, v7}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    .line 622
    .end local v1    # "shealth_dbFile":Ljava/io/File;
    :goto_0
    return-object v4

    .line 608
    :pswitch_1
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    const-string v5, "looking for sec_shealth2 db"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "databases"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "secure_shealth2.db"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 610
    .local v2, "shealth_secdbFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 611
    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4

    .line 612
    :cond_1
    invoke-static {v2, v7}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    goto :goto_0

    .line 614
    .end local v2    # "shealth_secdbFile":Ljava/io/File;
    :pswitch_2
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    const-string v5, "looking for shared pref file"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "shared_prefs"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "restore_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_preferences.xml"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 616
    .local v0, "defaultPrefFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 618
    new-instance v0, Ljava/io/File;

    .end local v0    # "defaultPrefFile":Ljava/io/File;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "shared_prefs"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_preferences.xml"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 620
    .restart local v0    # "defaultPrefFile":Ljava/io/File;
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_3

    .line 621
    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4

    .line 622
    :cond_3
    invoke-static {v0, v7}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    goto/16 :goto_0

    .line 598
    :pswitch_data_0
    .packed-switch 0x3e5
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 146
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->validateInitializationStatus()V

    .line 148
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    .line 149
    .local v4, "uriType":I
    const/16 v5, 0x3e3

    if-ne v4, v5, :cond_2

    .line 151
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    const-string v6, " Raw query for Migration URI "

    invoke-static {v5, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :try_start_0
    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5}, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;-><init>(Landroid/content/Context;)V

    .line 156
    .local v2, "restoreHelper":Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getHPasswd()[B

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    .line 157
    .local v0, "db":Lsamsung/database/sqlite/SecSQLiteDatabase;
    if-nez p3, :cond_1

    .line 187
    .end local v0    # "db":Lsamsung/database/sqlite/SecSQLiteDatabase;
    .end local v2    # "restoreHelper":Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;
    :cond_0
    :goto_0
    return-object v3

    .line 160
    .restart local v0    # "db":Lsamsung/database/sqlite/SecSQLiteDatabase;
    .restart local v2    # "restoreHelper":Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;
    :cond_1
    invoke-virtual {v0, p3, p4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 162
    .end local v0    # "db":Lsamsung/database/sqlite/SecSQLiteDatabase;
    .end local v2    # "restoreHelper":Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;
    :catch_0
    move-exception v1

    .line 164
    .local v1, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    const-string v6, " Exception occured No Migration Data Available"

    invoke-static {v5, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 168
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    const/16 v5, 0x3e4

    if-eq v4, v5, :cond_3

    .line 170
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 171
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "sync_status != 170004 AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 175
    :cond_3
    :goto_1
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->handleGenericQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 176
    .local v3, "resultCursor":Landroid/database/Cursor;
    if-nez v3, :cond_0

    .line 181
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 183
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    packed-switch v4, :pswitch_data_0

    .line 190
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown URI: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 173
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "resultCursor":Landroid/database/Cursor;
    :cond_4
    const-string/jumbo p3, "sync_status != 170004"

    goto :goto_1

    .line 187
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v3    # "resultCursor":Landroid/database/Cursor;
    :pswitch_0
    invoke-virtual {v0, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_0

    .line 183
    :pswitch_data_0
    .packed-switch 0x3e4
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 399
    if-nez p2, :cond_0

    .line 401
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": Content Values null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 403
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->validateInitializationStatus()V

    .line 406
    if-eqz p3, :cond_1

    .line 408
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 412
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "sync_status != 170004"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 413
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->handleGenericUpdate(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 414
    .local v0, "rowsUpdated":I
    const/4 v2, -0x2

    if-eq v0, v2, :cond_2

    .line 416
    return v0

    .line 411
    .end local v0    # "rowsUpdated":I
    :cond_1
    const-string p3, ""

    goto :goto_0

    .line 420
    .restart local v0    # "rowsUpdated":I
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/BaseContentProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 425
    .local v1, "uriType":I
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
