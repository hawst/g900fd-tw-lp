.class public interface abstract Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTimeTypeSpecific;
.super Ljava/lang/Object;
.source "CommonDaoWithDateTimeTypeSpecific.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;",
        "DataTypeDescriptor::",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime",
        "<TT;>;"
    }
.end annotation


# virtual methods
.method public abstract deleteAll(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDataTypeDescriptor;)I"
        }
    .end annotation
.end method

.method public abstract getAggregationFunctionResultForPeriod(JJLcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;)F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;",
            "TDataTypeDescriptor;)F"
        }
    .end annotation
.end method

.method public abstract getAllDatas(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDataTypeDescriptor;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getDataForDay(JLcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JTDataTypeDescriptor;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJTDataTypeDescriptor;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getDataForPeriodsWithItAggregation(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;",
            "Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;",
            "TDataTypeDescriptor;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end method
