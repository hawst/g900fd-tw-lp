.class public abstract Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;
.super Ljava/lang/Object;
.source "BaseFoodSearchApi.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;
    }
.end annotation


# static fields
.field protected static final AUTO_COMPLETE_LIST_MAX_LENGTH:I = 0xa

.field private static final CONNECTION_TIMEOUT:I = 0x1b58

.field private static final DEFAULT_AUTO_COMPLETE_MIN_LENGTH:I = 0x1

.field protected static final DEFAULT_LANG:Ljava/lang/String;

.field protected static final DEFAULT_REGION:Ljava/lang/String;

.field private static final ES_LANG:Ljava/lang/String; = "es"

.field private static final ES_REGION:Ljava/lang/String; = "ES"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final NO_RESOURCE_IDENTIFIER:I = -0x1

.field private static final PAGE_NUMBER_DEFAULT:I = 0x0

.field private static final PER_PAGE_NUMBER_DEFAULT:I = 0x32

.field private static final READ_DATA_TIMEOUT:I = 0x1388


# instance fields
.field protected context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->LOG_TAG:Ljava/lang/String;

    .line 57
    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->DEFAULT_LANG:Ljava/lang/String;

    .line 61
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->DEFAULT_REGION:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->context:Landroid/content/Context;

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .param p2, "x2"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->updateCategoriesData(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    return-void
.end method

.method protected static doHttpMethodReq(Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 15
    .param p0, "urlStr"    # Ljava/lang/String;
    .param p1, "requestMethod"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;
    .param p2, "paramStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 296
    .local p3, "header":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 297
    .local v9, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 298
    .local v1, "bufferedReader":Ljava/io/BufferedReader;
    const/4 v7, 0x0

    .line 300
    .local v7, "streamWriter":Ljava/io/OutputStreamWriter;
    :try_start_0
    sget-object v12, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->LOG_TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Http request params: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    new-instance v10, Ljava/net/URL;

    invoke-direct {v10, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 303
    .local v10, "url":Ljava/net/URL;
    invoke-virtual {v10}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v11

    check-cast v11, Ljava/net/HttpURLConnection;

    .line 304
    .local v11, "urlConnection":Ljava/net/HttpURLConnection;
    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 305
    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 306
    const/16 v12, 0x1b58

    invoke-virtual {v11, v12}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 307
    const/16 v12, 0x1388

    invoke-virtual {v11, v12}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 308
    if-eqz p1, :cond_0

    .line 309
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 311
    :cond_0
    if-eqz p3, :cond_1

    .line 312
    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    .line 313
    .local v4, "entrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 314
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 340
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "entrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v10    # "url":Ljava/net/URL;
    .end local v11    # "urlConnection":Ljava/net/HttpURLConnection;
    :catchall_0
    move-exception v12

    :goto_1
    const/4 v13, 0x2

    new-array v13, v13, [Ljava/io/Closeable;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    const/4 v14, 0x1

    aput-object v1, v13, v14

    invoke-static {v13}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    throw v12

    .line 318
    .restart local v10    # "url":Ljava/net/URL;
    .restart local v11    # "urlConnection":Ljava/net/HttpURLConnection;
    :cond_1
    if-eqz p1, :cond_2

    :try_start_1
    sget-object v12, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->POST:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    if-eqz p2, :cond_2

    .line 319
    new-instance v8, Ljava/io/OutputStreamWriter;

    invoke-virtual {v11}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v12

    invoke-direct {v8, v12}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 320
    .end local v7    # "streamWriter":Ljava/io/OutputStreamWriter;
    .local v8, "streamWriter":Ljava/io/OutputStreamWriter;
    :try_start_2
    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 321
    invoke-virtual {v8}, Ljava/io/OutputStreamWriter;->flush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v7, v8

    .line 324
    .end local v8    # "streamWriter":Ljava/io/OutputStreamWriter;
    .restart local v7    # "streamWriter":Ljava/io/OutputStreamWriter;
    :cond_2
    :try_start_3
    invoke-virtual {v11}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v12

    const/16 v13, 0x190

    if-lt v12, v13, :cond_3

    .line 325
    new-instance v12, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/IllegalInputDataException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Wrong input data: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/IllegalInputDataException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 329
    :cond_3
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v12, Ljava/io/InputStreamReader;

    invoke-virtual {v11}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v12}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 333
    .end local v1    # "bufferedReader":Ljava/io/BufferedReader;
    .local v2, "bufferedReader":Ljava/io/BufferedReader;
    :goto_2
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v6

    .line 334
    .local v6, "line":Ljava/lang/String;
    if-nez v6, :cond_4

    .line 340
    const/4 v12, 0x2

    new-array v12, v12, [Ljava/io/Closeable;

    const/4 v13, 0x0

    aput-object v7, v12, v13

    const/4 v13, 0x1

    aput-object v2, v12, v13

    invoke-static {v12}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    .line 343
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    return-object v12

    .line 337
    :cond_4
    :try_start_5
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    .line 340
    .end local v6    # "line":Ljava/lang/String;
    :catchall_1
    move-exception v12

    move-object v1, v2

    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferedReader":Ljava/io/BufferedReader;
    goto/16 :goto_1

    .end local v7    # "streamWriter":Ljava/io/OutputStreamWriter;
    .restart local v8    # "streamWriter":Ljava/io/OutputStreamWriter;
    :catchall_2
    move-exception v12

    move-object v7, v8

    .end local v8    # "streamWriter":Ljava/io/OutputStreamWriter;
    .restart local v7    # "streamWriter":Ljava/io/OutputStreamWriter;
    goto/16 :goto_1
.end method

.method private handleException(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult",
            "<TResp;TReq;>;Req:",
            "Ljava/lang/Object;",
            "Resp:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable",
            "<TE;>;)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 416
    .local p1, "resultRunnable":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable<TE;>;"
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->handleExceptionAndSetRequest(Ljava/lang/Object;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    return-object v0
.end method

.method private handleExceptionAndSetRequest(Ljava/lang/Object;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult",
            "<TResp;TReq;>;Req:",
            "Ljava/lang/Object;",
            "Resp:",
            "Ljava/lang/Object;",
            ">(TReq;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable",
            "<TE;>;)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .local p1, "request":Ljava/lang/Object;, "TReq;"
    .local p2, "resultRunnable":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable<TE;>;"
    const v5, 0x7f09095a

    .line 441
    :try_start_0
    invoke-interface {p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;->run()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    move-result-object v2

    .line 442
    .local v2, "searchResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;, "TE;"
    if-eqz p1, :cond_0

    if-eqz v2, :cond_0

    .line 443
    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;->setRequest(Ljava/lang/Object;)V

    .line 445
    :cond_0
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->createSuccessfulResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    :try_end_0
    .catch Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/ParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/sec/android/app/shealth/food/foodpick/search/api/base/IllegalInputDataException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    .line 464
    .end local v2    # "searchResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;, "TE;"
    .local v1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    :goto_0
    return-object v1

    .line 446
    .end local v1    # "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    :catch_0
    move-exception v0

    .line 447
    .local v0, "e":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodNotFoundException;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 448
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->NO_BARCODE_FOUND:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->context:Landroid/content/Context;

    const v5, 0x7f09095f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->createErrorResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v1

    .line 463
    .restart local v1    # "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    goto :goto_0

    .line 450
    .end local v0    # "e":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodNotFoundException;
    .end local v1    # "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    :catch_1
    move-exception v0

    .line 451
    .local v0, "e":Lorg/apache/http/ParseException;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 452
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->ERROR_PARSE:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->context:Landroid/content/Context;

    const v5, 0x7f09095b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->createErrorResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v1

    .line 463
    .restart local v1    # "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    goto :goto_0

    .line 454
    .end local v0    # "e":Lorg/apache/http/ParseException;
    .end local v1    # "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    :catch_2
    move-exception v0

    .line 455
    .local v0, "e":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/IllegalInputDataException;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 457
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->ERROR_BAD_REQUEST:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->context:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->createErrorResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v1

    .line 463
    .restart local v1    # "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    goto :goto_0

    .line 459
    .end local v0    # "e":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/IllegalInputDataException;
    .end local v1    # "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    :catch_3
    move-exception v0

    .line 460
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 461
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->ERROR_CONNECTION:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->context:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->createErrorResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v1

    .restart local v1    # "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    goto :goto_0
.end method

.method protected static loadXmlDocument(Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 5
    .param p0, "xml"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 357
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 358
    .local v1, "docBuilderFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 359
    .local v0, "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v3, Lorg/xml/sax/InputSource;

    new-instance v4, Ljava/io/StringReader;

    invoke-direct {v4, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0, v3}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    return-object v3

    .line 360
    .end local v0    # "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v1    # "docBuilderFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :catch_0
    move-exception v2

    .line 361
    .local v2, "ex":Ljava/io/IOException;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 362
    throw v2

    .line 363
    .end local v2    # "ex":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 364
    .local v2, "ex":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 365
    new-instance v3, Landroid/util/AndroidRuntimeException;

    invoke-direct {v3, v2}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/Exception;)V

    throw v3
.end method

.method private updateCategoriesData(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 1
    .param p1, "result"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .param p2, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 205
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getRootCategoriesName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerRootCategory(Ljava/lang/String;)V

    .line 206
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getRootCategoriesId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerRootCategoryId(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getSubCategoriesName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSubCategory(Ljava/lang/String;)V

    .line 208
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getSubCategoriesId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSubCategoryId(Ljava/lang/String;)V

    .line 209
    return-void
.end method


# virtual methods
.method protected checkLang(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 407
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 408
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->LOG_TAG:Ljava/lang/String;

    const-string v1, "lang is specified as null"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    sget-object p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->DEFAULT_LANG:Ljava/lang/String;

    .line 411
    .end local p1    # "lang":Ljava/lang/String;
    :cond_1
    return-object p1
.end method

.method protected checkRegion(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "region"    # Ljava/lang/String;

    .prologue
    .line 389
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 390
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "region is specified as null"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    sget-object p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->DEFAULT_REGION:Ljava/lang/String;

    .line 397
    .end local p1    # "region":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p1

    .line 394
    .restart local p1    # "region":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "es"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ES"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 395
    const-string p1, "ES"

    goto :goto_0
.end method

.method public editSearchPhrase(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "searchText"    # Ljava/lang/String;

    .prologue
    .line 488
    return-object p1
.end method

.method public getAutoCompleteMinLength()I
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x1

    return v0
.end method

.method protected getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->checkRegion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIndexOfFirstPage()I
    .locals 1

    .prologue
    .line 498
    const/4 v0, 0x0

    return v0
.end method

.method protected getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->checkLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLogoResourceId()I
    .locals 1

    .prologue
    .line 265
    const/4 v0, -0x1

    return v0
.end method

.method public getPerPageCount()I
    .locals 1

    .prologue
    .line 493
    const/16 v0, 0x32

    return v0
.end method

.method public hasLogo()Z
    .locals 2

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->getLogoResourceId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAutoCompleteSupported()Z
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x1

    return v0
.end method

.method public final performAutoCompleteSearch(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 2
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)V

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->handleExceptionAndSetRequest(Ljava/lang/Object;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    .line 85
    .local v0, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;>;"
    return-object v0
.end method

.method protected abstract performAutoCompleteSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public performBarcodeRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 2
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$7;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$7;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)V

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->handleException(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    .line 257
    .local v0, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;>;"
    return-object v0
.end method

.method protected abstract performBarcodeSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final performCategorySearch()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$4;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;)V

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->handleException(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    .line 165
    .local v0, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;>;"
    return-object v0
.end method

.method protected abstract performCategorySearchRealization()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final performExtraFoodInfoRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 2
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$6;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$6;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;)V

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->handleExceptionAndSetRequest(Ljava/lang/Object;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    .line 225
    .local v0, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;>;"
    return-object v0
.end method

.method protected abstract performExtraFoodInfoRequestRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final performFoodSearch(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 2
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 103
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$2;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)V

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->handleExceptionAndSetRequest(Ljava/lang/Object;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    .line 111
    .local v0, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;>;"
    return-object v0
.end method

.method public performFoodSearchByCategory(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 2
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 116
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$3;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;)V

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->handleExceptionAndSetRequest(Ljava/lang/Object;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    .line 123
    .local v0, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;>;"
    return-object v0
.end method

.method protected performFoodSearchByCategoryRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;
    .locals 5
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->getSubCategory()Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->getSubCategoryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->getPageNumber()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->getPerPageNumber()I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;-><init>(Ljava/lang/String;II)V

    .line 152
    .local v0, "searchListRequest":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->performFoodSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v1

    .line 153
    .local v1, "searchListResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;->getTotalCount()I

    move-result v4

    invoke-direct {v3, v2, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;-><init>(Ljava/util/List;I)V

    return-object v3
.end method

.method protected abstract performFoodSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final performSubCategorySearch(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 2
    .param p1, "foodSubCategoryRequest"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$5;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$5;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;)V

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->handleExceptionAndSetRequest(Ljava/lang/Object;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    .line 188
    .local v0, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;>;"
    return-object v0
.end method

.method protected abstract performSubCategorySearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method
