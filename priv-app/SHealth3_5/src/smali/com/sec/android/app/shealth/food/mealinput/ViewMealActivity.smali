.class public Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;
.super Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
.source "ViewMealActivity.java"


# static fields
.field private static final DRAWABLE_EDIT_BUTTON:I = 0x7f020299

.field private static MEAL_DATE_TIME_FORMAT:Ljava/lang/String; = null

.field private static final REQUEST_EDIT_MEAL_ACTIVITY:I = 0x64

.field private static final TTS_MEAL_DATE_TIME_FORMAT:Ljava/lang/String; = "yyyy/MM/dd"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-string v0, " HH:mm "

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->MEAL_DATE_TIME_FORMAT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;-><init>()V

    return-void
.end method

.method private initMealTimeHeader()V
    .locals 9

    .prologue
    .line 55
    const v5, 0x7f080416

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 56
    .local v1, "dateTimeText":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v5

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_0

    .line 57
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    check-cast v2, Ljava/text/SimpleDateFormat;

    .line 58
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/text/SimpleDateFormat;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/text/SimpleDateFormat;->toLocalizedPattern()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormatString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 59
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    .local v3, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "yyyy/MM/dd"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 60
    .local v4, "ttsSDF":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/Date;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 61
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 64
    .end local v0    # "date":Ljava/util/Date;
    .end local v3    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v4    # "ttsSDF":Ljava/text/SimpleDateFormat;
    :cond_0
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 90
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->customizeActionBar()V

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 92
    .local v0, "actionBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 93
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeCustomView()V

    .line 96
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v3

    invoke-static {p0, v3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealTypeString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 100
    .local v2, "title":Ljava/lang/String;
    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 101
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f020299

    const v4, 0x7f090040

    new-instance v5, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;)V

    invoke-direct {v1, v3, v6, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    .line 112
    .local v1, "edit":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-array v3, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v3, v6

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 116
    return-void
.end method

.method protected initMemo()V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 75
    const v5, 0x7f0803e5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 76
    .local v0, "memoTextView":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getComment()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getComment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    move v1, v3

    .line 78
    .local v1, "memoViewVisibility":I
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    const v5, 0x7f0803e6

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 80
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getComment()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v3, ""

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getComment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 81
    const v3, 0x7f0803e4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 82
    new-instance v2, Landroid/text/SpannableString;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getComment()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 83
    .local v2, "s":Landroid/text/SpannableString;
    new-instance v3, Landroid/text/style/LeadingMarginSpan$Standard;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a06e9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    invoke-direct {v3, v5, v4}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(II)V

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 84
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    .end local v2    # "s":Landroid/text/SpannableString;
    :cond_0
    return-void

    .end local v1    # "memoViewVisibility":I
    :cond_1
    move v1, v4

    .line 76
    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 120
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 121
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 122
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p3}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->setResult(ILandroid/content/Intent;)V

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->finish()V

    .line 125
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 145
    :try_start_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onBackPressed()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_0
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "ViewMealActivity"

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onMealItemsQuantityChanged(I)V
    .locals 0
    .param p1, "quantity"    # I

    .prologue
    .line 130
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->VIEW_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->setFragmentMode(Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V

    .line 47
    const v0, 0x7f0803d6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->initMealTimeHeader()V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v0

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealTypeString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 52
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 69
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onResume()V

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->initMealTimeHeader()V

    .line 71
    return-void
.end method

.method public prepareBundleData()Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 134
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 135
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "MEAL_DATA_HOLDER"

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 136
    const-string v1, "START_FROM_MEA_VIEW_ACTIVITY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 137
    const-string v1, "STARTED_FROM_FOOD_TRACKER"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "STARTED_FROM_FOOD_TRACKER"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 138
    return-object v0
.end method
