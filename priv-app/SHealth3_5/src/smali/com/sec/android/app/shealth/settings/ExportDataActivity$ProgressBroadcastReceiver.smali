.class Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ExportDataActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/ExportDataActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProgressBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 305
    if-nez p2, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 312
    const-string v1, "com.sec.android.app.shealth.DATA_EXPORT_PROGRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    const-string/jumbo v2, "progress_percent"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->data:I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$302(Lcom/sec/android/app/shealth/settings/ExportDataActivity;I)I

    .line 314
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$400(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->data:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$300(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    const v4, 0x7f090d15

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$500(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->data:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$300(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 316
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->data:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$300(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)I

    move-result v1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_0

    .line 317
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->exportHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$700(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->exportDataRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$600(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
