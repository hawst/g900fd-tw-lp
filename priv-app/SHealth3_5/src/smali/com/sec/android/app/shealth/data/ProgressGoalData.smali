.class public Lcom/sec/android/app/shealth/data/ProgressGoalData;
.super Ljava/lang/Object;
.source "ProgressGoalData.java"


# instance fields
.field private goalValue:J

.field private maxValue:F

.field private minValue:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method

.method public constructor <init>(JFF)V
    .locals 0
    .param p1, "goalValue"    # J
    .param p3, "minValue"    # F
    .param p4, "maxValue"    # F

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-wide p1, p0, Lcom/sec/android/app/shealth/data/ProgressGoalData;->goalValue:J

    .line 16
    iput p3, p0, Lcom/sec/android/app/shealth/data/ProgressGoalData;->minValue:F

    .line 17
    iput p4, p0, Lcom/sec/android/app/shealth/data/ProgressGoalData;->maxValue:F

    .line 18
    return-void
.end method


# virtual methods
.method public getGoalValue()J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/sec/android/app/shealth/data/ProgressGoalData;->goalValue:J

    return-wide v0
.end method

.method public getMaxValue()F
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/shealth/data/ProgressGoalData;->maxValue:F

    return v0
.end method

.method public getMinValue()F
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/data/ProgressGoalData;->minValue:F

    return v0
.end method

.method public setGoalValue(J)V
    .locals 0
    .param p1, "goalValue"    # J

    .prologue
    .line 25
    iput-wide p1, p0, Lcom/sec/android/app/shealth/data/ProgressGoalData;->goalValue:J

    .line 26
    return-void
.end method

.method public setMaxValue(F)V
    .locals 0
    .param p1, "maxValue"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/app/shealth/data/ProgressGoalData;->maxValue:F

    .line 42
    return-void
.end method

.method public setMinValue(F)V
    .locals 0
    .param p1, "minValue"    # F

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/app/shealth/data/ProgressGoalData;->minValue:F

    .line 34
    return-void
.end method
