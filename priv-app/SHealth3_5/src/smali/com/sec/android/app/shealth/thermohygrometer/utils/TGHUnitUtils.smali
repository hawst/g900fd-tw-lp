.class public Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;
.super Ljava/lang/Object;
.source "TGHUnitUtils.java"


# static fields
.field public static final CELSIUS_TO_FAHRENHEIT_MULTIPLIER:F = 1.8f

.field public static final CELSIUS_TO_FAHRENHEIT_SUMMAND:I = 0x20

.field public static final FAHRENHEIT_TO_CELSIUS_MULTIPLIER:F = 0.5555556f

.field public static final FAHRENHEIT_TO_CELSIUS_SUMMAND:I = -0x20


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convertCelsiusToFahrenheit(F)F
    .locals 2
    .param p0, "temperatureInCelsius"    # F

    .prologue
    .line 55
    const v0, 0x3fe66666    # 1.8f

    mul-float/2addr v0, p0

    const/high16 v1, 0x42000000    # 32.0f

    add-float/2addr v0, v1

    return v0
.end method

.method private static convertFahrenheitToCelsius(F)F
    .locals 2
    .param p0, "temperatureInFahrenheit"    # F

    .prologue
    .line 61
    const/high16 v0, -0x3e000000    # -32.0f

    add-float/2addr v0, p0

    const v1, 0x3f0e38e4

    mul-float/2addr v0, v1

    return v0
.end method

.method public static convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F
    .locals 3
    .param p0, "data"    # F
    .param p1, "inputUnit"    # Ljava/lang/String;
    .param p2, "outputUnit"    # Ljava/lang/String;

    .prologue
    .line 23
    const-string v0, "C"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "F"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Temperature unit should be C or F, but is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    const-string v0, "C"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "F"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Temperature unit should be C or F, but is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_1
    const-string v0, "C"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 36
    const-string v0, "C"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 51
    .end local p0    # "data":F
    :cond_2
    :goto_0
    return p0

    .line 39
    .restart local p0    # "data":F
    :cond_3
    const-string v0, "F"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 40
    invoke-static {p0}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertFahrenheitToCelsius(F)F

    move-result p0

    goto :goto_0

    .line 43
    :cond_4
    const-string v0, "F"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 44
    const-string v0, "F"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 47
    const-string v0, "C"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 48
    invoke-static {p0}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertCelsiusToFahrenheit(F)F

    move-result p0

    goto :goto_0

    .line 51
    :cond_5
    const/4 p0, 0x0

    goto :goto_0
.end method
