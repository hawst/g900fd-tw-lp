.class public final Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;
.super Ljava/lang/Object;
.source "ConfigurationConstants.java"


# static fields
.field public static final CHINA_AMAP_LOCATION_ON:Z

.field public static final CHINA_BACKUP_RESTORE_OFF:Z

.field public static CIGNA_ENABLED:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCurrentSettingSupportedByCigna()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CIGNA_ENABLED:Z

    .line 48
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    .line 52
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_BACKUP_RESTORE_OFF:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
