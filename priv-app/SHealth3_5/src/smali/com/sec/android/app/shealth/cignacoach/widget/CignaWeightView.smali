.class public Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
.super Landroid/widget/RelativeLayout;
.source "CignaWeightView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;,
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;
    }
.end annotation


# instance fields
.field private inputFilter:Landroid/text/InputFilter;

.field private mContext:Landroid/content/Context;

.field private mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

.field private mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

.field private mFocusHandler:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;

.field private mInputFilters:[Landroid/text/InputFilter;

.field private mIsWeightValueChanged:Z

.field private mLbMinValue:F

.field private mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

.field private mMaxValue:F

.field private mMinValue:F

.field private mPrevKgValue:F

.field private mPrevLbValue:F

.field private mSystemNumberSeparator:Ljava/lang/String;

.field private mWatcher:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;

.field private mWeightUnit:I

.field private mWeightValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const v0, 0x1fbd1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    .line 34
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMinValue:F

    .line 35
    const v0, 0x408ccccd    # 4.4f

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mLbMinValue:F

    .line 36
    const v0, 0x4479f99a    # 999.9f

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMaxValue:F

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mIsWeightValueChanged:Z

    .line 48
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mSystemNumberSeparator:Ljava/lang/String;

    .line 399
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->inputFilter:Landroid/text/InputFilter;

    .line 61
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mContext:Landroid/content/Context;

    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->initLayout()V

    .line 67
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevLbValue:F

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevLbValue:F

    return p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mIsWeightValueChanged:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevKgValue:F

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
    .param p1, "x1"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevKgValue:F

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setDatalimit(Ljava/lang/String;I)V

    return-void
.end method

.method private convertComma(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mSystemNumberSeparator:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v1, "."

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 197
    :cond_0
    return-object p1
.end method

.method private initLayout()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 75
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f030087

    invoke-static {v2, v3, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 77
    .local v0, "view":Landroid/view/View;
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;

    invoke-direct {v2, p0, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWatcher:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;

    .line 78
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;

    invoke-direct {v2, p0, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mFocusHandler:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;

    .line 80
    new-array v2, v4, [Landroid/text/InputFilter;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mInputFilters:[Landroid/text/InputFilter;

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mInputFilters:[Landroid/text/InputFilter;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->inputFilter:Landroid/text/InputFilter;

    aput-object v3, v2, v5

    .line 83
    const v2, 0x7f0802a4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mInputFilters:[Landroid/text/InputFilter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWatcher:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mFocusHandler:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 90
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f0901d1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f0901d5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 91
    .local v1, "weightUnits":[Ljava/lang/String;
    const v2, 0x7f0802a3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setDropDownVerticalOffset(I)V

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    const v3, 0x7f02086f

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setPopupBackgroundResource(I)V

    .line 94
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomBaseSpinnerAdapter;

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    const v6, 0x7f0301f1

    invoke-direct {v3, v4, v5, v6, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomBaseSpinnerAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;I[Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 161
    return-void
.end method

.method private setDatalimit(Ljava/lang/String;I)V
    .locals 5
    .param p1, "preData"    # Ljava/lang/String;
    .param p2, "prePos"    # I

    .prologue
    const v4, 0x1fbd1

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 303
    .local v1, "editData":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 304
    :cond_0
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    if-ne v2, v4, :cond_2

    .line 305
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMinValue:F

    .line 313
    .local v0, "data":F
    :goto_0
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    if-ne v2, v4, :cond_4

    .line 314
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMaxValue:F

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 315
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 316
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 324
    :cond_1
    :goto_1
    return-void

    .line 307
    .end local v0    # "data":F
    :cond_2
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMinValue:F

    const-string v3, "lb"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToUnit(FLjava/lang/String;)F

    move-result v0

    .restart local v0    # "data":F
    goto :goto_0

    .line 310
    .end local v0    # "data":F
    :cond_3
    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .restart local v0    # "data":F
    goto :goto_0

    .line 319
    :cond_4
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMaxValue:F

    const-string v3, "lb"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToUnit(FLjava/lang/String;)F

    move-result v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    goto :goto_1
.end method


# virtual methods
.method public checkValueText()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 419
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getValue()F

    move-result v0

    .line 421
    .local v0, "value":F
    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, v0, v2

    if-nez v2, :cond_1

    .line 439
    :cond_0
    :goto_0
    return v1

    .line 425
    :cond_1
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    const v3, 0x1fbd1

    if-ne v2, v3, :cond_3

    .line 427
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMinValue:F

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_0

    const/high16 v2, 0x43fa0000    # 500.0f

    cmpl-float v2, v0, v2

    if-gtz v2, :cond_0

    .line 439
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 433
    :cond_3
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mLbMinValue:F

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_0

    const v2, 0x4489c99a    # 1102.3f

    cmpl-float v2, v0, v2

    if-lez v2, :cond_2

    goto :goto_0
.end method

.method public getDropDownListValue()I
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method public getKgValue()F
    .locals 3

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getValue()F

    move-result v0

    .line 467
    .local v0, "value":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->checkValueText()Z

    move-result v1

    if-nez v1, :cond_0

    .line 468
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    if-eqz v1, :cond_0

    .line 469
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;->onCignaDialog(Landroid/view/View;)V

    .line 470
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setMinValueText()V

    .line 471
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->selectAll()V

    .line 472
    const/high16 v1, -0x40800000    # -1.0f

    .line 483
    :goto_0
    return v1

    .line 477
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    const v2, 0x1fbd1

    if-ne v1, v2, :cond_1

    .line 478
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    .line 483
    :goto_1
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    goto :goto_0

    .line 480
    :cond_1
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertLbToKg(F)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    goto :goto_1
.end method

.method public getValue()F
    .locals 2

    .prologue
    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 289
    .local v0, "value":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 291
    :cond_0
    const/high16 v1, -0x40800000    # -1.0f

    .line 294
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_0
.end method

.method public getWeight()F
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    return v0
.end method

.method public getWeightUnit()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    return v0
.end method

.method public setDropDownListValue(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setSelection(I)V

    .line 173
    return-void
.end method

.method public setMinValueText()V
    .locals 2

    .prologue
    .line 444
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    const v1, 0x1fbd1

    if-ne v0, v1, :cond_1

    .line 446
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getValue()F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMinValue:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMinValue:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 462
    :goto_0
    return-void

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const/high16 v1, 0x43fa0000    # 500.0f

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 454
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getValue()F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mLbMinValue:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mLbMinValue:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 457
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const v1, 0x4489c99a    # 1102.3f

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setOnCignaDialogListener(Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    .line 71
    return-void
.end method

.method public setValue(F)V
    .locals 5
    .param p1, "weight"    # F

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 217
    const/4 v0, 0x0

    .line 219
    .local v0, "weightVal":F
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setVisibility(I)V

    .line 221
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    const v2, 0x1fbd1

    if-ne v1, v2, :cond_2

    .line 222
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mIsWeightValueChanged:Z

    if-eqz v1, :cond_1

    .line 224
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mIsWeightValueChanged:Z

    .line 225
    invoke-static {p1, v4}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v0

    .line 226
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMinValue:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 228
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMinValue:F

    .line 230
    :cond_0
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevKgValue:F

    .line 254
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->convertComma(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    return-void

    .line 234
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevKgValue:F

    goto :goto_0

    .line 238
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mIsWeightValueChanged:Z

    if-eqz v1, :cond_4

    .line 240
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mIsWeightValueChanged:Z

    .line 241
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToLb(F)F

    move-result v1

    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v0

    .line 243
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mLbMinValue:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    .line 245
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mLbMinValue:F

    .line 247
    :cond_3
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevLbValue:F

    goto :goto_0

    .line 251
    :cond_4
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevLbValue:F

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 488
    const v0, 0x7f0802a4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 489
    const v0, 0x7f0802a3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 490
    return-void
.end method

.method public setWeightPreviousValue(F)V
    .locals 2
    .param p1, "prevWeight"    # F

    .prologue
    .line 202
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    .line 203
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevKgValue:F

    .line 204
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    const-string v1, "lb"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToUnit(FLjava/lang/String;)F

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->convertDecimalFormat(F)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->convertComma(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevLbValue:F

    .line 205
    return-void
.end method

.method public setWeightUnit(I)V
    .locals 0
    .param p1, "unit"    # I

    .prologue
    .line 184
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    .line 185
    return-void
.end method

.method public setWeightValue()V
    .locals 4

    .prologue
    .line 259
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 260
    :cond_0
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMinValue:F

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    .line 283
    :cond_1
    :goto_0
    return-void

    .line 262
    :cond_2
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I

    const v3, 0x1fbd1

    if-ne v2, v3, :cond_3

    .line 263
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v1

    .line 264
    .local v1, "roundedWeightValue":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 265
    .local v0, "currentWeightValue":F
    cmpl-float v2, v1, v0

    if-eqz v2, :cond_1

    .line 267
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    goto :goto_0

    .line 272
    .end local v0    # "currentWeightValue":F
    .end local v1    # "roundedWeightValue":F
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertLbToKg(F)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    .line 275
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMinValue:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_4

    .line 277
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mMinValue:F

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    .line 280
    :cond_4
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->ceilToDigit(FI)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F

    goto :goto_0
.end method
