.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;
.super Ljava/lang/Object;
.source "ExerciseProMaxHRModeSelectActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->showMaxHrWarningPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/16 v2, 0xf0

    const/16 v1, 0x64

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;I)I

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 362
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V

    .line 364
    invoke-virtual {p3}, Landroid/app/Dialog;->dismiss()V

    .line 365
    return-void

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)I

    move-result v0

    if-le v0, v2, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;I)I

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
