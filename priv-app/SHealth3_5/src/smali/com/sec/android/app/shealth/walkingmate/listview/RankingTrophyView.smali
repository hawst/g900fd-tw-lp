.class public Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;
.super Landroid/widget/FrameLayout;
.source "RankingTrophyView.java"


# instance fields
.field private rankingView:Landroid/widget/TextView;

.field private trophyView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->init()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->init()V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->init()V

    .line 35
    return-void
.end method

.method private init()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v5, -0x1

    const/4 v6, 0x0

    .line 38
    new-instance v3, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->trophyView:Landroid/widget/ImageView;

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0a87

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    .line 40
    .local v0, "IMAGE_SIZE":I
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 41
    .local v2, "trophyViewLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0a88

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 42
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->trophyView:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 43
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->trophyView:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 44
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->trophyView:Landroid/widget/ImageView;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->addView(Landroid/view/View;)V

    .line 45
    new-instance v3, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    .line 46
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 47
    .local v1, "rankingViewLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 48
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0a8b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v6, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 49
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07019a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 50
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 51
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    const v4, 0x7f020614

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 52
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    invoke-static {v4, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 53
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 54
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    invoke-virtual {v3, v6, v6, v6, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 55
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->addView(Landroid/view/View;)V

    .line 56
    return-void
.end method


# virtual methods
.method public setRanking(IZ)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "isMyRanking"    # Z

    .prologue
    .line 65
    const/4 v0, 0x2

    .line 66
    .local v0, "THIRD_POSITION":I
    const/4 v1, 0x2

    if-le p1, v1, :cond_1

    .line 67
    if-eqz p2, :cond_0

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    const v2, 0x7f020615

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->trophyView:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 74
    :cond_1
    return-void
.end method

.method public setTrophySrc(I)V
    .locals 2
    .param p1, "src"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->trophyView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->trophyView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->rankingView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 62
    return-void
.end method
