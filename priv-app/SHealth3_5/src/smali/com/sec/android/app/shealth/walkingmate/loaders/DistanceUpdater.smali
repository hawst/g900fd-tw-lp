.class public abstract Lcom/sec/android/app/shealth/walkingmate/loaders/DistanceUpdater;
.super Landroid/os/AsyncTask;
.source "DistanceUpdater.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Intent;",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private isSuccess:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/DistanceUpdater;->isSuccess:Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, [Landroid/content/Intent;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/loaders/DistanceUpdater;->doInBackground([Landroid/content/Intent;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/content/Intent;)Ljava/lang/Void;
    .locals 10
    .param p1, "params"    # [Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 19
    aget-object v5, p1, v9

    .line 20
    .local v5, "intent":Landroid/content/Intent;
    const/4 v1, 0x0

    .line 21
    .local v1, "attempts":I
    const-wide/16 v3, 0x0

    .line 22
    .local v3, "distance":J
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/loaders/DistanceUpdater;->isCancelled()Z

    move-result v7

    if-nez v7, :cond_0

    if-ge v1, v8, :cond_0

    iget-boolean v7, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/DistanceUpdater;->isSuccess:Z

    if-nez v7, :cond_0

    .line 24
    :try_start_0
    const-string v7, "access_token"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 25
    .local v0, "accessToken":Ljava/lang/String;
    const-string v7, "client_id"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 26
    .local v6, "userId":Ljava/lang/String;
    invoke-static {v6, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getTotalWalkingStats(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$TotalUserStatisticResponse;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$TotalUserStatisticResponse;->getTotalDistance()J

    move-result-wide v3

    .line 27
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/DistanceUpdater;->isSuccess:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    .end local v0    # "accessToken":Ljava/lang/String;
    .end local v6    # "userId":Ljava/lang/String;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 28
    :catch_0
    move-exception v2

    .line 29
    .local v2, "catchMeIfYouCan":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 30
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/DistanceUpdater;->isSuccess:Z

    goto :goto_1

    .line 34
    .end local v2    # "catchMeIfYouCan":Ljava/lang/Exception;
    :cond_0
    new-array v7, v8, [Ljava/lang/Long;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/walkingmate/loaders/DistanceUpdater;->publishProgress([Ljava/lang/Object;)V

    .line 35
    const/4 v7, 0x0

    return-object v7
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Long;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/Long;

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 41
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/DistanceUpdater;->isSuccess:Z

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/loaders/DistanceUpdater;->onUpdate(JZ)V

    .line 42
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, [Ljava/lang/Long;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/loaders/DistanceUpdater;->onProgressUpdate([Ljava/lang/Long;)V

    return-void
.end method

.method public abstract onUpdate(JZ)V
.end method
