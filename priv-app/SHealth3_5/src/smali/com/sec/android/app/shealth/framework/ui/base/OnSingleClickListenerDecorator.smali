.class public Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListenerDecorator;
.super Lcom/sec/android/app/shealth/framework/ui/base/OnClickListenerDecorator;


# static fields
.field private static final DEFAULT_TIMEOUT:I = 0x1f4

.field private static mLastTime:J

.field private static mLastViewRootHash:I


# direct methods
.method public constructor <init>(Landroid/view/View$OnClickListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/OnClickListenerDecorator;-><init>(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 14

    const-wide/16 v12, 0x0

    const-wide v10, 0x1833ec865f4634d4L

    const/4 v9, 0x1

    const/16 v8, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v9

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v4, v2, v1

    long-to-int v1, v4

    if-gtz v1, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, v0

    shl-long/2addr v0, v8

    ushr-long v4, v0, v8

    aget-wide v0, v2, v3

    cmp-long v6, v0, v12

    if-eqz v6, :cond_1

    xor-long/2addr v0, v10

    :cond_1
    ushr-long/2addr v0, v8

    shl-long/2addr v0, v8

    xor-long/2addr v0, v4

    xor-long/2addr v0, v10

    aput-wide v0, v2, v3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sget-wide v0, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListenerDecorator;->mLastTime:J

    const-wide/16 v6, 0x1f4

    add-long/2addr v0, v6

    cmp-long v0, v0, v4

    if-ltz v0, :cond_6

    sget v6, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListenerDecorator;->mLastViewRootHash:I

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v9, [I

    const/16 v0, 0x741c

    aput v0, v1, v3

    new-array v0, v9, [I

    const/16 v2, 0x742c

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v7, v0, v12

    if-eqz v7, :cond_5

    xor-long/2addr v0, v10

    :cond_5
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    if-eq v6, v0, :cond_b

    :cond_6
    sput-wide v4, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListenerDecorator;->mLastTime:J

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_9

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v9, [I

    const/16 v0, 0x1928

    aput v0, v1, v3

    new-array v0, v9, [I

    const/16 v2, 0x1918

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v5, v0

    if-lt v2, v5, :cond_7

    array-length v0, v1

    new-array v0, v0, [C

    :goto_3
    array-length v2, v0

    if-lt v3, v2, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_7
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_8
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_9
    aget-wide v0, v2, v3

    cmp-long v2, v0, v12

    if-eqz v2, :cond_a

    xor-long/2addr v0, v10

    :cond_a
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    sput v0, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListenerDecorator;->mLastViewRootHash:I

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListenerDecorator;->mListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_b
    return-void
.end method
