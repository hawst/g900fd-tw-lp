.class public Lcom/sec/android/app/shealth/receiver/SyncedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SyncedReceiver.java"


# static fields
.field private static final HEALTH_PROFILE_UPDATED:Ljava/lang/String; = "com.samsung.android.sdk.health.sensor.action.PROFILE_UPDATED"

.field private static final TAG:Ljava/lang/String; = "SyncedReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 49
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.samsung.android.sdk.health.sensor.action.PROFILE_UPDATED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    const-string v2, "SyncedReceiver"

    const-string v3, "Receive HEALTH_PROFILE_UPDATED"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-static {p1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isSHealthUpgradeNeeded(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 61
    const-string v2, "SyncedReceiver"

    const-string v3, "isUpgradeNeeded = true"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 65
    :cond_2
    invoke-static {p1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isHealthServiceUpgradeNeeded(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 67
    const-string v2, "SyncedReceiver"

    const-string v3, "isHealthServiceOld = true"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 84
    :cond_3
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setLastUpdated(Landroid/content/Context;)V

    .line 85
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 86
    .local v0, "shealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-direct {v1, p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 88
    .local v1, "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->saveUnitforUnitHelper(II)V

    .line 90
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getDistanceUnit()I

    move-result v2

    const v3, 0x29811

    if-ne v2, v3, :cond_4

    .line 91
    const-string v2, "km"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putDistanceUnit(Ljava/lang/String;)V

    .line 94
    :goto_1
    const-string v2, "SyncedReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Receive Heignt Unit : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Weignt Unit : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", DistanceUnit : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getDistanceUnit()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 93
    :cond_4
    const-string/jumbo v2, "mi"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putDistanceUnit(Ljava/lang/String;)V

    goto :goto_1
.end method
