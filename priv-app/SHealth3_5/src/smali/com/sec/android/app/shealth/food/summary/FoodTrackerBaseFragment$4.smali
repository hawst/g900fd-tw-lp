.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$4;
.super Ljava/lang/Object;
.source "FoodTrackerBaseFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V
    .locals 0

    .prologue
    .line 770
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$4;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 4
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 775
    sget-object v2, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne v2, p1, :cond_0

    .line 777
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$4;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    const v3, 0x7f08047e

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 778
    .local v0, "calorieEdit":Landroid/widget/EditText;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 779
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$4;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->showMinMaxToast()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$500(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    .line 780
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 781
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 782
    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 783
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$4;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 806
    .end local v0    # "calorieEdit":Landroid/widget/EditText;
    :cond_0
    :goto_0
    return-void

    .line 788
    .restart local v0    # "calorieEdit":Landroid/widget/EditText;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$4;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 790
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$4;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "quick_input_poup"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    .line 791
    .local v1, "quickInputDialog":Landroid/support/v4/app/DialogFragment;
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$4;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 793
    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 796
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$4;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->updateQuickInput()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$600(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    goto :goto_0
.end method
