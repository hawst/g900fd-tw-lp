.class Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 40

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x282489481cf95298L    # -1.690822792558037E115

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x282489481cf95298L    # -1.690822792558037E115

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    const/16 v1, 0x1d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, -0x44d1

    aput v31, v2, v30

    const/16 v30, -0x31

    aput v30, v2, v29

    const/16 v29, -0x31

    aput v29, v2, v28

    const/16 v28, 0x1022

    aput v28, v2, v27

    const/16 v27, -0x7187

    aput v27, v2, v26

    const/16 v26, -0x6

    aput v26, v2, v25

    const/16 v25, -0x42

    aput v25, v2, v24

    const/16 v24, -0x66

    aput v24, v2, v23

    const/16 v23, -0x22

    aput v23, v2, v22

    const/16 v22, 0x7139

    aput v22, v2, v21

    const/16 v21, 0x1f

    aput v21, v2, v20

    const/16 v20, -0x4c97

    aput v20, v2, v19

    const/16 v19, -0x39

    aput v19, v2, v18

    const/16 v18, -0x62

    aput v18, v2, v17

    const/16 v17, -0x2bc0

    aput v17, v2, v16

    const/16 v16, -0x79

    aput v16, v2, v15

    const/16 v15, 0x5352

    aput v15, v2, v14

    const/16 v14, -0x3ca

    aput v14, v2, v13

    const/16 v13, -0x61

    aput v13, v2, v12

    const/16 v12, 0x1b09

    aput v12, v2, v11

    const/16 v11, 0x106d

    aput v11, v2, v10

    const/16 v10, -0x458b

    aput v10, v2, v9

    const/4 v9, -0x2

    aput v9, v2, v8

    const/16 v8, -0x53f1

    aput v8, v2, v7

    const/16 v7, -0x37

    aput v7, v2, v6

    const/16 v6, 0x7f1d

    aput v6, v2, v5

    const/16 v5, 0x3016

    aput v5, v2, v4

    const/16 v4, -0x68af

    aput v4, v2, v3

    const/16 v3, -0x39

    aput v3, v2, v1

    const/16 v1, 0x1d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, -0x44aa

    aput v32, v1, v31

    const/16 v31, -0x45

    aput v31, v1, v30

    const/16 v30, -0x5a

    aput v30, v1, v29

    const/16 v29, 0x1054

    aput v29, v1, v28

    const/16 v28, -0x71f0

    aput v28, v1, v27

    const/16 v27, -0x72

    aput v27, v1, v26

    const/16 v26, -0x23

    aput v26, v1, v25

    const/16 v25, -0x25

    aput v25, v1, v24

    const/16 v24, -0x53

    aput v24, v1, v23

    const/16 v23, 0x715e

    aput v23, v1, v22

    const/16 v22, 0x71

    aput v22, v1, v21

    const/16 v21, -0x4d00

    aput v21, v1, v20

    const/16 v20, -0x4d

    aput v20, v1, v19

    const/16 v19, -0x16

    aput v19, v1, v18

    const/16 v18, -0x2bdb

    aput v18, v1, v17

    const/16 v17, -0x2c

    aput v17, v1, v16

    const/16 v16, 0x5321

    aput v16, v1, v15

    const/16 v15, -0x3ad

    aput v15, v1, v14

    const/4 v14, -0x4

    aput v14, v1, v13

    const/16 v13, 0x1b60

    aput v13, v1, v12

    const/16 v12, 0x101b

    aput v12, v1, v11

    const/16 v11, -0x45f0

    aput v11, v1, v10

    const/16 v10, -0x46

    aput v10, v1, v9

    const/16 v9, -0x5395

    aput v9, v1, v8

    const/16 v8, -0x54

    aput v8, v1, v7

    const/16 v7, 0x7f6f

    aput v7, v1, v6

    const/16 v6, 0x307f

    aput v6, v1, v5

    const/16 v5, -0x68d0

    aput v5, v1, v4

    const/16 v4, -0x69

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x13

    aput v38, v2, v37

    const/16 v37, -0x13

    aput v37, v2, v36

    const/16 v36, -0x6a92

    aput v36, v2, v35

    const/16 v35, -0xa

    aput v35, v2, v34

    const/16 v34, -0x36e8

    aput v34, v2, v33

    const/16 v33, -0x59

    aput v33, v2, v32

    const/16 v32, -0x6d

    aput v32, v2, v31

    const/16 v31, -0x59

    aput v31, v2, v30

    const/16 v30, -0x4ccc

    aput v30, v2, v29

    const/16 v29, -0x6d

    aput v29, v2, v28

    const/16 v28, -0x61

    aput v28, v2, v27

    const/16 v27, -0x3ea4

    aput v27, v2, v26

    const/16 v26, -0x58

    aput v26, v2, v25

    const/16 v25, -0x6

    aput v25, v2, v24

    const/16 v24, -0x78

    aput v24, v2, v23

    const/16 v23, -0x22

    aput v23, v2, v22

    const/16 v22, -0x45

    aput v22, v2, v21

    const/16 v21, -0x7d

    aput v21, v2, v20

    const/16 v20, -0x52

    aput v20, v2, v19

    const/16 v19, -0x4

    aput v19, v2, v18

    const/16 v18, 0x4a4f

    aput v18, v2, v17

    const/16 v17, -0x4dc

    aput v17, v2, v16

    const/16 v16, -0x6c

    aput v16, v2, v15

    const/16 v15, 0x707e

    aput v15, v2, v14

    const/16 v14, -0x7bb0

    aput v14, v2, v13

    const/16 v13, -0x9

    aput v13, v2, v12

    const/16 v12, 0x2d11

    aput v12, v2, v11

    const/16 v11, 0x170d

    aput v11, v2, v10

    const/16 v10, 0x870

    aput v10, v2, v9

    const/16 v9, -0x7e9a

    aput v9, v2, v8

    const/16 v8, -0x18

    aput v8, v2, v7

    const/16 v7, 0x417b

    aput v7, v2, v6

    const/16 v6, -0x15d1

    aput v6, v2, v5

    const/16 v5, -0x7d

    aput v5, v2, v3

    const/16 v3, -0x5df2

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x77

    aput v39, v1, v38

    const/16 v38, -0x78

    aput v38, v1, v37

    const/16 v37, -0x6ae6

    aput v37, v1, v36

    const/16 v36, -0x6b

    aput v36, v1, v35

    const/16 v35, -0x3683

    aput v35, v1, v34

    const/16 v34, -0x37

    aput v34, v1, v33

    const/16 v33, -0x3

    aput v33, v1, v32

    const/16 v32, -0x38

    aput v32, v1, v31

    const/16 v31, -0x4ca9

    aput v31, v1, v30

    const/16 v30, -0x4d

    aput v30, v1, v29

    const/16 v29, -0x6

    aput v29, v1, v28

    const/16 v28, -0x3ec1

    aput v28, v1, v27

    const/16 v27, -0x3f

    aput v27, v1, v26

    const/16 v26, -0x74

    aput v26, v1, v25

    const/16 v25, -0x6

    aput v25, v1, v24

    const/16 v24, -0x45

    aput v24, v1, v23

    const/16 v23, -0x18

    aput v23, v1, v22

    const/16 v22, -0x5d

    aput v22, v1, v21

    const/16 v21, -0x7d

    aput v21, v1, v20

    const/16 v20, -0x24

    aput v20, v1, v19

    const/16 v19, 0x4a2a

    aput v19, v1, v18

    const/16 v18, -0x4b6

    aput v18, v1, v17

    const/16 v17, -0x5

    aput v17, v1, v16

    const/16 v16, 0x701a

    aput v16, v1, v15

    const/16 v15, -0x7b90

    aput v15, v1, v14

    const/16 v14, -0x7c

    aput v14, v1, v13

    const/16 v13, 0x2d78

    aput v13, v1, v12

    const/16 v12, 0x172d

    aput v12, v1, v11

    const/16 v11, 0x817

    aput v11, v1, v10

    const/16 v10, -0x7ef8

    aput v10, v1, v9

    const/16 v9, -0x7f

    aput v9, v1, v8

    const/16 v8, 0x411f

    aput v8, v1, v7

    const/16 v7, -0x15bf

    aput v7, v1, v6

    const/16 v6, -0x16

    aput v6, v1, v5

    const/16 v5, -0x5db4

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->requestPairedDevice()V

    return-void

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method public onServiceDisconnected(I)V
    .locals 33

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x59d6449ce3af919bL    # -7.602650588790266E-125

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x59d6449ce3af919bL    # -7.602650588790266E-125

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    const/16 v1, 0x1d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x154f

    aput v31, v2, v30

    const/16 v30, 0x5161

    aput v30, v2, v29

    const/16 v29, -0x2ec8

    aput v29, v2, v28

    const/16 v28, -0x59

    aput v28, v2, v27

    const/16 v27, 0x184e

    aput v27, v2, v26

    const/16 v26, 0xb6c

    aput v26, v2, v25

    const/16 v25, 0x5668

    aput v25, v2, v24

    const/16 v24, 0x7117

    aput v24, v2, v23

    const/16 v23, 0x2902

    aput v23, v2, v22

    const/16 v22, -0xb2

    aput v22, v2, v21

    const/16 v21, -0x6f

    aput v21, v2, v20

    const/16 v20, -0x4f

    aput v20, v2, v19

    const/16 v19, -0x2e

    aput v19, v2, v18

    const/16 v18, -0x6691

    aput v18, v2, v17

    const/16 v17, -0x4

    aput v17, v2, v16

    const/16 v16, 0x2721

    aput v16, v2, v15

    const/16 v15, -0x6eac

    aput v15, v2, v14

    const/16 v14, -0xc

    aput v14, v2, v13

    const/4 v13, -0x1

    aput v13, v2, v12

    const/16 v12, -0x45

    aput v12, v2, v11

    const/16 v11, 0x784b

    aput v11, v2, v10

    const/16 v10, -0x26e3

    aput v10, v2, v9

    const/16 v9, -0x63

    aput v9, v2, v8

    const/16 v8, -0x7d90

    aput v8, v2, v7

    const/16 v7, -0x19

    aput v7, v2, v6

    const/16 v6, -0x2f91

    aput v6, v2, v5

    const/16 v5, -0x47

    aput v5, v2, v4

    const/16 v4, -0x5e

    aput v4, v2, v3

    const/16 v3, 0x7863

    aput v3, v2, v1

    const/16 v1, 0x1d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1536

    aput v32, v1, v31

    const/16 v31, 0x5115

    aput v31, v1, v30

    const/16 v30, -0x2eaf

    aput v30, v1, v29

    const/16 v29, -0x2f

    aput v29, v1, v28

    const/16 v28, 0x1827

    aput v28, v1, v27

    const/16 v27, 0xb18

    aput v27, v1, v26

    const/16 v26, 0x560b

    aput v26, v1, v25

    const/16 v25, 0x7156

    aput v25, v1, v24

    const/16 v24, 0x2971

    aput v24, v1, v23

    const/16 v23, -0xd7

    aput v23, v1, v22

    const/16 v22, -0x1

    aput v22, v1, v21

    const/16 v21, -0x28

    aput v21, v1, v20

    const/16 v20, -0x5a

    aput v20, v1, v19

    const/16 v19, -0x66e5

    aput v19, v1, v18

    const/16 v18, -0x67

    aput v18, v1, v17

    const/16 v17, 0x2772

    aput v17, v1, v16

    const/16 v16, -0x6ed9

    aput v16, v1, v15

    const/16 v15, -0x6f

    aput v15, v1, v14

    const/16 v14, -0x64

    aput v14, v1, v13

    const/16 v13, -0x2e

    aput v13, v1, v12

    const/16 v12, 0x783d

    aput v12, v1, v11

    const/16 v11, -0x2688

    aput v11, v1, v10

    const/16 v10, -0x27

    aput v10, v1, v9

    const/16 v9, -0x7dec

    aput v9, v1, v8

    const/16 v8, -0x7e

    aput v8, v1, v7

    const/16 v7, -0x2fe3

    aput v7, v1, v6

    const/16 v6, -0x30

    aput v6, v1, v5

    const/16 v5, -0x3d

    aput v5, v1, v4

    const/16 v4, 0x7833

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, -0x7f

    aput v23, v2, v22

    const/16 v22, 0x4b4e

    aput v22, v2, v21

    const/16 v21, -0x3ec1

    aput v21, v2, v20

    const/16 v20, -0x5e

    aput v20, v2, v19

    const/16 v19, -0x7897

    aput v19, v2, v18

    const/16 v18, -0x17

    aput v18, v2, v17

    const/16 v17, -0x3e

    aput v17, v2, v16

    const/16 v16, -0x5e

    aput v16, v2, v15

    const/4 v15, -0x7

    aput v15, v2, v14

    const/16 v14, -0x15

    aput v14, v2, v13

    const/16 v13, 0x3a00

    aput v13, v2, v12

    const/16 v12, 0x535e

    aput v12, v2, v11

    const/16 v11, 0x3173

    aput v11, v2, v10

    const/16 v10, 0x3f54

    aput v10, v2, v9

    const/16 v9, -0x1ca4

    aput v9, v2, v8

    const/16 v8, -0x76

    aput v8, v2, v7

    const/16 v7, -0x9c7

    aput v7, v2, v6

    const/16 v6, -0x7c

    aput v6, v2, v5

    const/16 v5, -0x129d

    aput v5, v2, v3

    const/16 v3, -0x42

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0x1b

    aput v24, v1, v23

    const/16 v23, 0x4b2b

    aput v23, v1, v22

    const/16 v22, -0x3eb5

    aput v22, v1, v21

    const/16 v21, -0x3f

    aput v21, v1, v20

    const/16 v20, -0x78f4

    aput v20, v1, v19

    const/16 v19, -0x79

    aput v19, v1, v18

    const/16 v18, -0x54

    aput v18, v1, v17

    const/16 v17, -0x33

    aput v17, v1, v16

    const/16 v16, -0x66

    aput v16, v1, v15

    const/16 v15, -0x68

    aput v15, v1, v14

    const/16 v14, 0x3a69

    aput v14, v1, v13

    const/16 v13, 0x533a

    aput v13, v1, v12

    const/16 v12, 0x3153

    aput v12, v1, v11

    const/16 v11, 0x3f31

    aput v11, v1, v10

    const/16 v10, -0x1cc1

    aput v10, v1, v9

    const/16 v9, -0x1d

    aput v9, v1, v8

    const/16 v8, -0x9b1

    aput v8, v1, v7

    const/16 v7, -0xa

    aput v7, v1, v6

    const/16 v6, -0x12fa

    aput v6, v1, v5

    const/16 v5, -0x13

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->clearController()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$202(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    return-void

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method
