.class Lcom/sec/android/app/shealth/common/utils/NetworkUtils$1;
.super Ljava/lang/Object;
.source "NetworkUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->setAirplaneModeEnabled(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

.field final synthetic val$enabled:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/utils/NetworkUtils;Z)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils$1;->this$0:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils$1;->val$enabled:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 202
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils$1;->this$0:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    # getter for: Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->access$000(Lcom/sec/android/app/shealth/common/utils/NetworkUtils;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "airplane_mode_on"

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils$1;->val$enabled:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v1

    .line 207
    .local v1, "state":Z
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 208
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x20000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 209
    const-string/jumbo v2, "state"

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils$1;->val$enabled:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 210
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils$1;->this$0:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    # getter for: Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->access$000(Lcom/sec/android/app/shealth/common/utils/NetworkUtils;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 211
    return-void

    .line 202
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "state":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
