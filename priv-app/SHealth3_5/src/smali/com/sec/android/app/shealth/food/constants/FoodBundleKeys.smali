.class public final Lcom/sec/android/app/shealth/food/constants/FoodBundleKeys;
.super Ljava/lang/Object;
.source "FoodBundleKeys.java"


# static fields
.field public static final ACTION_BAR_TITLE:Ljava/lang/String; = "ACTION_BAR_TITLE"

.field public static final ADD_CUSTOM_FOOD_LAYOUT_IS_VISIBLE:Ljava/lang/String; = "ADD_CUSTOM_FOOD_LAYOUT_IS_VISIBLE"

.field public static final BAR_CODE_RESULT_FOOD_INFO:Ljava/lang/String; = "BAR_CODE_RESULT_FOOD_INFO"

.field public static final CALLING_ACTIVITY_CLASS:Ljava/lang/String; = "CALLING_ACTIVITY_CLASS"

.field public static final CATEGORY_HOLDERS_LIST:Ljava/lang/String; = "CATEGORY_HOLDERS_LIST"

.field public static final CURRENT_FOOD_PICK_FRAGMENT_TAG:Ljava/lang/String; = "CURRENT_FOOD_PICK_FRAGMENT_TAG"

.field public static final EDIT_MEAL_BACK_PRESS_SAVE:Ljava/lang/String; = "edit_meal_back_press_save"

.field public static final FAT_SECRET_RESULT_KEY:Ljava/lang/String; = "FAT_SECRET_RESULT_KEY"

.field public static final FOOD_NAME_NOT_FOUND:Ljava/lang/String; = "FOOD_NAME_NOT_FOUND"

.field public static final FOOD_TRACKER_BASE_IMAGE_DATA_FROM_CAMERA:Ljava/lang/String; = "FOOD_TRACKER_BASE_DATA_FROM_CAMERA"

.field public static final FOOD_TRACKER_INIT_TIME:Ljava/lang/String; = "FOOD_TRACKER_INIT_TIME"

.field public static final GALLERY_IMAGE_PATHS:Ljava/lang/String; = "gallery_image_paths"

.field public static final GALLERY_SELECTED_IMAGE_PATH:Ljava/lang/String; = "gallery_selected_image_path"

.field public static final LIST_FOOD_INFO:Ljava/lang/String; = "LIST_FOOD_INFO"

.field public static final MAX_ACCEPTABLE_CALORIES_VALUE:Ljava/lang/String; = "MAX_ACCEPTABLE_CALORIES_VALUE"

.field public static final MEAL_DATA_HOLDER:Ljava/lang/String; = "MEAL_DATA_HOLDER"

.field public static final MEAL_ID:Ljava/lang/String; = "MEAL_ID"

.field public static final MEAL_ID_THAT_WAS_ADDED:Ljava/lang/String; = "MEAL_ID_THAT_WAS_ADDED"

.field public static final MEAL_ITEMS_COUNT:Ljava/lang/String; = "MEAL_ITEMS_COUNT"

.field public static final MEAL_ITEM_LIST:Ljava/lang/String; = "MEAL_ITEM_LIST"

.field public static final MEAL_PLAN_CANCELED_TIME_BUNDLE_KEY:Ljava/lang/String; = "MEAL_PLAN_CANCELED_TIME_BUNDLE_KEY"

.field public static final MEAL_PLAN_ID_BUNDLE_KEY:Ljava/lang/String; = "MEAL_PLAN_ID_BUNDLE_KEY"

.field public static final MEAL_TIME:Ljava/lang/String; = "MEAL_TIME"

.field public static final MEAL_TYPE:Ljava/lang/String; = "MEAL_TYPE"

.field public static final MY_FOOD_INFO_DATA_ID:Ljava/lang/String; = "MY_FOOD_INFO_DATA_ID"

.field public static final NO_DATA_LAYOUT_DESCRIPTION_TEXT_ID:Ljava/lang/String; = "NO_DATA_LAYOUT_DESCRIPTION_TEXT_ID"

.field public static final NUTRITION_DATA:Ljava/lang/String; = "NUTRITION_DATA"

.field public static final PORTION_SIZE_HOLDER:Ljava/lang/String; = "PORTION_SIZE_HOLDER"

.field public static final QUANTITY_INPUT_KEY:Ljava/lang/String; = "QUANTITY_INPUT"

.field public static final QUICK_INPUT_MEAL_TYPE:Ljava/lang/String; = "QUICK_INPUT_MEAL_TYPE"

.field public static final QUICK_INPUT_SELECTED_INDEX:Ljava/lang/String; = "QUICK_INPUT_SELECTED_INDEX"

.field public static final RESULT_RECEIVER_TAG:Ljava/lang/String; = "RESULT_RECEIVER_TAG"

.field public static final SHOULD_ALWAYS_SAVE_TO_MY_FOOD:Ljava/lang/String; = "SHOULD_ALWAYS_SAVE_TO_MY_FOOD"

.field public static final SINGLE_CATEGORY_TITLE_VISIBLE:Ljava/lang/String; = "single_category_title_visible"

.field public static final STARTED_FROM_FOOD_TRACKER:Ljava/lang/String; = "STARTED_FROM_FOOD_TRACKER"

.field public static final START_FROM_FOOD_TRACKER_BASE_FRAGMENT:Ljava/lang/String; = "START_FROM_FOOD_TRACKER_BASE_FRAGMENT"

.field public static final START_FROM_FOOD_TRACKER_BASE_FRAGMENT_VALUE:Ljava/lang/String; = "START_FROM_FOOD_TRACKER_BASE_FRAGMENT_VALUE"

.field public static final START_FROM_VIEW_MEAL_ACTIVITY:Ljava/lang/String; = "START_FROM_MEA_VIEW_ACTIVITY"

.field public static final START_TYPE:Ljava/lang/String; = "START_TYPE"

.field public static final STATE_HANDLER_KEY:Ljava/lang/String; = "STATE_HANDLER"

.field public static final TIME_OF_CURRENT_DATA:Ljava/lang/String; = "TIME_OF_CURRENT_DATA"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    return-void
.end method
