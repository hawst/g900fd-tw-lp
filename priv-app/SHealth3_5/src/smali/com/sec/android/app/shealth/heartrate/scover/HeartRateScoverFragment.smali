.class public Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "HeartRateScoverFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$23;,
        Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;,
        Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;
    }
.end annotation


# static fields
.field private static final DELAYTIME_CHECK_SENSOR_DETECT:I = 0x96

.field private static final MEASURE_FAIL_MAX_COUNT:I = 0x0

.field private static final PULSE_MODE_DELAYE_TIME:I = 0x370

.field private static final PULSE_MODE_DRAWING:I = 0x0

.field private static final PULSE_MODE_ERASER:I = 0x1

.field public static final TAG:Ljava/lang/String;

.field private static effectAudio:[I


# instance fields
.field private isEndPulseDelay:Z

.field private isFingerDetected:Z

.field private isRetryMeasuring:Z

.field private mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mCancelDialog:Landroid/widget/Button;

.field private mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

.field private mContext:Landroid/content/Context;

.field private mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

.field private mErrorType:I

.field private mFirstMessage:Landroid/widget/TextView;

.field private mFirstStatus:Landroid/widget/TextView;

.field public mHandler:Landroid/os/Handler;

.field private mHeartRate:I

.field mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mHrmScoverFragmentHandler:Landroid/os/Handler;

.field private mIsGreen:Z

.field private mIsOnConfigChanged:Z

.field private mIsRunPulseView:Z

.field private mMainView:Landroid/view/View;

.field private mMeasureFailCount:I

.field private mPrevError:I

.field private mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;

.field private mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

.field private mScaleAnimation:Landroid/view/animation/Animation;

.field private mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mSecondBpm:Landroid/widget/TextView;

.field private mSecondBpmUnit:Landroid/widget/TextView;

.field private mSecondCenterIcon:Landroid/widget/ImageView;

.field private mSecondCenterIconBig:Landroid/widget/ImageView;

.field private mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

.field private mSecondCenterPulseViewGreenIcon:Landroid/widget/ImageView;

.field private mSecondCenterPulseViewIcon:Landroid/widget/ImageView;

.field private mSecondCenterPulseViewLayout:Landroid/widget/LinearLayout;

.field private mSecondCenterPulseViewMask:Landroid/widget/ImageView;

.field private mSecondCenterPulseViewOrangeIcon:Landroid/widget/ImageView;

.field private mSecondIcon:Landroid/widget/ImageView;

.field mSecondLayout:Landroid/widget/FrameLayout;

.field private mSecondRetry:Landroid/widget/LinearLayout;

.field private mSensorErrorTimer:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;

.field private mStartButton:Landroid/widget/TextView;

.field private pool:Landroid/media/SoundPool;

.field public pulseHandler:Landroid/os/Handler;

.field private readyTimer:Landroid/os/CountDownTimer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const-class v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    .line 91
    const/4 v0, 0x5

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->effectAudio:[I

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V
    .locals 6
    .param p1, "_FragmentHandler"    # Landroid/os/Handler;
    .param p2, "_heartRateScoverActivity"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .prologue
    const/4 v0, 0x0

    .line 201
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 81
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isFingerDetected:Z

    .line 82
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isRetryMeasuring:Z

    .line 83
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsRunPulseView:Z

    .line 84
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsGreen:Z

    .line 85
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isEndPulseDelay:Z

    .line 86
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsOnConfigChanged:Z

    .line 92
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mMeasureFailCount:I

    .line 97
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    .line 120
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->getsCoverInstance()Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .line 122
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0xc8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSensorErrorTimer:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;

    .line 137
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$1;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHandler:Landroid/os/Handler;

    .line 168
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$2;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pulseHandler:Landroid/os/Handler;

    .line 882
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$11;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x64

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$11;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->readyTimer:Landroid/os/CountDownTimer;

    .line 202
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHrmScoverFragmentHandler:Landroid/os/Handler;

    .line 204
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V
    .locals 6
    .param p1, "_heartRateScoverActivity"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .prologue
    const/4 v0, 0x0

    .line 196
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 81
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isFingerDetected:Z

    .line 82
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isRetryMeasuring:Z

    .line 83
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsRunPulseView:Z

    .line 84
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsGreen:Z

    .line 85
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isEndPulseDelay:Z

    .line 86
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsOnConfigChanged:Z

    .line 92
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mMeasureFailCount:I

    .line 97
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    .line 120
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->getsCoverInstance()Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .line 122
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0xc8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSensorErrorTimer:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;

    .line 137
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$1;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHandler:Landroid/os/Handler;

    .line 168
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$2;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pulseHandler:Landroid/os/Handler;

    .line 882
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$11;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x64

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$11;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->readyTimer:Landroid/os/CountDownTimer;

    .line 198
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .line 199
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/FrameLayout;)Landroid/widget/FrameLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/FrameLayout;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPrevError:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewLayout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewIcon:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewGreenIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewGreenIcon:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewOrangeIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewOrangeIcon:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewMask:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewMask:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCancelDialog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/Button;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCancelDialog:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHrmScoverFragmentHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->setReadyUI(I)V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpmUnit:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpmUnit:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$2302(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mStartButton:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$2400()[I
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->effectAudio:[I

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->playSound(I)V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/heartrate/PulseView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Lcom/sec/android/app/shealth/heartrate/PulseView;)Lcom/sec/android/app/shealth/heartrate/PulseView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/heartrate/PulseView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;

    return-object p1
.end method

.method static synthetic access$2702(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;)Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    return-object p1
.end method

.method static synthetic access$2802(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->initAoudioFile()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->setMeasuringUI(I)V

    return-void
.end method

.method static synthetic access$3002(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/view/animation/Animation;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsRunPulseView:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsRunPulseView:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsOnConfigChanged:Z

    return v0
.end method

.method static synthetic access$3202(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsOnConfigChanged:Z

    return p1
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isFingerDetected:Z

    return v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->readyTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSensorErrorTimer:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsGreen:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->setMeasuringFailUI(I)V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pulseAnimationShow(Z)V

    return-void
.end method

.method static synthetic access$4100(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/media/SoundPool;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pool:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isRetryMeasuring:Z

    return v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->changeUIReadyToMeasuring(Z)V

    return-void
.end method

.method static synthetic access$4400(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mErrorType:I

    return v0
.end method

.method static synthetic access$4602(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isEndPulseDelay:Z

    return p1
.end method

.method static synthetic access$4700(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->stopPulse()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->showPulse(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondIcon:Landroid/widget/ImageView;

    return-object p1
.end method

.method private changePulse(Z)V
    .locals 2
    .param p1, "isGreen"    # Z

    .prologue
    .line 1271
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1273
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->changePulseImage(ZZ)V

    .line 1274
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isEndPulseDelay:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->getDrawingMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1275
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/heartrate/PulseView;->setChangeColor(Z)V

    .line 1277
    :cond_1
    return-void
.end method

.method private changePulseImage(ZZ)V
    .locals 6
    .param p1, "isGreen"    # Z
    .param p2, "isChange"    # Z

    .prologue
    const-wide/16 v4, 0x64

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1282
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewMask:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const v0, 0x7f020523

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1284
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    const v0, 0x7f020528

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1285
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewMask:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1286
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1288
    if-nez p2, :cond_0

    .line 1289
    if-eqz p1, :cond_3

    .line 1290
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewGreenIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1291
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewOrangeIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1292
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewGreenIcon:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$20;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$20;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/widget/ImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1311
    :cond_0
    :goto_2
    return-void

    .line 1282
    :cond_1
    const v0, 0x7f020525

    goto :goto_0

    .line 1284
    :cond_2
    const v0, 0x7f02052a

    goto :goto_1

    .line 1300
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewGreenIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1301
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewOrangeIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1302
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewOrangeIcon:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$21;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$21;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/widget/ImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2
.end method

.method private changeUIReadyToMeasuring(Z)V
    .locals 5
    .param p1, "isFail"    # Z

    .prologue
    .line 1171
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changeUIReadyToMeasuring : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1174
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->readyTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v2}, Landroid/os/CountDownTimer;->cancel()V

    .line 1176
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSensorErrorTimer:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;

    if-eqz v2, :cond_0

    .line 1177
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSensorErrorTimer:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$SensorErrorTimer;->cancel()V

    .line 1181
    :cond_0
    if-eqz p1, :cond_1

    .line 1182
    const/4 v0, 0x0

    .line 1186
    .local v0, "failMsg":I
    :goto_0
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1187
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1188
    iput v0, v1, Landroid/os/Message;->arg1:I

    .line 1189
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1190
    return-void

    .line 1184
    .end local v0    # "failMsg":I
    .end local v1    # "msg":Landroid/os/Message;
    :cond_1
    const/4 v0, 0x1

    .restart local v0    # "failMsg":I
    goto :goto_0
.end method

.method private checkCircleViewStatus()V
    .locals 2

    .prologue
    .line 716
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_1

    .line 717
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->getIsRun()Z

    move-result v0

    if-nez v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->startAnimation(Z)V

    .line 720
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsGreen:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->changeColor(Z)V

    .line 722
    :cond_1
    return-void
.end method

.method private checkRunPulseView()V
    .locals 1

    .prologue
    .line 725
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsRunPulseView:Z

    if-eqz v0, :cond_0

    .line 726
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->changePulse(Z)V

    .line 730
    :goto_0
    return-void

    .line 728
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->scaleAnimationStarter(Z)V

    goto :goto_0
.end method

.method private firstAnimation(Landroid/view/View;I)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "stringID"    # I

    .prologue
    .line 535
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "firstAnimation : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    if-nez v0, :cond_0

    .line 564
    :goto_0
    return-void

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$8;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$8;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private initAoudioFile()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 787
    new-instance v0, Landroid/media/SoundPool;

    invoke-direct {v0, v5, v6, v4}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pool:Landroid/media/SoundPool;

    .line 788
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f06002c

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v4

    .line 789
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f060027

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v5

    .line 790
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->effectAudio:[I

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f06002d

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 791
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f060028

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v6

    .line 792
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->effectAudio:[I

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f06002a

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 793
    return-void
.end method

.method private initMeasureStatus()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1193
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isFingerDetected:Z

    .line 1194
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRate:I

    .line 1195
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mMeasureFailCount:I

    .line 1196
    return-void
.end method

.method private playSound(I)V
    .locals 3
    .param p1, "effectAudio"    # I

    .prologue
    .line 797
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 799
    .local v0, "aManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-nez v1, :cond_1

    .line 815
    :cond_0
    :goto_0
    return-void

    .line 802
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pool:Landroid/media/SoundPool;

    if-eqz v1, :cond_0

    .line 803
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$10;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$10;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;I)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private pulseAnimationShow(Z)V
    .locals 4
    .param p1, "isGreen"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1228
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1229
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1230
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsRunPulseView:Z

    .line 1232
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1233
    invoke-direct {p0, p1, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->changePulseImage(ZZ)V

    .line 1234
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pulseHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1235
    return-void
.end method

.method private resetPulseView()V
    .locals 2

    .prologue
    .line 1314
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1339
    return-void
.end method

.method private scaleAnimationStarter(Z)V
    .locals 3
    .param p1, "isGreen"    # Z

    .prologue
    .line 641
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "scaleAnimationStarter : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    if-nez v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040010

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    .line 645
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 646
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 647
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$9;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$9;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 672
    :goto_0
    return-void

    .line 670
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsGreen:Z

    if-eqz v0, :cond_1

    const v0, 0x7f020526

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f020529

    goto :goto_1
.end method

.method private setMeasuringFailUI(I)V
    .locals 4
    .param p1, "errorType"    # I

    .prologue
    const/4 v3, 0x3

    .line 675
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringFailUI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    const/4 v0, -0x6

    if-ne p1, v0, :cond_3

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 682
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 683
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 684
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->effectAudio:[I

    aget v0, v0, v3

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->playSound(I)V

    .line 685
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->showErrorDialog(I)V

    .line 713
    :cond_1
    :goto_0
    return-void

    .line 689
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->effectAudio:[I

    aget v0, v0, v3

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->playSound(I)V

    .line 690
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->showErrorDialog(I)V

    goto :goto_0

    .line 695
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsGreen:Z

    .line 696
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->checkRunPulseView()V

    .line 698
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->checkCircleViewStatus()V

    .line 701
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    const v1, 0x7f090c04

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 702
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    const v1, 0x7f090c23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 705
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070182

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 706
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v1, 0x7f020529

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 707
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020524

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 710
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mErrorType:I

    .line 711
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING_ERROR:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    goto :goto_0
.end method

.method private setMeasuringUI(I)V
    .locals 8
    .param p1, "isShowAnimation"    # I

    .prologue
    const v7, 0x7f090c23

    const v6, 0x7f090c04

    const v5, 0x106000c

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 602
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringUI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 608
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020522

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 609
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 610
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 612
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsGreen:Z

    .line 613
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->checkRunPulseView()V

    .line 615
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    .line 617
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->checkCircleViewStatus()V

    .line 619
    if-nez p1, :cond_4

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-direct {p0, v0, v6}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->firstAnimation(Landroid/view/View;I)V

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-direct {p0, v0, v7}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->firstAnimation(Landroid/view/View;I)V

    .line 632
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 633
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 634
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpmUnit:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 635
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 636
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    .line 637
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 638
    :cond_3
    return-void

    .line 623
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 626
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 627
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 628
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(I)V

    .line 629
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private setReadyUI(I)V
    .locals 9
    .param p1, "animation"    # I

    .prologue
    const v8, 0x7f090c22

    const v7, 0x7f090c05

    const v6, 0x106000c

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 567
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setReadyUI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    if-eqz v1, :cond_0

    .line 569
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 571
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 572
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 573
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v2, 0x7f020527

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 574
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    if-eqz v1, :cond_1

    .line 575
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04000e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 576
    .local v0, "alphaAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 578
    .end local v0    # "alphaAnimation":Landroid/view/animation/Animation;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v2, 0x7f020522

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 580
    if-nez p1, :cond_5

    .line 581
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-direct {p0, v1, v7}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->firstAnimation(Landroid/view/View;I)V

    .line 582
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->firstAnimation(Landroid/view/View;I)V

    .line 592
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 593
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 594
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpmUnit:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 595
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 596
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_4

    .line 597
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 598
    :cond_4
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->READY:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    .line 599
    return-void

    .line 584
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 585
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 587
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 588
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 589
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(I)V

    .line 590
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private showPulse(Z)V
    .locals 2
    .param p1, "isDrawing"    # Z

    .prologue
    .line 1239
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->reset()V

    .line 1240
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/heartrate/PulseView;->setMode(Z)V

    .line 1241
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsGreen:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/PulseView;->setChangeColor(Z)V

    .line 1243
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    if-eqz v0, :cond_0

    .line 1244
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->threadStop()V

    .line 1245
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    .line 1246
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    .line 1247
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->setDrawingMode(Z)V

    .line 1248
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->start()V

    .line 1254
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isEndPulseDelay:Z

    .line 1255
    return-void

    .line 1250
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    .line 1251
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->setDrawingMode(Z)V

    .line 1252
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->start()V

    goto :goto_0
.end method

.method private startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V
    .locals 2
    .param p1, "timerType"    # Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    .prologue
    .line 1200
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$19;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$19;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1225
    return-void
.end method

.method private stopPulse()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1258
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->resetPulseView()V

    .line 1259
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsRunPulseView:Z

    .line 1260
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isEndPulseDelay:Z

    .line 1261
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;

    if-eqz v0, :cond_0

    .line 1262
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->reset()V

    .line 1264
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    if-eqz v0, :cond_1

    .line 1265
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->threadStop()V

    .line 1266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    .line 1268
    :cond_1
    return-void
.end method

.method private stopSensorAndAnimationInitialize()V
    .locals 2

    .prologue
    .line 516
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sth  stopSensorAndAnimationInitialize enter "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->stopMeasuring()V

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->stopCountDownTimer()V

    .line 522
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$7;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 532
    return-void
.end method


# virtual methods
.method public createMeasurementDialog()V
    .locals 14

    .prologue
    const v13, 0x43a28000    # 325.0f

    const v12, 0x43908000    # 289.0f

    const/4 v11, 0x0

    .line 214
    sget-object v8, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string v9, "createMeasurementDialog"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v9}, Landroid/view/Window;->clearFlags(I)V

    .line 216
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 217
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iput-boolean v11, v8, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isMeasureCompleted:Z

    .line 218
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getAppContext()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x7

    invoke-direct {v0, v8, v9}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 220
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 221
    const-string v8, ""

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 223
    const v8, 0x7f030152

    new-instance v9, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;

    invoke-direct {v9, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v0, v8, v9}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 327
    new-instance v8, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$4;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$4;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 341
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v8, :cond_0

    .line 342
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 345
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 348
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v5

    .line 349
    .local v5, "v":Landroid/view/View;
    const v8, 0x7f080033

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 350
    .local v1, "flP":Landroid/widget/FrameLayout;
    if-eqz v1, :cond_1

    .line 351
    invoke-virtual {v1, v11, v11, v11, v11}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 352
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 353
    .local v4, "mlp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v13

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v12

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 357
    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 360
    .end local v4    # "mlp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    const/4 v7, 0x0

    .line 362
    .local v7, "window":Landroid/view/Window;
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    .line 364
    if-eqz v7, :cond_2

    .line 365
    const/16 v8, 0x400

    invoke-virtual {v7, v8}, Landroid/view/Window;->clearFlags(I)V

    .line 369
    :cond_2
    if-eqz v7, :cond_4

    .line 380
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v12

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 381
    .local v2, "height":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v13

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 382
    .local v6, "width":I
    invoke-virtual {v7, v6, v2}, Landroid/view/Window;->setLayout(II)V

    .line 384
    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 387
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 388
    const/16 v8, 0x31

    iput v8, v3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 390
    invoke-virtual {v7, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 394
    const/16 v8, 0x800

    invoke-virtual {v7, v8}, Landroid/view/Window;->addFlags(I)V

    .line 396
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x12

    if-lt v8, v9, :cond_3

    .line 397
    const/high16 v8, 0x4000000

    invoke-virtual {v7, v8}, Landroid/view/Window;->addFlags(I)V

    .line 399
    :cond_3
    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x500

    invoke-virtual {v8, v9}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 402
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v8, :cond_4

    .line 403
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    const/4 v9, 0x1

    invoke-virtual {v8, v7, v9}, Lcom/samsung/android/sdk/cover/ScoverManager;->setCoverModeToWindow(Landroid/view/Window;I)V

    .line 406
    .end local v2    # "height":I
    .end local v3    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v6    # "width":I
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->refreshFragmentFocusables()V

    .line 408
    .end local v1    # "flP":Landroid/widget/FrameLayout;
    .end local v5    # "v":Landroid/view/View;
    .end local v7    # "window":Landroid/view/Window;
    :cond_5
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 208
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .line 209
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 210
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 423
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 413
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 414
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeartrateSummaryFragmentNew onConfigurationChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Landroid/content/res/Configuration;->userSetLocale:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsOnConfigChanged:Z

    .line 418
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 188
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreateView"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const v0, 0x7f0301a5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mMainView:Landroid/view/View;

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 192
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->setRetainInstance(Z)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mMainView:Landroid/view/View;

    return-object v0
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;)V
    .locals 8
    .param p1, "hrm"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 819
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onDataReceived isFingerDetected : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isFingerDetected:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->heartRate:I

    .line 822
    .local v1, "rate":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-nez v2, :cond_1

    .line 823
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->stopSensorAndAnimationInitialize()V

    .line 865
    :cond_0
    :goto_0
    return-void

    .line 827
    :cond_1
    if-gez v1, :cond_2

    .line 828
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onDataReceived state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ShealthSensorDevice.DataListener onDataReceived: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isDetected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isFingerDetected:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isFingerDetected:Z

    if-eqz v2, :cond_4

    .line 833
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isRetryMeasuring:Z

    .line 834
    if-eq v1, v5, :cond_0

    .line 837
    if-gez v1, :cond_3

    const/16 v2, -0x9

    if-lt v1, v2, :cond_3

    .line 838
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isFingerDetected:Z

    .line 840
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 841
    .local v0, "msg":Landroid/os/Message;
    iput v7, v0, Landroid/os/Message;->what:I

    .line 842
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 843
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 845
    .end local v0    # "msg":Landroid/os/Message;
    :cond_3
    if-lez v1, :cond_0

    .line 846
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.shealth.heartrate"

    const-string v4, "HR02"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRate:I

    .line 848
    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRate:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->stopSensorAndUpdateUI(I)V

    .line 849
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->effectAudio:[I

    const/4 v3, 0x4

    aget v2, v2, v3

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->playSound(I)V

    goto :goto_0

    .line 854
    :cond_4
    if-ne v1, v5, :cond_0

    .line 855
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isFingerDetected:Z

    .line 856
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isRetryMeasuring:Z

    if-eqz v2, :cond_5

    .line 857
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    .line 860
    :cond_5
    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPrevError:I

    if-ne v2, v7, :cond_0

    .line 861
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isRetryMeasuring:Z

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->changeUIReadyToMeasuring(Z)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 465
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    .line 467
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 427
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDetach()V

    .line 428
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 452
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->stopSensorAndUpdateUI(I)V

    .line 455
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->stopAnimation(Z)V

    .line 460
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onPause()V

    .line 461
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 432
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_0

    .line 434
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isFingerDetected:Z

    if-eqz v0, :cond_1

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 436
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->setMeasuringUI(I)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p0, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;Z)V

    .line 447
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    .line 448
    return-void

    .line 438
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isMeasureCompleted:Z

    if-eqz v0, :cond_2

    .line 439
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->setMeasuringEndUI()V

    goto :goto_0

    .line 441
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 442
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->setReadyUI(I)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p0, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;Z)V

    goto :goto_0
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 880
    return-void
.end method

.method public onTimeout()V
    .locals 2

    .prologue
    .line 869
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRate:I

    if-lez v0, :cond_0

    .line 875
    :goto_0
    return-void

    .line 872
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onTimeout"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    const/4 v0, -0x6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->stopSensorAndUpdateUI(I)V

    goto :goto_0
.end method

.method public setFragmentHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "_FragmentHandler"    # Landroid/os/Handler;

    .prologue
    .line 1484
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHrmScoverFragmentHandler:Landroid/os/Handler;

    .line 1485
    return-void
.end method

.method public setMeasuringEndUI()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 733
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setMeasuringEndUI"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 736
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 738
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020522

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 742
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    if-eqz v0, :cond_4

    .line 743
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 745
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isMeasureCompleted:Z

    if-eqz v0, :cond_4

    .line 747
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 752
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    .line 753
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 755
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 763
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_5

    .line 764
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->stopAnimation(Z)V

    .line 767
    :cond_5
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRate:I

    if-lez v0, :cond_9

    .line 768
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    const v1, 0x7f090c06

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07010c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 770
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 773
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 774
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 775
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpmUnit:Landroid/widget/TextView;

    if-eqz v0, :cond_7

    .line 776
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 777
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_8

    .line 778
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 779
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mStartButton:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 780
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mStartButton:Landroid/widget/TextView;

    const v1, 0x7f090050

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 783
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->stopPulse()V

    .line 784
    return-void
.end method

.method public setScoverManger(Lcom/samsung/android/sdk/cover/ScoverManager;)V
    .locals 0
    .param p1, "_coverManager"    # Lcom/samsung/android/sdk/cover/ScoverManager;

    .prologue
    .line 1488
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 1489
    return-void
.end method

.method public showErrorDialog(I)V
    .locals 14
    .param p1, "errorType"    # I

    .prologue
    const v13, 0x43a28000    # 325.0f

    const v12, 0x43908000    # 289.0f

    const/4 v11, 0x0

    .line 940
    sget-object v8, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "showErrorDialog : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 942
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->stopPulse()V

    .line 944
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v8, :cond_0

    .line 945
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    invoke-virtual {v8, v11}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->stopAnimation(Z)V

    .line 948
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    if-nez v8, :cond_2

    .line 1117
    :cond_1
    :goto_0
    return-void

    .line 951
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v9}, Landroid/view/Window;->clearFlags(I)V

    .line 954
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getAppContext()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x7

    invoke-direct {v0, v8, v9}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 958
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 959
    const-string v8, ""

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 962
    const v8, 0x7f030153

    new-instance v9, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$12;

    invoke-direct {v9, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$12;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v0, v8, v9}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1016
    new-instance v8, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$13;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$13;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1023
    new-instance v8, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$14;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$14;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1030
    new-instance v8, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$15;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$15;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1038
    new-instance v8, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$16;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$16;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1048
    new-instance v8, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$17;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$17;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1058
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 1059
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v8, :cond_3

    .line 1060
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1063
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v9

    const-string v10, "error"

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1064
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v5

    .line 1065
    .local v5, "v":Landroid/view/View;
    if-eqz v5, :cond_4

    .line 1066
    const v8, 0x7f080033

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 1067
    .local v1, "flP":Landroid/widget/FrameLayout;
    if-eqz v1, :cond_4

    .line 1068
    invoke-virtual {v1, v11, v11, v11, v11}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 1069
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1070
    .local v4, "mlp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v13

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 1072
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v12

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 1074
    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1079
    .end local v1    # "flP":Landroid/widget/FrameLayout;
    .end local v4    # "mlp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_4
    const/4 v7, 0x0

    .line 1080
    .local v7, "window":Landroid/view/Window;
    if-nez v7, :cond_5

    .line 1081
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    .line 1086
    :cond_5
    if-eqz v7, :cond_6

    .line 1097
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v12

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1098
    .local v2, "height":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v13

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 1099
    .local v6, "width":I
    invoke-virtual {v7, v6, v2}, Landroid/view/Window;->setLayout(II)V

    .line 1101
    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 1102
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 1103
    const/16 v8, 0x31

    iput v8, v3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1105
    invoke-virtual {v7, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1106
    const/high16 v8, 0x80000

    invoke-virtual {v7, v8}, Landroid/view/Window;->addFlags(I)V

    .line 1107
    const/16 v8, 0x800

    invoke-virtual {v7, v8}, Landroid/view/Window;->addFlags(I)V

    .line 1108
    const/high16 v8, 0x4000000

    invoke-virtual {v7, v8}, Landroid/view/Window;->addFlags(I)V

    .line 1109
    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x500

    invoke-virtual {v8, v9}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1112
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v8, :cond_6

    .line 1113
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    const/4 v9, 0x1

    invoke-virtual {v8, v7, v9}, Lcom/samsung/android/sdk/cover/ScoverManager;->setCoverModeToWindow(Landroid/view/Window;I)V

    .line 1115
    .end local v2    # "height":I
    .end local v3    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v6    # "width":I
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->refreshFragmentFocusables()V

    goto/16 :goto_0
.end method

.method public startSensorAndDelayUIForDetectedFinger(I)V
    .locals 5
    .param p1, "animation"    # I

    .prologue
    .line 470
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startSensorAndDelayUIForDetectedFinger : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->initMeasureStatus()V

    .line 474
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, p0, v4}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;Z)V

    .line 477
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    new-instance v3, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$5;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 490
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isMeasureCompleted:Z

    .line 491
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$6;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$6;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;I)V

    .line 511
    .local v0, "task":Ljava/util/TimerTask;
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 512
    .local v1, "timer":Ljava/util/Timer;
    const-wide/16 v2, 0x96

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 513
    return-void
.end method

.method public stopSensorAndUpdateUI(I)V
    .locals 4
    .param p1, "heartRate"    # I

    .prologue
    .line 1120
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopSensorAndUpdateUI"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1121
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stopSensorAndUpdateUI rate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " RemainFailCount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mMeasureFailCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1123
    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mMeasureFailCount:I

    if-lez v1, :cond_1

    .line 1124
    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mMeasureFailCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mMeasureFailCount:I

    .line 1168
    :cond_0
    :goto_0
    return-void

    .line 1128
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->stopSensorAndAnimationInitialize()V

    .line 1130
    if-nez p1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isMeasureCompleted:Z

    if-nez v1, :cond_3

    .line 1131
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 1134
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 1140
    :cond_3
    if-ltz p1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isMeasureCompleted:Z

    if-nez v1, :cond_4

    .line 1141
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1144
    if-lez p1, :cond_0

    .line 1145
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isMeasureCompleted:Z

    .line 1146
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$18;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$18;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;I)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1161
    :cond_4
    if-gez p1, :cond_0

    .line 1162
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1163
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1164
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1165
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
