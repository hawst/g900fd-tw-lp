.class public Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;
.super Ljava/lang/Object;
.source "NetCalorieUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBMR()D
    .locals 12

    .prologue
    const/4 v8, 0x1

    .line 65
    new-instance v5, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 67
    .local v5, "shealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v1, Ljava/sql/Date;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Ljava/sql/Date;-><init>(J)V

    .line 68
    .local v1, "birthday":Ljava/sql/Date;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 69
    .local v2, "birthdayCalendar":Ljava/util/Calendar;
    invoke-virtual {v1}, Ljava/sql/Date;->getTime()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 70
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-virtual {v2, v8}, Ljava/util/Calendar;->get(I)I

    move-result v7

    sub-int v0, v6, v7

    .line 71
    .local v0, "age":I
    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v6

    const v7, 0x2e635

    if-ne v6, v7, :cond_1

    .line 72
    const-wide v6, 0x4056172b020c49baL    # 88.362

    const-wide v8, 0x402acb4395810625L    # 13.397

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v10

    float-to-double v10, v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    const-wide v8, 0x4013322d0e560419L    # 4.799

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v10

    float-to-double v10, v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    const-wide v8, 0x4016b53f7ced9168L    # 5.677

    int-to-double v10, v0

    mul-double/2addr v8, v10

    sub-double v3, v6, v8

    .line 76
    .local v3, "bmr":D
    :goto_0
    const-wide/16 v6, 0x0

    cmpg-double v6, v3, v6

    if-gez v6, :cond_0

    .line 77
    const-wide/16 v3, 0x0

    .line 79
    :cond_0
    return-wide v3

    .line 74
    .end local v3    # "bmr":D
    :cond_1
    const-wide v6, 0x407bf97ced916873L    # 447.593

    const-wide v8, 0x40227e76c8b43958L    # 9.247

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v10

    float-to-double v10, v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    const-wide v8, 0x4008c8b439581062L    # 3.098

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v10

    float-to-double v10, v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    const-wide v8, 0x401151eb851eb852L    # 4.33

    int-to-double v10, v0

    mul-double/2addr v8, v10

    sub-double v3, v6, v8

    .restart local v3    # "bmr":D
    goto :goto_0
.end method

.method public static getNetCalorie()I
    .locals 7

    .prologue
    const-wide v5, 0x3ff3333340000000L    # 1.2000000476837158

    .line 33
    new-instance v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 35
    .local v2, "shealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 52
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getBMR()D

    move-result-wide v3

    mul-double v0, v3, v5

    .line 54
    .local v0, "netCalorie":D
    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-int v3, v3

    return v3

    .line 37
    .end local v0    # "netCalorie":D
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getBMR()D

    move-result-wide v3

    mul-double v0, v3, v5

    .line 38
    .restart local v0    # "netCalorie":D
    goto :goto_0

    .line 40
    .end local v0    # "netCalorie":D
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getBMR()D

    move-result-wide v3

    const-wide/high16 v5, 0x3ff6000000000000L    # 1.375

    mul-double v0, v3, v5

    .line 41
    .restart local v0    # "netCalorie":D
    goto :goto_0

    .line 43
    .end local v0    # "netCalorie":D
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getBMR()D

    move-result-wide v3

    const-wide v5, 0x3ff8ccccc0000000L    # 1.5499999523162842

    mul-double v0, v3, v5

    .line 44
    .restart local v0    # "netCalorie":D
    goto :goto_0

    .line 46
    .end local v0    # "netCalorie":D
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getBMR()D

    move-result-wide v3

    const-wide v5, 0x3ffb9999a0000000L    # 1.725000023841858

    mul-double v0, v3, v5

    .line 47
    .restart local v0    # "netCalorie":D
    goto :goto_0

    .line 49
    .end local v0    # "netCalorie":D
    :pswitch_4
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getBMR()D

    move-result-wide v3

    const-wide v5, 0x3ffe666660000000L    # 1.899999976158142

    mul-double v0, v3, v5

    .line 50
    .restart local v0    # "netCalorie":D
    goto :goto_0

    .line 35
    nop

    :pswitch_data_0
    .packed-switch 0x2bf21
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getRecommendedCalorieBurn()D
    .locals 4

    .prologue
    .line 93
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getNetCalorie()I

    move-result v0

    int-to-double v0, v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getBMR()D

    move-result-wide v2

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method public static getRecommendedCalorieIntake()I
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getNetCalorie()I

    move-result v0

    return v0
.end method
