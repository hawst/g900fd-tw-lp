.class public Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;
.super Landroid/widget/FrameLayout;
.source "ImageGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;,
        Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$OnGridItemEventListener;
    }
.end annotation


# static fields
.field private static final FOOD_GRID_LAYOUTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final ID_DELETE_BUTTON:I = 0x7f0803c1

.field private static final ID_IMAGE_BACKGROUND:I = 0x7f0803c0

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private handler:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;

.field private isEditableMode:Z

.field private volatile isHandlerAllowed:Z

.field private mImageDeletedListener:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageDeletedListener;

.field private mImagePathCollection:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mOnGridItemEventListener:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$OnGridItemEventListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->LOG_TAG:Ljava/lang/String;

    .line 51
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->FOOD_GRID_LAYOUTS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->isEditableMode:Z

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->isEditableMode:Z

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->isEditableMode:Z

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->isHandlerAllowed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;)Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->handler:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;)Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageDeletedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImageDeletedListener:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageDeletedListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->init()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;)Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$OnGridItemEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mOnGridItemEventListener:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$OnGridItemEventListener;

    return-object v0
.end method

.method private generateGridItemBackgroundId(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 317
    add-int/lit8 v0, p1, 0x1

    return v0
.end method

.method private getImageGridView(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;>;"
    const/4 v4, 0x0

    .line 193
    if-nez p1, :cond_0

    .line 194
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "input List should be not empty"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 197
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 198
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->setVisibility(I)V

    .line 212
    :goto_0
    return-void

    .line 199
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    sget-object v3, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->FOOD_GRID_LAYOUTS:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 200
    sget-object v2, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->FOOD_GRID_LAYOUTS:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 201
    .local v1, "idMainContainer":I
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->setVisibility(I)V

    .line 202
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 203
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getViewGroupAllWithItem()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-direct {p0, v2, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->initAllViews(Landroid/view/ViewGroup;Ljava/util/List;)V

    goto :goto_0

    .line 205
    .end local v1    # "idMainContainer":I
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->FOOD_GRID_LAYOUTS:Ljava/util/List;

    sget-object v3, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->FOOD_GRID_LAYOUTS:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 206
    .restart local v1    # "idMainContainer":I
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->setVisibility(I)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 208
    const v2, 0x7f0803c3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridLayout;

    .line 209
    .local v0, "gridLayout":Landroid/widget/GridLayout;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->inflateAllViews(Landroid/widget/GridLayout;I)V

    .line 210
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->initAllViews(Landroid/view/ViewGroup;Ljava/util/List;)V

    goto :goto_0
.end method

.method private getViewGroupAllWithItem()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 249
    const v0, 0x7f0803c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private inflateAllViews(Landroid/widget/GridLayout;I)V
    .locals 6
    .param p1, "containerOfView"    # Landroid/widget/GridLayout;
    .param p2, "count"    # I

    .prologue
    .line 215
    invoke-virtual {p1}, Landroid/widget/GridLayout;->getColumnCount()I

    move-result v0

    .line 216
    .local v0, "colCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_2

    .line 217
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0300d0

    invoke-static {v4, v5, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 218
    invoke-virtual {p1, v1}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 219
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/GridLayout$LayoutParams;

    .line 220
    .local v2, "layoutParams":Landroid/widget/GridLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0509

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/GridLayout$LayoutParams;->width:I

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a050a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/GridLayout$LayoutParams;->height:I

    .line 222
    div-int v4, v1, v0

    const/4 v5, 0x1

    if-lt v4, v5, :cond_0

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0809

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/GridLayout$LayoutParams;->topMargin:I

    .line 225
    :cond_0
    add-int/lit8 v4, v1, 0x1

    rem-int/2addr v4, v0

    if-eqz v4, :cond_1

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a080a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/GridLayout$LayoutParams;->rightMargin:I

    .line 228
    :cond_1
    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 216
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 230
    .end local v2    # "layoutParams":Landroid/widget/GridLayout$LayoutParams;
    .end local v3    # "view":Landroid/view/View;
    :cond_2
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->isHandlerAllowed:Z

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->removeAllViews()V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getImageGridView(Ljava/util/List;)V

    .line 187
    return-void
.end method

.method private initAllViews(Landroid/view/ViewGroup;Ljava/util/List;)V
    .locals 3
    .param p1, "containerOfView"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 234
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;>;"
    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getViewGroupAllWithItem()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;-><init>(Landroid/view/ViewGroup;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->handler:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;

    .line 235
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 236
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    invoke-direct {p0, v1, v2, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->initView(Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;I)V

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    :cond_0
    return-void
.end method

.method private initView(Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;I)V
    .locals 5
    .param p1, "parentView"    # Landroid/view/ViewGroup;
    .param p2, "imagePagerHolder"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;
    .param p3, "position"    # I

    .prologue
    const v3, 0x7f0803c0

    .line 253
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_1

    .line 254
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->generateGridItemBackgroundId(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 258
    .local v1, "imageResource":Landroid/widget/ImageButton;
    :goto_0
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->generateGridItemBackgroundId(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setId(I)V

    .line 263
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090028

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901e5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 265
    const v2, 0x7f0803c1

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 266
    .local v0, "imageDeleteButton":Landroid/widget/ImageButton;
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->isEditableMode:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 268
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 269
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;

    invoke-direct {v3, p0, p2, p1, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;Landroid/view/ViewGroup;Landroid/widget/ImageButton;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 296
    :cond_0
    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$3;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$3;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 305
    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$4;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$4;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 314
    return-void

    .line 256
    .end local v0    # "imageDeleteButton":Landroid/widget/ImageButton;
    .end local v1    # "imageResource":Landroid/widget/ImageButton;
    :cond_1
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .restart local v1    # "imageResource":Landroid/widget/ImageButton;
    goto :goto_0

    .line 266
    .restart local v0    # "imageDeleteButton":Landroid/widget/ImageButton;
    :cond_2
    const/16 v2, 0x8

    goto :goto_1
.end method


# virtual methods
.method public addItem(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;)V
    .locals 1
    .param p1, "imagePagerHolder"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->init()V

    .line 180
    return-void
.end method

.method public addItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 133
    .local p1, "imagePagerHolders":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->init()V

    .line 135
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->init()V

    .line 93
    return-void
.end method

.method public destroyGridViewItems()V
    .locals 6

    .prologue
    const v5, 0x7f0803c0

    .line 96
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getViewGroupAllWithItem()Landroid/view/ViewGroup;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 98
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getViewGroupAllWithItem()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getViewGroupAllWithItem()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    if-nez v3, :cond_1

    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getViewGroupAllWithItem()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->generateGridItemBackgroundId(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 112
    .local v1, "imageResource":Landroid/widget/ImageButton;
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    instance-of v3, v3, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_0

    .line 116
    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 117
    .local v2, "mBitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 119
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 98
    .end local v2    # "mBitmap":Landroid/graphics/Bitmap;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    .end local v1    # "imageResource":Landroid/widget/ImageButton;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getViewGroupAllWithItem()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .restart local v1    # "imageResource":Landroid/widget/ImageButton;
    goto :goto_1

    .line 126
    .end local v0    # "i":I
    .end local v1    # "imageResource":Landroid/widget/ImageButton;
    :cond_2
    return-void
.end method

.method public getImagesCount()I
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->isHandlerAllowed:Z

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->destroyDrawingCache()V

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->destroyGridViewItems()V

    .line 347
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 348
    return-void
.end method

.method public setEditableMode(Z)V
    .locals 5
    .param p1, "isEditable"    # Z

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getViewGroupAllWithItem()Landroid/view/ViewGroup;

    move-result-object v1

    .line 163
    .local v1, "container":Landroid/view/ViewGroup;
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->isEditableMode:Z

    .line 164
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-lez v3, :cond_1

    .line 165
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 166
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 167
    .local v0, "child":Landroid/view/ViewGroup;
    const v3, 0x7f0803c1

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz p1, :cond_0

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 165
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 167
    :cond_0
    const/16 v3, 0x8

    goto :goto_1

    .line 170
    .end local v0    # "child":Landroid/view/ViewGroup;
    .end local v2    # "i":I
    :cond_1
    return-void
.end method

.method public setImageDeletedListener(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageDeletedListener;)V
    .locals 0
    .param p1, "imageDeletedListener"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageDeletedListener;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImageDeletedListener:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageDeletedListener;

    .line 84
    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "imagePagerHolders":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;

    .line 143
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->init()V

    .line 144
    return-void
.end method

.method public setOnGridItemListener(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$OnGridItemEventListener;)V
    .locals 0
    .param p1, "onGridItemEventListener"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$OnGridItemEventListener;

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mOnGridItemEventListener:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$OnGridItemEventListener;

    .line 246
    return-void
.end method
