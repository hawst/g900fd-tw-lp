.class public abstract Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "CignaLoadFragmentActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method private startCignaCoachActivity()V
    .locals 3

    .prologue
    .line 51
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 52
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 53
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    const-string v1, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 55
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 56
    return-void
.end method


# virtual methods
.method public onChangeLanguage(Z)V
    .locals 3
    .param p1, "changeLanguage"    # Z

    .prologue
    .line 45
    if-eqz p1, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_CHANGE_LANGUAGE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 48
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->killProcessIfDBNotInitialised(Landroid/app/Activity;)V

    .line 21
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->finishIfCignaUnsupported(Landroid/app/Activity;)V

    .line 23
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    move-result-object v2

    invoke-virtual {v2, p0, p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->registerReceiver(Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;)V

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 26
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "EXTRA_NAME_CHANGE_LANGUAGE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 28
    .local v0, "changeLanguage":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0009

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    const-string v2, "EXTRA_NAME_CHANGE_LANGUAGE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;->startCignaCoachActivity()V

    .line 34
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 40
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    move-result-object v0

    invoke-virtual {v0, p0, p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->unRegisterReceiver(Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;)V

    .line 41
    return-void
.end method
