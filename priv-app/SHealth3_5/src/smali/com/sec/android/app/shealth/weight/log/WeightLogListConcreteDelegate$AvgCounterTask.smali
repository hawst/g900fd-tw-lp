.class Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;
.super Landroid/os/AsyncTask;
.source "WeightLogListConcreteDelegate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AvgCounterTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTextView:Landroid/widget/TextView;

.field private mTime:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;Landroid/content/Context;Landroid/widget/TextView;J)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "viewToUpdate"    # Landroid/widget/TextView;
    .param p4, "time"    # J

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 211
    iput-object p3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mTextView:Landroid/widget/TextView;

    .line 212
    iput-wide p4, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mTime:J

    .line 213
    iput-object p2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mContext:Landroid/content/Context;

    .line 214
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Float;
    .locals 4
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;

    # invokes: Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getDao()Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->access$000(Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;)Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mTime:J

    # invokes: Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->countWeightAvgForDay(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;J)F
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->access$100(Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;J)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mContext:Landroid/content/Context;

    # invokes: Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getUnitHelper(Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->access$200(Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 205
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Float;)V
    .locals 7
    .param p1, "result"    # Ljava/lang/Float;

    .prologue
    const/16 v6, 0x20

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mContext:Landroid/content/Context;

    # invokes: Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getUnitHelper(Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->access$200(Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v0

    .line 226
    .local v0, "weightUnit":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090f20

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x29

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090204

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getFullName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901fd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 239
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 205
    check-cast p1, Ljava/lang/Float;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->onPostExecute(Ljava/lang/Float;)V

    return-void
.end method
