.class Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;
.super Ljava/lang/Object;
.source "WeightInputActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

.field final synthetic val$rootView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;->val$rootView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 322
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # invokes: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$200(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getButtonContainer()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 323
    .local v1, "viewGroup":Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 324
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # invokes: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$300(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getButtonContainer()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v0

    .line 325
    .local v0, "id":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;->val$rootView:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 326
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;->val$rootView:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 329
    .end local v0    # "id":I
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$000(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 330
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$000(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v2

    .line 331
    .local v2, "weightText":Landroid/widget/EditText;
    invoke-virtual {v2}, Landroid/widget/EditText;->hasFocus()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 332
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$000(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->showKeyboard()V

    .line 335
    .end local v2    # "weightText":Landroid/widget/EditText;
    :cond_1
    return-void
.end method
