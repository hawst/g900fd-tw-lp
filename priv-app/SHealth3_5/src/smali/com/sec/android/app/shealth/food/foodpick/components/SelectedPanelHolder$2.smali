.class Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$2;
.super Ljava/util/ArrayList;
.source "SelectedPanelHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;FI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$amount:F

.field final synthetic val$foodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

.field final synthetic val$unit:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;FI)V
    .locals 8

    .prologue
    const-wide/16 v1, -0x1

    .line 63
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$2;->val$foodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    iput p2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$2;->val$amount:F

    iput p3, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$2;->val$unit:I

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 66
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$2;->val$foodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v5

    iget v7, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$2;->val$amount:F

    move-wide v3, v1

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;-><init>(JJJF)V

    .line 67
    .local v0, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    iget v1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$2;->val$unit:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setUnit(I)V

    .line 68
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$2;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method
