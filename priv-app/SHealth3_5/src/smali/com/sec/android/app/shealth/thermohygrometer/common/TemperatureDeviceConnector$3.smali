.class Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$3;
.super Ljava/lang/Object;
.source "TemperatureDeviceConnector.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)V
    .locals 0

    .prologue
    .line 392
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "dataType"    # I
    .param p2, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 406
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener onReceived1: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener extra: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    const/16 v1, 0x13

    if-ne p1, v1, :cond_0

    move-object v0, p2

    .line 411
    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;

    .line 412
    .local v0, "tempData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener tempData.temperature: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->temperature:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener tempData.accuracy: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->accuracy:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->temperature:F

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->accuracy:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 421
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mTempSensorListener:Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;)V

    .line 427
    .end local v0    # "tempData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;
    :cond_0
    return-void
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 5
    .param p1, "dataType"    # I
    .param p2, "data"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # [Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 431
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener onReceived2: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener data[0]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p2, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener extra: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const/16 v1, 0x13

    if-ne p1, v1, :cond_0

    .line 437
    aget-object v0, p2, v4

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;

    .line 438
    .local v0, "tempData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener tempData.temperature: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->temperature:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener tempData.accuracy: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->accuracy:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->temperature:F

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->accuracy:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 447
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mTempSensorListener:Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;)V

    .line 453
    .end local v0    # "tempData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;
    :cond_0
    return-void
.end method

.method public onStarted(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 396
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthSensorDevice.DataListener onStarted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    return-void
.end method

.method public onStopped(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 401
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthSensorDevice.DataListener onStopped : dataType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", error = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    return-void
.end method
