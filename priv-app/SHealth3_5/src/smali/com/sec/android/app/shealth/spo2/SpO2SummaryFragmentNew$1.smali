.class Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$1;
.super Landroid/os/Handler;
.source "SpO2SummaryFragmentNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$1;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$1;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget v1, p1, Landroid/os/Message;->what:I

    iput v1, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mPrevError:I

    .line 182
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 199
    :goto_0
    return-void

    .line 185
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$1;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->setReadyUI(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$000(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    goto :goto_0

    .line 189
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$1;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->setMeasuringUI(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    goto :goto_0

    .line 192
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$1;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->setMeasuringFailUI(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    goto :goto_0

    .line 195
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$1;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->setMeasuringEndUI()V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$1;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->refreshFragmentFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$300(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    goto :goto_0

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
