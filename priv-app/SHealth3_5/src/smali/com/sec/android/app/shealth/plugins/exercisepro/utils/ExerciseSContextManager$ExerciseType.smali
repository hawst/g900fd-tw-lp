.class public final enum Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;
.super Ljava/lang/Enum;
.source "ExerciseSContextManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ExerciseType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

.field public static final enum EXERCISE_TYPE_CYCLING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

.field public static final enum EXERCISE_TYPE_END:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

.field public static final enum EXERCISE_TYPE_HIKING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

.field public static final enum EXERCISE_TYPE_NONE:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

.field public static final enum EXERCISE_TYPE_RUNNING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

.field public static final enum EXERCISE_TYPE_WALKING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    const-string v1, "EXERCISE_TYPE_NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_NONE:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    const-string v1, "EXERCISE_TYPE_RUNNING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_RUNNING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    const-string v1, "EXERCISE_TYPE_WALKING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_WALKING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    const-string v1, "EXERCISE_TYPE_CYCLING"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_CYCLING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    const-string v1, "EXERCISE_TYPE_HIKING"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_HIKING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    const-string v1, "EXERCISE_TYPE_END"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_END:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    .line 21
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_NONE:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_RUNNING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_WALKING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_CYCLING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_HIKING:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_END:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->$VALUES:[Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->$VALUES:[Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    return-object v0
.end method
