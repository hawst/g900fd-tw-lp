.class public Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;
.super Landroid/widget/LinearLayout;
.source "CurrentMissionDetailTrakingDayOfWeekView.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mSelect:Z

.field private mTrackingDayOfWeekLayout:Landroid/widget/RelativeLayout;

.field private mTrackingDayOfWeekText:Landroid/widget/TextView;

.field private mTrackingDayText:Landroid/widget/TextView;

.field private mTrackingDisableView:Landroid/view/View;

.field private mTrackingMode:Z

.field private mTrackingModeLayout:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mSelect:Z

    .line 26
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingMode:Z

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mContext:Landroid/content/Context;

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->initLayout()V

    .line 43
    return-void
.end method

.method private initLayout()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mContext:Landroid/content/Context;

    const v1, 0x7f03003f

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 49
    const v0, 0x7f08014a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayOfWeekLayout:Landroid/widget/RelativeLayout;

    .line 50
    const v0, 0x7f08014b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingModeLayout:Landroid/widget/RelativeLayout;

    .line 52
    const v0, 0x7f080149

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayText:Landroid/widget/TextView;

    .line 53
    const v0, 0x7f08014c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayOfWeekText:Landroid/widget/TextView;

    .line 54
    const v0, 0x7f08014d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDisableView:Landroid/view/View;

    .line 55
    return-void
.end method


# virtual methods
.method public isSelection()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mSelect:Z

    return v0
.end method

.method public isTrackingMode()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingMode:Z

    return v0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayOfWeekLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    return-void
.end method

.method public setSelection(Z)V
    .locals 3
    .param p1, "isSelect"    # Z

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingMode:Z

    if-eqz v0, :cond_1

    .line 94
    if-eqz p1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayOfWeekLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setSelected(Z)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayOfWeekText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070044

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 111
    :goto_0
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mSelect:Z

    .line 112
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayOfWeekLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setSelected(Z)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayOfWeekText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070100

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 104
    :cond_1
    if-eqz p1, :cond_2

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayOfWeekLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020103

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayOfWeekLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020102

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setTrackingMode(Z)V
    .locals 0
    .param p1, "trackingMode"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingMode:Z

    .line 120
    return-void
.end method

.method public updateTrackingLayoutVisibility()V
    .locals 2

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingMode:Z

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingModeLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingModeLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateView(J)V
    .locals 3
    .param p1, "timeMillis"    # J

    .prologue
    .line 68
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isToday(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c8b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07012e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 77
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingMode:Z

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayOfWeekText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getDayShortStr(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayOfWeekText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getDayStr(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDisableView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 85
    return-void

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getExcludeYearDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->mTrackingDayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070136

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
