.class public Lcom/sec/android/app/shealth/cignacoach/widget/CignaWebLinkImageView;
.super Landroid/widget/ImageView;
.source "CignaWebLinkImageView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    invoke-virtual {p0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWebLinkImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 17
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 21
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWebLinkImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "http://www.cigna.com/cignacoach"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->startWeb(Landroid/content/Context;Ljava/lang/String;)V

    .line 22
    return-void
.end method
