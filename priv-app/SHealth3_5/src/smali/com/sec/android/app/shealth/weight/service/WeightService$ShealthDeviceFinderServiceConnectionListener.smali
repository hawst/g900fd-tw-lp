.class Lcom/sec/android/app/shealth/weight/service/WeightService$ShealthDeviceFinderServiceConnectionListener;
.super Ljava/lang/Object;
.source "WeightService.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/service/WeightService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShealthDeviceFinderServiceConnectionListener"
.end annotation


# instance fields
.field weightService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/weight/service/WeightService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/service/WeightService;)V
    .locals 1
    .param p1, "wgtService"    # Lcom/sec/android/app/shealth/weight/service/WeightService;

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$ShealthDeviceFinderServiceConnectionListener;->weightService:Ljava/lang/ref/WeakReference;

    .line 189
    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 1
    .param p1, "error"    # I

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$ShealthDeviceFinderServiceConnectionListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # invokes: Lcom/sec/android/app/shealth/weight/service/WeightService;->requestPairedDevices()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$000(Lcom/sec/android/app/shealth/weight/service/WeightService;)V

    .line 195
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$ShealthDeviceFinderServiceConnectionListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$ShealthDeviceFinderServiceConnectionListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$100(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$ShealthDeviceFinderServiceConnectionListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$100(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$ShealthDeviceFinderServiceConnectionListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$102(Lcom/sec/android/app/shealth/weight/service/WeightService;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 203
    :cond_0
    return-void
.end method
