.class public Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
.super Landroid/app/Service;
.source "RealtimeHealthService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$AMapLocationService;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$RealtimeHealthBinder;
    }
.end annotation


# static fields
.field private static ACTION_DEVICE_CONNECTED:Ljava/lang/String; = null

.field private static ANTFEisAvailable:Z = false

.field private static ANTHRMisAvailable:Z = false

.field private static DEVICE_ID:Ljava/lang/String; = null

.field private static DEVICE_TYPE:Ljava/lang/String; = null

.field public static final EXERCISE_ID_NOT_USED:J = 0x0L

.field private static final GPS_DISCONNECTED_TIME:I = 0x3a98

.field private static final HRM_TAG:Ljava/lang/String; = "HeartRateMonitor"

.field private static final INSERT_SPEED_DB_INTERVAL:I = 0x2710

.field public static SamsungLocationMonitor:Z

.field private static final TAG:Ljava/lang/String;

.field private static accuracy_sdk:F

.field private static altitude_sdk:F

.field private static altitude_sdk_check:Ljava/lang/Double;

.field private static averageSpeed_sdk:F

.field private static bearing_sdk:F

.field private static consumedCalorie_sdk:F

.field private static deviceId:Ljava/lang/String;

.field static fFEConnecting:Z

.field static fFirstConnected:Z

.field private static gpsjoinRetry:Z

.field private static isAvailableBarometerFlag:Z

.field private static isBSACDevice:Z

.field private static isForceMusicPaused:Z

.field private static isGPSpopup:Z

.field private static isReadyGpsOn:Z

.field private static isRecording:Z

.field public static isRestart:Z

.field private static isRun:Z

.field private static isScreenLockState:Z

.field private static lastTotalDistance_sdk:F

.field private static latitude_sdk:Ljava/lang/Double;

.field private static longitude_sdk:Ljava/lang/Double;

.field private static mGpsStartTime:J

.field public static mPrevLocation:Landroid/location/Location;

.field public static mPreviousLatLng:Lcom/google/android/gms/maps/model/LatLng;

.field private static mUsePedoState:Z

.field static noti_id:I

.field private static pserviceType:I

.field private static speed_sdk:F

.field private static time_sdk:J

.field private static tmpSpeedAve:F

.field private static tmpSpeedCnt:F

.field private static tmpSpeedTotal:F

.field private static toggleInsertMapDb:I

.field private static totalDistance_sdk:F


# instance fields
.field private BSACStatus:I

.field private FEStatus:I

.field private FEType:I

.field private GpsStatus:I

.field private HRMStatus:I

.field private SAPHRMStatus:I

.field private currentMode:I

.field public dbSaveLock:Ljava/lang/Object;

.field private gpsconnectCount:J

.field private gpsdisconnectCount:J

.field public handler:Landroid/os/Handler;

.field listenerGoal:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

.field listenerLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;

.field private mAMapLocationClient:Lcom/amap/api/location/LocationManagerProxy;

.field private mAMapLocationService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$AMapLocationService;

.field mBSACDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field private mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field private mBSACEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field private final mBinder:Landroid/os/IBinder;

.field private mConnectedDevicelist:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectedTime:Landroid/text/format/Time;

.field public mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

.field private mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

.field private mCurrLocationStartTime:J

.field private mDumyExerciseId:J

.field private mExerciseId:J

.field mFEDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field private mFEDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field private mFEEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field mGPSDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field private mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field public mGpsHandler:Landroid/os/Handler;

.field mHRMDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field private mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field private mHRMEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field private mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

.field mHRMScanListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;

.field private mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private mHealthSensorC:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

.field private mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

.field private mLastDistance:F

.field private mLastDuration:J

.field private mLastLocationMillis:J

.field private mLastTimeSpeed:J

.field private mListListener:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field private mPreDurationTime:J

.field private mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSensorReceiver:Landroid/content/BroadcastReceiver;

.field mSensorScanListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;

.field mServiceConnection:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

.field mServiceConnectionC:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

.field private mWorkoutState:I

.field private maxSpeed:F

.field private oldLocale:Ljava/util/Locale;

.field type:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 85
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    .line 87
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRun:Z

    .line 88
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording:Z

    .line 89
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isForceMusicPaused:Z

    .line 90
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isScreenLockState:Z

    .line 91
    const-string v0, "ACTION_DEVICE_CONNECTED"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ACTION_DEVICE_CONNECTED:Ljava/lang/String;

    .line 92
    const-string v0, "DEVICE_TYPE"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->DEVICE_TYPE:Ljava/lang/String;

    .line 93
    const-string v0, "DEVICE_ID"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->DEVICE_ID:Ljava/lang/String;

    .line 94
    const/16 v0, 0xe

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->pserviceType:I

    .line 100
    sput v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->noti_id:I

    .line 118
    sput-boolean v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->fFirstConnected:Z

    .line 119
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->fFEConnecting:Z

    .line 123
    sput-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPrevLocation:Landroid/location/Location;

    .line 126
    sput-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreviousLatLng:Lcom/google/android/gms/maps/model/LatLng;

    .line 134
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mUsePedoState:Z

    .line 138
    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->toggleInsertMapDb:I

    .line 139
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedAve:F

    .line 140
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedTotal:F

    .line 141
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedCnt:F

    .line 151
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isGPSpopup:Z

    .line 155
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRestart:Z

    .line 162
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk:F

    .line 165
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->latitude_sdk:Ljava/lang/Double;

    .line 166
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->longitude_sdk:Ljava/lang/Double;

    .line 169
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->lastTotalDistance_sdk:F

    .line 170
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->totalDistance_sdk:F

    .line 171
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->averageSpeed_sdk:F

    .line 172
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->consumedCalorie_sdk:F

    .line 173
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsStartTime:J

    .line 175
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isReadyGpsOn:Z

    .line 176
    sput-boolean v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->SamsungLocationMonitor:Z

    .line 177
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isBSACDevice:Z

    .line 179
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->deviceId:Ljava/lang/String;

    .line 180
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsjoinRetry:Z

    .line 181
    sput-boolean v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isAvailableBarometerFlag:Z

    .line 183
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ANTFEisAvailable:Z

    .line 184
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ANTHRMisAvailable:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 83
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 95
    iput v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->currentMode:I

    .line 97
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    .line 103
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->HRMStatus:I

    .line 104
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->FEStatus:I

    .line 105
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->SAPHRMStatus:I

    .line 106
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->BSACStatus:I

    .line 107
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->GpsStatus:I

    .line 108
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->FEType:I

    .line 109
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedTime:Landroid/text/format/Time;

    .line 111
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 112
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 113
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 114
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 121
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 122
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 124
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationStartTime:J

    .line 125
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreDurationTime:J

    .line 129
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    .line 130
    const-wide/16 v0, 0x270f

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mDumyExerciseId:J

    .line 142
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastTimeSpeed:J

    .line 144
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->maxSpeed:F

    .line 148
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastLocationMillis:J

    .line 149
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsdisconnectCount:J

    .line 150
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsconnectCount:J

    .line 158
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->dbSaveLock:Ljava/lang/Object;

    .line 186
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->listenerLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;

    .line 222
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->listenerGoal:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    .line 247
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    invoke-direct {v0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    .line 303
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$RealtimeHealthBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$RealtimeHealthBinder;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBinder:Landroid/os/IBinder;

    .line 543
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->type:I

    .line 762
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMScanListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;

    .line 1117
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    .line 1432
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mServiceConnectionC:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .line 1445
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mServiceConnection:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .line 1499
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mSensorScanListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;

    .line 1541
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$10;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .line 1636
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .line 2066
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$12;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsHandler:Landroid/os/Handler;

    .line 2133
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 2294
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$14;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLocationEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .line 2358
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$15;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .line 2474
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$16;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 2532
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 2633
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 2964
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationClient:Lcom/amap/api/location/LocationManagerProxy;

    .line 2965
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$AMapLocationService;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$AMapLocationService;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$AMapLocationService;

    .line 2966
    return-void
.end method

.method private LocationMonitorPause(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    .locals 6
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .prologue
    .line 1737
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1738
    .local v1, "param":Landroid/os/Bundle;
    const-string v2, "LocationMonitorPause"

    .line 1739
    .local v2, "param_type":Ljava/lang/String;
    const-string v3, "PARAM_STATE"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1740
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v3, :cond_0

    .line 1742
    :try_start_0
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorPause"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1743
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    new-instance v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    const-string v5, "CMD_REQUEST_EXTRA"

    invoke-direct {v4, v5, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->request(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_6

    .line 1760
    :cond_0
    :goto_0
    return-void

    .line 1744
    :catch_0
    move-exception v0

    .line 1745
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorPause ->RemoteException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1746
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1747
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorPause ->IllegalStateException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1748
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 1749
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorPause ->IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1750
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 1751
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorPause ->ShealthSensorServiceNotBoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1752
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 1753
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorPause ->ShealthSensorNotSupportedException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1754
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_5
    move-exception v0

    .line 1755
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorPause ->ShealthSensorDeviceInUseException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1756
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_6
    move-exception v0

    .line 1757
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorPause ->SHealthSensorInternalErrorException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private LocationMonitorResume(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    .locals 6
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .prologue
    .line 1763
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1764
    .local v1, "param":Landroid/os/Bundle;
    const-string v2, "LocationMonitorResume"

    .line 1765
    .local v2, "param_type":Ljava/lang/String;
    const-string v3, "PARAM_STATE"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1766
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v3, :cond_0

    .line 1768
    :try_start_0
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorResume"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1769
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    new-instance v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    const-string v5, "CMD_REQUEST_EXTRA"

    invoke-direct {v4, v5, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->request(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_6

    .line 1786
    :cond_0
    :goto_0
    return-void

    .line 1770
    :catch_0
    move-exception v0

    .line 1771
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorResume ->RemoteException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1772
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1773
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorResume ->IllegalStateException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1774
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 1775
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorResume ->IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1776
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 1777
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorResume ->ShealthSensorServiceNotBoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1778
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 1779
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorResume ->ShealthSensorNotSupportedException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1780
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_5
    move-exception v0

    .line 1781
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorResume ->ShealthSensorDeviceInUseException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1782
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_6
    move-exception v0

    .line 1783
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v4, "LocationMonitorResume ->SHealthSensorInternalErrorException"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private SamsungLocationMonitorData()V
    .locals 13

    .prologue
    .line 1991
    new-instance v6, Landroid/location/Location;

    const-string v9, "gps"

    invoke-direct {v6, v9}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 1992
    .local v6, "mData":Landroid/location/Location;
    sget v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->speed_sdk:F

    invoke-virtual {v6, v9}, Landroid/location/Location;->setSpeed(F)V

    .line 1993
    sget v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->accuracy_sdk:F

    invoke-virtual {v6, v9}, Landroid/location/Location;->setAccuracy(F)V

    .line 1994
    sget v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk:F

    float-to-double v9, v9

    invoke-virtual {v6, v9, v10}, Landroid/location/Location;->setAltitude(D)V

    .line 1995
    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->latitude_sdk:Ljava/lang/Double;

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    invoke-virtual {v6, v9, v10}, Landroid/location/Location;->setLatitude(D)V

    .line 1996
    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->longitude_sdk:Ljava/lang/Double;

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    invoke-virtual {v6, v9, v10}, Landroid/location/Location;->setLongitude(D)V

    .line 1997
    sget v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->bearing_sdk:F

    invoke-virtual {v6, v9}, Landroid/location/Location;->setBearing(F)V

    .line 1998
    sget-wide v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->time_sdk:J

    invoke-virtual {v6, v9, v10}, Landroid/location/Location;->setTime(J)V

    .line 2000
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    sget v10, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->totalDistance_sdk:F

    iput v10, v9, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    .line 2001
    const-string v9, "SamsungLocationMonitorData"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2002
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    sget v10, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->consumedCalorie_sdk:F

    iput v10, v9, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_sum:F

    .line 2003
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    sget v10, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk:F

    iput v10, v9, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->elevation:F

    .line 2004
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    sget v10, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->speed_sdk:F

    iput v10, v9, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    .line 2007
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getNotificationText()Ljava/lang/String;

    move-result-object v7

    .line 2008
    .local v7, "notitficationText":Ljava/lang/String;
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateNotificationText(Ljava/lang/String;)V

    .line 2010
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2011
    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "SamsungLocationMonitor  DISTANCE : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", SPEED : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", ELEVATION : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->elevation:F

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", ACCURACY : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->accuracy_sdk:F

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", CALORIES : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_sum:F

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2012
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->currentMode:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1

    sget-boolean v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isBSACDevice:Z

    if-nez v9, :cond_1

    .line 2013
    const/4 v9, 0x2

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-static {v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRoundedValue(F)F

    move-result v10

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2014
    const/4 v9, 0x3

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2015
    const/4 v9, 0x6

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->elevation:F

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2016
    const/4 v9, 0x1

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2017
    const/16 v9, 0x17

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getLastETEAchievedPercent()I

    move-result v10

    int-to-float v10, v10

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2018
    const/4 v9, 0x4

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_sum:F

    invoke-static {v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRoundedValue(F)F

    move-result v10

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2019
    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    monitor-enter v10

    .line 2020
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .line 2021
    .local v5, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/4 v9, 0x2

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    const/16 v12, 0x500

    invoke-interface {v5, v9, v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 2022
    const/4 v9, 0x6

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->elevation:F

    const/16 v12, 0x500

    invoke-interface {v5, v9, v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 2023
    const/4 v9, 0x3

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    const/16 v12, 0x500

    invoke-interface {v5, v9, v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 2024
    const/4 v9, 0x4

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_sum:F

    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRoundedValue(F)F

    move-result v11

    const/4 v12, 0x0

    invoke-interface {v5, v9, v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    goto :goto_0

    .line 2026
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2029
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    sget-boolean v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mUsePedoState:Z

    if-nez v9, :cond_2

    .line 2030
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->onLocationChanged(Landroid/location/Location;)V

    .line 2032
    :cond_2
    sget-boolean v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording:Z

    if-eqz v9, :cond_5

    .line 2033
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2034
    .local v0, "curTime":J
    sget v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedTotal:F

    sget v10, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->speed_sdk:F

    const/high16 v11, 0x45610000    # 3600.0f

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    sput v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedTotal:F

    .line 2035
    sget v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedCnt:F

    const/high16 v10, 0x3f800000    # 1.0f

    add-float/2addr v9, v10

    sput v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedCnt:F

    .line 2036
    sget v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedTotal:F

    sget v10, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedCnt:F

    div-float/2addr v9, v10

    sput v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedAve:F

    .line 2037
    iget-wide v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastTimeSpeed:J

    const-wide/16 v11, 0x2710

    add-long/2addr v9, v11

    cmp-long v9, v9, v0

    if-ltz v9, :cond_3

    sget v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedAve:F

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->maxSpeed:F

    cmpl-float v9, v9, v10

    if-lez v9, :cond_5

    .line 2038
    :cond_3
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 2039
    .local v8, "values":Landroid/content/ContentValues;
    const-string/jumbo v9, "sample_time"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2040
    iget-wide v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-nez v9, :cond_4

    .line 2041
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getExerciseId()J

    move-result-wide v9

    iput-wide v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    .line 2042
    iget-wide v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealtimeExerciseId(J)V

    .line 2044
    :cond_4
    const-string v9, "exercise__id"

    iget-wide v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2045
    const-string/jumbo v9, "user_device__id"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getGpsDeviceID()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2046
    const-string/jumbo v9, "speed_per_hour"

    sget v10, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedAve:F

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2047
    const-string v9, "data_type"

    const v10, 0x493e1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2049
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9, v10, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 2055
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->doSaveExercise()V

    .line 2056
    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastTimeSpeed:J

    .line 2057
    const/4 v9, 0x0

    sput v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedTotal:F

    .line 2058
    const/4 v9, 0x0

    sput v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedCnt:F

    .line 2059
    sget v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedAve:F

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->maxSpeed:F

    cmpl-float v9, v9, v10

    if-lez v9, :cond_5

    .line 2060
    sget v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedAve:F

    iput v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->maxSpeed:F

    .line 2064
    .end local v0    # "curTime":J
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_5
    return-void

    .line 2050
    .restart local v0    # "curTime":J
    .restart local v8    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v4

    .line 2051
    .local v4, "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "insert exception -"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2052
    .end local v4    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    :catch_1
    move-exception v2

    .line 2053
    .local v2, "e":Ljava/lang/Exception;
    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v10, "Speed Save Exception"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    return-object v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->DEVICE_ID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object v0
.end method

.method static synthetic access$1200()Z
    .locals 1

    .prologue
    .line 83
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ANTHRMisAvailable:Z

    return v0
.end method

.method static synthetic access$1202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 83
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ANTHRMisAvailable:Z

    return p0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->notifyExerciseStartToWorkForLife()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGoal(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Landroid/text/format/Time;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedTime:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->notifyExerciseStopToWorkForLife()V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object p1
.end method

.method static synthetic access$1900()Z
    .locals 1

    .prologue
    .line 83
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isAvailableBarometerFlag:Z

    return v0
.end method

.method static synthetic access$1902(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 83
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isAvailableBarometerFlag:Z

    return p0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # J

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->checkTTSOnUpdateLapClock(J)V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getConnectedDeviceList()V

    return-void
.end method

.method static synthetic access$2100()Z
    .locals 1

    .prologue
    .line 83
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ANTFEisAvailable:Z

    return v0
.end method

.method static synthetic access$2102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 83
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ANTFEisAvailable:Z

    return p0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateHRMDataState()V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateBSACDataState()V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->SamsungLocationMonitorData()V

    return-void
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastLocationMillis:J

    return-wide v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # J

    .prologue
    .line 83
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastLocationMillis:J

    return-wide p1
.end method

.method static synthetic access$2900()Z
    .locals 1

    .prologue
    .line 83
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isGPSpopup:Z

    return v0
.end method

.method static synthetic access$2902(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 83
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isGPSpopup:Z

    return p0
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 83
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->notifygpsstate(Z)V

    return-void
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsconnectCount:J

    return-wide v0
.end method

.method static synthetic access$3102(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # J

    .prologue
    .line 83
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsconnectCount:J

    return-wide p1
.end method

.method static synthetic access$3108(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J
    .locals 4
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsconnectCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsconnectCount:J

    return-wide v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsdisconnectCount:J

    return-wide v0
.end method

.method static synthetic access$3202(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # J

    .prologue
    .line 83
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsdisconnectCount:J

    return-wide p1
.end method

.method static synthetic access$3208(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J
    .locals 4
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsdisconnectCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsdisconnectCount:J

    return-wide v0
.end method

.method static synthetic access$3302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 83
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mUsePedoState:Z

    return p0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationStartTime:J

    return-wide v0
.end method

.method static synthetic access$3402(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # J

    .prologue
    .line 83
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationStartTime:J

    return-wide p1
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    return-object p1
.end method

.method static synthetic access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreDurationTime:J

    return-wide v0
.end method

.method static synthetic access$3614(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;J)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # J

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreDurationTime:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreDurationTime:J

    return-wide v0
.end method

.method static synthetic access$3700()J
    .locals 2

    .prologue
    .line 83
    sget-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsStartTime:J

    return-wide v0
.end method

.method static synthetic access$3702(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 83
    sput-wide p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsStartTime:J

    return-wide p0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    return-object p1
.end method

.method static synthetic access$3902(Ljava/lang/Double;)Ljava/lang/Double;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Double;

    .prologue
    .line 83
    sput-object p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->latitude_sdk:Ljava/lang/Double;

    return-object p0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->currentMode:I

    return v0
.end method

.method static synthetic access$4002(Ljava/lang/Double;)Ljava/lang/Double;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Double;

    .prologue
    .line 83
    sput-object p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->longitude_sdk:Ljava/lang/Double;

    return-object p0
.end method

.method static synthetic access$4102(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 83
    sput-wide p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->time_sdk:J

    return-wide p0
.end method

.method static synthetic access$4200()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk_check:Ljava/lang/Double;

    return-object v0
.end method

.method static synthetic access$4202(Ljava/lang/Double;)Ljava/lang/Double;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Double;

    .prologue
    .line 83
    sput-object p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk_check:Ljava/lang/Double;

    return-object p0
.end method

.method static synthetic access$4302(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 83
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk:F

    return p0
.end method

.method static synthetic access$4402(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 83
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->accuracy_sdk:F

    return p0
.end method

.method static synthetic access$4502(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 83
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->speed_sdk:F

    return p0
.end method

.method static synthetic access$4602(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 83
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->bearing_sdk:F

    return p0
.end method

.method static synthetic access$4702(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 83
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->totalDistance_sdk:F

    return p0
.end method

.method static synthetic access$4802(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 83
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->consumedCalorie_sdk:F

    return p0
.end method

.method static synthetic access$4902(F)F
    .locals 0
    .param p0, "x0"    # F

    .prologue
    .line 83
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->averageSpeed_sdk:F

    return p0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    return-wide v0
.end method

.method static synthetic access$5002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 83
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isReadyGpsOn:Z

    return p0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # J

    .prologue
    .line 83
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    return-wide p1
.end method

.method static synthetic access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # I

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGpsStatus(I)V

    return-void
.end method

.method static synthetic access$5300(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getExerciseId()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$5400(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # F

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->checkTTSOnUpdateVaule(F)V

    return-void
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .param p1, "x1"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    return p1
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ACTION_DEVICE_CONNECTED:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->DEVICE_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method private checkTTSOnUpdateLapClock(J)V
    .locals 9
    .param p1, "second"    # J

    .prologue
    .line 2935
    const-wide/16 v3, 0x0

    .line 2936
    .local v3, "pretime":J
    const-wide/16 v1, 0x0

    .line 2937
    .local v1, "nowtime":J
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeFrequencyOfAudioGuide()I

    move-result v0

    .line 2938
    .local v0, "frequency":I
    packed-switch v0, :pswitch_data_0

    .line 2957
    :goto_0
    cmp-long v5, v3, v1

    if-eqz v5, :cond_0

    .line 2958
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->playTTS()V

    .line 2960
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastDuration:J

    .line 2961
    return-void

    .line 2940
    :pswitch_0
    iget-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastDuration:J

    const-wide/16 v7, 0x12c

    div-long v3, v5, v7

    .line 2941
    const-wide/16 v5, 0x12c

    div-long v1, p1, v5

    .line 2942
    goto :goto_0

    .line 2944
    :pswitch_1
    iget-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastDuration:J

    const-wide/16 v7, 0x258

    div-long v3, v5, v7

    .line 2945
    const-wide/16 v5, 0x258

    div-long v1, p1, v5

    .line 2946
    goto :goto_0

    .line 2948
    :pswitch_2
    iget-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastDuration:J

    const-wide/16 v7, 0x708

    div-long v3, v5, v7

    .line 2949
    const-wide/16 v5, 0x708

    div-long v1, p1, v5

    .line 2950
    goto :goto_0

    .line 2952
    :pswitch_3
    iget-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastDuration:J

    const-wide/16 v7, 0xe10

    div-long v3, v5, v7

    .line 2953
    const-wide/16 v5, 0xe10

    div-long v1, p1, v5

    goto :goto_0

    .line 2938
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private checkTTSOnUpdateVaule(F)V
    .locals 13
    .param p1, "distance"    # F

    .prologue
    const-wide/16 v11, 0x7d0

    const-wide/16 v9, 0x3e8

    const-wide/16 v7, 0x1f4

    .line 2910
    const-wide/16 v3, 0x0

    .line 2911
    .local v3, "predistance":J
    const-wide/16 v1, 0x0

    .line 2912
    .local v1, "nowdistance":J
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeFrequencyOfAudioGuide()I

    move-result v0

    .line 2913
    .local v0, "frequency":I
    packed-switch v0, :pswitch_data_0

    .line 2928
    :goto_0
    cmp-long v5, v3, v1

    if-eqz v5, :cond_0

    .line 2929
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->playTTS()V

    .line 2931
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastDistance:F

    .line 2932
    return-void

    .line 2915
    :pswitch_0
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastDistance:F

    float-to-long v5, v5

    div-long v3, v5, v7

    .line 2916
    float-to-long v5, p1

    div-long v1, v5, v7

    .line 2917
    goto :goto_0

    .line 2919
    :pswitch_1
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastDistance:F

    float-to-long v5, v5

    div-long v3, v5, v9

    .line 2920
    float-to-long v5, p1

    div-long v1, v5, v9

    .line 2921
    goto :goto_0

    .line 2923
    :pswitch_2
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastDistance:F

    float-to-long v5, v5

    div-long v3, v5, v11

    .line 2924
    float-to-long v5, p1

    div-long v1, v5, v11

    goto :goto_0

    .line 2913
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getConnectedDeviceList()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1694
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v1, :cond_0

    .line 1697
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v2, 0x4

    const/16 v3, 0x271c

    const/4 v4, 0x6

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedDevicelist:Ljava/util/List;

    .line 1699
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedDevicelist:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedDevicelist:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v5, :cond_2

    .line 1700
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->SamsungLocationMonitor:Z

    if-nez v1, :cond_1

    .line 1701
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedDevicelist:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 1705
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GPS Device name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1706
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsjoinRetry:Z

    if-eqz v1, :cond_0

    .line 1707
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGpsTracking(Z)V

    .line 1734
    :cond_0
    :goto_1
    return-void

    .line 1703
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedDevicelist:Ljava/util/List;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 1722
    :catch_0
    move-exception v0

    .line 1723
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v2, "SamsungLocationMonitor getConnectedDeviceList ->IndexOutOfBoundsException"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1710
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_2
    :try_start_1
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v2, "Did not get device information. retry. "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1711
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v2, 0x4

    const/16 v3, 0x271c

    const/4 v4, 0x6

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedDevicelist:Ljava/util/List;

    .line 1713
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedDevicelist:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedDevicelist:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v5, :cond_3

    .line 1714
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedDevicelist:Ljava/util/List;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 1715
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsjoinRetry:Z

    if-eqz v1, :cond_0

    .line 1716
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGpsTracking(Z)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_1

    .line 1724
    :catch_1
    move-exception v0

    .line 1725
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v2, "SamsungLocationMonitor getConnectedDeviceList ->RemoteException"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1719
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_3
    :try_start_2
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "retry fail"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 1726
    :catch_2
    move-exception v0

    .line 1727
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v2, "SamsungLocationMonitor getConnectedDeviceList ->IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1728
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 1729
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v2, "SamsungLocationMonitor getConnectedDeviceList ->ShealthSensorServiceNotBoundException"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1730
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 1731
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v2, "SamsungLocationMonitor getConnectedDeviceList ->ShealthSensorNotSupportedException"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getExerciseId()J
    .locals 6

    .prologue
    .line 2797
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2798
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v3, "exercise_info__id"

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v4, v4, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2799
    const-string v3, "exercise_type"

    const/16 v4, 0x4e21

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2800
    const-string/jumbo v3, "start_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2801
    const-string/jumbo v3, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2802
    const-string v3, "end_time"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2803
    const-string v3, "comment"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2804
    const-string v3, "input_source_type"

    const v4, 0x3f7a2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2805
    const/4 v2, 0x0

    .line 2807
    .local v2, "rowId":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2811
    :goto_0
    if-nez v2, :cond_0

    .line 2812
    const-wide/16 v3, -0x1

    .line 2813
    :goto_1
    return-wide v3

    .line 2808
    :catch_0
    move-exception v1

    .line 2809
    .local v1, "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert exception -"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2813
    .end local v1    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    :cond_0
    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    goto :goto_1
.end method

.method private gpsRecord()V
    .locals 4

    .prologue
    .line 1860
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v1, :cond_0

    .line 1862
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->record(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1869
    :goto_0
    return-void

    .line 1863
    :catch_0
    move-exception v0

    .line 1864
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v2, "gpsRecord - record Exception"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1867
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v2, "gpsRecord - mGPSDevice is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private gpsjoin()V
    .locals 6

    .prologue
    .line 1882
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v2, :cond_1

    .line 1885
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1886
    .local v0, "android_id":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->deviceId:Ljava/lang/String;

    .line 1887
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->deviceId:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGpsDeviceID(Ljava/lang/String;)V

    .line 1888
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLocationEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    .line 1889
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 1891
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getExerciseId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1895
    :goto_0
    :try_start_2
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealtimeExerciseId(J)V

    .line 1910
    .end local v0    # "android_id":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 1892
    .restart local v0    # "android_id":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1893
    .local v1, "e":Ljava/lang/Exception;
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mDumyExerciseId:J

    iput-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 1898
    .end local v0    # "android_id":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 1899
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "gpsjoin - join retry : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsjoinRetry:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1900
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsjoinRetry:Z

    if-nez v2, :cond_0

    .line 1901
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsjoinRetry:Z

    .line 1902
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGpsStatus(I)V

    .line 1903
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mServiceConnection:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    goto :goto_1

    .line 1907
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v3, "gpsjoin - mGPSDevice is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private gpsleave()V
    .locals 3

    .prologue
    .line 1914
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v1, :cond_0

    .line 1916
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1923
    :goto_0
    return-void

    .line 1917
    :catch_0
    move-exception v0

    .line 1918
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v2, "gpsleave leave Exception"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1921
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v2, "gpsleave - mGPSDevice is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static initService(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1303
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1304
    .local v0, "iService":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1305
    return-void
.end method

.method private isBSACDeviceCheck()V
    .locals 3

    .prologue
    .line 989
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isBSACDeviceCheck mBSACDevice : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v0, :cond_0

    .line 992
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isBSACDevice:Z

    .line 996
    :goto_0
    return-void

    .line 994
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isBSACDevice:Z

    goto :goto_0
.end method

.method public static isRecording()Z
    .locals 1

    .prologue
    .line 267
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording:Z

    return v0
.end method

.method public static isRun()Z
    .locals 1

    .prologue
    .line 259
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRun:Z

    return v0
.end method

.method public static isScreenLockState()Z
    .locals 1

    .prologue
    .line 291
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isScreenLockState:Z

    return v0
.end method

.method private notifyExerciseStartToWorkForLife()V
    .locals 2

    .prologue
    .line 951
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.measure.realtime.EXERCISE_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->sendBroadcast(Landroid/content/Intent;)V

    .line 952
    return-void
.end method

.method private notifyExerciseStopToWorkForLife()V
    .locals 2

    .prologue
    .line 955
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.measure.realtime.EXERCISE_STOP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->sendBroadcast(Landroid/content/Intent;)V

    .line 956
    return-void
.end method

.method private notifygpsstate(Z)V
    .locals 7
    .param p1, "enable"    # Z

    .prologue
    .line 1927
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1928
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .line 1929
    .local v2, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "onNotiGpsState : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1930
    invoke-interface {v2, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onNotiGpsState(Z)V

    goto :goto_0

    .line 1932
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1934
    :catch_0
    move-exception v0

    .line 1935
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "notifygpsstate: Exception"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1937
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 1932
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private onLocationChanged(Landroid/location/Location;)V
    .locals 12
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v7, 0x0

    .line 1940
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    cmpl-double v5, v5, v7

    if-nez v5, :cond_1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    cmpl-double v5, v5, v7

    if-nez v5, :cond_1

    .line 1941
    const-string v5, "GPS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Latitude : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Longitude : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1988
    :cond_0
    :goto_0
    return-void

    .line 1945
    :cond_1
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPrevLocation:Landroid/location/Location;

    if-nez v5, :cond_2

    .line 1946
    sput-object p1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPrevLocation:Landroid/location/Location;

    .line 1947
    new-instance v5, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    sput-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreviousLatLng:Lcom/google/android/gms/maps/model/LatLng;

    goto :goto_0

    .line 1951
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    monitor-enter v6

    .line 1952
    :try_start_0
    sget-boolean v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mUsePedoState:Z

    if-nez v5, :cond_3

    sget-boolean v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isBSACDevice:Z

    if-nez v5, :cond_3

    .line 1953
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRoundedValue(F)F

    move-result v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->checkTTSOnUpdateVaule(F)V

    .line 1955
    :cond_3
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Latitude : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", Longitude : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1956
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .line 1957
    .local v3, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    invoke-interface {v3, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onNewLocation(Landroid/location/Location;)V

    goto :goto_1

    .line 1959
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_4
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1960
    sput-object p1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPrevLocation:Landroid/location/Location;

    .line 1961
    new-instance v5, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    sput-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreviousLatLng:Lcom/google/android/gms/maps/model/LatLng;

    .line 1963
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    const/16 v6, 0x7d1

    if-ne v5, v6, :cond_0

    iget-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    cmp-long v5, v5, v10

    if-eqz v5, :cond_0

    sget-boolean v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isBSACDevice:Z

    if-nez v5, :cond_0

    .line 1964
    sget v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->toggleInsertMapDb:I

    rem-int/lit8 v5, v5, 0xa

    if-eqz v5, :cond_5

    sget v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->lastTotalDistance_sdk:F

    const/high16 v6, 0x41200000    # 10.0f

    add-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->totalDistance_sdk:F

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_7

    .line 1965
    :cond_5
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1966
    .local v4, "values":Landroid/content/ContentValues;
    iget-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    cmp-long v5, v5, v10

    if-nez v5, :cond_6

    .line 1967
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getExerciseId()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    .line 1968
    iget-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealtimeExerciseId(J)V

    .line 1970
    :cond_6
    const-string/jumbo v5, "sample_time"

    sget-wide v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->time_sdk:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1971
    const-string v5, "exercise__id"

    iget-wide v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1972
    const-string/jumbo v5, "user_device__id"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getGpsDeviceID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1973
    const-string v5, "latitude"

    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->latitude_sdk:Ljava/lang/Double;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1974
    const-string v5, "longitude"

    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->longitude_sdk:Ljava/lang/Double;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1975
    const-string v5, "accuracy"

    sget v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->accuracy_sdk:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1976
    const-string v5, "altitude"

    sget v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1978
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1979
    sget v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->totalDistance_sdk:F

    sput v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->lastTotalDistance_sdk:F
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1986
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_7
    :goto_2
    sget v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->toggleInsertMapDb:I

    add-int/lit8 v5, v5, 0x1

    sput v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->toggleInsertMapDb:I

    goto/16 :goto_0

    .line 1980
    .restart local v4    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v2

    .line 1981
    .local v2, "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insert exception -"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1982
    .end local v2    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    :catch_1
    move-exception v0

    .line 1983
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v6, " Location Cal Data Save Exception"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private playTTS()V
    .locals 15

    .prologue
    .line 2817
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTypeOfFeedBack()J

    move-result-wide v1

    .line 2818
    .local v1, "feedback":J
    const-string v5, ""

    .line 2820
    .local v5, "strTTS":Ljava/lang/String;
    new-instance v6, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 2822
    .local v6, "unit":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    const-wide/16 v7, 0x1

    and-long/2addr v7, v1

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_0

    .line 2823
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905e8

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    const/high16 v12, 0x45610000    # 3600.0f

    div-float/2addr v11, v12

    float-to-int v11, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    const/high16 v12, 0x42700000    # 60.0f

    div-float/2addr v11, v12

    const/high16 v12, 0x42700000    # 60.0f

    rem-float/2addr v11, v12

    float-to-int v11, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2825
    :cond_0
    const-wide/16 v7, 0x2

    and-long/2addr v7, v1

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_1

    .line 2826
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v7

    const-string v8, "km"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 2827
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905e9

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2833
    :cond_1
    :goto_0
    const-wide/16 v7, 0x4

    and-long/2addr v7, v1

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_2

    .line 2834
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v7

    const-string v8, "km"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2835
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905eb

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    float-to-double v11, v11

    const-wide v13, 0x400ccccccccccccdL    # 3.6

    mul-double/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2841
    :cond_2
    :goto_1
    const-wide/16 v7, 0x8

    and-long/2addr v7, v1

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->speed_count:I

    if-lez v7, :cond_3

    .line 2842
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget-wide v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_speed:D

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v9, v9, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->speed_count:I

    int-to-double v9, v9

    div-double/2addr v7, v9

    double-to-float v0, v7

    .line 2843
    .local v0, "avgSpped":F
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v7

    const-string v8, "km"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 2844
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905ed

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    float-to-double v11, v0

    const-wide v13, 0x400ccccccccccccdL    # 3.6

    mul-double/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2850
    .end local v0    # "avgSpped":F
    :cond_3
    :goto_2
    const-wide/16 v7, 0x10

    and-long/2addr v7, v1

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-lez v7, :cond_4

    .line 2851
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v7

    const-string v8, "km"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 2852
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905ef

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-wide/high16 v11, 0x406b000000000000L    # 216.0

    iget-object v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v13, v13, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    float-to-double v13, v13

    div-double/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2858
    :cond_4
    :goto_3
    const-wide/16 v7, 0x20

    and-long/2addr v7, v1

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget-wide v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_speed:D

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v9, v9, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->speed_count:I

    int-to-double v9, v9

    div-double/2addr v7, v9

    const-wide/16 v9, 0x0

    cmpl-double v7, v7, v9

    if-lez v7, :cond_5

    .line 2859
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget-wide v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_speed:D

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v9, v9, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->speed_count:I

    int-to-double v9, v9

    div-double/2addr v7, v9

    double-to-float v0, v7

    .line 2860
    .restart local v0    # "avgSpped":F
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v7

    const-string v8, "km"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 2861
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905f1

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-wide/high16 v11, 0x406b000000000000L    # 216.0

    float-to-double v13, v0

    div-double/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2867
    .end local v0    # "avgSpped":F
    :cond_5
    :goto_4
    const-wide/16 v7, 0x40

    and-long/2addr v7, v1

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_6

    .line 2868
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905f3

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_sum:F

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2870
    :cond_6
    const-wide/16 v7, 0x80

    and-long/2addr v7, v1

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_7

    .line 2871
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getType()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_11

    .line 2872
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    sub-float/2addr v7, v8

    float-to-int v4, v7

    .line 2873
    .local v4, "remain_duration":I
    if-ltz v4, :cond_7

    .line 2874
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905f6

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    div-int/lit16 v11, v4, 0xe10

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    div-int/lit8 v11, v4, 0x3c

    rem-int/lit8 v11, v11, 0x3c

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2889
    .end local v4    # "remain_duration":I
    :cond_7
    :goto_5
    const-wide/16 v7, 0x100

    and-long/2addr v7, v1

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_8

    .line 2890
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905f7

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_heartrate:F

    float-to-int v11, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2892
    :cond_8
    const-wide/16 v7, 0x200

    and-long/2addr v7, v1

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_9

    .line 2893
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905f8

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_cadence:F

    float-to-int v11, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2895
    :cond_9
    const-wide/16 v7, 0x400

    and-long/2addr v7, v1

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_a

    .line 2896
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v7

    const-string v8, "km"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 2897
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905ed

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->elevation:F

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2903
    :cond_a
    :goto_6
    const-string v7, ""

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 2904
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 2905
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 2907
    :cond_b
    return-void

    .line 2830
    :cond_c
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905ea

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    float-to-int v11, v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertMetersToMiles(I)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 2838
    :cond_d
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905ec

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    float-to-double v11, v11

    const-wide v13, 0x400ccccccccccccdL    # 3.6

    mul-double/2addr v11, v13

    double-to-int v11, v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertMetersToMiles(I)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 2847
    .restart local v0    # "avgSpped":F
    :cond_e
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905ee

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    float-to-double v11, v0

    const-wide v13, 0x400ccccccccccccdL    # 3.6

    mul-double/2addr v11, v13

    double-to-int v11, v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertMetersToMiles(I)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    .line 2855
    .end local v0    # "avgSpped":F
    :cond_f
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905f0

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-wide/high16 v11, 0x406b000000000000L    # 216.0

    iget-object v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v13, v13, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    float-to-double v13, v13

    div-double/2addr v11, v13

    double-to-int v11, v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertMetersToMiles(I)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_3

    .line 2864
    .restart local v0    # "avgSpped":F
    :cond_10
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905f2

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-wide/high16 v11, 0x406b000000000000L    # 216.0

    float-to-double v13, v0

    div-double/2addr v11, v13

    double-to-int v11, v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertMetersToMiles(I)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4

    .line 2877
    .end local v0    # "avgSpped":F
    :cond_11
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getType()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_7

    .line 2878
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getValue()I

    move-result v7

    int-to-float v7, v7

    const/high16 v8, 0x447a0000    # 1000.0f

    div-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    sub-float v3, v7, v8

    .line 2879
    .local v3, "remain_distance":F
    float-to-double v7, v3

    const-wide/16 v9, 0x0

    cmpl-double v7, v7, v9

    if-ltz v7, :cond_7

    .line 2880
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v7

    const-string v8, "km"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 2881
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905f4

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 2884
    :cond_12
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905f5

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    float-to-int v11, v3

    invoke-static {v11}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertMetersToMiles(I)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 2900
    .end local v3    # "remain_distance":F
    :cond_13
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0905ee

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->elevation:F

    float-to-int v11, v11

    mul-int/lit8 v11, v11, 0x64

    int-to-float v11, v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToFeet(F)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_6
.end method

.method private setGoal(Z)Z
    .locals 8
    .param p1, "startWorkout"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 511
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v2

    .line 512
    .local v2, "type":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalValue()I

    move-result v3

    .line 513
    .local v3, "value":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v1

    .line 515
    .local v1, "teLevel":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 517
    if-eqz p1, :cond_2

    .line 518
    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->currentMode:I

    if-ne v6, v4, :cond_1

    .line 519
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getHRMStatus()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    move v0, v4

    .line 527
    .local v0, "hrmPaired":Z
    :goto_0
    invoke-static {v2, v3, v1, v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->setGoal(IIIZZ)V

    .line 528
    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v6

    const/4 v7, 0x5

    if-ne v6, v7, :cond_3

    :goto_1
    return v4

    .end local v0    # "hrmPaired":Z
    :cond_0
    move v0, v5

    .line 519
    goto :goto_0

    .line 521
    :cond_1
    const/4 v0, 0x1

    .restart local v0    # "hrmPaired":Z
    goto :goto_0

    .line 524
    .end local v0    # "hrmPaired":Z
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "hrmPaired":Z
    goto :goto_0

    :cond_3
    move v4, v5

    .line 528
    goto :goto_1
.end method

.method private setGpsDeviceID(Ljava/lang/String;)V
    .locals 3
    .param p1, "SLMDeviceId"    # Ljava/lang/String;

    .prologue
    .line 1876
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setGpsDeviceID:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1877
    sput-object p1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->deviceId:Ljava/lang/String;

    .line 1878
    return-void
.end method

.method private setGpsStatus(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 2287
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setGPStatus:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2288
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->GpsStatus:I

    if-eq v0, p1, :cond_0

    .line 2290
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->GpsStatus:I

    .line 2292
    :cond_0
    return-void
.end method

.method public static setMusicPause(Z)V
    .locals 0
    .param p0, "isPause"    # Z

    .prologue
    .line 287
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isForceMusicPaused:Z

    .line 288
    return-void
.end method

.method public static setRecording(Z)V
    .locals 0
    .param p0, "isRecording"    # Z

    .prologue
    .line 271
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording:Z

    .line 272
    if-eqz p0, :cond_0

    .line 274
    invoke-static {}, Lcom/sec/android/app/shealth/backup/server/BackupNotifier;->blockBackup()V

    .line 280
    :goto_0
    return-void

    .line 278
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/backup/server/BackupNotifier;->unblockBackup()V

    goto :goto_0
.end method

.method public static setScreenLockState(Z)V
    .locals 0
    .param p0, "isLock"    # Z

    .prologue
    .line 295
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isScreenLockState:Z

    .line 296
    return-void
.end method

.method private startAMapGpsTracking()V
    .locals 6

    .prologue
    .line 3010
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationClient:Lcom/amap/api/location/LocationManagerProxy;

    if-nez v0, :cond_0

    .line 3011
    invoke-static {p0}, Lcom/amap/api/location/LocationManagerProxy;->getInstance(Landroid/content/Context;)Lcom/amap/api/location/LocationManagerProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationClient:Lcom/amap/api/location/LocationManagerProxy;

    .line 3012
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationClient:Lcom/amap/api/location/LocationManagerProxy;

    if-eqz v0, :cond_0

    .line 3013
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationClient:Lcom/amap/api/location/LocationManagerProxy;

    const-string v1, "gps"

    const-wide/16 v2, 0x7d0

    const/high16 v4, 0x41200000    # 10.0f

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$AMapLocationService;

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/location/LocationManagerProxy;->requestLocationUpdates(Ljava/lang/String;JFLcom/amap/api/location/AMapLocationListener;)V

    .line 3016
    :cond_0
    return-void
.end method

.method private startSensor(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Z)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .param p2, "isHRM"    # Z

    .prologue
    const/4 v2, 0x1

    .line 804
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v1, :cond_1

    .line 805
    if-eqz p2, :cond_0

    .line 806
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setHRMStatus(I)V

    .line 809
    :cond_0
    if-eqz p2, :cond_2

    .line 810
    :try_start_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 811
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    .line 827
    :goto_0
    if-nez p2, :cond_1

    .line 828
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setBSACStatus(I)V

    .line 829
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 832
    :cond_1
    return-void

    .line 814
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_0

    .line 815
    :catch_0
    move-exception v0

    .line 816
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 817
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 818
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 819
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 820
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 821
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 822
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 823
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_4
    move-exception v0

    .line 824
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0
.end method

.method private stopAMapGpsTracking()V
    .locals 2

    .prologue
    .line 3019
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationClient:Lcom/amap/api/location/LocationManagerProxy;

    if-eqz v0, :cond_0

    .line 3020
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationClient:Lcom/amap/api/location/LocationManagerProxy;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$AMapLocationService;

    invoke-virtual {v0, v1}, Lcom/amap/api/location/LocationManagerProxy;->removeUpdates(Lcom/amap/api/location/AMapLocationListener;)V

    .line 3021
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationClient:Lcom/amap/api/location/LocationManagerProxy;

    invoke-virtual {v0}, Lcom/amap/api/location/LocationManagerProxy;->destory()V

    .line 3022
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mAMapLocationClient:Lcom/amap/api/location/LocationManagerProxy;

    .line 3024
    :cond_0
    return-void
.end method

.method private stopSensor(Z)V
    .locals 7
    .param p1, "isHRM"    # Z

    .prologue
    const/4 v6, 0x0

    .line 835
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v4, :cond_0

    .line 837
    if-eqz p1, :cond_1

    .line 838
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 848
    :cond_0
    :goto_0
    if-eqz p1, :cond_2

    .line 849
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "setHRMStatus - Disconnected"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setHRMStatus(I)V

    .line 857
    :goto_1
    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getHRMUID()Ljava/lang/String;

    move-result-object v3

    .line 860
    .local v3, "mID":Ljava/lang/String;
    :goto_2
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 861
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .line 862
    .local v2, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/4 v4, 0x0

    const/4 v6, 0x3

    invoke-interface {v2, v3, v4, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onChannelStateChanged(Ljava/lang/String;II)V

    goto :goto_3

    .line 864
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 866
    :catch_0
    move-exception v0

    .line 867
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "stopBSAC : Exception"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    return-void

    .line 840
    .end local v3    # "mID":Ljava/lang/String;
    :cond_1
    :try_start_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 841
    :catch_1
    move-exception v0

    .line 842
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "stopBSAC : RemoteException"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 843
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v0

    .line 844
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "stopBSAC : ShealthSensorServiceNotBoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 853
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :cond_2
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "setBSACStatus - Disconnected"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 854
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setBSACStatus(I)V

    goto :goto_1

    .line 857
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getBSACUID()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 864
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "mID":Ljava/lang/String;
    :cond_4
    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4
.end method

.method private updateBSACDataState()V
    .locals 2

    .prologue
    .line 1313
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "updateBSACDataState"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1314
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateDataState(Z)V

    .line 1315
    return-void
.end method

.method private updateDataState(Z)V
    .locals 6
    .param p1, "isHRM"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1319
    if-eqz p1, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v4, :cond_2

    .line 1320
    .local v1, "isFlag":Z
    :cond_0
    :goto_0
    if-eqz p1, :cond_4

    const-string v2, "HRM"

    .line 1322
    .local v2, "tmpTag":Ljava/lang/String;
    :goto_1
    if-eqz v1, :cond_1

    .line 1323
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    const/16 v4, 0x7d1

    if-ne v3, v4, :cond_6

    .line 1325
    :try_start_0
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "mHRMDevice - startReceivingData"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    if-eqz p1, :cond_5

    .line 1327
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1363
    :cond_1
    :goto_2
    return-void

    .end local v1    # "isFlag":Z
    .end local v2    # "tmpTag":Ljava/lang/String;
    :cond_2
    move v1, v3

    .line 1319
    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v4, :cond_0

    move v1, v3

    goto :goto_0

    .line 1320
    .restart local v1    # "isFlag":Z
    :cond_4
    const-string v2, "BSAC"

    goto :goto_1

    .line 1329
    .restart local v2    # "tmpTag":Ljava/lang/String;
    :cond_5
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_2

    .line 1330
    :catch_0
    move-exception v0

    .line 1331
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataState : WORKOUT_STATE_RUNNING ->RemoteException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1332
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1333
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataState : WORKOUT_STATE_RUNNING ->IllegalArgumentException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1334
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 1335
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataState : WORKOUT_STATE_RUNNING ->IllegalStateException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1336
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 1337
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataState : WORKOUT_STATE_RUNNING ->ShealthSensorServiceNotBoundException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1338
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 1339
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataState : WORKOUT_STATE_RUNNING ->ShealthSensorDeviceInUseException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1340
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v0

    .line 1341
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataState : WORKOUT_STATE_RUNNING ->SHealthSensorInternalErrorException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1346
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :cond_6
    if-eqz p1, :cond_7

    .line 1347
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_2 .. :try_end_2} :catch_a

    goto/16 :goto_2

    .line 1350
    :catch_6
    move-exception v0

    .line 1351
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataState : !WORKOUT_STATE_RUNNING ->RemoteException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1349
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_3 .. :try_end_3} :catch_a

    goto/16 :goto_2

    .line 1352
    :catch_7
    move-exception v0

    .line 1353
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataState : !WORKOUT_STATE_RUNNING ->IllegalArgumentException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1354
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_8
    move-exception v0

    .line 1355
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataState : !WORKOUT_STATE_RUNNING ->IllegalStateException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1356
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_9
    move-exception v0

    .line 1357
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataState : !WORKOUT_STATE_RUNNING ->ShealthSensorServiceNotBoundException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1358
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_a
    move-exception v0

    .line 1359
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataState : !WORKOUT_STATE_RUNNING ->SHealthSensorInternalErrorException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private updateHRMDataState()V
    .locals 2

    .prologue
    .line 1308
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "updateHRMDataState"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1309
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateDataState(Z)V

    .line 1310
    return-void
.end method


# virtual methods
.method public doSaveExercise()V
    .locals 23

    .prologue
    .line 3027
    const-wide/16 v21, -0x1

    .line 3028
    .local v21, "result":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    float-to-long v0, v2

    move-wide/from16 v19, v0

    .line 3029
    .local v19, "duration":J
    const-wide/16 v2, 0x1

    cmp-long v2, v19, v2

    if-gez v2, :cond_0

    .line 3030
    const-wide/16 v19, 0x1

    .line 3033
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->currentMode:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 3034
    const/16 v7, 0x4e22

    .line 3038
    .local v7, "exerciseType":I
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget-wide v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_exID:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget-wide v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->start_time:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget-wide v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->start_time:J

    const-wide/16 v12, 0x3e8

    mul-long v12, v12, v19

    add-long/2addr v10, v12

    const-wide/16 v12, 0x3e8

    mul-long v12, v12, v19

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v14, v14, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_sum:F

    float-to-int v14, v14

    int-to-float v14, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v15, v15, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_cadence:F

    move/from16 v16, v0

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    const-string v17, ""

    const/16 v18, 0x37

    invoke-static/range {v2 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->updateRealtimeItem(Landroid/content/Context;JJIJJJFFILjava/lang/String;I)J

    move-result-wide v21

    .line 3041
    sget-wide v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsStartTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 3042
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    sget-wide v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsStartTime:J

    const-wide/16 v2, 0x3e8

    mul-long v13, v19, v2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    invoke-static/range {v8 .. v15}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->updateExerciseActivityItem(Landroid/content/Context;JJJLcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)J

    .line 3043
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doSaveExercise  result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v21

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 3044
    return-void

    .line 3036
    .end local v7    # "exerciseType":I
    :cond_2
    const/16 v7, 0x4e21

    .restart local v7    # "exerciseType":I
    goto/16 :goto_0
.end method

.method public getBSACUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 999
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 1000
    const-string v0, ""

    .line 1001
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurWorkoutResult()Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    return-object v0
.end method

.method public getCurrentMode()I
    .locals 1

    .prologue
    .line 1295
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->currentMode:I

    return v0
.end method

.method public getFEStatus()I
    .locals 1

    .prologue
    .line 1261
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->FEStatus:I

    return v0
.end method

.method public getFEUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1255
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 1256
    const/4 v0, 0x0

    .line 1257
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getGpsDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1872
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getGpsSensorDevice(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    .locals 9
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .prologue
    const/high16 v8, 0x437f0000    # 255.0f

    const/4 v7, 0x0

    .line 1789
    new-instance v4, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 1790
    .local v4, "shProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1791
    .local v2, "param":Landroid/os/Bundle;
    const/4 v3, 0x0

    .line 1793
    .local v3, "param_type":I
    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    .line 1794
    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v1

    .line 1795
    .local v1, "height":F
    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v5

    .line 1797
    .local v5, "weight":F
    cmpg-float v6, v1, v7

    if-gtz v6, :cond_0

    .line 1798
    const/high16 v1, 0x432a0000    # 170.0f

    .line 1800
    :cond_0
    cmpg-float v6, v5, v7

    if-gtz v6, :cond_1

    .line 1801
    const/high16 v5, 0x42700000    # 60.0f

    .line 1802
    :cond_1
    cmpl-float v6, v1, v8

    if-lez v6, :cond_2

    .line 1803
    const/high16 v1, 0x437f0000    # 255.0f

    .line 1805
    :cond_2
    cmpl-float v6, v5, v8

    if-lez v6, :cond_3

    .line 1806
    const/high16 v5, 0x437f0000    # 255.0f

    .line 1808
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    packed-switch v6, :pswitch_data_0

    .line 1823
    const/4 v3, 0x1

    .line 1827
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getPserviceType()I

    move-result v6

    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->pserviceType:I

    .line 1829
    const-string v6, "PARAM_TYPE"

    invoke-virtual {v2, v6, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1830
    const-string v6, "PARAM_CHECK_WEARABLE_DEVICE_CONNECTED"

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1831
    const-string v6, "PARAM_HEIGHT"

    invoke-virtual {v2, v6, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1832
    const-string v6, "PARAM_WEIGHT"

    invoke-virtual {v2, v6, v5}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1833
    const-string v6, "PARAM_PSERVICE"

    sget v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->pserviceType:I

    invoke-virtual {v2, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1834
    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "mGPSDevice - request type:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", height:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", weight:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", region:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->pserviceType:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1838
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v6, :cond_4

    .line 1840
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    new-instance v7, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    const-string v8, "CMD_REQUEST_EXTRA"

    invoke-direct {v7, v8, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->request(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_6

    .line 1857
    :cond_4
    :goto_1
    return-void

    .line 1811
    :pswitch_0
    const/4 v3, 0x2

    .line 1812
    goto :goto_0

    .line 1814
    :pswitch_1
    const/4 v3, 0x1

    .line 1815
    goto :goto_0

    .line 1817
    :pswitch_2
    const/4 v3, 0x3

    .line 1818
    goto :goto_0

    .line 1820
    :pswitch_3
    const/4 v3, 0x4

    .line 1821
    goto/16 :goto_0

    .line 1841
    :catch_0
    move-exception v0

    .line 1842
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v7, "getGpsSensorDevice->RemoteException"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1843
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1844
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v7, "getGpsSensorDevice->IllegalStateException"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1845
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 1846
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v7, "getGpsSensorDevice->IllegalArgumentException"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1847
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 1848
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v7, "getGpsSensorDevice->ShealthSensorServiceNotBoundException"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1849
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 1850
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v7, "getGpsSensorDevice->ShealthSensorNotSupportedException"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1851
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_5
    move-exception v0

    .line 1852
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v7, "getGpsSensorDevice->ShealthSensorDeviceInUseException"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1853
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_6
    move-exception v0

    .line 1854
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v7, "getGpsSensorDevice->SHealthSensorInternalErrorException"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1808
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getGpsStatus()I
    .locals 1

    .prologue
    .line 2283
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->GpsStatus:I

    return v0
.end method

.method public getHRMSensorName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHRMStatus()I
    .locals 1

    .prologue
    .line 969
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->HRMStatus:I

    return v0
.end method

.method public getHRMUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 959
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 960
    const-string v0, ""

    .line 961
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNotificationText()Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v10, 0x4

    const/4 v11, 0x2

    .line 3051
    const-string v7, ""

    .line 3052
    .local v7, "valueStr":Ljava/lang/String;
    const-string v4, ""

    .line 3053
    .local v4, "unitStr":Ljava/lang/String;
    const-string v2, ""

    .line 3054
    .local v2, "notiText":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->isGoalAchieved()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 3055
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090b95

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3092
    :goto_0
    return-object v2

    .line 3057
    :cond_0
    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->type:I

    if-ne v8, v11, :cond_2

    .line 3058
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->loadUnitsFromSharedPreferences()V

    .line 3059
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-static {v11, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainValueToGoal(IF)F

    move-result v5

    .line 3061
    .local v5, "val":F
    sget-object v8, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v9, "km"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 3062
    float-to-long v8, v5

    long-to-double v8, v8

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getMilesFromMetersWhileWorkingOut(D)Ljava/lang/String;

    move-result-object v7

    .line 3066
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-static {v11, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 3067
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090a6f

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3068
    goto :goto_0

    .line 3064
    :cond_1
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-static {v11, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 3068
    .end local v5    # "val":F
    :cond_2
    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->type:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_3

    .line 3069
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    float-to-long v8, v8

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainSecToGoal(J)J

    move-result-wide v5

    .line 3070
    .local v5, "val":J
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v7

    .line 3071
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090a6e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3072
    goto/16 :goto_0

    .end local v5    # "val":J
    :cond_3
    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->type:I

    if-ne v8, v10, :cond_4

    .line 3073
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_sum:F

    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRoundedValue(F)F

    move-result v8

    invoke-static {v10, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainValueToGoal(IF)F

    move-result v5

    .line 3074
    .local v5, "val":F
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-static {v10, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v7

    .line 3075
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-static {v10, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 3076
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090a70

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3077
    goto/16 :goto_0

    .end local v5    # "val":F
    :cond_4
    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->type:I

    const/16 v9, 0x17

    if-eq v8, v9, :cond_5

    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->type:I

    const/4 v9, 0x5

    if-ne v8, v9, :cond_6

    .line 3078
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090a2d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x18

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getLastETEAchievedPercent()I

    move-result v10

    int-to-float v10, v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " %"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 3081
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 3082
    .restart local v5    # "val":F
    const/high16 v8, 0x41200000    # 10.0f

    div-float v8, v5, v8

    float-to-int v8, v8

    mul-int/lit8 v8, v8, 0xa

    int-to-float v1, v8

    .line 3083
    .local v1, "ivalue":F
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-static {v11, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-static {v11, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3088
    .local v3, "secondValue":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    float-to-long v8, v8

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v0

    .line 3089
    .local v0, "firstValue":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public getRealtimeExerciseId()J
    .locals 2

    .prologue
    .line 3047
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    return-wide v0
.end method

.method public getShealthDeviceFinder()Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 1

    .prologue
    .line 1405
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object v0
.end method

.method public getShealthDeviceFinderC()Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    .locals 1

    .prologue
    .line 1409
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensorC:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    return-object v0
.end method

.method public getWorkoutState()I
    .locals 1

    .prologue
    .line 536
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    return v0
.end method

.method public initCurWorkoutResult(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 254
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    .line 255
    return-void
.end method

.method public isReadyGpsOn()Z
    .locals 1

    .prologue
    .line 2280
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isReadyGpsOn:Z

    return v0
.end method

.method public notiLastValue()V
    .locals 8

    .prologue
    const/high16 v7, 0x41200000    # 10.0f

    const/4 v4, 0x2

    .line 1023
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->currentMode:I

    if-ne v3, v4, :cond_1

    .line 1025
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1026
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .line 1027
    .local v2, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    float-to-long v5, v3

    invoke-interface {v2, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateLapClock(J)V

    .line 1028
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->paceGuide:I

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateTEPaceGuide(I)V

    .line 1029
    const/16 v3, 0x17

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getLastETEtrainingEffect()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v7

    const/4 v6, 0x0

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1030
    const/16 v3, 0x18

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getLastETEAchievedPercent()I

    move-result v5

    int-to-float v5, v5

    const/4 v6, 0x0

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1031
    const/4 v3, 0x4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_sum:F

    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRoundedValue(F)F

    move-result v5

    const/4 v6, 0x0

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1032
    const/4 v3, 0x2

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRoundedValue(F)F

    move-result v5

    const/4 v6, 0x0

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1033
    const/4 v3, 0x3

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    const/4 v6, 0x0

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1034
    const/4 v3, 0x5

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_heartrate:F

    const/4 v6, 0x0

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1035
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onSensorInfoReceived(I)V

    goto :goto_0

    .line 1037
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1039
    :catch_0
    move-exception v0

    .line 1040
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "notiLastValue -> INDOORS_MODE : Exception"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 1037
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1045
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    monitor-enter v4
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 1046
    :try_start_5
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .line 1047
    .restart local v2    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    if-eqz v3, :cond_2

    .line 1048
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->getElapsedTime()J

    move-result-wide v5

    invoke-interface {v2, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateLapClock(J)V

    .line 1050
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->paceGuide:I

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateTEPaceGuide(I)V

    .line 1051
    const/16 v3, 0x17

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getLastETEtrainingEffect()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v7

    const/4 v6, 0x0

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1052
    const/16 v3, 0x18

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getLastETEAchievedPercent()I

    move-result v5

    int-to-float v5, v5

    const/4 v6, 0x0

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1053
    const/4 v3, 0x5

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_heartrate:F

    const/4 v6, 0x0

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1054
    const/4 v3, 0x6

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->elevation:F

    const/16 v6, 0x500

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1055
    const/4 v3, 0x2

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRoundedValue(F)F

    move-result v5

    const/16 v6, 0x500

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1056
    const/4 v3, 0x4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_sum:F

    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRoundedValue(F)F

    move-result v5

    const/16 v6, 0x1197

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1057
    const/4 v3, 0x3

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    const/16 v6, 0x500

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    .line 1058
    const/4 v3, 0x6

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->elevation:F

    const/16 v6, 0x500

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    goto :goto_2

    .line 1060
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v3
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 1062
    :catch_1
    move-exception v0

    .line 1063
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "notiLastValue -> OUTDOORS_MODE : Exception"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1060
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 441
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 442
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->oldLocale:Ljava/util/Locale;

    if-eq v0, v1, :cond_0

    .line 443
    const/16 v0, 0xc8

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->closeNotification(I)V

    .line 444
    const/16 v0, 0x81

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->showNotification(ILcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)Landroid/app/Notification;

    .line 446
    :cond_0
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->oldLocale:Ljava/util/Locale;

    .line 447
    return-void
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 313
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 314
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRun:Z

    if-eqz v1, :cond_0

    .line 316
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v2, "RealtimeHealthService is already running!!"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :goto_0
    return-void

    .line 320
    :cond_0
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mServiceConnectionC:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensorC:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .line 322
    sput-boolean v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRun:Z

    .line 323
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 324
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setRecording(Z)V

    .line 325
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    .line 326
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeMode()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->currentMode:I

    .line 327
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->currentMode:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->initCurWorkoutResult(I)V

    .line 329
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_1

    .line 330
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 343
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 344
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 345
    const/16 v1, 0x3e7

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mSensorReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_2

    .line 349
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mSensorReceiver:Landroid/content/BroadcastReceiver;

    .line 406
    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    .end local v0    # "filter":Landroid/content/IntentFilter;
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 407
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ACTION_DEVICE_CONNECTED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 408
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mSensorReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 410
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->oldLocale:Ljava/util/Locale;

    .line 411
    const/16 v1, 0xc8

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->closeNotification(I)V

    .line 412
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopForeground(Z)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 417
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 418
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setRecording(Z)V

    .line 419
    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRun:Z

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 421
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    .line 422
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    .line 423
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v0, :cond_1

    .line 426
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy()-even though healthManager == null, stop is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 429
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 430
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensorC:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensorC:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->close()V

    .line 435
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensorC:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x1

    .line 469
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isSHealthUpgradeNeeded(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 471
    const-string v0, "RealtimeHealthService"

    const-string v1, "isUpgradeNeeded = true"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    :cond_0
    :goto_0
    return v3

    .line 475
    :cond_1
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isHealthServiceUpgradeNeeded(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 477
    const-string v0, "RealtimeHealthService"

    const-string v1, "isHealthServiceOld = true"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 481
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v0, :cond_0

    .line 483
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mServiceConnection:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    goto :goto_0
.end method

.method public pauseWorkout()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 655
    const-string/jumbo v2, "workout"

    const-string/jumbo v3, "pauseWorkout"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    const/16 v3, 0x7d1

    if-eq v2, v3, :cond_0

    .line 658
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v3, "Error. State not running"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    :goto_0
    return-void

    .line 661
    :cond_0
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRestart:Z

    .line 663
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->goalPause()V

    .line 664
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    if-eqz v2, :cond_1

    .line 665
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->pause()V

    .line 667
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 668
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090a84

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 669
    const/16 v2, 0x7d2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    .line 671
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->SamsungLocationMonitor:Z

    if-eqz v2, :cond_3

    .line 672
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;-><init>()V

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->LocationMonitorPause(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V

    .line 673
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 674
    invoke-static {v5, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 675
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    monitor-enter v3

    .line 676
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .line 677
    .local v1, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/4 v2, 0x3

    const/4 v4, 0x0

    const/16 v5, 0x500

    invoke-interface {v1, v2, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    goto :goto_1

    .line 679
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 682
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateHRMDataState()V

    .line 683
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateBSACDataState()V

    goto :goto_0
.end method

.method public registerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .prologue
    .line 489
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 490
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    monitor-enter v1

    .line 491
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 494
    :cond_0
    monitor-exit v1

    .line 496
    :cond_1
    return-void

    .line 494
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resetWorkout()V
    .locals 1

    .prologue
    .line 709
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    .line 710
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopWorkout()V

    .line 711
    return-void
.end method

.method public restartWorkout()V
    .locals 2

    .prologue
    .line 687
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "restartWorkout"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    const/16 v1, 0x7d2

    if-eq v0, v1, :cond_0

    .line 689
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v1, "Error. State not pause"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    :goto_0
    return-void

    .line 693
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRestart:Z

    .line 694
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->goalResume()V

    .line 695
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    if-eqz v0, :cond_1

    .line 696
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->resume()V

    .line 698
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 699
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090a83

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 700
    const/16 v0, 0x7d1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    .line 701
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastLocationMillis:J

    .line 702
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;-><init>()V

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->LocationMonitorResume(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V

    .line 703
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateHRMDataState()V

    .line 704
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateBSACDataState()V

    .line 705
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isBSACDeviceCheck()V

    goto :goto_0
.end method

.method public setBSACStatus(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 1009
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->BSACStatus:I

    if-eq v0, p1, :cond_0

    .line 1011
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->BSACStatus:I

    .line 1012
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->BSACStatus:I

    if-nez v0, :cond_1

    .line 1013
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v1, 0x3f4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1020
    :cond_0
    :goto_0
    return-void

    .line 1014
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->BSACStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1015
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v1, 0x3f5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1016
    :cond_2
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->BSACStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1017
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v1, 0x3f6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public setCurrentMode(I)V
    .locals 0
    .param p1, "currentMode"    # I

    .prologue
    .line 1291
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->currentMode:I

    .line 1292
    return-void
.end method

.method public setFEStatus(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 1265
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->FEStatus:I

    if-eq v0, p1, :cond_0

    .line 1267
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->FEStatus:I

    .line 1268
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->FEStatus:I

    if-nez v0, :cond_1

    .line 1269
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v1, 0x3ee

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1276
    :cond_0
    :goto_0
    return-void

    .line 1270
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->FEStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1271
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v1, 0x3ef

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1272
    :cond_2
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->FEStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1273
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v1, 0x3f0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public setGpsTracking(Z)V
    .locals 2
    .param p1, "setValue"    # Z

    .prologue
    .line 938
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->currentMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 948
    :goto_0
    return-void

    .line 941
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 942
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v1, "----------> START GPS tracking!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 943
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->startGpsTracking()V

    goto :goto_0

    .line 945
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v1, "----------> STOP GPS tracking!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 946
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopGpsTracking()V

    goto :goto_0
.end method

.method public setHRMStatus(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 973
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setHRMStatus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->HRMStatus:I

    if-eq v0, p1, :cond_0

    .line 976
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->HRMStatus:I

    .line 977
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->HRMStatus:I

    if-nez v0, :cond_1

    .line 978
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 985
    :cond_0
    :goto_0
    return-void

    .line 979
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->HRMStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 980
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 981
    :cond_2
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->HRMStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 982
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v1, 0x3ea

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public setHRWorkoutResult(I)V
    .locals 7
    .param p1, "hr"    # I

    .prologue
    const/4 v5, 0x5

    .line 2454
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_heartrate:F

    int-to-float v4, p1

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_1

    .line 2455
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2456
    int-to-float v3, p1

    invoke-static {v5, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2457
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    monitor-enter v4

    .line 2458
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2460
    .local v2, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/4 v3, 0x5

    int-to-float v5, p1

    const/4 v6, 0x0

    :try_start_1
    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2462
    :catch_0
    move-exception v0

    .line 2463
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2466
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2471
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    int-to-float v4, p1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->setHeartrate(F)V

    .line 2472
    return-void

    .line 2468
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2469
    int-to-float v3, p1

    invoke-static {v5, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    goto :goto_1
.end method

.method public setWorkoutState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 540
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    .line 541
    return-void
.end method

.method public startFE()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1069
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startFE"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v1, :cond_0

    .line 1071
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v1, :cond_1

    .line 1073
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    .line 1089
    :goto_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setFEStatus(I)V

    .line 1091
    :cond_0
    return-void

    .line 1074
    :catch_0
    move-exception v0

    .line 1075
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startFE : RemoteException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1076
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1077
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startFE : IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1078
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 1079
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startFE : ShealthSensorServiceNotBoundException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1080
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 1081
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startFE : IllegalStateException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1082
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_4
    move-exception v0

    .line 1083
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startFE : SHealthSensorInternalErrorException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1087
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :cond_1
    sput-boolean v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->fFEConnecting:Z

    goto :goto_0
.end method

.method public startGpsTracking()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 893
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startGpsTracking :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getGpsStatus()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 894
    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 895
    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 896
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isReadyGpsOn:Z

    .line 897
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getGpsStatus()I

    move-result v0

    if-nez v0, :cond_0

    .line 898
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGpsStatus(I)V

    .line 899
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v0, :cond_1

    .line 901
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsjoin()V

    .line 905
    :goto_0
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v0, :cond_0

    .line 906
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->startAMapGpsTracking()V

    .line 908
    :cond_0
    return-void

    .line 903
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "mGPSDevice is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startHRM(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 882
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startHRM"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->startSensor(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Z)V

    .line 884
    return-void
.end method

.method public startScanHRM(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;)V
    .locals 6
    .param p1, "HRMlistener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 789
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startScanHRM"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v0, :cond_0

    .line 791
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    .line 792
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v1, 0x0

    const/16 v2, 0x2718

    const/4 v3, 0x4

    const/16 v4, 0x64

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMScanListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->startScan(IIIILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;)V

    .line 794
    :cond_0
    return-void
.end method

.method public startWorkout(I)V
    .locals 12
    .param p1, "activityType"    # I

    .prologue
    const/16 v11, 0xc8

    const-wide/16 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 545
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    const/16 v3, 0x7d0

    if-eq v2, v3, :cond_0

    .line 652
    :goto_0
    return-void

    .line 548
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getExerciseId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    .line 549
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 550
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string v3, "Error. Exercise ID is NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 554
    :cond_1
    sput-boolean v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRestart:Z

    .line 557
    sput v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->toggleInsertMapDb:I

    .line 558
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedAve:F

    .line 559
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedTotal:F

    .line 560
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedCnt:F

    .line 561
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastTimeSpeed:J

    .line 562
    iput v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->maxSpeed:F

    .line 563
    sput-wide v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsStartTime:J

    .line 564
    iput-wide v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreDurationTime:J

    .line 565
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 566
    sput-boolean v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isReadyGpsOn:Z

    .line 568
    sput-boolean v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isReadyGpsOn:Z

    .line 569
    sput-boolean v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isGPSpopup:Z

    .line 570
    iput-wide v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsdisconnectCount:J

    .line 572
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->notifyExerciseStartToWorkForLife()V

    .line 573
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    if-nez v2, :cond_2

    .line 574
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    .line 576
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    if-eqz v2, :cond_3

    .line 577
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->listenerLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->registerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;)V

    .line 578
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->start()V

    .line 581
    :cond_3
    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setRecording(Z)V

    .line 582
    const/16 v2, 0x7d1

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    .line 583
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastLocationMillis:J

    .line 585
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    invoke-direct {v2, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    .line 586
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->getStartTimeMillis()J

    move-result-wide v3

    iput-wide v3, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->start_time:J

    .line 587
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput v6, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_sum:F

    .line 588
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput v6, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    .line 589
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput v6, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->elevation:F

    .line 590
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput v6, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    .line 592
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk:F

    .line 593
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->lastTotalDistance_sdk:F

    .line 594
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->totalDistance_sdk:F

    .line 595
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->averageSpeed_sdk:F

    .line 596
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->consumedCalorie_sdk:F

    .line 597
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->accuracy_sdk:F

    .line 598
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->bearing_sdk:F

    .line 599
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->speed_sdk:F

    .line 602
    sput v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->lastupdatedistance:I

    .line 603
    sput v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->lastupdatedbdistance:I

    .line 604
    sput v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->db_index:I

    .line 606
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput p1, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    .line 607
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    int-to-long v3, p1

    iput-wide v3, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_exID:J

    .line 610
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealtimeExerciseId(J)V

    .line 613
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 614
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->listenerGoal:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->registerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;)V

    .line 616
    const-string v0, ""

    .line 617
    .local v0, "audioGuideStr":Ljava/lang/String;
    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGoal(Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 618
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090a2e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 636
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 638
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 640
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->closeNotification(I)V

    .line 641
    const/16 v2, 0x81

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->showNotification(ILcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)Landroid/app/Notification;

    move-result-object v1

    .line 642
    .local v1, "notification":Landroid/app/Notification;
    if-eqz v1, :cond_4

    .line 643
    invoke-virtual {p0, v11, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->startForeground(ILandroid/app/Notification;)V

    .line 645
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateHRMDataState()V

    .line 646
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateBSACDataState()V

    .line 647
    sput-boolean v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsjoinRetry:Z

    .line 648
    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGpsStatus(I)V

    .line 649
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGpsTracking(Z)V

    .line 650
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isBSACDeviceCheck()V

    .line 651
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getType()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->type:I

    goto/16 :goto_0

    .line 621
    .end local v1    # "notification":Landroid/app/Notification;
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeActivityType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 626
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090a30

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 627
    goto :goto_1

    .line 623
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090a2f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 624
    goto :goto_1

    .line 629
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090a32

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 630
    goto :goto_1

    .line 632
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090a31

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 621
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public stopFE()V
    .locals 1

    .prologue
    .line 1094
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopFE(Z)V

    .line 1095
    return-void
.end method

.method public stopFE(Z)V
    .locals 4
    .param p1, "userCancel"    # Z

    .prologue
    const/4 v3, 0x0

    .line 1098
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopFE"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    sput-boolean v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->fFEConnecting:Z

    .line 1100
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v1, :cond_0

    .line 1101
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v1, :cond_0

    .line 1103
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1109
    :goto_0
    if-eqz p1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording:Z

    if-eqz v1, :cond_0

    .line 1110
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v2, 0x3f3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1113
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setFEStatus(I)V

    .line 1114
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->FEType:I

    .line 1115
    return-void

    .line 1104
    :catch_0
    move-exception v0

    .line 1105
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopFE : RemoteException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1106
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1107
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopFE : ShealthSensorServiceNotBoundException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopGpsTracking()V
    .locals 4

    .prologue
    .line 912
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 913
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isReadyGpsOn:Z

    .line 914
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stopGpsTracking :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getGpsStatus()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getGpsStatus()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 918
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v1, :cond_1

    .line 921
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V

    .line 922
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->gpsleave()V

    .line 923
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopGpsTracking - gpsleave---"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 932
    :goto_0
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v1, :cond_0

    .line 933
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopAMapGpsTracking()V

    .line 935
    :cond_0
    return-void

    .line 925
    :cond_1
    :try_start_1
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "mGPSDevice is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 928
    :catch_0
    move-exception v0

    .line 930
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopGpsTracking -> Exception"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopHRM()V
    .locals 2

    .prologue
    .line 872
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopHRM"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopSensor(Z)V

    .line 874
    return-void
.end method

.method public stopScanHRM()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
        }
    .end annotation

    .prologue
    .line 797
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopScanHRM"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->stopScan()V

    .line 801
    :cond_0
    return-void
.end method

.method public stopWorkout()V
    .locals 6

    .prologue
    const/16 v5, 0x7d0

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    if-eqz v0, :cond_0

    .line 716
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->stop()V

    .line 717
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->unregisterListener()V

    .line 718
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLapClock:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    .line 720
    :cond_0
    sput-boolean v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRestart:Z

    .line 723
    sput v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->toggleInsertMapDb:I

    .line 724
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedAve:F

    .line 725
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedTotal:F

    .line 726
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->tmpSpeedCnt:F

    .line 727
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mLastTimeSpeed:J

    .line 730
    sput v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->lastupdatedistance:I

    .line 731
    sput v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->lastupdatedbdistance:I

    .line 732
    sput v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->db_index:I

    .line 734
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 735
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->unregisterListener()V

    .line 736
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->goalEnd(JD)V

    .line 737
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    if-eq v0, v5, :cond_1

    .line 738
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 739
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090a85

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 742
    :cond_1
    const/16 v0, 0x8

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeFrequencyOfAudioGuide()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 743
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->playTTS()V

    .line 745
    :cond_2
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setRecording(Z)V

    .line 746
    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    .line 747
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGpsTracking(Z)V

    .line 748
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    const/4 v1, -0x1

    iput v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->paceGuide:I

    .line 749
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->notifyExerciseStopToWorkForLife()V

    .line 750
    const/16 v0, 0xc8

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->closeNotification(I)V

    .line 751
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopForeground(Z)V

    .line 752
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateHRMDataState()V

    .line 753
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateBSACDataState()V

    .line 754
    return-void
.end method

.method public unregisterListener(Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .prologue
    .line 499
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    monitor-enter v1

    .line 500
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 503
    :cond_0
    monitor-exit v1

    .line 504
    return-void

    .line 503
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updateGPSState(Z)Z
    .locals 4
    .param p1, "isGPSOn"    # Z

    .prologue
    .line 1366
    const/4 v1, 0x0

    .line 1367
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v2, :cond_3

    .line 1368
    if-eqz p1, :cond_3

    .line 1369
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I

    const/16 v3, 0x7d1

    if-ne v2, v3, :cond_2

    .line 1370
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mGPSDevice updateGPSDataState"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1372
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    const/16 v3, 0x4654

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    const/16 v3, 0x4655

    if-ne v2, v3, :cond_1

    .line 1373
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isReadyGpsOn:Z

    if-eqz v2, :cond_1

    .line 1374
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1396
    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 1398
    :cond_2
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isReadyGpsOn:Z

    .line 1401
    :cond_3
    return v1

    .line 1377
    :catch_0
    move-exception v0

    .line 1379
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 1380
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1382
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1383
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 1385
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 1386
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 1388
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 1389
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 1391
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_0

    .line 1392
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v0

    .line 1394
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0
.end method

.method public updateNotificationText(Ljava/lang/String;)V
    .locals 0
    .param p1, "notiText"    # Ljava/lang/String;

    .prologue
    .line 3096
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->updateNotification(Ljava/lang/String;)V

    .line 3097
    return-void
.end method
