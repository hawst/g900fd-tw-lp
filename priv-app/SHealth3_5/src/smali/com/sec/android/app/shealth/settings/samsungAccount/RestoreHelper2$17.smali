.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$17;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0

    .prologue
    .line 1412
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$17;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 2
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 1416
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_0

    .line 1418
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$17;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 1419
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$17;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finishAffinity()V

    .line 1420
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 1427
    :cond_0
    :goto_0
    return-void

    .line 1424
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$17;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreAgainHomePopup(Landroid/content/Context;Z)V

    .line 1425
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$17;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->finish()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto :goto_0
.end method
