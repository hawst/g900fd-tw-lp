.class public Lcom/sec/android/app/shealth/settings/SettingsFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/SettingsFragment$CheckUpdateDismissController;
    }
.end annotation


# static fields
.field private static final CHECK_UPDATE:Ljava/lang/String; = "check for updates dialog"

.field private static FROM_SETTINGS:Ljava/lang/String;

.field private static REQUEST_CODE:I


# instance fields
.field private final mDismissControlMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

.field private updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

.field view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const/16 v0, 0x65

    sput v0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->REQUEST_CODE:I

    .line 81
    const-string v0, "fromSettings"

    sput-object v0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->FROM_SETTINGS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .line 94
    new-instance v0, Lcom/sec/android/app/shealth/settings/SettingsFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment$1;-><init>(Lcom/sec/android/app/shealth/settings/SettingsFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mDismissControlMap:Ljava/util/Map;

    .line 416
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/SettingsFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SettingsFragment;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/settings/SettingsFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SettingsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/SettingsFragment;)Lcom/sec/android/app/shealth/settings/SHealthUpdation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SettingsFragment;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    return-object v0
.end method

.method private checkForUpdates()V
    .locals 4

    .prologue
    .line 231
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    if-eqz v1, :cond_0

    .line 292
    :goto_0
    return-void

    .line 234
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .line 238
    new-instance v1, Lcom/sec/android/app/shealth/settings/SettingsFragment$3;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/settings/SettingsFragment$3;-><init>(Lcom/sec/android/app/shealth/settings/SettingsFragment;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0908b4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setTitle(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    new-instance v2, Lcom/sec/android/app/shealth/settings/SettingsFragment$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment$4;-><init>(Lcom/sec/android/app/shealth/settings/SettingsFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 257
    new-instance v0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;-><init>(Lcom/sec/android/app/shealth/settings/SettingsFragment;)V

    .line 289
    .local v0, "onAppUpdateListener":Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->registerAppUpdateListener(Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;)V

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->show()V

    goto :goto_0
.end method

.method private startNextActivity(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 221
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 222
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 223
    const-class v1, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    if-ne p1, v1, :cond_0

    .line 224
    sget-object v1, Lcom/sec/android/app/shealth/settings/SettingsFragment;->FROM_SETTINGS:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 225
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startActivity(Landroid/content/Intent;)V

    .line 226
    return-void
.end method


# virtual methods
.method public getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 433
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mDismissControlMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 196
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 197
    sget v0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->REQUEST_CODE:I

    if-ne p1, v0, :cond_0

    .line 199
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 201
    const-string v0, "UserProfileActivity"

    const-string/jumbo v1, "save failed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    if-eqz p3, :cond_0

    .line 206
    const-string v0, "UserProfileActivity"

    const-string v1, "Send ACTION_UPDATE_PROFILE"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v7, 0x7f0808e8

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 307
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 309
    .local v1, "i":I
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->getInstance()Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    move-result-object v0

    .line 311
    .local v0, "fm":Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;
    packed-switch v1, :pswitch_data_0

    .line 410
    :goto_0
    :pswitch_0
    return-void

    .line 313
    :pswitch_1
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 314
    .local v2, "intent":Landroid/content/Intent;
    const-string v6, "TYPE"

    invoke-virtual {v2, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 315
    sget v6, Lcom/sec/android/app/shealth/settings/SettingsFragment;->REQUEST_CODE:I

    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 321
    .end local v2    # "intent":Landroid/content/Intent;
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_0

    .line 322
    const-class v6, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startNextActivity(Ljava/lang/Class;)V

    goto :goto_0

    .line 324
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "com.sec.android.app.shealth"

    const-string v8, "S001"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    const-class v6, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startNextActivity(Ljava/lang/Class;)V

    goto :goto_0

    .line 331
    :pswitch_3
    const-class v6, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startNextActivity(Ljava/lang/Class;)V

    goto :goto_0

    .line 335
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "com.sec.android.app.shealth"

    const-string v8, "S002"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    const-class v6, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startNextActivity(Ljava/lang/Class;)V

    goto :goto_0

    .line 340
    :pswitch_5
    if-eqz v0, :cond_1

    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->checkFeature(I)Z

    move-result v6

    if-ne v6, v8, :cond_1

    .line 341
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 342
    .local v3, "intent_noti":Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 344
    .end local v3    # "intent_noti":Landroid/content/Intent;
    :cond_1
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    .line 345
    .local v4, "isChecked":Z
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    if-nez v4, :cond_3

    move v7, v8

    :goto_1
    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 346
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const/4 v7, 0x0

    if-nez v4, :cond_2

    move v9, v8

    :cond_2
    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_3
    move v7, v9

    .line 345
    goto :goto_1

    .line 356
    .end local v4    # "isChecked":Z
    :pswitch_6
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 357
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "com.sec.android.app.shealth"

    const-string v8, "S003"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DATA_TYPE_KEY:Ljava/lang/String;

    invoke-virtual {v2, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 359
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 367
    .end local v2    # "intent":Landroid/content/Intent;
    :pswitch_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "com.sec.android.app.shealth"

    const-string v8, "S004"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    const-class v6, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startNextActivity(Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 372
    :pswitch_8
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "com.sec.android.app.shealth"

    const-string v8, "S005"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-class v6, Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startNextActivity(Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 377
    :pswitch_9
    const-class v6, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startNextActivity(Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 381
    :pswitch_a
    new-instance v5, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 382
    .local v5, "termsOfUseIntent":Landroid/content/Intent;
    const/high16 v6, 0x24000000

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 383
    const-string v6, "launched_from_settings"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 384
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 388
    .end local v5    # "termsOfUseIntent":Landroid/content/Intent;
    :pswitch_b
    const-class v6, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startNextActivity(Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 392
    :pswitch_c
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "com.sec.android.app.shealth"

    const-string v8, "S006"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isAnyNetworkEnabled(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 395
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->checkForUpdates()V

    goto/16 :goto_0

    .line 399
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f09078f

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v9}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 404
    :pswitch_d
    const-class v6, Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startNextActivity(Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 311
    nop

    :pswitch_data_0
    .packed-switch 0x7f0808df
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_c
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_9
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v10, 0x8

    const v9, 0x7f0901fd

    const v8, 0x7f0808f5

    const v7, 0x7f0808f0

    const/4 v6, 0x0

    .line 105
    const v3, 0x7f0301fc

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    .line 108
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808df

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808e1

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808e3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808e4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808e6

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808f6

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808f2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808f3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808f4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 121
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 122
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808ee

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808ea

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808ec

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getUpgradeStatus(Landroid/content/Context;)I

    move-result v1

    .line 143
    .local v1, "migrationState":I
    invoke-static {}, Lcom/sec/android/app/shealth/home/HomeActivity;->exportDataExist()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 149
    const-string v3, "SETTINGSFRAGMENT"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[EXPORT] HomeActivity.exportDataExist() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/home/HomeActivity;->exportDataExist()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808ef

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 152
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808de

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0907b3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808e9

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0907b4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808ed

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0907b5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808f1

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->getInstance()Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    move-result-object v0

    .line 169
    .local v0, "fm":Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;
    if-eqz v0, :cond_0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->checkFeature(I)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 170
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808e8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-virtual {v3, v10}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 172
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808e8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getNotificationValue(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808e8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    new-instance v4, Lcom/sec/android/app/shealth/settings/SettingsFragment$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment$2;-><init>(Lcom/sec/android/app/shealth/settings/SettingsFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v2, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 185
    .local v2, "systemLocale":Ljava/util/Locale;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ko"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 186
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0905c4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 191
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    return-object v3

    .line 154
    .end local v0    # "fm":Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;
    .end local v2    # "systemLocale":Ljava/util/Locale;
    :cond_1
    const-string v3, "SETTINGSFRAGMENT"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[EXPORT] migrationState : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    const-string v3, "SETTINGSFRAGMENT"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[EXPORT] HomeActivity.exportDataExist() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/home/HomeActivity;->exportDataExist()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    const v4, 0x7f0808ef

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/view/View;->setVisibility(I)V

    .line 158
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 188
    .restart local v0    # "fm":Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;
    .restart local v2    # "systemLocale":Ljava/util/Locale;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->view:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090e43

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    .line 300
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .line 302
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroyView()V

    .line 303
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 216
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    .line 218
    return-void
.end method
