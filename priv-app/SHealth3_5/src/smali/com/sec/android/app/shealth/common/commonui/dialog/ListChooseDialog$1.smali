.class Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$1;
.super Ljava/lang/Object;
.source "ListChooseDialog.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/view/View;)V
    .locals 5
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 190
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    iget-object v4, v3, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    iget-object v3, v3, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    check-cast v3, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->getIndexChecked()I

    move-result v3

    invoke-virtual {v4, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 191
    .local v0, "curChecked":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 192
    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$id;->radio_button:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 193
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

    move-result-object v1

    .line 195
    .local v1, "onSaveListener":Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    iget-object v3, v3, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    invoke-virtual {v3, p1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v2

    .line 196
    .local v2, "position":I
    if-eqz v1, :cond_1

    .line 197
    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;->onSave(I)V

    .line 199
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->dismissAllowStateLoss()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V

    .line 201
    return-void
.end method
