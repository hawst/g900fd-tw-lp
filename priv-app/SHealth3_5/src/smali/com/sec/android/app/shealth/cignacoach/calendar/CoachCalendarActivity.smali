.class public Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;
.source "CoachCalendarActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/framework/ui/calendar/OnPeriodSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;
    }
.end annotation


# static fields
.field private static final COACH_CALENDAR_DIALOG:Ljava/lang/String; = "coach_calendar_dialog"

.field private static mOldSelectedTime:J


# instance fields
.field CancelButtoOnClick:Landroid/view/View$OnClickListener;

.field private btnNextPeriod:Landroid/widget/Button;

.field private btnPrevPeriod:Landroid/widget/Button;

.field private calendarCurrentMonth:Landroid/widget/TextView;

.field private calendarCurrentYear:Landroid/widget/TextView;

.field private mAddHour:I

.field private mAddMin:I

.field private mBackupEndTime:J

.field private mBackupInputButtonType:Ljava/lang/String;

.field private mBackupStartTime:J

.field private mCancelButton:Landroid/view/View;

.field private mDataType:Ljava/lang/String;

.field private mItemPeriodType:I

.field private mKeyboardLeftRight:Z

.field private mPagePeriodType:I

.field private mPager:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;

.field private mReturnTime:J

.field private mSelectedTime:J

.field private mSetButton:Landroid/view/View;

.field private mStartPeriodList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mTimeColumnName:Ljava/lang/String;

.field private mVisibleDate:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mOldSelectedTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;-><init>()V

    .line 57
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mVisibleDate:Ljava/util/Calendar;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mDataType:Ljava/lang/String;

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mKeyboardLeftRight:Z

    .line 65
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mAddHour:I

    .line 66
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mAddMin:I

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mReturnTime:J

    .line 74
    const-string/jumbo v0, "sample_time"

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mTimeColumnName:Ljava/lang/String;

    .line 244
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$4;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->CancelButtoOnClick:Landroid/view/View$OnClickListener;

    .line 658
    return-void
.end method

.method private IsSameMonth(J)Z
    .locals 9
    .param p1, "selectTime"    # J

    .prologue
    const/4 v8, 0x2

    .line 341
    const/4 v4, 0x0

    .line 343
    .local v4, "return_value":Z
    const-wide/16 v6, -0x1

    cmp-long v6, p1, v6

    if-eqz v6, :cond_0

    .line 346
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 347
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 348
    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 350
    .local v5, "selected_month":I
    const-wide/32 v6, 0x1ee62800

    add-long v2, p1, v6

    .line 351
    .local v2, "end_time":J
    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 352
    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 354
    .local v1, "end_month":I
    if-ne v5, v1, :cond_1

    .line 355
    const/4 v4, 0x1

    .line 359
    .end local v0    # "c":Ljava/util/Calendar;
    .end local v1    # "end_month":I
    .end local v2    # "end_time":J
    .end local v5    # "selected_month":I
    :cond_0
    :goto_0
    return v4

    .line 357
    .restart local v0    # "c":Ljava/util/Calendar;
    .restart local v1    # "end_month":I
    .restart local v2    # "end_time":J
    .restart local v5    # "selected_month":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;
    .param p1, "x1"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->pageSelected(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPager:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->checkPeriodsAvailable()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mDataType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupStartTime:J

    return-wide v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupEndTime:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupInputButtonType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mTimeColumnName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mKeyboardLeftRight:Z

    return v0
.end method

.method private addMonthsToPeriodStarts()V
    .locals 6

    .prologue
    .line 413
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 414
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 416
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 417
    .local v2, "previousMonths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v3, 0x0

    .line 418
    .local v3, "show_month":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->IsSameMonth(J)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 419
    const/4 v3, 0x0

    .line 423
    :goto_0
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, v3, :cond_1

    .line 424
    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 425
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 421
    .end local v1    # "j":I
    :cond_0
    const/4 v3, 0x1

    goto :goto_0

    .line 427
    .restart local v1    # "j":I
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->addToPeriodStarts(Ljava/util/List;)V

    .line 428
    return-void
.end method

.method private addToPeriodStarts(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 397
    .local p1, "temp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 398
    .local v3, "time":Ljava/lang/Long;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 400
    const/4 v0, 0x0

    .line 401
    .local v0, "i":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 402
    .local v2, "list_size":I
    :goto_1
    if-ge v0, v2, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    .line 404
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 406
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v4, v0, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 409
    .end local v0    # "i":I
    .end local v2    # "list_size":I
    .end local v3    # "time":Ljava/lang/Long;
    :cond_2
    return-void
.end method

.method private addTodayAndYesterdayMonthesToPeriodStarts()V
    .locals 6

    .prologue
    .line 385
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    .line 388
    .local v0, "todayInMillis":J
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPagePeriodType:I

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfCurrentPeriod(JI)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->addToPeriodStarts(Ljava/util/List;)V

    .line 393
    return-void
.end method

.method private checkPeriodsAvailable()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 809
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnPrevPeriod:Landroid/widget/Button;

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPager:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;->getCurrentItem()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 811
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnPrevPeriod:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnPrevPeriod:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->isEnabled()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    .line 812
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnNextPeriod:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPager:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;->getCurrentItem()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v3, v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 814
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnNextPeriod:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnNextPeriod:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    .line 816
    return-void

    :cond_0
    move v0, v2

    .line 809
    goto :goto_0

    :cond_1
    move v1, v2

    .line 812
    goto :goto_1
.end method

.method private disableDayPopup(J)V
    .locals 7
    .param p1, "periodStart"    # J

    .prologue
    .line 475
    const-wide/32 v1, 0x5265c00

    .line 477
    .local v1, "checkTime":J
    iget-wide v3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupStartTime:J

    sub-long v3, p1, v3

    div-long/2addr v3, v1

    const-wide/16 v5, 0x6

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupInputButtonType:Ljava/lang/String;

    const-string v4, "EndDatePicker"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 479
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 481
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v3, 0x7f0900d7

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 482
    const-string v3, "Coach starting date should be set another one."

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 483
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "coach_calendar_dialog"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 486
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_0
    return-void
.end method

.method private findIndex(J)I
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 432
    const/4 v0, 0x0

    .line 433
    .local v0, "index":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 434
    .local v1, "list_size":I
    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-gtz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPagePeriodType:I

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfCurrentPeriod(JI)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_1

    .line 438
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 441
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_2

    .line 442
    const/4 v0, 0x0

    .line 444
    .end local v0    # "index":I
    :cond_2
    return v0
.end method

.method public static getOldSelectedTime()J
    .locals 2

    .prologue
    .line 79
    sget-wide v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mOldSelectedTime:J

    return-wide v0
.end method

.method private pageSelected(I)V
    .locals 20
    .param p1, "index"    # I

    .prologue
    .line 501
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mItemPeriodType:I

    packed-switch v15, :pswitch_data_0

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 504
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-lez v15, :cond_0

    .line 505
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v5

    .line 506
    .local v5, "displayingMonth":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 507
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mVisibleDate:Ljava/util/Calendar;

    .line 509
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v15

    iget-object v15, v15, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v15}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    .line 510
    .local v8, "language":Ljava/lang/String;
    const-string v7, ""

    .line 511
    .local v7, "koreanYear":Ljava/lang/String;
    const-string v15, "ko"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    const-string/jumbo v15, "zh"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    const-string v15, "ja"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 512
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f090066

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 515
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    move-wide/from16 v17, v0

    cmp-long v15, v15, v17

    if-gtz v15, :cond_7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPagePeriodType:I

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v15}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfCurrentPeriod(JI)J

    move-result-wide v18

    cmp-long v15, v16, v18

    if-gtz v15, :cond_7

    .line 518
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0e0001

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    .line 520
    .local v10, "months":[Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 521
    .local v2, "c":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    move-wide v0, v15

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 523
    const-string v15, "ko"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_3

    const-string/jumbo v15, "zh"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_3

    const-string v15, "ja"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 525
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentMonth:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v16

    const v17, 0x7f0c0014

    invoke-virtual/range {v15 .. v17}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 526
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentMonth:Landroid/widget/TextView;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 528
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentYear:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v16

    const v17, 0x7f0c0013

    invoke-virtual/range {v15 .. v17}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 529
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentYear:Landroid/widget/TextView;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    aget-object v17, v10, v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 544
    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 545
    .local v3, "c2":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    move-wide v0, v15

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 546
    const/4 v15, 0x2

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Ljava/util/Calendar;->add(II)V

    .line 587
    :goto_2
    if-lez p1, :cond_4

    .line 588
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v13

    .line 589
    .local v13, "previousMonth":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    add-int/lit8 v16, p1, -0x1

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v13, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 594
    .end local v13    # "previousMonth":Ljava/util/Calendar;
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    move/from16 v0, p1

    if-eq v0, v15, :cond_0

    .line 595
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v11

    .line 596
    .local v11, "nextMonth":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    add-int/lit8 v16, p1, 0x1

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v11, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto/16 :goto_0

    .line 531
    .end local v3    # "c2":Ljava/util/Calendar;
    .end local v11    # "nextMonth":Ljava/util/Calendar;
    :cond_5
    const-string v15, "ar"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 533
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const/high16 v16, 0x7f0e0000

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    .line 535
    .local v9, "month_short":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentMonth:Landroid/widget/TextView;

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v16

    aget-object v16, v9, v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 536
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentYear:Landroid/widget/TextView;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 540
    .end local v9    # "month_short":[Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentMonth:Landroid/widget/TextView;

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v16

    aget-object v16, v10, v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 541
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentYear:Landroid/widget/TextView;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 552
    .end local v2    # "c":Ljava/util/Calendar;
    .end local v10    # "months":[Ljava/lang/String;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0e0001

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    .line 554
    .restart local v10    # "months":[Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 555
    .restart local v2    # "c":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 557
    const-string v15, "ko"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_8

    const-string/jumbo v15, "zh"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 559
    :cond_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentMonth:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v16

    const v17, 0x7f0c0014

    invoke-virtual/range {v15 .. v17}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 560
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentMonth:Landroid/widget/TextView;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 562
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentYear:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v16

    const v17, 0x7f0c0013

    invoke-virtual/range {v15 .. v17}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 563
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentYear:Landroid/widget/TextView;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    aget-object v17, v10, v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 582
    :goto_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 583
    .restart local v3    # "c2":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 584
    const/4 v15, 0x2

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Ljava/util/Calendar;->add(II)V

    goto/16 :goto_2

    .line 565
    .end local v3    # "c2":Ljava/util/Calendar;
    :cond_9
    const-string v15, "ar"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 567
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const/high16 v16, 0x7f0e0000

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    .line 569
    .restart local v9    # "month_short":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentMonth:Landroid/widget/TextView;

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v16

    aget-object v16, v9, v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 570
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentYear:Landroid/widget/TextView;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 574
    .end local v9    # "month_short":[Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentMonth:Landroid/widget/TextView;

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v16

    aget-object v16, v10, v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 575
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentYear:Landroid/widget/TextView;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 604
    .end local v2    # "c":Ljava/util/Calendar;
    .end local v5    # "displayingMonth":Ljava/util/Calendar;
    .end local v7    # "koreanYear":Ljava/lang/String;
    .end local v8    # "language":Ljava/lang/String;
    .end local v10    # "months":[Ljava/lang/String;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-lez v15, :cond_0

    .line 605
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v5

    .line 606
    .restart local v5    # "displayingMonth":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 607
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getInstance()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v15

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 608
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    move-wide/from16 v17, v0

    cmp-long v15, v15, v17

    if-gtz v15, :cond_b

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPagePeriodType:I

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v15}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfCurrentPeriod(JI)J

    move-result-wide v18

    cmp-long v15, v16, v18

    if-gtz v15, :cond_b

    .line 614
    :cond_b
    if-lez p1, :cond_c

    .line 615
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v13

    .line 616
    .restart local v13    # "previousMonth":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    add-int/lit8 v16, p1, -0x1

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v13, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 621
    .end local v13    # "previousMonth":Ljava/util/Calendar;
    :cond_c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    move/from16 v0, p1

    if-eq v0, v15, :cond_0

    .line 622
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v11

    .line 623
    .restart local v11    # "nextMonth":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    add-int/lit8 v16, p1, 0x1

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v11, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto/16 :goto_0

    .line 630
    .end local v5    # "displayingMonth":Ljava/util/Calendar;
    .end local v11    # "nextMonth":Ljava/util/Calendar;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-lez v15, :cond_0

    .line 631
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v6

    .line 632
    .local v6, "displayingYear":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v6, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 634
    if-lez p1, :cond_d

    .line 635
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v14

    .line 636
    .local v14, "previousYear":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    add-int/lit8 v16, p1, -0x1

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    invoke-virtual/range {v14 .. v16}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 641
    .end local v14    # "previousYear":Ljava/util/Calendar;
    :cond_d
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    move/from16 v0, p1

    if-eq v0, v15, :cond_0

    .line 642
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v12

    .line 643
    .local v12, "nextYear":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    add-int/lit8 v16, p1, 0x1

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v12, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto/16 :goto_0

    .line 650
    .end local v6    # "displayingYear":Ljava/util/Calendar;
    .end local v12    # "nextYear":Ljava/util/Calendar;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-lez v15, :cond_0

    .line 651
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v4

    .line 652
    .local v4, "displayingDecade":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v4, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto/16 :goto_0

    .line 501
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static setOldSelectedTime(J)V
    .locals 0
    .param p0, "oldSelectedTime"    # J

    .prologue
    .line 83
    sput-wide p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mOldSelectedTime:J

    .line 84
    return-void
.end method

.method private setRequestFocus(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 796
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 797
    .local v0, "amm":Landroid/media/AudioManager;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 798
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 800
    return-void
.end method

.method private setToStartDayTime(Ljava/util/Calendar;II)V
    .locals 1
    .param p1, "calendar"    # Ljava/util/Calendar;
    .param p2, "hour"    # I
    .param p3, "min"    # I

    .prologue
    .line 470
    const/16 v0, 0xb

    invoke-virtual {p1, v0, p2}, Ljava/util/Calendar;->set(II)V

    .line 471
    const/16 v0, 0xc

    invoke-virtual {p1, v0, p3}, Ljava/util/Calendar;->set(II)V

    .line 472
    return-void
.end method

.method private setTodayDate(J)V
    .locals 11
    .param p1, "selectTime"    # J

    .prologue
    const/16 v10, 0xa

    const/16 v9, 0xc

    const/4 v8, 0x2

    .line 287
    const-wide/16 v6, -0x1

    cmp-long v6, p1, v6

    if-eqz v6, :cond_2

    .line 288
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 289
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 293
    invoke-virtual {v0, v10}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 294
    .local v4, "hour":Ljava/lang/String;
    const-string v6, "0"

    invoke-virtual {v4, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 295
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 296
    .local v2, "currentTime":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 297
    .local v1, "calendar":Ljava/util/Calendar;
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 298
    invoke-virtual {v1, v10}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 299
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v6, v8, :cond_0

    .line 300
    const-string v6, "0"

    invoke-virtual {v6, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 301
    :cond_0
    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 302
    .local v5, "min":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v6, v8, :cond_1

    .line 303
    const-string v6, "0"

    invoke-virtual {v6, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 311
    :cond_1
    const/16 v6, 0xb

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mAddHour:I

    .line 312
    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mAddMin:I

    .line 338
    .end local v0    # "c":Ljava/util/Calendar;
    .end local v1    # "calendar":Ljava/util/Calendar;
    .end local v2    # "currentTime":J
    .end local v4    # "hour":Ljava/lang/String;
    .end local v5    # "min":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 318
    .restart local v0    # "c":Ljava/util/Calendar;
    .restart local v4    # "hour":Ljava/lang/String;
    :cond_3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v6, v8, :cond_4

    .line 319
    const-string v6, "0"

    invoke-virtual {v6, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 320
    :cond_4
    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 321
    .restart local v5    # "min":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v6, v8, :cond_2

    .line 322
    const-string v6, "0"

    invoke-virtual {v6, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method private updateTime(J)J
    .locals 4
    .param p1, "periodStart"    # J

    .prologue
    .line 456
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 457
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 459
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 460
    .local v1, "hour":Ljava/lang/String;
    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 461
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mAddHour:I

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mAddMin:I

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setToStartDayTime(Ljava/util/Calendar;II)V

    .line 462
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide p1

    .line 465
    .end local p1    # "periodStart":J
    :cond_0
    return-wide p1
.end method


# virtual methods
.method public buttonOnClick()V
    .locals 7

    .prologue
    .line 273
    const-wide/32 v1, 0x5265c00

    .line 274
    .local v1, "oneDay":J
    iget-wide v3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mReturnTime:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->finish()V

    .line 284
    :goto_0
    return-void

    .line 278
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 279
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "returnTime"

    iget-wide v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mReturnTime:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 280
    const-string/jumbo v3, "returnEndTime"

    iget-wide v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupEndTime:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 281
    const/4 v3, -0x1

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setResult(ILandroid/content/Intent;)V

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->finish()V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v13, 0x6

    const/4 v12, 0x5

    const/4 v9, 0x1

    .line 691
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v10

    const/16 v11, 0x15

    if-ne v10, v11, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v10

    if-nez v10, :cond_2

    .line 693
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v7

    .line 694
    .local v7, "view":Landroid/view/View;
    instance-of v10, v7, Landroid/widget/ImageButton;

    if-eqz v10, :cond_4

    .line 696
    check-cast v7, Landroid/widget/ImageButton;

    .end local v7    # "view":Landroid/view/View;
    invoke-virtual {v7}, Landroid/widget/ImageButton;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 697
    .local v6, "text":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mVisibleDate:Ljava/util/Calendar;

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v11

    if-eq v10, v11, :cond_4

    .line 699
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mVisibleDate:Ljava/util/Calendar;

    invoke-virtual {v10, v13}, Ljava/util/Calendar;->get(I)I

    move-result v10

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    add-int/2addr v10, v11

    add-int/lit8 v10, v10, -0x2

    mul-int/lit16 v10, v10, 0x3e8

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    .line 700
    .local v8, "viewGroup":Landroid/view/ViewGroup;
    if-eqz v8, :cond_4

    .line 702
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 703
    .local v0, "child_count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 705
    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    instance-of v10, v10, Landroid/widget/ImageButton;

    if-eqz v10, :cond_1

    .line 707
    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setRequestFocus(Landroid/view/View;)V

    .line 791
    .end local v0    # "child_count":I
    .end local v2    # "i":I
    .end local v6    # "text":Ljava/lang/String;
    .end local v8    # "viewGroup":Landroid/view/ViewGroup;
    :cond_0
    :goto_1
    return v9

    .line 703
    .restart local v0    # "child_count":I
    .restart local v2    # "i":I
    .restart local v6    # "text":Ljava/lang/String;
    .restart local v8    # "viewGroup":Landroid/view/ViewGroup;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 717
    .end local v0    # "child_count":I
    .end local v2    # "i":I
    .end local v6    # "text":Ljava/lang/String;
    .end local v8    # "viewGroup":Landroid/view/ViewGroup;
    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v10

    const/16 v11, 0x16

    if-ne v10, v11, :cond_4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v10

    if-nez v10, :cond_4

    .line 719
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v7

    .line 720
    .restart local v7    # "view":Landroid/view/View;
    instance-of v10, v7, Landroid/widget/ImageButton;

    if-eqz v10, :cond_4

    .line 722
    check-cast v7, Landroid/widget/ImageButton;

    .end local v7    # "view":Landroid/view/View;
    invoke-virtual {v7}, Landroid/widget/ImageButton;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 723
    .restart local v6    # "text":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mVisibleDate:Ljava/util/Calendar;

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v11

    if-eq v10, v11, :cond_4

    .line 725
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mVisibleDate:Ljava/util/Calendar;

    invoke-virtual {v10, v13}, Ljava/util/Calendar;->get(I)I

    move-result v10

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    add-int/2addr v10, v11

    mul-int/lit16 v10, v10, 0x3e8

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    .line 726
    .restart local v8    # "viewGroup":Landroid/view/ViewGroup;
    if-eqz v8, :cond_4

    .line 728
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 729
    .restart local v0    # "child_count":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    if-ge v2, v0, :cond_0

    .line 731
    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    instance-of v10, v10, Landroid/widget/ImageButton;

    if-eqz v10, :cond_3

    .line 733
    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setRequestFocus(Landroid/view/View;)V

    goto :goto_1

    .line 729
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 743
    .end local v0    # "child_count":I
    .end local v2    # "i":I
    .end local v6    # "text":Ljava/lang/String;
    .end local v8    # "viewGroup":Landroid/view/ViewGroup;
    :cond_4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v9

    const/16 v10, 0x14

    if-ne v9, v10, :cond_6

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v9

    if-nez v9, :cond_6

    .line 746
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v7

    .line 747
    .restart local v7    # "view":Landroid/view/View;
    instance-of v9, v7, Landroid/widget/ImageButton;

    if-nez v9, :cond_5

    instance-of v9, v7, Landroid/widget/RelativeLayout;

    if-eqz v9, :cond_6

    .line 750
    :cond_5
    invoke-virtual {v7}, Landroid/view/View;->getNextFocusDownId()I

    move-result v3

    .line 751
    .local v3, "id":I
    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v9

    if-ne v9, v3, :cond_6

    .line 752
    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mCancelButton:Landroid/view/View;

    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setRequestFocus(Landroid/view/View;)V

    .line 754
    invoke-virtual {v7}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    .line 755
    .local v4, "row":Landroid/view/ViewParent;
    if-eqz v4, :cond_a

    instance-of v9, v4, Landroid/view/ViewGroup;

    if-eqz v9, :cond_a

    move-object v5, v4

    .line 756
    check-cast v5, Landroid/view/ViewGroup;

    .line 757
    .local v5, "rowGroup":Landroid/view/ViewGroup;
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 758
    .local v1, "firstView":Landroid/view/View;
    if-eqz v1, :cond_9

    .line 759
    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mCancelButton:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 768
    .end local v1    # "firstView":Landroid/view/View;
    .end local v3    # "id":I
    .end local v4    # "row":Landroid/view/ViewParent;
    .end local v5    # "rowGroup":Landroid/view/ViewGroup;
    .end local v7    # "view":Landroid/view/View;
    :cond_6
    :goto_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v9

    const/16 v10, 0x13

    if-ne v9, v10, :cond_8

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v9

    if-nez v9, :cond_8

    .line 771
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v7

    .line 772
    .restart local v7    # "view":Landroid/view/View;
    instance-of v9, v7, Landroid/widget/ImageButton;

    if-nez v9, :cond_7

    instance-of v9, v7, Landroid/widget/RelativeLayout;

    if-eqz v9, :cond_8

    .line 775
    :cond_7
    invoke-virtual {v7}, Landroid/view/View;->getNextFocusUpId()I

    move-result v3

    .line 776
    .restart local v3    # "id":I
    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v9

    if-ne v9, v3, :cond_8

    .line 777
    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnPrevPeriod:Landroid/widget/Button;

    invoke-virtual {v9}, Landroid/widget/Button;->isEnabled()Z

    move-result v9

    if-eqz v9, :cond_b

    .line 779
    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnPrevPeriod:Landroid/widget/Button;

    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setRequestFocus(Landroid/view/View;)V

    .line 791
    .end local v3    # "id":I
    .end local v7    # "view":Landroid/view/View;
    :cond_8
    :goto_4
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v9

    goto/16 :goto_1

    .line 761
    .restart local v1    # "firstView":Landroid/view/View;
    .restart local v3    # "id":I
    .restart local v4    # "row":Landroid/view/ViewParent;
    .restart local v5    # "rowGroup":Landroid/view/ViewGroup;
    .restart local v7    # "view":Landroid/view/View;
    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mCancelButton:Landroid/view/View;

    invoke-virtual {v9, v3}, Landroid/view/View;->setNextFocusUpId(I)V

    goto :goto_3

    .line 763
    .end local v1    # "firstView":Landroid/view/View;
    .end local v5    # "rowGroup":Landroid/view/ViewGroup;
    :cond_a
    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mCancelButton:Landroid/view/View;

    invoke-virtual {v9, v3}, Landroid/view/View;->setNextFocusUpId(I)V

    goto :goto_3

    .line 784
    .end local v4    # "row":Landroid/view/ViewParent;
    :cond_b
    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnNextPeriod:Landroid/widget/Button;

    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setRequestFocus(Landroid/view/View;)V

    goto :goto_4
.end method

.method protected getDaysStatuses(JJ)Ljava/util/TreeMap;
    .locals 1
    .param p1, "startMonthTime"    # J
    .param p3, "endMonthTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 804
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 96
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->requestWindowFeature(I)Z

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getWindow()Landroid/view/Window;

    move-result-object v8

    new-instance v9, Landroid/graphics/drawable/ColorDrawable;

    const/4 v10, 0x0

    invoke-direct {v9, v10}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v8, v9}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 99
    const v8, 0x7f03002d

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setContentView(I)V

    .line 100
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setFinishOnTouchOutside(Z)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    sput-object v8, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->mContext:Landroid/content/Context;

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string/jumbo v9, "period_type"

    const/4 v10, -0x1

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mItemPeriodType:I

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "data_type"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mDataType:Ljava/lang/String;

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "current_time"

    const-wide/16 v10, -0x1

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "backupStartTime"

    const-wide/16 v10, -0x1

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupStartTime:J

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "backupEndTime"

    const-wide/16 v10, -0x1

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupEndTime:J

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "backupInputButtonType"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupInputButtonType:Ljava/lang/String;

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "column_name"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "column_name"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mTimeColumnName:Ljava/lang/String;

    .line 117
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 118
    .local v1, "d":Ljava/util/Calendar;
    iget-wide v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    invoke-virtual {v1, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 120
    iget v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mItemPeriodType:I

    const/4 v9, -0x1

    if-eq v8, v9, :cond_1

    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mDataType:Ljava/lang/String;

    if-eqz v8, :cond_1

    iget-wide v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-nez v8, :cond_3

    .line 121
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string/jumbo v9, "period_type"

    const/4 v10, -0x1

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mItemPeriodType:I

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "data_type"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mDataType:Ljava/lang/String;

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "current_time"

    const-wide/16 v10, -0x1

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    .line 127
    iget v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mItemPeriodType:I

    const/4 v9, -0x1

    if-eq v8, v9, :cond_2

    iget-wide v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-nez v8, :cond_3

    .line 128
    :cond_2
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Intent extras not found: BundleKeys.PERIOD_TYPE_KEY and/or BundleKeys.DATA_TYPE_KEY and/or BundleKeys.CURRENT_TIME"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 133
    :cond_3
    iget v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mItemPeriodType:I

    if-eqz v8, :cond_4

    iget v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mItemPeriodType:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    .line 135
    :cond_4
    const/4 v8, 0x2

    iput v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPagePeriodType:I

    .line 146
    :goto_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    .line 147
    const/4 v3, 0x0

    .line 149
    .local v3, "index":I
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->addTodayAndYesterdayMonthesToPeriodStarts()V

    .line 150
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->addMonthsToPeriodStarts()V

    .line 152
    iget-wide v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    invoke-direct {p0, v8, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->findIndex(J)I

    move-result v3

    .line 154
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v4, v8, -0x1

    .line 155
    .local v4, "lastPeriodStart":I
    const/4 v8, -0x1

    if-ne v3, v8, :cond_5

    .line 156
    move v3, v4

    .line 159
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v8

    iget v10, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPagePeriodType:I

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfCurrentPeriod(JI)J

    move-result-wide v6

    .line 163
    .local v6, "todayPeriodStart":J
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 165
    const/4 v2, 0x0

    .line 166
    .local v2, "i":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    .line 167
    .local v5, "list_size":I
    :goto_1
    if-ge v2, v5, :cond_9

    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v8, v8, v6

    if-gez v8, :cond_9

    .line 169
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 136
    .end local v2    # "i":I
    .end local v3    # "index":I
    .end local v4    # "lastPeriodStart":I
    .end local v5    # "list_size":I
    .end local v6    # "todayPeriodStart":J
    :cond_6
    iget v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mItemPeriodType:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_7

    .line 137
    const/4 v8, 0x3

    iput v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPagePeriodType:I

    goto :goto_0

    .line 138
    :cond_7
    iget v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mItemPeriodType:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_8

    .line 139
    const/4 v8, 0x4

    iput v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPagePeriodType:I

    goto :goto_0

    .line 141
    :cond_8
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Specified period type is not supported by "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 172
    .restart local v2    # "i":I
    .restart local v3    # "index":I
    .restart local v4    # "lastPeriodStart":I
    .restart local v5    # "list_size":I
    .restart local v6    # "todayPeriodStart":J
    :cond_9
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ne v2, v8, :cond_c

    .line 173
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    .end local v2    # "i":I
    .end local v5    # "list_size":I
    :cond_a
    :goto_2
    const v8, 0x7f0800a9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentMonth:Landroid/widget/TextView;

    .line 185
    const v8, 0x7f0800aa

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->calendarCurrentYear:Landroid/widget/TextView;

    .line 187
    iget-wide v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    invoke-direct {p0, v8, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setTodayDate(J)V

    .line 189
    const v8, 0x7f0800ac

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPager:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;

    .line 190
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    iget v10, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mItemPeriodType:I

    invoke-direct {v0, p0, v8, v9, v10}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;I)V

    .line 193
    .local v0, "adapter":Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPager:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;

    invoke-virtual {v8, v0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 194
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPager:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;->setDrawingCacheEnabled(Z)V

    .line 196
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPager:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;

    new-instance v9, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$1;

    invoke-direct {v9, p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 205
    const v8, 0x7f0800a8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnPrevPeriod:Landroid/widget/Button;

    .line 206
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnPrevPeriod:Landroid/widget/Button;

    new-instance v9, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$2;

    invoke-direct {v9, p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)V

    invoke-virtual {v8, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    const v8, 0x7f0800ab

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnNextPeriod:Landroid/widget/Button;

    .line 217
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnNextPeriod:Landroid/widget/Button;

    new-instance v9, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$3;

    invoke-direct {v9, p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)V

    invoke-virtual {v8, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->IsSameMonth(J)Z

    move-result v8

    if-nez v8, :cond_b

    .line 228
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnPrevPeriod:Landroid/widget/Button;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 229
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->btnNextPeriod:Landroid/widget/Button;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 232
    :cond_b
    const v8, 0x7f0800ad

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mCancelButton:Landroid/view/View;

    .line 233
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mCancelButton:Landroid/view/View;

    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->CancelButtoOnClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPager:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;

    const/4 v9, 0x0

    invoke-virtual {v8, v3, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;->setCurrentItem(IZ)V

    .line 239
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->pageSelected(I)V

    .line 240
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mPager:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarRefactoring;->setFocusable(Z)V

    .line 242
    return-void

    .line 175
    .end local v0    # "adapter":Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;
    .restart local v2    # "i":I
    .restart local v5    # "list_size":I
    :cond_c
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mStartPeriodList:Ljava/util/List;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v2, v9}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 88
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mOldSelectedTime:J

    .line 89
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;->onDestroy()V

    .line 90
    return-void
.end method

.method public onPeriodSelected(JIZ)V
    .locals 2
    .param p1, "periodStart"    # J
    .param p3, "periodType"    # I
    .param p4, "containsMeasurements"    # Z

    .prologue
    .line 450
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->disableDayPopup(J)V

    .line 451
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setTodayDate(J)V

    .line 452
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->updateTime(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mReturnTime:J

    .line 453
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 490
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 491
    const-string/jumbo v0, "period_type"

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mItemPeriodType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 492
    const-string v0, "data_type"

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mDataType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    const-string v0, "current_time"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 494
    const-string v0, "backupStartTime"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupStartTime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 495
    const-string v0, "backupEndTime"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupEndTime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 496
    const-string v0, "backupInputButtonType"

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupInputButtonType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    return-void
.end method

.method protected refreshFocusables_ForcedWithDelay(J)V
    .locals 1
    .param p1, "delay"    # J

    .prologue
    .line 366
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$5;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;J)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$5;->start()V

    .line 381
    return-void
.end method
