.class public Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;
.super Ljava/lang/Object;
.source "ActivityMonitorData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivityTime:I

.field private mCreateTime:J

.field private mDistance:F

.field private mExerciseId:J

.field private mId:J

.field private mKcal:F

.field private mKindOfWalking:I

.field private mRunSteps:I

.field private mSpeed:F

.field private mTimeZone:I

.field private mTotalStep:I

.field private mUpDownSteps:I

.field private mUserDeviceId:Ljava/lang/String;

.field private mWalkSteps:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 223
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mId:J

    .line 62
    return-void
.end method

.method public constructor <init>(JJJIIIIFFFILjava/lang/String;II)V
    .locals 4
    .param p1, "id"    # J
    .param p3, "exerciseId"    # J
    .param p5, "createTime"    # J
    .param p7, "totalStep"    # I
    .param p8, "runSteps"    # I
    .param p9, "walkSteps"    # I
    .param p10, "upDownSteps"    # I
    .param p11, "kcal"    # F
    .param p12, "speed"    # F
    .param p13, "distance"    # F
    .param p14, "kindOfWalking"    # I
    .param p15, "userdeviceid"    # Ljava/lang/String;
    .param p16, "timeZone"    # I
    .param p17, "activityTime"    # I

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mId:J

    .line 40
    const-wide/16 v1, -0x1

    cmp-long v1, p1, v1

    if-gez v1, :cond_0

    .line 42
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id should not be "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 44
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mId:J

    .line 45
    iput-wide p3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mExerciseId:J

    .line 46
    iput-wide p5, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mCreateTime:J

    .line 47
    iput p7, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mTotalStep:I

    .line 48
    iput p8, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mRunSteps:I

    .line 49
    iput p9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mWalkSteps:I

    .line 50
    iput p10, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mUpDownSteps:I

    .line 51
    iput p11, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mKcal:F

    .line 52
    move/from16 v0, p12

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mSpeed:F

    .line 53
    move/from16 v0, p13

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mDistance:F

    .line 54
    move/from16 v0, p14

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mKindOfWalking:I

    .line 55
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mUserDeviceId:Ljava/lang/String;

    .line 56
    move/from16 v0, p16

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mTimeZone:I

    .line 57
    move/from16 v0, p17

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mActivityTime:I

    .line 58
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    return v0
.end method

.method public getActivityTime()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mActivityTime:I

    return v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mCreateTime:J

    return-wide v0
.end method

.method public getDistance()F
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mDistance:F

    return v0
.end method

.method public getExerciseId()J
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mExerciseId:J

    return-wide v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mId:J

    return-wide v0
.end method

.method public getKcal()F
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mKcal:F

    return v0
.end method

.method public getKindOfWalking()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mKindOfWalking:I

    return v0
.end method

.method public getRunSteps()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mRunSteps:I

    return v0
.end method

.method public getSpeed()F
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mSpeed:F

    return v0
.end method

.method public getTimeZone()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mTimeZone:I

    return v0
.end method

.method public getTotalStep()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mTotalStep:I

    return v0
.end method

.method public getUpDownSteps()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mUpDownSteps:I

    return v0
.end method

.method public getUserDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mUserDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getWalkSteps()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mWalkSteps:I

    return v0
.end method

.method public setActivityTime(I)V
    .locals 0
    .param p1, "activityTime"    # I

    .prologue
    .line 201
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mActivityTime:I

    .line 202
    return-void
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "createTime"    # J

    .prologue
    .line 91
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mCreateTime:J

    .line 92
    return-void
.end method

.method public setDistance(F)V
    .locals 0
    .param p1, "distance"    # F

    .prologue
    .line 161
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mDistance:F

    .line 162
    return-void
.end method

.method public setExerciseId(J)V
    .locals 0
    .param p1, "exerciseId"    # J

    .prologue
    .line 81
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mExerciseId:J

    .line 82
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 71
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mId:J

    .line 72
    return-void
.end method

.method public setKcal(F)V
    .locals 0
    .param p1, "kcal"    # F

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mKcal:F

    .line 142
    return-void
.end method

.method public setKindOfWalking(I)V
    .locals 0
    .param p1, "kindOfWalking"    # I

    .prologue
    .line 171
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mKindOfWalking:I

    .line 172
    return-void
.end method

.method public setRunSteps(I)V
    .locals 0
    .param p1, "runSteps"    # I

    .prologue
    .line 111
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mRunSteps:I

    .line 112
    return-void
.end method

.method public setSpeed(F)V
    .locals 0
    .param p1, "speed"    # F

    .prologue
    .line 151
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mSpeed:F

    .line 152
    return-void
.end method

.method public setTimeZone(I)V
    .locals 0
    .param p1, "timeZone"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mTimeZone:I

    .line 192
    return-void
.end method

.method public setTotalStep(I)V
    .locals 0
    .param p1, "totalStep"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mTotalStep:I

    .line 102
    return-void
.end method

.method public setUpDownSteps(I)V
    .locals 0
    .param p1, "upDownSteps"    # I

    .prologue
    .line 131
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mUpDownSteps:I

    .line 132
    return-void
.end method

.method public setUserDeviceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userdeviceid"    # Ljava/lang/String;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mUserDeviceId:Ljava/lang/String;

    .line 182
    return-void
.end method

.method public setWalkSteps(I)V
    .locals 0
    .param p1, "walkSteps"    # I

    .prologue
    .line 121
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mWalkSteps:I

    .line 122
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 207
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 208
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mExerciseId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 209
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mCreateTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 210
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mTotalStep:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 211
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mRunSteps:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 212
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mWalkSteps:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 213
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mUpDownSteps:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 214
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mKcal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 215
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mSpeed:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 216
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mDistance:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 217
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mKindOfWalking:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mUserDeviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 219
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mTimeZone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 220
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorData;->mActivityTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 221
    return-void
.end method
