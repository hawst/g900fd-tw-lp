.class Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$7;
.super Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->showLoadingPopup(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

.field final synthetic val$resourceId:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Landroid/content/Context;I)V
    .locals 8

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    const/4 v0, 0x0

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v3, 0x0

    int-to-long v0, p3

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, -0x6b636b15698a5ec0L

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, -0x6b636b15698a5ec0L

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$7;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, 0x5d4c

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, 0x5d7c

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    const-wide v2, -0x6b636b15698a5ec0L

    xor-long/2addr v0, v2

    :cond_5
    const/16 v2, 0x20

    shl-long/2addr v0, v2

    const/16 v2, 0x20

    shr-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$7;->val$resourceId:I

    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public doAfterDialogIsShown()V
    .locals 0

    return-void
.end method

.method protected setText(I)V
    .locals 11

    const-wide v9, 0x2bbf540ab654f4b1L    # 5.7292541912540444E-98

    const/4 v8, 0x0

    const/16 v7, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v8}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v3, v0, v7

    aget-wide v0, v2, v8

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-eqz v5, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v3

    xor-long/2addr v0, v9

    aput-wide v0, v2, v8

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$7;->val$resourceId:I

    invoke-super {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setText(I)V

    return-void
.end method
