.class public Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ExerciseProTrainingEffectGoalActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$SliderAdapterImpl;
    }
.end annotation


# static fields
.field private static final CHANGE_POSITION_KEY:Ljava/lang/String; = "change_position"

.field private static mPosition:I


# instance fields
.field private mActivityType:I

.field private mMoreInfoButton:Landroid/widget/Button;

.field private slider:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;

.field private sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 45
    const/16 v0, 0x4653

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mActivityType:I

    .line 207
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mActivityType:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 39
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    return v0
.end method

.method static synthetic access$102(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 39
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    return p0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;
    .param p1, "x1"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->setTrainingEffectTitleAndContent(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;Ljava/lang/Class;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;
    .param p1, "x1"    # Ljava/lang/Class;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->startNextActivity(Ljava/lang/Class;)V

    return-void
.end method

.method private setAllButtonsWereClicked(Z)V
    .locals 0
    .param p1, "clicked"    # Z

    .prologue
    .line 148
    return-void
.end method

.method private setTrainingEffectTitleAndContent(I)V
    .locals 13
    .param p1, "level"    # I

    .prologue
    .line 152
    const v8, 0x7f0807b5

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 153
    .local v6, "title":Landroid/widget/TextView;
    const v8, 0x7f0807b7

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 154
    .local v4, "subtitle":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget-object v5, v8, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 155
    .local v5, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 156
    .local v3, "strLanguage":Ljava/lang/String;
    if-eqz v3, :cond_0

    const-string v8, "fa"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 157
    const/4 v8, 0x5

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 159
    :cond_0
    const v8, 0x7f0807b6

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 160
    .local v7, "value":Landroid/widget/TextView;
    const v8, 0x7f0807b4

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 161
    .local v1, "heading":Landroid/widget/TextView;
    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    const v8, 0x7f0807b8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 163
    .local v0, "content":Landroid/widget/TextView;
    const v8, 0x7f0807bb

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;

    .line 164
    .local v2, "sliderView":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;
    if-nez p1, :cond_2

    .line 165
    const v8, 0x7f090ad9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    const v8, 0x7f090ad9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    const v8, 0x7f090ae9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-wide v11, 0x3ffe666666666666L    # 1.9

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    const v8, 0x7f090a61

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x1e

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    const v8, 0x7f090f63

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const v9, 0x7f090ada

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f090ad9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f090adb

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 193
    :cond_1
    :goto_0
    return-void

    .line 171
    :cond_2
    const/4 v8, 0x1

    if-ne p1, v8, :cond_3

    .line 172
    const v8, 0x7f090f6f

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    const v8, 0x7f090f6f

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    const v8, 0x7f090ae9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-wide/high16 v11, 0x4004000000000000L    # 2.5

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    const v8, 0x7f090a61

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x1e

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    const v8, 0x7f090f64

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const v9, 0x7f090ada

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f090f6f

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f090adb

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 178
    :cond_3
    const/4 v8, 0x2

    if-ne p1, v8, :cond_4

    .line 179
    const v8, 0x7f090ad8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    const v8, 0x7f090ad8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    const v8, 0x7f090ae9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-wide/high16 v11, 0x400c000000000000L    # 3.5

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    const v8, 0x7f090a61

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x2d

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    const v8, 0x7f090f65

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const v9, 0x7f090ada

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f090ad8

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f090adb

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 185
    :cond_4
    const/4 v8, 0x3

    if-ne p1, v8, :cond_1

    .line 186
    const v8, 0x7f09013e

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    const v8, 0x7f09013e

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    const v8, 0x7f090ae9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-wide/high16 v11, 0x4012000000000000L    # 4.5

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    const v8, 0x7f090a67

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    const v8, 0x7f090a65

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const v9, 0x7f090ada

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f090a5f

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f090adb

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private startNextActivity(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 118
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24010000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 121
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->startActivity(Landroid/content/Intent;)V

    .line 122
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090a2d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 143
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, -0x1

    .line 340
    const/16 v0, 0x7c

    if-ne p1, v0, :cond_0

    .line 341
    if-ne p2, v1, :cond_0

    .line 342
    invoke-virtual {p0, v1, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->setResult(ILandroid/content/Intent;)V

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->finish()V

    .line 346
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->finish()V

    .line 135
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f090042

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 49
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v2, 0x7f0301b7

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->setContentView(I)V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "pick_type"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "pick_type"

    const/16 v4, 0x4653

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mActivityType:I

    .line 54
    :cond_0
    if-eqz p1, :cond_2

    .line 55
    const-string v2, "change_position"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    .line 59
    :goto_0
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    if-gez v2, :cond_3

    .line 60
    sput v5, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    .line 63
    :cond_1
    :goto_1
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->setTrainingEffectTitleAndContent(I)V

    .line 64
    const v2, 0x7f0807c6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 65
    .local v0, "leftClickableView":Landroid/view/View;
    invoke-virtual {v0, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 67
    const v2, 0x7f08067f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 68
    invoke-virtual {v0, v6}, Landroid/view/View;->setClickable(Z)V

    .line 69
    const v2, 0x7f0807c7

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 70
    .local v1, "rightClickableView":Landroid/view/View;
    const v2, 0x7f080680

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 76
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$SliderAdapterImpl;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v5, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$SliderAdapterImpl;-><init>(Landroid/content/Context;ILcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$1;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    .line 92
    const v2, 0x7f0807bb

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->slider:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->slider:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;

    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->initialize(FLcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;)V

    .line 94
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->slider:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->setHandlerPositionListener(Lcom/sec/android/app/shealth/plugins/exercisepro/listener/HandlerPositionListener;)V

    .line 107
    const v2, 0x7f0807b9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mMoreInfoButton:Landroid/widget/Button;

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mMoreInfoButton:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    return-void

    .line 57
    .end local v0    # "leftClickableView":Landroid/view/View;
    .end local v1    # "rightClickableView":Landroid/view/View;
    :cond_2
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mActivityType:I

    const/4 v3, 0x5

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getActivityTypeGoalValue(Landroid/content/Context;II)I

    move-result v2

    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    goto/16 :goto_0

    .line 61
    :cond_3
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    if-le v2, v5, :cond_1

    .line 62
    sput v5, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 129
    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 203
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 204
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->setAllButtonsWereClicked(Z)V

    .line 205
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 356
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 357
    const-string v0, "change_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    .line 358
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 198
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->setAllButtonsWereClicked(Z)V

    .line 199
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 350
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 351
    const-string v0, "change_position"

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 352
    return-void
.end method
