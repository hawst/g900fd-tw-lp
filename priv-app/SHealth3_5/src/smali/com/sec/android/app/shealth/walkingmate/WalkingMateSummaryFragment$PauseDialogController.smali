.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$PauseDialogController;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PauseDialogController"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 4
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    const/4 v3, 0x0

    .line 1798
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$15;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1829
    :goto_0
    :pswitch_0
    return-void

    .line 1800
    :pswitch_1
    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setStartWalking(Z)V

    .line 1801
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1802
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStop(Z)V

    .line 1804
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->updateWidgets()V

    .line 1805
    invoke-static {}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->checkCoverView()V

    .line 1807
    const-string/jumbo v0, "pause inactivetime monitor"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 1808
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetInactiveMonitor()V

    .line 1809
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetHealthyStep()V

    .line 1810
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetActiveTime()V

    .line 1811
    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotificationONOFF(Z)V

    .line 1813
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->onUpdateScreen()V

    .line 1814
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1815
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1818
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.walkingmate"

    const-string v2, "W009"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertStatusLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1821
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090b91

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4602(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 1822
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4600(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1798
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
