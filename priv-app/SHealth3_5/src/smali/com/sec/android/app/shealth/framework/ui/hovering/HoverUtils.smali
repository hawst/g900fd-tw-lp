.class public Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$6;,
        Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;
    }
.end annotation


# static fields
.field private static final HOVER_POPUP_BACKGROUND:Ljava/lang/String; = "tw_toast_frame_holo_light"

.field private static TAG:Ljava/lang/String; = null

.field static final URI_AIR_VIEW_MASTER_STATE:Ljava/lang/String; = "air_view_master_onoff"

.field private static final ZOOMIN_POPUP_BACKGROUND:Ljava/lang/String; = "fairview_internet_folder_hover_bg_dark"

.field private static sInformationPopUpResource:I

.field private static sIsHoveringEnabled:Z

.field public static sOnHoverListener:Landroid/view/View$OnHoverListener;

.field private static sToolTipResource:I

.field private static sZoomInPopUpResource:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->TAG:Ljava/lang/String;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->hover_tooltip_popup:I

    sput v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sToolTipResource:I

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->hover_information_popup:I

    sput v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sInformationPopUpResource:I

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->hover_zoom_in_popup:I

    sput v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sZoomInPopUpResource:I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sIsHoveringEnabled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getOnHoverHapticFeedbackListener()Landroid/view/View$OnHoverListener;
    .locals 1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sOnHoverListener:Landroid/view/View$OnHoverListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$5;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$5;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sOnHoverListener:Landroid/view/View$OnHoverListener;

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sOnHoverListener:Landroid/view/View$OnHoverListener;

    return-object v0
.end method

.method public static isFingerHoveringSoundAndHapticFeedbackOn(Landroid/view/View;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "finger_air_view_information_preview"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "finger_air_view_sound_and_haptic_feedback"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isPenHoveringSoundAndHapticFeedbackOn(Landroid/view/View;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "pen_hovering_information_preview"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "pen_hovering_sound"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    :try_start_0
    sget-boolean v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sIsHoveringEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$6;->$SwitchMap$com$sec$android$app$shealth$framework$ui$hovering$HoverUtils$HoverWindowType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setHoverPopupType(I)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setHoverPopupListener2 > NoSuchMethodError Exception :android.widget.HoverPopupWindow getHoverPopupWindow()"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    :try_start_1
    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sToolTipResource:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->tw_toast_frame_holo_light:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->first_text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Landroid/view/View;->setHoverPopupType(I)V

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$2;

    invoke-direct {v3, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$2;-><init>(Landroid/widget/TextView;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sInformationPopUpResource:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->airview_popup_picker_bg_light:I

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->first_text:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->second_text:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    if-eqz p3, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Landroid/view/View;->setHoverPopupType(I)V

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$3;

    invoke-direct {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$3;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    goto/16 :goto_0

    :cond_1
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sZoomInPopUpResource:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->food_tracker_zoom_in_popup:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->fairview_internet_folder_hover_bg_dark:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {p0, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Landroid/view/View;->setHoverPopupType(I)V

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$4;

    invoke-direct {v3, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$4;-><init>(Landroid/view/View;Landroid/widget/ImageView;)V

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    sget-boolean v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->sIsHoveringEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$6;->$SwitchMap$com$sec$android$app$shealth$framework$ui$hovering$HoverUtils$HoverWindowType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->grid_hover_layout:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->zoom_in_icon:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->gridInfoPreview:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    const/4 v3, 0x0

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_1
    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Landroid/view/View;->setHoverPopupType(I)V

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;

    invoke-direct {v4, v2, v0, v1, p2}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;-><init>(Landroid/view/View;Landroid/widget/ImageView;Landroid/widget/GridView;Ljava/util/List;)V

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setHoverPopupListener1 > NoSuchMethodError Exception :android.widget.HoverPopupWindow getHoverPopupWindow()"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
