.class public Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "AutoBackupActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ProfileFinishedReceiver;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    }
.end annotation


# static fields
.field private static final interval:[J


# instance fields
.field private TAG:Ljava/lang/String;

.field private isProfileRestored:Z

.field private mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

.field private mAppID:Ljava/lang/String;

.field private mClickState:Z

.field private mContext:Landroid/content/Context;

.field private mDataMigrationHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

.field private mIntervalListView:Landroid/widget/ListView;

.field private mIsBottomViewEnabled:Z

.field private mMigrationState:I

.field private mPeriodicSyncManager:Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

.field private mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ProfileFinishedReceiver;

.field private mSipIntentFilter:Landroid/content/IntentFilter;

.field private mSwitch:Landroid/widget/Switch;

.field private mSyncInterval:J

.field private mUIPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

.field private nextButtonLayout:Landroid/widget/RelativeLayout;

.field private restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

.field private vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x5

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->interval:[J

    return-void

    nop

    :array_0
    .array-data 8
        0x2a30
        0x5460
        0xa8c0
        0x15180
        0x3f480
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 54
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->TAG:Ljava/lang/String;

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mClickState:Z

    .line 63
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mPeriodicSyncManager:Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    .line 64
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIsBottomViewEnabled:Z

    .line 71
    const-string v0, "com.sec.android.app.shealth"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAppID:Ljava/lang/String;

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mContext:Landroid/content/Context;

    .line 551
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->setEnableListView(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSyncInterval:J

    return-wide v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;
    .param p1, "x1"    # J

    .prologue
    .line 49
    iput-wide p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSyncInterval:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAppID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mDataMigrationHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mClickState:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mClickState:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mPeriodicSyncManager:Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    return-object p1
.end method

.method static synthetic access$400()[J
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->interval:[J

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIntervalListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->refreshWithoutContent()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIsBottomViewEnabled:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->refreshContent()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mMigrationState:I

    return v0
.end method

.method private refreshContent()V
    .locals 1

    .prologue
    .line 380
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->clearScreensViewsList()V

    .line 381
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 383
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->addToScreenViewsList(Landroid/view/View;)V

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mContent:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mContent:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->addToScreenViewsList(Landroid/view/View;)V

    .line 389
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->refreshFocusables()V

    .line 390
    return-void
.end method

.method private refreshWithoutContent()V
    .locals 2

    .prologue
    const v1, 0x7f080054

    .line 366
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->clearScreensViewsList()V

    .line 367
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->addToScreenViewsList(Landroid/view/View;)V

    .line 371
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 373
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->addToScreenViewsList(Landroid/view/View;)V

    .line 375
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->refreshFocusables()V

    .line 377
    return-void
.end method

.method private registerProfileFinishedReceiver()V
    .locals 2

    .prologue
    .line 568
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ProfileFinishedReceiver;

    if-nez v0, :cond_0

    .line 570
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ProfileFinishedReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ProfileFinishedReceiver;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ProfileFinishedReceiver;

    .line 571
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSipIntentFilter:Landroid/content/IntentFilter;

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSipIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 573
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ProfileFinishedReceiver;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSipIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 576
    :cond_0
    return-void
.end method

.method private setEnableListView(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    const v1, 0x7f080050

    .line 240
    const v0, 0x7f08004f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 241
    const v0, 0x7f080051

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 242
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 243
    if-nez p1, :cond_1

    .line 244
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 249
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 252
    :cond_0
    return-void

    .line 246
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private unregisterProfileFinishedReceiver()V
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ProfileFinishedReceiver;

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ProfileFinishedReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 586
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ProfileFinishedReceiver;

    .line 588
    :cond_0
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 176
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v4, 0x7f090274

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v4, 0x7f030004

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 179
    .local v0, "mSwitchView":Landroid/view/View;
    const v1, 0x7f080025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addCustomView(Landroid/view/View;)V

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mUIPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    if-nez v1, :cond_0

    .line 185
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mUIPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 187
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mUIPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->isInitialSettingOngoing()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIsBottomViewEnabled:Z

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;

    if-eqz v1, :cond_2

    .line 190
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIsBottomViewEnabled:Z

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupSwitchStatus(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setAutoBackupSwitchStatus(Landroid/content/Context;Z)V

    .line 205
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 237
    :cond_2
    return-void

    :cond_3
    move v1, v2

    .line 187
    goto :goto_0

    .line 194
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIsBottomViewEnabled:Z

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupSwitchStatus(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setAutoBackupSwitchStatus(Landroid/content/Context;Z)V

    .line 197
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->cancelReminderNotification(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    goto :goto_1

    .line 199
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setAutoBackupSwitchStatus(Landroid/content/Context;Z)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mPeriodicSyncManager:Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->cancelPeriodicSync(Landroid/content/Context;)Z

    .line 202
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v1, p0, v3, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->scheduleReminderNotification(Landroid/content/Context;ILcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    goto :goto_1
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 162
    const-string/jumbo v0, "network_failed_popup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "local_failed_popup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "overwrite_confirm_popup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "migration_progress_popup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mDataMigrationHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    .line 168
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0
.end method

.method getRestoreHelper()Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    return-object v0
.end method

.method public initLayout()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 256
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupSwitchStatus(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 257
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->setEnableListView(Z)V

    .line 262
    :goto_0
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->interval:[J

    invoke-direct {v1, p0, p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Landroid/content/Context;[J)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    .line 264
    const v1, 0x7f080053

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIntervalListView:Landroid/widget/ListView;

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIntervalListView:Landroid/widget/ListView;

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 266
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIntervalListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->notifyDataSetChanged()V

    .line 269
    const v1, 0x7f080613

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->nextButtonLayout:Landroid/widget/RelativeLayout;

    .line 271
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->nextButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const v1, 0x7f080052

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090d22

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901fd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIsBottomViewEnabled:Z

    if-nez v1, :cond_1

    .line 275
    const v1, 0x7f080054

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 296
    :goto_1
    const v1, 0x7f080051

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 297
    .local v0, "mAutoBackupWifiEnable":Landroid/widget/CheckBox;
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIsBottomViewEnabled:Z

    if-eqz v1, :cond_2

    .line 299
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 300
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setAutoBackupWifiEnabled(Landroid/content/Context;Z)V

    .line 306
    :goto_2
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$4;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 322
    const v1, 0x7f08004f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$5;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupInterval(Landroid/content/Context;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSyncInterval:J

    .line 331
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->notifyDataSetChanged()V

    .line 333
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupSwitchStatus(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 334
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->setEnableListView(Z)V

    .line 338
    :goto_3
    return-void

    .line 259
    .end local v0    # "mAutoBackupWifiEnable":Landroid/widget/CheckBox;
    :cond_0
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->setEnableListView(Z)V

    goto/16 :goto_0

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->nextButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->nextButtonLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 304
    .restart local v0    # "mAutoBackupWifiEnable":Landroid/widget/CheckBox;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupWifiEnabled(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2

    .line 336
    :cond_3
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->setEnableListView(Z)V

    goto :goto_3
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 395
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onActivityResult() requestCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iget v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mMigrationState:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mMigrationState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mDataMigrationHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->handleUserInteraction(IILandroid/content/Intent;)Z

    .line 404
    :goto_0
    return-void

    .line 402
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->handleUserInteraction(IILandroid/content/Intent;)Z

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 78
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    iput-object p0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mContext:Landroid/content/Context;

    .line 80
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mUIPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    if-nez v4, :cond_0

    .line 81
    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mUIPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 84
    .local v0, "settingIntent":Landroid/content/Intent;
    const-string v4, "launched_from_settings"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIsBottomViewEnabled:Z

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mMigrationState:I

    .line 88
    const v2, 0x7f03000e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->setContentView(I)V

    .line 90
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v2, :cond_1

    .line 91
    const v2, 0x7f090d4d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "tmpString":Ljava/lang/String;
    const-string v2, "Wi-Fi"

    const-string v4, "WLAN"

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 93
    const v2, 0x7f080050

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    .end local v1    # "tmpString":Ljava/lang/String;
    :cond_1
    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .line 97
    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mDataMigrationHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->initLayout()V

    .line 100
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isRestoreOngoing(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 102
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->doNormalInitialization(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Z

    .line 127
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->setContentProviderState(Z)V

    .line 131
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->registerProfileFinishedReceiver()V

    .line 132
    return-void

    :cond_3
    move v2, v3

    .line 84
    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->unregisterProfileFinishedReceiver()V

    .line 156
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 157
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 527
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "S014"

    const-string v3, "ON"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 535
    return-void

    .line 531
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "S014"

    const-string v3, "OFF"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 137
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 140
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->finish()V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->restoreState()V

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mDataMigrationHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    if-eqz v0, :cond_2

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mDataMigrationHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->migrationState()V

    .line 151
    :cond_2
    return-void
.end method

.method protected onResumeFragments()V
    .locals 3

    .prologue
    .line 343
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResumeFragments()V

    .line 346
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIsBottomViewEnabled:Z

    if-nez v1, :cond_0

    .line 347
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 349
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/SettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 350
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 351
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->startActivity(Landroid/content/Intent;)V

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->finish()V

    .line 357
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    if-nez v1, :cond_1

    .line 359
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->refreshWithoutContent()V

    .line 362
    :cond_1
    return-void
.end method

.method public restoreCompleted(Z)V
    .locals 3
    .param p1, "restored"    # Z

    .prologue
    .line 540
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 541
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "profileRestored"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->isProfileRestored:Z

    if-nez v1, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 542
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->startActivity(Landroid/content/Intent;)V

    .line 543
    return-void

    .line 541
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
