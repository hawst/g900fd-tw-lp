.class public final enum Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;
.super Ljava/lang/Enum;
.source "RegiEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field public static final enum RESULT_ALREADY_REGISTERED:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field public static final enum RESULT_BLOCKED_APP:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field public static final enum RESULT_FAIL:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field public static final enum RESULT_HTTP_FAIL:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field public static final enum RESULT_INTERNAL_DB_ERROR:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field public static final enum RESULT_INTERNAL_ERROR:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field public static final enum RESULT_INVALID_PARAMS:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field public static final enum RESULT_SUCCESS:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field public static final enum RESULT_TIMEOUT:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field public static final enum RESUlT_PACKAGE_NOT_FOUND:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field public static final enum UNKNOWN:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

.field private static final amount:I

.field private static eventSet:[Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 26
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    const-string v5, "RESULT_SUCCESS"

    const/16 v6, 0x64

    invoke-direct {v4, v5, v8, v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_SUCCESS:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 27
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    const-string v5, "RESULT_ALREADY_REGISTERED"

    const/16 v6, 0xc8

    invoke-direct {v4, v5, v9, v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_ALREADY_REGISTERED:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 30
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    const-string v5, "RESULT_FAIL"

    const/4 v6, -0x1

    invoke-direct {v4, v5, v10, v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_FAIL:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 31
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    const-string v5, "RESULT_INVALID_PARAMS"

    const/4 v6, -0x2

    invoke-direct {v4, v5, v11, v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_INVALID_PARAMS:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 32
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    const-string v5, "RESULT_INTERNAL_ERROR"

    const/4 v6, -0x3

    invoke-direct {v4, v5, v12, v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_INTERNAL_ERROR:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 33
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    const-string v5, "RESULT_INTERNAL_DB_ERROR"

    const/4 v6, 0x5

    const/4 v7, -0x4

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_INTERNAL_DB_ERROR:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 34
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    const-string v5, "RESULT_HTTP_FAIL"

    const/4 v6, 0x6

    const/4 v7, -0x5

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_HTTP_FAIL:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 35
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    const-string v5, "RESULT_TIMEOUT"

    const/4 v6, 0x7

    const/4 v7, -0x6

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_TIMEOUT:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 36
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    const-string v5, "RESULT_BLOCKED_APP"

    const/16 v6, 0x8

    const/4 v7, -0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_BLOCKED_APP:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 37
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    const-string v5, "RESUlT_PACKAGE_NOT_FOUND"

    const/16 v6, 0x9

    const/4 v7, -0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESUlT_PACKAGE_NOT_FOUND:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 38
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    const-string v5, "UNKNOWN"

    const/16 v6, 0xa

    const/16 v7, -0x3e8

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->UNKNOWN:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 23
    const/16 v4, 0xb

    new-array v4, v4, [Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_SUCCESS:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    aput-object v5, v4, v8

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_ALREADY_REGISTERED:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    aput-object v5, v4, v9

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_FAIL:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    aput-object v5, v4, v10

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_INVALID_PARAMS:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    aput-object v5, v4, v11

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_INTERNAL_ERROR:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    aput-object v5, v4, v12

    const/4 v5, 0x5

    sget-object v6, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_INTERNAL_DB_ERROR:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    aput-object v6, v4, v5

    const/4 v5, 0x6

    sget-object v6, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_HTTP_FAIL:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_TIMEOUT:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_BLOCKED_APP:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESUlT_PACKAGE_NOT_FOUND:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    sget-object v6, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->UNKNOWN:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    aput-object v6, v4, v5

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->$VALUES:[Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 50
    const-class v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    invoke-static {v4}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/AbstractCollection;->size()I

    move-result v4

    sput v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->amount:I

    .line 51
    sget v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->amount:I

    new-array v4, v4, [Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    sput-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->eventSet:[Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "i":I
    const-class v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    invoke-static {v4}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 55
    .local v3, "q":Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;
    sget-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->eventSet:[Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput-object v3, v4, v0

    move v0, v1

    .line 56
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 57
    .end local v3    # "q":Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->value:I

    .line 42
    return-void
.end method

.method public static fromInt(I)Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;
    .locals 3
    .param p0, "i"    # I

    .prologue
    .line 60
    const-class v2, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    invoke-static {v2}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    .line 61
    .local v1, "q":Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->value()I

    move-result v2

    if-ne v2, p0, :cond_0

    .line 65
    .end local v1    # "q":Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;
    :goto_0
    return-object v1

    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->UNKNOWN:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->$VALUES:[Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    return-object v0
.end method


# virtual methods
.method public value()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->value:I

    return v0
.end method
