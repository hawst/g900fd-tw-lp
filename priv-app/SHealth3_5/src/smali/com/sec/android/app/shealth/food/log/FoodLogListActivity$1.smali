.class Lcom/sec/android/app/shealth/food/log/FoodLogListActivity$1;
.super Ljava/lang/Object;
.source "FoodLogListActivity.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity$1;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/AbsListView;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 87
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2
    .param p1, "arg0"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 73
    if-nez p2, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity$1;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;

    # getter for: Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mFoodLogListConcreteDelegate:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->access$000(Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;)Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->setScrollEnabled(Z)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity$1;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;

    # invokes: Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->access$100(Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->notifyDataSetChanged()V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity$1;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->invalidate()V

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity$1;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;

    # getter for: Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->mFoodLogListConcreteDelegate:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;->access$000(Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;)Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->setScrollEnabled(Z)V

    goto :goto_0
.end method
