.class Lcom/sec/android/app/shealth/award/AwardActivity$3;
.super Ljava/lang/Object;
.source "AwardActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/award/AwardActivity;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/award/AwardActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/award/AwardActivity;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/sec/android/app/shealth/award/AwardActivity$3;->this$0:Lcom/sec/android/app/shealth/award/AwardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 296
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity$3;->this$0:Lcom/sec/android/app/shealth/award/AwardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/award/AwardActivity;->doTakePhotoAction()V

    .line 300
    :cond_0
    if-nez p1, :cond_1

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity$3;->this$0:Lcom/sec/android/app/shealth/award/AwardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/award/AwardActivity;->doTakeAlbumAction()V

    .line 304
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity$3;->this$0:Lcom/sec/android/app/shealth/award/AwardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/award/AwardActivity;->doDeletePhotoAction()V

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity$3;->this$0:Lcom/sec/android/app/shealth/award/AwardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/award/AwardActivity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity$3;->this$0:Lcom/sec/android/app/shealth/award/AwardActivity;

    # getter for: Lcom/sec/android/app/shealth/award/AwardActivity;->mDrawerMenuList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/award/AwardActivity;->access$300(Lcom/sec/android/app/shealth/award/AwardActivity;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity$3;->this$0:Lcom/sec/android/app/shealth/award/AwardActivity;

    # getter for: Lcom/sec/android/app/shealth/award/AwardActivity;->mDrawerMenuList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/award/AwardActivity;->access$400(Lcom/sec/android/app/shealth/award/AwardActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 314
    :cond_2
    return-void
.end method
