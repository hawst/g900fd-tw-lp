.class public Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;
.super Ljava/lang/Object;
.source "ReminderNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;


# instance fields
.field private mPendingIntent:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->TAG:Ljava/lang/String;

    .line 20
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->mInstance:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->mPendingIntent:Landroid/app/PendingIntent;

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->mInstance:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    return-object v0
.end method


# virtual methods
.method public cancelReminderNotification(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    .prologue
    .line 56
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->isReminderNotificationEnable(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->isReminderNotificationScheduled(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->TAG:Ljava/lang/String;

    const-string v2, "Cancel reminder notification"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const-string v1, "alarm"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 61
    .local v0, "am":Landroid/app/AlarmManager;
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 62
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->mPendingIntent:Landroid/app/PendingIntent;

    .line 63
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setReminderNotificationCount(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public dismissNotification(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "notification_id"    # I

    .prologue
    .line 101
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->TAG:Ljava/lang/String;

    const-string v2, "dismissNotification"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const-string/jumbo v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 103
    .local v0, "mNotificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v0, p2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 104
    return-void
.end method

.method public isReminderNotificationEnable(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    .prologue
    .line 82
    const/4 v1, 0x0

    .line 84
    .local v1, "ai":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x80

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 88
    :goto_0
    const/4 v3, 0x0

    .line 89
    .local v3, "isEnable":Z
    if-eqz v1, :cond_0

    .line 90
    iget-object v0, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 91
    .local v0, "aBundle":Landroid/os/Bundle;
    sget-object v4, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    if-ne p2, v4, :cond_1

    .line 92
    const-string v4, "AutoBackupON"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 96
    .end local v0    # "aBundle":Landroid/os/Bundle;
    :cond_0
    :goto_1
    sget-object v4, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isReminderNotificationEnable: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    return v3

    .line 85
    .end local v3    # "isEnable":Z
    :catch_0
    move-exception v2

    .line 86
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 93
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "aBundle":Landroid/os/Bundle;
    .restart local v3    # "isEnable":Z
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    if-ne p2, v4, :cond_0

    .line 94
    const-string v4, "SignupNotification"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    goto :goto_1
.end method

.method public isReminderNotificationScheduled(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    .prologue
    .line 107
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->mPendingIntent:Landroid/app/PendingIntent;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 108
    .local v0, "alarmUp":Z
    :goto_0
    return v0

    .line 107
    .end local v0    # "alarmUp":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reScheduleReminderNotification(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->TAG:Ljava/lang/String;

    const-string v1, "ReSchedule reminder notification"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->cancelReminderNotification(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    .line 74
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setReminderNotificationCount(Landroid/content/Context;I)V

    .line 75
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, p2}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->scheduleReminderNotification(Landroid/content/Context;ILcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    .line 78
    return-void
.end method

.method public scheduleReminderNotification(Landroid/content/Context;ILcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "minutes"    # I
    .param p3, "type"    # Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-virtual {p0, p1, p3}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->isReminderNotificationEnable(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1, p3}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->isReminderNotificationScheduled(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->TAG:Ljava/lang/String;

    const-string v3, "Schedule reminder notification"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    const-string v2, "alarm"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 45
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 46
    .local v7, "intent":Landroid/content/Intent;
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    if-ne p3, v2, :cond_3

    .line 47
    const-string v2, "com.sec.shealth.intent.action.AUTOBACKUP_OFF"

    invoke-virtual {v7, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    :cond_2
    :goto_1
    invoke-static {p1, v1, v7, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->mPendingIntent:Landroid/app/PendingIntent;

    .line 52
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    mul-int/lit16 v4, p2, 0x2760

    mul-int/lit8 v4, v4, 0x3c

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    add-long/2addr v2, v4

    mul-int/lit16 v4, p2, 0x2760

    mul-int/lit8 v4, v4, 0x3c

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_0

    .line 48
    :cond_3
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    if-ne p3, v2, :cond_2

    .line 49
    const-string v2, "com.sec.shealth.intent.action.SAMSUNG_ACCOUNT_OFF"

    invoke-virtual {v7, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method
