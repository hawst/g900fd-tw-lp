.class public Lcom/sec/android/app/shealth/stress/data/StressData;
.super Ljava/lang/Object;
.source "StressData.java"


# instance fields
.field private applicationId:Ljava/lang/String;

.field private average:D

.field private comment:Ljava/lang/String;

.field private createTime:J

.field private heartRate:I

.field private id:J

.field private max:D

.field private min:D

.field private sampleTime:J

.field private score:D

.field private state:I

.field private updateTime:J

.field private userDeviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAverage()D
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->average:D

    return-wide v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->id:J

    return-wide v0
.end method

.method public getMax()D
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->max:D

    return-wide v0
.end method

.method public getMin()D
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->min:D

    return-wide v0
.end method

.method public getSampleTime()J
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->sampleTime:J

    return-wide v0
.end method

.method public getScore()D
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->score:D

    return-wide v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->state:I

    return v0
.end method

.method public getUserDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->userDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public setApplicationId(Ljava/lang/String;)V
    .locals 0
    .param p1, "applicationId"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->applicationId:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setAverage(D)V
    .locals 0
    .param p1, "average"    # D

    .prologue
    .line 119
    iput-wide p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->average:D

    .line 120
    return-void
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->comment:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "createTime"    # J

    .prologue
    .line 79
    iput-wide p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->createTime:J

    .line 80
    return-void
.end method

.method public setHeartRate(I)V
    .locals 0
    .param p1, "heartRate"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->heartRate:I

    .line 64
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->id:J

    .line 24
    return-void
.end method

.method public setMax(D)V
    .locals 0
    .param p1, "max"    # D

    .prologue
    .line 111
    iput-wide p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->max:D

    .line 112
    return-void
.end method

.method public setMin(D)V
    .locals 0
    .param p1, "min"    # D

    .prologue
    .line 103
    iput-wide p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->min:D

    .line 104
    return-void
.end method

.method public setSampleTime(J)V
    .locals 0
    .param p1, "sampleTime"    # J

    .prologue
    .line 71
    iput-wide p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->sampleTime:J

    .line 72
    return-void
.end method

.method public setScore(D)V
    .locals 0
    .param p1, "score"    # D

    .prologue
    .line 55
    iput-wide p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->score:D

    .line 56
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->state:I

    .line 48
    return-void
.end method

.method public setUpdateTime(J)V
    .locals 0
    .param p1, "updateTime"    # J

    .prologue
    .line 87
    iput-wide p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->updateTime:J

    .line 88
    return-void
.end method

.method public setUserDeviceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userDeviceId"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/data/StressData;->userDeviceId:Ljava/lang/String;

    .line 40
    return-void
.end method
