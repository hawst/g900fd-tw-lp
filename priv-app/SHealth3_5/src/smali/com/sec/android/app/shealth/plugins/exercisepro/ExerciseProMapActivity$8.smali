.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$8;
.super Ljava/lang/Object;
.source "ExerciseProMapActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->initLayout(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0

    .prologue
    .line 463
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->delay:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)I

    move-result v1

    invoke-static {v0, v1, v2, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->DialogViewModeSetting(Landroid/app/Activity;IZZ)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getMapMode()I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1202(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;I)I

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->viewModeHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->viewModeRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->delay:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$002(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)Z

    .line 472
    :cond_0
    return-void
.end method
