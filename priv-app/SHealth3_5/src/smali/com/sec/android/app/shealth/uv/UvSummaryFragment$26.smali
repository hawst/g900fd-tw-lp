.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;
.super Ljava/lang/Object;
.source "UvSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initBuilder()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0

    .prologue
    .line 2667
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 7
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v4, 0x1

    .line 2672
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v1, v4}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2674
    .local v0, "value":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 2675
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    const/16 v2, 0x1e

    # setter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3302(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)I

    .line 2676
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v1, v4}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3300(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2685
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$500(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090db8

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3300(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2687
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfSeekText:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3400(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/RelativeLayout;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfTextView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$500(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    const v4, 0x7f09020a

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2692
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3300(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->saveSelectedSPF(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3500(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)V

    .line 2693
    return-void

    .line 2678
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 2679
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # setter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3302(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)I

    .line 2680
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v1, v4}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2683
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3302(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)I

    goto/16 :goto_0
.end method
