.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;
.super Ljava/lang/Object;
.source "UvSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initBuilder()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0

    .prologue
    .line 2563
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 6
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const/4 v5, 0x1

    .line 2570
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    const v3, 0x7f080af1

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/NumberPicker;

    iput-object v3, v4, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    .line 2572
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v3, v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v3, v5}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 2573
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v3, v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    const/16 v4, 0x3e8

    invoke-virtual {v3, v4}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 2574
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v3, v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3300(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 2579
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v3, v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    const/high16 v4, 0x60000

    invoke-virtual {v3, v4}, Landroid/widget/NumberPicker;->setDescendantFocusability(I)V

    .line 2581
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v3, v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    new-instance v4, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$1;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;)V

    invoke-virtual {v3, v4}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    .line 2593
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v3, v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v3, v5}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 2595
    .local v2, "e1":Landroid/widget/EditText;
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v3, v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2596
    .local v0, "b1Down":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v3, v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2598
    .local v1, "b2Down":Landroid/view/View;
    new-instance v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$2;

    invoke-direct {v3, p0, v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$2;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2614
    new-instance v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$3;

    invoke-direct {v3, p0, v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$3;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;Landroid/widget/EditText;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2630
    new-instance v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$4;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2664
    return-void
.end method
