.class Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$AnimeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setupDateBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->access$000(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->access$100(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->access$000(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setPressed(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->access$100(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setPressed(Z)V

    return-void
.end method

.method public onAnimationStart()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->access$000(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->access$100(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    return-void
.end method
