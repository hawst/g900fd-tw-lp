.class public Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;
.super Ljava/lang/Object;
.source "ExerciseData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cadence:I

.field private comment:Ljava/lang/String;

.field private createTime:J

.field private deviceInfoId:J

.field private distance:I

.field private distanceUnit:I

.field private durationMin:I

.field private endTime:J

.field private energyExpended:F

.field private exerciseInfoId:J

.field private exerciseType:I

.field private goalId:J

.field private id:J

.field private startTime:J

.field private timeZone:I

.field private updateTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 238
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-wide/16 v0, -0x2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->id:J

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->comment:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public constructor <init>(JJJJIIIIFILjava/lang/String;JJJJI)V
    .locals 5
    .param p1, "id"    # J
    .param p3, "exerciseInfoId"    # J
    .param p5, "deviceInfoId"    # J
    .param p7, "goalId"    # J
    .param p9, "exerciseType"    # I
    .param p10, "distance"    # I
    .param p11, "distanceUnit"    # I
    .param p12, "cadence"    # I
    .param p13, "energyExpended"    # F
    .param p14, "durationMin"    # I
    .param p15, "comment"    # Ljava/lang/String;
    .param p16, "startTime"    # J
    .param p18, "endTime"    # J
    .param p20, "createTime"    # J
    .param p22, "updateTime"    # J
    .param p24, "timeZone"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-wide/16 v2, -0x2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->id:J

    .line 22
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->comment:Ljava/lang/String;

    .line 31
    const-wide/16 v2, -0x2

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    .line 33
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id should not be "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 35
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->id:J

    .line 36
    iput-wide p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->exerciseInfoId:J

    .line 37
    iput-wide p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->deviceInfoId:J

    .line 38
    iput-wide p7, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->goalId:J

    .line 39
    iput p9, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->exerciseType:I

    .line 40
    iput p10, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->distance:I

    .line 41
    move/from16 v0, p11

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->distanceUnit:I

    .line 42
    move/from16 v0, p12

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->cadence:I

    .line 43
    move/from16 v0, p13

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->energyExpended:F

    .line 44
    move/from16 v0, p14

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->durationMin:I

    .line 45
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->comment:Ljava/lang/String;

    .line 46
    move-wide/from16 v0, p16

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->startTime:J

    .line 47
    move-wide/from16 v0, p18

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->endTime:J

    .line 48
    move-wide/from16 v0, p20

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->createTime:J

    .line 49
    move-wide/from16 v0, p22

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->updateTime:J

    .line 50
    move/from16 v0, p24

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->timeZone:I

    .line 51
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    return v0
.end method

.method public getCadence()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->cadence:I

    return v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 189
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->createTime:J

    return-wide v0
.end method

.method public getDeviceInfoId()J
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->deviceInfoId:J

    return-wide v0
.end method

.method public getDistance()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->distance:I

    return v0
.end method

.method public getDistanceUnit()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->distanceUnit:I

    return v0
.end method

.method public getDurationMin()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->durationMin:I

    return v0
.end method

.method public getEndTime()J
    .locals 2

    .prologue
    .line 179
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->endTime:J

    return-wide v0
.end method

.method public getEnergyExpended()F
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->energyExpended:F

    return v0
.end method

.method public getExerciseInfoId()J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->exerciseInfoId:J

    return-wide v0
.end method

.method public getExerciseType()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->exerciseType:I

    return v0
.end method

.method public getGoalId()J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->goalId:J

    return-wide v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->id:J

    return-wide v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 169
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->startTime:J

    return-wide v0
.end method

.method public getTimeZone()I
    .locals 1

    .prologue
    .line 209
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->timeZone:I

    return v0
.end method

.method public getUpdateTime()J
    .locals 2

    .prologue
    .line 199
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->updateTime:J

    return-wide v0
.end method

.method public setCadence(I)V
    .locals 0
    .param p1, "cadence"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->cadence:I

    .line 135
    return-void
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->comment:Ljava/lang/String;

    .line 165
    return-void
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "createTime"    # J

    .prologue
    .line 194
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->createTime:J

    .line 195
    return-void
.end method

.method public setDeviceInfoId(J)V
    .locals 0
    .param p1, "deviceInfoId"    # J

    .prologue
    .line 84
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->deviceInfoId:J

    .line 85
    return-void
.end method

.method public setDistance(I)V
    .locals 0
    .param p1, "distance"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->distance:I

    .line 115
    return-void
.end method

.method public setDistanceUnit(I)V
    .locals 0
    .param p1, "distanceUnit"    # I

    .prologue
    .line 124
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->distanceUnit:I

    .line 125
    return-void
.end method

.method public setDurationMin(I)V
    .locals 0
    .param p1, "durationMin"    # I

    .prologue
    .line 154
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->durationMin:I

    .line 155
    return-void
.end method

.method public setEndTime(J)V
    .locals 0
    .param p1, "endTime"    # J

    .prologue
    .line 184
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->endTime:J

    .line 185
    return-void
.end method

.method public setEnergyExpended(F)V
    .locals 0
    .param p1, "energyExpended"    # F

    .prologue
    .line 144
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->energyExpended:F

    .line 145
    return-void
.end method

.method public setExerciseInfoId(J)V
    .locals 0
    .param p1, "exerciseInfoId"    # J

    .prologue
    .line 74
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->exerciseInfoId:J

    .line 75
    return-void
.end method

.method public setExerciseType(I)V
    .locals 0
    .param p1, "exerciseType"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->exerciseType:I

    .line 105
    return-void
.end method

.method public setGoalId(J)V
    .locals 0
    .param p1, "goalId"    # J

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->goalId:J

    .line 95
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->id:J

    .line 65
    return-void
.end method

.method public setStartTime(J)V
    .locals 0
    .param p1, "startTime"    # J

    .prologue
    .line 174
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->startTime:J

    .line 175
    return-void
.end method

.method public setTimeZone(I)V
    .locals 0
    .param p1, "timeZone"    # I

    .prologue
    .line 214
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->timeZone:I

    .line 215
    return-void
.end method

.method public setUpdateTime(J)V
    .locals 0
    .param p1, "updateTime"    # J

    .prologue
    .line 204
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->updateTime:J

    .line 205
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 220
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 221
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->exerciseInfoId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 222
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->deviceInfoId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 223
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->goalId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 224
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->exerciseType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 225
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->distance:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 226
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->distanceUnit:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 227
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->cadence:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 228
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->energyExpended:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 229
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->durationMin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->comment:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 231
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->startTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 232
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->endTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 233
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->createTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 234
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->updateTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 235
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseData;->timeZone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 236
    return-void
.end method
