.class Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$1;
.super Ljava/lang/Object;
.source "MissionCompleteActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->setCustomActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 148
    instance-of v2, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 149
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 151
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    if-nez v2, :cond_0

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->prepareShareView()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;)V

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.cignacoach"

    const-string v4, "0031"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mFinishDelayHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_0
    :goto_0
    return-void

    .line 157
    .restart local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :catch_0
    move-exception v1

    .line 158
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
