.class Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment$1;
.super Landroid/os/AsyncTask;
.source "LifeStyleOneScoreFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->updateHeaderProgress()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private updateHeaderView()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mCignaLifestyleScoreHeader:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->drawCircleProgress()V

    .line 96
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getOverallScore()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 73
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "scores"    # Ljava/lang/Integer;

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 85
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gez v0, :cond_0

    .line 86
    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GetScoreAsyncTask Result is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment$1;->updateHeaderView()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 73
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment$1;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
