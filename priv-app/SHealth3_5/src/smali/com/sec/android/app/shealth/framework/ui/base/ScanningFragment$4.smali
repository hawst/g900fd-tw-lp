.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 40

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x345d38f5af454336L    # -2.3021149424465023E56

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x345d38f5af454336L    # -2.3021149424465023E56

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x3b81

    aput v12, v2, v11

    const/16 v11, -0x59

    aput v11, v2, v10

    const/16 v10, 0x6374

    aput v10, v2, v9

    const/16 v9, 0x5515

    aput v9, v2, v8

    const/16 v8, 0x5c27

    aput v8, v2, v7

    const/16 v7, -0xc7

    aput v7, v2, v6

    const/16 v6, -0x54

    aput v6, v2, v5

    const/4 v5, -0x6

    aput v5, v2, v4

    const/16 v4, -0x3a

    aput v4, v2, v3

    const/16 v3, -0x56

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x3be6

    aput v13, v1, v12

    const/16 v12, -0x3c

    aput v12, v1, v11

    const/16 v11, 0x631d

    aput v11, v1, v10

    const/16 v10, 0x5563

    aput v10, v1, v9

    const/16 v9, 0x5c55

    aput v9, v1, v8

    const/16 v8, -0xa4

    aput v8, v1, v7

    const/4 v7, -0x1

    aput v7, v1, v6

    const/16 v6, -0x68

    aput v6, v1, v5

    const/16 v5, -0x51

    aput v5, v1, v4

    const/16 v4, -0x3a

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x6

    aput v38, v2, v37

    const/16 v37, -0x4794

    aput v37, v2, v36

    const/16 v36, -0x34

    aput v36, v2, v35

    const/16 v35, -0x4f

    aput v35, v2, v34

    const/16 v34, -0xdca

    aput v34, v2, v33

    const/16 v33, -0x64

    aput v33, v2, v32

    const/16 v32, 0x4856

    aput v32, v2, v31

    const/16 v31, -0x28d9

    aput v31, v2, v30

    const/16 v30, -0x4c

    aput v30, v2, v29

    const/16 v29, -0x51

    aput v29, v2, v28

    const/16 v28, 0x435f

    aput v28, v2, v27

    const/16 v27, -0x5ae0

    aput v27, v2, v26

    const/16 v26, -0x34

    aput v26, v2, v25

    const/16 v25, -0x40

    aput v25, v2, v24

    const/16 v24, -0x1a

    aput v24, v2, v23

    const/16 v23, -0x4b

    aput v23, v2, v22

    const/16 v22, -0x39ef

    aput v22, v2, v21

    const/16 v21, -0x1a

    aput v21, v2, v20

    const/16 v20, -0x51f7

    aput v20, v2, v19

    const/16 v19, -0x72

    aput v19, v2, v18

    const/16 v18, -0x70

    aput v18, v2, v17

    const/16 v17, -0x7d

    aput v17, v2, v16

    const/16 v16, 0x7f6b

    aput v16, v2, v15

    const/16 v15, 0x151b

    aput v15, v2, v14

    const/16 v14, 0x7535

    aput v14, v2, v13

    const/16 v13, 0x4506

    aput v13, v2, v12

    const/16 v12, 0x1b2c

    aput v12, v2, v11

    const/16 v11, 0x113b

    aput v11, v2, v10

    const/16 v10, -0x6a8a

    aput v10, v2, v9

    const/4 v9, -0x5

    aput v9, v2, v8

    const/16 v8, -0xf

    aput v8, v2, v7

    const/16 v7, -0x12

    aput v7, v2, v6

    const/16 v6, 0x740c

    aput v6, v2, v5

    const/16 v5, 0x1d1d

    aput v5, v2, v3

    const/16 v3, 0xf5f

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x62

    aput v39, v1, v38

    const/16 v38, -0x47f7

    aput v38, v1, v37

    const/16 v37, -0x48

    aput v37, v1, v36

    const/16 v36, -0x2e

    aput v36, v1, v35

    const/16 v35, -0xdad

    aput v35, v1, v34

    const/16 v34, -0xe

    aput v34, v1, v33

    const/16 v33, 0x4838

    aput v33, v1, v32

    const/16 v32, -0x28b8

    aput v32, v1, v31

    const/16 v31, -0x29

    aput v31, v1, v30

    const/16 v30, -0x71

    aput v30, v1, v29

    const/16 v29, 0x433a

    aput v29, v1, v28

    const/16 v28, -0x5abd

    aput v28, v1, v27

    const/16 v27, -0x5b

    aput v27, v1, v26

    const/16 v26, -0x4a

    aput v26, v1, v25

    const/16 v25, -0x6c

    aput v25, v1, v24

    const/16 v24, -0x30

    aput v24, v1, v23

    const/16 v23, -0x39be

    aput v23, v1, v22

    const/16 v22, -0x3a

    aput v22, v1, v21

    const/16 v21, -0x51dc

    aput v21, v1, v20

    const/16 v20, -0x52

    aput v20, v1, v19

    const/16 v19, -0xb

    aput v19, v1, v18

    const/16 v18, -0x13

    aput v18, v1, v17

    const/16 v17, 0x7f04

    aput v17, v1, v16

    const/16 v16, 0x157f

    aput v16, v1, v15

    const/16 v15, 0x7515

    aput v15, v1, v14

    const/16 v14, 0x4575

    aput v14, v1, v13

    const/16 v13, 0x1b45

    aput v13, v1, v12

    const/16 v12, 0x111b

    aput v12, v1, v11

    const/16 v11, -0x6aef

    aput v11, v1, v10

    const/16 v10, -0x6b

    aput v10, v1, v9

    const/16 v9, -0x68

    aput v9, v1, v8

    const/16 v8, -0x76

    aput v8, v1, v7

    const/16 v7, 0x7462

    aput v7, v1, v6

    const/16 v6, 0x1d74

    aput v6, v1, v5

    const/16 v5, 0xf1d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->requestPairedDevices()V

    return-void

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method public onServiceDisconnected(I)V
    .locals 25

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x6502e23138849a48L

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x6502e23138849a48L

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x30

    aput v12, v2, v11

    const/16 v11, -0x59

    aput v11, v2, v10

    const/16 v10, -0x30bd

    aput v10, v2, v9

    const/16 v9, -0x47

    aput v9, v2, v8

    const/16 v8, -0x3b4

    aput v8, v2, v7

    const/16 v7, -0x67

    aput v7, v2, v6

    const/16 v6, -0x70

    aput v6, v2, v5

    const/16 v5, -0x80

    aput v5, v2, v4

    const/16 v4, -0x79

    aput v4, v2, v3

    const/16 v3, -0x32ba

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x4b

    aput v13, v1, v12

    const/16 v12, -0x3c

    aput v12, v1, v11

    const/16 v11, -0x30d6

    aput v11, v1, v10

    const/16 v10, -0x31

    aput v10, v1, v9

    const/16 v9, -0x3c2

    aput v9, v1, v8

    const/4 v8, -0x4

    aput v8, v1, v7

    const/16 v7, -0x3d

    aput v7, v1, v6

    const/16 v6, -0x1e

    aput v6, v1, v5

    const/16 v5, -0x12

    aput v5, v1, v4

    const/16 v4, -0x32d6

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x5c01

    aput v23, v2, v22

    const/16 v22, 0x5e39

    aput v22, v2, v21

    const/16 v21, 0x5d2a

    aput v21, v2, v20

    const/16 v20, 0x7c3e

    aput v20, v2, v19

    const/16 v19, -0xfe7

    aput v19, v2, v18

    const/16 v18, -0x62

    aput v18, v2, v17

    const/16 v17, -0x78

    aput v17, v2, v16

    const/16 v16, -0x16

    aput v16, v2, v15

    const/16 v15, -0x3f

    aput v15, v2, v14

    const/4 v14, -0x5

    aput v14, v2, v13

    const/16 v13, -0x28

    aput v13, v2, v12

    const/16 v12, 0x584b

    aput v12, v2, v11

    const/16 v11, -0x2788

    aput v11, v2, v10

    const/16 v10, -0x43

    aput v10, v2, v9

    const/16 v9, -0x11

    aput v9, v2, v8

    const/16 v8, -0x59

    aput v8, v2, v7

    const/16 v7, 0x4158

    aput v7, v2, v6

    const/16 v6, -0x9cd

    aput v6, v2, v5

    const/16 v5, -0x6d

    aput v5, v2, v3

    const/16 v3, 0x560c

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x5c65

    aput v24, v1, v23

    const/16 v23, 0x5e5c

    aput v23, v1, v22

    const/16 v22, 0x5d5e

    aput v22, v1, v21

    const/16 v21, 0x7c5d

    aput v21, v1, v20

    const/16 v20, -0xf84

    aput v20, v1, v19

    const/16 v19, -0x10

    aput v19, v1, v18

    const/16 v18, -0x1a

    aput v18, v1, v17

    const/16 v17, -0x7b

    aput v17, v1, v16

    const/16 v16, -0x5e

    aput v16, v1, v15

    const/16 v15, -0x78

    aput v15, v1, v14

    const/16 v14, -0x4f

    aput v14, v1, v13

    const/16 v13, 0x582f

    aput v13, v1, v12

    const/16 v12, -0x27a8

    aput v12, v1, v11

    const/16 v11, -0x28

    aput v11, v1, v10

    const/16 v10, -0x74

    aput v10, v1, v9

    const/16 v9, -0x32

    aput v9, v1, v8

    const/16 v8, 0x412e

    aput v8, v1, v7

    const/16 v7, -0x9bf

    aput v7, v1, v6

    const/16 v6, -0xa

    aput v6, v1, v5

    const/16 v5, 0x565f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2702(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2902(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    return-void

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method
