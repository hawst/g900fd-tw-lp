.class public Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;
.super Ljava/lang/Object;
.source "AppWidgetBitmapManager.java"


# static fields
.field public static final BITMAP_INIT:I = 0x1

.field public static final BITMAP_UPDATE:I = 0x2

.field public static final RECYCEL_ALL:I = 0x1

.field public static final RECYCLE_INIT:I = 0x2

.field public static final RECYCLE_UPDATE:I = 0x3


# instance fields
.field private mBitmapWhenInit:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mBitmapWhenUpdate:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdating:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->mBitmapWhenInit:Ljava/util/ArrayList;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->mBitmapWhenUpdate:Ljava/util/ArrayList;

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->mUpdating:Z

    return-void
.end method

.method private varargs recycle([Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "arrBitmaps":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    if-eqz p1, :cond_0

    array-length v6, p1

    if-nez v6, :cond_1

    .line 69
    :cond_0
    return-void

    .line 54
    :cond_1
    move-object v0, p1

    .local v0, "arr$":[Ljava/util/ArrayList;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v2, v0, v3

    .line 56
    .local v2, "bmps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_3

    .line 54
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 60
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 61
    .local v4, "itBmps":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/graphics/Bitmap;>;"
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 62
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 63
    .local v1, "bmp":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_4

    .line 64
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 66
    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_1
.end method


# virtual methods
.method public varargs addBitmap(I[Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "type"    # I
    .param p2, "bmps"    # [Landroid/graphics/Bitmap;

    .prologue
    .line 21
    const/4 v1, 0x0

    .line 22
    .local v1, "arrBmps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v5, 0x1

    if-ne p1, v5, :cond_2

    .line 23
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->mBitmapWhenInit:Ljava/util/ArrayList;

    .line 28
    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    array-length v5, p2

    if-nez v5, :cond_3

    .line 32
    :cond_1
    return-void

    .line 24
    :cond_2
    const/4 v5, 0x2

    if-ne p1, v5, :cond_0

    .line 25
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->mBitmapWhenUpdate:Ljava/util/ArrayList;

    goto :goto_0

    .line 29
    :cond_3
    move-object v0, p2

    .local v0, "arr$":[Landroid/graphics/Bitmap;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    .line 30
    .local v2, "bmp":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_4

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public isUpdating()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->mUpdating:Z

    return v0
.end method

.method public recycle(I)V
    .locals 4
    .param p1, "recycleType"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    packed-switch p1, :pswitch_data_0

    .line 47
    :goto_0
    return-void

    .line 38
    :pswitch_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->mBitmapWhenInit:Ljava/util/ArrayList;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->mBitmapWhenUpdate:Ljava/util/ArrayList;

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->recycle([Ljava/util/ArrayList;)V

    goto :goto_0

    .line 41
    :pswitch_1
    new-array v0, v3, [Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->mBitmapWhenInit:Ljava/util/ArrayList;

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->recycle([Ljava/util/ArrayList;)V

    goto :goto_0

    .line 44
    :pswitch_2
    new-array v0, v3, [Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->mBitmapWhenUpdate:Ljava/util/ArrayList;

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->recycle([Ljava/util/ArrayList;)V

    goto :goto_0

    .line 36
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setUpdating(Z)V
    .locals 0
    .param p1, "updating"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->mUpdating:Z

    .line 77
    return-void
.end method
