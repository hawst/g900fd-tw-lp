.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$24;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutController.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->showMinWorkoutIntervalPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V
    .locals 0

    .prologue
    .line 1930
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$24;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelButtonClick(Landroid/app/Activity;)V
    .locals 2
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 1934
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$24;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1935
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$24;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isMinWorkoutPopUpShown:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$4502(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;Z)Z

    .line 1936
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$24;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->restartWorkout()V

    .line 1937
    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveUiLockState(Z)V

    .line 1938
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$24;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateOutdoorLayout()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$2800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V

    .line 1940
    :cond_0
    return-void
.end method
