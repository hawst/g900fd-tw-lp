.class Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$TimerHandler;
.super Landroid/os/Handler;
.source "ProgressUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$TimerHandler;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$TimerHandler;-><init>(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$TimerHandler;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$TimerHandler;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->access$100(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$TimerHandler;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->messagesCount:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->access$200(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->onProgressUpdate(F)V

    .line 60
    return-void
.end method
