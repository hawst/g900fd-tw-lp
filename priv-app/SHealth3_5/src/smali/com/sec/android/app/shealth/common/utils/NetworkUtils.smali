.class public Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
.super Ljava/lang/Object;
.source "NetworkUtils.java"


# static fields
.field private static final DEBUG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mContext:Landroid/content/Context;

    .line 31
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 32
    const-string/jumbo v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/utils/NetworkUtils;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static isAnyNetworkEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    const-string v1, "connectivity"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 42
    .local v0, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    if-ne v1, v2, :cond_1

    .line 44
    :cond_0
    const/4 v1, 0x1

    .line 47
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isMobilePolicyDataEnable()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 244
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v4, "isMobilePolicyDataEnable"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 245
    .local v1, "method":Ljava/lang/reflect/Method;
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 249
    .end local v1    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    .line 249
    goto :goto_0
.end method

.method private isNetworkEnabled(I)Z
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 57
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 58
    .local v0, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 60
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    if-ne v1, v2, :cond_1

    .line 61
    :cond_0
    const/4 v1, 0x1

    .line 65
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setMobileDataEnabled(Z)V
    .locals 7
    .param p1, "enabled"    # Z

    .prologue
    .line 220
    const/4 v3, 0x1

    :try_start_0
    new-array v2, v3, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    .line 221
    .local v2, "paramTypes":[Ljava/lang/Class;
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string/jumbo v4, "setMobileDataEnabled"

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 222
    .local v1, "method":Ljava/lang/reflect/Method;
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    .end local v1    # "method":Ljava/lang/reflect/Method;
    .end local v2    # "paramTypes":[Ljava/lang/Class;
    :goto_0
    return-void

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public isAirplaneModeEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 195
    .local v0, "enabled":Z
    :cond_0
    return v0
.end method

.method public isDataRoamingEnabled()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 162
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "data_roaming"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-ne v3, v1, :cond_0

    .line 169
    :goto_0
    return v1

    :cond_0
    move v1, v2

    .line 162
    goto :goto_0

    .line 167
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v1, v2

    .line 169
    goto :goto_0
.end method

.method public isMobileDataEnabled()Z
    .locals 2

    .prologue
    .line 94
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isNetworkEnabled(I)Z

    move-result v0

    .line 98
    .local v0, "enabled":Z
    return v0
.end method

.method public isMoblieDataLimit()Z
    .locals 2

    .prologue
    .line 137
    const/4 v0, 0x0

    .line 138
    .local v0, "limit":Z
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isMobilePolicyDataEnable()Z

    move-result v0

    .line 142
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNetworkConnected()Z
    .locals 3

    .prologue
    .line 253
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 254
    .local v0, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    .line 255
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    .line 257
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isRoamingEnabled()Z
    .locals 2

    .prologue
    .line 148
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v0

    .line 155
    .local v0, "enabled":Z
    return v0
.end method

.method public isSimReady()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    .line 123
    .local v0, "SimState":I
    if-eqz v0, :cond_0

    if-ne v0, v1, :cond_1

    .line 127
    :cond_0
    const/4 v1, 0x0

    .line 132
    :cond_1
    return v1
.end method

.method public isWifiEnabled()Z
    .locals 2

    .prologue
    .line 71
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isNetworkEnabled(I)Z

    move-result v0

    .line 75
    .local v0, "enabled":Z
    return v0
.end method

.method public setAirplaneModeEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 200
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils$1;-><init>(Lcom/sec/android/app/shealth/common/utils/NetworkUtils;Z)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 213
    return-void
.end method

.method public setDataRoamingEnabled(Z)V
    .locals 4
    .param p1, "value"    # Z

    .prologue
    const/4 v1, 0x1

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isMobileDataEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 176
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->setMobileEnabled(Z)V

    .line 181
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "data_roaming"

    if-eqz p1, :cond_1

    :goto_0
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v0

    .line 186
    .local v0, "enabled":Z
    return-void

    .line 181
    .end local v0    # "enabled":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMobileEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isSimReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->setMobileDataEnabled(Z)V

    .line 112
    :cond_0
    return-void
.end method

.method public setWifiEnabled(Z)Z
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 83
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1, p1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v0

    .line 87
    .local v0, "state":Z
    return v0
.end method
