.class public Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;
.super Ljava/lang/Object;
.source "PedometerUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;,
        Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;
    }
.end annotation


# static fields
.field public static final FILTER_ALL:Ljava/lang/String; = "0==0"

.field public static final NOT_ASSIGNED:I = -0x1

.field public static final SQLITE_WFL_GROUPBY_DAY:Ljava/lang/String; = "strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\')"

.field private static TAG:Ljava/lang/String;

.field private static mContext:Landroid/content/Context;

.field private static mInstance:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;


# instance fields
.field private mBestGoalInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

.field private mFirstDate:J

.field private mLastDate:J

.field private mListGoal:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    .line 69
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mInstance:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    .line 70
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mContext:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mBestGoalInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    .line 72
    iput-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mFirstDate:J

    .line 73
    iput-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mLastDate:J

    .line 74
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mListGoal:Ljava/util/ArrayList;

    .line 86
    return-void
.end method

.method public static allowDeviceConnectionSwitch(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 594
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v0

    .line 595
    .local v0, "deviceConnected":I
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[FIRSTCONNECTION]allowDeviceConnectionSwitch - deviceConnected:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    const/4 v5, -0x1

    if-eq v0, v5, :cond_3

    .line 597
    const/4 v1, 0x0

    .line 601
    .local v1, "status":Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getSharedPrefKeyOfVersion3_2(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isFirstDeviceConnectedCheck(Ljava/lang/String;)Z

    move-result v2

    .line 603
    .local v2, "statusForVersion3_2_3":Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isFirstDeviceConnection(I)Z

    move-result v3

    .line 605
    .local v3, "statusForVersion3_5_1":Z
    if-nez v2, :cond_1

    if-eqz v3, :cond_1

    .line 607
    const/4 v1, 0x1

    .line 617
    :goto_0
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[FIRSTCONNECTION]allowDeviceConnectionSwitch - status:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    if-eqz v1, :cond_0

    .line 619
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setFirstConnectionAsSuccess(IZ)V

    .line 623
    .end local v1    # "status":Z
    .end local v2    # "statusForVersion3_2_3":Z
    .end local v3    # "statusForVersion3_5_1":Z
    :cond_0
    :goto_1
    return v1

    .line 609
    .restart local v1    # "status":Z
    .restart local v2    # "statusForVersion3_2_3":Z
    .restart local v3    # "statusForVersion3_5_1":Z
    :cond_1
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 611
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setFirstConnectionAsSuccess(IZ)V

    goto :goto_0

    .line 615
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .end local v1    # "status":Z
    .end local v2    # "statusForVersion3_2_3":Z
    .end local v3    # "statusForVersion3_5_1":Z
    :cond_3
    move v1, v4

    .line 623
    goto :goto_1
.end method

.method public static get12Hrformat(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "format24hr"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 791
    move-object v0, p0

    .line 793
    .local v0, "date12Hrformat":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "H:mm"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 794
    .local v3, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v3, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 795
    .local v1, "dateObj":Ljava/util/Date;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "hh:mm a"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 799
    .end local v1    # "dateObj":Ljava/util/Date;
    .end local v3    # "sdf":Ljava/text/SimpleDateFormat;
    :goto_0
    return-object v0

    .line 796
    :catch_0
    move-exception v2

    .line 797
    .local v2, "e":Ljava/text/ParseException;
    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public static get24Hrformat(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "format24hr"    # Ljava/lang/String;

    .prologue
    .line 804
    move-object v0, p0

    .line 806
    .local v0, "date24Hrformat":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "HH:mm"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 807
    .local v3, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v3, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 808
    .local v1, "dateObj":Ljava/util/Date;
    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 812
    .end local v1    # "dateObj":Ljava/util/Date;
    .end local v3    # "sdf":Ljava/text/SimpleDateFormat;
    :goto_0
    return-object v0

    .line 809
    :catch_0
    move-exception v2

    .line 810
    .local v2, "e":Ljava/text/ParseException;
    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getConnectedDeviceType(Landroid/content/Context;)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 384
    const/4 v2, -0x1

    .line 387
    .local v2, "connectedDeviceType":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "connected_wearable_id"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 389
    .local v0, "connectedDeviceAddress":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "connected_wearable"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 391
    .local v1, "connectedDeviceName":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "wmanager_connected"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 393
    .local v3, "connectionStatus":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isActivityTrackerConnected(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 394
    const/16 v2, 0x2727

    .line 413
    :cond_0
    :goto_0
    const/4 v4, -0x1

    if-ne v2, v4, :cond_9

    .line 414
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v5, "connectedDeviceType : NOT_ASSIGNED"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :goto_1
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setLastConnectedDeviceType(I)V

    .line 422
    return v2

    .line 396
    :cond_1
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_8

    .line 397
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connected wearabe name :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const/16 v4, 0x2724

    invoke-static {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->hasWearableManager(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 399
    const/16 v2, 0x2724

    goto :goto_0

    .line 400
    :cond_3
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/16 v4, 0x2726

    invoke-static {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->hasWearableManager(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 401
    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceTypeForGearTizen(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 402
    :cond_4
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR 3"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR S"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_5
    const/16 v4, 0x272e

    invoke-static {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->hasWearableManager(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 403
    const/16 v2, 0x272e

    goto/16 :goto_0

    .line 404
    :cond_6
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR O"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x2730

    invoke-static {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->hasWearableManager(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 405
    const/16 v2, 0x2730

    goto/16 :goto_0

    .line 406
    :cond_7
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "WINGTIP"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v4, 0x2723

    invoke-static {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->hasWearableManager(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 407
    const/16 v2, 0x2723

    goto/16 :goto_0

    .line 410
    :cond_8
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v5, "connectedDeviceName is invalid"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 416
    :cond_9
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setLastConnectedDeviceType(I)V

    .line 417
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connectedDeviceType : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", connectedDeviceAddress : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", connectionStatus : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private static getConnectedDeviceTypeForGearTizen(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deviceAddress"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 518
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 519
    .local v0, "adapter":Landroid/bluetooth/BluetoothAdapter;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v3

    const/16 v4, 0xa

    if-ne v3, v4, :cond_1

    .line 520
    :cond_0
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v4, "Error: getConnectedDevices: adapter is null or bluetooth is turned off, cannot find device type"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    :goto_0
    return v2

    .line 523
    :cond_1
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " getConnectedDevices btAddress="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", str="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    .line 525
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 526
    .local v1, "btDevice":Landroid/bluetooth/BluetoothDevice;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 527
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " getConnectedDevices alias="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAliasName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", name="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    invoke-static {p2, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getDeviceTypeForGearTizen(Ljava/lang/String;Landroid/os/Parcelable;)I

    move-result v2

    .line 529
    .local v2, "deviceType":I
    goto :goto_0

    .line 531
    .end local v2    # "deviceType":I
    :cond_2
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v4, "Error1: getConnectedDevices: couldnot find device type"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 534
    .end local v1    # "btDevice":Landroid/bluetooth/BluetoothDevice;
    :cond_3
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v4, "Error2: getConnectedDevices: couldnot find device type"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getConnectedDeviceTypewithDeviceId(Landroid/content/Context;I)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deviceId"    # I

    .prologue
    .line 463
    const/4 v2, -0x1

    .line 464
    .local v2, "connectedDeviceType":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "connected_wearable_id"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 465
    .local v0, "connectedDeviceAddress":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "connected_wearable"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 466
    .local v1, "connectedDeviceName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 467
    .local v3, "toReturn":I
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isActivityTrackerConnected(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/16 v4, 0x2727

    if-ne p1, v4, :cond_1

    .line 468
    const/16 v2, 0x2727

    .line 469
    move v3, v2

    .line 493
    :cond_0
    :goto_0
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setLastConnectedDeviceType(I)V

    .line 494
    return v3

    .line 471
    :cond_1
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_a

    .line 472
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connected wearabe name :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 474
    :cond_2
    const/16 v2, 0x2724

    .line 485
    :cond_3
    :goto_1
    if-eq p1, v2, :cond_4

    const/16 v4, 0x272f

    if-ne p1, v4, :cond_0

    .line 486
    :cond_4
    move v3, v2

    goto :goto_0

    .line 475
    :cond_5
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 476
    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceTypeForGearTizen(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 478
    :cond_6
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR 3"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR S"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 479
    :cond_7
    const/16 v2, 0x272e

    goto :goto_1

    .line 480
    :cond_8
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR O"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 481
    const/16 v2, 0x2730

    goto :goto_1

    .line 482
    :cond_9
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "WINGTIP"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 483
    const/16 v2, 0x2723

    goto :goto_1

    .line 489
    :cond_a
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v5, "connectedDeviceName is invalid"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method public static getConnectedGearDeviceType(Landroid/content/Context;)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 431
    const/4 v2, -0x1

    .line 434
    .local v2, "connectedDeviceType":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "connected_wearable_id"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 436
    .local v0, "connectedDeviceAddress":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "connected_wearable"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 438
    .local v1, "connectedDeviceName":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "wmanager_connected"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 440
    .local v3, "connectionStatus":Ljava/lang/String;
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7

    .line 441
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connected wearabe name :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 443
    :cond_0
    const/16 v2, 0x2724

    .line 457
    :cond_1
    :goto_0
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connectedDeviceType : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", connectedDeviceAddress : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", connectionStatus : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    return v2

    .line 444
    :cond_2
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 445
    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceTypeForGearTizen(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 447
    :cond_3
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR 3"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR S"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 448
    :cond_4
    const/16 v2, 0x272e

    goto :goto_0

    .line 449
    :cond_5
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR O"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 450
    const/16 v2, 0x2730

    goto :goto_0

    .line 451
    :cond_6
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "WINGTIP"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 452
    const/16 v2, 0x2723

    goto :goto_0

    .line 455
    :cond_7
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v5, "connectedDeviceName is invalid"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static getCustomeNameFromDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 890
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 891
    .local v9, "query":Ljava/lang/StringBuilder;
    const-string v0, "SELECT "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 892
    const-string v0, "custom_name"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 893
    const-string v0, " FROM "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 894
    const-string/jumbo v0, "user_device"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 895
    const-string v0, " WHERE "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 897
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090190

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 898
    const-string v0, "device_type = 10020"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 899
    const-string v0, " OR "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 900
    const-string v0, "device_type = 10024"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 910
    :cond_0
    :goto_0
    const-string v0, " ORDER BY update_time DESC LIMIT 1"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 912
    const/4 v6, 0x0

    .line 913
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 915
    .local v7, "customName":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 916
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 917
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 918
    const-string v0, "custom_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 923
    :cond_1
    if-eqz v6, :cond_2

    .line 924
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 928
    :cond_2
    :goto_1
    return-object v7

    .line 901
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "customName":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090bc3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 902
    const-string v0, "device_type = 10022"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 903
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090bc0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 904
    const-string v0, "device_type = 10030"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 905
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090bc1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 906
    const-string v0, "device_type = 10032"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 907
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090bc2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 908
    const-string v0, "device_type = 10019"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 920
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "customName":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 921
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 923
    if-eqz v6, :cond_2

    .line 924
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 923
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_7

    .line 924
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0
.end method

.method public static getDeletableItemList(Landroid/content/Context;Ljava/util/ArrayList;)Lcom/sec/android/app/shealth/walkingmate/utils/DeleteableData;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sec/android/app/shealth/walkingmate/utils/DeleteableData;"
        }
    .end annotation

    .prologue
    .line 695
    .local p1, "selectedTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v13, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v14, "getDeletableItemList"

    invoke-static {v13, v14}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 696
    new-instance v7, Lcom/sec/android/app/shealth/walkingmate/utils/DeleteableData;

    invoke-direct {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/DeleteableData;-><init>()V

    .line 697
    .local v7, "returnData":Lcom/sec/android/app/shealth/walkingmate/utils/DeleteableData;
    const-wide/16 v11, 0x0

    .line 698
    .local v11, "time":J
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v13, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v14

    invoke-direct {v6, v13, v14}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 699
    .local v6, "mDateFormatter":Ljava/text/DateFormat;
    const/4 v2, 0x0

    .line 701
    .local v2, "deleted_time":[J
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 702
    .local v1, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 703
    .local v8, "sb":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_3

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_3

    .line 704
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 705
    .local v9, "selectedTagSize":I
    if-lez v9, :cond_0

    .line 706
    new-array v2, v9, [J

    .line 709
    :cond_0
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v9, :cond_2

    .line 710
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    const-string v14, "_"

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    aget-object v10, v13, v14

    .line 713
    .local v10, "tag":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 714
    :try_start_0
    invoke-virtual {v6, v10}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v11

    .line 721
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v3

    .line 722
    .local v3, "deviceType":I
    aput-wide v11, v2, v5

    .line 723
    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 724
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "start_time >= "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 725
    const-string v13, " AND "

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 726
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "start_time <= "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 727
    const-string v13, " AND "

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 728
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 730
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 709
    .end local v3    # "deviceType":I
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 716
    :catch_0
    move-exception v4

    .line 717
    .local v4, "e":Ljava/text/ParseException;
    invoke-virtual {v4}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_1

    .line 733
    .end local v4    # "e":Ljava/text/ParseException;
    .end local v10    # "tag":Ljava/lang/String;
    :cond_2
    if-lez v9, :cond_3

    .line 734
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v13

    invoke-virtual {v13, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->deleteListbyTime([J)V

    .line 735
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->deleteExercise([J)V

    .line 739
    .end local v5    # "i":I
    .end local v9    # "selectedTagSize":I
    :cond_3
    iput-object v1, v7, Lcom/sec/android/app/shealth/walkingmate/utils/DeleteableData;->deleteableTagList:Ljava/util/ArrayList;

    .line 740
    iput-object v2, v7, Lcom/sec/android/app/shealth/walkingmate/utils/DeleteableData;->deleteableEntryTime:[J

    .line 741
    return-object v7
.end method

.method private static getDeviceTypeForGearTizen(Ljava/lang/String;Landroid/os/Parcelable;)I
    .locals 10
    .param p0, "deviceName"    # Ljava/lang/String;
    .param p1, "parcelableBtDevice"    # Landroid/os/Parcelable;

    .prologue
    .line 540
    move-object v0, p1

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 541
    .local v0, "btDevice":Landroid/bluetooth/BluetoothDevice;
    const/4 v4, -0x1

    .line 542
    .local v4, "wearableDeviceType":I
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "create_ShealthSensorDevice2 btDevice name : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 544
    :cond_0
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v7, "deviceName is null."

    invoke-static {v6, v7}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 584
    .end local v4    # "wearableDeviceType":I
    .local v5, "wearableDeviceType":I
    :goto_0
    return v5

    .line 548
    .end local v5    # "wearableDeviceType":I
    .restart local v4    # "wearableDeviceType":I
    :cond_1
    const-string v6, " "

    const-string v7, ""

    invoke-virtual {p0, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 550
    .local v2, "deviceTypeStr":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDeviceType deviceTypeStr: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "Gear 2"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    const-string v8, " "

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 553
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 554
    .local v1, "btDeviceName":Ljava/lang/String;
    const/16 v4, 0x2726

    .line 557
    if-eqz v1, :cond_2

    .line 559
    :try_start_0
    const-string v6, " "

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 560
    const/4 v6, 0x0

    const-string v7, "("

    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 561
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 562
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "btDeviceName = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    :goto_1
    const-string v6, "GEAR"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 576
    const/16 v4, 0x2728

    .line 577
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v7, "Device is Tizen Gear1"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .end local v1    # "btDeviceName":Ljava/lang/String;
    :cond_2
    :goto_2
    move v5, v4

    .line 584
    .end local v4    # "wearableDeviceType":I
    .restart local v5    # "wearableDeviceType":I
    goto/16 :goto_0

    .line 563
    .end local v5    # "wearableDeviceType":I
    .restart local v1    # "btDeviceName":Ljava/lang/String;
    .restart local v4    # "wearableDeviceType":I
    :catch_0
    move-exception v3

    .line 564
    .local v3, "ex":Ljava/lang/IndexOutOfBoundsException;
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v7, "Not Gear1 device!!"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    const-string v1, ""

    goto :goto_1

    .line 582
    .end local v1    # "btDeviceName":Ljava/lang/String;
    .end local v3    # "ex":Ljava/lang/IndexOutOfBoundsException;
    :cond_3
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v7, "This is not a Gear2/TizenGear1/Gear3"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static getGearDeviceNames(J)Ljava/lang/String;
    .locals 20
    .param p0, "time"    # J

    .prologue
    .line 816
    invoke-static/range {p0 .. p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v18

    .line 817
    .local v18, "startTimeOfDay":J
    invoke-static/range {p0 .. p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v15

    .line 819
    .local v15, "endTimeOfDay":J
    const-string v8, "DEVICE_TYPE"

    .line 820
    .local v8, "DEVICE_TYPE":Ljava/lang/String;
    const/4 v10, 0x1

    .line 821
    .local v10, "START_INDEX":I
    const/4 v9, 0x5

    .line 822
    .local v9, "DEVICE_TYPE_LENGTH":I
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 823
    .local v17, "query":Ljava/lang/StringBuilder;
    const-string v2, "SELECT "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 824
    const-string v2, "SUBSTR(user_device__id, 1, 5) AS DEVICE_TYPE"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 825
    const-string v2, " FROM "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 826
    const-string/jumbo v2, "walk_info"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 827
    const-string v2, " WHERE "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 828
    const-string/jumbo v2, "total_step > 0 "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 829
    const-string v2, " AND "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 830
    const-string/jumbo v2, "user_device__id NOT LIKE \'10009%\' "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    const-string v2, " AND "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 832
    const-string/jumbo v2, "user_device__id NOT LIKE \'10023%\' "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 833
    const-string v2, " AND "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 834
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time>="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 835
    const-string v2, " AND "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 836
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time<="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide v0, v15

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 837
    const-string v2, " GROUP BY DEVICE_TYPE"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 838
    const-string v2, " ORDER BY update_time DESC"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 840
    const/4 v11, 0x0

    .line 841
    .local v11, "cursor":Landroid/database/Cursor;
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12}, Ljava/lang/String;-><init>()V

    .line 844
    .local v12, "deviceNames":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 845
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_2

    .line 846
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 847
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    .line 849
    const-string v2, "DEVICE_TYPE"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 851
    .local v13, "device_type":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 852
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 854
    :cond_0
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 874
    :goto_1
    :pswitch_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 877
    .end local v13    # "device_type":Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 878
    .local v14, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 880
    if-eqz v11, :cond_1

    .line 881
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 885
    .end local v14    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    return-object v12

    .line 857
    .restart local v13    # "device_type":Ljava/lang/String;
    :pswitch_1
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090190

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 858
    goto :goto_1

    .line 860
    :pswitch_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 861
    goto :goto_1

    .line 863
    :pswitch_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 864
    goto :goto_1

    .line 866
    :pswitch_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 867
    goto/16 :goto_1

    .line 869
    :pswitch_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v12

    .line 870
    goto/16 :goto_1

    .line 880
    .end local v13    # "device_type":Ljava/lang/String;
    :cond_2
    if-eqz v11, :cond_1

    .line 881
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 880
    :catchall_0
    move-exception v2

    if-eqz v11, :cond_3

    .line 881
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2

    .line 854
    nop

    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private getGoalList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .local v0, "GoalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;>;"
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 261
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 262
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;

    const-string/jumbo v2, "value"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    const-string/jumbo v3, "set_time"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;-><init>(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;FJ)V

    .line 265
    .local v1, "goalData":Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "user_device_id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "user_device__id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " set_time : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;->getSetTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " value : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;->getValue()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 270
    .end local v1    # "goalData":Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;
    :cond_0
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    if-eqz p0, :cond_0

    .line 78
    sput-object p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mContext:Landroid/content/Context;

    .line 81
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mInstance:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    return-object v0
.end method

.method private static getSharedPrefKeyOfVersion3_2(I)Ljava/lang/String;
    .locals 1
    .param p0, "deviceType"    # I

    .prologue
    .line 633
    const/4 v0, 0x0

    .line 634
    .local v0, "sharedPrefKey":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 652
    :goto_0
    :pswitch_0
    return-object v0

    .line 637
    :pswitch_1
    const-string v0, "first_gear1_android_connected"

    .line 638
    goto :goto_0

    .line 640
    :pswitch_2
    const-string v0, "first_gear2_connected"

    .line 641
    goto :goto_0

    .line 643
    :pswitch_3
    const-string v0, "first_gear1_tizen_connected"

    .line 644
    goto :goto_0

    .line 646
    :pswitch_4
    const-string v0, "first_gear_fit_connected"

    .line 647
    goto :goto_0

    .line 649
    :pswitch_5
    const-string v0, "first_activity_tracker_connected"

    goto :goto_0

    .line 634
    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method

.method public static hasWearableManager(Landroid/content/Context;I)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deviceType"    # I

    .prologue
    .line 980
    const/4 v3, 0x0

    .line 981
    .local v3, "pkgName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 982
    .local v2, "isWearableMgrPresent":Z
    packed-switch p1, :pswitch_data_0

    .line 999
    :goto_0
    :pswitch_0
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "hasWearableManager pkgName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " deviceType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    if-eqz v3, :cond_0

    .line 1002
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 1004
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v4, v3, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1005
    .local v1, "info":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    .line 1006
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "hasWearableManager pkgName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1007
    const/4 v2, 0x1

    .line 1016
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    :goto_1
    return v2

    .line 989
    :pswitch_1
    const-string v3, "com.samsung.android.app.watchmanager"

    .line 990
    goto :goto_0

    .line 992
    :pswitch_2
    const-string v3, "com.samsung.android.wms"

    .line 993
    goto :goto_0

    .line 995
    :pswitch_3
    const-string v3, "com.samsung.android.app.atracker"

    goto :goto_0

    .line 1009
    .restart local v4    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v0

    .line 1011
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string v6, "hasWearableManager pkgName : pkg not found"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1012
    const/4 v2, 0x0

    .line 1013
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 982
    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static isDeviceConnected(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 330
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isActivityTrackerConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isGear2Connected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isGearFitConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isGear3Connected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isGear1Connected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isGearOConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isGearDeviceConnected(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 965
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedGearDeviceType(Landroid/content/Context;)I

    move-result v0

    .line 967
    .local v0, "connectedDeviceType":I
    const/16 v1, 0x2728

    if-eq v0, v1, :cond_0

    const/16 v1, 0x2724

    if-eq v0, v1, :cond_0

    const/16 v1, 0x2726

    if-eq v0, v1, :cond_0

    const/16 v1, 0x272e

    if-eq v0, v1, :cond_0

    const/16 v1, 0x2730

    if-eq v0, v1, :cond_0

    const/16 v1, 0x2723

    if-ne v0, v1, :cond_1

    .line 974
    :cond_0
    const/4 v1, 0x1

    .line 976
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isGearManagerAvailable(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 663
    const/4 v7, 0x3

    new-array v1, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "com.samsung.android.app.watchmanager"

    aput-object v8, v1, v7

    const-string v7, "com.samsung.android.wms"

    aput-object v7, v1, v6

    const/4 v7, 0x2

    const-string v8, "com.samsung.android.gear1plugin"

    aput-object v8, v1, v7

    .line 667
    .local v1, "gearProviders":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 669
    .local v4, "pgm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 671
    .local v0, "flag":Z
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    array-length v7, v1

    if-ge v2, v7, :cond_0

    .line 674
    :try_start_0
    aget-object v7, v1, v2

    const/4 v8, 0x1

    invoke-virtual {v4, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 675
    .local v3, "pginfo":Landroid/content/pm/PackageInfo;
    if-eqz v3, :cond_1

    move v0, v6

    .line 685
    .end local v0    # "flag":Z
    .end local v3    # "pginfo":Landroid/content/pm/PackageInfo;
    :cond_0
    return v0

    .line 679
    .restart local v0    # "flag":Z
    :catch_0
    move-exception v5

    .line 681
    .local v5, "pnnfe":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v0, 0x0

    .line 671
    .end local v5    # "pnnfe":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static isMobilePedometerDisabled(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 934
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSensorHubSupported()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isWearableDevicesConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSCSConnection(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 498
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "connected_wearable"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 499
    .local v0, "connectedDeviceName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 513
    :cond_0
    :goto_0
    return v4

    .line 502
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "wearable_connect_type"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 503
    .local v1, "connectionType":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 506
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 507
    .local v3, "upperCase":Ljava/lang/String;
    const-string v5, "GEAR 3"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "GEAR S"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 508
    :cond_2
    const-string v5, "#"

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 509
    .local v2, "lastToken":Ljava/lang/String;
    const-string v5, "2"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 510
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public static isSensorHubSupported()Z
    .locals 5

    .prologue
    .line 344
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 345
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    const-string v2, "com.sec.feature.sensorhub"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.sec.feature.scontext_lite"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 347
    .local v0, "isSupported":Z
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sensor hub supported: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    return v0

    .line 345
    .end local v0    # "isSupported":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportDurationAndHealthPace()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 746
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->checkFeature(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->checkFeature(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 751
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportDurationAndHealthPaceForGear()Z
    .locals 9

    .prologue
    .line 756
    const/4 v8, 0x0

    .line 757
    .local v8, "isExist":Z
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "COUNT(device_type) as DEVICE_COUNT"

    aput-object v1, v2, v0

    .line 760
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "device_type = 10030"

    .line 761
    .local v3, "selection":Ljava/lang/String;
    const/4 v7, 0x0

    .line 763
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 766
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 767
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 768
    const-string v0, "DEVICE_COUNT"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 769
    .local v6, "count":I
    if-eqz v6, :cond_0

    .line 770
    const/4 v8, 0x1

    .line 775
    .end local v6    # "count":I
    :cond_0
    if-eqz v7, :cond_1

    .line 776
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 780
    :cond_1
    return v8

    .line 775
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 776
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static isWearableDevicesConnected(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 941
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v0

    .line 942
    .local v0, "connectedDeviceType":I
    const/16 v3, 0x2728

    if-eq v0, v3, :cond_0

    const/16 v3, 0x2724

    if-eq v0, v3, :cond_0

    const/16 v3, 0x2726

    if-eq v0, v3, :cond_0

    const/16 v3, 0x272e

    if-eq v0, v3, :cond_0

    const/16 v3, 0x2730

    if-eq v0, v3, :cond_0

    const/16 v3, 0x2723

    if-eq v0, v3, :cond_0

    const/16 v3, 0x2727

    if-ne v0, v3, :cond_1

    .line 960
    :cond_0
    :goto_0
    return v2

    .line 953
    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 955
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getLoadUserDeviceName()Ljava/util/ArrayList;

    move-result-object v1

    .line 956
    .local v1, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;>;"
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_0

    .line 960
    .end local v1    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;>;"
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static updatePedoWidget()V
    .locals 9

    .prologue
    .line 356
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSensorHubSupported()Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v2, 0x1

    .line 357
    .local v2, "isAdvancedDevice":Z
    :goto_0
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isAdvancedDevice: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    :try_start_0
    new-instance v5, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 360
    .local v5, "widgetName":Landroid/content/ComponentName;
    invoke-static {v5, v2}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->enableWidgets(Landroid/content/ComponentName;Z)V

    .line 362
    new-instance v4, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;

    invoke-direct {v4, v6, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 363
    .local v4, "plainWidgetName":Landroid/content/ComponentName;
    invoke-static {v4, v2}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->enableWidgets(Landroid/content/ComponentName;Z)V

    .line 366
    new-instance v1, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;

    invoke-direct {v1, v6, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 367
    .local v1, "greedWidget":Landroid/content/ComponentName;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->enableWidgets(Landroid/content/ComponentName;Z)V

    .line 369
    new-instance v0, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    invoke-direct {v0, v6, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 370
    .local v0, "easyWidget":Landroid/content/ComponentName;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->enableWidgets(Landroid/content/ComponentName;Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 375
    .end local v0    # "easyWidget":Landroid/content/ComponentName;
    .end local v1    # "greedWidget":Landroid/content/ComponentName;
    .end local v4    # "plainWidgetName":Landroid/content/ComponentName;
    .end local v5    # "widgetName":Landroid/content/ComponentName;
    :goto_1
    return-void

    .line 356
    .end local v2    # "isAdvancedDevice":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 372
    .restart local v2    # "isAdvancedDevice":Z
    :catch_0
    move-exception v3

    .line 373
    .local v3, "npe":Ljava/lang/NullPointerException;
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public getBestRecordInfo()Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshBestRecordInfo()V

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mBestGoalInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    return-object v0
.end method

.method public getFirstDate()J
    .locals 4

    .prologue
    .line 164
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mFirstDate:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshFirstDate()V

    .line 168
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mFirstDate:J

    return-wide v0
.end method

.method public getLastDate()J
    .locals 4

    .prologue
    .line 172
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mLastDate:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshLastDate()V

    .line 176
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mLastDate:J

    return-wide v0
.end method

.method public isGoalAchived(JI)Z
    .locals 7
    .param p1, "date"    # J
    .param p3, "steps"    # I

    .prologue
    .line 306
    const/16 v3, 0x2710

    .line 307
    .local v3, "goalSteps":I
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    .line 309
    .local v0, "endOfDay":J
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mListGoal:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 310
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mListGoal:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;

    .line 311
    .local v2, "goal":Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;->getSetTime()J

    move-result-wide v5

    cmp-long v5, v5, v0

    if-gtz v5, :cond_0

    .line 312
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;->getValue()F

    move-result v5

    float-to-int v3, v5

    .line 318
    .end local v2    # "goal":Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    if-lt p3, v3, :cond_2

    .line 319
    const/4 v5, 0x1

    .line 321
    :goto_0
    return v5

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public refreshBestRecordInfo()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 181
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "refreshBestRecordInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    invoke-direct {v6}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;-><init>()V

    .line 184
    .local v6, "bestGoalInfo":Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;
    # setter for: Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mTime:J
    invoke-static {v6, v10, v11}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->access$002(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;J)J

    .line 185
    const/4 v0, 0x0

    # setter for: Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mSteps:I
    invoke-static {v6, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->access$102(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;I)I

    .line 187
    const/4 v7, 0x0

    .line 190
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v8

    .line 191
    .local v8, "deviceType":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    .local v9, "query":Ljava/lang/StringBuilder;
    const-string v0, "SELECT SUM(total_step) AS total_step"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    const-string v0, " , "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-string/jumbo v0, "start_time"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    const-string v0, " FROM walk_info"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    const-string v0, " WHERE "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    const-string v0, " AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    const-string/jumbo v0, "sync_status != 170004"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    const-string v0, " GROUP BY strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\')"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    const-string v0, " ORDER BY total_step DESC LIMIT 1"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 206
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 207
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 208
    const-string/jumbo v0, "total_step"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    # setter for: Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mSteps:I
    invoke-static {v6, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->access$102(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;I)I

    .line 209
    const-string/jumbo v0, "start_time"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    # setter for: Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mTime:J
    invoke-static {v6, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->access$002(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    :cond_0
    if-eqz v7, :cond_1

    .line 213
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 217
    :cond_1
    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mSteps:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->access$100(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;)I

    move-result v0

    const/16 v1, 0x2710

    if-ge v0, v1, :cond_2

    .line 218
    # setter for: Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mTime:J
    invoke-static {v6, v10, v11}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->access$002(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;J)J

    .line 219
    const/16 v0, 0x270f

    # setter for: Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mSteps:I
    invoke-static {v6, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->access$102(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;I)I

    .line 220
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "refreshBestRecordInfo() - Best steps is 9999, because best steps in DB is less than 10000"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :cond_2
    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mBestGoalInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    .line 224
    return-void

    .line 212
    .end local v8    # "deviceType":I
    .end local v9    # "query":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    .line 213
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public refreshFirstDate()V
    .locals 10

    .prologue
    .line 102
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "refreshFirstDate"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v6, 0x0

    .line 105
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v7

    .line 106
    .local v7, "deviceType":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .local v9, "query":Ljava/lang/StringBuilder;
    const-string v0, "SELECT start_time"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    const-string v0, " FROM walk_info"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    const-string v0, " WHERE "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    const-string v0, " AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string/jumbo v0, "total_step > 0"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const-string v0, " AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string/jumbo v0, "sync_status != 170004"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string v0, " ORDER BY start_time ASC LIMIT 1"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 118
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 120
    const-string/jumbo v0, "start_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mFirstDate:J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :goto_0
    if-eqz v6, :cond_0

    .line 128
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 131
    .end local v7    # "deviceType":I
    .end local v9    # "query":Ljava/lang/StringBuilder;
    :cond_0
    :goto_1
    return-void

    .line 122
    .restart local v7    # "deviceType":I
    .restart local v9    # "query":Ljava/lang/StringBuilder;
    :cond_1
    const-wide/16 v0, 0x0

    :try_start_1
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mFirstDate:J
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 124
    .end local v7    # "deviceType":I
    .end local v9    # "query":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v8

    .line 125
    .local v8, "e":Ljava/lang/RuntimeException;
    const-wide/16 v0, 0x0

    :try_start_2
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mFirstDate:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 127
    if-eqz v6, :cond_0

    .line 128
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 127
    .end local v8    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 128
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public refreshGoalInfo()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 274
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "refreshGoalInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    const/4 v6, 0x0

    .line 277
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "value"

    aput-object v0, v2, v3

    const/4 v0, 0x1

    const-string/jumbo v1, "set_time"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "user_device__id"

    aput-object v1, v2, v0

    .line 280
    .local v2, "projection":[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v7

    .line 281
    .local v7, "deviceType":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 283
    .local v8, "selection":Ljava/lang/StringBuilder;
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 284
    const-string v0, "goal_type = 40001"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    const-string v0, " AND "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    const-string/jumbo v5, "set_time DESC"

    .line 291
    .local v5, "orderby":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 294
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 296
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getGoalList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mListGoal:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    :cond_0
    if-eqz v6, :cond_1

    .line 300
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 303
    :cond_1
    return-void

    .line 299
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 300
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public refreshLastDate()V
    .locals 10

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "refreshLastDate"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const/4 v6, 0x0

    .line 137
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v7

    .line 138
    .local v7, "deviceType":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    .local v9, "query":Ljava/lang/StringBuilder;
    const-string v0, "SELECT start_time"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    const-string v0, " FROM walk_info"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const-string v0, " WHERE "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    const-string v0, " AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    const-string/jumbo v0, "total_step > 0"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string v0, " ORDER BY start_time DESC LIMIT 1"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 148
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 150
    const-string/jumbo v0, "start_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mLastDate:J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    :goto_0
    if-eqz v6, :cond_0

    .line 158
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 161
    .end local v7    # "deviceType":I
    .end local v9    # "query":Ljava/lang/StringBuilder;
    :cond_0
    :goto_1
    return-void

    .line 152
    .restart local v7    # "deviceType":I
    .restart local v9    # "query":Ljava/lang/StringBuilder;
    :cond_1
    const-wide/16 v0, 0x0

    :try_start_1
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mLastDate:J
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 154
    .end local v7    # "deviceType":I
    .end local v9    # "query":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v8

    .line 155
    .local v8, "e":Ljava/lang/RuntimeException;
    const-wide/16 v0, 0x0

    :try_start_2
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->mLastDate:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 157
    if-eqz v6, :cond_0

    .line 158
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 157
    .end local v8    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 158
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method
