.class public Lcom/sec/android/app/shealth/common/utils/SupportUtils;
.super Ljava/lang/Object;
.source "SupportUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/utils/SupportUtils$1;
    }
.end annotation


# static fields
.field public static final TEST_UVI_ADDRESS:Ljava/lang/String; = "samstresstest@gmail.com"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method

.method public static enableWidgets(Landroid/content/ComponentName;Z)V
    .locals 4
    .param p0, "widgetName"    # Landroid/content/ComponentName;
    .param p1, "shouldBeEnabled"    # Z

    .prologue
    const/4 v0, 0x1

    .line 64
    if-eqz p1, :cond_0

    .line 68
    .local v0, "componentFutureState":I
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, p0, v0, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :goto_1
    return-void

    .line 64
    .end local v0    # "componentFutureState":I
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 70
    .restart local v0    # "componentFutureState":I
    :catch_0
    move-exception v1

    .line 71
    .local v1, "iae":Ljava/lang/IllegalArgumentException;
    const-string v2, "SupportUtils"

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isSamsungAccount(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v2, "eng"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    :goto_0
    return v0

    .line 45
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/SupportUtils$1;->$SwitchMap$com$sec$android$app$shealth$common$config$FeatureTable$FeatureType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 55
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 47
    :pswitch_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static isSamsungAccount(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    const/4 v5, 0x0

    .line 23
    .local v5, "isSamAccount":Z
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 24
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 25
    .local v1, "accounts":[Landroid/accounts/Account;
    sget-object v3, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    .line 27
    .local v3, "emailPattern":Ljava/util/regex/Pattern;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v6, v2

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v0, v2, v4

    .line 28
    .local v0, "account":Landroid/accounts/Account;
    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 29
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 30
    .local v7, "possibleEmail":Ljava/lang/String;
    const-string/jumbo v8, "samstresstest@gmail.com"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 31
    const/4 v5, 0x1

    .line 27
    .end local v7    # "possibleEmail":Ljava/lang/String;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 36
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accounts":[Landroid/accounts/Account;
    .end local v2    # "arr$":[Landroid/accounts/Account;
    .end local v3    # "emailPattern":Ljava/util/regex/Pattern;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    :cond_1
    return v5
.end method
