.class public Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryUtils;
.super Ljava/lang/Object;
.source "CignaLibraryUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getRandomNumber(II)I
    .locals 4
    .param p0, "start"    # I
    .param p1, "end"    # I

    .prologue
    .line 19
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    sub-int v2, p1, p0

    add-int/lit8 v2, v2, 0x1

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    add-int/2addr v0, p0

    return v0
.end method
