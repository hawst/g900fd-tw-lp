.class public Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;
.super Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;
.source "UvGraphFragmentSIC.java"


# static fields
.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final DAY_IN_MONTH:B = 0x1ft

.field public static final HOURS_IN_DAY:B = 0x18t

.field private static MARKING_COUNT:I = 0x0

.field private static MAX_INDEX:F = 0.0f

.field private static MAX_Y:F = 0.0f

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_MONTH:J = 0x9fa52400L

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field private static final MIN_Y:F = 0.0f

.field private static final PATTERN_24_HANDLER:Ljava/lang/String; = "HH:mm"

.field private static final PATTERN_24_HOURS:Ljava/lang/String; = "HH"

.field private static final PATTERN_DAY_FORMAT:Ljava/lang/String; = "dd"

.field private static final PATTERN_MONTH_FORMAT:Ljava/lang/String; = "MM"

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field private static volatile mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;


# instance fields
.field private chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

.field private density:F

.field private isConfigurationChanged:Z

.field private mDataCount:I

.field private mDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

.field private mDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation
.end field

.field private mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

.field private mInfoView:Lcom/sec/android/app/shealth/uv/UvInformationAreaView;

.field private mLegendView:Landroid/view/View;

.field private mObserver:Landroid/database/ContentObserver;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private switchToBodyfatSummary:Landroid/widget/ImageButton;

.field private timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const/16 v0, 0xc

    sput v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->MARKING_COUNT:I

    .line 64
    const v0, 0x4131999a    # 11.1f

    sput v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->MAX_Y:F

    .line 65
    const/high16 v0, 0x41300000    # 11.0f

    sput v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->MAX_INDEX:F

    .line 86
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 88
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$1;-><init>(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mObserver:Landroid/database/ContentObserver;

    .line 693
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;-><init>(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->initGeneralView()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->showSummarayFragment()V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mReadyToShown:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/sec/android/app/shealth/uv/UvInformationAreaView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/uv/UvInformationAreaView;

    return-object v0
.end method

.method private initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 23
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    .line 451
    new-instance v15, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v15}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 452
    .local v15, "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v18, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 453
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 454
    const/16 v18, 0xff

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v15, v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 455
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 456
    const-string v18, "font/Roboto-Light.ttf"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 458
    new-instance v17, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 459
    .local v17, "yTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v18, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 460
    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 461
    const/16 v18, 0xff

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-virtual/range {v17 .. v21}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 462
    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 463
    const-string v18, "font/Roboto-Light.ttf"

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 465
    new-instance v16, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 466
    .local v16, "yLabelTitleStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a0050

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 467
    const/16 v18, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 468
    const/16 v18, 0xff

    const/16 v19, 0xac

    const/16 v20, 0xac

    const/16 v21, 0xac

    move-object/from16 v0, v16

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 469
    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 470
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x106000c

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getColor(I)I

    move-result v18

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 471
    const-string v18, "font/Roboto-Light.ttf"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 474
    new-instance v5, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 475
    .local v5, "backgroundTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v18, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 476
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 477
    const/16 v18, 0xff

    const/16 v19, 0xac

    const/16 v20, 0xac

    const/16 v21, 0xac

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v5, v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 478
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 479
    const-string v18, "font/Roboto-Light.ttf"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 481
    new-instance v10, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v10}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 482
    .local v10, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v18, 0x41900000    # 18.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 483
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 484
    const/16 v18, 0xff

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v10, v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 485
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 487
    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/high16 v21, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 488
    const v18, -0xa0a0b

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setChartBackgroundColor(I)V

    .line 491
    const/high16 v18, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisWidth(FI)V

    .line 492
    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 493
    const v18, -0xcbb1ec

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisColor(II)V

    .line 494
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v15, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 495
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v18, v0

    const/high16 v19, 0x41000000    # 8.0f

    mul-float v18, v18, v19

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextSpace(FI)V

    .line 498
    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 499
    const/16 v18, 0x1

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 500
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 501
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v18, v0

    const/high16 v19, 0x41000000    # 8.0f

    mul-float v18, v18, v19

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextSpace(FI)V

    .line 502
    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 515
    const/16 v18, 0x1

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 526
    const/16 v18, 0x3c

    move/from16 v0, v18

    new-array v12, v0, [Ljava/lang/String;

    .line 528
    .local v12, "labels":[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v18

    if-nez v18, :cond_5

    .line 530
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    const/16 v18, 0x9

    move/from16 v0, v18

    if-ge v11, v0, :cond_0

    .line 531
    const-string v18, ""

    aput-object v18, v12, v11

    .line 530
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 532
    :cond_0
    const/16 v18, 0x9

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f090d94

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v12, v18

    .line 534
    const/16 v11, 0xa

    :goto_1
    const/16 v18, 0x18

    move/from16 v0, v18

    if-ge v11, v0, :cond_1

    .line 535
    const-string v18, ""

    aput-object v18, v12, v11

    .line 534
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 536
    :cond_1
    const/16 v18, 0x18

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f090d95

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v12, v18

    .line 538
    const/16 v11, 0x19

    :goto_2
    const/16 v18, 0x24

    move/from16 v0, v18

    if-ge v11, v0, :cond_2

    .line 539
    const-string v18, ""

    aput-object v18, v12, v11

    .line 538
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 540
    :cond_2
    const/16 v18, 0x24

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f090d96

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v12, v18

    .line 542
    const/16 v11, 0x25

    :goto_3
    const/16 v18, 0x30

    move/from16 v0, v18

    if-ge v11, v0, :cond_3

    .line 543
    const-string v18, ""

    aput-object v18, v12, v11

    .line 542
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 544
    :cond_3
    const/16 v18, 0x30

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f090d97

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v12, v18

    .line 546
    const/16 v11, 0x31

    :goto_4
    const/16 v18, 0x3a

    move/from16 v0, v18

    if-ge v11, v0, :cond_4

    .line 547
    const-string v18, ""

    aput-object v18, v12, v11

    .line 546
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 548
    :cond_4
    const/16 v18, 0x3a

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f090d98

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v12, v18

    .line 574
    :goto_5
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v12, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisCustomLabel([Ljava/lang/String;I)V

    .line 628
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v18

    if-nez v18, :cond_b

    const v18, 0x7f0203b3

    :goto_6
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 631
    .local v7, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    .end local v7    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 632
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 635
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v18, v0

    sget-object v19, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_c

    .line 636
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0203c3

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 637
    .restart local v7    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    const/high16 v18, 0x426c0000    # 59.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 638
    const-string v18, "HH:mm"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 639
    const-string v18, "HH"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 646
    :goto_7
    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    .end local v7    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 647
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 663
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemStrokeWidth(F)V

    .line 664
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 665
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 666
    const/high16 v18, 0x427c0000    # 63.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 667
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a0057

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x3f000000    # 0.5f

    mul-float v18, v18, v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0a0058

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    add-float v18, v18, v19

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 668
    const/high16 v18, 0x41a00000    # 20.0f

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 669
    const-wide/16 v18, 0x1388

    move-object/from16 v0, p1

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTimeOutDelay(J)V

    .line 670
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerAnimation(Z)V

    .line 671
    const-wide/16 v18, 0x3e8

    move-object/from16 v0, p1

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerAnimationTime(J)V

    .line 675
    const/16 v18, 0x12c

    const/16 v19, 0x12c

    sget-object v20, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v18 .. v20}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 676
    .local v13, "lineBitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0203c4

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 677
    .local v9, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, v13}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 678
    .local v8, "canvas":Landroid/graphics/Canvas;
    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-virtual {v8}, Landroid/graphics/Canvas;->getWidth()I

    move-result v20

    invoke-virtual {v8}, Landroid/graphics/Canvas;->getHeight()I

    move-result v21

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 679
    invoke-virtual {v9, v8}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 680
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 682
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupEnable(Z)V

    .line 683
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setMarkingLineBase(I)V

    .line 684
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendVisible(Z)V

    .line 685
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v18, v0

    const/high16 v19, 0x42080000    # 34.0f

    mul-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 686
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipEnable(Z)V

    .line 688
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v14

    .line 689
    .local v14, "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/16 v18, 0x50

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAlpha(I)V

    .line 690
    const/high16 v18, 0x41400000    # 12.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 691
    return-void

    .line 551
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    .end local v8    # "canvas":Landroid/graphics/Canvas;
    .end local v9    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v11    # "i":I
    .end local v13    # "lineBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :cond_5
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_8
    const/16 v18, 0x9

    move/from16 v0, v18

    if-ge v11, v0, :cond_6

    .line 552
    const-string v18, ""

    aput-object v18, v12, v11

    .line 551
    add-int/lit8 v11, v11, 0x1

    goto :goto_8

    .line 553
    :cond_6
    const/16 v18, 0x9

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f091158

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v12, v18

    .line 555
    const/16 v11, 0xa

    :goto_9
    const/16 v18, 0x16

    move/from16 v0, v18

    if-ge v11, v0, :cond_7

    .line 556
    const-string v18, ""

    aput-object v18, v12, v11

    .line 555
    add-int/lit8 v11, v11, 0x1

    goto :goto_9

    .line 557
    :cond_7
    const/16 v18, 0x16

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f091159

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v12, v18

    .line 559
    const/16 v11, 0x17

    :goto_a
    const/16 v18, 0x1f

    move/from16 v0, v18

    if-ge v11, v0, :cond_8

    .line 560
    const-string v18, ""

    aput-object v18, v12, v11

    .line 559
    add-int/lit8 v11, v11, 0x1

    goto :goto_a

    .line 561
    :cond_8
    const/16 v18, 0x1f

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f09115a

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v12, v18

    .line 563
    const/16 v11, 0x20

    :goto_b
    const/16 v18, 0x2c

    move/from16 v0, v18

    if-ge v11, v0, :cond_9

    .line 564
    const-string v18, ""

    aput-object v18, v12, v11

    .line 563
    add-int/lit8 v11, v11, 0x1

    goto :goto_b

    .line 565
    :cond_9
    const/16 v18, 0x2c

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f09115b

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v12, v18

    .line 567
    const/16 v11, 0x2d

    :goto_c
    const/16 v18, 0x38

    move/from16 v0, v18

    if-ge v11, v0, :cond_a

    .line 568
    const-string v18, ""

    aput-object v18, v12, v11

    .line 567
    add-int/lit8 v11, v11, 0x1

    goto :goto_c

    .line 569
    :cond_a
    const/16 v18, 0x38

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f09115c

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v12, v18

    goto/16 :goto_5

    .line 628
    :cond_b
    const v18, 0x7f0203b4

    goto/16 :goto_6

    .line 641
    .restart local v6    # "bitmap":Landroid/graphics/Bitmap;
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0203c2

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 642
    .restart local v7    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    const/high16 v18, 0x41f80000    # 31.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 643
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getTimeDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    goto/16 :goto_7
.end method

.method private initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 13
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const v12, 0x106000c

    const/high16 v11, 0x40a00000    # 5.0f

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 410
    new-instance v3, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 411
    .local v3, "lineTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0062

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 412
    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 414
    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 415
    const-string v7, "font/Roboto-Light.ttf"

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 417
    new-instance v2, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 418
    .local v2, "lineLabelStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0063

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 419
    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 420
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 421
    invoke-virtual {v2, v10}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 422
    const-string v7, "font/Roboto-Light.ttf"

    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 424
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0203c0

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 425
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 427
    .local v4, "normalBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0203c1

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 428
    .restart local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 430
    .local v1, "handlerOverBitmap":Landroid/graphics/Bitmap;
    new-instance v6, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v6}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 432
    .local v6, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    const/4 v7, 0x0

    sget v8, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->MAX_Y:F

    invoke-virtual {v6, v9, v9, v7, v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 433
    invoke-virtual {v6, v3}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 434
    invoke-virtual {v6, v2}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextPostfixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 435
    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingVisible(Z)V

    .line 436
    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 437
    invoke-virtual {v6, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v5, v7, Landroid/util/DisplayMetrics;->density:F

    .line 439
    .local v5, "scale":F
    mul-float v7, v11, v5

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingSize(F)V

    .line 441
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070185

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 443
    invoke-virtual {v6, v11}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineThickness(F)V

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f07008b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalOverColor(I)V

    .line 445
    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setEnableDisconnectByTime(Z)V

    .line 446
    invoke-virtual {p1, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 447
    return-void
.end method

.method private initTimeLineChartView(Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V
    .locals 7
    .param p1, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    .prologue
    const/4 v3, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 275
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    if-eqz v2, :cond_0

    .line 276
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v1

    .line 277
    .local v1, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 278
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 279
    iput-object v3, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    .line 282
    .end local v1    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-direct {v2, v3, v4, p1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    .line 284
    new-instance v0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;-><init>()V

    .line 285
    .local v0, "interaction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setScaleZoomInteractionEnabled(Z)V

    .line 286
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInteractionEnabled(Z)V

    .line 287
    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setPatialZoomInteractionEnabled(Z)V

    .line 289
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_1

    .line 290
    const/high16 v2, 0x40e00000    # 7.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 291
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 292
    invoke-virtual {v0, v5, v5}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalLimit(II)V

    .line 293
    invoke-virtual {v0, v5, v5}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalStep(II)V

    .line 299
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 300
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    new-instance v3, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$3;-><init>(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setReadyToShowListener(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;)V

    .line 309
    return-void

    .line 295
    :cond_1
    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 296
    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    goto :goto_0
.end method

.method private isDataCountChanged()Z
    .locals 2

    .prologue
    .line 152
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getDataCount()I

    move-result v0

    .line 153
    .local v0, "uvDataCount":I
    iget v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDataCount:I

    if-ne v0, v1, :cond_0

    .line 154
    const/4 v1, 0x0

    .line 157
    :goto_0
    return v1

    .line 156
    :cond_0
    iput v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDataCount:I

    .line 157
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 3
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 203
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_1

    .line 205
    const-string v1, "UV_CHARTTAB"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 217
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 219
    return-void

    .line 207
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_2

    .line 209
    const-string v1, "UV_CHARTTAB"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 211
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_0

    .line 213
    const-string v1, "UV_CHARTTAB"

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method private setChartData(Lcom/samsung/android/sdk/chart/series/SchartDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 9
    .param p1, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 378
    new-instance v5, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 379
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    invoke-virtual {v5, p2}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getGraphDatasByType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/ArrayList;

    move-result-object v4

    .line 385
    .local v4, "uvDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/uv/data/UvData;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/32 v7, 0x413b380

    sub-long v2, v5, v7

    .line 387
    .local v2, "testTime":J
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    if-nez v5, :cond_0

    .line 388
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    .line 393
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 394
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 395
    .local v0, "cdata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->setTime(J)V

    .line 397
    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v5, :cond_1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/data/UvData;->getScore()D

    move-result-wide v5

    :goto_2
    sget v7, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->MAX_INDEX:F

    float-to-double v7, v7

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v8, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v5, v8, :cond_2

    const/4 v5, 0x1

    :goto_3
    invoke-virtual {v0, v6, v7, v5}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->setValue(DZ)V

    .line 398
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 390
    .end local v0    # "cdata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v1    # "i":I
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 397
    .restart local v0    # "cdata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/data/UvData;->getMax()D

    move-result-wide v5

    goto :goto_2

    :cond_2
    const/4 v5, 0x0

    goto :goto_3

    .line 401
    .end local v0    # "cdata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->setAllData(Ljava/util/List;)V

    .line 402
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {p1, v5}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 403
    return-void
.end method

.method private setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 2
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getSeparatorDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorDateFormat(Ljava/lang/String;)V

    .line 313
    return-void
.end method

.method private setStartVisual(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 14
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const-wide/16 v12, 0x0

    .line 316
    const/4 v1, 0x0

    .line 317
    .local v1, "level":I
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_3

    .line 318
    const/4 v1, 0x0

    .line 324
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v0

    if-lez v0, :cond_d

    .line 329
    const-wide/16 v6, 0x0

    .line 330
    .local v6, "selectedTime":J
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_5

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v10, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v6

    .line 335
    :goto_1
    const-wide/16 v8, 0x0

    .line 336
    .local v8, "startTimes":J
    const-wide/16 v2, 0x0

    .line 338
    .local v2, "startTime":D
    sget v5, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->MARKING_COUNT:I

    .line 339
    .local v5, "markingCount":I
    const/4 v4, 0x1

    .line 341
    .local v4, "intervel":I
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_1

    .line 342
    sput-object p1, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 345
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_8

    .line 347
    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v10

    sget-object v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v11, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v11, :cond_7

    const-string v0, "%Y-%m-%d"

    :goto_2
    invoke-virtual {v10, v0, v6, v7}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v8

    .line 348
    cmp-long v0, v8, v12

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v10, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v8

    .line 349
    :cond_2
    const-wide/32 v10, 0x1ee6280

    sub-long v10, v8, v10

    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getStartHourToMillis(J)J

    move-result-wide v10

    long-to-double v2, v10

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setScrollRangeDepthLevel(I)V

    .line 351
    const/16 v4, 0x3c

    .line 352
    const/16 v5, 0xd

    .line 366
    :goto_3
    sput-object p1, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setStartVisual(IDII)V

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    long-to-double v10, v8

    invoke-virtual {v0, v10, v11}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setHandlerStartDate(D)V

    .line 373
    .end local v2    # "startTime":D
    .end local v4    # "intervel":I
    .end local v5    # "markingCount":I
    .end local v6    # "selectedTime":J
    .end local v8    # "startTimes":J
    :goto_4
    return-void

    .line 319
    :cond_3
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_4

    .line 320
    const/4 v1, 0x2

    goto/16 :goto_0

    .line 321
    :cond_4
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_0

    .line 322
    const/4 v1, 0x5

    goto/16 :goto_0

    .line 333
    .restart local v6    # "selectedTime":J
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/uv/UvInformationAreaView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/uv/UvInformationAreaView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->getSelectedDateInChart()J

    move-result-wide v6

    :goto_5
    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v10, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v6

    goto :goto_5

    .line 347
    .restart local v2    # "startTime":D
    .restart local v4    # "intervel":I
    .restart local v5    # "markingCount":I
    .restart local v8    # "startTimes":J
    :cond_7
    const-string v0, "%Y-%m"

    goto :goto_2

    .line 354
    :cond_8
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_b

    .line 355
    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v10

    sget-object v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v11, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v11, :cond_a

    const-string v0, "%Y-%m-%d"

    :goto_6
    invoke-virtual {v10, v0, v6, v7}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v8

    .line 356
    cmp-long v0, v8, v12

    if-gtz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v10, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v8

    .line 357
    :cond_9
    const-wide/32 v10, 0x19bfcc00

    sub-long v10, v8, v10

    long-to-double v2, v10

    .line 358
    const/4 v5, 0x7

    goto/16 :goto_3

    .line 355
    :cond_a
    const-string v0, "%Y-%m"

    goto :goto_6

    .line 361
    :cond_b
    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v0

    const-string v10, "%Y-%m"

    invoke-virtual {v0, v10, v6, v7}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v8

    .line 362
    cmp-long v0, v8, v12

    if-gtz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v10, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v8

    .line 363
    :cond_c
    const-wide v10, 0x59cce4400L

    sub-long v10, v8, v10

    long-to-double v2, v10

    .line 364
    const/16 v5, 0xc

    goto/16 :goto_3

    .line 371
    .end local v2    # "startTime":D
    .end local v4    # "intervel":I
    .end local v5    # "markingCount":I
    .end local v6    # "selectedTime":J
    .end local v8    # "startTimes":J
    :cond_d
    const v0, 0x7f0205b7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->showNoData(I)V

    goto/16 :goto_4
.end method

.method private showSummarayFragment()V
    .locals 5

    .prologue
    .line 171
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/uv/UvActivity;

    .line 172
    .local v0, "activity":Lcom/sec/android/app/shealth/uv/UvActivity;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->switchFragmentToSummary()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    return-void

    .line 173
    .end local v0    # "activity":Lcom/sec/android/app/shealth/uv/UvActivity;
    :catch_0
    move-exception v1

    .line 174
    .local v1, "e":Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must be instance of UvActivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method protected getSeparatorDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;
    .locals 7
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 751
    const-string v2, "-"

    .line 752
    .local v2, "REGEX_ONE_OR_MORE":Ljava/lang/String;
    const-string v3, "/"

    .line 753
    .local v3, "SLASH":Ljava/lang/String;
    const-string v0, "d"

    .line 754
    .local v0, "DAY_CHAR":Ljava/lang/String;
    const-string v1, "M"

    .line 757
    .local v1, "MONTH_CHAR":Ljava/lang/String;
    const-string v4, ""

    .line 758
    .local v4, "dateFormat":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "date_format"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 759
    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v5, :cond_0

    .line 760
    const-string v5, "d"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 761
    const-string v5, "M"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 762
    const-string v5, "--"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 772
    :goto_0
    return-object v4

    .line 763
    :cond_0
    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v5, :cond_1

    .line 764
    const-string v5, "dd-"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 765
    const-string v5, "-dd"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 766
    const-string v5, "-"

    const-string v6, "/"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 770
    :cond_1
    const-string v5, "-"

    const-string v6, "/"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method protected getTimeDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;
    .locals 3
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 257
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_0

    .line 258
    const-string v0, "dd"

    .line 262
    :goto_0
    return-object v0

    .line 259
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_1

    .line 260
    const-string v0, "MM"

    goto :goto_0

    .line 261
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_2

    .line 262
    const-string v0, "HH"

    goto :goto_0

    .line 264
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal periodType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020b

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 128
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initDateBar()V

    .line 129
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090211

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 130
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090213

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 131
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090212

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 132
    return-void
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 3

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 233
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03026c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mLegendView:Landroid/view/View;

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v2, 0x7f080b05

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->switchToBodyfatSummary:Landroid/widget/ImageButton;

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->switchToBodyfatSummary:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$2;-><init>(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mLegendView:Landroid/view/View;

    return-object v1
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 3
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v2, 0x1

    .line 180
    iput-object p3, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 181
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 182
    new-instance v1, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-direct {v1, v2, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;-><init>(II)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .line 183
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;-><init>()V

    .line 186
    .local v0, "dataSet":Lcom/samsung/android/sdk/chart/series/SchartDataSet;
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 188
    invoke-direct {p0, v0, p3}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->setChartData(Lcom/samsung/android/sdk/chart/series/SchartDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 194
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->initTimeLineChartView(Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V

    .line 195
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->setStartVisual(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    return-object v1
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 2
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 224
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/uv/UvInformationAreaView;

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/uv/UvInformationAreaView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->setDateFormat()V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/uv/UvInformationAreaView;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 163
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->onConfiguarationLanguageChanged()V

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->switchToBodyfatSummary:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->isConfigurationChanged:Z

    .line 167
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 107
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onCreate(Landroid/os/Bundle;)V

    .line 108
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->setHasOptionsMenu(Z)V

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getDataCount()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mDataCount:I

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "UV_CHARTTAB"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 114
    .local v0, "mPeriod":I
    if-ne v0, v4, :cond_0

    .line 116
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 123
    :goto_0
    return-void

    .line 118
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 120
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0

    .line 122
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 147
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onDestroyView()V

    .line 148
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 136
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onResume()V

    .line 137
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->isConfigurationChanged:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->isDataCountChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->initGeneralView()V

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->isConfigurationChanged:Z

    .line 142
    :cond_1
    return-void
.end method

.method protected setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 253
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 254
    return-void
.end method
