.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$2;
.super Ljava/lang/Object;
.source "FoodTrackerBaseFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;)V
    .locals 0

    .prologue
    .line 1044
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$2;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 1

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$2;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1049
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$2;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mListPopupShown:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1600(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1051
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$2;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->showMealTypeSelectorPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->access$1500(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;)V

    .line 1053
    :cond_0
    return-void
.end method
