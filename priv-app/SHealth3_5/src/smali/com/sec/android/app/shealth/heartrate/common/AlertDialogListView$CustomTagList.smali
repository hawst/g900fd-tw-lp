.class Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;
.super Landroid/widget/ArrayAdapter;
.source "AlertDialogListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CustomTagList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/app/Activity;

.field private mPref:Landroid/content/SharedPreferences;

.field private final selectedTag:Ljava/lang/String;

.field private final tags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .param p2, "context"    # Landroid/app/Activity;
    .param p4, "mSelectedTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 171
    .local p3, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    .line 172
    const v0, 0x7f030161

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 173
    iput-object p2, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->context:Landroid/app/Activity;

    .line 174
    iput-object p3, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->tags:Ljava/util/ArrayList;

    .line 175
    iput-object p4, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->selectedTag:Ljava/lang/String;

    .line 176
    iget-object v0, p1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->mPref:Landroid/content/SharedPreferences;

    .line 177
    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->mPref:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->tags:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public getTagList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->tags:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 180
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->context:Landroid/app/Activity;

    invoke-virtual {v10}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 181
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v10, 0x7f030160

    const/4 v11, 0x0

    invoke-virtual {v3, v10, p3, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 182
    .local v7, "rowView":Landroid/view/View;
    const v10, 0x7f0805de

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 183
    .local v9, "txtTitle":Landroid/widget/TextView;
    const v10, 0x7f0805f9

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 184
    .local v2, "imageView":Landroid/widget/ImageView;
    const v10, 0x7f0805f6

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioButton;

    .line 187
    .local v5, "mButton":Landroid/widget/RadioButton;
    :try_start_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v10

    const-string v11, "com.sec.android.app.shealth"

    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/ResourceUtil;

    move-result-object v4

    .line 188
    .local v4, "instance":Lcom/sec/android/app/shealth/common/utils/ResourceUtil;
    const-string/jumbo v10, "tw_btn_radio_selector"

    invoke-virtual {v4, v10}, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 189
    .local v0, "btnDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v5, v0}, Landroid/widget/RadioButton;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    .end local v0    # "btnDrawable":Landroid/graphics/drawable/Drawable;
    .end local v4    # "instance":Lcom/sec/android/app/shealth/common/utils/ResourceUtil;
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->tags:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget-object v10, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v11, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v10, v11, :cond_0

    .line 200
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    iget-object v10, v10, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->tags:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget v10, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v11, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 201
    .local v8, "text":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->tags:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget v1, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    .line 208
    .local v1, "icon":I
    :goto_1
    const v10, 0x7f0204cb

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 209
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    const v10, 0x7f02053d

    if-eq v1, v10, :cond_1

    .line 211
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 217
    :goto_2
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->selectedTag:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 218
    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 219
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->checkPosition:I
    invoke-static {v10, p1}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$202(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;I)I

    .line 220
    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    .line 226
    :goto_3
    new-instance v10, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;

    invoke-direct {v10, p0, p1}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList$1;-><init>(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;I)V

    invoke-virtual {v7, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    return-object v7

    .line 191
    .end local v1    # "icon":I
    .end local v8    # "text":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 193
    .local v6, "nne":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 205
    .end local v6    # "nne":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->tags:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget-object v8, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    .line 206
    .restart local v8    # "text":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->tags:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget v1, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    .restart local v1    # "icon":I
    goto :goto_1

    .line 214
    :cond_1
    const v10, 0x7f02051d

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 223
    :cond_2
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_3
.end method
