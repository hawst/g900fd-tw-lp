.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateConnectionStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0

    .prologue
    .line 841
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 844
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_0

    .line 845
    const-string v2, "Summary fragment is not added"

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 866
    :goto_0
    return-void

    .line 850
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectionStatus:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 853
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "wmanager_connected"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 855
    .local v0, "connectionStatus":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " getConnectedDevices connection status :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$600()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isActivityTrackerConnected(Landroid/content/Context;)Z

    move-result v1

    .line 859
    .local v1, "isActivityTrackerConnected":Z
    if-eqz v0, :cond_1

    const-string v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    :cond_1
    if-nez v1, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isWintipConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 861
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectionStatus:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 865
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->displayGearState()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    goto :goto_0

    .line 863
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectionStatus:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
