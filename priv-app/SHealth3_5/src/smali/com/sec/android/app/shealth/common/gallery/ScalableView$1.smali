.class Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;
.super Ljava/lang/Object;
.source "ScalableView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/gallery/ScalableView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 81
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewPagerChangeStats:Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$200(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 82
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewPagerChangeStats:Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$200(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;

    move-result-object v5

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v3

    const/high16 v6, 0x3f800000    # 1.0f

    cmpg-float v3, v3, v6

    if-gtz v3, :cond_1

    move v3, v4

    :goto_0
    invoke-interface {v5, v3}, Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;->setEnableFlip(Z)V

    .line 84
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$400(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/view/ScaleGestureDetector;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 85
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mDoubleTapGestureDetector:Landroid/view/GestureDetector;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$500(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/view/GestureDetector;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 86
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-direct {v0, v3, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 87
    .local v0, "currentPoint":Landroid/graphics/PointF;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 99
    :goto_1
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$900(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/Matrix;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 100
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->invalidate()V

    .line 101
    return v4

    .line 82
    .end local v0    # "currentPoint":Landroid/graphics/PointF;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 89
    .restart local v0    # "currentPoint":Landroid/graphics/PointF;
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mLastPoint:Landroid/graphics/PointF;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$600(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 90
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartPoint:Landroid/graphics/PointF;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$700(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/PointF;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mLastPoint:Landroid/graphics/PointF;
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$600(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/PointF;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_1

    .line 93
    :pswitch_2
    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mLastPoint:Landroid/graphics/PointF;
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$600(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/PointF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/PointF;->x:F

    sub-float v1, v3, v5

    .line 94
    .local v1, "deltaX":F
    iget v3, v0, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mLastPoint:Landroid/graphics/PointF;
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$600(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/PointF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float v2, v3, v5

    .line 95
    .local v2, "deltaY":F
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # invokes: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->correctMatrixMeasure(FF)V
    invoke-static {v3, v1, v2}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$800(Lcom/sec/android/app/shealth/common/gallery/ScalableView;FF)V

    .line 96
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mLastPoint:Landroid/graphics/PointF;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$600(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_1

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
