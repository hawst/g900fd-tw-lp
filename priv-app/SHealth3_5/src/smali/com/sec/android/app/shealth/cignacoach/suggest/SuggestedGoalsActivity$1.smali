.class Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$1;
.super Ljava/lang/Object;
.source "SuggestedGoalsActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateChanged()V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public onGoalMissionStatusChange(II)V
    .locals 2
    .param p1, "goalId"    # I
    .param p2, "missionId"    # I

    .prologue
    const/4 v0, -0x1

    .line 174
    if-eq p1, v0, :cond_0

    if-ne p2, v0, :cond_0

    .line 175
    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Because user goal completed or cancelled, suggested goal list update"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$1$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$1;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 185
    :goto_0
    return-void

    .line 183
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "In this case mission, suggested goal list no update"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
