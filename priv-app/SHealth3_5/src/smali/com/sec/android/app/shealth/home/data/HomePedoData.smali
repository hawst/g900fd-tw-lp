.class public Lcom/sec/android/app/shealth/home/data/HomePedoData;
.super Ljava/lang/Object;
.source "HomePedoData.java"


# instance fields
.field calories:F

.field deviceCount:I

.field latestDeviceTotalSteps:I

.field time:J

.field totalSteps:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public constructor <init>(IFJ)V
    .locals 1
    .param p1, "totalSteps"    # I
    .param p2, "calories"    # F
    .param p3, "time"    # J

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput p1, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->totalSteps:I

    .line 24
    iput p2, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->calories:F

    .line 25
    iput-wide p3, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->time:J

    .line 26
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->deviceCount:I

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->latestDeviceTotalSteps:I

    .line 28
    return-void
.end method

.method public constructor <init>(IFJII)V
    .locals 0
    .param p1, "totalSteps"    # I
    .param p2, "calories"    # F
    .param p3, "time"    # J
    .param p5, "latestDeviceTotalSteps"    # I
    .param p6, "deviceCount"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/home/data/HomePedoData;-><init>(IFJ)V

    .line 32
    iput p5, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->latestDeviceTotalSteps:I

    .line 33
    iput p6, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->deviceCount:I

    .line 34
    return-void
.end method


# virtual methods
.method public getCalories()F
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->calories:F

    return v0
.end method

.method public getDeviceCount()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->deviceCount:I

    return v0
.end method

.method public getLatestDeviceTotalSteps()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->latestDeviceTotalSteps:I

    return v0
.end method

.method public getLatestDeviceValueString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "goal"    # I

    .prologue
    const v3, 0x7f090b68

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 77
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getLastConnectedDeviceType()I

    move-result v0

    .line 80
    .local v0, "deviceType":I
    sparse-switch v0, :sswitch_data_0

    .line 106
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "text":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 82
    .end local v1    # "text":Ljava/lang/String;
    :sswitch_0
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 83
    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0

    .line 86
    .end local v1    # "text":Ljava/lang/String;
    :sswitch_1
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090bc6

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getLatestDeviceTotalSteps()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 87
    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0

    .line 91
    .end local v1    # "text":Ljava/lang/String;
    :sswitch_2
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090bc7

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getLatestDeviceTotalSteps()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 92
    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0

    .line 95
    .end local v1    # "text":Ljava/lang/String;
    :sswitch_3
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090bc8

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getLatestDeviceTotalSteps()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 96
    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0

    .line 99
    .end local v1    # "text":Ljava/lang/String;
    :sswitch_4
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090bc4

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getLatestDeviceTotalSteps()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 100
    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0

    .line 103
    .end local v1    # "text":Ljava/lang/String;
    :sswitch_5
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090bc9

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getLatestDeviceTotalSteps()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 104
    .restart local v1    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 80
    nop

    :sswitch_data_0
    .sparse-switch
        0x2719 -> :sswitch_0
        0x2723 -> :sswitch_1
        0x2724 -> :sswitch_2
        0x2726 -> :sswitch_3
        0x2727 -> :sswitch_5
        0x2728 -> :sswitch_2
        0x272e -> :sswitch_4
    .end sparse-switch
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->time:J

    return-wide v0
.end method

.method public getTotalSteps()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->totalSteps:I

    return v0
.end method

.method public setCalories(F)V
    .locals 0
    .param p1, "calories"    # F

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->calories:F

    .line 50
    return-void
.end method

.method public setDeviceCount(I)V
    .locals 0
    .param p1, "deviceCount"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->deviceCount:I

    .line 66
    return-void
.end method

.method public setLatestDeviceTotalSteps(I)V
    .locals 0
    .param p1, "latestDeviceTotalSteps"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->latestDeviceTotalSteps:I

    .line 74
    return-void
.end method

.method public setTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 57
    iput-wide p1, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->time:J

    .line 58
    return-void
.end method

.method public setTotalSteps(I)V
    .locals 0
    .param p1, "totalSteps"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/app/shealth/home/data/HomePedoData;->totalSteps:I

    .line 42
    return-void
.end method
