.class public Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;
.super Landroid/widget/LinearLayout;
.source "TGHContentBar.java"


# static fields
.field public static final DEFAULT_VALUE:F = -1000.0f

.field public static final FADE_DURATION:I = 0xa7

.field private static final INSIDE_RANGE:I = 0x1

.field private static final NONE:I = 0x0

.field private static final OUTSIDE_RANGE:I = 0x2


# instance fields
.field private mBalanceIconId:I

.field private mBalanceLineValue:F

.field private mBalanceText:Landroid/widget/TextView;

.field private mContextLayout:Landroid/widget/FrameLayout;

.field private mCurrentValue:F

.field private mFadeInBalanceTextAnimator:Landroid/animation/ObjectAnimator;

.field private mFadeInContextLayoutAnimator:Landroid/animation/ObjectAnimator;

.field private mFadeInValueLayoutAnimator:Landroid/animation/ObjectAnimator;

.field private mFadeOutBalanceTextAnimator:Landroid/animation/ObjectAnimator;

.field private mFadeOutContextLayoutAnimator:Landroid/animation/ObjectAnimator;

.field private mFadeOutValueLayoutAnimator:Landroid/animation/ObjectAnimator;

.field private mListener:Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;

.field private mMaskId:I

.field private mNoDataIconId:I

.field private mState:I

.field private mTextBalanceColor:I

.field private mTextNoDataColor:I

.field private mTextUnbalanceColor:I

.field private mTitleText:Landroid/widget/TextView;

.field private mUnbalanceIconId:I

.field private mUnitIcon:Landroid/widget/TextView;

.field private mUnitIconId:I

.field private mValueIcon:Landroid/widget/ImageView;

.field private mValueLayout:Landroid/widget/LinearLayout;

.field private mValueText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 50
    const/high16 v0, -0x3b860000    # -1000.0f

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceLineValue:F

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mState:I

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->init()V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const/high16 v0, -0x3b860000    # -1000.0f

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceLineValue:F

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mState:I

    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->init()V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    const/high16 v0, -0x3b860000    # -1000.0f

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceLineValue:F

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mState:I

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->init()V

    .line 87
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mValueLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValueText()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mContextLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->refreshBalanceState()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceText:Landroid/widget/TextView;

    return-object v0
.end method

.method private applyNewValue(F)V
    .locals 3
    .param p1, "value"    # F

    .prologue
    .line 202
    iget v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mCurrentValue:F

    cmpl-float v1, p1, v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-eqz v1, :cond_0

    .line 215
    :goto_0
    return-void

    .line 205
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mCurrentValue:F

    .line 206
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->cancelAllAnimation()V

    .line 207
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 208
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeInValueLayoutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeOutValueLayoutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 209
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->changeBalance(FLandroid/animation/AnimatorSet;)V

    .line 210
    const-wide/16 v1, 0xa7

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 211
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mListener:Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;

    if-eqz v1, :cond_1

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mListener:Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 214
    :cond_1
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method private cancelAllAnimation()V
    .locals 1

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeInValueLayoutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 219
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeOutValueLayoutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 220
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeInContextLayoutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeOutContextLayoutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 222
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeInBalanceTextAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 223
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeOutBalanceTextAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 224
    return-void
.end method

.method private changeBalance(FLandroid/animation/AnimatorSet;)V
    .locals 5
    .param p1, "value"    # F
    .param p2, "animatorSet"    # Landroid/animation/AnimatorSet;

    .prologue
    const/high16 v4, -0x3b860000    # -1000.0f

    .line 272
    iget v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mState:I

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeOutContextLayoutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 273
    .local v1, "fadeOut":Landroid/animation/ObjectAnimator;
    :goto_0
    iget v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mState:I

    if-nez v3, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeInContextLayoutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 274
    .local v0, "fadeIn":Landroid/animation/ObjectAnimator;
    :goto_1
    cmpl-float v3, p1, v4

    if-nez v3, :cond_3

    .line 275
    const/4 v2, 0x0

    .line 276
    .local v2, "state":I
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeOutContextLayoutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 277
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeInContextLayoutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 284
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->refreshBalanceState()V

    .line 286
    iget v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mState:I

    if-eq v3, v2, :cond_0

    .line 287
    iput v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mState:I

    .line 288
    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 290
    :cond_0
    return-void

    .line 272
    .end local v0    # "fadeIn":Landroid/animation/ObjectAnimator;
    .end local v1    # "fadeOut":Landroid/animation/ObjectAnimator;
    .end local v2    # "state":I
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeOutBalanceTextAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v1

    goto :goto_0

    .line 273
    .restart local v1    # "fadeOut":Landroid/animation/ObjectAnimator;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeInBalanceTextAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_1

    .line 278
    .restart local v0    # "fadeIn":Landroid/animation/ObjectAnimator;
    :cond_3
    iget v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceLineValue:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceLineValue:F

    cmpl-float v3, p1, v3

    if-ltz v3, :cond_4

    .line 279
    const/4 v2, 0x2

    .restart local v2    # "state":I
    goto :goto_2

    .line 281
    .end local v2    # "state":I
    :cond_4
    const/4 v2, 0x1

    .restart local v2    # "state":I
    goto :goto_2
.end method

.method private getFadeInAnimator(Landroid/view/View;)Landroid/animation/ObjectAnimator;
    .locals 3
    .param p1, "targetView"    # Landroid/view/View;

    .prologue
    .line 380
    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 381
    .local v0, "anim":Landroid/animation/ObjectAnimator;
    return-object v0

    .line 380
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private getFadeInBalanceTextAnimator()Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInBalanceTextAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceText:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeInAnimator(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInBalanceTextAnimator:Landroid/animation/ObjectAnimator;

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInBalanceTextAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$5;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInBalanceTextAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private getFadeInContextLayoutAnimator()Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInContextLayoutAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mContextLayout:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeInAnimator(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInContextLayoutAnimator:Landroid/animation/ObjectAnimator;

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInContextLayoutAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$3;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInContextLayoutAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private getFadeInValueLayoutAnimator()Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInValueLayoutAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mValueLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeInAnimator(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInValueLayoutAnimator:Landroid/animation/ObjectAnimator;

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInValueLayoutAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$1;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeInValueLayoutAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private getFadeOutAnimator(Landroid/view/View;)Landroid/animation/ObjectAnimator;
    .locals 3
    .param p1, "targetView"    # Landroid/view/View;

    .prologue
    .line 385
    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 386
    .local v0, "anim":Landroid/animation/ObjectAnimator;
    return-object v0

    .line 385
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private getFadeOutBalanceTextAnimator()Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutBalanceTextAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceText:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeOutAnimator(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutBalanceTextAnimator:Landroid/animation/ObjectAnimator;

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutBalanceTextAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$6;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutBalanceTextAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private getFadeOutContextLayoutAnimator()Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutContextLayoutAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mContextLayout:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeOutAnimator(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutContextLayoutAnimator:Landroid/animation/ObjectAnimator;

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutContextLayoutAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$4;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutContextLayoutAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private getFadeOutValueLayoutAnimator()Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutValueLayoutAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mValueLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getFadeOutAnimator(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutValueLayoutAnimator:Landroid/animation/ObjectAnimator;

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutValueLayoutAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar$2;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mFadeOutValueLayoutAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030254

    invoke-static {v0, v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 91
    const v0, 0x7f080a81

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mTitleText:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f080a7f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mValueText:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f080a82

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceText:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f080a7e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mValueIcon:Landroid/widget/ImageView;

    .line 96
    const v0, 0x7f080a80

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mUnitIcon:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f080a7d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mValueLayout:Landroid/widget/LinearLayout;

    .line 98
    const v0, 0x7f080a7c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mContextLayout:Landroid/widget/FrameLayout;

    .line 99
    const v0, 0x7f09027f

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mMaskId:I

    .line 101
    return-void
.end method

.method private refreshBalanceState()V
    .locals 4

    .prologue
    .line 230
    iget v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mState:I

    packed-switch v3, :pswitch_data_0

    .line 249
    const/4 v0, 0x0

    .line 250
    .local v0, "imageResId":I
    const/4 v1, 0x0

    .line 251
    .local v1, "textColor":I
    const/4 v2, 0x0

    .line 255
    .local v2, "unitResId":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mValueIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 256
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mUnitIcon:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    .line 257
    return-void

    .line 232
    .end local v0    # "imageResId":I
    .end local v1    # "textColor":I
    .end local v2    # "unitResId":I
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceIconId:I

    .line 233
    .restart local v0    # "imageResId":I
    iget v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mTextBalanceColor:I

    .line 234
    .restart local v1    # "textColor":I
    iget v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mUnitIconId:I

    .line 235
    .restart local v2    # "unitResId":I
    goto :goto_0

    .line 238
    .end local v0    # "imageResId":I
    .end local v1    # "textColor":I
    .end local v2    # "unitResId":I
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mUnbalanceIconId:I

    .line 239
    .restart local v0    # "imageResId":I
    iget v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mTextUnbalanceColor:I

    .line 240
    .restart local v1    # "textColor":I
    iget v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mUnitIconId:I

    .line 241
    .restart local v2    # "unitResId":I
    goto :goto_0

    .line 244
    .end local v0    # "imageResId":I
    .end local v1    # "textColor":I
    .end local v2    # "unitResId":I
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mNoDataIconId:I

    .line 245
    .restart local v0    # "imageResId":I
    iget v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mTextNoDataColor:I

    .line 246
    .restart local v1    # "textColor":I
    iget v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mUnitIconId:I

    .line 247
    .restart local v2    # "unitResId":I
    goto :goto_0

    .line 230
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setValueText()V
    .locals 6

    .prologue
    .line 260
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mCurrentValue:F

    const/high16 v1, -0x3b860000    # -1000.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mValueText:Landroid/widget/TextView;

    const v1, 0x7f09027e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 267
    :goto_0
    return-void

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mValueText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mMaskId:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mCurrentValue:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public setAnimatorListener(Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mListener:Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;

    .line 137
    return-void
.end method

.method public setBalanceLineValue(F)V
    .locals 2
    .param p1, "balanceLineValue"    # F

    .prologue
    const/high16 v1, 0x41200000    # 10.0f

    .line 159
    mul-float v0, p1, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceLineValue:F

    .line 160
    return-void
.end method

.method public setBalanceLineValue(I)V
    .locals 1
    .param p1, "balanceLineValue"    # I

    .prologue
    .line 169
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceLineValue:F

    .line 170
    return-void
.end method

.method public setStatisticsSource(Ljava/lang/String;)V
    .locals 0
    .param p1, "statisticsSource"    # Ljava/lang/String;

    .prologue
    .line 179
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "titleText"    # Ljava/lang/String;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    return-void
.end method

.method public setUnitIconIds(I)V
    .locals 0
    .param p1, "unitIconId"    # I

    .prologue
    .line 126
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mUnitIconId:I

    .line 128
    return-void
.end method

.method public setValue(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 187
    const v0, 0x7f090280

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mMaskId:I

    .line 188
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->applyNewValue(F)V

    .line 189
    return-void
.end method

.method public setValue(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 197
    const v0, 0x7f090280

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mMaskId:I

    .line 198
    int-to-float v0, p1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->applyNewValue(F)V

    .line 199
    return-void
.end method

.method public setValueIconIds(III)V
    .locals 0
    .param p1, "balanceIconId"    # I
    .param p2, "unbalanceIconId"    # I
    .param p3, "noDataIconId"    # I

    .prologue
    .line 120
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mBalanceIconId:I

    .line 121
    iput p2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mUnbalanceIconId:I

    .line 122
    iput p3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mNoDataIconId:I

    .line 123
    return-void
.end method

.method public setValueTextColors(III)V
    .locals 0
    .param p1, "textBalanceColor"    # I
    .param p2, "textUnbalanceColor"    # I
    .param p3, "textNoDataColor"    # I

    .prologue
    .line 147
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mTextBalanceColor:I

    .line 148
    iput p2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mTextUnbalanceColor:I

    .line 149
    iput p3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->mTextNoDataColor:I

    .line 150
    return-void
.end method
