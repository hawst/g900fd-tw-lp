.class Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$1;
.super Ljava/lang/Object;
.source "DeleteUserDataActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 56
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 57
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 58
    const v1, 0x7f090d47

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    const v2, 0x7f090d54

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 60
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 61
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->CONFIRM_DELETE:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$000(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 62
    return-void
.end method
