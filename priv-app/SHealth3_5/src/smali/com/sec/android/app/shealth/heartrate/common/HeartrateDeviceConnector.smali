.class public Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
.super Ljava/lang/Object;
.source "HeartrateDeviceConnector.java"


# static fields
.field private static final MEASURING_TIMEOUT:I = 0x3a98

.field private static TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

.field private static sCoverInstance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;


# instance fields
.field private IsDeviceStarted:Z

.field public isRecord:Z

.field private mContext:Landroid/content/Context;

.field private mCountDownTimer:Landroid/os/CountDownTimer;

.field mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field private mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

.field mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field private mHeartrateSensorListener:Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;

.field private mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->isRecord:Z

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mContext:Landroid/content/Context;

    .line 270
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$1;-><init>(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .line 324
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;-><init>(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .line 361
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$3;-><init>(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 398
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$4;

    const-wide/16 v2, 0x3a98

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$4;-><init>(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z

    return p1
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startDevice()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mHeartrateSensorListener:Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .line 65
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    return-object v0
.end method

.method public static getsCoverInstance()Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->sCoverInstance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->sCoverInstance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .line 72
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->sCoverInstance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    return-object v0
.end method

.method private scanDevice()V
    .locals 10

    .prologue
    const/16 v9, 0x2718

    const/4 v8, 0x4

    .line 86
    sget-object v4, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "scanDevice"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v5, 0x4

    const/16 v6, 0x2718

    const/4 v7, 0x4

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v0

    .line 90
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-eqz v0, :cond_4

    .line 91
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 92
    sget-object v4, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    const-string v5, "deviceList == isEmpty"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V

    .line 127
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_0
    :goto_0
    return-void

    .line 95
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_1
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 96
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 97
    .local v1, "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v4

    if-ne v4, v8, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v4

    if-ne v4, v9, :cond_2

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 100
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    .line 101
    sget-object v4, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "scanDevice() device: found- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_1

    .line 114
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 115
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 103
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    sget-object v4, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "scanDevice() device: else - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_5

    goto/16 :goto_1

    .line 116
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v2

    .line 117
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    .line 106
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v4, :cond_0

    .line 107
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_2 .. :try_end_2} :catch_5

    goto/16 :goto_0

    .line 118
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_2
    move-exception v2

    .line 119
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 111
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_4
    :try_start_3
    sget-object v4, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    const-string v5, "deviceList == null"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_3 .. :try_end_3} :catch_5

    goto/16 :goto_0

    .line 120
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :catch_3
    move-exception v2

    .line 121
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto/16 :goto_0

    .line 122
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_4
    move-exception v2

    .line 123
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_0

    .line 124
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :catch_5
    move-exception v2

    .line 125
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private startDevice()V
    .locals 4

    .prologue
    .line 147
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    const-string v2, "Start"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-nez v1, :cond_0

    .line 150
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    const-string v2, "SHealthDeviceFinder is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .line 180
    :goto_0
    return-void

    .line 153
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v1, :cond_1

    .line 154
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->scanDevice()V

    goto :goto_0

    .line 157
    :cond_1
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z

    if-nez v1, :cond_2

    .line 158
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 160
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->isRecord:Z

    if-eqz v1, :cond_2

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startRecord()V

    .line 164
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 167
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 168
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 169
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 171
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 172
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 173
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 174
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_0

    .line 175
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v0

    .line 176
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0
.end method

.method private stopDevice()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 210
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v2, :cond_0

    .line 213
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V

    .line 214
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "stopReceivingData_3 is called"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    .line 228
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_8

    .line 239
    :goto_1
    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 243
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-eqz v2, :cond_1

    .line 245
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->stopScan()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_b

    .line 255
    :goto_2
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->close()V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/util/ConcurrentModificationException; {:try_start_3 .. :try_end_3} :catch_d

    .line 261
    :goto_3
    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .line 263
    :cond_1
    return-void

    .line 215
    :catch_0
    move-exception v1

    .line 216
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 217
    .end local v1    # "e1":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 218
    .local v1, "e1":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 219
    .end local v1    # "e1":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 220
    .local v1, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 221
    .end local v1    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 222
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 223
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v0

    .line 224
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 229
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 230
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 231
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_6
    move-exception v0

    .line 232
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 233
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_7
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 235
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_8
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 246
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_9
    move-exception v1

    .line 247
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 248
    .end local v1    # "e1":Landroid/os/RemoteException;
    :catch_a
    move-exception v1

    .line 249
    .local v1, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_2

    .line 250
    .end local v1    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_b
    move-exception v0

    .line 251
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 256
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_c
    move-exception v0

    .line 257
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_3

    .line 258
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_d
    move-exception v0

    .line 259
    .local v0, "e":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v0}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    goto :goto_3
.end method


# virtual methods
.method public clearListener()V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mHeartrateSensorListener:Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;

    .line 267
    return-void
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z

    return v0
.end method

.method public startCountDownTimer()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 141
    return-void
.end method

.method public startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;
    .param p3, "isRecordON"    # Z

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mContext:Landroid/content/Context;

    .line 132
    if-eqz p2, :cond_0

    .line 133
    iput-object p2, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mHeartrateSensorListener:Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;

    .line 135
    :cond_0
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->isRecord:Z

    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startDevice()V

    .line 137
    return-void
.end method

.method public startRecord()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 185
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->record()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_5

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 190
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 192
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 193
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 194
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 195
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 196
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 197
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 198
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 199
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopCountDownTimer()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 144
    return-void
.end method

.method public stopMeasuring()V
    .locals 2

    .prologue
    .line 204
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopMeasuring"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->stopDevice()V

    .line 206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z

    .line 207
    return-void
.end method
