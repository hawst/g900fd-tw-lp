.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$9;
.super Ljava/lang/Object;
.source "RestoreActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V
    .locals 0

    .prologue
    .line 782
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 786
    const v0, 0x7f08008a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090d3f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 788
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v0, 0x7f08008b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$502(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;

    .line 789
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v0, 0x7f08008d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgress:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1502(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 790
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgress:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 792
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v0, 0x7f08008c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$602(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 794
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 795
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 796
    return-void
.end method
