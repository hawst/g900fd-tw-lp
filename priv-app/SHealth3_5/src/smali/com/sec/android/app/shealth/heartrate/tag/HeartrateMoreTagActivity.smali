.class public Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HeartrateMoreTagActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;
    }
.end annotation


# static fields
.field private static EDIT_TAG_CHECK:Ljava/lang/String;


# instance fields
.field private lvEditTagList:Landroid/widget/ListView;

.field private lvEmotionTagList:Landroid/widget/ListView;

.field private mAddtagView:Landroid/widget/LinearLayout;

.field mCustomTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

.field mEmotionTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mPref:Landroid/content/SharedPreferences;

.field private mSelTag:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-string/jumbo v0, "tagcheck"

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->EDIT_TAG_CHECK:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEmotionTagList:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEditTagList:Landroid/widget/ListView;

    .line 48
    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mSelTag:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEmotionTagList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEditTagList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->setcheckedPosition(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->markTag(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mSelTag:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->returnActivity(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V

    return-void
.end method

.method private createCustomOptionMenu()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 292
    const v6, 0x7f0207a4

    .line 293
    .local v6, "ACTION_BAR_ADD_BUTTON_SELECTOR":I
    const v7, 0x7f0207b0

    .line 294
    .local v7, "ACTION_BAR_DELETE_BUTTON_SELECTOR":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 295
    new-instance v4, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$2;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)V

    .line 320
    .local v4, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v1, 0x7f0207a4

    const v3, 0x7f09003f

    const v5, 0x7f020022

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;I)V

    .line 323
    .local v0, "actionBarButtonAddList":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v0, v3, v2

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 326
    return-void
.end method

.method private markTag(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V
    .locals 0
    .param p1, "tag"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mSelTag:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 161
    return-void
.end method

.method private returnActivity(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V
    .locals 6
    .param p1, "tag"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .prologue
    const/4 v5, 0x1

    .line 164
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 165
    .local v1, "mPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 166
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "from_more_tag"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 168
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 169
    .local v2, "returnIntent":Landroid/content/Intent;
    iget-object v3, p1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v4, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v3, v4, :cond_1

    .line 170
    const-string v3, "has_more_default_tag_pref"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 171
    const-string/jumbo v3, "more_tag_id"

    iget v4, p1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagId:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 181
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 182
    const/4 v3, -0x1

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->setResult(ILandroid/content/Intent;)V

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->finish()V

    .line 184
    return-void

    .line 173
    :cond_1
    iget-object v3, p1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 175
    const-string v3, "has_more_default_tag_pref"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 176
    const-string/jumbo v3, "more_tag_id"

    iget v4, p1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagId:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 177
    const-string/jumbo v3, "more_tag_name"

    iget-object v4, p1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 178
    const-string/jumbo v3, "more_tag_icon_id"

    iget v4, p1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagIconId:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method private setValueInternal(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 278
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 279
    .local v0, "prefsEdit":Landroid/content/SharedPreferences$Editor;
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 280
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 281
    return-void
.end method

.method private setcheckedPosition(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "value"    # I

    .prologue
    .line 272
    if-nez p1, :cond_0

    .line 275
    :goto_0
    return-void

    .line 274
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->EDIT_TAG_CHECK:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->setValueInternal(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 361
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 363
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$3;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)V

    .line 392
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 394
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f090048

    invoke-direct {v1, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    .line 395
    .local v1, "cancelButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setISOKCancelType(Z)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 396
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 398
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f090044

    invoke-direct {v2, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    .line 399
    .local v2, "saveButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setISOKCancelType(Z)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 400
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 401
    return-void
.end method

.method disablescroll(Landroid/widget/ListView;)V
    .locals 7
    .param p1, "mListVw"    # Landroid/widget/ListView;

    .prologue
    const/4 v6, 0x0

    .line 341
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 342
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_2

    .line 343
    const/4 v3, 0x0

    .line 344
    .local v3, "mHeight":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 345
    const/4 v5, 0x0

    invoke-interface {v0, v1, v5, p1}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 346
    .local v2, "listItem":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 347
    invoke-virtual {v2, v6, v6}, Landroid/view/View;->measure(II)V

    .line 348
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v3, v5

    .line 344
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 352
    .end local v2    # "listItem":Landroid/view/View;
    :cond_1
    invoke-virtual {p1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 353
    .local v4, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p1}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v5

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    mul-int/2addr v5, v6

    add-int/2addr v5, v3

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 354
    invoke-virtual {p1, v4}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 356
    .end local v1    # "i":I
    .end local v3    # "mHeight":I
    .end local v4    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_2
    return-void
.end method

.method public getCheckedPosition(Landroid/content/Context;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, -0x1

    .line 284
    if-nez p1, :cond_0

    .line 287
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mPref:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->EDIT_TAG_CHECK:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 329
    packed-switch p1, :pswitch_data_0

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 331
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    goto :goto_0

    .line 329
    :pswitch_data_0
    .packed-switch 0x45c
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "mCurrentConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 268
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 269
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mPref:Landroid/content/SharedPreferences;

    .line 56
    const v0, 0x7f03013f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->setContentView(I)V

    .line 57
    const v0, 0x7f080542

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mAddtagView:Landroid/widget/LinearLayout;

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mAddtagView:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$1;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 264
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 265
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 259
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 260
    return-void
.end method

.method protected onResume()V
    .locals 12

    .prologue
    const v11, 0x7f080549

    const/4 v10, -0x1

    const/4 v9, 0x1

    .line 188
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 190
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 191
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .local v5, "recentTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mPref:Landroid/content/SharedPreferences;

    const-string v8, "first_r_tag"

    invoke-interface {v7, v8, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v8, "second_r_tag"

    invoke-interface {v7, v8, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v8, "third_r_tag"

    invoke-interface {v7, v8, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    const v7, 0x7f080548

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ListView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEmotionTagList:Landroid/widget/ListView;

    .line 197
    const v7, 0x7f08054b

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ListView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEditTagList:Landroid/widget/ListView;

    .line 200
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEditTagList:Landroid/widget/ListView;

    invoke-virtual {v7, v9}, Landroid/widget/ListView;->setClickable(Z)V

    .line 201
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEmotionTagList:Landroid/widget/ListView;

    invoke-virtual {v7, v9}, Landroid/widget/ListView;->setClickable(Z)V

    .line 203
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 204
    .local v3, "mHeartrateTagEmotionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    const/16 v7, 0x52d1

    invoke-static {v7}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    const/16 v7, 0x52d2

    invoke-static {v7}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    const/16 v7, 0x52d3

    invoke-static {v7}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    const/16 v7, 0x52d4

    invoke-static {v7}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    const/16 v7, 0x52d5

    invoke-static {v7}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    const/16 v7, 0x52d6

    invoke-static {v7}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    const/16 v7, 0x5336

    invoke-static {v7}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    const/16 v7, 0x5337

    invoke-static {v7}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    const/16 v7, 0x5338

    invoke-static {v7}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getCustomTags()Ljava/util/ArrayList;

    move-result-object v0

    .line 216
    .local v0, "customTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_5

    .line 217
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v1, v7, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 218
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 220
    .local v6, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 221
    .local v4, "recTag":Ljava/lang/Integer;
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget v8, v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagId:I

    if-ne v7, v8, :cond_0

    .line 222
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 217
    .end local v4    # "recTag":Ljava/lang/Integer;
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 228
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v6    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v1, v7, -0x1

    :goto_1
    if-ltz v1, :cond_5

    .line 229
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 231
    .restart local v6    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 232
    .restart local v4    # "recTag":Ljava/lang/Integer;
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget v8, v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagId:I

    if-ne v7, v8, :cond_3

    .line 233
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 228
    .end local v4    # "recTag":Ljava/lang/Integer;
    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 240
    .end local v1    # "i":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v6    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_6

    .line 241
    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 246
    :goto_2
    new-instance v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mSelTag:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    invoke-direct {v7, p0, p0, v3, v8}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;Landroid/app/Activity;Ljava/util/ArrayList;Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V

    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mEmotionTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    .line 247
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEmotionTagList:Landroid/widget/ListView;

    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mEmotionTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 249
    new-instance v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mSelTag:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    invoke-direct {v7, p0, p0, v0, v8}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;Landroid/app/Activity;Ljava/util/ArrayList;Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V

    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mCustomTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    .line 250
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEditTagList:Landroid/widget/ListView;

    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mCustomTagListadapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 252
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEmotionTagList:Landroid/widget/ListView;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->disablescroll(Landroid/widget/ListView;)V

    .line 253
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEditTagList:Landroid/widget/ListView;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->disablescroll(Landroid/widget/ListView;)V

    .line 254
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEditTagList:Landroid/widget/ListView;

    invoke-virtual {v7, v9}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 255
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEmotionTagList:Landroid/widget/ListView;

    invoke-virtual {v7, v9}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 256
    return-void

    .line 243
    :cond_6
    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method
