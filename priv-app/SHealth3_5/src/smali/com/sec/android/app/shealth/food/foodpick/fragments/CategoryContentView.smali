.class Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;
.super Landroid/widget/FrameLayout;
.source "CategoryContentView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;
    }
.end annotation


# instance fields
.field private mAdapter:Landroid/widget/ListAdapter;

.field private mOnScrollToEndListener:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;

.field private mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->init()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->init()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->init()V

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->mOnScrollToEndListener:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;

    return-object v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030101

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 70
    return-void
.end method

.method private initListView(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 73
    const v2, 0x7f08044c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 76
    .local v1, "listView":Landroid/widget/ListView;
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 77
    .local v0, "header":Landroid/view/View;
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 78
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 79
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->removeHeaderView(Landroid/view/View;)Z

    .line 80
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$1;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;Landroid/widget/ListView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 95
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 96
    return-void
.end method

.method private initLogoIfNeeded(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 99
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->hasLogo()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    const v1, 0x7f08044d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 101
    .local v0, "logo":Landroid/widget/ImageView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->getLogoResourceId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 104
    .end local v0    # "logo":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method


# virtual methods
.method public setFoodSearchApi(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)V
    .locals 0
    .param p1, "foodSearchApi"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    .line 65
    invoke-direct {p0, p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->initLogoIfNeeded(Landroid/view/View;)V

    .line 66
    return-void
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 0
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->mAdapter:Landroid/widget/ListAdapter;

    .line 56
    invoke-direct {p0, p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->initListView(Landroid/view/View;)V

    .line 57
    return-void
.end method

.method public setOnScrollToEndListener(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;)V
    .locals 0
    .param p1, "onScrollToEndListener"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->mOnScrollToEndListener:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;

    .line 114
    return-void
.end method
