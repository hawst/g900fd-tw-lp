.class public Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final DELTA_ALLOWED_FOR_TAP:I = 0xf


# instance fields
.field private mCanBeTap:Z

.field private mDistanceMoved:D

.field private mPreviousX:F

.field private mPreviousY:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mCanBeTap:Z

    return-void
.end method

.method private computeDistanceBetween(FFFF)D
    .locals 6

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    sub-float v0, p1, p3

    float-to-double v0, v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    sub-float v2, p2, p4

    float-to-double v2, v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private refreshCanBeTapField(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 5

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mCanBeTap:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p2, v4}, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->updateDistanceAndCoordinates(Landroid/view/MotionEvent;Z)V

    iget-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mDistanceMoved:D

    const-wide/high16 v2, 0x402e000000000000L    # 15.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mCanBeTap:Z

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->onNotTap(Landroid/view/View;Landroid/view/MotionEvent;)V

    :cond_0
    return-void
.end method

.method private updateDistanceAndCoordinates(Landroid/view/MotionEvent;Z)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    if-nez p2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mDistanceMoved:D

    iget v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mPreviousX:F

    iget v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mPreviousY:F

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->computeDistanceBetween(FFFF)D

    move-result-wide v4

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mDistanceMoved:D

    :goto_0
    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mPreviousX:F

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mPreviousY:F

    return-void

    :cond_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mDistanceMoved:D

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mCanBeTap:Z

    goto :goto_0
.end method


# virtual methods
.method protected onGenericTouchEvent(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method protected onNotTap(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method protected onScroll(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method protected onTap(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->onGenericTouchEvent(Landroid/view/View;Landroid/view/MotionEvent;)V

    const/4 v0, 0x0

    return v0

    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->updateDistanceAndCoordinates(Landroid/view/MotionEvent;Z)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->refreshCanBeTapField(Landroid/view/View;Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->refreshCanBeTapField(Landroid/view/View;Landroid/view/MotionEvent;)V

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->mCanBeTap:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->onTap(Landroid/view/View;Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/TouchListenerForScrollViews;->onScroll(Landroid/view/View;Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
