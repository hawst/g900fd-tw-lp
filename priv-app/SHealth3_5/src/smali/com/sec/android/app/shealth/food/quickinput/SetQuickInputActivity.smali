.class public Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "SetQuickInputActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# static fields
.field private static final SET_QUICK_INPUT_TAG:Ljava/lang/String; = "set_quick_input_tag"


# instance fields
.field private final SPACE:Ljava/lang/String;

.field private mBreakfastRow:Landroid/widget/RelativeLayout;

.field private mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

.field private mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

.field private mClickedView:Landroid/view/View;

.field private mDinnerRow:Landroid/widget/RelativeLayout;

.field mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

.field private mIncorrectRangeToast:Landroid/widget/Toast;

.field private mInvalidInputToast:Landroid/widget/Toast;

.field private mLunchRow:Landroid/widget/RelativeLayout;

.field mMealRowClickListener:Landroid/view/View$OnClickListener;

.field private mMinMaxIntegerInputFilter:Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

.field private mSetQuickInputDialog:Landroid/support/v4/app/DialogFragment;

.field private mSnacksRow:Landroid/widget/RelativeLayout;

.field mealTitleResourceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 52
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->SPACE:Ljava/lang/String;

    .line 58
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$1;-><init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mealTitleResourceMap:Ljava/util/HashMap;

    .line 227
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;-><init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mMealRowClickListener:Landroid/view/View$OnClickListener;

    .line 391
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$5;-><init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    .line 408
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$6;

    const/high16 v1, 0x40000000    # 2.0f

    const v2, 0x459c4000    # 5000.0f

    const/4 v3, 0x2

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$6;-><init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mMinMaxIntegerInputFilter:Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;)Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Landroid/support/v4/app/DialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mSetQuickInputDialog:Landroid/support/v4/app/DialogFragment;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;Landroid/support/v4/app/DialogFragment;)Landroid/support/v4/app/DialogFragment;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;
    .param p1, "x1"    # Landroid/support/v4/app/DialogFragment;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mSetQuickInputDialog:Landroid/support/v4/app/DialogFragment;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getCalorieValue()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->showRangeToast()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mMinMaxIntegerInputFilter:Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mInvalidInputToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mInvalidInputToast:Landroid/widget/Toast;

    return-object p1
.end method

.method private getBreakfastRow()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mBreakfastRow:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 74
    const v0, 0x7f08049d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mBreakfastRow:Landroid/widget/RelativeLayout;

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mBreakfastRow:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private getCalorieValue()I
    .locals 4

    .prologue
    .line 350
    const/4 v1, 0x0

    .line 351
    .local v1, "calValue":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 352
    .local v0, "calString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 353
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 355
    :cond_0
    return v1
.end method

.method private getDinnerRow()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mDinnerRow:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 96
    const v0, 0x7f08049f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mDinnerRow:Landroid/widget/RelativeLayout;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mDinnerRow:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private getLunchRow()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mLunchRow:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 85
    const v0, 0x7f08049e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mLunchRow:Landroid/widget/RelativeLayout;

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mLunchRow:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private getSnacksRow()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mSnacksRow:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 107
    const v0, 0x7f0804a0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mSnacksRow:Landroid/widget/RelativeLayout;

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mSnacksRow:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private initCalorieValues()V
    .locals 0

    .prologue
    .line 188
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->setBreakFastCalorieText()V

    .line 189
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->setLunchCalorieText()V

    .line 190
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->setDinnerCalorieText()V

    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->setSnacksCalorieText()V

    .line 193
    return-void
.end method

.method private initMealText()V
    .locals 4

    .prologue
    const v3, 0x7f0803b0

    .line 179
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getBreakfastRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090920

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getLunchRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090921

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getDinnerRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090922

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getSnacksRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090923

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    return-void
.end method

.method private setBreakFastCalorieText()V
    .locals 4

    .prologue
    .line 197
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->BREAK_FAST:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;->getCalorieForMealType(Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "calorieValue":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getBreakfastRow()Landroid/widget/RelativeLayout;

    move-result-object v1

    const v2, 0x7f0803b1

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    return-void
.end method

.method private setClickListeners()V
    .locals 2

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getBreakfastRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mMealRowClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getLunchRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mMealRowClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getDinnerRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mMealRowClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getSnacksRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mMealRowClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    return-void
.end method

.method private setDinnerCalorieText()V
    .locals 4

    .prologue
    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->DINNER:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;->getCalorieForMealType(Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    .local v0, "calorieValue":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getDinnerRow()Landroid/widget/RelativeLayout;

    move-result-object v1

    const v2, 0x7f0803b1

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    return-void
.end method

.method private setLayoutTags()V
    .locals 2

    .prologue
    .line 320
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getBreakfastRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->BREAK_FAST:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 321
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getLunchRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->LUNCH:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 322
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getDinnerRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->DINNER:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 323
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getSnacksRow()Landroid/widget/RelativeLayout;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->SNACKS:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 325
    return-void
.end method

.method private setLunchCalorieText()V
    .locals 4

    .prologue
    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->LUNCH:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;->getCalorieForMealType(Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "calorieValue":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getLunchRow()Landroid/widget/RelativeLayout;

    move-result-object v1

    const v2, 0x7f0803b1

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    return-void
.end method

.method private setSnacksCalorieText()V
    .locals 4

    .prologue
    .line 215
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->SNACKS:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;->getCalorieForMealType(Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, "calorieValue":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getSnacksRow()Landroid/widget/RelativeLayout;

    move-result-object v1

    const v2, 0x7f0803b1

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    return-void
.end method

.method private showRangeToast()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    .line 329
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mIncorrectRangeToast:Landroid/widget/Toast;

    if-nez v1, :cond_0

    .line 332
    const v1, 0x7f09098d

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x1388

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 336
    .local v0, "text":Ljava/lang/String;
    invoke-static {p0, v0, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mIncorrectRangeToast:Landroid/widget/Toast;

    .line 340
    .end local v0    # "text":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mIncorrectRangeToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 343
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mIncorrectRangeToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 346
    :cond_1
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 3

    .prologue
    .line 165
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 169
    .local v0, "actionBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 171
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeCustomView()V

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09094f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 373
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;-><init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V

    return-object v0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 272
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;-><init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 116
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 117
    const v0, 0x7f030117

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->setContentView(I)V

    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->initMealText()V

    .line 119
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->setLayoutTags()V

    .line 120
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->initCalorieValues()V

    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->setClickListeners()V

    .line 123
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 128
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 130
    if-eqz p1, :cond_1

    const-string/jumbo v1, "meal_type"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 132
    const-string/jumbo v1, "meal_type"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 133
    .local v0, "clickedMealType":I
    invoke-static {}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->values()[Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    move-result-object v1

    aget-object v1, v1, v0

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->BREAK_FAST:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne v1, v2, :cond_2

    .line 138
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getBreakfastRow()Landroid/widget/RelativeLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedView:Landroid/view/View;

    .line 156
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "set_quick_input_tag"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mSetQuickInputDialog:Landroid/support/v4/app/DialogFragment;

    .line 159
    .end local v0    # "clickedMealType":I
    :cond_1
    return-void

    .line 141
    .restart local v0    # "clickedMealType":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->LUNCH:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne v1, v2, :cond_3

    .line 143
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getLunchRow()Landroid/widget/RelativeLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedView:Landroid/view/View;

    goto :goto_0

    .line 146
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->DINNER:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne v1, v2, :cond_4

    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getDinnerRow()Landroid/widget/RelativeLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedView:Landroid/view/View;

    goto :goto_0

    .line 151
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->SNACKS:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne v1, v2, :cond_0

    .line 153
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getSnacksRow()Landroid/widget/RelativeLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedView:Landroid/view/View;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-eqz v0, :cond_0

    .line 363
    const-string/jumbo v0, "meal_type"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 366
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 368
    return-void
.end method
