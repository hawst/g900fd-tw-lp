.class public Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;
.super Ljava/lang/Object;
.source "LastUserGoalMissionInfo.java"


# static fields
.field private static mInstance:Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;


# instance fields
.field private mGoalID:I

.field private mMissionID:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;->mGoalID:I

    .line 12
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;->mMissionID:I

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;->mInstance:Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;->mInstance:Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;

    .line 18
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;->mInstance:Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;

    return-object v0
.end method


# virtual methods
.method public setGoalID(I)V
    .locals 0
    .param p1, "goalID"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;->mGoalID:I

    .line 27
    return-void
.end method

.method public setMissionID(I)V
    .locals 0
    .param p1, "missionID"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;->mMissionID:I

    .line 35
    return-void
.end method
