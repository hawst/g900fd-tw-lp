.class public Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;
.super Ljava/lang/Object;
.source "EventKeyUtils.java"


# static fields
.field public static IsExternKeyBoardDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->IsExternKeyBoardDisabled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isCallKey(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p0, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 282
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 290
    :cond_0
    :goto_0
    return v0

    .line 286
    :cond_1
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    .line 290
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isExternalKeyBoardDisabled()Z
    .locals 1

    .prologue
    .line 169
    sget-boolean v0, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->IsExternKeyBoardDisabled:Z

    return v0
.end method

.method public static isExternalKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 6
    .param p0, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 33
    sget-boolean v4, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->IsExternKeyBoardDisabled:Z

    if-eqz v4, :cond_0

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->isMusicKey(Landroid/view/KeyEvent;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->isSpecialFunction(Landroid/view/KeyEvent;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->isCallKey(Landroid/view/KeyEvent;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 35
    :cond_0
    const/4 v3, 0x0

    .line 89
    :cond_1
    :goto_0
    return v3

    .line 37
    :cond_2
    const/4 v3, 0x0

    .line 39
    .local v3, "isExternal":Z
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    .line 41
    .local v0, "device":Landroid/view/InputDevice;
    const/4 v2, 0x0

    .line 47
    .local v2, "field":Ljava/lang/reflect/Field;
    if-eqz v0, :cond_3

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 49
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string/jumbo v5, "mIsExternal"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 51
    :cond_3
    if-eqz v2, :cond_1

    .line 55
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 57
    invoke-virtual {v2, v0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v3

    goto :goto_0

    .line 63
    :catch_0
    move-exception v1

    .line 69
    .local v1, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v1}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 73
    .end local v1    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v1

    .line 77
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 81
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 85
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isExternalKeyEvent2(Landroid/view/KeyEvent;)Z
    .locals 6
    .param p0, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 101
    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->isMusicKey(Landroid/view/KeyEvent;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->isSpecialFunction(Landroid/view/KeyEvent;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->isCallKey(Landroid/view/KeyEvent;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 103
    :cond_0
    const/4 v3, 0x0

    .line 157
    :cond_1
    :goto_0
    return v3

    .line 105
    :cond_2
    const/4 v3, 0x0

    .line 107
    .local v3, "isExternal":Z
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    .line 109
    .local v0, "device":Landroid/view/InputDevice;
    const/4 v2, 0x0

    .line 115
    .local v2, "field":Ljava/lang/reflect/Field;
    if-eqz v0, :cond_3

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 117
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string/jumbo v5, "mIsExternal"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 119
    :cond_3
    if-eqz v2, :cond_1

    .line 123
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 125
    invoke-virtual {v2, v0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v3

    goto :goto_0

    .line 131
    :catch_0
    move-exception v1

    .line 137
    .local v1, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v1}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 141
    .end local v1    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v1

    .line 145
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 149
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 153
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isMusicKey(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p0, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 182
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x7e

    if-ne v1, v2, :cond_1

    .line 246
    :cond_0
    :goto_0
    return v0

    .line 186
    :cond_1
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x58

    if-eq v1, v2, :cond_0

    .line 190
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x82

    if-eq v1, v2, :cond_0

    .line 194
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x59

    if-eq v1, v2, :cond_0

    .line 198
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x56

    if-eq v1, v2, :cond_0

    .line 202
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x80

    if-eq v1, v2, :cond_0

    .line 206
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x81

    if-eq v1, v2, :cond_0

    .line 210
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x5a

    if-eq v1, v2, :cond_0

    .line 214
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x57

    if-eq v1, v2, :cond_0

    .line 218
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x7f

    if-eq v1, v2, :cond_0

    .line 222
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x55

    if-eq v1, v2, :cond_0

    .line 226
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0xd1

    if-eq v1, v2, :cond_0

    .line 230
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x5b

    if-eq v1, v2, :cond_0

    .line 234
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x19

    if-eq v1, v2, :cond_0

    .line 238
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0xa4

    if-eq v1, v2, :cond_0

    .line 242
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x18

    if-eq v1, v2, :cond_0

    .line 246
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSpecialFunction(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p0, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 259
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 263
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 265
    const/4 v0, 0x1

    .line 269
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
