.class public Lcom/sec/android/app/shealth/cignacoach/constants/CignaLibraryConstants;
.super Ljava/lang/Object;
.source "CignaLibraryConstants.java"


# static fields
.field public static final CATEGORY_EXERCISE:Ljava/lang/String; = "exercise"

.field public static final CATEGORY_FOOD:Ljava/lang/String; = "food"

.field public static final CATEGORY_SLEEP:Ljava/lang/String; = "sleep"

.field public static final CATEGORY_STRESS:Ljava/lang/String; = "stress"

.field public static final CATEGORY_WEIGHT:Ljava/lang/String; = "weight"

.field public static final EXTRA_NAME_ARTICLE_ID:Ljava/lang/String; = "extra_name_article_id"

.field public static final EXTRA_NAME_ARTICLE_IMAGE_RESOURCE_ID:Ljava/lang/String; = "article_resource_image"

.field public static final EXTRA_NAME_CATEGORY_ID:Ljava/lang/String; = "extra_name_category_id"

.field public static final EXTRA_NAME_SUB_CATEGORY_ID:Ljava/lang/String; = "extra_name_sub_category_id"

.field public static final REQUEST_CODE_FAVORITE_DELETE:I = 0x64

.field public static final WEBVIEW_URL_PREFIX_STRING:Ljava/lang/String; = "http://"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
