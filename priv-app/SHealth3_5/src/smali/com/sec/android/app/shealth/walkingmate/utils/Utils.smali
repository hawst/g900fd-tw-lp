.class public Lcom/sec/android/app/shealth/walkingmate/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;,
        Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;,
        Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;,
        Lcom/sec/android/app/shealth/walkingmate/utils/Utils$AnimationListenerForLazy;
    }
.end annotation


# static fields
.field private static final ACTIVITY_TRACKER_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_SBAND_SYNC"

.field protected static final ADD_SAMSUNG_ACCOUNT:Ljava/lang/String; = "com.osp.app.signin"

.field private static final DAY_CHAR:Ljava/lang/String; = "d"

.field public static final FILTER_ALL:Ljava/lang/String; = "0==0"

.field private static final GEAR2_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_GEAR2_SYNC"

.field private static final GEAR3_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_GEAR_SYNC"

.field private static final GEAR_SYNC:Ljava/lang/String; = "android.intent.action.PEDOMETER_SETTING_SYNC"

.field private static final MONTH_CHAR:Ljava/lang/String; = "M"

.field private static final MONTH_GROUP_DATE_SEPARATOR:Ljava/lang/String; = " "

.field private static final MONTH_GROUP_MONTH_FORMAT_PATTERN:Ljava/lang/String; = "MMM"

.field private static final MONTH_GROUP_YEAR_FORMAT_PATTERN:Ljava/lang/String; = "yyyy"

.field private static final REGEX_END_OF_LINE:Ljava/lang/String; = "$"

.field private static final REGEX_ONE_OR_MORE:Ljava/lang/String; = "+"

.field public static final SQLITE_WFL_GROUPBY_DAY:Ljava/lang/String; = "strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\')"

.field public static final SQLITE_WFL_GROUPBY_HOUR:Ljava/lang/String; = "strftime(\"%d-%m-%Y %H\",([start_time]/1000),\'unixepoch\',\'localtime\')"

.field public static final SQLITE_WFL_GROUPBY_MONTH:Ljava/lang/String; = "strftime(\"%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\')"

.field private static final TAG:Ljava/lang/String; = "Utils"

.field private static final WINGTIP_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_WINGTIP_SYNC"

.field private static final YEAR_CHAR:Ljava/lang/String; = "y"

.field private static final tempCal:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->tempCal:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 879
    return-void
.end method

.method public static checkForSamsungAccounts(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 480
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 481
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "com.osp.app.signin"

    const-string v4, "com.osp.app.signin.AccountView"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 482
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    if-nez v3, :cond_1

    .line 491
    :cond_0
    :goto_0
    return v2

    .line 486
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getCurrentSamsungAccountUserName()Ljava/lang/String;

    move-result-object v0

    .line 487
    .local v0, "accountUserName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 491
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static clearWalkInfoData(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 397
    const/4 v1, 0x0

    .line 398
    .local v1, "rowsDeleted":I
    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 400
    if-eqz p0, :cond_0

    .line 401
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "exercise_type = 20003"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 402
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 403
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.shealth.command.resettotalstep"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 404
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 407
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return v1
.end method

.method public static clearWalkInfoExtendedData(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 412
    const-string v1, "PEDOCHECK"

    const-string v2, "delete all walk_info_extended"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    const/4 v0, 0x0

    .line 414
    .local v0, "rowsDeleted":I
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 416
    if-eqz p0, :cond_0

    .line 417
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 420
    :cond_0
    return v0
.end method

.method public static clearWalkingGoalData(Landroid/content/Context;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 425
    const/4 v0, 0x0

    .line 426
    .local v0, "rowsDeleted":I
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 428
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "goal_type=40001"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 430
    :cond_0
    return v0
.end method

.method public static convertDpToPx(Landroid/content/Context;F)F
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dp"    # F

    .prologue
    .line 110
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/content/res/Resources;F)F

    move-result v0

    return v0
.end method

.method public static convertDpToPx(Landroid/content/res/Resources;F)F
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "dp"    # F

    .prologue
    .line 114
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/util/DisplayMetrics;F)F

    move-result v0

    return v0
.end method

.method public static convertDpToPx(Landroid/util/DisplayMetrics;F)F
    .locals 1
    .param p0, "displayMetrics"    # Landroid/util/DisplayMetrics;
    .param p1, "dp"    # F

    .prologue
    .line 118
    const/4 v0, 0x1

    invoke-static {v0, p1, p0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public static deleteWalkDataInExercise(JLandroid/content/Context;)V
    .locals 9
    .param p0, "time"    # J
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 355
    const-string v1, "_id in(SELECT EX._id FROM exercise AS EX, user_device AS UD, exercise_device_info AS EDI WHERE EX.start_time BETWEEN ? AND ? AND EX.exercise_type = 20003 AND EX._id = EDI.exercise__id AND EDI.user_device__id = UD._id AND UD.device_type = 10009)"

    .line 365
    .local v1, "selectionClause":Ljava/lang/String;
    const-string v3, "_id in(SELECT WI._id FROM walk_info AS WI, user_device AS UD WHERE WI.start_time BETWEEN ? AND ? AND WI.user_device__id = UD._id AND UD.device_type = 10009)"

    .line 372
    .local v3, "walk_info_selectionClause":Ljava/lang/String;
    const-string v4, "PEDOCHECK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "delete walk_info_extended, time =  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    const-string v2, "_id in(SELECT WIE._id FROM walk_info_extended AS WIE WHERE WIE.start_time BETWEEN ? AND ? )"

    .line 378
    .local v2, "walk_info_extended_selectionClause":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v0, v7

    const-string v4, ""

    aput-object v4, v0, v8

    .line 380
    .local v0, "mSelectionArgs":[Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v7

    .line 381
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v8

    .line 382
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v1, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 383
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 384
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v2, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 385
    return-void
.end method

.method public static disableClickFor(JLandroid/view/View;)V
    .locals 1
    .param p0, "millis"    # J
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 335
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setClickable(Z)V

    .line 336
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$1;

    invoke-direct {v0, p2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$1;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v0, p0, p1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 342
    return-void
.end method

.method public static displayLastUpdatedTime(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 8
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "isMyRanking"    # Z

    .prologue
    .line 689
    const-wide/16 v0, 0x0

    .line 690
    .local v0, "time":J
    const-string v4, ""

    .line 692
    .local v4, "updatedTimeStr":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 693
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getMyRankingLastUpdatedTime()J

    move-result-wide v0

    .line 697
    :goto_0
    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-lez v5, :cond_0

    .line 698
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    .line 699
    .local v2, "timeFormat":Ljava/text/DateFormat;
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 700
    .local v3, "timeStr":Ljava/lang/String;
    if-eqz p1, :cond_2

    .line 701
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090aba

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v7, "/"

    invoke-static {p0, v6, v7}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getSystemDateFormat(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 705
    .end local v2    # "timeFormat":Ljava/text/DateFormat;
    .end local v3    # "timeStr":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v4

    .line 695
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getLboardRankingLastUpdatedTime()J

    move-result-wide v0

    goto :goto_0

    .line 703
    .restart local v2    # "timeFormat":Ljava/text/DateFormat;
    .restart local v3    # "timeStr":Ljava/lang/String;
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0907e1

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v7, "/"

    invoke-static {p0, v6, v7}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getSystemDateFormat(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public static getAverageSteps(Landroid/content/Context;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 592
    const/4 v8, 0x0

    .line 593
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 594
    .local v7, "avgsteps":F
    const-string v6, "AVG_STEP"

    .line 595
    .local v6, "avgColumn":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select (SUM(total_step) / ((strftime(\'%s\',(MAX(start_time) / 1000),\'unixepoch\',\'localtime\',\'start of day\',\'+1 day\',\'utc\') - (strftime(\'%s\',(MIN(start_time) / 1000),\'unixepoch\',\'localtime\',\'start of day\',\'utc\'))) / (60 * 60 * 24))) as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from walk_info "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "left outer join "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "user_device "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "on walk_info.user_device__id == user_device._id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "where walk_info.sync_status != 170004"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and total_step > 0 and ifnull(device_type, 10009) and device_type == 10009 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 605
    .local v3, "selectionClause2":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 606
    if-eqz v8, :cond_2

    .line 607
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 608
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 609
    invoke-interface {v8, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getFloat(I)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 615
    :cond_0
    :goto_0
    if-eqz v8, :cond_1

    .line 616
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 619
    :cond_1
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v9

    .line 620
    .local v9, "step":I
    return v9

    .line 612
    .end local v9    # "step":I
    :cond_2
    const/4 v7, 0x0

    goto :goto_0

    .line 615
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 616
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static getBestStepValue(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 267
    const/4 v6, 0x0

    .line 269
    .local v6, "bestStep":I
    const-string v8, "0==0"

    .line 270
    .local v8, "selectionClause":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and device_type == 10009"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select max(SUM_DAY_STEP) as best_step from (select sum(total_step) as SUM_DAY_STEP from walk_info left outer join user_device on walk_info.user_device__id == user_device._id where "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and walk_info.sync_status != 170004"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and total_step > 0 and ifnull(device_type, 10009) and device_type == 10009 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "group by (strftime(\'%d-%m-%Y\',([start_time]/1000),\'unixepoch\',\'localtime\')))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 280
    .local v3, "selectionClause2":Ljava/lang/String;
    const/4 v7, 0x0

    .line 282
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 284
    if-eqz v7, :cond_1

    .line 285
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 286
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 288
    const-string v0, "best_step"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 289
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 293
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_0

    .line 294
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 293
    :cond_1
    if-eqz v7, :cond_2

    .line 294
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 297
    :cond_2
    return v6
.end method

.method public static getCountryCode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 199
    const/4 v2, 0x0

    .line 202
    .local v2, "countryCode":Ljava/lang/String;
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 203
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 204
    .local v4, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "ro.csc.countryiso_code"

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 205
    :catch_0
    move-exception v3

    .line 206
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getCurrentSamsungAccountUserName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 495
    const/4 v2, 0x0

    .line 497
    .local v2, "email":Ljava/lang/String;
    const/4 v0, 0x0

    .line 498
    .local v0, "account":Landroid/accounts/Account;
    const/4 v1, 0x0

    .line 499
    .local v1, "accounts":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 500
    .local v3, "manager":Landroid/accounts/AccountManager;
    if-eqz v3, :cond_0

    .line 501
    const-string v4, "com.osp.app.signin"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 503
    :cond_0
    if-eqz v1, :cond_2

    .line 504
    array-length v4, v1

    if-lez v4, :cond_1

    .line 505
    const/4 v4, 0x0

    aget-object v0, v1, v4

    .line 507
    :cond_1
    if-eqz v0, :cond_2

    .line 508
    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 511
    :cond_2
    return-object v2
.end method

.method public static getKoreanMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "time"    # J

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 732
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 733
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 734
    const-string v1, "%d%s %d%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const v3, 0x7f090066

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x3

    const v4, 0x7f090068

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getLoadUserDevice()[I
    .locals 16

    .prologue
    const/4 v15, 0x1

    .line 738
    const/4 v0, 0x7

    new-array v7, v0, [I

    fill-array-data v7, :array_0

    .line 739
    .local v7, "device":[I
    const/4 v6, 0x0

    .line 740
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 741
    .local v9, "existGear":Z
    const/4 v10, 0x0

    .line 743
    .local v10, "existTizen":Z
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "device_type"

    aput-object v1, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 745
    .local v2, "projection":[Ljava/lang/String;
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 749
    :goto_0
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 750
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 752
    const-string v0, "device_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 779
    :goto_2
    :pswitch_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 801
    .end local v2    # "projection":[Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 802
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 746
    .restart local v2    # "projection":[Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 747
    .local v8, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 754
    .end local v8    # "e":Ljava/lang/Exception;
    :pswitch_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, v7, v0

    goto :goto_2

    .line 757
    :pswitch_2
    const/4 v0, 0x1

    const/4 v1, 0x0

    aput v1, v7, v0

    .line 758
    const/4 v9, 0x1

    .line 759
    goto :goto_2

    .line 761
    :pswitch_3
    const/4 v0, 0x2

    const/4 v1, 0x0

    aput v1, v7, v0

    .line 762
    const/4 v10, 0x1

    .line 763
    goto :goto_2

    .line 765
    :pswitch_4
    const/4 v0, 0x3

    const/4 v1, 0x0

    aput v1, v7, v0

    goto :goto_2

    .line 768
    :pswitch_5
    const/4 v0, 0x4

    const/4 v1, 0x0

    aput v1, v7, v0

    goto :goto_2

    .line 771
    :pswitch_6
    const/4 v0, 0x5

    const/4 v1, 0x0

    aput v1, v7, v0

    goto :goto_2

    .line 774
    :pswitch_7
    const/4 v0, 0x6

    const/4 v1, 0x0

    aput v1, v7, v0

    goto :goto_2

    .line 782
    :cond_1
    if-eqz v9, :cond_2

    if-eqz v10, :cond_2

    .line 783
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getGearConnectedTime()J

    move-result-wide v11

    .line 784
    .local v11, "gearConnectedTime":J
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getTizenGearConnectedTime()J

    move-result-wide v13

    .line 787
    .local v13, "tizenConnectedTime":J
    cmp-long v0, v11, v13

    if-gez v0, :cond_5

    .line 788
    const/4 v0, 0x1

    const/16 v1, 0x8

    aput v1, v7, v0

    .line 796
    .end local v11    # "gearConnectedTime":J
    .end local v13    # "tizenConnectedTime":J
    :cond_2
    :goto_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isActivityTrackerConnected(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v15, :cond_3

    .line 797
    const/4 v0, 0x5

    const/4 v1, 0x0

    aput v1, v7, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 801
    :cond_3
    if-eqz v6, :cond_4

    .line 802
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 805
    :cond_4
    return-object v7

    .line 791
    .restart local v11    # "gearConnectedTime":J
    .restart local v13    # "tizenConnectedTime":J
    :cond_5
    const/4 v0, 0x2

    const/16 v1, 0x8

    :try_start_4
    aput v1, v7, v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 738
    nop

    :array_0
    .array-data 4
        0x8
        0x8
        0x8
        0x8
        0x8
        0x8
        0x8
    .end array-data

    .line 752
    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_6
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public static getLoadUserDeviceName()Ljava/util/ArrayList;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v15, 0x2726

    const/16 v14, 0x2724

    const/16 v13, 0x2723

    const/4 v12, 0x2

    const/4 v11, 0x0

    .line 824
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getLoadUserDevice()[I

    move-result-object v2

    .line 825
    .local v2, "devices":[I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 826
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;>;"
    const/4 v1, 0x0

    .line 827
    .local v1, "cntGearDevices":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_0
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v5, v6, :cond_1

    .line 828
    aget v6, v2, v5

    if-nez v6, :cond_0

    .line 829
    add-int/lit8 v1, v1, 0x1

    .line 827
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 832
    :cond_1
    const-string v6, "Utils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Count of connected GEAR devices:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v4, v6, :cond_2

    .line 835
    if-lt v1, v12, :cond_5

    .line 836
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    const/16 v7, 0x272f

    const/16 v8, 0x272f

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090bbe

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 867
    :cond_2
    const/4 v6, 0x5

    aget v6, v2, v6

    if-nez v6, :cond_3

    .line 868
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    const/16 v7, 0x2727

    const/16 v8, 0x2727

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090bbd

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 870
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_4

    .line 871
    new-instance v3, Ljava/io/File;

    const-string v6, "/mnt/sdcard/Download/shealth"

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 872
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 873
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0907db

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v11, v11, v7}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 876
    .end local v3    # "file":Ljava/io/File;
    :cond_4
    return-object v0

    .line 839
    :cond_5
    aget v6, v2, v4

    if-nez v6, :cond_6

    .line 840
    packed-switch v4, :pswitch_data_0

    .line 860
    :pswitch_0
    const-string v6, "Utils"

    const-string v7, "getLoadUserDeviceName : default"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    :cond_6
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 842
    :pswitch_1
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090bb9

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v13, v13, v7}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 845
    :pswitch_2
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090bba

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v14, v14, v7}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 848
    :pswitch_3
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    const/16 v7, 0x2728

    const/16 v8, 0x2728

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090bba

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 851
    :pswitch_4
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f09061c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v15, v15, v7}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 854
    :pswitch_5
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    const/16 v7, 0x272e

    const/16 v8, 0x272e

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090bbb

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 857
    :pswitch_6
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    const/16 v7, 0x2730

    const/16 v8, 0x2730

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090bbc

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 840
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public static getMonthGroupDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 720
    new-instance v0, Ljava/lang/String;

    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 722
    .local v0, "dateFormatPattern":Ljava/lang/String;
    const-string v1, "d+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 723
    const-string v1, "M+"

    const-string v2, "MMM "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 724
    const-string/jumbo v1, "y+"

    const-string/jumbo v2, "yyyy "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 726
    const-string v1, " $"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 728
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public static getNotificationHRIntent()Landroid/app/PendingIntent;
    .locals 5

    .prologue
    .line 810
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 811
    .local v1, "intentHr":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 813
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v3, "widgetActivityAction"

    const-string v4, "com.sec.shealth.action.HEART_RATE"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 814
    const-string/jumbo v3, "widgetActivityPackage"

    const-string v4, "com.sec.android.app.shealth"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 815
    const-string v3, "HEARTRATE_WIDGET_CLICKED"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 816
    const-class v3, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 817
    const-string v3, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 818
    const/16 v3, 0x10e7

    const/high16 v4, 0x8000000

    invoke-static {v0, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 820
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    return-object v2
.end method

.method public static getRankingString(I)I
    .locals 5
    .param p0, "ranking"    # I

    .prologue
    .line 1079
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1081
    .local v1, "numStr":Ljava/lang/String;
    const-string v4, "(1[1-9])$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 1082
    .local v2, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1084
    .local v0, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1085
    const/4 v3, 0x4

    .line 1089
    .local v3, "rankStrType":I
    :goto_0
    return v3

    .line 1087
    .end local v3    # "rankStrType":I
    :cond_0
    rem-int/lit8 v3, p0, 0xa

    .restart local v3    # "rankStrType":I
    goto :goto_0
.end method

.method public static getSystemTimeDateFormat()Ljava/text/SimpleDateFormat;
    .locals 4

    .prologue
    .line 1229
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 1231
    .local v0, "pattern":Ljava/lang/String;
    const-string v1, "dd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1232
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1236
    :goto_0
    return-object v1

    .line 1233
    :cond_0
    const-string v1, "MM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1234
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MM/dd/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 1236
    :cond_1
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy/MM/dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0
.end method

.method public static getWearableSyncIntent(I)Landroid/content/Intent;
    .locals 6
    .param p0, "deviceType"    # I

    .prologue
    .line 1097
    const-string v0, ""

    .line 1098
    .local v0, "action":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1100
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "WalkingmateReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deviceType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    packed-switch p0, :pswitch_data_0

    .line 1157
    :cond_0
    :goto_0
    :pswitch_0
    const-string v3, "auto_sync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "action is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1158
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1159
    return-object v2

    .line 1105
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceTypewithDeviceId(Landroid/content/Context;I)I

    move-result v1

    .line 1106
    .local v1, "currentConnectedDevice":I
    if-eqz v1, :cond_0

    .line 1107
    const-string v3, "CURRENT_DEVICE_FOR_INTENT"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1108
    const/16 v3, 0x2723

    if-ne v3, v1, :cond_1

    .line 1109
    const-string v0, "com.samsung.android.shealth.ACTION_WINGTIP_SYNC"

    goto :goto_0

    .line 1110
    :cond_1
    const/16 v3, 0x2724

    if-ne v3, v1, :cond_2

    .line 1111
    const-string v0, "android.intent.action.PEDOMETER_SETTING_SYNC"

    goto :goto_0

    .line 1112
    :cond_2
    const/16 v3, 0x2728

    if-ne v3, v1, :cond_3

    .line 1113
    const-string v0, "com.samsung.android.shealth.ACTION_GEAR2_SYNC"

    goto :goto_0

    .line 1114
    :cond_3
    const/16 v3, 0x2726

    if-ne v3, v1, :cond_4

    .line 1115
    const-string v0, "com.samsung.android.shealth.ACTION_GEAR2_SYNC"

    goto :goto_0

    .line 1116
    :cond_4
    const/16 v3, 0x272e

    if-ne v3, v1, :cond_5

    .line 1117
    const-string v0, "com.samsung.android.shealth.ACTION_GEAR_SYNC"

    goto :goto_0

    .line 1118
    :cond_5
    const/16 v3, 0x2730

    if-ne v3, v1, :cond_0

    .line 1119
    const-string v0, "com.samsung.android.shealth.ACTION_GEAR_SYNC"

    .line 1120
    const-string v3, "DEVICE"

    const-string v4, "Gear O"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 1127
    .end local v1    # "currentConnectedDevice":I
    :pswitch_2
    const-string v0, "com.samsung.android.shealth.ACTION_WINGTIP_SYNC"

    .line 1128
    goto :goto_0

    .line 1131
    :pswitch_3
    const-string v0, "android.intent.action.PEDOMETER_SETTING_SYNC"

    .line 1132
    goto :goto_0

    .line 1135
    :pswitch_4
    const-string v0, "com.samsung.android.shealth.ACTION_GEAR2_SYNC"

    .line 1136
    goto :goto_0

    .line 1139
    :pswitch_5
    const-string v0, "com.samsung.android.shealth.ACTION_GEAR2_SYNC"

    .line 1140
    goto :goto_0

    .line 1143
    :pswitch_6
    const-string v0, "com.samsung.android.shealth.ACTION_GEAR_SYNC"

    .line 1144
    goto :goto_0

    .line 1147
    :pswitch_7
    const-string v0, "com.samsung.android.shealth.ACTION_GEAR_SYNC"

    .line 1148
    const-string v3, "DEVICE"

    const-string v4, "Gear O"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 1152
    :pswitch_8
    const-string v0, "com.samsung.android.shealth.ACTION_SBAND_SYNC"

    .line 1153
    goto :goto_0

    .line 1101
    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_8
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_1
        :pswitch_7
    .end packed-switch
.end method

.method public static getWeek_three_chars(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p0, "con"    # Landroid/content/Context;
    .param p1, "time"    # J

    .prologue
    .line 1249
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1250
    .local v0, "CAL":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1251
    const-string v1, ""

    .line 1252
    .local v1, "WEEKDAY":Ljava/lang/String;
    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1278
    :goto_0
    return-object v1

    .line 1254
    :pswitch_0
    const v2, 0x7f090d05

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1255
    goto :goto_0

    .line 1257
    :pswitch_1
    const v2, 0x7f090d06

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1258
    goto :goto_0

    .line 1260
    :pswitch_2
    const v2, 0x7f090d07

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1261
    goto :goto_0

    .line 1263
    :pswitch_3
    const v2, 0x7f090d08

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1264
    goto :goto_0

    .line 1266
    :pswitch_4
    const v2, 0x7f090d09

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1267
    goto :goto_0

    .line 1269
    :pswitch_5
    const v2, 0x7f090d0a

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1270
    goto :goto_0

    .line 1272
    :pswitch_6
    const v2, 0x7f090d0b

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1273
    goto :goto_0

    .line 1252
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static insertitem(Landroid/content/Context;IIIFJ)J
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "period"    # I
    .param p2, "goalType"    # I
    .param p3, "goalSubType"    # I
    .param p4, "value"    # F
    .param p5, "setTime"    # J

    .prologue
    .line 932
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 933
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "period"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 934
    const-string v3, "goal_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 935
    const-string v3, "goal_subtype"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 936
    const-string/jumbo v3, "value"

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 937
    const-string/jumbo v3, "set_time"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 938
    const-string/jumbo v3, "user_device__id"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "10009_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p0}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    const/4 v1, 0x0

    .line 941
    .local v1, "rawContactUri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 945
    :goto_0
    invoke-static {p0, p4}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->replaceGearGoalItem(Landroid/content/Context;F)V

    .line 946
    if-nez v1, :cond_0

    .line 947
    const-wide/16 v3, -0x1

    .line 948
    :goto_1
    return-wide v3

    .line 942
    :catch_0
    move-exception v0

    .line 943
    .local v0, "ie":Ljava/lang/IllegalArgumentException;
    const-string v3, "Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert exception -"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 948
    .end local v0    # "ie":Ljava/lang/IllegalArgumentException;
    :cond_0
    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v3

    goto :goto_1
.end method

.method public static isArabLocale()Z
    .locals 3

    .prologue
    .line 1302
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 1303
    .local v1, "language":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1304
    .local v0, "isArabLocale":Z
    if-eqz v1, :cond_0

    const-string v2, "ar"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1305
    const/4 v0, 0x1

    .line 1307
    :cond_0
    return v0
.end method

.method public static isChinaModel()Z
    .locals 1

    .prologue
    .line 180
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->China:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isCountryModel(Ljava/lang/String;)Z
    .locals 2
    .param p0, "country"    # Ljava/lang/String;

    .prologue
    .line 194
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLatinModel()Z
    .locals 3

    .prologue
    .line 188
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 189
    .local v1, "languageCode":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, "countryName":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, "es"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "US"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isNotUsedForAWeek()Z
    .locals 9

    .prologue
    .line 463
    const/4 v0, 0x0

    .line 464
    .local v0, "isNotUsed":Z
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->loadLatestDate()J

    move-result-wide v1

    .line 465
    .local v1, "latestDataTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v7

    sub-long v3, v5, v7

    .line 467
    .local v3, "timeInterval":J
    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-nez v5, :cond_1

    .line 468
    const/4 v0, 0x0

    .line 475
    :cond_0
    :goto_0
    return v0

    .line 470
    :cond_1
    const-wide/32 v5, 0x1ee62800

    cmp-long v5, v3, v5

    if-lez v5, :cond_0

    .line 471
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isUrduLocale()Z
    .locals 3

    .prologue
    .line 1312
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 1313
    .local v1, "language":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1314
    .local v0, "isUrduLocale":Z
    if-eqz v1, :cond_0

    const-string/jumbo v2, "ur"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1315
    const/4 v0, 0x1

    .line 1317
    :cond_0
    return v0
.end method

.method public static isWalkingDataExist()Z
    .locals 11

    .prologue
    .line 1195
    const/4 v9, 0x0

    .line 1196
    .local v9, "isExist":Z
    const/4 v6, 0x0

    .line 1197
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v7

    .line 1198
    .local v7, "deviceType":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 1199
    .local v10, "query":Ljava/lang/StringBuilder;
    const-string v0, "SELECT total_step"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1200
    const-string v0, " FROM "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1201
    const-string/jumbo v0, "walk_info"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1202
    const-string v0, " WHERE "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1203
    const-string/jumbo v0, "total_step > 0 "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1204
    const-string v0, " AND "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1205
    const-string/jumbo v0, "sync_status != 170004"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1206
    const-string v0, " AND "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207
    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1208
    const-string v0, " LIMIT 1 "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1211
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1212
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    .line 1214
    const/4 v9, 0x1

    .line 1219
    :cond_0
    if-eqz v6, :cond_1

    .line 1220
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1223
    :cond_1
    :goto_0
    return v9

    .line 1216
    :catch_0
    move-exception v8

    .line 1217
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1219
    if-eqz v6, :cond_1

    .line 1220
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1219
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1220
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static loadLatestDate()J
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 435
    const-wide/16 v7, 0x0

    .line 436
    .local v7, "latestDate":J
    const-string/jumbo v3, "start_time<? "

    .line 437
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v4, v2

    .line 439
    .local v4, "mSelectionArgs":[Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    .line 440
    const/4 v6, 0x0

    .line 442
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string/jumbo v5, "start_time desc "

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 443
    if-eqz v6, :cond_2

    .line 444
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 445
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 447
    const-string/jumbo v0, "start_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v7

    .line 455
    :goto_0
    if-eqz v6, :cond_0

    .line 456
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 459
    :cond_0
    return-wide v7

    .line 449
    :cond_1
    const-wide/16 v7, 0x0

    goto :goto_0

    .line 452
    :cond_2
    const-wide/16 v7, 0x0

    goto :goto_0

    .line 455
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 456
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static refreshGearGoal(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1015
    const/4 v7, 0x0

    .line 1016
    .local v7, "goalStep":I
    const/4 v6, 0x0

    .line 1018
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string/jumbo v3, "select value from goal left join user_device on goal.user_device__id == user_device._id where goal_type == 40001 and ifnull(device_type, 10009) == 10009 order by set_time desc limit 1"

    .line 1019
    .local v3, "query":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1020
    if-eqz v6, :cond_2

    .line 1021
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1023
    const-string/jumbo v0, "value"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 1033
    :goto_0
    if-eqz v6, :cond_0

    .line 1034
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1037
    :cond_0
    int-to-float v0, v7

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->replaceGearGoalItem(Landroid/content/Context;F)V

    .line 1038
    return-void

    .line 1027
    :cond_1
    const/16 v7, 0x2710

    goto :goto_0

    .line 1030
    :cond_2
    const/16 v7, 0x2710

    goto :goto_0

    .line 1033
    .end local v3    # "query":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 1034
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static replaceGearGoalItem(Landroid/content/Context;F)V
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # F

    .prologue
    .line 953
    const/4 v13, 0x0

    .line 954
    .local v13, "strGearID":Ljava/lang/String;
    const-string v3, "device_type == ? "

    .line 955
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, ""

    aput-object v1, v4, v0

    .line 957
    .local v4, "mSelectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    const/16 v1, 0x2724

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 958
    const/4 v11, 0x0

    .line 960
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 961
    if-eqz v11, :cond_0

    .line 962
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 963
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 965
    const-string v0, "_id"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 969
    :cond_0
    if-eqz v11, :cond_1

    .line 970
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 973
    :cond_1
    if-eqz v13, :cond_3

    .line 974
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 975
    .local v14, "values":Landroid/content/ContentValues;
    const-string/jumbo v0, "period"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 976
    const-string v0, "goal_type"

    const v1, 0x9c41

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 977
    const-string v0, "goal_subtype"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 978
    const-string/jumbo v0, "value"

    invoke-static/range {p1 .. p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 979
    const-string/jumbo v0, "set_time"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 980
    const-string/jumbo v0, "user_device__id"

    invoke-virtual {v14, v0, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    const-string/jumbo v8, "select goal._id from goal left join user_device on goal.user_device__id == user_device._id where goal_type == 40001 and ifnull(device_type, 10009) == 10020"

    .line 984
    .local v8, "query":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 985
    if-eqz v11, :cond_2

    .line 986
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 987
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_6

    .line 988
    const-string v3, "_id == ? "

    .line 989
    const/4 v0, 0x0

    const-string v1, "_id"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 991
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v14, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1007
    :cond_2
    :goto_0
    if-eqz v11, :cond_3

    .line 1008
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1012
    .end local v8    # "query":Ljava/lang/String;
    .end local v14    # "values":Landroid/content/ContentValues;
    :cond_3
    return-void

    .line 969
    :catchall_0
    move-exception v0

    if-eqz v11, :cond_4

    .line 970
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 992
    .restart local v8    # "query":Ljava/lang/String;
    .restart local v14    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v12

    .line 993
    .local v12, "ie":Ljava/lang/IllegalArgumentException;
    :try_start_3
    const-string v0, "Utils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insert exception -"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v12}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 1007
    .end local v12    # "ie":Ljava/lang/IllegalArgumentException;
    :catchall_1
    move-exception v0

    if-eqz v11, :cond_5

    .line 1008
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 999
    :cond_6
    :try_start_4
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v14}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 1000
    :catch_1
    move-exception v12

    .line 1001
    .restart local v12    # "ie":Ljava/lang/IllegalArgumentException;
    :try_start_5
    const-string v0, "Utils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insert exception -"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v12}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0
.end method

.method public static saveLastUpdatedTime(Z)V
    .locals 2
    .param p0, "isMyRanking"    # Z

    .prologue
    .line 711
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 712
    .local v0, "curTime":J
    if-eqz p0, :cond_0

    .line 713
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setMyRankingLastUpdatedTime(J)V

    .line 717
    :goto_0
    return-void

    .line 715
    :cond_0
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setLboardRankingLastUpdatedTime(J)V

    goto :goto_0
.end method

.method public static saveViewStepCountToMainSettingDB(I)V
    .locals 4
    .param p0, "viewStepCount"    # I

    .prologue
    .line 1291
    const-string v1, "SAVED_DEVICE"

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1294
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "shealth_pedometer_view_step_count"

    invoke-static {v1, v2, p0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1298
    :goto_0
    return-void

    .line 1295
    :catch_0
    move-exception v0

    .line 1296
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "WalkingMateMainDB"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Settings.System.getInt(shealth_pedometer_view_step_count):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
