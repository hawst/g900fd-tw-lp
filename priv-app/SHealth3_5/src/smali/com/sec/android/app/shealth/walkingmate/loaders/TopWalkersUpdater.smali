.class public abstract Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;
.super Landroid/os/AsyncTask;
.source "TopWalkersUpdater.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Intent;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private mJsonObj:Lorg/json/JSONObject;

.field private mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

.field private myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

.field private topTenList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 28
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->TAG:Ljava/lang/String;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->topTenList:Ljava/util/ArrayList;

    .line 36
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->DEFAULT:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    return-void
.end method

.method private initTopTenList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    .locals 7
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "clientId"    # Ljava/lang/String;
    .param p3, "deviceId"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 87
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 89
    .local v3, "json":Lorg/json/JSONObject;
    :try_start_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 90
    const-string/jumbo p4, "thisweek"

    .line 91
    :cond_0
    const-string/jumbo v4, "type"

    invoke-virtual {v3, v4, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 92
    const-string/jumbo v4, "pageNo"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 93
    const-string/jumbo v4, "pageSize"

    const/16 v5, 0xa

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_0
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, p3, p1, p2, v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getWalkingMateRanking(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mJsonObj:Lorg/json/JSONObject;

    .line 101
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mJsonObj:Lorg/json/JSONObject;

    if-eqz v4, :cond_1

    .line 102
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "json : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mJsonObj:Lorg/json/JSONObject;

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mJsonObj:Lorg/json/JSONObject;

    const-string/jumbo v5, "myRanking"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 105
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mJsonObj:Lorg/json/JSONObject;

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getMyRanking(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 107
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mJsonObj:Lorg/json/JSONObject;

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getWorldRanking(Lorg/json/JSONObject;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->topTenList:Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_5

    .line 130
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->topTenList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_4

    .line 131
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 134
    :goto_2
    return-object v4

    .line 94
    :catch_0
    move-exception v2

    .line 96
    .local v2, "e1":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 109
    .end local v2    # "e1":Lorg/json/JSONException;
    :catch_1
    move-exception v1

    .line 110
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 111
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SERVER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 112
    const-string v4, "SERVER_ERROR"

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    goto :goto_1

    .line 113
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 114
    .local v1, "e":Ljava/net/URISyntaxException;
    invoke-virtual {v1}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 115
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SERVER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    goto :goto_1

    .line 116
    .end local v1    # "e":Ljava/net/URISyntaxException;
    :catch_3
    move-exception v1

    .line 117
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 118
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SERVER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    goto :goto_1

    .line 119
    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v0

    .line 120
    .local v0, "catchMeIfYouCan":Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;->getStatusCode()I

    move-result v4

    const/16 v5, 0x191

    if-ne v4, v5, :cond_3

    .line 121
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->INVALID_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    goto :goto_1

    .line 123
    :cond_3
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SERVER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    goto :goto_1

    .line 125
    .end local v0    # "catchMeIfYouCan":Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
    :catch_5
    move-exception v1

    .line 126
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 127
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SERVER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    goto :goto_1

    .line 134
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_4
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SUCCESS:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    goto :goto_2
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, [Landroid/content/Intent;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->doInBackground([Landroid/content/Intent;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/content/Intent;)Ljava/lang/Void;
    .locals 11
    .param p1, "params"    # [Landroid/content/Intent;

    .prologue
    const/4 v10, 0x0

    .line 43
    aget-object v5, p1, v10

    .line 44
    .local v5, "intent":Landroid/content/Intent;
    const-string v7, "access_token"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "accessToken":Ljava/lang/String;
    const-string v7, "client_id"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 47
    .local v3, "clientId":Ljava/lang/String;
    const-string/jumbo v7, "type"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 48
    .local v6, "type":Ljava/lang/String;
    const-string v7, "device_id"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 50
    .local v4, "deviceId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 51
    .local v1, "attempts":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->isCancelled()Z

    move-result v7

    if-nez v7, :cond_0

    const/4 v7, 0x1

    if-ge v1, v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    sget-object v8, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SUCCESS:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-eq v7, v8, :cond_0

    .line 55
    :try_start_0
    invoke-direct {p0, v0, v3, v4, v6}, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->initTopTenList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "doInBackground - mRequestResult : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 56
    :catch_0
    move-exception v2

    .line 57
    .local v2, "catchMeIfYouCan":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 59
    sget-object v7, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->OTHER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    iput-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    goto :goto_1

    .line 64
    .end local v2    # "catchMeIfYouCan":Ljava/lang/Exception;
    :cond_0
    new-array v7, v10, [Ljava/lang/Void;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->publishProgress([Ljava/lang/Object;)V

    .line 65
    const/4 v7, 0x0

    return-object v7
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onProgressUpdate - mRequestResult : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->topTenList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->onUpdate(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)V

    .line 73
    return-void
.end method

.method public abstract onUpdate(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            ">;",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            "Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;",
            ")V"
        }
    .end annotation
.end method
