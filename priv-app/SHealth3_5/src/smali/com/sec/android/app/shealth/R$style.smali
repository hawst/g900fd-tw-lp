.class public final Lcom/sec/android/app/shealth/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final ActionButton:I = 0x7f0c002a

.field public static final AppBaseTheme:I = 0x7f0c0000

.field public static final AppTheme:I = 0x7f0c0001

.field public static final CustomActionBar:I = 0x7f0c001a

.field public static final CustomActionBarTheme:I = 0x7f0c0091

.field public static final CustomActionOverflowDropDownText:I = 0x7f0c0024

.field public static final CustomActionOverflowDropDownText_HS:I = 0x7f0c000a

.field public static final CustomAnimation:I = 0x7f0c0020

.field public static final Dialog_HS:I = 0x7f0c0003

.field public static final Dialog_NoTitleBar_And_Frame:I = 0x7f0c0010

.field public static final Dialog_NoTitleBar_And_Frame_HS:I = 0x7f0c0004

.field public static final Error_Dialog_NoTitleBar_And_Frame:I = 0x7f0c0082

.field public static final ExerciseLogTheme:I = 0x7f0c0092

.field public static final HelpBGShdow:I = 0x7f0c00a2

.field public static final HelpListItem:I = 0x7f0c00bf

.field public static final HelpListItemImage:I = 0x7f0c00bd

.field public static final HelpListItemNumber:I = 0x7f0c00c0

.field public static final HelpListItemNumber0:I = 0x7f0c00c1

.field public static final HelpListItemNumberRTL:I = 0x7f0c00c8

.field public static final HelpListItem_HelphubTextView:I = 0x7f0c00c2

.field public static final HelpListItem_HelphubTextView_NoLeftMargin:I = 0x7f0c00c3

.field public static final HelpListItem_HelphubTextView_NoMargin:I = 0x7f0c00c4

.field public static final HelpListItem_HelphubTextView_NoMargin_Favorites:I = 0x7f0c00c5

.field public static final HelpListItem_NoLeftMargin:I = 0x7f0c00c6

.field public static final Information_Dialog_NoTitleBar_And_Frame:I = 0x7f0c0081

.field public static final ListFont:I = 0x7f0c000f

.field public static final ListSelector:I = 0x7f0c0023

.field public static final ListSelector_HS:I = 0x7f0c0009

.field public static final MenuPopupMenu:I = 0x7f0c0022

.field public static final MenuPopupMenu_HS:I = 0x7f0c0008

.field public static final NumberedList:I = 0x7f0c00be

.field public static final OverFlow:I = 0x7f0c0029

.field public static final SCover_Theme:I = 0x7f0c00c7

.field public static final SHealthHome:I = 0x7f0c001f

.field public static final SHealthSplash:I = 0x7f0c001e

.field public static final SHealth_ActionMode_Style:I = 0x7f0c0025

.field public static final SHealth_ActionMode_Style_HS:I = 0x7f0c000b

.field public static final SHealth_Style_DeviceDefault:I = 0x7f0c001d

.field public static final SHealth_Style_DeviceDefault_ActionBarOverlay:I = 0x7f0c0021

.field public static final SHealth_Style_DeviceDefault_HS:I = 0x7f0c0006

.field public static final SHealth_Style_DeviceDefault_menu_bg_white:I = 0x7f0c0086

.field public static final SHealth_Style_Transparent:I = 0x7f0c00aa

.field public static final Shealthservice_Style_ALertDialog_Theme_HS:I = 0x7f0c0007

.field public static final Theme_ActionBar_Main_ActionBarStyle:I = 0x7f0c0026

.field public static final Theme_ActionBar_Main_ActionBarStyle_HS:I = 0x7f0c000c

.field public static final Theme_ActionBar_Main_ActionBarStyle_Transparent:I = 0x7f0c0028

.field public static final Theme_ActionBar_Main_ActionBarStyle_Transparent_HS:I = 0x7f0c000e

.field public static final Theme_ActionBar_Main_ActionBar_TitleTextStyle:I = 0x7f0c0027

.field public static final Theme_ActionBar_Main_ActionBar_TitleTextStyle_HS:I = 0x7f0c000d

.field public static final UV_Dialog_NoTitleBar_And_Frame:I = 0x7f0c0083

.field public static final Widget_GIFView:I = 0x7f0c0040

.field public static final action_bar_text_style:I = 0x7f0c001c

.field public static final actionbar_ok_cancel_btn:I = 0x7f0c003d

.field public static final actionbar_text_btn:I = 0x7f0c003e

.field public static final boldType:I = 0x7f0c0019

.field public static final calendar_NoTitleBar_And_Frame:I = 0x7f0c001b

.field public static final chooser_dialog_list_item:I = 0x7f0c0011

.field public static final cigna_regular_light:I = 0x7f0c00a8

.field public static final cigna_regular_normal:I = 0x7f0c00a7

.field public static final cigna_regular_thin:I = 0x7f0c00a9

.field public static final circle_progress_view:I = 0x7f0c0093

.field public static final common_ab_spn:I = 0x7f0c003c

.field public static final common_btn:I = 0x7f0c0038

.field public static final common_btn_general:I = 0x7f0c0032

.field public static final common_cbx:I = 0x7f0c003a

.field public static final common_cbx_general:I = 0x7f0c0036

.field public static final common_et:I = 0x7f0c0037

.field public static final common_et_general:I = 0x7f0c0031

.field public static final common_rb:I = 0x7f0c0039

.field public static final common_rb_general:I = 0x7f0c0035

.field public static final common_spn:I = 0x7f0c003b

.field public static final common_spn_general:I = 0x7f0c0030

.field public static final common_spn_txt_general:I = 0x7f0c002f

.field public static final common_wgt_general:I = 0x7f0c002e

.field public static final customProgressBar:I = 0x7f0c008f

.field public static final default_checkbox:I = 0x7f0c0005

.field public static final default_checkbox_style:I = 0x7f0c0097

.field public static final default_common_edit_text_single_line:I = 0x7f0c0017

.field public static final default_edit_text_single_line:I = 0x7f0c0015

.field public static final default_edit_text_single_line_walk_input_module:I = 0x7f0c0016

.field public static final detected_accessories_title_list_item:I = 0x7f0c002c

.field public static final dialogCustomTheme_HS:I = 0x7f0c0002

.field public static final drop_down_list_style:I = 0x7f0c0018

.field public static final font_roboto_light:I = 0x7f0c0066

.field public static final food_add_custom_edit_text:I = 0x7f0c0065

.field public static final food_add_custom_food_text:I = 0x7f0c0052

.field public static final food_default_checkbox:I = 0x7f0c004b

.field public static final food_default_edit_text_single_line:I = 0x7f0c0043

.field public static final food_dialog_style:I = 0x7f0c0054

.field public static final food_image_holder_margins:I = 0x7f0c0057

.field public static final food_image_size_large_l:I = 0x7f0c0060

.field public static final food_image_size_large_m:I = 0x7f0c005f

.field public static final food_image_size_large_s:I = 0x7f0c005e

.field public static final food_image_size_large_xl:I = 0x7f0c0061

.field public static final food_image_size_medium_l:I = 0x7f0c005d

.field public static final food_image_size_medium_m:I = 0x7f0c005c

.field public static final food_image_size_medium_s:I = 0x7f0c005b

.field public static final food_image_size_small_m:I = 0x7f0c0059

.field public static final food_image_size_small_s:I = 0x7f0c0058

.field public static final food_meal_plan_day_of_week_check_box:I = 0x7f0c0051

.field public static final food_notes_edit_text:I = 0x7f0c0042

.field public static final food_notes_text_view:I = 0x7f0c0041

.field public static final food_nutrition_info_split_line_base:I = 0x7f0c0044

.field public static final food_nutrition_info_split_line_base_with_margin_black:I = 0x7f0c0045

.field public static final food_nutrition_info_split_line_base_with_margin_grey:I = 0x7f0c0046

.field public static final food_nutrition_info_split_line_base_with_margin_light_grey:I = 0x7f0c0047

.field public static final food_nutrition_info_split_line_bold_with_margin:I = 0x7f0c0049

.field public static final food_nutrition_info_split_line_bold_with_margin_grey:I = 0x7f0c004a

.field public static final food_nutrition_info_split_line_thin_with_margin:I = 0x7f0c0048

.field public static final food_nutrition_info_sub_text_view_style:I = 0x7f0c0050

.field public static final food_nutrition_value_info_main_text_view_style:I = 0x7f0c004c

.field public static final food_nutrition_value_info_main_text_view_style_light_margin_left:I = 0x7f0c004d

.field public static final food_nutrition_value_info_main_text_view_style_text_style_light:I = 0x7f0c004f

.field public static final food_nutrition_value_info_main_text_view_style_text_wrap_enabled:I = 0x7f0c004e

.field public static final food_pick_list_item_product_calories_text_view:I = 0x7f0c0056

.field public static final food_pick_list_item_product_name_text_view:I = 0x7f0c0055

.field public static final food_pick_search_fragment_buttons:I = 0x7f0c0062

.field public static final food_planed_eat_meal_button_container:I = 0x7f0c0063

.field public static final food_planed_eat_meal_buttons:I = 0x7f0c0064

.field public static final food_title_style_of_text_view:I = 0x7f0c0053

.field public static final hrm_bold:I = 0x7f0c009f

.field public static final hrm_bold_green:I = 0x7f0c00a1

.field public static final hrm_bold_white:I = 0x7f0c00a0

.field public static final hrm_circle_progress_view:I = 0x7f0c00ad

.field public static final hrm_light:I = 0x7f0c0096

.field public static final hrm_light_green:I = 0x7f0c009b

.field public static final hrm_light_white:I = 0x7f0c009a

.field public static final hrm_list_category_divider:I = 0x7f0c00ab

.field public static final hrm_list_divider:I = 0x7f0c0095

.field public static final hrm_log_list_divider:I = 0x7f0c00af

.field public static final hrm_medium:I = 0x7f0c009c

.field public static final hrm_medium_green:I = 0x7f0c009e

.field public static final hrm_medium_white:I = 0x7f0c009d

.field public static final hrm_regular:I = 0x7f0c00ac

.field public static final hrm_scover_circle_progress_view:I = 0x7f0c0094

.field public static final image_size_small_l:I = 0x7f0c005a

.field public static final init_widget_title_font:I = 0x7f0c0034

.field public static final myCustomMenuTextApearance:I = 0x7f0c0085

.field public static final no_actionbar:I = 0x7f0c003f

.field public static final pro_exercise_circle_progress_view:I = 0x7f0c00ae

.field public static final profile_checkbox:I = 0x7f0c00a3

.field public static final profile_information:I = 0x7f0c0090

.field public static final profile_information_button:I = 0x7f0c008e

.field public static final profile_sub_title_textview:I = 0x7f0c008a

.field public static final regular_bold:I = 0x7f0c0033

.field public static final regular_light:I = 0x7f0c0014

.field public static final regular_normal:I = 0x7f0c0013

.field public static final roboto_Light:I = 0x7f0c007f

.field public static final samsung_neo_num:I = 0x7f0c0080

.field public static final settings_list_item:I = 0x7f0c0088

.field public static final settings_title_accessory_list_item:I = 0x7f0c0087

.field public static final settings_title_list_item:I = 0x7f0c002b

.field public static final settings_title_list_item_2:I = 0x7f0c002d

.field public static final shealth_checkbox:I = 0x7f0c0098

.field public static final shealth_checkbox_regular:I = 0x7f0c0099

.field public static final sleep_list_divider:I = 0x7f0c0067

.field public static final sleep_rating_bar:I = 0x7f0c0068

.field public static final spo2_circle_progress_view:I = 0x7f0c006b

.field public static final spo2_light:I = 0x7f0c0069

.field public static final spo2_list_divider:I = 0x7f0c006c

.field public static final spo2_log_list_divider:I = 0x7f0c006d

.field public static final spo2_regular:I = 0x7f0c006a

.field public static final stm_bold:I = 0x7f0c007a

.field public static final stm_bold_green:I = 0x7f0c007c

.field public static final stm_bold_white:I = 0x7f0c007b

.field public static final stm_circle_progress_view:I = 0x7f0c007d

.field public static final stm_light:I = 0x7f0c0074

.field public static final stm_light_green:I = 0x7f0c0076

.field public static final stm_light_white:I = 0x7f0c0075

.field public static final stm_list_category_divider:I = 0x7f0c006e

.field public static final stm_list_divider:I = 0x7f0c006f

.field public static final stm_log_list_divider:I = 0x7f0c007e

.field public static final stm_medium:I = 0x7f0c0077

.field public static final stm_medium_green:I = 0x7f0c0079

.field public static final stm_medium_white:I = 0x7f0c0078

.field public static final stm_regular:I = 0x7f0c0071

.field public static final stm_regular_green:I = 0x7f0c0073

.field public static final stm_regular_white:I = 0x7f0c0072

.field public static final summary_view_update_button_style:I = 0x7f0c0012

.field public static final terms_button:I = 0x7f0c008d

.field public static final terms_of_use_content:I = 0x7f0c008b

.field public static final terms_text_view:I = 0x7f0c008c

.field public static final tgh_light:I = 0x7f0c00c9

.field public static final uv_list_category_divider:I = 0x7f0c0084

.field public static final uv_list_divider:I = 0x7f0c0070

.field public static final uv_skin_list_item:I = 0x7f0c0089

.field public static final wgt_robo_light_text_view:I = 0x7f0c00a5

.field public static final wgt_settings_radio_item:I = 0x7f0c00a4

.field public static final wgt_summary_view_text:I = 0x7f0c00a6

.field public static final workout_default_search_text_single_line:I = 0x7f0c00b1

.field public static final workout_health_care_button:I = 0x7f0c00b0

.field public static final workout_image_holder_margins:I = 0x7f0c00b2

.field public static final workout_image_size_large_l:I = 0x7f0c00bb

.field public static final workout_image_size_large_m:I = 0x7f0c00ba

.field public static final workout_image_size_large_s:I = 0x7f0c00b9

.field public static final workout_image_size_large_xl:I = 0x7f0c00bc

.field public static final workout_image_size_medium_l:I = 0x7f0c00b8

.field public static final workout_image_size_medium_m:I = 0x7f0c00b7

.field public static final workout_image_size_medium_s:I = 0x7f0c00b6

.field public static final workout_image_size_small_l:I = 0x7f0c00b5

.field public static final workout_image_size_small_m:I = 0x7f0c00b4

.field public static final workout_image_size_small_s:I = 0x7f0c00b3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
