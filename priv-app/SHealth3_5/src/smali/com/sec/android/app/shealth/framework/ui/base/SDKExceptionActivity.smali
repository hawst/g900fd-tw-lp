.class public Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;
.super Landroid/support/v4/app/FragmentActivity;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$3;,
        Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$UpdateServiceBackPressController;,
        Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$UpdateServiceDialogButtonController;
    }
.end annotation


# static fields
.field private static UPDATE_SERVICE:Ljava/lang/String;


# instance fields
.field private final mBackPressMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;",
            ">;"
        }
    .end annotation
.end field

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xe

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, -0x26ba

    aput v15, v1, v14

    const/16 v14, -0x46

    aput v14, v1, v13

    const/16 v13, -0x42

    aput v13, v1, v12

    const/16 v12, -0x37

    aput v12, v1, v11

    const/16 v11, -0x56

    aput v11, v1, v10

    const/16 v10, 0x112c

    aput v10, v1, v9

    const/16 v9, 0x1e62

    aput v9, v1, v8

    const/16 v8, -0x49c2

    aput v8, v1, v7

    const/16 v7, -0x2d

    aput v7, v1, v6

    const/16 v6, -0x55

    aput v6, v1, v5

    const/16 v5, -0x57d7

    aput v5, v1, v4

    const/16 v4, -0x34

    aput v4, v1, v3

    const/16 v3, 0x266e

    aput v3, v1, v2

    const/16 v2, -0x67ad

    aput v2, v1, v0

    const/16 v0, 0xe

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, -0x26dd

    aput v16, v0, v15

    const/16 v15, -0x27

    aput v15, v0, v14

    const/16 v14, -0x29

    aput v14, v0, v13

    const/16 v13, -0x41

    aput v13, v0, v12

    const/16 v12, -0x28

    aput v12, v0, v11

    const/16 v11, 0x1149

    aput v11, v0, v10

    const/16 v10, 0x1e11

    aput v10, v0, v9

    const/16 v9, -0x49e2

    aput v9, v0, v8

    const/16 v8, -0x4a

    aput v8, v0, v7

    const/16 v7, -0x21

    aput v7, v0, v6

    const/16 v6, -0x57b8

    aput v6, v0, v5

    const/16 v5, -0x58

    aput v5, v0, v4

    const/16 v4, 0x261e

    aput v4, v0, v3

    const/16 v3, -0x67da

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;->UPDATE_SERVICE:Ljava/lang/String;

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;->mDialogControllerMap:Ljava/util/Map;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;->mBackPressMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getBackPressController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;->mBackPressMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;

    return-object v0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 20

    invoke-super/range {p0 .. p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->blank_layout_for_popup:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;->setContentView(I)V

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->health_service:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->msg_popup_update:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->ok:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x26e3

    aput v18, v2, v17

    const/16 v17, -0x46

    aput v17, v2, v16

    const/16 v16, -0x3de6

    aput v16, v2, v15

    const/16 v15, -0x4c

    aput v15, v2, v14

    const/16 v14, -0x3cbd

    aput v14, v2, v13

    const/16 v13, -0x5a

    aput v13, v2, v12

    const/16 v12, -0x17

    aput v12, v2, v11

    const/16 v11, -0x3f

    aput v11, v2, v10

    const/16 v10, -0x38

    aput v10, v2, v9

    const/16 v9, -0x2cc1

    aput v9, v2, v8

    const/16 v8, -0x4e

    aput v8, v2, v7

    const/16 v7, -0x5d

    aput v7, v2, v6

    const/16 v6, 0x1b55

    aput v6, v2, v3

    const/16 v3, -0x7392

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, -0x2688

    aput v19, v1, v18

    const/16 v18, -0x27

    aput v18, v1, v17

    const/16 v17, -0x3d8d

    aput v17, v1, v16

    const/16 v16, -0x3e

    aput v16, v1, v15

    const/16 v15, -0x3ccf

    aput v15, v1, v14

    const/16 v14, -0x3d

    aput v14, v1, v13

    const/16 v13, -0x66

    aput v13, v1, v12

    const/16 v12, -0x1f

    aput v12, v1, v11

    const/16 v11, -0x53

    aput v11, v1, v10

    const/16 v10, -0x2cb5

    aput v10, v1, v9

    const/16 v9, -0x2d

    aput v9, v1, v8

    const/16 v8, -0x39

    aput v8, v1, v7

    const/16 v7, 0x1b25

    aput v7, v1, v6

    const/16 v6, -0x73e5

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v6, v1

    if-lt v3, v6, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v6, v1

    if-lt v3, v6, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method
