.class public Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;
.super Ljava/lang/Object;
.source "HeartrateTagUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field static final mHeartrateTag:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->mHeartrateTag:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag;

    .line 23
    const-class v0, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getIconNameId(I)I
    .locals 4
    .param p0, "tagIconId"    # I

    .prologue
    .line 108
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$3;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$3;-><init>()V

    .line 144
    .local v0, "iconMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 148
    :goto_0
    return v1

    .line 147
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown tag name id("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")is encountered."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const v1, 0x7f0910e8

    goto :goto_0
.end method

.method public static getIconResourceId(I)I
    .locals 4
    .param p0, "tagIconId"    # I

    .prologue
    .line 61
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$2;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$2;-><init>()V

    .line 97
    .local v0, "iconMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 101
    :goto_0
    return v1

    .line 100
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown tag icon id("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")is encountered."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const v1, 0x7f0204e6

    goto :goto_0
.end method

.method public static getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .locals 2
    .param p0, "tagId"    # I

    .prologue
    .line 26
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;-><init>()V

    .line 47
    .local v0, "DefaultTag":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    return-object v1
.end method

.method public static getTag(IILjava/lang/String;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .locals 6
    .param p0, "tagId"    # I
    .param p1, "tagIconId"    # I
    .param p2, "tagName"    # Ljava/lang/String;

    .prologue
    .line 52
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_USER_ADDED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    invoke-static {p1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconResourceId(I)I

    move-result v3

    move v2, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;IIILjava/lang/String;)V

    .line 53
    .local v0, "retTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    if-eqz p2, :cond_0

    const/16 v1, 0x57e7

    if-le p1, v1, :cond_0

    .line 54
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->TAG:Ljava/lang/String;

    const-string v2, "Tag icon id is used same as the tag icon resource id."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .end local v0    # "retTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_USER_ADDED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    move v2, p0

    move v3, p1

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;IIILjava/lang/String;)V

    .line 57
    .restart local v0    # "retTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :cond_0
    return-object v0
.end method

.method public static updateRecentTagPreference(ILandroid/content/Context;)V
    .locals 6
    .param p0, "tagId"    # I
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 155
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 157
    .local v2, "tagIdKeyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/16 v3, 0x5208

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    const/16 v3, 0x5335

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    const/16 v3, 0x526d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    const/16 v3, 0x526f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 164
    .local v1, "mPref":Landroid/content/SharedPreferences;
    if-lez p0, :cond_0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 165
    const-string v3, "first_r_tag"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne p0, v3, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    const-string/jumbo v3, "second_r_tag"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne p0, v3, :cond_3

    .line 169
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 170
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "first_r_tag"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-lez v3, :cond_2

    .line 171
    const-string/jumbo v3, "second_r_tag"

    const-string v4, "first_r_tag"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 172
    :cond_2
    const-string v3, "first_r_tag"

    invoke-interface {v0, v3, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 173
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 177
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_3
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 178
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v3, "second_r_tag"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-lez v3, :cond_4

    .line 179
    const-string/jumbo v3, "third_r_tag"

    const-string/jumbo v4, "second_r_tag"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 180
    :cond_4
    const-string v3, "first_r_tag"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-lez v3, :cond_5

    .line 181
    const-string/jumbo v3, "second_r_tag"

    const-string v4, "first_r_tag"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 182
    :cond_5
    const-string v3, "first_r_tag"

    invoke-interface {v0, v3, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 183
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
