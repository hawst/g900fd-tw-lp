.class public Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "HeartrateActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$IOnDismissListener;
.implements Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;
.implements Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;


# static fields
.field private static final CHOOSE_TAG:I = 0x93

.field private static DEFAULT_TAG_SET:Ljava/lang/String; = null

.field private static final INFO_DIAG_TAG:Ljava/lang/String; = "hrm_information_dialog"

.field private static final SHOW_INFORMATION_DIALOG:I = 0x1

.field private static final SHOW_INFORMATION_DIALOG_TIME:J = 0xfaL

.field private static final SHOW_TAG_DIALOG:I = 0x1

.field private static final SHOW_TAG_DIALOG_TIME:J = 0xc8L

.field private static TAG:Ljava/lang/String;

.field public static isDialogDissmised:Z


# instance fields
.field private InfoDiagContentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

.field private actionbarClickListener:Landroid/view/View$OnClickListener;

.field againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field animationFadeIn:Landroid/view/animation/AnimationSet;

.field animationFadeOut:Landroid/view/animation/AnimationSet;

.field backFinish:Z

.field private callFromHome:Z

.field private doNotShowCheckBox:Landroid/widget/CheckBox;

.field fadeIn:Landroid/view/animation/Animation;

.field fadeOut:Landroid/view/animation/Animation;

.field handler:Landroid/os/Handler;

.field private informationImages:[I

.field private informationImagesArray:[Ljava/lang/String;

.field public isAutoStartup:Z

.field public isDialogshown:Z

.field isImage1Found:Z

.field isImage2Found:Z

.field public isMeasureCompleted:Z

.field private mAnimationViewInfo0:Landroid/widget/ImageView;

.field private mAnimationViewInfo1:Landroid/widget/ImageView;

.field mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

.field private mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mDeviceTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mDialogHandler:Landroid/os/Handler;

.field private mGraphFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

.field private mHeartRateData:I

.field private mInfoDiagDismissHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

.field private mInfoView:Landroid/view/View;

.field private mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field public mIsActivityOnConfigChanged:Z

.field private mNoSensorInfoDiag:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mOnCheckedChanged:Z

.field private mPref:Landroid/content/SharedPreferences;

.field private mShowAgain:Landroid/widget/CheckBox;

.field private mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

.field private mShowAgainNosensorCheckLayout:Landroid/widget/RelativeLayout;

.field private mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

.field private mTextView1:Landroid/widget/TextView;

.field private mTextView11:Landroid/widget/TextView;

.field private mTextView2:Landroid/widget/TextView;

.field private mTextView22:Landroid/widget/TextView;

.field private mTextView3:Landroid/widget/TextView;

.field private mTextView33:Landroid/widget/TextView;

.field private mTextView4:Landroid/widget/TextView;

.field private mTextView44:Landroid/widget/TextView;

.field private mTextView5:Landroid/widget/TextView;

.field private mTextView55:Landroid/widget/TextView;

.field private mTextViewCb:Landroid/widget/TextView;

.field private mTextViewClinicalInfo:Landroid/widget/TextView;

.field private tagItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 104
    const-class v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->TAG:Ljava/lang/String;

    .line 123
    const-string v0, "default_tag_set"

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->DEFAULT_TAG_SET:Ljava/lang/String;

    .line 142
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDialogDissmised:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 116
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isAutoStartup:Z

    .line 117
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    .line 118
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->callFromHome:Z

    .line 120
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mIsActivityOnConfigChanged:Z

    .line 136
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mOnCheckedChanged:Z

    .line 141
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isImage1Found:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isImage2Found:Z

    .line 143
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImages:[I

    .line 145
    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "hr_guide_t_1"

    aput-object v1, v0, v3

    const-string v1, "hr_guide_t_2"

    aput-object v1, v0, v2

    const/4 v1, 0x2

    const-string v2, "hr_guide_t_3"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hr_guide_t_4"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "hr_guide_t_5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "hr_guide_t_6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "hr_guide_t_5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "hr_guide_t_6"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImagesArray:[Ljava/lang/String;

    .line 146
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mHeartRateData:I

    .line 241
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$1;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDialogHandler:Landroid/os/Handler;

    .line 254
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$2;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->handler:Landroid/os/Handler;

    .line 679
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$12;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    .line 749
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->backFinish:Z

    .line 807
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$14;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    .line 885
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$17;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->InfoDiagContentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    .line 956
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoDiagDismissHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    .line 1151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->tagItemList:Ljava/util/ArrayList;

    .line 1190
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$24;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$24;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void

    .line 143
    :array_0
    .array-data 4
        0x7f0201fa
        0x7f0201fb
        0x7f0201fc
        0x7f0201fd
        0x7f0201fe
        0x7f0201ff
        0x7f0201fe
        0x7f0201ff
    .end array-data
.end method

.method private OnConnectivityActivity()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 713
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeviceTypes:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 715
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeviceTypes:Ljava/util/ArrayList;

    .line 718
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v2, 0x2728

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 719
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v2, 0x2726

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 720
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v2, 0x2723

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 721
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v2, 0x272e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 723
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 724
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DATA_TYPE_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 725
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->CONNECTIVITY_TYPE_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 726
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HEADER_TEXT_ID:Ljava/lang/String;

    const v2, 0x7f090c2a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 727
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_TYPE_INFO_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v2, 0x7f090c2b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 728
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->NO_DEVICES_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v2, 0x7f090092

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 729
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DEVICE_TYPES_KEY:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeviceTypes:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 730
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->startActivity(Landroid/content/Intent;)V

    .line 731
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showNoSensorInformationDialog()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/app/Activity;
    .param p3, "x3"    # Landroid/app/Dialog;
    .param p4, "x4"    # Landroid/os/Bundle;
    .param p5, "x5"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 102
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->infoDiagContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->infoDiagOKBtnHandler()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoDiagDismissHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showSummarayFragment()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    .param p1, "x1"    # Landroid/widget/ImageView;
    .param p2, "x2"    # Landroid/widget/ImageView;
    .param p3, "x3"    # [I
    .param p4, "x4"    # [Ljava/lang/String;
    .param p5, "x5"    # I
    .param p6, "x6"    # Z

    .prologue
    .line 102
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showInfomationDialog()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgainNosensorCheckLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    .param p1, "x1"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgainNosensorCheckLayout:Landroid/widget/RelativeLayout;

    return-object p1
.end method

.method static synthetic access$2102(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mOnCheckedChanged:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showDeleteDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->OnConnectivityActivity()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mGraphFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->prepareShareView()V

    return-void
.end method

.method private animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    .locals 12
    .param p1, "imageView1"    # Landroid/widget/ImageView;
    .param p2, "imageView2"    # Landroid/widget/ImageView;
    .param p3, "images"    # [I
    .param p4, "ImageArr"    # [Ljava/lang/String;
    .param p5, "imageIndex"    # I
    .param p6, "forever"    # Z

    .prologue
    .line 1022
    sget-boolean v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDialogDissmised:Z

    if-eqz v1, :cond_0

    .line 1106
    :goto_0
    return-void

    .line 1024
    :cond_0
    move/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->initFadingAnimationEffect(I)V

    .line 1025
    invoke-virtual {p1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1026
    .local v9, "ImgTag1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1027
    .local v10, "ImgTag2":Ljava/lang/String;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isImage1Found:Z

    .line 1028
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isImage2Found:Z

    .line 1029
    aget-object v1, p4, p5

    invoke-virtual {v9, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1030
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isImage1Found:Z

    .line 1031
    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1032
    invoke-virtual {p2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1033
    array-length v1, p3

    add-int/lit8 v1, v1, -0x1

    move/from16 v0, p5

    if-ne v1, v0, :cond_3

    .line 1034
    const/4 v1, 0x0

    aget v1, p3, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1035
    const/4 v1, 0x0

    aget-object v1, p4, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1040
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1041
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1043
    :cond_1
    aget-object v1, p4, p5

    invoke-virtual {v10, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1044
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isImage2Found:Z

    .line 1045
    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1046
    invoke-virtual {p2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1047
    array-length v1, p3

    add-int/lit8 v1, v1, -0x1

    move/from16 v0, p5

    if-ne v0, v1, :cond_4

    .line 1049
    const/4 v1, 0x0

    aget v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1050
    const/4 v1, 0x0

    aget-object v1, p4, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1055
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1056
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1059
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$19;

    invoke-direct {v2, p0, p2, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$19;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1079
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$20;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move/from16 v6, p5

    move-object/from16 v7, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$20;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;[II[Ljava/lang/String;Z)V

    invoke-virtual {v11, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto/16 :goto_0

    .line 1037
    :cond_3
    add-int/lit8 v1, p5, 0x1

    aget v1, p3, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1038
    add-int/lit8 v1, p5, 0x1

    aget-object v1, p4, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    .line 1052
    :cond_4
    add-int/lit8 v1, p5, 0x1

    aget v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1053
    add-int/lit8 v1, p5, 0x1

    aget-object v1, p4, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private displayInformationDialog()V
    .locals 4

    .prologue
    .line 264
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$3;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 270
    return-void
.end method

.method private infoDiagContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 4
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const v3, 0x7f0804ef

    .line 912
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    .line 913
    const/4 v0, 0x0

    .line 914
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgain:Landroid/widget/CheckBox;

    if-eqz v1, :cond_0

    .line 915
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 917
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgain:Landroid/widget/CheckBox;

    .line 918
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f080559

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    .line 919
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f08055a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    .line 921
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f08055b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView1:Landroid/widget/TextView;

    .line 922
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f08055c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView11:Landroid/widget/TextView;

    .line 923
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f08055d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView2:Landroid/widget/TextView;

    .line 924
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f08055e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView22:Landroid/widget/TextView;

    .line 925
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f08055f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView3:Landroid/widget/TextView;

    .line 926
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f080560

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView33:Landroid/widget/TextView;

    .line 927
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f080561

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView4:Landroid/widget/TextView;

    .line 928
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f080562

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView44:Landroid/widget/TextView;

    .line 929
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f080563

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView5:Landroid/widget/TextView;

    .line 930
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f080564

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView55:Landroid/widget/TextView;

    .line 931
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f080557

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextViewCb:Landroid/widget/TextView;

    .line 932
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f080565

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextViewClinicalInfo:Landroid/widget/TextView;

    .line 933
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0804ee

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    .line 935
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgain:Landroid/widget/CheckBox;

    .line 936
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 937
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgain:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 938
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 939
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->initDialogAnimation(Landroid/view/View;)V

    .line 940
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->startDialogAnimation()V

    .line 941
    return-void
.end method

.method private infoDiagOKBtnHandler()V
    .locals 4

    .prologue
    .line 944
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 945
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "heartrate_warning_checked"

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 946
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 948
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 953
    :goto_0
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDialogDissmised:Z

    .line 954
    return-void

    .line 949
    :catch_0
    move-exception v1

    .line 950
    .local v1, "ise":Ljava/lang/IllegalStateException;
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->TAG:Ljava/lang/String;

    const-string v3, "IllegalStateException while Information Dialog dismiss"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initDialogAnimation(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 974
    if-eqz p1, :cond_0

    .line 975
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 976
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f080559

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    .line 977
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f08055a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    .line 978
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImages:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 979
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImages:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 982
    :cond_0
    return-void
.end method

.method private initFadingAnimationEffect(I)V
    .locals 7
    .param p1, "imageIndex"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 996
    const/16 v0, 0x1f4

    .line 997
    .local v0, "fadeInDuration":I
    const/16 v2, 0x5dc

    .line 998
    .local v2, "timeBetween":I
    const/16 v1, 0x1f4

    .line 1000
    .local v1, "fadeOutDuration":I
    const/4 v3, 0x4

    if-eq p1, v3, :cond_0

    const/4 v3, 0x5

    if-eq p1, v3, :cond_0

    const/4 v3, 0x6

    if-ne p1, v3, :cond_1

    .line 1001
    :cond_0
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeIn:Landroid/view/animation/Animation;

    .line 1002
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v4, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeOut:Landroid/view/animation/Animation;

    .line 1008
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeIn:Landroid/view/animation/Animation;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1009
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeIn:Landroid/view/animation/Animation;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1010
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeIn:Landroid/view/animation/Animation;

    const/16 v4, 0x514

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1012
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeOut:Landroid/view/animation/Animation;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1013
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeOut:Landroid/view/animation/Animation;

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1014
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeOut:Landroid/view/animation/Animation;

    int-to-long v4, v1

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1015
    new-instance v3, Landroid/view/animation/AnimationSet;

    invoke-direct {v3, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    .line 1016
    new-instance v3, Landroid/view/animation/AnimationSet;

    invoke-direct {v3, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    .line 1017
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeIn:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1018
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeOut:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1019
    return-void

    .line 1004
    :cond_1
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeIn:Landroid/view/animation/Animation;

    .line 1005
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->fadeOut:Landroid/view/animation/Animation;

    goto :goto_0
.end method

.method private isEditTagMenuDisplay()Z
    .locals 2

    .prologue
    .line 1254
    const/4 v0, 0x0

    .line 1255
    .local v0, "ret":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1256
    const/4 v0, 0x1

    .line 1257
    :cond_0
    return v0
.end method

.method private isNonSensorMenuShow()Z
    .locals 1

    .prologue
    .line 1242
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCountToShowTag()I

    move-result v0

    if-lez v0, :cond_0

    .line 1243
    const/4 v0, 0x1

    .line 1244
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private prepareListDialogItem()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1174
    sget-object v3, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "prepareListDialogItem"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1175
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1177
    .local v2, "tagNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->tagItemList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1178
    .local v1, "tagName":Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1181
    .end local v1    # "tagName":Ljava/lang/String;
    :cond_0
    return-object v2
.end method

.method private showDeleteDialog()V
    .locals 4

    .prologue
    .line 428
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 429
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 430
    const v1, 0x7f090c11

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 431
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 432
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 433
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$8;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 444
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$9;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 454
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$10;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 464
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$11;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 471
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 472
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 473
    return-void
.end method

.method private showInfomationDialog()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 841
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 842
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 843
    const v1, 0x7f090a4c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 844
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 845
    const v1, 0x7f030144

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$15;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$15;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 854
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$16;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$16;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 862
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoDiagDismissHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 865
    if-eqz p0, :cond_1

    .line 867
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 868
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->startDialogAnimation()V

    .line 869
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "hrm_information_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 871
    :cond_1
    return-void
.end method

.method private showNoSensorInformationDialog()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1109
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v2, 0x7f0907be

    invoke-direct {v1, p0, v3, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 1112
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1113
    const v1, 0x7f030147

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$21;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$21;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1128
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$22;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$22;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1139
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$23;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$23;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1147
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mNoSensorInfoDiag:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1148
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mNoSensorInfoDiag:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "hrm_information"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1149
    return-void
.end method

.method private showSummarayFragment()V
    .locals 2

    .prologue
    .line 1185
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    if-eqz v0, :cond_0

    .line 1186
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 1188
    :cond_0
    return-void
.end method

.method private showTaggingListChooseDialog()V
    .locals 5

    .prologue
    .line 1154
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v3, 0x7f090c0b

    const/16 v4, 0xc

    invoke-direct {v1, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 1156
    .local v1, "dialogBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->updateTagItemList()V

    .line 1157
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->prepareListDialogItem()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 1159
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->tagItemList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Z

    .line 1160
    .local v0, "adapterSelection":[Z
    const/4 v3, 0x0

    const/4 v4, 0x1

    aput-boolean v4, v0, v3

    .line 1161
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 1163
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v2

    .line 1164
    .local v2, "listChooseDialog":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1166
    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$IOnDismissListener;)V

    .line 1167
    return-void
.end method

.method private updateTagItemList()V
    .locals 1

    .prologue
    .line 1170
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/utils/SupportUtils;->getLoadTagList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->tagItemList:Ljava/util/ArrayList;

    .line 1171
    return-void
.end method


# virtual methods
.method public callShowInformationDialog()V
    .locals 4

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateMeasuringEndUI:Z

    if-nez v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 252
    :cond_0
    return-void
.end method

.method protected customizeActionBar()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 655
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 656
    const/4 v0, 0x0

    .line 657
    .local v0, "emptyActionItemTextId":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 658
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    const v3, 0x7f090022

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 659
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v9, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f0207c0

    const v6, 0x7f09005a

    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 660
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v9, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f0207bc

    const v6, 0x7f0907b4

    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 662
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    .line 663
    .local v1, "graphFragment":Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v2

    if-eqz v2, :cond_0

    .line 664
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v9, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f0207cf

    const v6, 0x7f090033

    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 665
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 676
    :goto_0
    return-void

    .line 667
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 668
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeActionButton(I)V

    .line 669
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_0

    .line 671
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1226
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x16

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 1228
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 1229
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f080304

    if-ne v1, v2, :cond_0

    .line 1231
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f08030e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 1232
    const/4 v1, 0x1

    .line 1237
    .end local v0    # "currentView":Landroid/view/View;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1

    .prologue
    .line 1199
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method public getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 789
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getCallFromHome()Z
    .locals 1

    .prologue
    .line 401
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->callFromHome:Z

    return v0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 876
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    .line 877
    .local v0, "ret":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    if-eqz v0, :cond_0

    .line 882
    .end local v0    # "ret":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    :goto_0
    return-object v0

    .line 879
    .restart local v0    # "ret":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    :cond_0
    const-string v1, "hrm_information_dialog"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 880
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->InfoDiagContentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    goto :goto_0

    .line 882
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .locals 1

    .prologue
    .line 734
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mGraphFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    return-object v0
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1219
    const-string v0, "com.sec.shealth.help.action.HEART_RATE"

    return-object v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1206
    const/4 v0, 0x0

    return-object v0
.end method

.method public getShowDeleteDialog()Z
    .locals 2

    .prologue
    .line 794
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    .line 795
    const/4 v0, 0x1

    .line 800
    .local v0, "isShow":Z
    :goto_0
    return v0

    .line 797
    .end local v0    # "isShow":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "isShow":Z
    goto :goto_0
.end method

.method protected isDrawerMenuShown()Z
    .locals 1

    .prologue
    .line 1261
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->isDrawerMenuShown()Z

    move-result v0

    return v0
.end method

.method public isHrSensorNotAvailable()Z
    .locals 1

    .prologue
    .line 1248
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHRAvailablilty(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1249
    const/4 v0, 0x0

    .line 1250
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 477
    sparse-switch p1, :sswitch_data_0

    .line 551
    :cond_0
    :goto_0
    return-void

    .line 479
    :sswitch_0
    const/4 v5, -0x1

    if-ne p2, v5, :cond_1

    .line 480
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isAutoStartup:Z

    .line 481
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0

    .line 483
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->finish()V

    goto :goto_0

    .line 487
    :sswitch_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->activateListners()V

    .line 488
    if-eqz p2, :cond_3

    .line 489
    const-string v5, "have_default_tag"

    invoke-virtual {p3, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 490
    .local v1, "hasDefault":Z
    if-eqz v1, :cond_2

    .line 492
    const-string/jumbo v5, "result_tag_id"

    invoke-virtual {p3, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 493
    .local v3, "tagId":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setTag(I)V

    goto :goto_0

    .line 497
    .end local v3    # "tagId":I
    :cond_2
    const-string/jumbo v5, "result_tag_name"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 498
    .local v4, "tagName":Ljava/lang/String;
    const-string/jumbo v5, "result_tag_id"

    invoke-virtual {p3, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 499
    .restart local v3    # "tagId":I
    const-string/jumbo v5, "result_tag_icon_id"

    invoke-virtual {p3, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 500
    .local v2, "tagIconId":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v5, v4, v3, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setTag(Ljava/lang/String;II)V

    goto :goto_0

    .line 503
    .end local v1    # "hasDefault":Z
    .end local v2    # "tagIconId":I
    .end local v3    # "tagId":I
    .end local v4    # "tagName":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string v6, "from_more_tag"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 504
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string v6, "has_more_default_tag_pref"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 505
    .restart local v1    # "hasDefault":Z
    if-eqz v1, :cond_4

    .line 507
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v6, "more_tag_id"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 508
    .restart local v3    # "tagId":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setTag(I)V

    .line 517
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 518
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "from_more_tag"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 519
    const-string/jumbo v5, "more_tag_name"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 520
    const-string/jumbo v5, "more_tag_id"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 521
    const-string/jumbo v5, "more_tag_icon_id"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 522
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 512
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "tagId":I
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v6, "more_tag_id"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 513
    .restart local v3    # "tagId":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v6, "more_tag_name"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 514
    .restart local v4    # "tagName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v6, "more_tag_icon_id"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 515
    .restart local v2    # "tagIconId":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v5, v4, v3, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setTag(Ljava/lang/String;II)V

    goto :goto_1

    .line 528
    .end local v1    # "hasDefault":Z
    .end local v2    # "tagIconId":I
    .end local v3    # "tagId":I
    .end local v4    # "tagName":Ljava/lang/String;
    :sswitch_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string v6, "from_more_tag"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 529
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string v6, "has_more_default_tag_pref"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 530
    .restart local v1    # "hasDefault":Z
    if-eqz v1, :cond_5

    .line 532
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v6, "more_tag_id"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 533
    .restart local v3    # "tagId":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setTag(I)V

    .line 542
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 543
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "from_more_tag"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 544
    const-string/jumbo v5, "more_tag_name"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 545
    const-string/jumbo v5, "more_tag_id"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 546
    const-string/jumbo v5, "more_tag_icon_id"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 547
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 537
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "tagId":I
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v6, "more_tag_id"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 538
    .restart local v3    # "tagId":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v6, "more_tag_name"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 539
    .restart local v4    # "tagName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v6, "more_tag_icon_id"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 540
    .restart local v2    # "tagIconId":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v5, v4, v3, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setTag(Ljava/lang/String;II)V

    goto :goto_2

    .line 477
    :sswitch_data_0
    .sparse-switch
        0x93 -> :sswitch_1
        0x94 -> :sswitch_2
        0x45a -> :sswitch_0
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 761
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mGraphFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 762
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->callFromHome:Z

    if-eqz v0, :cond_0

    .line 763
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->finish()V

    .line 786
    :goto_0
    return-void

    .line 765
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0

    .line 767
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 768
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->backFinish:Z

    if-eqz v0, :cond_2

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopSensorAndUpdateUI(I)V

    .line 770
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->finish()V

    .line 775
    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$13;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 772
    :cond_2
    const v0, 0x7f090c18

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 773
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->backFinish:Z

    goto :goto_1

    .line 784
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 382
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mIsActivityOnConfigChanged:Z

    .line 384
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->clearAndRefreshDrawerMenu()V

    .line 385
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$7;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 394
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 398
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v13, 0x7f090f09

    const/4 v9, 0x2

    const/4 v12, 0x3

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 151
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 153
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 154
    .local v5, "model":Ljava/lang/String;
    const-string v7, "SM-G850F"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "SM-G850S"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "SM-G850A"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "SM-G850L"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "SM-G850W"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "SM-G850FQ"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 157
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImagesArray:[Ljava/lang/String;

    const-string v8, "hr_guide_alpha_1"

    aput-object v8, v7, v10

    .line 158
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImagesArray:[Ljava/lang/String;

    const-string v8, "hr_guide_alpha_2"

    aput-object v8, v7, v11

    .line 159
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImagesArray:[Ljava/lang/String;

    const-string v8, "hr_guide_alpha_3"

    aput-object v8, v7, v9

    .line 160
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImagesArray:[Ljava/lang/String;

    const-string v8, "hr_guide_alpha_4"

    aput-object v8, v7, v12

    .line 162
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImages:[I

    const v8, 0x7f0201f6

    aput v8, v7, v10

    .line 163
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImages:[I

    const v8, 0x7f0201f7

    aput v8, v7, v11

    .line 164
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImages:[I

    const v8, 0x7f0201f8

    aput v8, v7, v9

    .line 165
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImages:[I

    const v8, 0x7f0201f9

    aput v8, v7, v12

    .line 169
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "hrm_tag_chooser_dialog"

    invoke-virtual {v7, v8}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Landroid/support/v4/app/DialogFragment;

    .line 170
    .local v4, "mDialogFragment":Landroid/support/v4/app/DialogFragment;
    if-eqz v4, :cond_2

    .line 171
    invoke-virtual {v4}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 172
    const/4 v4, 0x0

    .line 176
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "HEARTRATE_WIDGET_CLICKED"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 177
    .local v3, "isUseWidget":Ljava/lang/Boolean;
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "com.sec.android.app.shealth.heartrate"

    const-string v9, "HR06"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_3
    iput-boolean v10, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isAutoStartup:Z

    .line 182
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v8, "show_graph_fragment"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 184
    .local v2, "isShowGraphFrag":Z
    if-eqz p1, :cond_4

    .line 185
    const-string v7, "chart_shown"

    invoke-virtual {p1, v7, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 186
    const-string v7, "chart_shown"

    invoke-virtual {p1, v7, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 188
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v8, "scover"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 189
    .local v1, "isFromScoverActivity":Z
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    sget-object v8, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->DEFAULT_TAG_SET:Ljava/lang/String;

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v6

    check-cast v6, Ljava/util/HashSet;

    .line 191
    .local v6, "tagSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v7

    if-ge v7, v12, :cond_5

    .line 193
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090f0a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090f0b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090f0c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090f0d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090f0e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090f0f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090f10

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 201
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 202
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v7, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->DEFAULT_TAG_SET:Ljava/lang/String;

    invoke-interface {v0, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 203
    sget-object v7, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->DEFAULT_TAG_SET:Ljava/lang/String;

    invoke-interface {v0, v7, v6}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f020503

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090f0a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0204f7

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090f0b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0204fd

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 207
    const v7, 0x7f090f0c

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0204eb

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 208
    const v7, 0x7f090f0d

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0204f1

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 209
    const v7, 0x7f090f0e

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0204f9

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 210
    const v7, 0x7f090f0f

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0204ef

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 211
    const v7, 0x7f090f10

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0204f5

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 212
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 214
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_5
    new-instance v7, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-direct {v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .line 215
    new-instance v7, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    invoke-direct {v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mGraphFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    .line 216
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mGraphFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    sget-object v8, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 218
    if-eqz p1, :cond_6

    .line 219
    const-string v7, "isTagListDialogVisible"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDialogshown:Z

    .line 220
    const-string/jumbo v7, "mHeartRateData"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mHeartRateData:I

    .line 221
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    const-string/jumbo v8, "state_finished"

    invoke-virtual {p1, v8, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    iput-boolean v8, v7, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateMeasuringEndUI:Z

    .line 228
    :cond_6
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, "heartrate_warning_checked"

    invoke-interface {v7, v8, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_7

    if-nez v2, :cond_7

    if-nez v1, :cond_7

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->callShowInformationDialog()V

    .line 233
    :goto_0
    if-eqz v2, :cond_8

    .line 234
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mGraphFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 235
    iput-boolean v11, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->callFromHome:Z

    .line 239
    :goto_1
    return-void

    .line 231
    :cond_7
    iput-boolean v11, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isAutoStartup:Z

    goto :goto_0

    .line 237
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 555
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100016

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 557
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDrawerMenuShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    const/4 v0, 0x0

    .line 561
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    if-eqz v0, :cond_1

    .line 370
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .line 372
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onDestroy()V

    .line 373
    return-void
.end method

.method public onDismiss()V
    .locals 2

    .prologue
    .line 1211
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    if-nez v0, :cond_0

    .line 1212
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 1214
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isAutoStartup:Z

    .line 1215
    return-void
.end method

.method public onDrawerClosed()V
    .locals 1

    .prologue
    .line 1275
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1276
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopDrawerView()V

    .line 1278
    :cond_0
    return-void
.end method

.method public onDrawerOpened()V
    .locals 1

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1267
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startDrawerView()V

    .line 1271
    :cond_0
    return-void
.end method

.method protected onLogSelected()V
    .locals 3

    .prologue
    .line 707
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.heartrate"

    const-string v2, "HR03"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->startActivity(Landroid/content/Intent;)V

    .line 709
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x0

    .line 613
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    if-nez v3, :cond_0

    .line 614
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopSensorAndUpdateUI(I)V

    .line 616
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 650
    :goto_0
    return v6

    .line 618
    :sswitch_0
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 621
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 622
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 625
    .end local v1    # "intent":Landroid/content/Intent;
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationFragment;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 626
    .local v0, "inf_intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 629
    .end local v0    # "inf_intent":Landroid/content/Intent;
    :sswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showTaggingListChooseDialog()V

    goto :goto_0

    .line 632
    :sswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showDeleteDialog()V

    goto :goto_0

    .line 635
    :sswitch_5
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.heartrate"

    const-string v5, "HR08"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->prepareShareView()V

    goto :goto_0

    .line 639
    :sswitch_6
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 640
    .local v2, "intentSetting":Landroid/content/Intent;
    const-string v3, "com.sec.android.app.shealth"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 641
    const-string v3, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 642
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 645
    .end local v2    # "intentSetting":Landroid/content/Intent;
    :sswitch_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.heartrate"

    const-string v5, "HR07"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    goto :goto_0

    .line 616
    :sswitch_data_0
    .sparse-switch
        0x7f0802db -> :sswitch_4
        0x7f080c8a -> :sswitch_7
        0x7f080c90 -> :sswitch_5
        0x7f080ca1 -> :sswitch_0
        0x7f080ca2 -> :sswitch_1
        0x7f080ca3 -> :sswitch_2
        0x7f080ca4 -> :sswitch_6
        0x7f080ca5 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 360
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPause()V

    .line 361
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 573
    if-eqz p1, :cond_1

    .line 574
    :try_start_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 575
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const v5, 0x7f100016

    invoke-virtual {v4, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 576
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDrawerMenuShown()Z

    move-result v4

    if-nez v4, :cond_0

    .line 577
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->customizeActionBar()V

    .line 578
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    .line 579
    .local v1, "graphFragment":Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 582
    const v2, 0x7f080ca1

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 583
    const v2, 0x7f080ca2

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 584
    const v2, 0x7f080ca3

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 585
    const v2, 0x7f080ca5

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 586
    const v2, 0x7f0802db

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 587
    const v2, 0x7f080ca4

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 588
    const v2, 0x7f080c90

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 589
    const v2, 0x7f080c8a

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 608
    .end local v1    # "graphFragment":Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;
    :cond_1
    :goto_0
    return v3

    .line 591
    .restart local v1    # "graphFragment":Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;
    :cond_2
    const v4, 0x7f080ca1

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-boolean v4, v4, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isReady:Z

    if-eqz v4, :cond_7

    :cond_3
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCountToShowTag()I

    move-result v4

    if-lez v4, :cond_7

    :cond_4
    move v4, v3

    :goto_1
    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 592
    const v4, 0x7f080ca2

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-boolean v4, v4, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isReady:Z

    if-eqz v4, :cond_8

    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isEditTagMenuDisplay()Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v3

    :goto_2
    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 593
    const v4, 0x7f080ca3

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v4

    sget-object v6, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->FULL:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v4, v6, :cond_9

    move v4, v3

    :goto_3
    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 595
    const v4, 0x7f080ca5

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 597
    const v4, 0x7f0802db

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 598
    const v4, 0x7f080ca4

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 599
    const v4, 0x7f080c90

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    if-eqz v5, :cond_a

    :goto_4
    move v2, v3

    :cond_6
    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 600
    const v2, 0x7f080c8a

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 604
    .end local v1    # "graphFragment":Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;
    :catch_0
    move-exception v0

    .line 605
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 591
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "graphFragment":Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;
    :cond_7
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isNonSensorMenuShow()Z

    move-result v4

    if-nez v4, :cond_4

    move v4, v2

    goto :goto_1

    :cond_8
    move v4, v2

    .line 592
    goto :goto_2

    :cond_9
    move v4, v2

    .line 593
    goto :goto_3

    .line 599
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isNonSensorMenuShow()Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    if-eqz v5, :cond_6

    goto :goto_4
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 274
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResume()V

    .line 275
    invoke-virtual {p0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->setDrawerMenuListener(Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;)V

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDrawerMenuShown()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->customizeActionBar()V

    .line 277
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mIsActivityOnConfigChanged:Z

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 282
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$4;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 303
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 305
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    .line 306
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInfoView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->initDialogAnimation(Landroid/view/View;)V

    .line 308
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->startDialogAnimation()V

    .line 313
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDialogshown:Z

    if-eqz v0, :cond_6

    .line 314
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mHeartRateData:I

    if-eq v0, v5, :cond_3

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mHeartRateData:I

    iput v1, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    .line 317
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iput-boolean v4, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateMeasuringEndUI:Z

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setMeasuringEndUI()V

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDialogHandler:Landroid/os/Handler;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setNeedsReadyUI(Z)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->activateListners()V

    .line 322
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mIsActivityOnConfigChanged:Z

    if-eqz v0, :cond_4

    .line 323
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mIsActivityOnConfigChanged:Z

    .line 352
    :cond_4
    :goto_1
    return-void

    .line 289
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mNoSensorInfoDiag:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mNoSensorInfoDiag:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mNoSensorInfoDiag:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mNoSensorInfoDiag:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 293
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mNoSensorInfoDiag:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$5;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 327
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setNeedsReadyUI(Z)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateMeasuringEndUI:Z

    if-eqz v0, :cond_8

    .line 331
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mHeartRateData:I

    if-eq v0, v5, :cond_7

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mHeartRateData:I

    iput v1, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    .line 335
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setMeasuringEndUI()V

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->activateListners()V

    .line 340
    :cond_8
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mIsActivityOnConfigChanged:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_9

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setMeasuringEndUI()V

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$6;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 351
    :cond_9
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mIsActivityOnConfigChanged:Z

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1288
    const-string v0, "chart_shown"

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mGraphFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->isVisible()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1289
    const-string/jumbo v0, "mHeartRateData"

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget v1, v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1291
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    if-eqz v0, :cond_0

    .line 1292
    const-string v0, "isTagListDialogVisible"

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->isVisible()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1294
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateMeasuringEndUI:Z

    if-eqz v0, :cond_1

    .line 1295
    const-string/jumbo v0, "state_finished"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1297
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1298
    return-void
.end method

.method public onSlide(F)V
    .locals 0
    .param p1, "offset"    # F

    .prologue
    .line 1284
    return-void
.end method

.method public startDialogAnimation()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 985
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 986
    sput-boolean v5, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDialogDissmised:Z

    .line 987
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImages:[I

    aget v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 988
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 989
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImages:[I

    aget v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 990
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 991
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImages:[I

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->informationImagesArray:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    .line 993
    :cond_0
    return-void
.end method

.method public switchFragmentToGraph()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 744
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.heartrate"

    const-string v2, "HR04"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->callFromHome:Z

    .line 746
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mGraphFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    invoke-virtual {p0, v0, v3, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZ)V

    .line 747
    return-void
.end method

.method public switchFragmentToSummary()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 739
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZ)V

    .line 740
    return-void
.end method

.method public updateStringOnLocaleChange()V
    .locals 3

    .prologue
    .line 405
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 406
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v0

    .line 407
    .local v0, "content":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 408
    const v1, 0x7f0800b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090047

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 409
    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090a4c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 411
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextViewCb:Landroid/widget/TextView;

    const v2, 0x7f09078c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 412
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView1:Landroid/widget/TextView;

    const v2, 0x7f090c1a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 413
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView11:Landroid/widget/TextView;

    const v2, 0x7f090c14

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 414
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView2:Landroid/widget/TextView;

    const v2, 0x7f090c1b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 415
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView22:Landroid/widget/TextView;

    const v2, 0x7f090c15

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 416
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView3:Landroid/widget/TextView;

    const v2, 0x7f090c1c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 417
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView33:Landroid/widget/TextView;

    const v2, 0x7f090c16

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 418
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView4:Landroid/widget/TextView;

    const v2, 0x7f090c1d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 419
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView44:Landroid/widget/TextView;

    const v2, 0x7f090c26

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 420
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView5:Landroid/widget/TextView;

    const v2, 0x7f090c1e

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 421
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextView55:Landroid/widget/TextView;

    const v2, 0x7f090c2d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 422
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mTextViewClinicalInfo:Landroid/widget/TextView;

    const v2, 0x7f090c08

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 425
    .end local v0    # "content":Landroid/view/View;
    :cond_0
    return-void
.end method
