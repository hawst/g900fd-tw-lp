.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;
.super Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;
.source "ExerciseProWorkoutInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExerciseImageScaleCopyTask"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private editMode:Z

.field private exerciseRowId:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "inputImagePath"    # Ljava/lang/String;
    .param p4, "outputImagePath"    # Ljava/lang/String;
    .param p5, "exerciseRowId"    # J
    .param p7, "editMode"    # Z

    .prologue
    .line 1173
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .line 1174
    invoke-direct {p0, p3, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1175
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->context:Landroid/content/Context;

    .line 1176
    iput-wide p5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->exerciseRowId:J

    .line 1177
    iput-boolean p7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->editMode:Z

    .line 1178
    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 5
    .param p1, "isSuccessful"    # Ljava/lang/Boolean;

    .prologue
    .line 1182
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1183
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1184
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->getOutputImagePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1187
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$1400()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onPostExecute() outputImagePath="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->getOutputImagePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", exerciseRowId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->exerciseRowId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", editMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->editMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1190
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->getOutputImagePath()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->exerciseRowId:J

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->editMode:Z

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->saveInfoAndUpdateViewGrid(Ljava/lang/String;JZ)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;Ljava/lang/String;JZ)V

    .line 1191
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->refreshLastTakenPhoto(Landroid/content/Context;)V

    .line 1193
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1167
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
