.class public Lcom/sec/android/app/shealth/framework/repository/common/DBTables$SensorTable;
.super Ljava/lang/Object;
.source "DBTables.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SensorTable"
.end annotation


# static fields
.field public static final COLUMN_NAME_APPLICATION_NAME:Ljava/lang/String; = "app_name"

.field public static final COLUMN_NAME_CONNECTIVITY:Ljava/lang/String; = "connectivity"

.field public static final COLUMN_NAME_MANUFACTURE:Ljava/lang/String; = "manufacture"

.field public static final COLUMN_NAME_MODEL_NUMBER:Ljava/lang/String; = "modelnumber"

.field public static final COLUMN_NAME_SENSOR_NAME:Ljava/lang/String; = "name"

.field public static final COLUMN_NAME_TYPE:Ljava/lang/String; = "type"

.field public static final COLUMN_NAME_VERSION:Ljava/lang/String; = "version"

.field public static final TABLE_NAME:Ljava/lang/String; = "sensor"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
