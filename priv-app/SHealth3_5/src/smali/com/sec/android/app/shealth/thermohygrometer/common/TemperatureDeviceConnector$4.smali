.class Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$4;
.super Landroid/os/CountDownTimer;
.source "TemperatureDeviceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 468
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 478
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onFinish!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mTempSensorListener:Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;->onTempTimeout()V

    .line 480
    return-void
.end method

.method public onTick(J)V
    .locals 2
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 472
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onTick: "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mTempSensorListener:Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;->onTempTick(J)V

    .line 474
    return-void
.end method
