.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;
.super Landroid/os/AsyncTask;
.source "WalkingMateRankingAsync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync$RankingAsyncListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# static fields
.field public static final SEARCH_TYPE_LEADER_BOARD:I = 0x1

.field public static final SEARCH_TYPE_YOUR_RANKING:I


# instance fields
.field private mJsonObj:Lorg/json/JSONObject;

.field private mRankingAsyncListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync$RankingAsyncListener;

.field private searchType:I


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;->searchType:I

    .line 23
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;->searchType:I

    .line 24
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Long;
    .locals 8
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 37
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 39
    .local v2, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string/jumbo v3, "type"

    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 40
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;->searchType:I

    if-ne v3, v5, :cond_0

    .line 41
    const-string/jumbo v3, "pageNo"

    const/4 v4, 0x1

    aget-object v4, p1, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 42
    const-string/jumbo v3, "pageSize"

    const/4 v4, 0x2

    aget-object v4, p1, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :cond_0
    :goto_0
    :try_start_1
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;->searchType:I

    const-string/jumbo v4, "testDeviceId"

    const-string v5, "C3D34953D4"

    const-string/jumbo v6, "testAppId"

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getWalkingMateRanking(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;->mJsonObj:Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_5

    .line 75
    :goto_1
    const/4 v3, 0x0

    return-object v3

    .line 45
    :catch_0
    move-exception v1

    .line 47
    .local v1, "e1":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 58
    .end local v1    # "e1":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 60
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 61
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 63
    .local v0, "e":Ljava/net/URISyntaxException;
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto :goto_1

    .line 64
    .end local v0    # "e":Ljava/net/URISyntaxException;
    :catch_3
    move-exception v0

    .line 66
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 67
    .end local v0    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v0

    .line 69
    .local v0, "e":Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;->printStackTrace()V

    goto :goto_1

    .line 70
    .end local v0    # "e":Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
    :catch_5
    move-exception v0

    .line 72
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;->doInBackground([Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Long;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Long;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;->mRankingAsyncListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync$RankingAsyncListener;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;->mJsonObj:Lorg/json/JSONObject;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync$RankingAsyncListener;->onPostExecuteRankingAsync(Lorg/json/JSONObject;)V

    .line 82
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 83
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;->onPostExecute(Ljava/lang/Long;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;->mRankingAsyncListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync$RankingAsyncListener;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync$RankingAsyncListener;->onPreExecuteRankingAsync()V

    .line 30
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 31
    return-void
.end method

.method public setRankingListener(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync$RankingAsyncListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync$RankingAsyncListener;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync;->mRankingAsyncListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync$RankingAsyncListener;

    .line 88
    return-void
.end method
