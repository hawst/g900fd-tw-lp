.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$7;
.super Ljava/lang/Object;
.source "StressSummaryFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measuringCircleAnimationStop(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0

    .prologue
    .line 861
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 873
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 874
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 875
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 877
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->cancel()V

    .line 878
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2202(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;

    .line 880
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 869
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 865
    return-void
.end method
