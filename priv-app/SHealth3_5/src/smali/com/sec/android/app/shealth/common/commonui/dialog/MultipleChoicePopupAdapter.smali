.class public Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;
.super Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;
.source "MultipleChoicePopupAdapter.java"


# instance fields
.field private checkbox:Landroid/widget/CheckBox;

.field private mIsChecked:[Z

.field private textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;[Z)V
    .locals 0
    .param p3, "checked"    # [Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[Z)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "descriptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 38
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->mIsChecked:[Z

    .line 39
    return-void
.end method


# virtual methods
.method public getCheckedItem(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->mIsChecked:[Z

    aget-boolean v0, v0, p1

    return v0
.end method

.method public getIndexChecked()[Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->mIsChecked:[Z

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 44
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->descriptions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->mIsChecked:[Z

    aget-boolean v3, v3, p1

    invoke-direct {v2, v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v2
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 111
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 50
    if-nez p2, :cond_1

    .line 52
    if-nez p3, :cond_0

    .line 53
    const/4 v1, 0x0

    .line 92
    :goto_0
    return-object v1

    .line 54
    :cond_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v4, "layout_inflater"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 55
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$layout;->multi_checkbox_dialog_item:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 57
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->multi_item_text:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->textView:Landroid/widget/TextView;

    .line 58
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->multi_item_checkbox:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->checkbox:Landroid/widget/CheckBox;

    .line 61
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_1
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->mIsChecked:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_3

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 85
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->items:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->items:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 86
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->items:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->checkbox:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->items:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 90
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->mdisabledIndex:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_2
    invoke-virtual {p2, v1}, Landroid/view/View;->setEnabled(Z)V

    move-object v1, p2

    .line 92
    goto :goto_0

    .line 82
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_4
    move v1, v3

    .line 90
    goto :goto_2
.end method
