.class public Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "UvLogDetailActivity.java"


# static fields
.field public static final ACTIONBAR_EDIT:I = 0x0

.field public static final EDIT:Ljava/lang/String; = "Edit"


# instance fields
.field private actionbarClickListener:Landroid/view/View$OnClickListener;

.field private ddate:Ljava/lang/String;

.field private mAccessoryType:Landroid/widget/TextView;

.field private mComment:Landroid/widget/TextView;

.field private mCommentTitle:Landroid/widget/TextView;

.field private mSpfLayout:Landroid/widget/LinearLayout;

.field private mSunProtectionLayout:Landroid/widget/LinearLayout;

.field private mTime:Landroid/widget/TextView;

.field private mTimeDate:Landroid/widget/TextView;

.field private mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

.field private mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

.field private mUvIcon:Landroid/widget/ImageView;

.field private mUvIndex:Landroid/widget/TextView;

.field private mUvSPFValue:Landroid/widget/TextView;

.field private mUvState:Landroid/widget/TextView;

.field private mUvSunProtectionTimeValue:Landroid/widget/TextView;

.field private memoIcon:Landroid/widget/ImageView;

.field private spfDivier:Landroid/view/View;

.field private sunProtectionDivider:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 226
    new-instance v0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;-><init>(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private UvScoreIcon(I)V
    .locals 2
    .param p1, "uvScore"    # I

    .prologue
    .line 163
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-gt p1, v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f090d94

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 174
    :goto_0
    return-void

    .line 165
    :cond_0
    const/4 v0, 0x3

    if-lt p1, v0, :cond_1

    const/4 v0, 0x5

    if-gt p1, v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f090d95

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 167
    :cond_1
    const/4 v0, 0x6

    if-lt p1, v0, :cond_2

    const/4 v0, 0x7

    if-gt p1, v0, :cond_2

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f090d96

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 169
    :cond_2
    const/16 v0, 0x8

    if-lt p1, v0, :cond_3

    const/16 v0, 0xa

    if-gt p1, v0, :cond_3

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f090d97

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 172
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f090d98

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private UvScoreIconForChina(I)V
    .locals 2
    .param p1, "uvScore"    # I

    .prologue
    .line 177
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-gt p1, v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f091158

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 188
    :goto_0
    return-void

    .line 179
    :cond_0
    const/4 v0, 0x3

    if-lt p1, v0, :cond_1

    const/4 v0, 0x4

    if-gt p1, v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f091159

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 181
    :cond_1
    const/4 v0, 0x5

    if-lt p1, v0, :cond_2

    const/4 v0, 0x6

    if-gt p1, v0, :cond_2

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f09115a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 183
    :cond_2
    const/4 v0, 0x7

    if-lt p1, v0, :cond_3

    const/16 v0, 0x9

    if-gt p1, v0, :cond_3

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f09115b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 186
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f09115c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Lcom/sec/android/app/shealth/uv/data/UvData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->ddate:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    return-object v0
.end method

.method private initView(Ljava/lang/String;)V
    .locals 13
    .param p1, "uvId"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x1

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 68
    invoke-static {p0}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    .line 69
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    invoke-virtual {v6, p1}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getDataById(Ljava/lang/String;)Lcom/sec/android/app/shealth/uv/data/UvData;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    .line 71
    const v6, 0x7f08052e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    .line 72
    const v6, 0x7f08052f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    .line 73
    const v6, 0x7f080a91

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mTime:Landroid/widget/TextView;

    .line 74
    const v6, 0x7f080539

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mComment:Landroid/widget/TextView;

    .line 76
    const v6, 0x7f0803e4

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->memoIcon:Landroid/widget/ImageView;

    .line 78
    const v6, 0x7f080a94

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvState:Landroid/widget/TextView;

    .line 80
    const v6, 0x7f080a92

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvIcon:Landroid/widget/ImageView;

    .line 82
    const v6, 0x7f080a99

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvSPFValue:Landroid/widget/TextView;

    .line 83
    const v6, 0x7f080a9d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvSunProtectionTimeValue:Landroid/widget/TextView;

    .line 85
    const v6, 0x7f080a95

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->spfDivier:Landroid/view/View;

    .line 86
    const v6, 0x7f080a9a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->sunProtectionDivider:Landroid/view/View;

    .line 88
    const v6, 0x7f080a9b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mSunProtectionLayout:Landroid/widget/LinearLayout;

    .line 89
    const v6, 0x7f080a96

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mSpfLayout:Landroid/widget/LinearLayout;

    .line 91
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getRecomendedTime()J

    move-result-wide v6

    sget-object v8, Lcom/sec/android/app/shealth/uv/common/UvConstants;->INVALID:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_3

    .line 92
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mSunProtectionLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 93
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->sunProtectionDivider:Landroid/view/View;

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    .line 100
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSunProtection()I

    move-result v6

    int-to-long v6, v6

    sget-object v8, Lcom/sec/android/app/shealth/uv/common/UvConstants;->INVALID:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_4

    .line 101
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->spfDivier:Landroid/view/View;

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    .line 102
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mSpfLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 110
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getUserDeviceId()Ljava/lang/String;

    move-result-object v4

    .line 111
    .local v4, "userDeviceId":Ljava/lang/String;
    if-eqz v4, :cond_6

    .line 113
    const-string v6, "10029"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 114
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 129
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v7

    invoke-static {p0, v7, v8}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mTime:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v7

    invoke-static {p0, v7, v8}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, "yyyy/MM/dd"

    invoke-direct {v2, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 132
    .local v2, "dformat":Ljava/text/SimpleDateFormat;
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->ddate:Ljava/lang/String;

    .line 133
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->ddate:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getScore()D

    move-result-wide v6

    double-to-int v5, v6

    .line 140
    .local v5, "uvScore":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v6

    if-nez v6, :cond_7

    .line 141
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->UvScoreIcon(I)V

    .line 146
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getComment()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getComment()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    .line 147
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->memoIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 149
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getComment()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getComment()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_2

    .line 151
    new-instance v3, Landroid/text/SpannableString;

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getComment()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 152
    .local v3, "mSpannableString":Landroid/text/SpannableString;
    new-instance v6, Landroid/text/style/LeadingMarginSpan$Standard;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a06a7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    invoke-direct {v6, v7, v10}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(II)V

    invoke-virtual {v3, v6, v10, v12, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 153
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mComment:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    const v6, 0x7f080538

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 155
    .local v0, "MemoLayout":Landroid/widget/RelativeLayout;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f09007c

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/uv/data/UvData;->getComment()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->memoIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 160
    .end local v0    # "MemoLayout":Landroid/widget/RelativeLayout;
    .end local v3    # "mSpannableString":Landroid/text/SpannableString;
    :cond_2
    return-void

    .line 95
    .end local v2    # "dformat":Ljava/text/SimpleDateFormat;
    .end local v4    # "userDeviceId":Ljava/lang/String;
    .end local v5    # "uvScore":I
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mSunProtectionLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 96
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->sunProtectionDivider:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 97
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvSunProtectionTimeValue:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/uv/data/UvData;->getRecomendedTime()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->calculateSafetyTime(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 104
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->spfDivier:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 105
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mSpfLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 106
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvSPFValue:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSunProtection()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 117
    .restart local v4    # "userDeviceId":Ljava/lang/String;
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    invoke-virtual {v6, p1}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "deviceName":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090a8c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v12, [Ljava/lang/Object;

    aput-object v1, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 125
    .end local v1    # "deviceName":Ljava/lang/String;
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 143
    .restart local v2    # "dformat":Ljava/text/SimpleDateFormat;
    .restart local v5    # "uvScore":I
    :cond_7
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->UvScoreIconForChina(I)V

    goto/16 :goto_3
.end method

.method private showDeletePopup()V
    .locals 3

    .prologue
    const v2, 0x7f090035

    .line 257
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09078e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$2;-><init>(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 267
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 219
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 220
    const/4 v0, 0x0

    .line 221
    .local v0, "emptyActionItemTextId":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f09005b

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 222
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v3, 0x0

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f020299

    const v6, 0x7f090040

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 224
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f03025c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->setContentView(I)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "HEART_RATE_ID_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->initView(Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10002b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 200
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 205
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 214
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 207
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.uv"

    const-string v2, "UV09"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->prepareShareView()V

    goto :goto_0

    .line 211
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->showDeletePopup()V

    goto :goto_0

    .line 205
    :pswitch_data_0
    .packed-switch 0x7f080c8d
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 192
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mTime:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    return-void
.end method
