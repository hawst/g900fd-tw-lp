.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;
.super Ljava/lang/Object;
.source "ExerciseProChartFragment.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)V
    .locals 0

    .prologue
    .line 509
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnGraphData(Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    const v11, 0x7f0807db

    const/4 v10, 0x2

    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 539
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getHandlerItemTextVisible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 540
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 542
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 543
    const/high16 v1, 0x41f80000    # 31.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->density:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 544
    const/high16 v1, 0x427c0000    # 63.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 545
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0058

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 549
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 550
    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 552
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 554
    .end local v0    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_0
    if-eqz p1, :cond_9

    .line 555
    const-string v1, "ExerciseProChartFragment"

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v2, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    long-to-double v3, v3

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 557
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080528

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 558
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0807d9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 559
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->getPaceMode()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 560
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    cmpg-double v1, v1, v8

    if-gtz v1, :cond_3

    .line 561
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    const-string v2, "0\'00\""

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getPaceBubbleAreaUnit()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mContext:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090a4d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setFirstInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportHRM()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v10, :cond_7

    .line 576
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    cmpg-double v1, v1, v8

    if-gtz v1, :cond_5

    .line 579
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setSecondInformationVisible(Z)V

    .line 588
    :goto_1
    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    cmpg-double v1, v1, v8

    if-gtz v1, :cond_6

    .line 591
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setThirdInformationVisible(Z)V

    .line 617
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->refreshInformationAreaView()V

    .line 623
    :cond_2
    :goto_3
    return-void

    .line 564
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v2, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm2:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/text/DecimalFormat;

    move-result-object v4

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getPaceValue(D)Ljava/lang/String;
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;D)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getPaceBubbleAreaUnit()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mContext:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090a4d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setFirstInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 570
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v2, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/text/DecimalFormat;

    move-result-object v3

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getSpeedBubbleAreaUnit()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mContext:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090a49

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setFirstInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 582
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080529

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 583
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0807da

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 584
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v2, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/text/DecimalFormat;

    move-result-object v3

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getBPMBubbleAreaUnit()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setSecondInformation(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 594
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0807dc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 595
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 596
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v2, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm3:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/text/DecimalFormat;

    move-result-object v3

    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "m"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setThirdInformation(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 599
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportHRM()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v10, :cond_1

    .line 600
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    cmpg-double v1, v1, v8

    if-gtz v1, :cond_8

    .line 605
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setSecondInformationVisible(Z)V

    .line 606
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setThirdInformationVisible(Z)V

    goto/16 :goto_2

    .line 610
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0807dc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 611
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 612
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setSecondInformationVisible(Z)V

    .line 613
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v2, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->oneDForm:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Ljava/text/DecimalFormat;

    move-result-object v3

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "m"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setThirdInformation(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 619
    :cond_9
    if-nez p1, :cond_2

    .line 620
    const-string v1, "ExerciseProChartFragment"

    const-string v2, "Handler Data:NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->dimInformationAreaView()V

    goto/16 :goto_3
.end method

.method public OnReleaseTimeOut()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 527
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 529
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 530
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 531
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 532
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 534
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 535
    return-void
.end method

.method public OnVisible()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 512
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 514
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 515
    const/high16 v1, 0x41f80000    # 31.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->density:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 516
    const/high16 v1, 0x427c0000    # 63.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 517
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0058

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 519
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 520
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 522
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 523
    return-void
.end method
