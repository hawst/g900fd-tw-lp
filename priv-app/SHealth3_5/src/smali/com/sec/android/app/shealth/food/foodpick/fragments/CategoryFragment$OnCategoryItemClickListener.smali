.class Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnCategoryItemClickListener;
.super Ljava/lang/Object;
.source "CategoryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnCategoryItemClickListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnCategoryItemClickListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;

    .prologue
    .line 433
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnCategoryItemClickListener;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;)V
    .locals 3
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;

    .prologue
    .line 437
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnCategoryItemClickListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$2100(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_CATEGORY_LIST_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 438
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnCategoryItemClickListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;)V

    .line 439
    .local v0, "task":Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;->isAlreadyOnRequiredStage()Z

    move-result v1

    if-nez v1, :cond_0

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnCategoryItemClickListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$2000(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;->addTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    .line 442
    :cond_0
    return-void
.end method

.method public bridge synthetic onItemClick(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 433
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnCategoryItemClickListener;->onItemClick(Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;)V

    return-void
.end method
