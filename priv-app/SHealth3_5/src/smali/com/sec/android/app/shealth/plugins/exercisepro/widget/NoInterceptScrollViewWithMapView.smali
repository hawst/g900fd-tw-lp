.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/NoInterceptScrollViewWithMapView;
.super Landroid/widget/ScrollView;
.source "NoInterceptScrollViewWithMapView.java"


# instance fields
.field mMapContainer:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method private inRegion(FFLandroid/view/View;)Z
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    const/4 v3, 0x2

    new-array v0, v3, [I

    fill-array-data v0, :array_0

    .line 47
    .local v0, "mCoordBuffer":[I
    invoke-virtual {p3, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 49
    aget v3, v0, v2

    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v3, v3, p1

    if-lez v3, :cond_0

    aget v3, v0, v1

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v3, v3, p2

    if-lez v3, :cond_0

    aget v3, v0, v2

    int-to-float v3, v3

    cmpg-float v3, v3, p1

    if-gez v3, :cond_0

    aget v3, v0, v1

    int-to-float v3, v3

    cmpg-float v3, v3, p2

    if-gez v3, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0

    .line 44
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/NoInterceptScrollViewWithMapView;->mMapContainer:Landroid/view/View;

    if-nez v0, :cond_0

    .line 34
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 39
    :goto_0
    return v0

    .line 36
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/NoInterceptScrollViewWithMapView;->mMapContainer:Landroid/view/View;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/NoInterceptScrollViewWithMapView;->inRegion(FFLandroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    const/4 v0, 0x0

    goto :goto_0

    .line 39
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setMapContainer(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/NoInterceptScrollViewWithMapView;->mMapContainer:Landroid/view/View;

    .line 58
    return-void
.end method
