.class public Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "SHealthAlertDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnSaveInstanceStateListener;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCenterButtonClickListener;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;
    }
.end annotation


# static fields
.field public static final ALERT_TEXT_KEY:Ljava/lang/String; = "alert_text"

.field protected static final ALERT_TEXT_RES_ID_KEY:Ljava/lang/String; = "alert_text_res_id"

.field protected static final ALERT_TITLE_BG_DRAWABLE_RES_ID_KEY:Ljava/lang/String; = "alert_title_bg_res_id"

.field protected static final ALERT_TITLE_COLOR_RES_ID_KEY:Ljava/lang/String; = "alert_title_color_res_id"

.field public static final BINDED_DATA:Ljava/lang/String; = "binded_data"

.field public static final CANCELABLE_KEY:Ljava/lang/String; = "cancelable"

.field public static final CANCEL_TEXT:Ljava/lang/String; = "cancel_text"

.field public static final CENTER_TEXT:Ljava/lang/String; = "center_text"

.field public static final CONTENT_RESOURCE_KEY:Ljava/lang/String; = "content_resource"

.field public static final DATE_PICKER_DIALOG:Ljava/lang/String; = "date_picker_dialog"

.field public static final DEFAULT_VALUE:I = 0x0

.field public static final DONT_DISMISS_ON_OK_CLICK:Ljava/lang/String; = "dont_dismiss_on_OK_click"

.field public static final DO_NOT_DISMISS_ON_BACK_PRESSED:Ljava/lang/String; = "dont_dismiss_on_back_pressed"

.field public static final FLAGS:Ljava/lang/String; = "flags"

.field public static final LIST_DISPLAY:Ljava/lang/String; = "list_display_dialog"

.field public static final MULTI_CHECKBOX:Ljava/lang/String; = "multi_checkbox_dialog"

.field public static final NEED_REDRAW:Ljava/lang/String; = "need_redraw"

.field public static final NEED_REFRESH_FOCUSABLES:Ljava/lang/String; = "need_refresh_focusables"

.field public static final NO_BUTTONS_TITLE:I = 0x7

.field public static final OK_TEXT:Ljava/lang/String; = "ok_text"

.field public static final SET_DEFAULT_CONTENT_MARGINS:Ljava/lang/String; = "set_default_content_margins"

.field private static final TAG:Ljava/lang/String;

.field public static final TIME_PICKER_DIALOG:Ljava/lang/String; = "time_picker_dialog"

.field protected static final TITLE_RES_ID_KEY:Ljava/lang/String; = "title_res_id"

.field protected static final TITLE_RES_KEY:Ljava/lang/String; = "title_string"

.field public static final TYPE_CANCEL:I = 0x1

.field public static final TYPE_KEY:Ljava/lang/String; = "type"

.field public static final TYPE_LIST_DISPLAY:I = 0x5

.field public static final TYPE_MULTI_CHECKBOX:I = 0x4

.field public static final TYPE_NO_BUTTONS:I = 0x3

.field public static final TYPE_OK:I = 0x0

.field public static final TYPE_OK_CANCEL:I = 0x2

.field public static final TYPE_THREE_BUTTONS:I = 0x6


# instance fields
.field adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;

.field addButton:Landroid/widget/TextView;

.field private addButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCenterButtonClickListener;

.field cancelAddSeparator:Landroid/view/View;

.field private cancelButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

.field checkedChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field protected content:Landroid/view/View;

.field private contentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

.field private doNotDismissOnBackPressed:Z

.field private isDialogScreenLoaded:Z

.field private isDismissOnAddPressed:Z

.field private itemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;",
            ">;"
        }
    .end annotation
.end field

.field listDisplayAdapter:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;

.field private listDisplayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field private mFocusedView:Landroid/view/View;

.field private mIsInstanceStateLoss:Z

.field private okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

.field private onBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

.field private onDismissListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 121
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 91
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isDialogScreenLoaded:Z

    .line 108
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->doNotDismissOnBackPressed:Z

    .line 109
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isDismissOnAddPressed:Z

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayList:Ljava/util/ArrayList;

    .line 114
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mIsInstanceStateLoss:Z

    .line 1422
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$5;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->checkedChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 122
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mFocusedView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->doNotDismissOnBackPressed:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isDialogScreenLoaded:Z

    return p1
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static onDialogNotSupported(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 3
    .param p0, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 426
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Dialog with tag \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private onNegativeButtonClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;

    if-eqz v2, :cond_1

    .line 733
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    .line 735
    .local v0, "dialogButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    if-eqz v0, :cond_1

    .line 736
    sget-object v2, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->NEGATIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    invoke-interface {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;->onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V

    .line 751
    .end local v0    # "dialogButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    :cond_0
    :goto_0
    return-void

    .line 740
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    if-eqz v2, :cond_2

    .line 741
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;->getNegativeButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;

    move-result-object v1

    .line 743
    .local v1, "negativeButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    if-eqz v1, :cond_2

    iget-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->btnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_2

    .line 744
    iget-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->btnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v2, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 748
    .end local v1    # "negativeButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->cancelButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

    if-eqz v2, :cond_0

    .line 749
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->cancelButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;->onCancelButtonClick(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private onNeutralButtonClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;

    if-eqz v1, :cond_1

    .line 755
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    .line 757
    .local v0, "dialogButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    if-eqz v0, :cond_1

    .line 758
    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->NEUTRAL:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;->onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V

    .line 765
    .end local v0    # "dialogButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    :cond_0
    :goto_0
    return-void

    .line 762
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->addButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCenterButtonClickListener;

    if-eqz v1, :cond_0

    .line 763
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->addButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCenterButtonClickListener;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCenterButtonClickListener;->onAddButtonClick(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private onPositiveButtonClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 710
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;

    if-eqz v2, :cond_1

    .line 711
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    .line 713
    .local v0, "dialogButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    if-eqz v0, :cond_1

    .line 714
    sget-object v2, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    invoke-interface {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;->onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V

    .line 729
    .end local v0    # "dialogButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    :cond_0
    :goto_0
    return-void

    .line 718
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    if-eqz v2, :cond_2

    .line 719
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;->getPositiveButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;

    move-result-object v1

    .line 721
    .local v1, "positiveButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    if-eqz v1, :cond_2

    iget-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->btnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_2

    .line 722
    iget-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->btnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v2, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 726
    .end local v1    # "positiveButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    if-eqz v2, :cond_0

    .line 727
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;->onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V

    goto :goto_0
.end method

.method private tryToDismiss()V
    .locals 1

    .prologue
    .line 768
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 769
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mIsInstanceStateLoss:Z

    if-eqz v0, :cond_1

    .line 770
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mIsInstanceStateLoss:Z

    .line 771
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 776
    :cond_0
    :goto_0
    return-void

    .line 774
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    goto :goto_0
.end method


# virtual methods
.method protected getGeneralParentContainer(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 21
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 431
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$layout;->alert_dialog:I

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v15, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 433
    .local v11, "parentContent":Landroid/view/View;
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->ok_button:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 435
    .local v8, "okButton":Landroid/widget/TextView;
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_button:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 437
    .local v2, "cancelButton":Landroid/widget/TextView;
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->add_button:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->addButton:Landroid/widget/TextView;

    .line 438
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->btns_margin2:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->cancelAddSeparator:Landroid/view/View;

    .line 439
    move-object/from16 v0, p0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 440
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 441
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->addButton:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 443
    const-string/jumbo v15, "title_res_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 444
    .local v14, "titleTextResId":I
    const/4 v15, -0x1

    if-eq v14, v15, :cond_9

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getStringFromResource(I)Ljava/lang/String;

    move-result-object v13

    .line 448
    .local v13, "titleStr":Ljava/lang/String;
    :goto_0
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->title:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    const-string/jumbo v16, "sec-roboto-light"

    const/16 v17, 0x1

    invoke-static/range {v16 .. v17}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 449
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->title:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v16

    sget v17, Lcom/sec/android/app/shealth/common/commonui/R$style;->boldType:I

    invoke-virtual/range {v15 .. v17}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 450
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 451
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->title:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    invoke-virtual {v15, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 460
    :cond_0
    const-string/jumbo v15, "type"

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v15

    packed-switch v15, :pswitch_data_0

    .line 499
    :cond_1
    :goto_1
    :pswitch_0
    const-string v15, "flags"

    const/16 v16, -0x1

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 500
    .local v6, "flags":I
    if-lez v6, :cond_2

    .line 501
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v15

    invoke-virtual {v15}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v15

    invoke-virtual {v15, v6}, Landroid/view/Window;->addFlags(I)V

    .line 504
    :cond_2
    const/4 v12, 0x0

    .line 505
    .local v12, "positiveButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    const/4 v7, 0x0

    .line 506
    .local v7, "negativeButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    const-string v9, ""

    .local v9, "okButtonText":Ljava/lang/String;
    const-string v3, ""

    .line 507
    .local v3, "cancelButtonText":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v15

    instance-of v15, v15, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    if-eqz v15, :cond_4

    .line 508
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v15

    check-cast v15, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;->getPositiveButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;

    move-result-object v12

    .line 510
    if-eqz v12, :cond_3

    .line 511
    iget v15, v12, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->textResId:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 512
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v15

    check-cast v15, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;->getNegativeButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;

    move-result-object v7

    .line 514
    if-eqz v7, :cond_4

    .line 515
    iget v15, v7, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->textResId:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 519
    :cond_4
    if-nez v12, :cond_5

    .line 520
    const-string/jumbo v15, "ok_text"

    const/16 v16, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    .line 521
    .local v10, "okResId":I
    if-eqz v10, :cond_5

    .line 522
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 525
    .end local v10    # "okResId":I
    :cond_5
    if-nez v7, :cond_6

    .line 526
    const-string v15, "cancel_text"

    const/16 v16, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 527
    .local v4, "cancelResId":I
    if-eqz v4, :cond_6

    .line 528
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 531
    .end local v4    # "cancelResId":I
    :cond_6
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_7

    .line 532
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 533
    :cond_7
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_8

    .line 534
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 537
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v15

    const-string v16, "cancelable"

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v16

    invoke-virtual/range {v15 .. v16}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 539
    new-instance v15, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$4;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v11}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$4;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Landroid/view/View;)V

    invoke-virtual {v11, v15}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 578
    return-object v11

    .line 444
    .end local v3    # "cancelButtonText":Ljava/lang/String;
    .end local v6    # "flags":I
    .end local v7    # "negativeButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    .end local v9    # "okButtonText":Ljava/lang/String;
    .end local v12    # "positiveButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    .end local v13    # "titleStr":Ljava/lang/String;
    :cond_9
    const-string/jumbo v15, "title_string"

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_0

    .line 462
    .restart local v13    # "titleStr":Ljava/lang/String;
    :pswitch_1
    const/16 v15, 0x8

    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 463
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->btns_margin:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    const/16 v16, 0x8

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 467
    :pswitch_2
    const/16 v15, 0x8

    invoke-virtual {v8, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 468
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/sec/android/app/shealth/common/commonui/R$color;->cancel_ok_green_text_color:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getColor(I)I

    move-result v15

    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setTextColor(I)V

    .line 469
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->btns_margin:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    const/16 v16, 0x8

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 473
    :pswitch_3
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_ok_buttons_container:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    const/16 v16, 0x8

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setVisibility(I)V

    .line 475
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->content_container:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    sget v17, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->tw_dialog_bottom_holo_light:I

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 476
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->content_container:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    sget v17, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->dialog_side_padding:I

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    const/16 v17, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    sget v19, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->dialog_side_padding:I

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    sget v20, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->dialog_bottom_padding:I

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    invoke-virtual/range {v15 .. v19}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_1

    .line 480
    :pswitch_4
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_ok_buttons_container:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    const/16 v16, 0x8

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setVisibility(I)V

    .line 483
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->alert_title_container:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    const/16 v16, 0x8

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 486
    :pswitch_5
    const/16 v15, 0x8

    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 487
    sget v15, Lcom/sec/android/app/shealth/common/commonui/R$id;->btns_margin:I

    invoke-virtual {v11, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    const/16 v16, 0x8

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 491
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->cancelAddSeparator:Landroid/view/View;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setVisibility(I)V

    .line 492
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->addButton:Landroid/widget/TextView;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setVisibility(I)V

    .line 493
    const-string v15, "center_text"

    const/16 v16, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 494
    .local v5, "centerResId":I
    if-eqz v5, :cond_1

    .line 495
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->addButton:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 460
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_4
    .end packed-switch
.end method

.method protected getStringFromResource(I)Ljava/lang/String;
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    .line 589
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 592
    :goto_0
    return-object v0

    .line 590
    :catch_0
    move-exception v0

    .line 592
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getsDrawaleFromResource(I)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    .line 597
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 600
    :goto_0
    return-object v0

    .line 598
    :catch_0
    move-exception v0

    .line 600
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 396
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 398
    .local v0, "decorView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 400
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$3;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$3;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 413
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 685
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v0

    .line 686
    .local v0, "view":Landroid/view/View;
    const/4 v1, 0x3

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$id;->ok_button:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_button:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$id;->add_button:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->temporarilyDisableClick([Landroid/view/View;)V

    .line 693
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$id;->ok_button:I

    if-ne v1, v2, :cond_1

    .line 694
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onPositiveButtonClick(Landroid/view/View;)V

    .line 695
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "dont_dismiss_on_OK_click"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 696
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->tryToDismiss()V

    .line 707
    :cond_0
    :goto_0
    return-void

    .line 698
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_button:I

    if-ne v1, v2, :cond_2

    .line 699
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onNegativeButtonClick(Landroid/view/View;)V

    .line 700
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->tryToDismiss()V

    goto :goto_0

    .line 701
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$id;->add_button:I

    if-ne v1, v2, :cond_0

    .line 702
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onNeutralButtonClick(Landroid/view/View;)V

    .line 703
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isDismissOnAddPressed:Z

    if-eqz v1, :cond_0

    .line 704
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->tryToDismiss()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 182
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 183
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setRetainInstance(Z)V

    .line 184
    const/4 v0, 0x2

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$style;->Dialog_NoTitleBar_And_Frame:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setStyle(II)V

    .line 185
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 204
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$2;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTheme()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$2;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Landroid/content/Context;I)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 20
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 259
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v14

    .line 261
    .local v14, "arguments":Landroid/os/Bundle;
    const-string/jumbo v4, "type"

    invoke-virtual {v14, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_0

    .line 262
    const-string/jumbo v4, "multi_checkbox_dialog"

    invoke-virtual {v14, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;

    .line 264
    :cond_0
    const-string/jumbo v4, "type"

    invoke-virtual {v14, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_1

    .line 265
    const-string v4, "list_display_dialog"

    invoke-virtual {v14, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayList:Ljava/util/ArrayList;

    .line 267
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v14}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getGeneralParentContainer(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v17

    .line 275
    .local v17, "parentContent":Landroid/view/View;
    const/4 v11, 0x0

    .line 276
    .local v11, "alertTitleBackground":Landroid/graphics/drawable/Drawable;
    const-string v4, "alert_title_bg_res_id"

    invoke-virtual {v14, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 278
    .local v13, "alertTitleDrawableResId":I
    const/4 v4, -0x1

    if-eq v13, v4, :cond_2

    .line 279
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getsDrawaleFromResource(I)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 281
    :cond_2
    if-eqz v11, :cond_3

    .line 282
    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$id;->alert_title_container:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout;

    invoke-virtual {v4, v11}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 287
    :cond_3
    const-string v4, "alert_title_color_res_id"

    invoke-virtual {v14, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 288
    .local v12, "alertTitleColorId":I
    const/4 v4, -0x1

    if-eq v12, v4, :cond_4

    .line 290
    :try_start_0
    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$id;->title:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    :cond_4
    :goto_0
    const-string v4, "alert_text_res_id"

    invoke-virtual {v14, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 300
    .local v10, "alertTextResId":I
    const/4 v4, -0x1

    if-eq v10, v4, :cond_8

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getStringFromResource(I)Ljava/lang/String;

    move-result-object v9

    .line 302
    .local v9, "alertText":Ljava/lang/String;
    :goto_1
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 303
    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$id;->alert_text:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 376
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    instance-of v4, v4, Landroid/widget/ListView;

    if-eqz v4, :cond_6

    const-string/jumbo v4, "type"

    invoke-virtual {v14, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_6

    .line 378
    new-instance v4, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;

    .line 380
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    check-cast v4, Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 383
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    instance-of v4, v4, Landroid/widget/ListView;

    if-eqz v4, :cond_7

    const-string/jumbo v4, "type"

    invoke-virtual {v14, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_7

    .line 385
    new-instance v4, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayAdapter:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;

    .line 386
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    check-cast v4, Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayAdapter:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 389
    :cond_7
    :goto_3
    return-object v17

    .line 293
    .end local v9    # "alertText":Ljava/lang/String;
    .end local v10    # "alertTextResId":I
    :catch_0
    move-exception v18

    .line 294
    .local v18, "rnf":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0

    .line 300
    .end local v18    # "rnf":Landroid/content/res/Resources$NotFoundException;
    .restart local v10    # "alertTextResId":I
    :cond_8
    const-string v4, "alert_text"

    invoke-virtual {v14, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 308
    .restart local v9    # "alertText":Ljava/lang/String;
    :cond_9
    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$id;->content_container:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup;

    .line 310
    .local v15, "contentContainer":Landroid/view/ViewGroup;
    invoke-virtual {v15}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 311
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    if-eqz v4, :cond_a

    const-string/jumbo v4, "need_redraw"

    const/4 v5, 0x0

    invoke-virtual {v14, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 313
    :cond_a
    :try_start_1
    const-string v4, "content_resource"

    invoke-virtual {v14, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    .line 315
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    instance-of v4, v4, Landroid/widget/TimePicker;

    if-eqz v4, :cond_b

    .line 316
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    check-cast v4, Landroid/widget/TimePicker;

    invoke-virtual/range {p1 .. p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 332
    :cond_b
    new-instance v16, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 337
    .local v16, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 338
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 340
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    move-object/from16 v0, v16

    invoke-virtual {v15, v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 342
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->contentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    if-eqz v4, :cond_d

    .line 346
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->contentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    sget v19, Lcom/sec/android/app/shealth/common/commonui/R$id;->ok_button:I

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;-><init>(Landroid/view/View;)V

    invoke-interface/range {v3 .. v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V

    goto/16 :goto_2

    .line 320
    .end local v16    # "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    :catch_1
    move-exception v18

    .line 329
    .restart local v18    # "rnf":Landroid/content/res/Resources$NotFoundException;
    goto/16 :goto_3

    .line 353
    .end local v18    # "rnf":Landroid/content/res/Resources$NotFoundException;
    .restart local v16    # "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;

    if-eqz v4, :cond_5

    .line 354
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v3

    .line 356
    .local v3, "initializationListener":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    if-eqz v3, :cond_5

    .line 357
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    sget v19, Lcom/sec/android/app/shealth/common/commonui/R$id;->ok_button:I

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;-><init>(Landroid/view/View;)V

    invoke-interface/range {v3 .. v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V

    goto/16 :goto_2
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getRetainInstance()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 192
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 194
    .local v0, "contentContainer":Landroid/view/ViewGroup;
    if-eqz v0, :cond_1

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->content:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 198
    .end local v0    # "contentContainer":Landroid/view/ViewGroup;
    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroyView()V

    .line 199
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 233
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;

    if-eqz v1, :cond_1

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;->getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;

    move-result-object v0

    .line 238
    .local v0, "dismissController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    if-eqz v0, :cond_1

    .line 240
    invoke-interface {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;->onDismiss()V

    .line 248
    .end local v0    # "dismissController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    :cond_0
    :goto_0
    return-void

    .line 244
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onDismissListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    if-eqz v1, :cond_0

    .line 246
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onDismissListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;->onDismiss(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onPause()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mFocusedView:Landroid/view/View;

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "date_picker_dialog"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "time_picker_dialog"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mFocusedView:Landroid/view/View;

    .line 135
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 141
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onResume()V

    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isDialogScreenLoaded:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->refreshAlertDialogFocusables(Landroid/view/View;)V

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mFocusedView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mFocusedView:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mFocusedView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mFocusedView:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 177
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mIsInstanceStateLoss:Z

    .line 253
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 254
    return-void
.end method

.method public refreshAlertDialogFocusables(Landroid/view/View;)V
    .locals 4
    .param p1, "parentContent"    # Landroid/view/View;

    .prologue
    .line 1514
    if-eqz p1, :cond_0

    .line 1516
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$6;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$6;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Landroid/view/View;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1535
    :cond_0
    return-void
.end method

.method protected setAddButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCenterButtonClickListener;)V
    .locals 0
    .param p1, "addButtonClickListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCenterButtonClickListener;

    .prologue
    .line 639
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->addButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCenterButtonClickListener;

    .line 640
    return-void
.end method

.method protected setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)V
    .locals 0
    .param p1, "cancelButtonClickListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

    .prologue
    .line 626
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->cancelButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

    .line 627
    return-void
.end method

.method protected setContentInitializationListener(Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)V
    .locals 0
    .param p1, "contentInitializationListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    .prologue
    .line 648
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->contentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    .line 649
    return-void
.end method

.method public setDismissOnAddButton(Z)V
    .locals 0
    .param p1, "dismissOnAddPressed"    # Z

    .prologue
    .line 680
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isDismissOnAddPressed:Z

    .line 681
    return-void
.end method

.method public setDoNotDismissOnBackPressed(Z)V
    .locals 0
    .param p1, "doNotDismissOnBackPressed"    # Z

    .prologue
    .line 672
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->doNotDismissOnBackPressed:Z

    .line 673
    return-void
.end method

.method protected setOkButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)V
    .locals 0
    .param p1, "okButtonClickListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    .prologue
    .line 613
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    .line 614
    return-void
.end method

.method public setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)V
    .locals 0
    .param p1, "onBackPressListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    .prologue
    .line 656
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    .line 657
    return-void
.end method

.method public setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V
    .locals 0
    .param p1, "onDismissListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    .prologue
    .line 664
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onDismissListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    .line 665
    return-void
.end method

.method public show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 1280
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1, p2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1281
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1282
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1288
    :cond_0
    :goto_0
    return-void

    .line 1284
    :catch_0
    move-exception v0

    goto :goto_0
.end method
