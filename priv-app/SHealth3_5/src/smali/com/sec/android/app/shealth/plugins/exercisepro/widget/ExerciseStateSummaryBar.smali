.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;
.super Landroid/widget/LinearLayout;
.source "ExerciseStateSummaryBar.java"


# instance fields
.field private ivPolygon:Landroid/widget/ImageView;

.field private leftLine:Landroid/view/View;

.field private llBar:Landroid/widget/LinearLayout;

.field private mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;

.field private rightLine:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const v0, 0x7f0301b3

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 25
    const v0, 0x7f08079d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->ivPolygon:Landroid/widget/ImageView;

    .line 26
    const v0, 0x7f08079e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->llBar:Landroid/widget/LinearLayout;

    .line 27
    const v0, 0x7f08079f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->leftLine:Landroid/view/View;

    .line 28
    const v0, 0x7f0807a0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->rightLine:Landroid/view/View;

    .line 29
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->llBar:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 31
    return-void
.end method

.method private convertDptoPx(F)I
    .locals 4
    .param p1, "dp"    # F

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 127
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 128
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v3

    .line 129
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method


# virtual methods
.method public changePolygonImage(I)V
    .locals 1
    .param p1, "res"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->ivPolygon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 135
    :cond_0
    return-void
.end method

.method public getStateColor()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->getStateBarColor()I

    move-result v0

    return v0
.end method

.method public moveToPolygon(I[I[I)V
    .locals 18
    .param p1, "bpm"    # I
    .param p2, "avgRange"    # [I
    .param p3, "pecentile"    # [I

    .prologue
    .line 34
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->invalidate()V

    .line 63
    const/4 v2, 0x0

    .line 64
    .local v2, "conversionScore":I
    const/16 v5, 0xc8

    .line 65
    .local v5, "maxHR":I
    const/high16 v10, 0x43860000    # 268.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->convertDptoPx(F)I

    move-result v9

    .line 66
    .local v9, "width":I
    const/4 v1, 0x0

    .line 67
    .local v1, "HRScoreMargin":I
    const/4 v3, 0x0

    .line 68
    .local v3, "greenBarMargin":I
    const/4 v4, 0x0

    .line 70
    .local v4, "greenBarWidth":I
    if-gtz p1, :cond_2

    .line 71
    const/4 v2, 0x0

    .line 78
    :goto_0
    int-to-float v10, v2

    int-to-float v11, v5

    div-float/2addr v10, v11

    int-to-float v11, v9

    mul-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 80
    if-gtz v1, :cond_4

    .line 81
    const/4 v1, 0x0

    .line 89
    :goto_1
    const/4 v10, 0x0

    aget v10, p3, v10

    int-to-float v10, v10

    int-to-float v11, v5

    div-float/2addr v10, v11

    int-to-float v11, v9

    mul-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 90
    const/4 v10, 0x1

    aget v10, p3, v10

    const/4 v11, 0x0

    aget v11, p3, v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    int-to-float v11, v5

    div-float/2addr v10, v11

    int-to-float v11, v9

    mul-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 91
    add-int/lit8 v3, v3, 0x7

    .line 92
    add-int v10, v3, v4

    add-int/lit8 v4, v10, -0xe

    .line 95
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->leftLine:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 96
    .local v7, "params1":Landroid/widget/RelativeLayout$LayoutParams;
    iput v3, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 97
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->leftLine:Landroid/view/View;

    invoke-virtual {v10, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->rightLine:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 100
    .local v8, "params2":Landroid/widget/RelativeLayout$LayoutParams;
    iput v4, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 101
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->rightLine:Landroid/view/View;

    invoke-virtual {v10, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 106
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x2

    const/4 v11, -0x2

    invoke-direct {v6, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 107
    .local v6, "params":Landroid/widget/LinearLayout$LayoutParams;
    iput v1, v6, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 108
    const/4 v10, 0x3

    iput v10, v6, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 109
    const/4 v10, 0x0

    aget v10, p2, v10

    move/from16 v0, p1

    if-lt v0, v10, :cond_0

    const/4 v10, 0x1

    aget v10, p2, v10

    move/from16 v0, p1

    if-le v0, v10, :cond_1

    .line 110
    :cond_0
    const/high16 v10, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->convertDptoPx(F)I

    move-result v10

    iput v10, v6, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 112
    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v10, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;

    iget-object v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->positions:[F

    const/4 v11, 0x1

    const/4 v12, 0x0

    aget v12, p3, v12

    int-to-float v12, v12

    int-to-float v13, v5

    div-float/2addr v12, v13

    aput v12, v10, v11

    .line 115
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;

    iget-object v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->positions:[F

    const/4 v11, 0x2

    const/4 v12, 0x0

    aget v12, p3, v12

    int-to-double v12, v12

    const/4 v14, 0x1

    aget v14, p3, v14

    const/4 v15, 0x0

    aget v15, p3, v15

    sub-int/2addr v14, v15

    int-to-double v14, v14

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v14, v14, v16

    add-double/2addr v12, v14

    double-to-float v12, v12

    int-to-float v13, v5

    div-float/2addr v12, v13

    aput v12, v10, v11

    .line 116
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;

    iget-object v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->positions:[F

    const/4 v11, 0x3

    const/4 v12, 0x1

    aget v12, p3, v12

    int-to-float v12, v12

    int-to-float v13, v5

    div-float/2addr v12, v13

    aput v12, v10, v11

    .line 118
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;

    invoke-virtual {v10, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->setStateBarColor(I)V

    .line 119
    return-void

    .line 72
    .end local v6    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v7    # "params1":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "params2":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    move/from16 v0, p1

    if-lt v0, v5, :cond_3

    .line 73
    move v2, v5

    goto/16 :goto_0

    .line 75
    :cond_3
    move/from16 v2, p1

    goto/16 :goto_0

    .line 82
    :cond_4
    const/high16 v10, 0x41300000    # 11.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->convertDptoPx(F)I

    move-result v10

    sub-int v10, v9, v10

    if-lt v1, v10, :cond_5

    .line 83
    const/high16 v10, 0x41300000    # 11.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->convertDptoPx(F)I

    move-result v10

    sub-int v1, v9, v10

    goto/16 :goto_1

    .line 85
    :cond_5
    const/high16 v10, 0x40800000    # 4.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->convertDptoPx(F)I

    move-result v10

    sub-int/2addr v1, v10

    goto/16 :goto_1
.end method
