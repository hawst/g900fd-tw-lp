.class public Lcom/sec/android/app/shealth/receiver/RestoreResultReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RestoreResultReceiver.java"


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 35
    const-class v0, Lcom/sec/android/app/shealth/receiver/RestoreResultReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/receiver/RestoreResultReceiver;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 38
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    if-nez p1, :cond_2

    .line 39
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/receiver/RestoreResultReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "onReceive called but Intent/Action/Context is null"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    :cond_1
    :goto_0
    return-void

    .line 45
    :cond_2
    const-string v4, "com.sec.android.intent.action.RESPONSE_RESTORE_SHEALTH"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 46
    iget-object v4, p0, Lcom/sec/android/app/shealth/receiver/RestoreResultReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Restore from Kies is called"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const/4 v0, 0x0

    .line 48
    .local v0, "error":Z
    const-string v4, "error"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 49
    if-ne v0, v8, :cond_1

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090d52

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 51
    .local v1, "errorToastMsg":Ljava/lang/String;
    invoke-static {p1, v1, v8}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 58
    .end local v0    # "error":Z
    .end local v1    # "errorToastMsg":Ljava/lang/String;
    :cond_3
    const-string v4, "android.intent.action.MY_PACKAGE_REPLACED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 59
    iget-object v4, p0, Lcom/sec/android/app/shealth/receiver/RestoreResultReceiver;->TAG:Ljava/lang/String;

    const-string v5, "ACTION_MY_PACKAGE_REPLACED received"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct {v3, p1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    .line 61
    .local v3, "mSharedPrefsHelper":Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->getUserAccountInfo()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 62
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setUserAccountInfo(Ljava/lang/String;)V

    goto :goto_0

    .line 66
    .end local v3    # "mSharedPrefsHelper":Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    :cond_4
    const-string v4, "com.sec.android.app.shealth.RESTORE_END"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 67
    iget-object v4, p0, Lcom/sec/android/app/shealth/receiver/RestoreResultReceiver;->TAG:Ljava/lang/String;

    const-string v5, "SHEALTH_RESTORE_END_BROADCAST received"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const-string v4, "IS_KIES_RESTORE_MODE"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 69
    .local v2, "isKiesMode":Z
    if-eqz v2, :cond_1

    .line 70
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-static {v4}, Landroid/os/Process;->killProcess(I)V

    goto/16 :goto_0
.end method
