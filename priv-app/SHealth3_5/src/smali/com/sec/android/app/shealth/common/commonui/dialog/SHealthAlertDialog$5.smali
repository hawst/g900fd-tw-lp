.class Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$5;
.super Ljava/lang/Object;
.source "SHealthAlertDialog.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)V
    .locals 0

    .prologue
    .line 1422
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 1428
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1429
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;

    iget-object v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->itemName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1430
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;

    iget-object v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->itemName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1431
    if-eqz p2, :cond_1

    .line 1432
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;

    const/4 v2, 0x1

    iput v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->checked:I

    .line 1428
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1434
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;

    const/4 v2, 0x0

    iput v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->checked:I

    goto :goto_1

    .line 1439
    :cond_2
    return-void
.end method
