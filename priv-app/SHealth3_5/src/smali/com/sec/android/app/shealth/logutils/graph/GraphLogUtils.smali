.class public Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils;
.super Ljava/lang/Object;
.source "GraphLogUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils$GraphToSummaryFragmentSwitchController;,
        Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils$SummaryToGraphFragmentSwitchController;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    return-void
.end method

.method public static addBundleToGraphFragment(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V
    .locals 4
    .param p0, "summaryFragment"    # Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
    .param p1, "graphFragment"    # Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .prologue
    .line 111
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 112
    .local v0, "selectionDataBundle":Landroid/os/Bundle;
    const-string v1, "SUMMARY_VIEW_DATE"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getSelectedDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 114
    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->setArguments(Landroid/os/Bundle;)V

    .line 115
    return-void
.end method

.method public static convertCaloriesToString(D)Ljava/lang/String;
    .locals 1
    .param p0, "calories"    # D

    .prologue
    .line 64
    double-to-int v0, p0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static countMonthAverageValue(JJF)F
    .locals 1
    .param p0, "currentTime"    # J
    .param p2, "monthTime"    # J
    .param p4, "valueForMonth"    # F

    .prologue
    .line 75
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils;->getPassedDaysCountForMonth(JJ)I

    move-result v0

    int-to-float v0, v0

    div-float v0, p4, v0

    return v0
.end method

.method private static getPassedDaysCountForMonth(JJ)I
    .locals 6
    .param p0, "currentTime"    # J
    .param p2, "anyTimeInMonth"    # J

    .prologue
    const/4 v5, 0x5

    .line 43
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 44
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v1

    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 46
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 47
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 51
    :goto_0
    return v1

    .line 50
    :cond_0
    invoke-virtual {v0, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 51
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    goto :goto_0
.end method
