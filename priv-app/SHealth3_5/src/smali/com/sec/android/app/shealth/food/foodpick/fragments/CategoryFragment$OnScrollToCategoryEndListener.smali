.class Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;
.super Ljava/lang/Object;
.source "CategoryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnScrollToCategoryEndListener"
.end annotation


# instance fields
.field private volatile isTaskRunning:Z

.field private mItemsAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchByCategoryListResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V
    .locals 1

    .prologue
    .line 378
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->isTaskRunning:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;

    .prologue
    .line 378
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->mItemsAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;

    .prologue
    .line 378
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->mSearchByCategoryListResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;

    return-object p1
.end method

.method static synthetic access$1902(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;
    .param p1, "x1"    # Z

    .prologue
    .line 378
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->isTaskRunning:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;

    .prologue
    .line 378
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->setResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;

    .prologue
    .line 378
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->setAdapter(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;)V

    return-void
.end method

.method private setAdapter(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 389
    .local p1, "itemsAdapter":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->mItemsAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;

    .line 390
    return-void
.end method

.method private setResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 385
    .local p1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->mSearchByCategoryListResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;

    .line 386
    return-void
.end method


# virtual methods
.method public onEnd(Landroid/widget/ListView;)V
    .locals 4
    .param p1, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 394
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->isTaskRunning:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->mSearchByCategoryListResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$900(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;->isAllDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 419
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->isTaskRunning:Z

    .line 398
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->mSearchByCategoryListResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$900(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;->createNextSearchListRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;

    move-result-object v1

    .line 399
    .local v1, "request":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, p0, v2, p1, v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;Landroid/content/Context;Landroid/widget/ListView;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;)V

    .line 418
    .local v0, "loadMoreTask":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$2000(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;->addTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    goto :goto_0
.end method
