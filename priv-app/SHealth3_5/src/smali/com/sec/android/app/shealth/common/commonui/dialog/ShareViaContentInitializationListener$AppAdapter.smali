.class Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ShareViaContentInitializationListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/content/pm/ResolveInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private pm:Landroid/content/pm/PackageManager;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pm"    # Landroid/content/pm/PackageManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/pm/PackageManager;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p3, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$layout;->share_item:I

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;->pm:Landroid/content/pm/PackageManager;

    .line 131
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;->pm:Landroid/content/pm/PackageManager;

    .line 133
    return-void
.end method

.method private bindView(ILandroid/view/View;)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "row"    # Landroid/view/View;

    .prologue
    .line 160
    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$id;->label:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 161
    .local v1, "label":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$id;->icon:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 163
    .local v0, "icon":Landroid/widget/ImageView;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 165
    return-void
.end method

.method private newView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 151
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$layout;->share_item:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 154
    .local v1, "view":Landroid/view/View;
    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->share_popup_item_selector:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 155
    return-object v1
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 138
    if-nez p2, :cond_0

    .line 140
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;->newView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 142
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;->bindView(ILandroid/view/View;)V

    .line 143
    return-object p2
.end method
