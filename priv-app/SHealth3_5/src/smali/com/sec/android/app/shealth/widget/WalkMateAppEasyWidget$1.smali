.class Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget$1;
.super Ljava/lang/Object;
.source "WalkMateAppEasyWidget.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget$1;->this$0:Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 404
    # getter for: Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "########## [StatusActivity] onServiceConnected ##########"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget$1;->this$0:Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    check-cast p2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$WalkingMateDayStepBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$WalkingMateDayStepBinder;->getService()Landroid/app/Service;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    # setter for: Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->access$102(Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget$1;->this$0:Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    # getter for: Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    invoke-static {v0}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->access$100(Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 408
    # getter for: Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mDayStepService is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :goto_0
    return-void

    .line 412
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mDayStepService is not null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget$1;->this$0:Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->access$102(Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 419
    return-void
.end method
