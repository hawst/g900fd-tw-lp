.class public Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;
.super Ljava/lang/Object;
.source "CignaLibraryMoreUtils.java"


# static fields
.field private static final DIALOG_FILTER:Ljava/lang/String; = "dialog_filter"

.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;


# instance fields
.field private mIsFilterCategory:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->mIsFilterCategory:Z

    .line 36
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;

    .line 32
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;

    return-object v0
.end method

.method private showListChooseDialog(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0019

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 58
    .local v1, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v4, 0x7f090c63

    const/16 v5, 0xc

    invoke-direct {v0, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 59
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 60
    const/4 v4, 0x2

    new-array v4, v4, [Z

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->mIsFilterCategory:Z

    aput-boolean v5, v4, v2

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->mIsFilterCategory:Z

    if-nez v5, :cond_0

    move v2, v3

    :cond_0
    aput-boolean v2, v4, v3

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 61
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v2

    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "dialog_filter"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method private showMoreForContextMenu(Landroid/content/Context;Landroid/support/v4/app/Fragment;Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p3, "items"    # Landroid/view/MenuItem;

    .prologue
    .line 44
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080c8d

    if-ne v0, v1, :cond_1

    .line 46
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->startDeleteFavoriteActivity(Landroid/app/Activity;)V

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 48
    .restart local p1    # "context":Landroid/content/Context;
    :cond_1
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080ca6

    if-ne v0, v1, :cond_0

    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->showListChooseDialog(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private startDeleteFavoriteActivity(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 77
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_SORTING_TYPE"

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->mIsFilterCategory:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 79
    const-string v1, "EXTRA_NAME_DELETE_TYPE"

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 80
    const/16 v1, 0x64

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 81
    return-void
.end method


# virtual methods
.method public setFilterCategory(Z)V
    .locals 0
    .param p1, "isCategory"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->mIsFilterCategory:Z

    .line 67
    return-void
.end method

.method public showMoreForContextMenu(Landroid/content/Context;Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # Landroid/view/MenuItem;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->showMoreForContextMenu(Landroid/content/Context;Landroid/support/v4/app/Fragment;Landroid/view/MenuItem;)V

    .line 40
    return-void
.end method
