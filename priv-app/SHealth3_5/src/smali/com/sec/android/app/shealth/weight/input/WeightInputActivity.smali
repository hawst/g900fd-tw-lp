.class public Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;
.source "WeightInputActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;,
        Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isConncetedToHWKeyboard:Z

.field protected isacc:Z

.field protected isfromLog:Z

.field private mComment:Ljava/lang/String;

.field protected mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

.field protected mDateFormat:Ljava/text/SimpleDateFormat;

.field private mRecievedTime:J

.field protected mTimeFormat:Ljava/text/SimpleDateFormat;

.field private mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

.field private mWasEdited:Z

.field private mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

.field private mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

.field private mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

.field private mWeightStartValue:F

.field private mWeightUnit:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

.field private mWeightUnitText:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    const-class v0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;-><init>()V

    .line 91
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mDateFormat:Ljava/text/SimpleDateFormat;

    .line 92
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mTimeFormat:Ljava/text/SimpleDateFormat;

    .line 101
    new-instance v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mComment:Ljava/lang/String;

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isConncetedToHWKeyboard:Z

    .line 426
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isConncetedToHWKeyboard:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    return-object v0
.end method

.method private initializeInputModule(Landroid/widget/RelativeLayout;)V
    .locals 6
    .param p1, "layout"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 487
    const v2, 0x7f080473

    invoke-virtual {p1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightUnitText:Landroid/widget/TextView;

    .line 488
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightUnit:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900c2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 489
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightUnitText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900c3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 494
    :goto_0
    const v2, 0x7f080c5c

    invoke-virtual {p1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    .line 495
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    const/high16 v3, 0x40000000    # 2.0f

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightUnit:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v3

    const/high16 v4, 0x43fa0000    # 500.0f

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightUnit:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setInputRange(FF)V

    .line 497
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    const v3, 0x3dcccccd    # 0.1f

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setMoveDistance(F)V

    .line 498
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightUnit:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setUnit(Ljava/lang/CharSequence;)V

    .line 499
    new-instance v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v0

    .line 500
    .local v0, "height":F
    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/weight/utils/WeightNormalRangeHelper;->getNormalRangeValues(Landroid/content/Context;F)Landroid/util/Pair;

    move-result-object v1

    .line 502
    .local v1, "normalRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    sget-object v2, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v3, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR_EXTENDED:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 503
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v4

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v3, v4, v2}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setNormalRange(FF)V

    .line 505
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightStartValue:F

    .line 506
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    iget v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightStartValue:F

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setValue(F)V

    .line 508
    sget-object v2, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v3, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR_EXTENDED:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 509
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    sget-object v3, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->EMPTY:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setAdditionalDrawingType(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;)V

    .line 512
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    new-instance v3, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$4;-><init>(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setValueOutOfRangeListener(Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;)V

    .line 523
    return-void

    .line 492
    .end local v0    # "height":F
    .end local v1    # "normalRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightUnitText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900c1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private isCommentChanged()Z
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mComment:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setContainerHeight()V
    .locals 4

    .prologue
    .line 526
    const v2, 0x7f080632

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 527
    .local v0, "frameLayout":Landroid/view/ViewGroup;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 528
    .local v1, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0b27

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 529
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 530
    return-void
.end method

.method private showAlertDialog(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;)V
    .locals 4
    .param p1, "input"    # Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;

    .prologue
    const/4 v2, 0x0

    .line 245
    invoke-interface {p1}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;->getAlertMessage()Ljava/lang/String;

    move-result-object v0

    .line 246
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-interface {p1}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;->getAlertTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$2;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$2;-><init>(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 265
    return-void
.end method

.method private showInvalidDateDialog()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 268
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090286

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090084

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090044

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 274
    return-void
.end method


# virtual methods
.method protected checkOutOfRange()Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;
    .locals 3

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->isOutOfRange()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;-><init>(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;Landroid/content/Context;Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeSampleTime(Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;)J
    .locals 9
    .param p1, "commonDaoWithDateTime"    # Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;

    .prologue
    const-wide/16 v7, 0x1

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getMeasureTimeInMillis()J

    move-result-wide v1

    .line 413
    .local v1, "dateTimePickerTime":J
    const-string v0, "Edit"

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mMode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getSampleTime()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getTimeWithoutSeconds(J)J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-nez v0, :cond_1

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getSampleTime()J

    move-result-wide v1

    .line 419
    .end local v1    # "dateTimePickerTime":J
    :cond_0
    :goto_0
    return-wide v1

    .line 417
    .restart local v1    # "dateTimePickerTime":J
    :cond_1
    const-wide/32 v3, 0xea60

    add-long/2addr v3, v1

    sub-long/2addr v3, v7

    sget-object v5, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;->getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Ljava/util/List;

    move-result-object v6

    .line 419
    .local v6, "minuteDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;>;"
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getSampleTime()J

    move-result-wide v3

    add-long v1, v3, v7

    goto :goto_0
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->customizeActionBar()V

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 179
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    .line 180
    return-void
.end method

.method protected disableInputModule()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->setEnabled(Z)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setEnabled(Z)V

    .line 199
    return-void
.end method

.method protected getContentView()Landroid/view/View;
    .locals 4

    .prologue
    .line 474
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getIntentExtras()V

    .line 475
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->setContainerHeight()V

    .line 476
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 477
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0302c0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 478
    .local v1, "layout":Landroid/widget/RelativeLayout;
    new-instance v2, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    .line 479
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightUnit:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    .line 480
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->initSelectedDate()V

    .line 482
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->initializeInputModule(Landroid/widget/RelativeLayout;)V

    .line 483
    return-object v1
.end method

.method protected getIntentExtras()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 359
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INPUT_ACTIVITY_MODE_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INPUT_ACTIVITY_MODE_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mMode:Ljava/lang/String;

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "log"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isfromLog:Z

    .line 362
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accessory"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isacc:Z

    .line 367
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INPUT_ACTIVITY_DATA_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 368
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INPUT_ACTIVITY_DATA_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    .line 372
    return-void

    .line 365
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Input mode wasn\'t added to intent!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 370
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Data wasn\'t put in extra"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected getTimeWithoutSeconds(J)J
    .locals 3
    .param p1, "time"    # J

    .prologue
    const/4 v2, 0x0

    .line 298
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 299
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 300
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 301
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 302
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method protected initSelectedDate()V
    .locals 3

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getSampleTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mRecievedTime:J

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mDateFormat:Ljava/text/SimpleDateFormat;

    iget-wide v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mRecievedTime:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mSelectedDate:Ljava/lang/String;

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mTimeFormat:Ljava/text/SimpleDateFormat;

    iget-wide v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mRecievedTime:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mSelectedTime:Ljava/lang/String;

    .line 353
    return-void
.end method

.method protected isDateChanged()Z
    .locals 6

    .prologue
    .line 282
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getMeasureTimeInMillis()J

    move-result-wide v0

    .line 283
    .local v0, "sampleTime":J
    iget-wide v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mRecievedTime:J

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getTimeWithoutSeconds(J)J

    move-result-wide v2

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getTimeWithoutSeconds(J)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected isEmpty()Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->isEmpty()Z

    move-result v0

    return v0
.end method

.method protected isInputChanged()Z
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->stopInertiaOfInputModules()V

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isDateChanged()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isValuesChanged()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isCommentChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isValuesChanged()Z
    .locals 2

    .prologue
    .line 563
    iget v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightStartValue:F

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isValuesCorrect()Z
    .locals 8

    .prologue
    .line 213
    const/4 v3, 0x1

    .line 214
    .local v3, "isCorrect":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 215
    const/4 v3, 0x0

    .line 217
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->checkOutOfRange()Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;

    move-result-object v2

    .line 218
    .local v2, "input":Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;
    if-eqz v2, :cond_2

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 220
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->showAlertDialog(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;)V

    .line 221
    :cond_1
    const/4 v3, 0x0

    .line 223
    :cond_2
    new-instance v6, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 224
    .local v0, "birthDate":J
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getMeasureTimeInMillis()J

    move-result-wide v4

    .line 225
    .local v4, "selectedDate":J
    cmp-long v6, v4, v0

    if-gez v6, :cond_3

    .line 226
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->showInvalidDateDialog()V

    .line 227
    const/4 v3, 0x0

    .line 229
    :cond_3
    return v3
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isValuesCorrect()Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    :goto_0
    return-void

    .line 237
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isInputChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    const-string v0, "Add"

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->saveValues(Z)V

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->showSavedToast()V

    .line 241
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->finish()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v1, 0x1

    .line 574
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 575
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v0, v1, :cond_1

    .line 576
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isConncetedToHWKeyboard:Z

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 578
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 579
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isConncetedToHWKeyboard:Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 136
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onCreate(Landroid/os/Bundle;)V

    .line 137
    const v1, 0x7f080631

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    const/16 v3, 0x500

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getInputSourceType()I

    move-result v1

    const v2, 0x3f7a1

    if-eq v1, v2, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->disableInputModule()V

    .line 144
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 145
    .local v0, "config":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v1, v4, :cond_4

    .line 146
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isConncetedToHWKeyboard:Z

    .line 152
    :cond_1
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isacc:Z

    if-eqz v1, :cond_2

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->disableInputModule()V

    .line 156
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getComment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mComment:Ljava/lang/String;

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mComment:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setSelection(I)V

    .line 160
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    new-instance v2, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$1;-><init>(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 172
    return-void

    .line 148
    :cond_4
    iget v1, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 149
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isConncetedToHWKeyboard:Z

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->stopInertiaOfInputModules()V

    .line 308
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onPause()V

    .line 309
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 313
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onResume()V

    .line 314
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 315
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    const v3, 0x7f080632

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 316
    .local v0, "inputContainer":Landroid/widget/FrameLayout;
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 317
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getRootView()Landroid/view/View;

    move-result-object v2

    .line 319
    .local v2, "rootView":Landroid/view/View;
    new-instance v3, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;

    invoke-direct {v3, p0, v2}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$3;-><init>(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;Landroid/view/View;)V

    const-wide/16 v4, 0x190

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 337
    return-void
.end method

.method protected onSaveButtonSelect(Z)V
    .locals 7
    .param p1, "isAddMode"    # Z

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x0

    .line 386
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->stopInertiaOfInputModules()V

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isValuesCorrect()Z

    move-result v1

    if-nez v1, :cond_0

    .line 389
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f09092d

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    const/16 v5, 0x1f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 404
    :goto_0
    return-void

    .line 393
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->saveValues(Z)V

    .line 394
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isInputChanged()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 395
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->showSavedToast()V

    .line 397
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isfromLog:Z

    if-eqz v1, :cond_3

    .line 398
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 399
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 400
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 403
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->finish()V

    goto :goto_0
.end method

.method protected saveValues(Z)V
    .locals 10
    .param p1, "isAdd"    # Z

    .prologue
    .line 534
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightUnit:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertUnitToKg(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v5

    .line 535
    .local v5, "weightValue":F
    new-instance v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 536
    .local v2, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->computeSampleTime(Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;)J

    move-result-wide v3

    .line 538
    .local v3, "sampleTime":J
    const-wide/16 v6, 0x0

    cmp-long v6, v3, v6

    if-gez v6, :cond_0

    .line 539
    const-wide/16 v3, 0x0

    .line 540
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v6, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->setWeight(F)V

    .line 541
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->setHeight(F)V

    .line 542
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getMemoContent()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->setComment(Ljava/lang/String;)V

    .line 543
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v6, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->setSampleTime(J)V

    .line 544
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-interface {v6, v7}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 545
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-interface {v6, v7, v8}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v0

    .line 546
    .local v0, "currentLatestWeight":F
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v6

    invoke-static {v6, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v6

    if-nez v6, :cond_1

    .line 548
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-interface {v6, v7, v8}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v6

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    .line 549
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 554
    :cond_1
    :goto_0
    const/16 v6, 0x2711

    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    const-string v8, "TIME"

    invoke-virtual {v7, v8, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "INPUT_ACTIVITY_WAS_CHANGED"

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "INPUT_ACTIVITY_DATA_KEY"

    iget-object v9, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->setResult(ILandroid/content/Intent;)V

    .line 558
    const-string v6, "com.sec.android.app.shealth.weight"

    const-string v7, "2001"

    invoke-static {p0, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    return-void

    .line 550
    :catch_0
    move-exception v1

    .line 551
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v6, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->TAG:Ljava/lang/String;

    invoke-static {v6, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected showSavedToast()V
    .locals 3

    .prologue
    .line 343
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090835

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 344
    return-void
.end method

.method protected stopInertiaOfInputModules()V
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->stopInertia()V

    .line 569
    return-void
.end method
