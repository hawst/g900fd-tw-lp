.class public Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;
.super Ljava/lang/Object;
.source "CircleViewAnimationRunner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;,
        Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;
    }
.end annotation


# static fields
.field private static final segments:[[F


# instance fields
.field private animationEndListener:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;

.field private final circleView:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;

.field private final from:F

.field private final to:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [[F

    const/4 v1, 0x0

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [F

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->segments:[[F

    return-void

    nop

    :array_0
    .array-data 4
        0x3d23d70a    # 0.04f
        0x3f37ced9    # 0.718f
        0x3f570a3d    # 0.84f
    .end array-data

    :array_1
    .array-data 4
        0x3f5851ec    # 0.845f
        0x3f7f7cee    # 0.998f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;FF)V
    .locals 0
    .param p1, "circleView"    # Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;
    .param p2, "from"    # F
    .param p3, "to"    # F

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->circleView:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;

    .line 29
    iput p2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->from:F

    .line 30
    iput p3, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->to:F

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->from:F

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->circleView:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->to:F

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->animationEndListener:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;

    return-object v0
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 13
    .param p1, "input"    # F

    .prologue
    const/4 v12, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    .line 65
    div-float v0, p1, v11

    .line 66
    .local v0, "_loc_5":F
    sget-object v6, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->segments:[[F

    array-length v1, v6

    .line 67
    .local v1, "_loc_6":I
    int-to-float v6, v1

    mul-float/2addr v6, v0

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v4, v6

    .line 68
    .local v4, "_loc_9":I
    sget-object v6, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->segments:[[F

    array-length v6, v6

    if-lt v4, v6, :cond_0

    .line 69
    sget-object v6, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->segments:[[F

    array-length v6, v6

    add-int/lit8 v4, v6, -0x1

    .line 71
    :cond_0
    int-to-float v6, v4

    int-to-float v7, v1

    div-float v7, v11, v7

    mul-float/2addr v6, v7

    sub-float v6, v0, v6

    int-to-float v7, v1

    mul-float v2, v6, v7

    .line 72
    .local v2, "_loc_7":F
    sget-object v6, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->segments:[[F

    aget-object v3, v6, v4

    .line 73
    .local v3, "_loc_8":[F
    const/4 v6, 0x0

    aget v7, v3, v12

    const/high16 v8, 0x40000000    # 2.0f

    sub-float v9, v11, v2

    mul-float/2addr v8, v9

    const/4 v9, 0x1

    aget v9, v3, v9

    aget v10, v3, v12

    sub-float/2addr v9, v10

    mul-float/2addr v8, v9

    const/4 v9, 0x2

    aget v9, v3, v9

    aget v10, v3, v12

    sub-float/2addr v9, v10

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    mul-float/2addr v8, v2

    add-float/2addr v7, v8

    mul-float/2addr v7, v11

    add-float v5, v6, v7

    .line 75
    .local v5, "ret":F
    return v5
.end method

.method public setAnimationEndListener(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;)V
    .locals 0
    .param p1, "animationEndListener"    # Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->animationEndListener:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;

    .line 39
    return-void
.end method

.method public start(II)V
    .locals 1
    .param p1, "duration"    # I
    .param p2, "frequency"    # I

    .prologue
    .line 34
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;-><init>(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;II)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->start()V

    .line 35
    return-void
.end method
