.class Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;
.super Landroid/database/ContentObserver;
.source "AddFavoritesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/AddFavoritesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RegistryContentObserver"
.end annotation


# instance fields
.field private mActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/home/AddFavoritesActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "activity"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 396
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 397
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    .line 398
    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    return-object v0
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    new-instance v1, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver$1;-><init>(Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 426
    :cond_0
    return-void
.end method
