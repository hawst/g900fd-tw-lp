.class public Lcom/sec/android/app/shealth/spo2/common/Spo2InformationActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "Spo2InformationActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/common/Spo2InformationActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0900e3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 54
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const-string v2, "layout_inflater"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/spo2/common/Spo2InformationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 40
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 41
    const v2, 0x7f030225

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 45
    .local v1, "view":Landroid/view/View;
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/spo2/common/Spo2InformationActivity;->setContentView(Landroid/view/View;)V

    .line 46
    return-void

    .line 43
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    const v2, 0x7f030227

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "view":Landroid/view/View;
    goto :goto_0
.end method
