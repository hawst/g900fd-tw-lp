.class public Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;
.super Landroid/app/Dialog;
.source "ThermoHygrometerDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private btnCancel:Landroid/widget/Button;

.field private btnOk:Landroid/widget/Button;

.field final handler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mInfoView:Landroid/view/View;

.field final runnable:Ljava/lang/Runnable;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const v0, 0x1030007

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 30
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->handler:Landroid/os/Handler;

    .line 73
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog$1;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->runnable:Ljava/lang/Runnable;

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->mContext:Landroid/content/Context;

    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->initLayout()V

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->mInfoView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->setContentView(Landroid/view/View;)V

    .line 41
    return-void
.end method

.method private initLayout()V
    .locals 3

    .prologue
    .line 44
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 45
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030095

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->mInfoView:Landroid/view/View;

    .line 46
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->mInfoView:Landroid/view/View;

    const v2, 0x7f0800b7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->btnOk:Landroid/widget/Button;

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->mInfoView:Landroid/view/View;

    const v2, 0x7f0800b5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->btnCancel:Landroid/widget/Button;

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->btnOk:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->btnCancel:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->mInfoView:Landroid/view/View;

    const v2, 0x7f08004d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->titleView:Landroid/widget/TextView;

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->titleView:Landroid/widget/TextView;

    const v2, 0x7f090242

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 52
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 57
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 71
    :goto_0
    :pswitch_0
    return-void

    .line 59
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->dismiss()V

    goto :goto_0

    .line 63
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->runnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0x12c

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x7f0800b5
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
