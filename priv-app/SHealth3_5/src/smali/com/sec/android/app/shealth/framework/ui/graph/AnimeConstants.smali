.class public final Lcom/sec/android/app/shealth/framework/ui/graph/AnimeConstants;
.super Ljava/lang/Object;


# static fields
.field public static final BUBBLE_EXISTANCE_TIME:I = 0xfa0

.field public static final DEFAULT_DELAY:I = 0x3e8

.field public static final DEFAULT_DURATION:I = 0x5dc

.field public static final DELAY_SMALL:I = 0x64

.field public static final DELAY_VERY_SMALL:I = 0x32

.field public static final DIE_APPLE:Z = true

.field public static final DIE_APPLE_DURATION:I = 0x7d

.field public static final DURATION_1000:I = 0x3e8

.field public static final DURATION_1200:I = 0x4b0

.field public static final DURATION_1500:I = 0x5dc

.field public static final DURATION_250:I = 0xfa

.field public static final DURATION_300:I = 0x12c

.field public static final DURATION_400:I = 0x190

.field public static final DURATION_500:I = 0x1f4

.field public static final DURATION_750:I = 0x2ee

.field public static final DURATION_800:I = 0x320

.field public static final HANDLER_EXISTANCE_TIME:I = 0x92e

.field public static final INSTANT_DURATION:I = 0x190

.field public static final INSTANT_DURATION_HALF:I = 0xc8


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
