.class public Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "UpdatePrivacyPolicy.java"


# instance fields
.field private isAgreeCheck:Z

.field private mAgreeCheck:Landroid/widget/CheckBox;

.field private mCheckLayout:Landroid/widget/RelativeLayout;

.field private mPrivacyPolicyValue:F

.field private mTermsOfUseValue:F

.field private nextButtonLayout:Landroid/widget/RelativeLayout;

.field private ppParagraph1:Landroid/widget/TextView;

.field private ppParagraph1open:Landroid/widget/TextView;

.field private ppParagraph2:Landroid/widget/TextView;

.field private ppParagraph2open:Landroid/widget/TextView;

.field private ppParagraph3:Landroid/widget/TextView;

.field private ppParagraph3_2:Landroid/widget/TextView;

.field private ppParagraph3open:Landroid/widget/TextView;

.field private ppParagraph3open2:Landroid/widget/TextView;

.field private ppParagraph4:Landroid/widget/TextView;

.field private ppParagraph4open:Landroid/widget/TextView;

.field private ppParagraph5:Landroid/widget/TextView;

.field private ppParagraph5open:Landroid/widget/TextView;

.field private ppSub:Landroid/widget/TextView;

.field private ppSubOpen:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mCheckLayout:Landroid/widget/RelativeLayout;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->isAgreeCheck:Z

    .line 34
    iput v1, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mTermsOfUseValue:F

    .line 35
    iput v1, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mPrivacyPolicyValue:F

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->isAgreeCheck:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->isAgreeCheck:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mTermsOfUseValue:F

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mPrivacyPolicyValue:F

    return v0
.end method

.method private initLayout()V
    .locals 11

    .prologue
    const v10, 0x7f090e70

    const v6, 0x7f090e6f

    const v9, 0x7f090e52

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 95
    const v3, 0x7f03018d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->setContentView(I)V

    .line 97
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, "strLanguage":Ljava/lang/String;
    if-eqz v1, :cond_3

    const-string v3, "kk"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 102
    const v3, 0x7f0806ae

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e72

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    const v3, 0x7f080694

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e72

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    :goto_0
    const v3, 0x7f0806a9

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    .line 130
    const v3, 0x7f0806aa

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph2:Landroid/widget/TextView;

    .line 131
    const v3, 0x7f0806ab

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph3:Landroid/widget/TextView;

    .line 132
    const v3, 0x7f0806ac

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph3_2:Landroid/widget/TextView;

    .line 133
    const v3, 0x7f0806b2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph4:Landroid/widget/TextView;

    .line 134
    const v3, 0x7f0806b5

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph5:Landroid/widget/TextView;

    .line 136
    const v3, 0x7f08068f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    .line 137
    const v3, 0x7f080690

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph2open:Landroid/widget/TextView;

    .line 138
    const v3, 0x7f080691

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph3open:Landroid/widget/TextView;

    .line 139
    const v3, 0x7f080692

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph3open2:Landroid/widget/TextView;

    .line 140
    const v3, 0x7f080698

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph4open:Landroid/widget/TextView;

    .line 141
    const v3, 0x7f08069b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph5open:Landroid/widget/TextView;

    .line 143
    const v3, 0x7f0806b0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f090e74

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e75

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e76

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e77

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e78

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e79

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e7a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e7b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    const v3, 0x7f080696

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f090e74

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e75

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e76

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e77

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e78

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e79

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e7a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e7b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    const v3, 0x7f0806b7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mCheckLayout:Landroid/widget/RelativeLayout;

    .line 165
    const v3, 0x7f0806b8

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;

    .line 167
    const v3, 0x7f080613

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->nextButtonLayout:Landroid/widget/RelativeLayout;

    .line 168
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->nextButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090042

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v2, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 171
    .local v2, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "countryName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v1, :cond_4

    const-string v3, "ko"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 176
    const v3, 0x7f0806b6

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 177
    const v3, 0x7f08069c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 179
    const v3, 0x7f0806a7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 180
    const v3, 0x7f08068d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f0905c4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 194
    :goto_1
    if-eqz v1, :cond_0

    const-string v3, "fr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "CA"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 196
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppSub:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090e54

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppSubOpen:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090e54

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "DE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 203
    if-eqz v1, :cond_1

    const-string v3, "de"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 205
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090e4f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph3:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090e50

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph3_2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090e51

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph4:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph5:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090e53

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph2open:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090e4f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph3open:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090e50

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph3open2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090e51

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph4open:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph5open:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090e53

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CH"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 222
    if-eqz v1, :cond_2

    const-string v3, "de"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 224
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph4:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->ppParagraph4open:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    :cond_2
    return-void

    .line 113
    .end local v0    # "countryName":Ljava/lang/String;
    .end local v2    # "systemLocale":Ljava/util/Locale;
    :cond_3
    const v3, 0x7f0806ae

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f090e6e

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e72

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    const v3, 0x7f080694

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f090e6e

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e72

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 184
    .restart local v0    # "countryName":Ljava/lang/String;
    .restart local v2    # "systemLocale":Ljava/util/Locale;
    :cond_4
    const v3, 0x7f0806b6

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 185
    const v3, 0x7f08069c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 187
    const v3, 0x7f0806a7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 188
    const v3, 0x7f08068d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f090e43

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    goto/16 :goto_1
.end method


# virtual methods
.method public closeScreen()V
    .locals 0

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->finish()V

    .line 240
    return-void
.end method

.method protected customizeActionBar()V
    .locals 0

    .prologue
    .line 250
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 252
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->finish()V

    .line 235
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "termOfUseVersion"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mTermsOfUseValue:F

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "privacyVersion"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mPrivacyPolicyValue:F

    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->initLayout()V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mCheckLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy$1;-><init>(Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy$2;-><init>(Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;->nextButtonLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy$3;-><init>(Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 244
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 245
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 257
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 258
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 262
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 263
    return-void
.end method
