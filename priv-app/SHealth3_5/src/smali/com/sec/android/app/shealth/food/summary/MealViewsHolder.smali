.class Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;
.super Ljava/lang/Object;
.source "MealViewsHolder.java"


# instance fields
.field private final mDoubleLineTextView:Landroid/view/View;

.field private final mDoubleLineView:Landroid/view/View;

.field private final mMealAddButton:Landroid/view/View;

.field private final mMealCaloriesUnitView:Landroid/view/View;

.field private final mMealCaloriesView:Landroid/view/View;

.field private final mMealImageView:Landroid/view/View;

.field private final mMealViewButton:Landroid/view/View;

.field private final mSingleLineTextView:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p1, "mealImageView"    # Landroid/view/View;
    .param p2, "mealViewButton"    # Landroid/view/View;
    .param p3, "singleLineTextView"    # Landroid/view/View;
    .param p4, "doubleLineTextView"    # Landroid/view/View;
    .param p5, "mealAddButton"    # Landroid/view/View;
    .param p6, "doubleLineView"    # Landroid/view/View;
    .param p7, "mealCaloriesView"    # Landroid/view/View;
    .param p8, "mealCaloriesUnitView"    # Landroid/view/View;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mMealImageView:Landroid/view/View;

    .line 37
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mMealViewButton:Landroid/view/View;

    .line 38
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mSingleLineTextView:Landroid/view/View;

    .line 39
    iput-object p4, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mDoubleLineTextView:Landroid/view/View;

    .line 40
    iput-object p5, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mMealAddButton:Landroid/view/View;

    .line 41
    iput-object p6, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mDoubleLineView:Landroid/view/View;

    .line 42
    iput-object p7, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mMealCaloriesView:Landroid/view/View;

    .line 44
    iput-object p8, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mMealCaloriesUnitView:Landroid/view/View;

    .line 45
    return-void
.end method


# virtual methods
.method getDoubleLineTextView()Landroid/view/View;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mDoubleLineTextView:Landroid/view/View;

    return-object v0
.end method

.method getDoubleLineView()Landroid/view/View;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mDoubleLineView:Landroid/view/View;

    return-object v0
.end method

.method getMealAddButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mMealAddButton:Landroid/view/View;

    return-object v0
.end method

.method public getMealCaloriesUnitView()Landroid/view/View;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mMealCaloriesUnitView:Landroid/view/View;

    return-object v0
.end method

.method getMealCaloriesView()Landroid/view/View;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mMealCaloriesView:Landroid/view/View;

    return-object v0
.end method

.method getMealImageView()Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mMealImageView:Landroid/view/View;

    return-object v0
.end method

.method getMealViewButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mMealViewButton:Landroid/view/View;

    return-object v0
.end method

.method getSingleLineTextView()Landroid/view/View;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->mSingleLineTextView:Landroid/view/View;

    return-object v0
.end method
