.class public Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;
.super Ljava/lang/Object;
.source "FeatureManager.java"


# static fields
.field public static final ACTIVE_TIME_MONITOR:I = 0x2

.field public static final H_DEVICE:I = 0x4

.field public static final INACTIVE_TIMER:I = 0x3

.field public static final J_DEVICE:I = 0x7

.field public static final K_DEVICE:I = 0x5

.field public static final MAX_FEATURE:I = 0xa

.field public static final PEDOMETER:I = 0x8

.field private static final SEC_FEATURE_LEVEL_SENSOR_HUB:Ljava/lang/String; = "com.sec.feature.sensorhub"

.field public static final SENSORHUB:I = 0x9

.field public static final STEP_LEVEL_MONITOR:I = 0x1

.field public static final T_DEVICE:I = 0x6

.field private static isInitiated:Z

.field private static mContext:Landroid/content/Context;

.field private static mInstance:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;


# instance fields
.field private TAG:Ljava/lang/String;

.field private final sHealthFeatureList:[Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->isInitiated:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v1, "Shealth::FeatureManager"

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    .line 33
    new-array v1, v3, [Z

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    .line 55
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->isInitiated:Z

    .line 56
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->checkSContextVersion(Landroid/content/Context;)V

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->prnFeatureStates()V

    .line 61
    return-void
.end method

.method private checkSContextVersion(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 100
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const-string v3, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "com.sec.feature.scontext_lite"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 102
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    const-string v4, "FeatureManager: This device does not have the SContext"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    const-string v3, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v2

    .line 107
    .local v2, "version":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/16 v4, 0x9

    aput-boolean v6, v3, v4

    .line 109
    packed-switch v2, :pswitch_data_0

    .line 123
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "sensor hub version = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :goto_1
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "scontext"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/scontext/SContextManager;

    .line 130
    .local v1, "scontextManager":Landroid/hardware/scontext/SContextManager;
    invoke-virtual {v1, v7}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 131
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/16 v4, 0x8

    aput-boolean v6, v3, v4

    .line 134
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    aget-boolean v3, v3, v8

    if-ne v3, v6, :cond_0

    .line 138
    const/16 v3, 0x23

    invoke-virtual {v1, v3}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v4, 0x3

    aput-boolean v6, v3, v4

    .line 141
    :cond_3
    const/16 v3, 0x22

    invoke-virtual {v1, v3}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 142
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    aput-boolean v6, v3, v7

    .line 144
    :cond_4
    const/16 v3, 0x21

    invoke-virtual {v1, v3}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    aput-boolean v6, v3, v6

    goto :goto_0

    .line 111
    .end local v1    # "scontextManager":Landroid/hardware/scontext/SContextManager;
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v4, 0x7

    aput-boolean v6, v3, v4

    goto :goto_1

    .line 114
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v4, 0x4

    aput-boolean v6, v3, v4

    goto :goto_1

    .line 117
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v4, 0x5

    aput-boolean v6, v3, v4

    goto :goto_1

    .line 120
    :pswitch_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    aput-boolean v6, v3, v8

    goto :goto_1

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;
    .locals 1

    .prologue
    .line 77
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->isInitiated:Z

    if-eqz v0, :cond_0

    .line 78
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->mInstance:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    .line 80
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    sput-object p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->mContext:Landroid/content/Context;

    .line 66
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->mInstance:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->mInstance:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    .line 71
    :goto_0
    return-object v0

    .line 69
    :cond_0
    sput-object p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->mContext:Landroid/content/Context;

    .line 70
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->mInstance:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    .line 71
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->mInstance:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    goto :goto_0
.end method


# virtual methods
.method public checkFeature(I)Z
    .locals 1
    .param p1, "feature"    # I

    .prologue
    .line 36
    const/16 v0, 0xa

    if-lt p1, v0, :cond_0

    .line 37
    const/4 v0, 0x0

    .line 38
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    aget-boolean v0, v0, p1

    goto :goto_0
.end method

.method public prnFeatureStates()V
    .locals 4

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    const-string v1, "feature check"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tSensorHub = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/16 v3, 0x9

    aget-boolean v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tPEDOMETER = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/16 v3, 0x8

    aget-boolean v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tH_DEVICE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v3, 0x4

    aget-boolean v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tK_DEVICE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v3, 0x5

    aget-boolean v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tT_DEVICE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v3, 0x6

    aget-boolean v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tJ_DEVICE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v3, 0x7

    aget-boolean v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tSTEP_LEVEL_MONITOR = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v3, 0x1

    aget-boolean v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tINACTIVE_TIMER = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v3, 0x3

    aget-boolean v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tACTIVE_TIME_MONITOR = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->sHealthFeatureList:[Z

    const/4 v3, 0x2

    aget-boolean v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    return-void
.end method
