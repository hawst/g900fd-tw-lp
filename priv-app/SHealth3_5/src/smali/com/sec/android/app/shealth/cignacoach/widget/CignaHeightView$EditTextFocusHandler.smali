.class Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;
.super Ljava/lang/Object;
.source "CignaHeightView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditTextFocusHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;

    .prologue
    .line 459
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 462
    if-nez p2, :cond_2

    .line 464
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->checkValueText()Z

    move-result v4

    if-nez v4, :cond_2

    .line 465
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 466
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$200(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)I

    move-result v4

    const v5, 0x249f1

    if-ne v4, v5, :cond_4

    .line 468
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 469
    .local v3, "valueString":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 470
    :cond_0
    const/high16 v2, -0x40800000    # -1.0f

    .line 476
    .local v2, "value":F
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMinValue:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$1000(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)F

    move-result v4

    cmpg-float v4, v2, v4

    if-gez v4, :cond_1

    .line 478
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    move-result-object v4

    invoke-interface {v4, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;->onCignaDialog(Landroid/view/View;)V

    .line 500
    .end local v2    # "value":F
    .end local v3    # "valueString":Ljava/lang/String;
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setMinValueText()V

    .line 504
    :cond_2
    return-void

    .line 473
    .restart local v3    # "valueString":Ljava/lang/String;
    :cond_3
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .restart local v2    # "value":F
    goto :goto_0

    .line 484
    .end local v2    # "value":F
    .end local v3    # "valueString":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->isHeightValidValue()F
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$1100(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)F

    move-result v4

    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    .line 486
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 487
    .local v0, "ftString":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$400(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 488
    .local v1, "inchString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_5

    .line 490
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$400(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 494
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    move-result-object v4

    invoke-interface {v4, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;->onCignaDialog(Landroid/view/View;)V

    goto :goto_1
.end method
