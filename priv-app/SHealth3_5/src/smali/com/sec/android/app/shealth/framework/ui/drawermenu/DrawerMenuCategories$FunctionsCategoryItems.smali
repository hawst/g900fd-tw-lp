.class public final enum Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FunctionsCategoryItems"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

.field public static final enum CAREGIVERS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

.field public static final enum CIGNA_COACH:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

.field public static final enum CIGNA_LIBRARY:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

.field public static final enum COMMUNITY:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

.field public static final enum EXERCISE:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

.field public static final enum EXERCISE_PRO:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

.field public static final enum HEALTH_NEWS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

.field public static final enum WEIGHT:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;


# instance fields
.field private itemName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    const-string v1, "EXERCISE_PRO"

    const-string v2, "Exercise pro"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->EXERCISE_PRO:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    const-string v1, "EXERCISE"

    const-string v2, "Exercise"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->EXERCISE:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    const-string v1, "WEIGHT"

    const-string v2, "Weight"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->WEIGHT:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    const-string v1, "CIGNA_COACH"

    const-string v2, "Cigna coach"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->CIGNA_COACH:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    const-string v1, "CIGNA_LIBRARY"

    const-string v2, "Cigna library"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->CIGNA_LIBRARY:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    const-string v1, "HEALTH_NEWS"

    const/4 v2, 0x5

    const-string v3, "Health News"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->HEALTH_NEWS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    const-string v1, "COMMUNITY"

    const/4 v2, 0x6

    const-string v3, "Community"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->COMMUNITY:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    const-string v1, "CAREGIVERS"

    const/4 v2, 0x7

    const-string v3, "Caregivers"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->CAREGIVERS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->EXERCISE_PRO:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->EXERCISE:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->WEIGHT:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->CIGNA_COACH:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->CIGNA_LIBRARY:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->HEALTH_NEWS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->COMMUNITY:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->CAREGIVERS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->$VALUES:[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->itemName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;
    .locals 1

    const-class v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;
    .locals 1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->$VALUES:[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;

    return-object v0
.end method


# virtual methods
.method public getMenuItemName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FunctionsCategoryItems;->itemName:Ljava/lang/String;

    return-object v0
.end method
