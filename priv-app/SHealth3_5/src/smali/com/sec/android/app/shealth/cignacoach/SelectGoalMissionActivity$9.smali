.class Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$9;
.super Ljava/lang/Object;
.source "SelectGoalMissionActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 0

    .prologue
    .line 572
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$9;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateChanged()V
    .locals 0

    .prologue
    .line 585
    return-void
.end method

.method public onGoalMissionStatusChange(II)V
    .locals 4
    .param p1, "goalId"    # I
    .param p2, "missionId"    # I

    .prologue
    const/4 v3, 0x0

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$9;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1500(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$9;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1500(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->cancel(Z)Z

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$9;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$9;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    # setter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1502(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;)Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    .line 580
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$9;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1500(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 581
    return-void
.end method
