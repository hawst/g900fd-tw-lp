.class public final Lcom/sec/android/app/shealth/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final actionbar_size:I = 0x7f010030

.field public static final autoStart:I = 0x7f010091

.field public static final balance_background:I = 0x7f010019

.field public static final btnIcon:I = 0x7f010010

.field public static final btnIconVisibility:I = 0x7f010012

.field public static final btnRightMargin:I = 0x7f010011

.field public static final btnTitleText:I = 0x7f01000f

.field public static final cameraBearing:I = 0x7f010001

.field public static final cameraTargetLat:I = 0x7f010002

.field public static final cameraTargetLng:I = 0x7f010003

.field public static final cameraTilt:I = 0x7f010004

.field public static final cameraZoom:I = 0x7f010005

.field public static final cigna_customFont:I = 0x7f010041

.field public static final cigna_viewSize:I = 0x7f010050

.field public static final click_remove_id:I = 0x7f01002e

.field public static final collapsed_height:I = 0x7f01001e

.field public static final color:I = 0x7f01004a

.field public static final color1:I = 0x7f010098

.field public static final color2:I = 0x7f010099

.field public static final colorFirst:I = 0x7f01003b

.field public static final customFont:I = 0x7f010037

.field public static final drag_enabled:I = 0x7f010028

.field public static final drag_handle_id:I = 0x7f01002c

.field public static final drag_scroll_start:I = 0x7f01001f

.field public static final drag_start_mode:I = 0x7f01002b

.field public static final drawables:I = 0x7f01008e

.field public static final drawing_strategy:I = 0x7f01001d

.field public static final drop_animation_duration:I = 0x7f010027

.field public static final end_angle:I = 0x7f010047

.field public static final ex_color:I = 0x7f010059

.field public static final ex_end_angle:I = 0x7f010056

.field public static final ex_init_position:I = 0x7f010058

.field public static final ex_max:I = 0x7f010053

.field public static final ex_pointer_color:I = 0x7f01005c

.field public static final ex_pointer_halo_color:I = 0x7f01005d

.field public static final ex_pointer_size:I = 0x7f010052

.field public static final ex_show_text:I = 0x7f010054

.field public static final ex_start_angle:I = 0x7f010055

.field public static final ex_text_color:I = 0x7f01005e

.field public static final ex_text_size:I = 0x7f010057

.field public static final ex_viewSize:I = 0x7f01005f

.field public static final ex_wheel_active_color:I = 0x7f01005a

.field public static final ex_wheel_size:I = 0x7f010051

.field public static final ex_wheel_unactive_color:I = 0x7f01005b

.field public static final externalRadius:I = 0x7f010040

.field public static final externalRadiusFirst:I = 0x7f01003a

.field public static final externalRadiusStm:I = 0x7f010097

.field public static final externalRadius_spo2:I = 0x7f010095

.field public static final fling_handle_id:I = 0x7f01002d

.field public static final float_alpha:I = 0x7f010024

.field public static final float_background_color:I = 0x7f010021

.field public static final font:I = 0x7f010038

.field public static final gif:I = 0x7f010031

.field public static final goalColor:I = 0x7f01007e

.field public static final help_text:I = 0x7f01007f

.field public static final idForToastText:I = 0x7f01001b

.field public static final image:I = 0x7f01008d

.field public static final images:I = 0x7f01008c

.field public static final init_position:I = 0x7f010049

.field public static final innerRadius:I = 0x7f01003f

.field public static final innerRadiusFirst:I = 0x7f010039

.field public static final innerRadiusStm:I = 0x7f010096

.field public static final innerRadius_spo2:I = 0x7f010094

.field public static final insideImg_gravity:I = 0x7f01008b

.field public static final insideImg_height:I = 0x7f010085

.field public static final insideImg_padding:I = 0x7f010084

.field public static final insideImg_paddingBottom:I = 0x7f010083

.field public static final insideImg_paddingLeft:I = 0x7f010080

.field public static final insideImg_paddingRight:I = 0x7f010081

.field public static final insideImg_paddingTop:I = 0x7f010082

.field public static final insideImg_width:I = 0x7f010086

.field public static final insideImg_yDiff:I = 0x7f010087

.field public static final isLeftAligned:I = 0x7f01000e

.field public static final layoutType:I = 0x7f01001a

.field public static final mapType:I = 0x7f010000

.field public static final max:I = 0x7f010044

.field public static final maxBalance:I = 0x7f010016

.field public static final maxValue:I = 0x7f010014

.field public static final max_drag_scroll_speed:I = 0x7f010020

.field public static final minBalance:I = 0x7f010015

.field public static final minValue:I = 0x7f010013

.field public static final newLineString:I = 0x7f01008a

.field public static final oneshot:I = 0x7f010092

.field public static final overlay_list:I = 0x7f010032

.field public static final period:I = 0x7f010090

.field public static final pointer_color:I = 0x7f01004d

.field public static final pointer_halo_color:I = 0x7f01004e

.field public static final pointer_size:I = 0x7f010043

.field public static final pro_color:I = 0x7f010068

.field public static final pro_end_angle:I = 0x7f010065

.field public static final pro_init_position:I = 0x7f010067

.field public static final pro_max:I = 0x7f010062

.field public static final pro_pointer_color:I = 0x7f01006b

.field public static final pro_pointer_halo_color:I = 0x7f01006c

.field public static final pro_pointer_size:I = 0x7f010061

.field public static final pro_show_text:I = 0x7f010063

.field public static final pro_start_angle:I = 0x7f010064

.field public static final pro_text_color:I = 0x7f01006d

.field public static final pro_text_size:I = 0x7f010066

.field public static final pro_viewSize:I = 0x7f01006e

.field public static final pro_wheel_active_color:I = 0x7f010069

.field public static final pro_wheel_size:I = 0x7f010060

.field public static final pro_wheel_unactive_color:I = 0x7f01006a

.field public static final progressImage:I = 0x7f01001c

.field public static final progressbar_background:I = 0x7f010018

.field public static final remove_animation_duration:I = 0x7f010026

.field public static final remove_enabled:I = 0x7f01002a

.field public static final remove_mode:I = 0x7f010022

.field public static final showDisplayString:I = 0x7f010089

.field public static final showDisplayValue:I = 0x7f010088

.field public static final show_slide_time:I = 0x7f010093

.field public static final show_text:I = 0x7f010045

.field public static final slide_shuffle_speed:I = 0x7f010025

.field public static final sort_enabled:I = 0x7f010029

.field public static final startButton:I = 0x7f01008f

.field public static final start_angle:I = 0x7f010046

.field public static final text:I = 0x7f010033

.field public static final textColor:I = 0x7f010035

.field public static final textFont:I = 0x7f010036

.field public static final textSize:I = 0x7f010034

.field public static final textStroke:I = 0x7f01003c

.field public static final textStrokeColor:I = 0x7f01003e

.field public static final textStrokeWidth:I = 0x7f01003d

.field public static final text_color:I = 0x7f01004f

.field public static final text_size:I = 0x7f010048

.field public static final track_drag_sort:I = 0x7f010023

.field public static final uiCompass:I = 0x7f010006

.field public static final uiRotateGestures:I = 0x7f010007

.field public static final uiScrollGestures:I = 0x7f010008

.field public static final uiTiltGestures:I = 0x7f010009

.field public static final uiZoomControls:I = 0x7f01000a

.field public static final uiZoomGestures:I = 0x7f01000b

.field public static final useViewLifecycle:I = 0x7f01000c

.field public static final use_default_controller:I = 0x7f01002f

.field public static final value_labels_array:I = 0x7f010017

.field public static final viewSize:I = 0x7f01007d

.field public static final walk_color:I = 0x7f010077

.field public static final walk_end_angle:I = 0x7f010074

.field public static final walk_init_position:I = 0x7f010076

.field public static final walk_max:I = 0x7f010071

.field public static final walk_pointer_color:I = 0x7f01007a

.field public static final walk_pointer_halo_color:I = 0x7f01007b

.field public static final walk_pointer_size:I = 0x7f010070

.field public static final walk_show_text:I = 0x7f010072

.field public static final walk_start_angle:I = 0x7f010073

.field public static final walk_text_color:I = 0x7f01007c

.field public static final walk_text_size:I = 0x7f010075

.field public static final walk_wheel_active_color:I = 0x7f010078

.field public static final walk_wheel_size:I = 0x7f01006f

.field public static final walk_wheel_unactive_color:I = 0x7f010079

.field public static final wheel_active_color:I = 0x7f01004b

.field public static final wheel_size:I = 0x7f010042

.field public static final wheel_unactive_color:I = 0x7f01004c

.field public static final zOrderOnTop:I = 0x7f01000d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
