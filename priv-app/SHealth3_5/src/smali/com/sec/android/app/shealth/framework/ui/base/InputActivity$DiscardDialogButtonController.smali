.class Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$DiscardDialogButtonController;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DiscardDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$DiscardDialogButtonController;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$DiscardDialogButtonController;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 2

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$5;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$DiscardDialogButtonController;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
