.class Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$2;
.super Ljava/lang/Object;
.source "CoachRestoreHelper.java"

# interfaces
.implements Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->cignaBackupRestore(Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V
    .locals 3
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "isSuccess"    # Z
    .param p3, "taskError"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    .param p4, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 203
    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "backup on Finished"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NONE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->currentStep:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$002(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->updateProgressPopupValue(I)V

    .line 207
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    if-ne p3, v0, :cond_0

    .line 208
    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cigna Backup before Restore successfully completed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    const-string v1, "RESTORE"

    # invokes: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->cignaBackupRestore(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Ljava/lang/String;)Z

    .line 214
    :goto_0
    return-void

    .line 211
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cigna Backup before Restore failed with error code : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->onException(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0
.end method

.method public onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    .locals 6
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "fractionCompleted"    # D

    .prologue
    .line 190
    const/high16 v0, 0x3f000000    # 0.5f

    .line 191
    .local v0, "multFactor":F
    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "backup on progress"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    float-to-double v2, v0

    mul-double/2addr v2, p2

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 194
    .local v1, "percentage":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->updateProgressPopupValue(I)V

    .line 196
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Progress : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    return-void
.end method

.method public onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V
    .locals 2
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    .prologue
    .line 184
    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "backup started"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    return-void
.end method
