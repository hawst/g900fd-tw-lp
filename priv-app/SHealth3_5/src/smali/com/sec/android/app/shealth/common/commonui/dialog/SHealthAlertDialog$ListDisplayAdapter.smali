.class Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;
.super Landroid/widget/BaseAdapter;
.source "SHealthAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListDisplayAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)V
    .locals 0

    .prologue
    .line 1442
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;

    .prologue
    .line 1442
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 1446
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$800(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1447
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$800(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1449
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 1455
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 1461
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 1468
    if-nez p2, :cond_2

    .line 1469
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1471
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$layout;->list_display_dialog_item:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    check-cast p2, Landroid/widget/LinearLayout;

    .line 1474
    .restart local p2    # "convertView":Landroid/view/View;
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;-><init>()V

    .line 1476
    .local v1, "vh":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;
    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$id;->primary_text:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;->primText:Landroid/widget/TextView;

    .line 1478
    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$id;->secondary_text:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;->secText:Landroid/widget/TextView;

    .line 1481
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1486
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$800(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;->primaryText:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$800(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;->primaryText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 1488
    iget-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;->primText:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1489
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;->primText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$800(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;->primaryText:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1492
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$800(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;->secondaryText:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$800(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;->secondaryText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 1494
    iget-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;->secText:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1495
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;->secText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->listDisplayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$800(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;->secondaryText:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1498
    :cond_1
    return-object p2

    .line 1483
    .end local v1    # "vh":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;

    .restart local v1    # "vh":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayViewHolder;
    goto :goto_0
.end method
