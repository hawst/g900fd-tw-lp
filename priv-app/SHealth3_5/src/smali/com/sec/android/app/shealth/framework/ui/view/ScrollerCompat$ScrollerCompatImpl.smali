.class interface abstract Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "ScrollerCompatImpl"
.end annotation


# virtual methods
.method public abstract abortAnimation(Ljava/lang/Object;)V
.end method

.method public abstract computeScrollOffset(Ljava/lang/Object;)Z
.end method

.method public abstract createScroller(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object;
.end method

.method public abstract getCurrX(Ljava/lang/Object;)I
.end method

.method public abstract getCurrY(Ljava/lang/Object;)I
.end method

.method public abstract getFinalX(Ljava/lang/Object;)I
.end method

.method public abstract getFinalY(Ljava/lang/Object;)I
.end method

.method public abstract isFinished(Ljava/lang/Object;)Z
.end method

.method public abstract startScroll(Ljava/lang/Object;IIIII)V
.end method
