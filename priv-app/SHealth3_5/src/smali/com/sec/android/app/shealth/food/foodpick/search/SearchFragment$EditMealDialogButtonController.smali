.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$EditMealDialogButtonController;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditMealDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0

    .prologue
    .line 1455
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$EditMealDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$1;

    .prologue
    .line 1455
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$EditMealDialogButtonController;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 1458
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$EditMealDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "my_food"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    .line 1459
    .local v0, "myFoodFragment":Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$24;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1470
    :cond_0
    :goto_0
    return-void

    .line 1462
    :pswitch_0
    if-eqz v0, :cond_0

    .line 1463
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$EditMealDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->deleteCheckedItems()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->toggleActivityState(Z)V

    goto :goto_0

    .line 1459
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
