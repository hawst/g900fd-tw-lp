.class Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$3;
.super Ljava/lang/Object;
.source "LogManager.java"

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getSamsungAccountUserId()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V
    .locals 0

    .prologue
    .line 1590
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$3;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "userToken"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "mcc"    # Ljava/lang/String;

    .prologue
    .line 1593
    const-string v1, "LogManager"

    const-string v2, "Success read samsung account information"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1594
    const-string v1, "LogManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "User id= ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1596
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v0

    .line 1597
    .local v0, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mUserId:Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$702(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Ljava/lang/String;)Ljava/lang/String;

    .line 1598
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$800(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessage(I)Z

    .line 1599
    return-void
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 4
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 1603
    const-string v1, "LogManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reading account information failed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1605
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v0

    .line 1606
    .local v0, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mUserId:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$702(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Ljava/lang/String;)Ljava/lang/String;

    .line 1607
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$800(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessage(I)Z

    .line 1608
    return-void
.end method

.method public setNetworkFailure()V
    .locals 3

    .prologue
    .line 1612
    const-string v1, "LogManager"

    const-string v2, "Reading account information network failure"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1614
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v0

    .line 1615
    .local v0, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mUserId:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$702(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Ljava/lang/String;)Ljava/lang/String;

    .line 1616
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$800(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessage(I)Z

    .line 1617
    return-void
.end method
