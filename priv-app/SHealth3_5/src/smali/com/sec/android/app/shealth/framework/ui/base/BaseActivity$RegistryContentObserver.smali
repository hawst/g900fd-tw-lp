.class Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;
.super Landroid/database/ContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RegistryContentObserver"
.end annotation


# instance fields
.field private mActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;)Ljava/lang/ref/WeakReference;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    return-object v0
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 40

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, -0x8ff

    aput v14, v2, v13

    const/16 v13, -0x7d

    aput v13, v2, v12

    const/16 v12, 0xd53

    aput v12, v2, v11

    const/16 v11, 0x587b

    aput v11, v2, v10

    const/16 v10, 0x5831

    aput v10, v2, v9

    const/16 v9, -0x61d4

    aput v9, v2, v8

    const/4 v8, -0x3

    aput v8, v2, v7

    const/16 v7, -0x36

    aput v7, v2, v6

    const/16 v6, -0x5f

    aput v6, v2, v5

    const/16 v5, -0x2ac2

    aput v5, v2, v4

    const/16 v4, -0x4c

    aput v4, v2, v3

    const/16 v3, -0x7c

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, -0x888

    aput v15, v1, v14

    const/16 v14, -0x9

    aput v14, v1, v13

    const/16 v13, 0xd3a

    aput v13, v1, v12

    const/16 v12, 0x580d

    aput v12, v1, v11

    const/16 v11, 0x5858

    aput v11, v1, v10

    const/16 v10, -0x61a8

    aput v10, v1, v9

    const/16 v9, -0x62

    aput v9, v1, v8

    const/16 v8, -0x75

    aput v8, v1, v7

    const/16 v7, -0x3c

    aput v7, v1, v6

    const/16 v6, -0x2ab3

    aput v6, v1, v5

    const/16 v5, -0x2b

    aput v5, v1, v4

    const/16 v4, -0x3a

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x6f

    aput v38, v2, v37

    const/16 v37, -0x20

    aput v37, v2, v36

    const/16 v36, -0x31eb

    aput v36, v2, v35

    const/16 v35, -0x60

    aput v35, v2, v34

    const/16 v34, -0x20

    aput v34, v2, v33

    const/16 v33, -0x63

    aput v33, v2, v32

    const/16 v32, 0x740e

    aput v32, v2, v31

    const/16 v31, -0x4dac

    aput v31, v2, v30

    const/16 v30, -0x24

    aput v30, v2, v29

    const/16 v29, -0x10e0

    aput v29, v2, v28

    const/16 v28, -0x31

    aput v28, v2, v27

    const/16 v27, -0x27

    aput v27, v2, v26

    const/16 v26, 0x3363

    aput v26, v2, v25

    const/16 v25, 0x556

    aput v25, v2, v24

    const/16 v24, -0x418d

    aput v24, v2, v23

    const/16 v23, -0x34

    aput v23, v2, v22

    const/16 v22, 0x554

    aput v22, v2, v21

    const/16 v21, -0x248a

    aput v21, v2, v20

    const/16 v20, -0x47

    aput v20, v2, v19

    const/16 v19, -0x1f

    aput v19, v2, v18

    const/16 v18, -0x6f

    aput v18, v2, v17

    const/16 v17, -0x46

    aput v17, v2, v16

    const/16 v16, -0x38

    aput v16, v2, v15

    const/16 v15, 0x2e31

    aput v15, v2, v14

    const/16 v14, 0x6340

    aput v14, v2, v13

    const/16 v13, 0x240c

    aput v13, v2, v12

    const/16 v12, -0x6399

    aput v12, v2, v11

    const/16 v11, -0x1b

    aput v11, v2, v10

    const/16 v10, -0x5d

    aput v10, v2, v9

    const/16 v9, -0x56

    aput v9, v2, v8

    const/16 v8, -0x34

    aput v8, v2, v7

    const/16 v7, -0x8a9

    aput v7, v2, v6

    const/16 v6, -0x70

    aput v6, v2, v5

    const/16 v5, -0x72

    aput v5, v2, v3

    const/16 v3, 0x6964

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x4f

    aput v39, v1, v38

    const/16 v38, -0x7b

    aput v38, v1, v37

    const/16 v37, -0x318e

    aput v37, v1, v36

    const/16 v36, -0x32

    aput v36, v1, v35

    const/16 v35, -0x7f

    aput v35, v1, v34

    const/16 v34, -0xb

    aput v34, v1, v33

    const/16 v33, 0x744d

    aput v33, v1, v32

    const/16 v32, -0x4d8c

    aput v32, v1, v31

    const/16 v31, -0x4e

    aput v31, v1, v30

    const/16 v30, -0x1091

    aput v30, v1, v29

    const/16 v29, -0x11

    aput v29, v1, v28

    const/16 v28, -0xc

    aput v28, v1, v27

    const/16 v27, 0x3311

    aput v27, v1, v26

    const/16 v26, 0x533

    aput v26, v1, v25

    const/16 v25, -0x41fb

    aput v25, v1, v24

    const/16 v24, -0x42

    aput v24, v1, v23

    const/16 v23, 0x531

    aput v23, v1, v22

    const/16 v22, -0x24fb

    aput v22, v1, v21

    const/16 v21, -0x25

    aput v21, v1, v20

    const/16 v20, -0x52

    aput v20, v1, v19

    const/16 v19, -0x1b

    aput v19, v1, v18

    const/16 v18, -0x2c

    aput v18, v1, v17

    const/16 v17, -0x53

    aput v17, v1, v16

    const/16 v16, 0x2e45

    aput v16, v1, v15

    const/16 v15, 0x632e

    aput v15, v1, v14

    const/16 v14, 0x2463

    aput v14, v1, v13

    const/16 v13, -0x63dc

    aput v13, v1, v12

    const/16 v12, -0x64

    aput v12, v1, v11

    const/16 v11, -0x2f

    aput v11, v1, v10

    const/16 v10, -0x22

    aput v10, v1, v9

    const/16 v9, -0x41

    aput v9, v1, v8

    const/16 v8, -0x8c2

    aput v8, v1, v7

    const/16 v7, -0x9

    aput v7, v1, v6

    const/16 v6, -0x15

    aput v6, v1, v5

    const/16 v5, 0x6936

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_4
    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :catch_0
    move-exception v1

    goto :goto_4
.end method
