.class public Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
.super Landroid/widget/RelativeLayout;
.source "ProfileWeightView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;,
        Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;
    }
.end annotation


# instance fields
.field private isChangeFocus:Z

.field private isChangeUnit:Z

.field private isChangeValue:Z

.field private mContext:Landroid/content/Context;

.field private mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

.field private mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

.field private mFocusHandler:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;

.field private mInputFilters:[Landroid/text/InputFilter;

.field private mIsWeightValueChanged:Z

.field private mLbMinValue:F

.field private mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

.field private mMaxValue:F

.field private mMinValue:F

.field private mPrevKgValue:F

.field private mPrevLbValue:F

.field private mSystemNumberSeparator:Ljava/lang/String;

.field private mWatcher:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;

.field private mWeightUnit:I

.field private mWeightValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    const v0, 0x1fbd1

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    .line 52
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMinValue:F

    .line 53
    const v0, 0x408ccccd    # 4.4f

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mLbMinValue:F

    .line 54
    const v0, 0x4479f99a    # 999.9f

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMaxValue:F

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mIsWeightValueChanged:Z

    .line 66
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mSystemNumberSeparator:Ljava/lang/String;

    .line 69
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeFocus:Z

    .line 70
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeValue:Z

    .line 71
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeUnit:Z

    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mContext:Landroid/content/Context;

    .line 85
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->initLayout()V

    .line 86
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setDatalimit(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeFocus:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeFocus:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeValue:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevLbValue:F

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .param p1, "x1"    # F

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevLbValue:F

    return p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mIsWeightValueChanged:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .param p1, "x1"    # F

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevKgValue:F

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .param p1, "x1"    # F

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevKgValue:F

    return p1
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeUnit:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    return-object v0
.end method

.method private convertComma(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mSystemNumberSeparator:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v1, "."

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 231
    :cond_0
    return-object p1
.end method

.method private initLayout()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 94
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f0301f2

    invoke-static {v2, v3, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 96
    .local v0, "view":Landroid/view/View;
    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;

    invoke-direct {v2, p0, v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWatcher:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;

    .line 97
    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;

    invoke-direct {v2, p0, v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mFocusHandler:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;

    .line 99
    new-array v2, v4, [Landroid/text/InputFilter;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mInputFilters:[Landroid/text/InputFilter;

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mInputFilters:[Landroid/text/InputFilter;

    sget-object v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->inputFilter:Landroid/text/InputFilter;

    aput-object v3, v2, v5

    .line 102
    const v2, 0x7f0808a8

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mInputFilters:[Landroid/text/InputFilter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWatcher:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mFocusHandler:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 109
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f0900c0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f0900c2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 110
    .local v1, "weightUnits":[Ljava/lang/String;
    const v2, 0x7f0808a9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setDropDownVerticalOffset(I)V

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    const v3, 0x7f02086f

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setPopupBackgroundResource(I)V

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomBaseSpinnerAdapter;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    const v6, 0x7f0301f1

    invoke-direct {v3, v4, v5, v6, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomBaseSpinnerAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;I[Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$2;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 181
    return-void
.end method

.method private setDatalimit(Ljava/lang/String;I)V
    .locals 6
    .param p1, "preData"    # Ljava/lang/String;
    .param p2, "prePos"    # I

    .prologue
    const v5, 0x1fbd1

    .line 335
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 336
    .local v1, "editData":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 337
    :cond_0
    iget v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    if-ne v3, v5, :cond_2

    .line 338
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMinValue:F

    .line 361
    .local v0, "data":F
    :goto_0
    iget v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    if-ne v3, v5, :cond_5

    .line 362
    iget v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMaxValue:F

    cmpl-float v3, v0, v3

    if-lez v3, :cond_1

    .line 363
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 372
    :cond_1
    :goto_1
    return-void

    .line 340
    .end local v0    # "data":F
    :cond_2
    iget v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMinValue:F

    const-string v4, "lb"

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToUnit(FLjava/lang/String;)F

    move-result v0

    .restart local v0    # "data":F
    goto :goto_0

    .line 345
    .end local v0    # "data":F
    :cond_3
    :try_start_0
    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .restart local v0    # "data":F
    goto :goto_0

    .line 347
    .end local v0    # "data":F
    :catch_0
    move-exception v2

    .line 349
    .local v2, "nfe":Ljava/lang/NumberFormatException;
    const-string v3, "ProfileWeightView"

    const-string v4, "Input values are invalid Integers "

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 353
    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 355
    const-string p1, "0"

    .line 357
    :cond_4
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .restart local v0    # "data":F
    goto :goto_0

    .line 367
    .end local v2    # "nfe":Ljava/lang/NumberFormatException;
    :cond_5
    iget v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMaxValue:F

    const-string v4, "lb"

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToUnit(FLjava/lang/String;)F

    move-result v3

    cmpl-float v3, v0, v3

    if-lez v3, :cond_1

    .line 368
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    goto :goto_1
.end method


# virtual methods
.method public checkChangeUnit()Z
    .locals 1

    .prologue
    .line 503
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeUnit:Z

    return v0
.end method

.method public checkChangeValue()Z
    .locals 1

    .prologue
    .line 499
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeValue:Z

    return v0
.end method

.method public checkValueText()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 453
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getValue()F

    move-result v0

    .line 455
    .local v0, "value":F
    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, v0, v2

    if-nez v2, :cond_1

    .line 473
    :cond_0
    :goto_0
    return v1

    .line 459
    :cond_1
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    const v3, 0x1fbd1

    if-ne v2, v3, :cond_3

    .line 461
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMinValue:F

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_0

    const/high16 v2, 0x43fa0000    # 500.0f

    cmpl-float v2, v0, v2

    if-gtz v2, :cond_0

    .line 473
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 467
    :cond_3
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mLbMinValue:F

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_0

    const v2, 0x4489c99a    # 1102.3f

    cmpl-float v2, v0, v2

    if-lez v2, :cond_2

    goto :goto_0
.end method

.method public getDropDownListValue()I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method public getValue()F
    .locals 5

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 315
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 326
    :cond_0
    :goto_0
    return v2

    .line 320
    :cond_1
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 323
    :catch_0
    move-exception v0

    .line 325
    .local v0, "nfe":Ljava/lang/NumberFormatException;
    const-string v3, "ProfileWeightView"

    const-string v4, "Input values are invalid Integers "

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getWeight()F
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    return v0
.end method

.method public getWeightUnit()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    return v0
.end method

.method public hideWeightKeyboard()V
    .locals 3

    .prologue
    .line 184
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mContext:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 185
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->clearFocus()V

    .line 189
    return-void
.end method

.method public setDropDownListValue(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setSelection(I)V

    .line 193
    return-void
.end method

.method public setMinValueText()V
    .locals 2

    .prologue
    .line 478
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    const v1, 0x1fbd1

    if-ne v0, v1, :cond_1

    .line 480
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getValue()F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMinValue:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMinValue:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 496
    :goto_0
    return-void

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const/high16 v1, 0x43fa0000    # 500.0f

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 488
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getValue()F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mLbMinValue:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mLbMinValue:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 491
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const v1, 0x4489c99a    # 1102.3f

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setOnProfileDialogListener(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    .line 90
    return-void
.end method

.method public setValue(F)V
    .locals 5
    .param p1, "weight"    # F

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 243
    const/4 v0, 0x0

    .line 244
    .local v0, "weightVal":F
    iget v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    const v2, 0x1fbd1

    if-ne v1, v2, :cond_2

    .line 245
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mIsWeightValueChanged:Z

    if-eqz v1, :cond_1

    .line 247
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mIsWeightValueChanged:Z

    .line 248
    invoke-static {p1, v4}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v0

    .line 249
    iget v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMinValue:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 251
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMinValue:F

    .line 253
    :cond_0
    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevKgValue:F

    .line 277
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->convertComma(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    return-void

    .line 257
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevKgValue:F

    goto :goto_0

    .line 261
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mIsWeightValueChanged:Z

    if-eqz v1, :cond_4

    .line 263
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mIsWeightValueChanged:Z

    .line 264
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToLb(F)F

    move-result v1

    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v0

    .line 266
    iget v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mLbMinValue:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    .line 268
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mLbMinValue:F

    .line 270
    :cond_3
    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevLbValue:F

    goto :goto_0

    .line 274
    :cond_4
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevLbValue:F

    goto :goto_0
.end method

.method public setWeightPreviousValue(F)V
    .locals 2
    .param p1, "prevWeight"    # F

    .prologue
    .line 236
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    .line 237
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevKgValue:F

    .line 238
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    const-string v1, "lb"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToUnit(FLjava/lang/String;)F

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->convertDecimalFormat(F)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->convertComma(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevLbValue:F

    .line 239
    return-void
.end method

.method public setWeightProfileData(FI)V
    .locals 2
    .param p1, "value"    # F
    .param p2, "unit"    # I

    .prologue
    .line 207
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    .line 210
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 211
    iput p2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    .line 213
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    const v1, 0x1fbd1

    if-ne v0, v1, :cond_2

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setSelection(I)V

    .line 219
    :cond_1
    :goto_0
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setValue(F)V

    .line 220
    return-void

    .line 216
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setSelection(I)V

    goto :goto_0
.end method

.method public setWeightUnit(I)V
    .locals 0
    .param p1, "unit"    # I

    .prologue
    .line 204
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    .line 205
    return-void
.end method

.method public setWeightValue()V
    .locals 4

    .prologue
    .line 282
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 283
    :cond_0
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMinValue:F

    iput v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    .line 308
    :cond_1
    :goto_0
    return-void

    .line 285
    :cond_2
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I

    const v3, 0x1fbd1

    if-ne v2, v3, :cond_3

    .line 286
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v1

    .line 287
    .local v1, "roundedWeightValue":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 288
    .local v0, "currentWeightValue":F
    cmpl-float v2, v1, v0

    if-eqz v2, :cond_1

    .line 290
    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    goto :goto_0

    .line 295
    .end local v0    # "currentWeightValue":F
    .end local v1    # "roundedWeightValue":F
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertLbToKg(F)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    .line 298
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    iget v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMinValue:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_4

    .line 300
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mMinValue:F

    iput v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    .line 303
    :cond_4
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->ceilToDigit(FI)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F

    goto :goto_0
.end method
