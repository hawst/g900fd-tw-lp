.class Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;
.super Ljava/lang/Object;
.source "AddFavoritesActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "adapter":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    const v5, 0x7f08033e

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/home/data/IconData;->setIsSelected(Z)V

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # operator-- for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$110(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I

    .line 171
    invoke-static {p2, v6, v4, v4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 172
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 173
    .local v0, "cb":Landroid/widget/CheckBox;
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 198
    .end local v0    # "cb":Landroid/widget/CheckBox;
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # invokes: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setActionBarSpinner()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$200(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # invokes: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->refreshFocusables()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$300(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V

    .line 200
    return-void

    :cond_0
    move v2, v4

    .line 169
    goto :goto_0

    .line 180
    :cond_1
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I

    move-result v2

    if-le v1, v2, :cond_3

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/home/data/IconData;->setIsSelected(Z)V

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # operator++ for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$108(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I

    .line 184
    invoke-static {p2, v6, v4, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 185
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 186
    .restart local v0    # "cb":Landroid/widget/CheckBox;
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .end local v0    # "cb":Landroid/widget/CheckBox;
    :cond_2
    move v2, v4

    .line 182
    goto :goto_2

    .line 191
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    const v2, 0x7f090dfa

    invoke-static {v1, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
