.class Lcom/sec/android/app/shealth/settings/UnitSettingActivity$2;
.super Ljava/lang/Object;
.source "UnitSettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/UnitSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 445
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "height"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->createHeightUnitPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$100(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    .line 459
    :cond_0
    :goto_0
    return-void

    .line 448
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, "weight"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->createWeightUnitPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$200(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    goto :goto_0

    .line 451
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "distance"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->createDistanceUnitPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$300(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    goto :goto_0

    .line 454
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "glucose"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->createGlucoseUnitPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$400(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    goto :goto_0

    .line 456
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, "temperature"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->createTemperatureUnitPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$500(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    goto :goto_0
.end method
