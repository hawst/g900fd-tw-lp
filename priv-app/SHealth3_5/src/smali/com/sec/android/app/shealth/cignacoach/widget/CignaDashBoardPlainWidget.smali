.class public Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "CignaDashBoardPlainWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget$LoadLatestDataThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final mAppWidgetBitmapManager:Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;

.field private static mCignaDashBoardPlainWidget:Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

.field private static mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->TAG:Ljava/lang/String;

    .line 32
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mAppWidgetBitmapManager:Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mHandler:Landroid/os/Handler;

    .line 36
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mCignaDashBoardPlainWidget:Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 39
    sput-object p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mCignaDashBoardPlainWidget:Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->loadLatestData()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "x3"    # [I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->updateWidgetUI(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    return-void
.end method

.method private declared-synchronized loadLatestData()V
    .locals 2

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    monitor-exit p0

    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static updateDashBoardPlainWidget(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 264
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.intent.action.UPDATE_WIDGET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 265
    .local v0, "intent":Landroid/content/Intent;
    if-nez p0, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object p0

    .line 267
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mCignaDashBoardPlainWidget:Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

    if-eqz v1, :cond_1

    .line 268
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->TAG:Ljava/lang/String;

    const-string v2, "Local Update"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mCignaDashBoardPlainWidget:Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->updateWidgets()V

    .line 274
    :goto_1
    return-void

    .line 265
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    goto :goto_0

    .line 271
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->TAG:Ljava/lang/String;

    const-string v2, "Remote Update"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method private updateWidgetUI(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 119
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v3

    .line 120
    .local v3, "isContentProviderAccessible":Z
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "isContentProviderAccessible : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v8, p3

    if-ge v2, v8, :cond_4

    .line 123
    aget v0, p3, v2

    .line 125
    .local v0, "appWidgetId":I
    const/4 v5, 0x0

    .line 127
    .local v5, "views":Landroid/widget/RemoteViews;
    if-eqz v3, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 128
    :cond_0
    new-instance v5, Landroid/widget/RemoteViews;

    .end local v5    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f03008b

    invoke-direct {v5, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 130
    .restart local v5    # "views":Landroid/widget/RemoteViews;
    const v8, 0x7f0802ad

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090351

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 132
    const v8, 0x7f0802af

    const v9, 0x7f090249

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 134
    const v8, 0x7f0802a5

    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 136
    const v8, 0x7f0802ac

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v9

    invoke-virtual {v9, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getCignaWebLinkPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 137
    const v8, 0x7f0802ac

    const v9, 0x7f090faf

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 139
    invoke-virtual {p2, v0, v5}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 122
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 141
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v8

    sget-object v9, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v8, p1, v9}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getWidgetInfo(Landroid/content/Context;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v6

    .line 143
    .local v6, "widgetInfo":Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    if-eqz v6, :cond_1

    .line 144
    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v7

    .line 145
    .local v7, "widgetInfoType":Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getLifeStyleScore()I

    move-result v4

    .line 147
    .local v4, "lifeStyleScore":I
    const/4 v1, 0x0

    .line 149
    .local v1, "circleProgressBmp":Landroid/graphics/Bitmap;
    sget-object v8, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->COACH_NOT_LAUNCHED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    if-ne v7, v8, :cond_3

    .line 151
    new-instance v5, Landroid/widget/RemoteViews;

    .end local v5    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f03008b

    invoke-direct {v5, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 153
    .restart local v5    # "views":Landroid/widget/RemoteViews;
    const v8, 0x7f0802ad

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090351

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 155
    const v8, 0x7f0802af

    const v9, 0x7f090249

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 158
    const v8, 0x7f0802a5

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v9

    const-string v10, "COACH_WIDGET"

    const/16 v11, 0x98e

    invoke-virtual {v9, p1, v10, v11}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getAppropriatePendingIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 230
    :goto_2
    const v8, 0x7f0802ac

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v9

    invoke-virtual {v9, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getCignaWebLinkPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 231
    const v8, 0x7f0802ac

    const v9, 0x7f090faf

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 233
    invoke-virtual {p2, v0, v5}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto :goto_1

    .line 160
    :cond_3
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mAppWidgetBitmapManager:Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->isUpdating()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 237
    .end local v0    # "appWidgetId":I
    .end local v1    # "circleProgressBmp":Landroid/graphics/Bitmap;
    .end local v4    # "lifeStyleScore":I
    .end local v5    # "views":Landroid/widget/RemoteViews;
    .end local v6    # "widgetInfo":Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    .end local v7    # "widgetInfoType":Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    :cond_4
    return-void

    .line 163
    .restart local v0    # "appWidgetId":I
    .restart local v1    # "circleProgressBmp":Landroid/graphics/Bitmap;
    .restart local v4    # "lifeStyleScore":I
    .restart local v5    # "views":Landroid/widget/RemoteViews;
    .restart local v6    # "widgetInfo":Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    .restart local v7    # "widgetInfoType":Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    :cond_5
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mAppWidgetBitmapManager:Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->setUpdating(Z)V

    .line 166
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mAppWidgetBitmapManager:Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->recycle(I)V

    .line 168
    if-lez v4, :cond_7

    .line 170
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    int-to-float v9, v4

    const/high16 v10, 0x42c80000    # 100.0f

    div-float/2addr v9, v10

    const v10, 0x7f020720

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressDrawer;->getProgressBitmapWidget(Landroid/content/Context;FI)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 172
    sget-object v8, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->NO_MISSIONS_ENROLLED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    if-ne v7, v8, :cond_6

    .line 174
    new-instance v5, Landroid/widget/RemoteViews;

    .end local v5    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f03008d

    invoke-direct {v5, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 175
    .restart local v5    # "views":Landroid/widget/RemoteViews;
    const v8, 0x7f0802a6

    invoke-virtual {v5, v8, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 177
    const v8, 0x7f0802ad

    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getTitleText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 178
    const v8, 0x7f0802b2

    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getTitleText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 179
    const v8, 0x7f0802b1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 181
    const v8, 0x7f0802aa

    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getMessageText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 182
    const v8, 0x7f0802a9

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v9

    const-string v10, "COACH_WIDGET"

    const/16 v11, 0x98e

    invoke-virtual {v9, p1, v10, v11}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getAppropriatePendingIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 183
    const v8, 0x7f0802ab

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v9

    const-string v10, "COACH_WIDGET"

    const/16 v11, 0x98e

    invoke-virtual {v9, p1, v10, v11}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getAppropriatePendingIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 186
    const v8, 0x7f0802a6

    invoke-virtual {p0, p1, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->getTitlePendingIntent(Landroid/content/Context;Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 205
    :goto_3
    const v8, 0x7f0802b0

    invoke-virtual {p0, p1, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->getMainPendingIntent(Landroid/content/Context;Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 224
    :goto_4
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mAppWidgetBitmapManager:Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;

    const/4 v9, 0x2

    const/4 v10, 0x1

    new-array v10, v10, [Landroid/graphics/Bitmap;

    const/4 v11, 0x0

    aput-object v1, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->addBitmap(I[Landroid/graphics/Bitmap;)V

    .line 225
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mAppWidgetBitmapManager:Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->setUpdating(Z)V

    .line 227
    const v8, 0x7f0802a6

    const v9, 0x7f090029

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 189
    :cond_6
    new-instance v5, Landroid/widget/RemoteViews;

    .end local v5    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f03008c

    invoke-direct {v5, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 190
    .restart local v5    # "views":Landroid/widget/RemoteViews;
    const v8, 0x7f0802a6

    invoke-virtual {v5, v8, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 191
    const v8, 0x7f0802ad

    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getTitleText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 192
    const v8, 0x7f0802b2

    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getTitleText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 193
    const v8, 0x7f0802b1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 194
    const v8, 0x7f0802aa

    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getMessageText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 197
    const v8, 0x7f0802a9

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v9

    const-string v10, "COACH_WIDGET"

    const/16 v11, 0x98e

    invoke-virtual {v9, p1, v10, v11}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getAppropriatePendingIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 198
    const v8, 0x7f0802ab

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v9

    const-string v10, "COACH_WIDGET"

    const/16 v11, 0x98e

    invoke-virtual {v9, p1, v10, v11}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getAppropriatePendingIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 201
    const v8, 0x7f0802a6

    invoke-virtual {p0, p1, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->getTitlePendingIntent(Landroid/content/Context;Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_3

    .line 208
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x0

    const v10, 0x7f020113

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressDrawer;->getProgressBitmapWidget(Landroid/content/Context;FI)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 209
    new-instance v5, Landroid/widget/RemoteViews;

    .end local v5    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f03008a

    invoke-direct {v5, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 210
    .restart local v5    # "views":Landroid/widget/RemoteViews;
    const v8, 0x7f0802a6

    invoke-virtual {v5, v8, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 213
    const v8, 0x7f0802ad

    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getTitleText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 214
    const v8, 0x7f0802a8

    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getTitleText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 216
    const v8, 0x7f0802a8

    invoke-virtual {p0, p1, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->getTitlePendingIntent(Landroid/content/Context;Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 218
    const v8, 0x7f0802aa

    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getMessageText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 219
    const v8, 0x7f0802a9

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v9

    const-string v10, "COACH_WIDGET"

    const/16 v11, 0x98e

    invoke-virtual {v9, p1, v10, v11}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getAppropriatePendingIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 221
    const v8, 0x7f0802a6

    invoke-virtual {p0, p1, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->getTitlePendingIntent(Landroid/content/Context;Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_4
.end method

.method private updateWidgets()V
    .locals 5

    .prologue
    .line 255
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 256
    .local v1, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 257
    .local v0, "appWidgetIds":[I
    array-length v2, v0

    if-lez v2, :cond_0

    .line 258
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 260
    :cond_0
    return-void
.end method


# virtual methods
.method public getMainPendingIntent(Landroid/content/Context;Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetInfo"    # Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .prologue
    .line 248
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getTitleIntent()Landroid/content/Intent;

    move-result-object v0

    .line 249
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "widgetType"

    const-string v2, "COACH_WIDGET"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 250
    const-string v1, "COACH_WIDGET"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 251
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x98e

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public getTitlePendingIntent(Landroid/content/Context;Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetInfo"    # Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .prologue
    .line 241
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getTitleIntent()Landroid/content/Intent;

    move-result-object v0

    .line 242
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "widgetType"

    const-string v2, "COACH_WIDGET"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 243
    const-string v1, "COACH_WIDGET"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 244
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x98e

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 76
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->mAppWidgetBitmapManager:Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/AppWidgetBitmapManager;->recycle(I)V

    .line 70
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 63
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 44
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 49
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "action":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "intent.getAction() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.intent.action.RESTART_WIDGET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.sec.android.app.shealth.intent.action.UPDATE_WIDGET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.cigna.mobile.coach.COACH_RESTORE_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->updateWidgets()V

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 80
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 82
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onUpdate"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget$LoadLatestDataThread;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget$LoadLatestDataThread;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;)V

    .line 84
    .local v0, "loadLatestDataThread":Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget$LoadLatestDataThread;
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 85
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 86
    return-void
.end method
