.class public Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;
.super Landroid/database/ContentObserver;
.source "SleepMonitorSummaryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SleepDBContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 712
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .line 713
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 714
    return-void
.end method


# virtual methods
.method public deliverSelfNotifications()Z
    .locals 1

    .prologue
    .line 718
    invoke-super {p0}, Landroid/database/ContentObserver;->deliverSelfNotifications()Z

    move-result v0

    return v0
.end method

.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .prologue
    .line 723
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 724
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->update()V

    .line 725
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 0
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 729
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 730
    return-void
.end method
