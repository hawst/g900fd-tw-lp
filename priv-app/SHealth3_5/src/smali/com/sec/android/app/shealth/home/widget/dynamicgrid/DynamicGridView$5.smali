.class Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$5;
.super Landroid/animation/AnimatorListenerAdapter;
.source "DynamicGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->animateBounds(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

.field final synthetic val$mobileView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$5;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iput-object p2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$5;->val$mobileView:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$5;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverAnimation:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$1302(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Z)Z

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$5;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # invokes: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->updateEnableState()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$1400(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$5;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$5;->val$mobileView:Landroid/view/View;

    # invokes: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->reset(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$1500(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Landroid/view/View;)V

    .line 466
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 457
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$5;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverAnimation:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$1302(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Z)Z

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$5;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # invokes: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->updateEnableState()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$1400(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V

    .line 459
    return-void
.end method
