.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13;
.super Ljava/lang/Object;
.source "HeartrateSummaryFragmentNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->HRMmeasuringFinishScaleAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 1276
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const-wide/16 v11, 0x104

    const/16 v4, 0x8

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/high16 v2, 0x3f000000    # 0.5f

    .line 1279
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2300(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1280
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$1500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1281
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/FrameLayout;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1282
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1284
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v6, v2

    move v7, v5

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1286
    .local v0, "scaleAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0, v11, v12}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1288
    new-instance v10, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    invoke-direct {v10, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1289
    .local v10, "fadeAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v10, v11, v12}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1291
    new-instance v9, Landroid/view/animation/AnimationSet;

    invoke-direct {v9, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1292
    .local v9, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1293
    invoke-virtual {v9, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1294
    const/4 v1, 0x0

    invoke-virtual {v9, v1}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 1295
    invoke-virtual {v9, v5}, Landroid/view/animation/AnimationSet;->setFillBefore(Z)V

    .line 1296
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13$1;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13;)V

    invoke-virtual {v9, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1320
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1321
    return-void
.end method
