.class public interface abstract Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodParser;
.super Ljava/lang/Object;
.source "FoodParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final EMPTY_STRING:Ljava/lang/String; = ""

.field public static final MISSING_TAG_VALUE:I = -0x1


# virtual methods
.method public abstract parseAutoCompleteSearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;"
        }
    .end annotation
.end method

.method public abstract parseBarcodeSearch(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public abstract parseCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;"
        }
    .end annotation
.end method

.method public abstract parseExtraFood(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;"
        }
    .end annotation
.end method

.method public abstract parseFoodByCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract parseFoodSearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract parseSubCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;"
        }
    .end annotation
.end method
