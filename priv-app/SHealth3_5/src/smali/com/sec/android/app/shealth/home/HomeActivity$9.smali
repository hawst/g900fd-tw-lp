.class Lcom/sec/android/app/shealth/home/HomeActivity$9;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/HomeActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/HomeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 1313
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 9
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const v8, 0x7f0900b9

    .line 1318
    const v5, 0x7f0804ea

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1319
    .local v2, "pedoText":Landroid/widget/TextView;
    const v5, 0x7f0804ec

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1320
    .local v0, "exerciseText":Landroid/widget/TextView;
    const v5, 0x7f0803f1

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1321
    .local v4, "totalText":Landroid/widget/TextView;
    const v5, 0x7f0804e8

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 1322
    .local v3, "pedoView":Landroid/widget/LinearLayout;
    const v5, 0x7f0804eb

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1324
    .local v1, "exerciseView":Landroid/widget/LinearLayout;
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mBurntCalories:[I
    invoke-static {v5}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1400(Lcom/sec/android/app/shealth/home/HomeActivity;)[I

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1325
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mBurntCalories:[I
    invoke-static {v6}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1400(Lcom/sec/android/app/shealth/home/HomeActivity;)[I

    move-result-object v6

    const/4 v7, 0x1

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1326
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mBurntCalories:[I
    invoke-static {v6}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1400(Lcom/sec/android/app/shealth/home/HomeActivity;)[I

    move-result-object v6

    const/4 v7, 0x2

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1327
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    const v7, 0x7f09008a

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    const v7, 0x7f090a19

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mBurntCalories:[I
    invoke-static {v6}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1400(Lcom/sec/android/app/shealth/home/HomeActivity;)[I

    move-result-object v6

    const/4 v7, 0x0

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/shealth/home/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1330
    :cond_0
    new-instance v5, Lcom/sec/android/app/shealth/home/HomeActivity$9$1;

    invoke-direct {v5, p0, p3}, Lcom/sec/android/app/shealth/home/HomeActivity$9$1;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity$9;Landroid/app/Dialog;)V

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1343
    new-instance v5, Lcom/sec/android/app/shealth/home/HomeActivity$9$2;

    invoke-direct {v5, p0, p3}, Lcom/sec/android/app/shealth/home/HomeActivity$9$2;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity$9;Landroid/app/Dialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1356
    return-void
.end method
