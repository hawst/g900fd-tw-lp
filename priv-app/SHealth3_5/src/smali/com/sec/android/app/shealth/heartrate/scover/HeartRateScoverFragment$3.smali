.class Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;
.super Ljava/lang/Object;
.source "HeartRateScoverFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->createMeasurementDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const/4 v4, 0x0

    .line 227
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string v1, "createMeasurementDialog onContentInitialization"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstStatus:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$702(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$802(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f080595

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondIcon:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$902(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$002(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1002(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/FrameLayout;)Landroid/widget/FrameLayout;

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805c3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewLayout:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1102(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805c4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewIcon:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1202(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805c5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewGreenIcon:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1302(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805c6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewOrangeIcon:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1402(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805c7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewMask:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1502(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805ce

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCancelDialog:Landroid/widget/Button;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1602(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/Button;)Landroid/widget/Button;

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCancelDialog:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1600(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3$1;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1900(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    move-result-object v0

    if-nez v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;-><init>(Landroid/content/Context;Landroid/view/View;)V

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1902(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2002(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 264
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805bf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpmUnit:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2102(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805c0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2202(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901ea

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mStartButton:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2302(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondRetry:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3$2;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/PulseView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/heartrate/PulseView;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2602(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Lcom/sec/android/app/shealth/heartrate/PulseView;)Lcom/sec/android/app/shealth/heartrate/PulseView;

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1100(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2600(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/heartrate/PulseView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseViewThread:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2702(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;)Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const v0, 0x7f0805c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2802(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # invokes: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->initAoudioFile()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2900(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScaleAnimation:Landroid/view/animation/Animation;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3002(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsRunPulseView:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3102(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)Z

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isAutoStartup:Z

    if-eqz v0, :cond_1

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsOnConfigChanged:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1700(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3$3;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsOnConfigChanged:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3202(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)Z

    .line 324
    :cond_1
    :goto_0
    return-void

    .line 306
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isMeasureCompleted:Z

    if-nez v0, :cond_3

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->startSensorAndDelayUIForDetectedFinger(I)V

    goto :goto_0

    .line 312
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3400(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v0

    if-nez v0, :cond_4

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->startSensorAndDelayUIForDetectedFinger(I)V

    goto :goto_0

    .line 319
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
