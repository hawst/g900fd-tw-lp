.class public final Lcom/sec/android/app/shealth/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ab_switch_bg_disabled_holo_light:I = 0x7f020000

.field public static final ab_switch_bg_focused_holo_light:I = 0x7f020001

.field public static final ab_switch_bg_holo_light:I = 0x7f020002

.field public static final accessory_health_icon:I = 0x7f020003

.field public static final accessory_icon_link:I = 0x7f020004

.field public static final accessory_link_blue_dot01:I = 0x7f020005

.field public static final accessory_link_blue_dot02:I = 0x7f020006

.field public static final accessory_link_blue_dot03:I = 0x7f020007

.field public static final accessory_link_green_dot01:I = 0x7f020008

.field public static final accessory_link_green_dot02:I = 0x7f020009

.field public static final accessory_link_green_dot03:I = 0x7f02000a

.field public static final accessory_product_adidas_micoachhrm:I = 0x7f02000b

.field public static final accessory_product_bg:I = 0x7f02000c

.field public static final accessory_product_bg_onetouch:I = 0x7f02000d

.field public static final accessory_product_garmin_heartratemonitor:I = 0x7f02000e

.field public static final accessory_product_garmin_premiumhrm:I = 0x7f02000f

.field public static final accessory_product_garmin_timex:I = 0x7f020010

.field public static final accessory_product_samsung_activitytracker:I = 0x7f020011

.field public static final accessory_product_samsung_black_gear:I = 0x7f020012

.field public static final accessory_product_samsung_eihh:I = 0x7f020013

.field public static final accessory_product_samsung_gear:I = 0x7f020014

.field public static final accessory_product_samsung_gear_blueblack:I = 0x7f020015

.field public static final accessory_product_samsung_gearfit:I = 0x7f020016

.field public static final accessory_product_samsung_neogear:I = 0x7f020017

.field public static final accessory_product_samsung_weigh:I = 0x7f020018

.field public static final accessory_product_wahoo_heartratestrap:I = 0x7f020019

.field public static final accessory_product_weight_bf:I = 0x7f02001a

.field public static final accessory_settings_button_selector:I = 0x7f02001b

.field public static final accessory_temperature_bg:I = 0x7f02001c

.field public static final accessory_temperature_bp:I = 0x7f02001d

.field public static final accessory_temperature_hr:I = 0x7f02001e

.field public static final accessory_temperature_weight:I = 0x7f02001f

.field public static final action_bar_btn_overflow:I = 0x7f020020

.field public static final action_bar_spinner_selecter:I = 0x7f020021

.field public static final action_item_background_select_mode:I = 0x7f020022

.field public static final action_item_background_selector:I = 0x7f020023

.field public static final action_item_cab_background_selector:I = 0x7f020024

.field public static final actionbar_item_background_selector:I = 0x7f020025

.field public static final airview_popup_picker_bg_light:I = 0x7f020026

.field public static final apptheme_btn_radio_holo_light:I = 0x7f020027

.field public static final apptheme_edit_text_holo_light:I = 0x7f020028

.field public static final award_custom_progress:I = 0x7f020029

.field public static final award_item_value_text_selector:I = 0x7f02002a

.field public static final baidu_dummy:I = 0x7f02002b

.field public static final boohee_logo:I = 0x7f02002c

.field public static final bottom_button_selector:I = 0x7f02002d

.field public static final bt_s_cancel:I = 0x7f02002e

.field public static final btn_heart_start_n:I = 0x7f02002f

.field public static final button2_2:I = 0x7f020030

.field public static final button_2:I = 0x7f020031

.field public static final cal_day_select:I = 0x7f020032

.field public static final cal_icon_food:I = 0x7f020033

.field public static final cal_icon_pedometer:I = 0x7f020034

.field public static final cal_icon_sleep:I = 0x7f020035

.field public static final cal_icon_weight:I = 0x7f020036

.field public static final cal_month_data_etc:I = 0x7f020037

.field public static final cal_month_popup_title_back:I = 0x7f020038

.field public static final cal_month_popup_title_back_dim:I = 0x7f020039

.field public static final cal_month_popup_title_back_dimfocus:I = 0x7f02003a

.field public static final cal_month_popup_title_back_focus:I = 0x7f02003b

.field public static final cal_month_popup_title_back_nor:I = 0x7f02003c

.field public static final cal_month_popup_title_back_press:I = 0x7f02003d

.field public static final cal_month_popup_title_next:I = 0x7f02003e

.field public static final cal_month_popup_title_next_dim:I = 0x7f02003f

.field public static final cal_month_popup_title_next_dimfocus:I = 0x7f020040

.field public static final cal_month_popup_title_next_focus:I = 0x7f020041

.field public static final cal_month_popup_title_next_nor:I = 0x7f020042

.field public static final cal_month_popup_title_next_press:I = 0x7f020043

.field public static final cal_month_popup_title_today:I = 0x7f020044

.field public static final cal_month_select:I = 0x7f020045

.field public static final cal_month_title_back:I = 0x7f020046

.field public static final cal_month_title_back_focus:I = 0x7f020047

.field public static final cal_month_title_back_press:I = 0x7f020048

.field public static final cal_month_title_next:I = 0x7f020049

.field public static final cal_month_title_next_focus:I = 0x7f02004a

.field public static final cal_month_title_next_press:I = 0x7f02004b

.field public static final cal_month_today_list:I = 0x7f02004c

.field public static final calendar_horizontall_divider:I = 0x7f02004d

.field public static final calendar_month_year_button_bottom_selector:I = 0x7f02004e

.field public static final calendar_month_year_button_top_selector:I = 0x7f02004f

.field public static final calendar_next_selector:I = 0x7f020050

.field public static final calendar_prev_selector:I = 0x7f020051

.field public static final calendar_textview_selector:I = 0x7f020052

.field public static final calendar_vertical_divider:I = 0x7f020053

.field public static final camera:I = 0x7f020054

.field public static final challenge_list_progress_bar01:I = 0x7f020055

.field public static final checkbox_selector:I = 0x7f020056

.field public static final checkbox_selector_for_textview:I = 0x7f020057

.field public static final cigna_action_bar_share_btn_selector:I = 0x7f020058

.field public static final cigna_assessment_button_selector:I = 0x7f020059

.field public static final cigna_badge_bg:I = 0x7f02005a

.field public static final cigna_badge_icon_actionhero:I = 0x7f02005b

.field public static final cigna_badge_icon_athlete:I = 0x7f02005c

.field public static final cigna_badge_icon_champ:I = 0x7f02005d

.field public static final cigna_badge_icon_coach:I = 0x7f02005e

.field public static final cigna_badge_icon_dreamcatcher:I = 0x7f02005f

.field public static final cigna_badge_icon_duelmaster:I = 0x7f020060

.field public static final cigna_badge_icon_fitnessguru:I = 0x7f020061

.field public static final cigna_badge_icon_foodie:I = 0x7f020062

.field public static final cigna_badge_icon_foursquare:I = 0x7f020063

.field public static final cigna_badge_icon_gearedup:I = 0x7f020064

.field public static final cigna_badge_icon_gogetter:I = 0x7f020065

.field public static final cigna_badge_icon_hattrick:I = 0x7f020066

.field public static final cigna_badge_icon_healthnut:I = 0x7f020067

.field public static final cigna_badge_icon_heavyweight:I = 0x7f020068

.field public static final cigna_badge_icon_highfive:I = 0x7f020069

.field public static final cigna_badge_icon_knowthself:I = 0x7f02006a

.field public static final cigna_badge_icon_lightweight:I = 0x7f02006b

.field public static final cigna_badge_icon_luckyseven:I = 0x7f02006c

.field public static final cigna_badge_icon_mirror:I = 0x7f02006d

.field public static final cigna_badge_icon_movingtheneedle:I = 0x7f02006e

.field public static final cigna_badge_icon_outofthegate:I = 0x7f02006f

.field public static final cigna_badge_icon_platebalancer:I = 0x7f020070

.field public static final cigna_badge_icon_relaxmaster:I = 0x7f020071

.field public static final cigna_badge_icon_rockstar:I = 0x7f020072

.field public static final cigna_badge_icon_sixpack:I = 0x7f020073

.field public static final cigna_badge_icon_sleepingbeauty:I = 0x7f020074

.field public static final cigna_badge_icon_slimntrim:I = 0x7f020075

.field public static final cigna_badge_icon_stickwithit:I = 0x7f020076

.field public static final cigna_badge_icon_superhero:I = 0x7f020077

.field public static final cigna_badge_icon_wellrounded:I = 0x7f020078

.field public static final cigna_badge_icon_zenmaster:I = 0x7f020079

.field public static final cigna_button_disabled:I = 0x7f02007a

.field public static final cigna_button_focus:I = 0x7f02007b

.field public static final cigna_button_icon_setgoal:I = 0x7f02007c

.field public static final cigna_button_normal:I = 0x7f02007d

.field public static final cigna_button_press:I = 0x7f02007e

.field public static final cigna_button_select:I = 0x7f02007f

.field public static final cigna_change_fragment_type_button_selector_goal_temp:I = 0x7f020080

.field public static final cigna_change_fragment_type_button_selector_main_temp:I = 0x7f020081

.field public static final cigna_checkbox_selector:I = 0x7f020082

.field public static final cigna_circle_progress_indicator_selector:I = 0x7f020083

.field public static final cigna_coach_bubble:I = 0x7f020084

.field public static final cigna_coach_bubble01:I = 0x7f020085

.field public static final cigna_coach_circle_empty:I = 0x7f020086

.field public static final cigna_coach_circle_fill:I = 0x7f020087

.field public static final cigna_coach_icon_bg:I = 0x7f020088

.field public static final cigna_coach_icon_bg01:I = 0x7f020089

.field public static final cigna_coach_icon_bg_focus:I = 0x7f02008a

.field public static final cigna_coach_icon_bg_press:I = 0x7f02008b

.field public static final cigna_coach_icon_bg_select:I = 0x7f02008c

.field public static final cigna_coach_icon_exercise_green:I = 0x7f02008d

.field public static final cigna_coach_icon_exercise_red:I = 0x7f02008e

.field public static final cigna_coach_icon_exercise_white:I = 0x7f02008f

.field public static final cigna_coach_icon_exercise_yellow:I = 0x7f020090

.field public static final cigna_coach_icon_food_green:I = 0x7f020091

.field public static final cigna_coach_icon_food_red:I = 0x7f020092

.field public static final cigna_coach_icon_food_white:I = 0x7f020093

.field public static final cigna_coach_icon_food_yellow:I = 0x7f020094

.field public static final cigna_coach_icon_sleep_green:I = 0x7f020095

.field public static final cigna_coach_icon_sleep_red:I = 0x7f020096

.field public static final cigna_coach_icon_sleep_white:I = 0x7f020097

.field public static final cigna_coach_icon_sleep_yellow:I = 0x7f020098

.field public static final cigna_coach_icon_stress_green:I = 0x7f020099

.field public static final cigna_coach_icon_stress_red:I = 0x7f02009a

.field public static final cigna_coach_icon_stress_white:I = 0x7f02009b

.field public static final cigna_coach_icon_stress_yellow:I = 0x7f02009c

.field public static final cigna_coach_icon_weight_green:I = 0x7f02009d

.field public static final cigna_coach_icon_weight_red:I = 0x7f02009e

.field public static final cigna_coach_icon_weight_white:I = 0x7f02009f

.field public static final cigna_coach_icon_weight_yellow:I = 0x7f0200a0

.field public static final cigna_current_mission_detail_day_view_background_focused_selected:I = 0x7f0200a1

.field public static final cigna_current_mission_detail_day_view_background_focused_unselected:I = 0x7f0200a2

.field public static final cigna_current_mission_detail_day_view_background_selector:I = 0x7f0200a3

.field public static final cigna_custom_viewpager_indicator_rectangle_selector:I = 0x7f0200a4

.field public static final cigna_dropdown_spinner_selector:I = 0x7f0200a5

.field public static final cigna_expandable_list_selector:I = 0x7f0200a6

.field public static final cigna_footer_next_btn_selector:I = 0x7f0200a7

.field public static final cigna_footer_previous_btn_selector:I = 0x7f0200a8

.field public static final cigna_highlight_button_selector:I = 0x7f0200a9

.field public static final cigna_icon_cap_02:I = 0x7f0200aa

.field public static final cigna_icon_logo_full:I = 0x7f0200ab

.field public static final cigna_img_01:I = 0x7f0200ac

.field public static final cigna_img_02:I = 0x7f0200ad

.field public static final cigna_img_03:I = 0x7f0200ae

.field public static final cigna_img_04:I = 0x7f0200af

.field public static final cigna_img_05:I = 0x7f0200b0

.field public static final cigna_img_06:I = 0x7f0200b1

.field public static final cigna_img_07:I = 0x7f0200b2

.field public static final cigna_img_08:I = 0x7f0200b3

.field public static final cigna_img_09:I = 0x7f0200b4

.field public static final cigna_img_10:I = 0x7f0200b5

.field public static final cigna_img_11:I = 0x7f0200b6

.field public static final cigna_img_12:I = 0x7f0200b7

.field public static final cigna_img_13:I = 0x7f0200b8

.field public static final cigna_img_14:I = 0x7f0200b9

.field public static final cigna_img_15:I = 0x7f0200ba

.field public static final cigna_img_16:I = 0x7f0200bb

.field public static final cigna_img_bg:I = 0x7f0200bc

.field public static final cigna_img_effect:I = 0x7f0200bd

.field public static final cigna_img_effect01:I = 0x7f0200be

.field public static final cigna_img_mask:I = 0x7f0200bf

.field public static final cigna_img_navi_off:I = 0x7f0200c0

.field public static final cigna_img_navi_on:I = 0x7f0200c1

.field public static final cigna_library_custom_viewpager_indicator_rectangle_selector:I = 0x7f0200c2

.field public static final cigna_library_list_icon_exercise:I = 0x7f0200c3

.field public static final cigna_library_list_icon_food:I = 0x7f0200c4

.field public static final cigna_library_list_icon_sleep:I = 0x7f0200c5

.field public static final cigna_library_list_icon_stress:I = 0x7f0200c6

.field public static final cigna_library_list_icon_weight:I = 0x7f0200c7

.field public static final cigna_lifestyle_bubble:I = 0x7f0200c8

.field public static final cigna_lifestyle_down:I = 0x7f0200c9

.field public static final cigna_lifestyle_graph_bg:I = 0x7f0200ca

.field public static final cigna_lifestyle_icon01:I = 0x7f0200cb

.field public static final cigna_lifestyle_icon02:I = 0x7f0200cc

.field public static final cigna_lifestyle_progress:I = 0x7f0200cd

.field public static final cigna_lifestyle_up:I = 0x7f0200ce

.field public static final cigna_list_icon_exercise:I = 0x7f0200cf

.field public static final cigna_list_icon_food:I = 0x7f0200d0

.field public static final cigna_list_icon_sleep:I = 0x7f0200d1

.field public static final cigna_list_icon_stress:I = 0x7f0200d2

.field public static final cigna_list_icon_weight:I = 0x7f0200d3

.field public static final cigna_list_no_selector:I = 0x7f0200d4

.field public static final cigna_list_selector:I = 0x7f0200d5

.field public static final cigna_logheaderholder_selector:I = 0x7f0200d6

.field public static final cigna_loglist_updown_selector:I = 0x7f0200d7

.field public static final cigna_logo_icon_01:I = 0x7f0200d8

.field public static final cigna_logo_icon_02:I = 0x7f0200d9

.field public static final cigna_logo_icon_03:I = 0x7f0200da

.field public static final cigna_logo_icon_focused:I = 0x7f0200db

.field public static final cigna_logo_icon_selector:I = 0x7f0200dc

.field public static final cigna_mission_badge_actionhero:I = 0x7f0200dd

.field public static final cigna_mission_badge_athlete:I = 0x7f0200de

.field public static final cigna_mission_badge_champ:I = 0x7f0200df

.field public static final cigna_mission_badge_dreamcatcher:I = 0x7f0200e0

.field public static final cigna_mission_badge_duelmaster:I = 0x7f0200e1

.field public static final cigna_mission_badge_fitnessguru:I = 0x7f0200e2

.field public static final cigna_mission_badge_foodie:I = 0x7f0200e3

.field public static final cigna_mission_badge_foursquare:I = 0x7f0200e4

.field public static final cigna_mission_badge_gearedup:I = 0x7f0200e5

.field public static final cigna_mission_badge_gogetter:I = 0x7f0200e6

.field public static final cigna_mission_badge_hattrick:I = 0x7f0200e7

.field public static final cigna_mission_badge_healthnut:I = 0x7f0200e8

.field public static final cigna_mission_badge_heavyweight:I = 0x7f0200e9

.field public static final cigna_mission_badge_highfive:I = 0x7f0200ea

.field public static final cigna_mission_badge_knowthyself:I = 0x7f0200eb

.field public static final cigna_mission_badge_lightweight:I = 0x7f0200ec

.field public static final cigna_mission_badge_luckyseven:I = 0x7f0200ed

.field public static final cigna_mission_badge_mirror:I = 0x7f0200ee

.field public static final cigna_mission_badge_movingtheneedle:I = 0x7f0200ef

.field public static final cigna_mission_badge_outofthegate:I = 0x7f0200f0

.field public static final cigna_mission_badge_platebalancer:I = 0x7f0200f1

.field public static final cigna_mission_badge_relaxmaster:I = 0x7f0200f2

.field public static final cigna_mission_badge_rockstar:I = 0x7f0200f3

.field public static final cigna_mission_badge_sixpack:I = 0x7f0200f4

.field public static final cigna_mission_badge_sleepingbeauty:I = 0x7f0200f5

.field public static final cigna_mission_badge_slimtrim:I = 0x7f0200f6

.field public static final cigna_mission_badge_stickwithit:I = 0x7f0200f7

.field public static final cigna_mission_badge_superhero:I = 0x7f0200f8

.field public static final cigna_mission_badge_wellrounded:I = 0x7f0200f9

.field public static final cigna_mission_badge_zenmaster:I = 0x7f0200fa

.field public static final cigna_mission_sub_exercise:I = 0x7f0200fb

.field public static final cigna_mission_sub_food:I = 0x7f0200fc

.field public static final cigna_mission_sub_sleep:I = 0x7f0200fd

.field public static final cigna_mission_sub_stress:I = 0x7f0200fe

.field public static final cigna_mission_sub_weight:I = 0x7f0200ff

.field public static final cigna_mission_week_button_off:I = 0x7f020100

.field public static final cigna_mission_week_button_on:I = 0x7f020101

.field public static final cigna_mission_week_checkbutton_off:I = 0x7f020102

.field public static final cigna_mission_week_checkbutton_on:I = 0x7f020103

.field public static final cigna_normal_btn_selector:I = 0x7f020104

.field public static final cigna_popup_icon_exercise:I = 0x7f020105

.field public static final cigna_popup_icon_food:I = 0x7f020106

.field public static final cigna_popup_icon_sleep:I = 0x7f020107

.field public static final cigna_popup_icon_stress:I = 0x7f020108

.field public static final cigna_popup_icon_weight:I = 0x7f020109

.field public static final cigna_progressbar_polygon:I = 0x7f02010a

.field public static final cigna_search_edit_text_icon_selector:I = 0x7f02010b

.field public static final cigna_select_all_selector:I = 0x7f02010c

.field public static final cigna_selector_dialog_button:I = 0x7f02010d

.field public static final cigna_spn_bg:I = 0x7f02010e

.field public static final cigna_submenu_plus:I = 0x7f02010f

.field public static final cigna_summary_icon_selector:I = 0x7f020110

.field public static final cigna_summary_selector:I = 0x7f020111

.field public static final cigna_tw_btn_right_default_holo_light:I = 0x7f020112

.field public static final cigna_widget_icon_mask:I = 0x7f020113

.field public static final coach_confirm_button_selector:I = 0x7f020114

.field public static final coachs_corner_information:I = 0x7f020115

.field public static final comfortzone_setting_graph_summer:I = 0x7f020116

.field public static final comfortzone_setting_graph_winter:I = 0x7f020117

.field public static final common_btn_bg:I = 0x7f020118

.field public static final common_cbx_bg:I = 0x7f020119

.field public static final common_et_bg:I = 0x7f02011a

.field public static final common_rb_bg:I = 0x7f02011b

.field public static final common_signin_btn_icon_dark:I = 0x7f02011c

.field public static final common_signin_btn_icon_disabled_dark:I = 0x7f02011d

.field public static final common_signin_btn_icon_disabled_focus_dark:I = 0x7f02011e

.field public static final common_signin_btn_icon_disabled_focus_light:I = 0x7f02011f

.field public static final common_signin_btn_icon_disabled_light:I = 0x7f020120

.field public static final common_signin_btn_icon_focus_dark:I = 0x7f020121

.field public static final common_signin_btn_icon_focus_light:I = 0x7f020122

.field public static final common_signin_btn_icon_light:I = 0x7f020123

.field public static final common_signin_btn_icon_normal_dark:I = 0x7f020124

.field public static final common_signin_btn_icon_normal_light:I = 0x7f020125

.field public static final common_signin_btn_icon_pressed_dark:I = 0x7f020126

.field public static final common_signin_btn_icon_pressed_light:I = 0x7f020127

.field public static final common_signin_btn_text_dark:I = 0x7f020128

.field public static final common_signin_btn_text_disabled_dark:I = 0x7f020129

.field public static final common_signin_btn_text_disabled_focus_dark:I = 0x7f02012a

.field public static final common_signin_btn_text_disabled_focus_light:I = 0x7f02012b

.field public static final common_signin_btn_text_disabled_light:I = 0x7f02012c

.field public static final common_signin_btn_text_focus_dark:I = 0x7f02012d

.field public static final common_signin_btn_text_focus_light:I = 0x7f02012e

.field public static final common_signin_btn_text_light:I = 0x7f02012f

.field public static final common_signin_btn_text_normal_dark:I = 0x7f020130

.field public static final common_signin_btn_text_normal_light:I = 0x7f020131

.field public static final common_signin_btn_text_pressed_dark:I = 0x7f020132

.field public static final common_signin_btn_text_pressed_light:I = 0x7f020133

.field public static final common_spn_bg:I = 0x7f020134

.field public static final common_thumb_switch_selector:I = 0x7f020135

.field public static final common_track_switch_selector:I = 0x7f020136

.field public static final compatible_product_selector:I = 0x7f020137

.field public static final contacts_default_caller_id:I = 0x7f020138

.field public static final contacts_default_image_add_icon:I = 0x7f020139

.field public static final contacts_default_image_add_icon_press:I = 0x7f02013a

.field public static final contacts_default_image_add_photo_press:I = 0x7f02013b

.field public static final contacts_default_image_small_03:I = 0x7f02013c

.field public static final custom_cursor:I = 0x7f02013d

.field public static final custom_popup_background_material:I = 0x7f02013e

.field public static final dashboard_selector:I = 0x7f02013f

.field public static final date_bar_selector:I = 0x7f020140

.field public static final dateselector_next_selector:I = 0x7f020141

.field public static final dateselector_prev_selector:I = 0x7f020142

.field public static final day_button_selector:I = 0x7f020143

.field public static final default_button_selector:I = 0x7f020144

.field public static final default_checkbox_selector:I = 0x7f020145

.field public static final default_dropdown_list_item_selector:I = 0x7f020146

.field public static final default_dropdown_list_selector:I = 0x7f020147

.field public static final drag:I = 0x7f020148

.field public static final drawer_icon_blood_glucose:I = 0x7f020149

.field public static final drawer_icon_blood_pressure:I = 0x7f02014a

.field public static final drawer_icon_coach:I = 0x7f02014b

.field public static final drawer_icon_exerise:I = 0x7f02014c

.field public static final drawer_icon_food:I = 0x7f02014d

.field public static final drawer_icon_heartrate:I = 0x7f02014e

.field public static final drawer_icon_home:I = 0x7f02014f

.field public static final drawer_icon_hr:I = 0x7f020150

.field public static final drawer_icon_hygrometer:I = 0x7f020151

.field public static final drawer_icon_more_apps:I = 0x7f020152

.field public static final drawer_icon_mychallenge:I = 0x7f020153

.field public static final drawer_icon_pedometer:I = 0x7f020154

.field public static final drawer_icon_sleep:I = 0x7f020155

.field public static final drawer_icon_spo2:I = 0x7f020156

.field public static final drawer_icon_stress:I = 0x7f020157

.field public static final drawer_icon_uv:I = 0x7f020158

.field public static final drawer_icon_weight:I = 0x7f020159

.field public static final drawer_list_photo_bg_01:I = 0x7f02015a

.field public static final easyhome_shealth_healthy:I = 0x7f02015b

.field public static final easyhome_shealth_inactive:I = 0x7f02015c

.field public static final easyhome_shealth_medal:I = 0x7f02015d

.field public static final easyhome_shealth_step:I = 0x7f02015e

.field public static final easyhome_shealth_step_dim:I = 0x7f02015f

.field public static final edge_icon_selector:I = 0x7f020160

.field public static final edge_icon_selector_gear:I = 0x7f020161

.field public static final edge_icon_selector_healthy:I = 0x7f020162

.field public static final edge_icon_selector_inactive:I = 0x7f020163

.field public static final edge_icon_selector_pause:I = 0x7f020164

.field public static final editfav_checkbox_selector:I = 0x7f020165

.field public static final ex_audio_guide_checkbox_selector:I = 0x7f020166

.field public static final ex_default_checkbox_selector:I = 0x7f020167

.field public static final ex_icon_cancel_press_selector:I = 0x7f020168

.field public static final ex_log_list_group_bg_selector:I = 0x7f020169

.field public static final ex_logheaderholder_selector:I = 0x7f02016a

.field public static final ex_loglist_up_down_selector:I = 0x7f02016b

.field public static final ex_menu_icon_delete:I = 0x7f02016c

.field public static final ex_s_health_care_btn_selector:I = 0x7f02016d

.field public static final ex_searchfiled:I = 0x7f02016e

.field public static final ex_summary_exercisediary_grid_item_image_border_focused:I = 0x7f02016f

.field public static final ex_tw_ic_cancel_selector:I = 0x7f020170

.field public static final exercise_btn_check_off:I = 0x7f020171

.field public static final exercise_btn_check_on:I = 0x7f020172

.field public static final exercise_guide_t_1:I = 0x7f020173

.field public static final exercise_guide_t_2:I = 0x7f020174

.field public static final exercise_pro_action_item_background_focus:I = 0x7f020175

.field public static final exercise_pro_action_item_background_press:I = 0x7f020176

.field public static final exercise_pro_action_item_background_select:I = 0x7f020177

.field public static final exercise_pro_icon_ad01:I = 0x7f020178

.field public static final exercise_pro_icon_audiooff:I = 0x7f020179

.field public static final exercise_pro_icon_audiooff01:I = 0x7f02017a

.field public static final exercise_pro_icon_audioon:I = 0x7f02017b

.field public static final exercise_pro_icon_audioon01:I = 0x7f02017c

.field public static final exercise_pro_icon_calorie:I = 0x7f02017d

.field public static final exercise_pro_icon_calorie01:I = 0x7f02017e

.field public static final exercise_pro_icon_distance:I = 0x7f02017f

.field public static final exercise_pro_icon_distance01:I = 0x7f020180

.field public static final exercise_pro_icon_duration:I = 0x7f020181

.field public static final exercise_pro_icon_duration01:I = 0x7f020182

.field public static final exercise_pro_icon_ecg:I = 0x7f020183

.field public static final exercise_pro_icon_ecg01:I = 0x7f020184

.field public static final exercise_pro_icon_elevation:I = 0x7f020185

.field public static final exercise_pro_icon_elevation01:I = 0x7f020186

.field public static final exercise_pro_icon_goal:I = 0x7f020187

.field public static final exercise_pro_icon_goal1:I = 0x7f020188

.field public static final exercise_pro_icon_music:I = 0x7f020189

.field public static final exercise_pro_icon_speed01:I = 0x7f02018a

.field public static final exercise_pro_summary_bg_effect:I = 0x7f02018b

.field public static final exercise_pro_summary_logo:I = 0x7f02018c

.field public static final f_airview_infopreview_thumb_bg_light:I = 0x7f02018d

.field public static final fairview_internet_folder_hover_bg_dark:I = 0x7f02018e

.field public static final fliper_date_selector:I = 0x7f02018f

.field public static final focus_drawable:I = 0x7f020190

.field public static final food_action_bar_add_button_selector_transparent:I = 0x7f020191

.field public static final food_action_bar_delete_button_selector_transparent:I = 0x7f020192

.field public static final food_check_box_disable_state_drawable:I = 0x7f020193

.field public static final food_controller_icon_delete_normal:I = 0x7f020194

.field public static final food_controller_icon_delete_press:I = 0x7f020195

.field public static final food_down_up_group_arrow_selector:I = 0x7f020196

.field public static final food_dropdown_bg_selector:I = 0x7f020197

.field public static final food_edit_meal_image_delete_button_selector:I = 0x7f020198

.field public static final food_edit_text_custom_cursor:I = 0x7f020199

.field public static final food_favorite_selector:I = 0x7f02019a

.field public static final food_gallery_view_item_selector:I = 0x7f02019b

.field public static final food_gallery_view_shape_selected:I = 0x7f02019c

.field public static final food_image_page_point_selector:I = 0x7f02019d

.field public static final food_input_edit_selector:I = 0x7f02019e

.field public static final food_list_selector:I = 0x7f02019f

.field public static final food_list_subtitle_tab:I = 0x7f0201a0

.field public static final food_list_subtitle_tab_arrow:I = 0x7f0201a1

.field public static final food_list_subtitle_tab_bg:I = 0x7f0201a2

.field public static final food_list_subtitle_tab_text_bg:I = 0x7f0201a3

.field public static final food_meal_plan_check_box_selector:I = 0x7f0201a4

.field public static final food_my_food_expandable_list_item_selector:I = 0x7f0201a5

.field public static final food_next_button_light_selector:I = 0x7f0201a6

.field public static final food_pick_search_edit_text_icon_selector:I = 0x7f0201a7

.field public static final food_pick_search_icon_with_offset:I = 0x7f0201a8

.field public static final food_pick_search_icon_with_offset_default:I = 0x7f0201a9

.field public static final food_pick_selected_item_bg_selector:I = 0x7f0201aa

.field public static final food_portion_slider_first_part:I = 0x7f0201ab

.field public static final food_portion_slider_fours_part:I = 0x7f0201ac

.field public static final food_portion_slider_second_part:I = 0x7f0201ad

.field public static final food_portion_slider_third_part:I = 0x7f0201ae

.field public static final food_previous_button_light_selector:I = 0x7f0201af

.field public static final food_quickinput_image_bg_selector:I = 0x7f0201b0

.field public static final food_quickinput_large_selector:I = 0x7f0201b1

.field public static final food_quickinput_medium_selector:I = 0x7f0201b2

.field public static final food_quickinput_skipped_selector:I = 0x7f0201b3

.field public static final food_quickinput_small_selector:I = 0x7f0201b4

.field public static final food_save_item_menu_icon_selector:I = 0x7f0201b5

.field public static final food_search_cancel_button_selector:I = 0x7f0201b6

.field public static final food_search_edit_text_selector:I = 0x7f0201b7

.field public static final food_summary_bottom_button_selector:I = 0x7f0201b8

.field public static final food_summary_list_icon_selector:I = 0x7f0201b9

.field public static final food_summary_plus_selector:I = 0x7f0201ba

.field public static final food_textfield_selector:I = 0x7f0201bb

.field public static final food_tw_drawerlist_focused_holo_light:I = 0x7f0201bc

.field public static final food_tw_drawerlist_pressed_holo_light:I = 0x7f0201bd

.field public static final food_tw_drawerlist_selected_holo_light:I = 0x7f0201be

.field public static final food_tw_list_icon_create_selector:I = 0x7f0201bf

.field public static final frag_scanning:I = 0x7f0201c0

.field public static final gallery:I = 0x7f0201c1

.field public static final graph_handleroverimage:I = 0x7f0201c2

.field public static final graph_in_range_point_image:I = 0x7f0201c3

.field public static final graph_inrangeimage:I = 0x7f0201c4

.field public static final graph_normal_point_image:I = 0x7f0201c5

.field public static final graph_normalimage:I = 0x7f0201c6

.field public static final hestia_chart_icon_01:I = 0x7f0201c7

.field public static final hestia_chart_icon_02:I = 0x7f0201c8

.field public static final hestia_chart_icon_03:I = 0x7f0201c9

.field public static final hestia_chart_icon_04:I = 0x7f0201ca

.field public static final hestia_chart_icon_05:I = 0x7f0201cb

.field public static final hestia_log_uv_icon_01:I = 0x7f0201cc

.field public static final hestia_log_uv_icon_02:I = 0x7f0201cd

.field public static final hestia_log_uv_icon_03:I = 0x7f0201ce

.field public static final hestia_log_uv_icon_04:I = 0x7f0201cf

.field public static final hestia_log_uv_icon_05:I = 0x7f0201d0

.field public static final hestia_summary_spo2_icon_bpm:I = 0x7f0201d1

.field public static final hestia_summary_spo2_icon_error:I = 0x7f0201d2

.field public static final hestia_summary_spo2_icon_finish:I = 0x7f0201d3

.field public static final hestia_summary_spo2_icon_high:I = 0x7f0201d4

.field public static final hestia_summary_spo2_icon_low:I = 0x7f0201d5

.field public static final hestia_summary_spo2_icon_measuring:I = 0x7f0201d6

.field public static final hestia_summary_spo2_icon_medium:I = 0x7f0201d7

.field public static final hestia_summary_spo2_icon_ready:I = 0x7f0201d8

.field public static final hestia_summary_uv_icon_01:I = 0x7f0201d9

.field public static final hestia_summary_uv_icon_02:I = 0x7f0201da

.field public static final hestia_summary_uv_icon_03:I = 0x7f0201db

.field public static final hestia_summary_uv_icon_04:I = 0x7f0201dc

.field public static final hestia_summary_uv_icon_05:I = 0x7f0201dd

.field public static final hestia_summary_uv_icon_error:I = 0x7f0201de

.field public static final hestia_summary_uv_icon_measuring:I = 0x7f0201df

.field public static final hestia_summary_uv_icon_ready:I = 0x7f0201e0

.field public static final home_add_selector:I = 0x7f0201e1

.field public static final home_del_focus:I = 0x7f0201e2

.field public static final home_del_nor:I = 0x7f0201e3

.field public static final home_del_press:I = 0x7f0201e4

.field public static final home_del_select:I = 0x7f0201e5

.field public static final home_del_selector:I = 0x7f0201e6

.field public static final home_edit_grid_selector:I = 0x7f0201e7

.field public static final home_favorite_check_box_selector:I = 0x7f0201e8

.field public static final home_favorite_item_selector:I = 0x7f0201e9

.field public static final home_sync_progress:I = 0x7f0201ea

.field public static final horizontal_input_module_back_btn_selector:I = 0x7f0201eb

.field public static final horizontal_input_module_controller_view_background_green_pin:I = 0x7f0201ec

.field public static final horizontal_input_module_controller_view_background_orange_pin:I = 0x7f0201ed

.field public static final horizontal_input_module_next_btn_selector:I = 0x7f0201ee

.field public static final hr_fail_alpha_1:I = 0x7f0201ef

.field public static final hr_fail_alpha_2:I = 0x7f0201f0

.field public static final hr_fail_alpha_3:I = 0x7f0201f1

.field public static final hr_fail_t_1:I = 0x7f0201f2

.field public static final hr_fail_t_2:I = 0x7f0201f3

.field public static final hr_fail_t_3:I = 0x7f0201f4

.field public static final hr_green:I = 0x7f0201f5

.field public static final hr_guide_alpha_1:I = 0x7f0201f6

.field public static final hr_guide_alpha_2:I = 0x7f0201f7

.field public static final hr_guide_alpha_3:I = 0x7f0201f8

.field public static final hr_guide_alpha_4:I = 0x7f0201f9

.field public static final hr_guide_t_1:I = 0x7f0201fa

.field public static final hr_guide_t_2:I = 0x7f0201fb

.field public static final hr_guide_t_3:I = 0x7f0201fc

.field public static final hr_guide_t_4:I = 0x7f0201fd

.field public static final hr_guide_t_5:I = 0x7f0201fe

.field public static final hr_guide_t_6:I = 0x7f0201ff

.field public static final hr_icon_in_g_00120:I = 0x7f020200

.field public static final hr_icon_in_o_00120:I = 0x7f020201

.field public static final hr_orange:I = 0x7f020202

.field public static final hrm_scover_close_button:I = 0x7f020203

.field public static final hrm_selector_dialog_button:I = 0x7f020204

.field public static final hrm_selector_log_group_expand:I = 0x7f020205

.field public static final hrm_selector_log_group_expandable_background:I = 0x7f020206

.field public static final hrm_selector_summary_button:I = 0x7f020207

.field public static final hrm_selector_summary_button_rec:I = 0x7f020208

.field public static final hrm_summary_tag_icon_selector:I = 0x7f020209

.field public static final hrm_tag_select_logedit:I = 0x7f02020a

.field public static final ic_launcher:I = 0x7f02020b

.field public static final ic_menu_moreoverflow_disable_holo_light:I = 0x7f02020c

.field public static final ic_menu_moreoverflow_normal_holo_light:I = 0x7f02020d

.field public static final ic_plusone_medium_off_client:I = 0x7f02020e

.field public static final ic_plusone_small_off_client:I = 0x7f02020f

.field public static final ic_plusone_standard_off_client:I = 0x7f020210

.field public static final ic_plusone_tall_off_client:I = 0x7f020211

.field public static final icon_foreground:I = 0x7f020212

.field public static final img_panels_screen_s_health_steps:I = 0x7f020213

.field public static final imported_from_weight_background_tmp:I = 0x7f020214

.field public static final imported_from_weight_button2_tmp:I = 0x7f020215

.field public static final imported_from_weight_button_tmp:I = 0x7f020216

.field public static final imported_from_weight_line1_tmp:I = 0x7f020217

.field public static final imported_from_weight_line2_tmp:I = 0x7f020218

.field public static final initial_profile_next_button_selector:I = 0x7f020219

.field public static final input_common_edit_text_background_selector:I = 0x7f02021a

.field public static final input_edit_text_background_selector:I = 0x7f02021b

.field public static final intro_next_button_selector:I = 0x7f02021c

.field public static final life_times_setup_btn_arrow:I = 0x7f02021d

.field public static final life_times_setup_btn_arrow_disabled:I = 0x7f02021e

.field public static final line1_2:I = 0x7f02021f

.field public static final line2_2:I = 0x7f020220

.field public static final list_select_all_selector:I = 0x7f020221

.field public static final list_selector:I = 0x7f020222

.field public static final log_list_group_bg_selector:I = 0x7f020223

.field public static final log_list_group_header_arrow_selector:I = 0x7f020224

.field public static final log_list_group_header_background_selector:I = 0x7f020225

.field public static final log_list_item_selector:I = 0x7f020226

.field public static final log_list_memo_icon:I = 0x7f020227

.field public static final log_list_selectall_selector:I = 0x7f020228

.field public static final logo_adidas:I = 0x7f020229

.field public static final logo_garmin:I = 0x7f02022a

.field public static final logo_omron:I = 0x7f02022b

.field public static final logo_samsung:I = 0x7f02022c

.field public static final logo_timex:I = 0x7f02022d

.field public static final logo_wahoo:I = 0x7f02022e

.field public static final logsdetail_indicator_shadow:I = 0x7f02022f

.field public static final menu_icon_delete:I = 0x7f020230

.field public static final menu_icon_filter:I = 0x7f020231

.field public static final menu_icon_guide:I = 0x7f020232

.field public static final menu_icon_print:I = 0x7f020233

.field public static final menu_icon_set_goal:I = 0x7f020234

.field public static final menu_icon_share:I = 0x7f020235

.field public static final menu_selector:I = 0x7f020236

.field public static final mypage_img_shadow:I = 0x7f020237

.field public static final mypage_medal_calorie:I = 0x7f020238

.field public static final mypage_medal_exercise:I = 0x7f020239

.field public static final mypage_medal_food:I = 0x7f02023a

.field public static final mypage_medal_pedometer:I = 0x7f02023b

.field public static final mypage_nomedal_calorie:I = 0x7f02023c

.field public static final mypage_nomedal_exercise:I = 0x7f02023d

.field public static final mypage_nomedal_food:I = 0x7f02023e

.field public static final mypage_nomedal_pedometer:I = 0x7f02023f

.field public static final mypage_profile_name_icon_female:I = 0x7f020240

.field public static final mypage_profile_name_icon_male:I = 0x7f020241

.field public static final myprofile_polygon:I = 0x7f020242

.field public static final navigation_selector:I = 0x7f020243

.field public static final next_button_light_selector:I = 0x7f020244

.field public static final no_data_chart_image:I = 0x7f020245

.field public static final no_item_bg_light_v:I = 0x7f020246

.field public static final noti_list_ic_close_dim:I = 0x7f020247

.field public static final noti_list_ic_close_focus:I = 0x7f020248

.field public static final noti_list_ic_close_normal:I = 0x7f020249

.field public static final noti_list_ic_close_press:I = 0x7f02024a

.field public static final noti_list_ic_close_select:I = 0x7f02024b

.field public static final options_menu_item_selector:I = 0x7f02024c

.field public static final popup_bottom_button_selector_bg:I = 0x7f02024d

.field public static final popup_button_selector_bg:I = 0x7f02024e

.field public static final previous_button_light_selector:I = 0x7f02024f

.field public static final pro_action_bar_camera_selector:I = 0x7f020250

.field public static final pro_action_bar_during_bg_selector:I = 0x7f020251

.field public static final pro_action_bar_map_button_selector_transparent:I = 0x7f020252

.field public static final pro_btn_round_minus_exercise_selector:I = 0x7f020253

.field public static final pro_color_route_gradient:I = 0x7f020254

.field public static final pro_default_button_selector:I = 0x7f020255

.field public static final pro_default_checkbox_selector:I = 0x7f020256

.field public static final pro_default_checkbox_selector_2:I = 0x7f020257

.field public static final pro_default_dropdown_list_item_selector:I = 0x7f020258

.field public static final pro_exercise_activity_type_button_bg_selector:I = 0x7f020259

.field public static final pro_exercise_activity_type_cycling_selector:I = 0x7f02025a

.field public static final pro_exercise_activity_type_hiking_selector:I = 0x7f02025b

.field public static final pro_exercise_activity_type_running_selector:I = 0x7f02025c

.field public static final pro_exercise_activity_type_walking_selector:I = 0x7f02025d

.field public static final pro_exercise_ant_button_selector:I = 0x7f02025e

.field public static final pro_exercise_pro_arrow_next_bottom_bg_selector:I = 0x7f02025f

.field public static final pro_exercise_pro_arrow_next_bottom_selector:I = 0x7f020260

.field public static final pro_exercise_pro_arrow_previous_bottom_selector:I = 0x7f020261

.field public static final pro_exercise_pro_te_guide_link_selector:I = 0x7f020262

.field public static final pro_exercise_pro_two_button_prev_bg_selector:I = 0x7f020263

.field public static final pro_exercise_pro_workout_hold_button_selector:I = 0x7f020264

.field public static final pro_expand_button_selector_for_consent:I = 0x7f020265

.field public static final pro_food_image_page_point_selector:I = 0x7f020266

.field public static final pro_goal_bubble_selector:I = 0x7f020267

.field public static final pro_goal_selector:I = 0x7f020268

.field public static final pro_list_selector:I = 0x7f020269

.field public static final pro_list_selector_for_consent:I = 0x7f02026a

.field public static final pro_listview_scrollbar_drawable:I = 0x7f02026b

.field public static final pro_logheaderholder_selector:I = 0x7f02026c

.field public static final pro_loglist_up_down_selector:I = 0x7f02026d

.field public static final pro_map_auto_btn_selector:I = 0x7f02026e

.field public static final pro_muisc_rewind_selector:I = 0x7f02026f

.field public static final pro_music_pause_selector:I = 0x7f020270

.field public static final pro_music_play_selector:I = 0x7f020271

.field public static final pro_music_unwind_selector:I = 0x7f020272

.field public static final pro_realtime_arrow_next_bottom:I = 0x7f020273

.field public static final pro_realtime_arrow_previous_bottom:I = 0x7f020274

.field public static final pro_realtime_drop_arrow_selector:I = 0x7f020275

.field public static final pro_realtime_map_dropdown_selector:I = 0x7f020276

.field public static final pro_realtime_progress_drawable:I = 0x7f020277

.field public static final pro_realtime_status_selector:I = 0x7f020278

.field public static final pro_realtime_two_button_next_selector:I = 0x7f020279

.field public static final pro_realtime_two_button_prev_selector:I = 0x7f02027a

.field public static final pro_s_health_h_list_icon_goal_selector:I = 0x7f02027b

.field public static final pro_s_health_summary_btn_selector:I = 0x7f02027c

.field public static final pro_s_health_summary_exercise_pro_check_selector:I = 0x7f02027d

.field public static final pro_summary_exercisediary_grid_item_image_border_focused:I = 0x7f02027e

.field public static final pro_unlock_btn_bg_selector:I = 0x7f02027f

.field public static final profile_default_image_selector:I = 0x7f020280

.field public static final profile_edit_text_background_selector:I = 0x7f020281

.field public static final profile_image_selector:I = 0x7f020282

.field public static final profile_info_select_list:I = 0x7f020283

.field public static final profile_set_image_selector:I = 0x7f020284

.field public static final profile_setting_dropdown_spinner_selector:I = 0x7f020285

.field public static final profilesetting_bg:I = 0x7f020286

.field public static final profilesetting_bottom_bg:I = 0x7f020287

.field public static final s_health_accessory_big_galaxy_gear:I = 0x7f020288

.field public static final s_health_accessory_btn_paired:I = 0x7f020289

.field public static final s_health_accessory_button_bg_focused:I = 0x7f02028a

.field public static final s_health_accessory_button_bg_normal:I = 0x7f02028b

.field public static final s_health_accessory_button_bg_pressed:I = 0x7f02028c

.field public static final s_health_accessory_button_bg_selected:I = 0x7f02028d

.field public static final s_health_accessory_icon_ant:I = 0x7f02028e

.field public static final s_health_accessory_icon_bluetooth:I = 0x7f02028f

.field public static final s_health_accessory_icon_usb:I = 0x7f020290

.field public static final s_health_accessory_text_icon_samsung:I = 0x7f020291

.field public static final s_health_accessory_text_icon_ultraeasy:I = 0x7f020292

.field public static final s_health_account_image_01:I = 0x7f020293

.field public static final s_health_account_image_03:I = 0x7f020294

.field public static final s_health_account_page_1:I = 0x7f020295

.field public static final s_health_account_page_2:I = 0x7f020296

.field public static final s_health_action_bar_back_nor:I = 0x7f020297

.field public static final s_health_action_bar_focus:I = 0x7f020298

.field public static final s_health_action_bar_icon_edit_nor:I = 0x7f020299

.field public static final s_health_action_bar_icon_loglist_nor:I = 0x7f02029a

.field public static final s_health_action_bar_icon_loglist_nor12:I = 0x7f02029b

.field public static final s_health_action_bar_icon_nor:I = 0x7f02029c

.field public static final s_health_action_bar_icon_paired_dim:I = 0x7f02029d

.field public static final s_health_action_bar_icon_paired_nor:I = 0x7f02029e

.field public static final s_health_action_bar_icon_trashcan_nor:I = 0x7f02029f

.field public static final s_health_action_bar_main_icon11:I = 0x7f0202a0

.field public static final s_health_actionbar_info:I = 0x7f0202a1

.field public static final s_health_blood_glucose_list_dot:I = 0x7f0202a2

.field public static final s_health_blood_glucose_mask:I = 0x7f0202a3

.field public static final s_health_blood_glucose_orange:I = 0x7f0202a4

.field public static final s_health_blood_glucose_setgoal:I = 0x7f0202a5

.field public static final s_health_blood_glucose_setgoal_pin_green:I = 0x7f0202a6

.field public static final s_health_blood_glucose_setgoal_pin_orange:I = 0x7f0202a7

.field public static final s_health_bottom_btn_icon:I = 0x7f0202a8

.field public static final s_health_calendar_bar:I = 0x7f0202a9

.field public static final s_health_calendar_h_sub_title_btn_normal:I = 0x7f0202aa

.field public static final s_health_calendar_h_sub_title_btn_press:I = 0x7f0202ab

.field public static final s_health_calendar_today:I = 0x7f0202ac

.field public static final s_health_calories_burned_exercise:I = 0x7f0202ad

.field public static final s_health_calories_burned_pedometer:I = 0x7f0202ae

.field public static final s_health_care_summary_btn_disabled:I = 0x7f0202af

.field public static final s_health_care_summary_btn_focus:I = 0x7f0202b0

.field public static final s_health_care_summary_btn_normal:I = 0x7f0202b1

.field public static final s_health_care_summary_btn_press:I = 0x7f0202b2

.field public static final s_health_care_summary_btn_select:I = 0x7f0202b3

.field public static final s_health_comfort_detail_icon_accessary:I = 0x7f0202b4

.field public static final s_health_comfort_fill_summer:I = 0x7f0202b5

.field public static final s_health_comfort_fill_winter:I = 0x7f0202b6

.field public static final s_health_comfort_graph_dot_01:I = 0x7f0202b7

.field public static final s_health_comfort_graph_dot_02:I = 0x7f0202b8

.field public static final s_health_comfort_graph_settings_bg:I = 0x7f0202b9

.field public static final s_health_comfort_stroke_green:I = 0x7f0202ba

.field public static final s_health_controller_icon_star_dim:I = 0x7f0202bb

.field public static final s_health_controller_icon_star_normal:I = 0x7f0202bc

.field public static final s_health_controller_icon_star_on_dim:I = 0x7f0202bd

.field public static final s_health_controller_icon_star_on_focus:I = 0x7f0202be

.field public static final s_health_controller_icon_star_on_focus_dim:I = 0x7f0202bf

.field public static final s_health_controller_icon_star_on_normal:I = 0x7f0202c0

.field public static final s_health_diary_wizard_btn_f_left:I = 0x7f0202c1

.field public static final s_health_diary_wizard_btn_f_right:I = 0x7f0202c2

.field public static final s_health_diary_wizard_btn_line:I = 0x7f0202c3

.field public static final s_health_diary_wizard_btn_n_left:I = 0x7f0202c4

.field public static final s_health_diary_wizard_btn_n_right:I = 0x7f0202c5

.field public static final s_health_diary_wizard_btn_p_left:I = 0x7f0202c6

.field public static final s_health_diary_wizard_btn_p_right:I = 0x7f0202c7

.field public static final s_health_diary_wizard_btn_s_left:I = 0x7f0202c8

.field public static final s_health_diary_wizard_btn_s_right:I = 0x7f0202c9

.field public static final s_health_divider:I = 0x7f0202ca

.field public static final s_health_drawer_setting:I = 0x7f0202cb

.field public static final s_health_ecg_list_icon01:I = 0x7f0202cc

.field public static final s_health_exercise_action_bar_bg:I = 0x7f0202cd

.field public static final s_health_exercise_add_camera:I = 0x7f0202ce

.field public static final s_health_exercise_add_gallery:I = 0x7f0202cf

.field public static final s_health_exercise_bubble:I = 0x7f0202d0

.field public static final s_health_exercise_button_focus:I = 0x7f0202d1

.field public static final s_health_exercise_button_normal:I = 0x7f0202d2

.field public static final s_health_exercise_button_press:I = 0x7f0202d3

.field public static final s_health_exercise_button_selected:I = 0x7f0202d4

.field public static final s_health_exercise_button_selected_focus:I = 0x7f0202d5

.field public static final s_health_exercise_cycling_icon_normal:I = 0x7f0202d6

.field public static final s_health_exercise_cycling_icon_press:I = 0x7f0202d7

.field public static final s_health_exercise_cycling_icon_selected:I = 0x7f0202d8

.field public static final s_health_exercise_data_bg:I = 0x7f0202d9

.field public static final s_health_exercise_hiking_icon_normal:I = 0x7f0202da

.field public static final s_health_exercise_hiking_icon_press:I = 0x7f0202db

.field public static final s_health_exercise_hiking_icon_selected:I = 0x7f0202dc

.field public static final s_health_exercise_icon_memo:I = 0x7f0202dd

.field public static final s_health_exercise_icon_next_page:I = 0x7f0202de

.field public static final s_health_exercise_icon_select_page:I = 0x7f0202df

.field public static final s_health_exercise_icon_xl_aerobics:I = 0x7f0202e0

.field public static final s_health_exercise_icon_xl_aquarobics:I = 0x7f0202e1

.field public static final s_health_exercise_icon_xl_badminton:I = 0x7f0202e2

.field public static final s_health_exercise_icon_xl_baseball:I = 0x7f0202e3

.field public static final s_health_exercise_icon_xl_basketball:I = 0x7f0202e4

.field public static final s_health_exercise_icon_xl_bicycling:I = 0x7f0202e5

.field public static final s_health_exercise_icon_xl_billiards:I = 0x7f0202e6

.field public static final s_health_exercise_icon_xl_bowling:I = 0x7f0202e7

.field public static final s_health_exercise_icon_xl_boxing:I = 0x7f0202e8

.field public static final s_health_exercise_icon_xl_chin_up:I = 0x7f0202e9

.field public static final s_health_exercise_icon_xl_dance:I = 0x7f0202ea

.field public static final s_health_exercise_icon_xl_default:I = 0x7f0202eb

.field public static final s_health_exercise_icon_xl_fasting_walking:I = 0x7f0202ec

.field public static final s_health_exercise_icon_xl_golf:I = 0x7f0202ed

.field public static final s_health_exercise_icon_xl_handball:I = 0x7f0202ee

.field public static final s_health_exercise_icon_xl_hanglider:I = 0x7f0202ef

.field public static final s_health_exercise_icon_xl_hiking:I = 0x7f0202f0

.field public static final s_health_exercise_icon_xl_horseback_riding:I = 0x7f0202f1

.field public static final s_health_exercise_icon_xl_hulahoop:I = 0x7f0202f2

.field public static final s_health_exercise_icon_xl_inline_skating:I = 0x7f0202f3

.field public static final s_health_exercise_icon_xl_judo:I = 0x7f0202f4

.field public static final s_health_exercise_icon_xl_jumping_rope:I = 0x7f0202f5

.field public static final s_health_exercise_icon_xl_power_walking:I = 0x7f0202f6

.field public static final s_health_exercise_icon_xl_press_up:I = 0x7f0202f7

.field public static final s_health_exercise_icon_xl_rock_climbing:I = 0x7f0202f8

.field public static final s_health_exercise_icon_xl_run:I = 0x7f0202f9

.field public static final s_health_exercise_icon_xl_sit_up:I = 0x7f0202fa

.field public static final s_health_exercise_icon_xl_skating:I = 0x7f0202fb

.field public static final s_health_exercise_icon_xl_ski:I = 0x7f0202fc

.field public static final s_health_exercise_icon_xl_skin_scuba_diving:I = 0x7f0202fd

.field public static final s_health_exercise_icon_xl_soccer:I = 0x7f0202fe

.field public static final s_health_exercise_icon_xl_softball:I = 0x7f0202ff

.field public static final s_health_exercise_icon_xl_sports_dance:I = 0x7f020300

.field public static final s_health_exercise_icon_xl_squash:I = 0x7f020301

.field public static final s_health_exercise_icon_xl_stationary_bicycle:I = 0x7f020302

.field public static final s_health_exercise_icon_xl_step_machine:I = 0x7f020303

.field public static final s_health_exercise_icon_xl_swimming:I = 0x7f020304

.field public static final s_health_exercise_icon_xl_table_tennis:I = 0x7f020305

.field public static final s_health_exercise_icon_xl_taekwondo:I = 0x7f020306

.field public static final s_health_exercise_icon_xl_taichi:I = 0x7f020307

.field public static final s_health_exercise_icon_xl_tennis:I = 0x7f020308

.field public static final s_health_exercise_icon_xl_volleyball:I = 0x7f020309

.field public static final s_health_exercise_icon_xl_walking:I = 0x7f02030a

.field public static final s_health_exercise_icon_xl_walking_down_stairs:I = 0x7f02030b

.field public static final s_health_exercise_icon_xl_walking_up_stairs:I = 0x7f02030c

.field public static final s_health_exercise_icon_xl_water_skiing:I = 0x7f02030d

.field public static final s_health_exercise_icon_xl_weight_machine:I = 0x7f02030e

.field public static final s_health_exercise_icon_xl_yoga:I = 0x7f02030f

.field public static final s_health_exercise_increase_down01:I = 0x7f020310

.field public static final s_health_exercise_increase_down02:I = 0x7f020311

.field public static final s_health_exercise_increase_down03:I = 0x7f020312

.field public static final s_health_exercise_increase_down04:I = 0x7f020313

.field public static final s_health_exercise_increase_keep:I = 0x7f020314

.field public static final s_health_exercise_increase_medal:I = 0x7f020315

.field public static final s_health_exercise_increase_up01:I = 0x7f020316

.field public static final s_health_exercise_increase_up02:I = 0x7f020317

.field public static final s_health_exercise_increase_up03:I = 0x7f020318

.field public static final s_health_exercise_increase_up04:I = 0x7f020319

.field public static final s_health_exercise_loglist_duration_icon:I = 0x7f02031a

.field public static final s_health_exercise_loglist_time_icon:I = 0x7f02031b

.field public static final s_health_exercise_map_hr_popup_nor:I = 0x7f02031c

.field public static final s_health_exercise_measuring_heart_logo_01:I = 0x7f02031d

.field public static final s_health_exercise_noitem:I = 0x7f02031e

.field public static final s_health_exercise_pro_button_lock:I = 0x7f02031f

.field public static final s_health_exercise_pro_button_lock_press:I = 0x7f020320

.field public static final s_health_exercise_pro_list_icon01:I = 0x7f020321

.field public static final s_health_exercise_pro_list_icon03:I = 0x7f020322

.field public static final s_health_exercise_pro_list_icon06:I = 0x7f020323

.field public static final s_health_exercise_progressbar_polygon:I = 0x7f020324

.field public static final s_health_exercise_running_icon_normal:I = 0x7f020325

.field public static final s_health_exercise_running_icon_press:I = 0x7f020326

.field public static final s_health_exercise_running_icon_selected:I = 0x7f020327

.field public static final s_health_exercise_thumbnail_aerobic:I = 0x7f020328

.field public static final s_health_exercise_thumbnail_aquarobics:I = 0x7f020329

.field public static final s_health_exercise_thumbnail_archery:I = 0x7f02032a

.field public static final s_health_exercise_thumbnail_badminton:I = 0x7f02032b

.field public static final s_health_exercise_thumbnail_ballet:I = 0x7f02032c

.field public static final s_health_exercise_thumbnail_balllroomdance:I = 0x7f02032d

.field public static final s_health_exercise_thumbnail_barbellbenchpress:I = 0x7f02032e

.field public static final s_health_exercise_thumbnail_barbellbentoverrow:I = 0x7f02032f

.field public static final s_health_exercise_thumbnail_barbellcurl:I = 0x7f020330

.field public static final s_health_exercise_thumbnail_barbellsquat:I = 0x7f020331

.field public static final s_health_exercise_thumbnail_barbelluprightrow:I = 0x7f020332

.field public static final s_health_exercise_thumbnail_baseball:I = 0x7f020333

.field public static final s_health_exercise_thumbnail_basketball:I = 0x7f020334

.field public static final s_health_exercise_thumbnail_bg:I = 0x7f020335

.field public static final s_health_exercise_thumbnail_bicyling:I = 0x7f020336

.field public static final s_health_exercise_thumbnail_billiard:I = 0x7f020337

.field public static final s_health_exercise_thumbnail_bowling:I = 0x7f020338

.field public static final s_health_exercise_thumbnail_boxing:I = 0x7f020339

.field public static final s_health_exercise_thumbnail_canoeingrowing:I = 0x7f02033a

.field public static final s_health_exercise_thumbnail_chinup:I = 0x7f02033b

.field public static final s_health_exercise_thumbnail_crunch:I = 0x7f02033c

.field public static final s_health_exercise_thumbnail_dance:I = 0x7f02033d

.field public static final s_health_exercise_thumbnail_dumbbellbenchpress:I = 0x7f02033e

.field public static final s_health_exercise_thumbnail_dumbbellkickback:I = 0x7f02033f

.field public static final s_health_exercise_thumbnail_dumbbelllateralraise:I = 0x7f020340

.field public static final s_health_exercise_thumbnail_dumbbelloverheadpress:I = 0x7f020341

.field public static final s_health_exercise_thumbnail_fastwalking:I = 0x7f020342

.field public static final s_health_exercise_thumbnail_fieldhockey:I = 0x7f020343

.field public static final s_health_exercise_thumbnail_flatbenchdumbbellfly:I = 0x7f020344

.field public static final s_health_exercise_thumbnail_golf:I = 0x7f020345

.field public static final s_health_exercise_thumbnail_handball:I = 0x7f020346

.field public static final s_health_exercise_thumbnail_hangliding:I = 0x7f020347

.field public static final s_health_exercise_thumbnail_hiking:I = 0x7f020348

.field public static final s_health_exercise_thumbnail_horsebackriding:I = 0x7f020349

.field public static final s_health_exercise_thumbnail_hulahooping:I = 0x7f02034a

.field public static final s_health_exercise_thumbnail_icedancing:I = 0x7f02034b

.field public static final s_health_exercise_thumbnail_icehockey:I = 0x7f02034c

.field public static final s_health_exercise_thumbnail_inclinebarbellpress:I = 0x7f02034d

.field public static final s_health_exercise_thumbnail_inclinedumbbellcurl:I = 0x7f02034e

.field public static final s_health_exercise_thumbnail_inlineskating:I = 0x7f02034f

.field public static final s_health_exercise_thumbnail_judo:I = 0x7f020350

.field public static final s_health_exercise_thumbnail_jumpingrope:I = 0x7f020351

.field public static final s_health_exercise_thumbnail_latpulldown:I = 0x7f020352

.field public static final s_health_exercise_thumbnail_legextension:I = 0x7f020353

.field public static final s_health_exercise_thumbnail_legpress:I = 0x7f020354

.field public static final s_health_exercise_thumbnail_lyingbarbellextension:I = 0x7f020355

.field public static final s_health_exercise_thumbnail_lyinglegcurl:I = 0x7f020356

.field public static final s_health_exercise_thumbnail_onearmdumbbellrow:I = 0x7f020357

.field public static final s_health_exercise_thumbnail_others:I = 0x7f020358

.field public static final s_health_exercise_thumbnail_pedometer:I = 0x7f020359

.field public static final s_health_exercise_thumbnail_pilates:I = 0x7f02035a

.field public static final s_health_exercise_thumbnail_pistolshooting:I = 0x7f02035b

.field public static final s_health_exercise_thumbnail_powerwalking:I = 0x7f02035c

.field public static final s_health_exercise_thumbnail_preachercurlmachine:I = 0x7f02035d

.field public static final s_health_exercise_thumbnail_pushup:I = 0x7f02035e

.field public static final s_health_exercise_thumbnail_reversecrunch:I = 0x7f02035f

.field public static final s_health_exercise_thumbnail_rockclimbing:I = 0x7f020360

.field public static final s_health_exercise_thumbnail_romaniandeadlift:I = 0x7f020361

.field public static final s_health_exercise_thumbnail_ropepressdown:I = 0x7f020362

.field public static final s_health_exercise_thumbnail_rugby:I = 0x7f020363

.field public static final s_health_exercise_thumbnail_runninggeneral:I = 0x7f020364

.field public static final s_health_exercise_thumbnail_sailingyachtingforleisure:I = 0x7f020365

.field public static final s_health_exercise_thumbnail_seatedcalfraise:I = 0x7f020366

.field public static final s_health_exercise_thumbnail_seatedlegcurl:I = 0x7f020367

.field public static final s_health_exercise_thumbnail_situp:I = 0x7f020368

.field public static final s_health_exercise_thumbnail_skating:I = 0x7f020369

.field public static final s_health_exercise_thumbnail_skiing:I = 0x7f02036a

.field public static final s_health_exercise_thumbnail_skindiving:I = 0x7f02036b

.field public static final s_health_exercise_thumbnail_slowwalking:I = 0x7f02036c

.field public static final s_health_exercise_thumbnail_smithmachineuprightrow:I = 0x7f02036d

.field public static final s_health_exercise_thumbnail_snowboarding:I = 0x7f02036e

.field public static final s_health_exercise_thumbnail_soccer:I = 0x7f02036f

.field public static final s_health_exercise_thumbnail_softball:I = 0x7f020370

.field public static final s_health_exercise_thumbnail_squash:I = 0x7f020371

.field public static final s_health_exercise_thumbnail_standingcalfraise:I = 0x7f020372

.field public static final s_health_exercise_thumbnail_stationarybicycle:I = 0x7f020373

.field public static final s_health_exercise_thumbnail_stepmachine:I = 0x7f020374

.field public static final s_health_exercise_thumbnail_swimming:I = 0x7f020375

.field public static final s_health_exercise_thumbnail_tabletennis:I = 0x7f020376

.field public static final s_health_exercise_thumbnail_taekwondo:I = 0x7f020377

.field public static final s_health_exercise_thumbnail_taichi:I = 0x7f020378

.field public static final s_health_exercise_thumbnail_tennis:I = 0x7f020379

.field public static final s_health_exercise_thumbnail_tretching:I = 0x7f02037a

.field public static final s_health_exercise_thumbnail_volleyball:I = 0x7f02037b

.field public static final s_health_exercise_thumbnail_walking:I = 0x7f02037c

.field public static final s_health_exercise_thumbnail_walkingdownstairs:I = 0x7f02037d

.field public static final s_health_exercise_thumbnail_walkingupstairs:I = 0x7f02037e

.field public static final s_health_exercise_thumbnail_watersking:I = 0x7f02037f

.field public static final s_health_exercise_thumbnail_weightmachine:I = 0x7f020380

.field public static final s_health_exercise_thumbnail_yoga:I = 0x7f020381

.field public static final s_health_exercise_top_icon_hr:I = 0x7f020382

.field public static final s_health_exercise_walking_icon_normal:I = 0x7f020383

.field public static final s_health_exercise_walking_icon_press:I = 0x7f020384

.field public static final s_health_exercise_walking_icon_selected:I = 0x7f020385

.field public static final s_health_exercise_wallpaper:I = 0x7f020386

.field public static final s_health_favorite_add_focused:I = 0x7f020387

.field public static final s_health_favorite_add_normal:I = 0x7f020388

.field public static final s_health_favorite_add_press:I = 0x7f020389

.field public static final s_health_favorite_add_selected:I = 0x7f02038a

.field public static final s_health_favorite_icon_coach:I = 0x7f02038b

.field public static final s_health_favorite_icon_exercise:I = 0x7f02038c

.field public static final s_health_favorite_icon_food:I = 0x7f02038d

.field public static final s_health_favorite_icon_hr:I = 0x7f02038e

.field public static final s_health_favorite_icon_hygrometer:I = 0x7f02038f

.field public static final s_health_favorite_icon_moreapps:I = 0x7f020390

.field public static final s_health_favorite_icon_pedometer:I = 0x7f020391

.field public static final s_health_favorite_icon_sleep_2:I = 0x7f020392

.field public static final s_health_favorite_icon_spo2:I = 0x7f020393

.field public static final s_health_favorite_icon_stress:I = 0x7f020394

.field public static final s_health_favorite_icon_uv:I = 0x7f020395

.field public static final s_health_favorite_icon_weight:I = 0x7f020396

.field public static final s_health_favorites_check_off_disabled:I = 0x7f020397

.field public static final s_health_favorites_check_off_disabledfocused:I = 0x7f020398

.field public static final s_health_favorites_check_off_focus:I = 0x7f020399

.field public static final s_health_favorites_check_off_normal:I = 0x7f02039a

.field public static final s_health_favorites_check_off_press:I = 0x7f02039b

.field public static final s_health_favorites_check_on_disabled:I = 0x7f02039c

.field public static final s_health_favorites_check_on_disabledfocused:I = 0x7f02039d

.field public static final s_health_favorites_check_on_focus:I = 0x7f02039e

.field public static final s_health_favorites_check_on_normal:I = 0x7f02039f

.field public static final s_health_favorites_check_on_press:I = 0x7f0203a0

.field public static final s_health_favorites_checkbox_press:I = 0x7f0203a1

.field public static final s_health_food_add_camera:I = 0x7f0203a2

.field public static final s_health_food_add_gallery:I = 0x7f0203a3

.field public static final s_health_food_barcode_frame01_nor:I = 0x7f0203a4

.field public static final s_health_food_barcode_frame02_nor:I = 0x7f0203a5

.field public static final s_health_food_barcode_frame03_nor:I = 0x7f0203a6

.field public static final s_health_food_barcode_frame04_nor:I = 0x7f0203a7

.field public static final s_health_food_barcode_scaping_bar:I = 0x7f0203a8

.field public static final s_health_food_divder:I = 0x7f0203a9

.field public static final s_health_food_listpopup_title:I = 0x7f0203aa

.field public static final s_health_food_nutrition_dot:I = 0x7f0203ab

.field public static final s_health_food_nutrition_info_bg:I = 0x7f0203ac

.field public static final s_health_food_page_01:I = 0x7f0203ad

.field public static final s_health_food_page_02:I = 0x7f0203ae

.field public static final s_health_food_popup_progress_info:I = 0x7f0203af

.field public static final s_health_food_tracker_log_list_01:I = 0x7f0203b0

.field public static final s_health_glucose_noitem:I = 0x7f0203b1

.field public static final s_health_graph_default_bg:I = 0x7f0203b2

.field public static final s_health_graph_default_bg03:I = 0x7f0203b3

.field public static final s_health_graph_default_bg04:I = 0x7f0203b4

.field public static final s_health_graph_dot_black_00:I = 0x7f0203b5

.field public static final s_health_graph_dot_black_01:I = 0x7f0203b6

.field public static final s_health_graph_dot_blue_00:I = 0x7f0203b7

.field public static final s_health_graph_dot_blue_01:I = 0x7f0203b8

.field public static final s_health_graph_dot_darkgreen_00:I = 0x7f0203b9

.field public static final s_health_graph_dot_darkgreen_01:I = 0x7f0203ba

.field public static final s_health_graph_dot_index_yellowgreen:I = 0x7f0203bb

.field public static final s_health_graph_dot_orange_00:I = 0x7f0203bc

.field public static final s_health_graph_dot_orange_01:I = 0x7f0203bd

.field public static final s_health_graph_dot_red_00:I = 0x7f0203be

.field public static final s_health_graph_dot_red_01:I = 0x7f0203bf

.field public static final s_health_graph_dot_yellowgreen_00:I = 0x7f0203c0

.field public static final s_health_graph_dot_yellowgreen_01:I = 0x7f0203c1

.field public static final s_health_graph_handler:I = 0x7f0203c2

.field public static final s_health_graph_handler01:I = 0x7f0203c3

.field public static final s_health_graph_handler_line:I = 0x7f0203c4

.field public static final s_health_graph_index01:I = 0x7f0203c5

.field public static final s_health_graph_index05:I = 0x7f0203c6

.field public static final s_health_graph_index_07:I = 0x7f0203c7

.field public static final s_health_graph_no_data:I = 0x7f0203c8

.field public static final s_health_graph_square_darkgreen_00:I = 0x7f0203c9

.field public static final s_health_graph_square_darkgreen_01:I = 0x7f0203ca

.field public static final s_health_graph_triangle_darkgreen_00:I = 0x7f0203cb

.field public static final s_health_graph_triangle_darkgreen_01:I = 0x7f0203cc

.field public static final s_health_graph_triangle_index_darkgreen:I = 0x7f0203cd

.field public static final s_health_graph_triangle_orange_00:I = 0x7f0203ce

.field public static final s_health_graph_triangle_orange_01:I = 0x7f0203cf

.field public static final s_health_green_page_turn_cigna_focus:I = 0x7f0203d0

.field public static final s_health_green_page_turn_cigna_normal:I = 0x7f0203d1

.field public static final s_health_green_page_turn_cigna_press:I = 0x7f0203d2

.field public static final s_health_green_page_turn_cigna_select:I = 0x7f0203d3

.field public static final s_health_green_page_turn_ecg_normal_transparent:I = 0x7f0203d4

.field public static final s_health_green_page_turn_food_normal:I = 0x7f0203d5

.field public static final s_health_green_page_turn_food_normal_transparent:I = 0x7f0203d6

.field public static final s_health_green_page_turn_lifestyle_focus:I = 0x7f0203d7

.field public static final s_health_green_page_turn_lifestyle_normal:I = 0x7f0203d8

.field public static final s_health_green_page_turn_lifestyle_press:I = 0x7f0203d9

.field public static final s_health_green_page_turn_lifestyle_select:I = 0x7f0203da

.field public static final s_health_green_page_turn_running_normal_transparent:I = 0x7f0203db

.field public static final s_health_green_page_turn_temperature_normal_transparent:I = 0x7f0203dc

.field public static final s_health_green_page_turn_walk_normal_transparent:I = 0x7f0203dd

.field public static final s_health_gymmode:I = 0x7f0203de

.field public static final s_health_h_ani_solar_system_01:I = 0x7f0203df

.field public static final s_health_h_ani_solar_system_02:I = 0x7f0203e0

.field public static final s_health_h_ani_solar_system_03:I = 0x7f0203e1

.field public static final s_health_h_ani_solar_system_04:I = 0x7f0203e2

.field public static final s_health_h_ani_solar_system_05:I = 0x7f0203e3

.field public static final s_health_h_bulletpoint:I = 0x7f0203e4

.field public static final s_health_h_common_icon_accessory:I = 0x7f0203e5

.field public static final s_health_h_exercise_detected:I = 0x7f0203e6

.field public static final s_health_h_exercise_drop_arrow_dim:I = 0x7f0203e7

.field public static final s_health_h_exercise_facebook_icon:I = 0x7f0203e8

.field public static final s_health_h_exercise_firstbeat_01:I = 0x7f0203e9

.field public static final s_health_h_exercise_firstbeat_03:I = 0x7f0203ea

.field public static final s_health_h_exercise_icon_xl_bike:I = 0x7f0203eb

.field public static final s_health_h_exercise_icon_xl_climber:I = 0x7f0203ec

.field public static final s_health_h_exercise_icon_xl_elliptical:I = 0x7f0203ed

.field public static final s_health_h_exercise_icon_xl_nordic_skier:I = 0x7f0203ee

.field public static final s_health_h_exercise_icon_xl_rower:I = 0x7f0203ef

.field public static final s_health_h_exercise_icon_xl_treadmill:I = 0x7f0203f0

.field public static final s_health_h_exercise_map_arrival:I = 0x7f0203f1

.field public static final s_health_h_exercise_map_btn_bg:I = 0x7f0203f2

.field public static final s_health_h_exercise_map_btn_disabled_bg:I = 0x7f0203f3

.field public static final s_health_h_exercise_map_btn_focused_bg:I = 0x7f0203f4

.field public static final s_health_h_exercise_map_btn_play_icon:I = 0x7f0203f5

.field public static final s_health_h_exercise_map_btn_point02_icon:I = 0x7f0203f6

.field public static final s_health_h_exercise_map_btn_point_icon:I = 0x7f0203f7

.field public static final s_health_h_exercise_map_btn_pressed_bg:I = 0x7f0203f8

.field public static final s_health_h_exercise_map_btn_selected_bg:I = 0x7f0203f9

.field public static final s_health_h_exercise_map_btn_view_icon:I = 0x7f0203fa

.field public static final s_health_h_exercise_map_building_bg:I = 0x7f0203fb

.field public static final s_health_h_exercise_map_start:I = 0x7f0203fc

.field public static final s_health_h_exercise_twitter_icon:I = 0x7f0203fd

.field public static final s_health_h_exercise_up:I = 0x7f0203fe

.field public static final s_health_h_exercise_working_triangle_icon:I = 0x7f0203ff

.field public static final s_health_h_list_icon_accessory:I = 0x7f020400

.field public static final s_health_h_list_icon_audio_on:I = 0x7f020401

.field public static final s_health_h_list_icon_burntcalories:I = 0x7f020402

.field public static final s_health_h_list_icon_distance:I = 0x7f020403

.field public static final s_health_h_list_icon_duration:I = 0x7f020404

.field public static final s_health_h_list_icon_goal_dim:I = 0x7f020405

.field public static final s_health_h_list_icon_running:I = 0x7f020406

.field public static final s_health_h_list_icon_setting:I = 0x7f020407

.field public static final s_health_h_list_icon_sync:I = 0x7f020408

.field public static final s_health_h_listview_memo:I = 0x7f020409

.field public static final s_health_h_popup_trophy_01:I = 0x7f02040a

.field public static final s_health_h_popup_trophy_02:I = 0x7f02040b

.field public static final s_health_h_popup_trophy_03:I = 0x7f02040c

.field public static final s_health_h_ranking_bg:I = 0x7f02040d

.field public static final s_health_h_walking_earth:I = 0x7f02040e

.field public static final s_health_h_walking_list_dot:I = 0x7f02040f

.field public static final s_health_h_walking_people:I = 0x7f020410

.field public static final s_health_h_walking_progressbar_me_bar_01:I = 0x7f020411

.field public static final s_health_h_walking_progressbar_me_bar_02:I = 0x7f020412

.field public static final s_health_h_weight_bar:I = 0x7f020413

.field public static final s_health_heartrate_list_icon02:I = 0x7f020414

.field public static final s_health_help_cigna_pageturn:I = 0x7f020415

.field public static final s_health_help_icon_delete:I = 0x7f020416

.field public static final s_health_help_icon_download:I = 0x7f020417

.field public static final s_health_help_icon_exercise:I = 0x7f020418

.field public static final s_health_help_icon_favorites_add:I = 0x7f020419

.field public static final s_health_help_icon_food:I = 0x7f02041a

.field public static final s_health_help_icon_heartrate:I = 0x7f02041b

.field public static final s_health_help_icon_hygrometer:I = 0x7f02041c

.field public static final s_health_help_icon_link:I = 0x7f02041d

.field public static final s_health_help_icon_menu:I = 0x7f02041e

.field public static final s_health_help_icon_moreapps:I = 0x7f02041f

.field public static final s_health_help_icon_pageturn:I = 0x7f020420

.field public static final s_health_help_icon_pedometer:I = 0x7f020421

.field public static final s_health_help_icon_plus:I = 0x7f020422

.field public static final s_health_help_icon_sleep:I = 0x7f020423

.field public static final s_health_help_icon_spo2:I = 0x7f020424

.field public static final s_health_help_icon_stress:I = 0x7f020425

.field public static final s_health_help_icon_uv:I = 0x7f020426

.field public static final s_health_help_icon_weight:I = 0x7f020427

.field public static final s_health_help_text_dot:I = 0x7f020428

.field public static final s_health_home_bt_check_icon_normal:I = 0x7f020429

.field public static final s_health_home_btn:I = 0x7f02042a

.field public static final s_health_home_calorie_icon:I = 0x7f02042b

.field public static final s_health_home_calorie_icon_02:I = 0x7f02042c

.field public static final s_health_home_check_off:I = 0x7f02042d

.field public static final s_health_home_check_on:I = 0x7f02042e

.field public static final s_health_home_drop_icon:I = 0x7f02042f

.field public static final s_health_home_favorites_add:I = 0x7f020430

.field public static final s_health_home_favorites_edit:I = 0x7f020431

.field public static final s_health_home_food_icon:I = 0x7f020432

.field public static final s_health_home_food_icon_02:I = 0x7f020433

.field public static final s_health_home_gear_icon:I = 0x7f020434

.field public static final s_health_home_gear_icon_02:I = 0x7f020435

.field public static final s_health_home_hr_progressbar_left:I = 0x7f020436

.field public static final s_health_home_hr_progressbar_right:I = 0x7f020437

.field public static final s_health_home_icon:I = 0x7f020438

.field public static final s_health_home_icon_add:I = 0x7f020439

.field public static final s_health_home_icon_coach:I = 0x7f02043a

.field public static final s_health_home_icon_exercise:I = 0x7f02043b

.field public static final s_health_home_icon_food:I = 0x7f02043c

.field public static final s_health_home_icon_hr:I = 0x7f02043d

.field public static final s_health_home_icon_hygrometer:I = 0x7f02043e

.field public static final s_health_home_icon_moreapps:I = 0x7f02043f

.field public static final s_health_home_icon_pedometer:I = 0x7f020440

.field public static final s_health_home_icon_shadow:I = 0x7f020441

.field public static final s_health_home_icon_shadow_02:I = 0x7f020442

.field public static final s_health_home_icon_sleep_2:I = 0x7f020443

.field public static final s_health_home_icon_spo2:I = 0x7f020444

.field public static final s_health_home_icon_stress:I = 0x7f020445

.field public static final s_health_home_icon_uv:I = 0x7f020446

.field public static final s_health_home_icon_wearable:I = 0x7f020447

.field public static final s_health_home_icon_weight:I = 0x7f020448

.field public static final s_health_home_list_gear:I = 0x7f020449

.field public static final s_health_home_list_icon_aerobic:I = 0x7f02044a

.field public static final s_health_home_list_icon_aquarobics:I = 0x7f02044b

.field public static final s_health_home_list_icon_archery:I = 0x7f02044c

.field public static final s_health_home_list_icon_badminton:I = 0x7f02044d

.field public static final s_health_home_list_icon_ballet:I = 0x7f02044e

.field public static final s_health_home_list_icon_balllroomdance:I = 0x7f02044f

.field public static final s_health_home_list_icon_barbellbenchpress:I = 0x7f020450

.field public static final s_health_home_list_icon_barbellbentoverrow:I = 0x7f020451

.field public static final s_health_home_list_icon_barbellcurl:I = 0x7f020452

.field public static final s_health_home_list_icon_barbellsquat:I = 0x7f020453

.field public static final s_health_home_list_icon_barbelluprightrow:I = 0x7f020454

.field public static final s_health_home_list_icon_baseball:I = 0x7f020455

.field public static final s_health_home_list_icon_basketball:I = 0x7f020456

.field public static final s_health_home_list_icon_bicyling:I = 0x7f020457

.field public static final s_health_home_list_icon_billiard:I = 0x7f020458

.field public static final s_health_home_list_icon_bodytemperature:I = 0x7f020459

.field public static final s_health_home_list_icon_bowling:I = 0x7f02045a

.field public static final s_health_home_list_icon_boxing:I = 0x7f02045b

.field public static final s_health_home_list_icon_canoeingrowing:I = 0x7f02045c

.field public static final s_health_home_list_icon_chinup:I = 0x7f02045d

.field public static final s_health_home_list_icon_crunch:I = 0x7f02045e

.field public static final s_health_home_list_icon_dance:I = 0x7f02045f

.field public static final s_health_home_list_icon_dumbbellbenchpress:I = 0x7f020460

.field public static final s_health_home_list_icon_dumbbellkickback:I = 0x7f020461

.field public static final s_health_home_list_icon_dumbbelllateralraise:I = 0x7f020462

.field public static final s_health_home_list_icon_dumbbelloverheadpress:I = 0x7f020463

.field public static final s_health_home_list_icon_fastwalking:I = 0x7f020464

.field public static final s_health_home_list_icon_fieldhockey:I = 0x7f020465

.field public static final s_health_home_list_icon_flatbenchdumbbellfly:I = 0x7f020466

.field public static final s_health_home_list_icon_golf:I = 0x7f020467

.field public static final s_health_home_list_icon_handball:I = 0x7f020468

.field public static final s_health_home_list_icon_hangliding:I = 0x7f020469

.field public static final s_health_home_list_icon_hiking:I = 0x7f02046a

.field public static final s_health_home_list_icon_horsebackriding:I = 0x7f02046b

.field public static final s_health_home_list_icon_hr:I = 0x7f02046c

.field public static final s_health_home_list_icon_hulahooping:I = 0x7f02046d

.field public static final s_health_home_list_icon_hygrometer:I = 0x7f02046e

.field public static final s_health_home_list_icon_icedancing:I = 0x7f02046f

.field public static final s_health_home_list_icon_icehockey:I = 0x7f020470

.field public static final s_health_home_list_icon_inclinebarbellpress:I = 0x7f020471

.field public static final s_health_home_list_icon_inclinedumbbellcurl:I = 0x7f020472

.field public static final s_health_home_list_icon_inlineskating:I = 0x7f020473

.field public static final s_health_home_list_icon_judo:I = 0x7f020474

.field public static final s_health_home_list_icon_jumpingrope:I = 0x7f020475

.field public static final s_health_home_list_icon_latpulldown:I = 0x7f020476

.field public static final s_health_home_list_icon_legextension:I = 0x7f020477

.field public static final s_health_home_list_icon_legpress:I = 0x7f020478

.field public static final s_health_home_list_icon_lyingbarbellextension:I = 0x7f020479

.field public static final s_health_home_list_icon_lyinglegcurl:I = 0x7f02047a

.field public static final s_health_home_list_icon_onearmdumbbellrow:I = 0x7f02047b

.field public static final s_health_home_list_icon_pilates:I = 0x7f02047c

.field public static final s_health_home_list_icon_pistolshooting:I = 0x7f02047d

.field public static final s_health_home_list_icon_powerwalking:I = 0x7f02047e

.field public static final s_health_home_list_icon_preachercurlmachine:I = 0x7f02047f

.field public static final s_health_home_list_icon_pushup:I = 0x7f020480

.field public static final s_health_home_list_icon_reversecrunch:I = 0x7f020481

.field public static final s_health_home_list_icon_rockclimbing:I = 0x7f020482

.field public static final s_health_home_list_icon_romaniandeadlift:I = 0x7f020483

.field public static final s_health_home_list_icon_ropepressdown:I = 0x7f020484

.field public static final s_health_home_list_icon_rugby:I = 0x7f020485

.field public static final s_health_home_list_icon_runninggeneral:I = 0x7f020486

.field public static final s_health_home_list_icon_sailingyachtingforleisure:I = 0x7f020487

.field public static final s_health_home_list_icon_seatedcalfraise:I = 0x7f020488

.field public static final s_health_home_list_icon_seatedlegcurl:I = 0x7f020489

.field public static final s_health_home_list_icon_situp:I = 0x7f02048a

.field public static final s_health_home_list_icon_skating:I = 0x7f02048b

.field public static final s_health_home_list_icon_skiing:I = 0x7f02048c

.field public static final s_health_home_list_icon_skindiving:I = 0x7f02048d

.field public static final s_health_home_list_icon_sleep:I = 0x7f02048e

.field public static final s_health_home_list_icon_slowwalking:I = 0x7f02048f

.field public static final s_health_home_list_icon_smithmachineuprightrow:I = 0x7f020490

.field public static final s_health_home_list_icon_snorkeling:I = 0x7f020491

.field public static final s_health_home_list_icon_snowboarding:I = 0x7f020492

.field public static final s_health_home_list_icon_soccer:I = 0x7f020493

.field public static final s_health_home_list_icon_softball:I = 0x7f020494

.field public static final s_health_home_list_icon_spo2:I = 0x7f020495

.field public static final s_health_home_list_icon_squash:I = 0x7f020496

.field public static final s_health_home_list_icon_standingcalfraise:I = 0x7f020497

.field public static final s_health_home_list_icon_stationarybicycle:I = 0x7f020498

.field public static final s_health_home_list_icon_stepmachine:I = 0x7f020499

.field public static final s_health_home_list_icon_stress:I = 0x7f02049a

.field public static final s_health_home_list_icon_swimming:I = 0x7f02049b

.field public static final s_health_home_list_icon_tabletennis:I = 0x7f02049c

.field public static final s_health_home_list_icon_taekwondo:I = 0x7f02049d

.field public static final s_health_home_list_icon_taichi:I = 0x7f02049e

.field public static final s_health_home_list_icon_tennis:I = 0x7f02049f

.field public static final s_health_home_list_icon_tretching:I = 0x7f0204a0

.field public static final s_health_home_list_icon_uv:I = 0x7f0204a1

.field public static final s_health_home_list_icon_volleyball:I = 0x7f0204a2

.field public static final s_health_home_list_icon_walking:I = 0x7f0204a3

.field public static final s_health_home_list_icon_walkingdownstairs:I = 0x7f0204a4

.field public static final s_health_home_list_icon_walkingupstairs:I = 0x7f0204a5

.field public static final s_health_home_list_icon_watersking:I = 0x7f0204a6

.field public static final s_health_home_list_icon_weightmachine:I = 0x7f0204a7

.field public static final s_health_home_list_icon_yoga:I = 0x7f0204a8

.field public static final s_health_home_main_bg:I = 0x7f0204a9

.field public static final s_health_home_main_bg_2:I = 0x7f0204aa

.field public static final s_health_home_main_bg_3:I = 0x7f0204ab

.field public static final s_health_home_mask:I = 0x7f0204ac

.field public static final s_health_home_pedometer_activity_icon:I = 0x7f0204ad

.field public static final s_health_home_pedometer_gear_icon:I = 0x7f0204ae

.field public static final s_health_home_pedometer_gearfit_icon:I = 0x7f0204af

.field public static final s_health_home_pedometer_icon:I = 0x7f0204b0

.field public static final s_health_home_pedometer_icon_02:I = 0x7f0204b1

.field public static final s_health_home_pedometer_icon_zero:I = 0x7f0204b2

.field public static final s_health_home_pedometer_inactive_icon:I = 0x7f0204b3

.field public static final s_health_home_pedometer_inactive_icon_02:I = 0x7f0204b4

.field public static final s_health_home_pedometer_pause:I = 0x7f0204b5

.field public static final s_health_home_pedometer_pause_02:I = 0x7f0204b6

.field public static final s_health_home_pedometer_step_icon:I = 0x7f0204b7

.field public static final s_health_home_pedometer_step_icon_02:I = 0x7f0204b8

.field public static final s_health_home_pro_medal_exercise_01:I = 0x7f0204b9

.field public static final s_health_home_pro_medal_food_01:I = 0x7f0204ba

.field public static final s_health_home_pro_medal_food_deprive:I = 0x7f0204bb

.field public static final s_health_home_pro_medal_gear_01:I = 0x7f0204bc

.field public static final s_health_home_pro_medal_pedometer_01:I = 0x7f0204bd

.field public static final s_health_home_progressbar_polygon:I = 0x7f0204be

.field public static final s_health_home_sound_wave_bg:I = 0x7f0204bf

.field public static final s_health_home_sound_wave_dot_effect:I = 0x7f0204c0

.field public static final s_health_home_sound_wave_mark_bg:I = 0x7f0204c1

.field public static final s_health_home_sublist_icon_calorie:I = 0x7f0204c2

.field public static final s_health_home_sublist_icon_distance:I = 0x7f0204c3

.field public static final s_health_home_sublist_icon_duration:I = 0x7f0204c4

.field public static final s_health_home_sublist_icon_elevation:I = 0x7f0204c5

.field public static final s_health_home_sublist_icon_hr:I = 0x7f0204c6

.field public static final s_health_hr_edit_list_icon_angry:I = 0x7f0204c7

.field public static final s_health_hr_edit_list_icon_atwork:I = 0x7f0204c8

.field public static final s_health_hr_edit_list_icon_baby_boy:I = 0x7f0204c9

.field public static final s_health_hr_edit_list_icon_baby_girl:I = 0x7f0204ca

.field public static final s_health_hr_edit_list_icon_bg:I = 0x7f0204cb

.field public static final s_health_hr_edit_list_icon_boy:I = 0x7f0204cc

.field public static final s_health_hr_edit_list_icon_cat:I = 0x7f0204cd

.field public static final s_health_hr_edit_list_icon_coffee:I = 0x7f0204ce

.field public static final s_health_hr_edit_list_icon_discuss:I = 0x7f0204cf

.field public static final s_health_hr_edit_list_icon_dog:I = 0x7f0204d0

.field public static final s_health_hr_edit_list_icon_dosage:I = 0x7f0204d1

.field public static final s_health_hr_edit_list_icon_drinking:I = 0x7f0204d2

.field public static final s_health_hr_edit_list_icon_elderly_m:I = 0x7f0204d3

.field public static final s_health_hr_edit_list_icon_elderly_w:I = 0x7f0204d4

.field public static final s_health_hr_edit_list_icon_exsecise:I = 0x7f0204d5

.field public static final s_health_hr_edit_list_icon_fearful:I = 0x7f0204d6

.field public static final s_health_hr_edit_list_icon_girl:I = 0x7f0204d7

.field public static final s_health_hr_edit_list_icon_happy:I = 0x7f0204d8

.field public static final s_health_hr_edit_list_icon_horse:I = 0x7f0204d9

.field public static final s_health_hr_edit_list_icon_inlove:I = 0x7f0204da

.field public static final s_health_hr_edit_list_icon_man:I = 0x7f0204db

.field public static final s_health_hr_edit_list_icon_meeting:I = 0x7f0204dc

.field public static final s_health_hr_edit_list_icon_middleage_m:I = 0x7f0204dd

.field public static final s_health_hr_edit_list_icon_middleage_w:I = 0x7f0204de

.field public static final s_health_hr_edit_list_icon_party:I = 0x7f0204df

.field public static final s_health_hr_edit_list_icon_plane:I = 0x7f0204e0

.field public static final s_health_hr_edit_list_icon_relaxing:I = 0x7f0204e1

.field public static final s_health_hr_edit_list_icon_running:I = 0x7f0204e2

.field public static final s_health_hr_edit_list_icon_sad:I = 0x7f0204e3

.field public static final s_health_hr_edit_list_icon_smoking:I = 0x7f0204e4

.field public static final s_health_hr_edit_list_icon_surprise:I = 0x7f0204e5

.field public static final s_health_hr_edit_list_icon_tag:I = 0x7f0204e6

.field public static final s_health_hr_edit_list_icon_walking:I = 0x7f0204e7

.field public static final s_health_hr_edit_list_icon_woman:I = 0x7f0204e8

.field public static final s_health_hr_edit_tag_icon_alcohol:I = 0x7f0204e9

.field public static final s_health_hr_edit_tag_icon_angry:I = 0x7f0204ea

.field public static final s_health_hr_edit_tag_icon_atwork:I = 0x7f0204eb

.field public static final s_health_hr_edit_tag_icon_car:I = 0x7f0204ec

.field public static final s_health_hr_edit_tag_icon_clover:I = 0x7f0204ed

.field public static final s_health_hr_edit_tag_icon_coffee:I = 0x7f0204ee

.field public static final s_health_hr_edit_tag_icon_cycling:I = 0x7f0204ef

.field public static final s_health_hr_edit_tag_icon_diamond:I = 0x7f0204f0

.field public static final s_health_hr_edit_tag_icon_exercise:I = 0x7f0204f1

.field public static final s_health_hr_edit_tag_icon_fright:I = 0x7f0204f2

.field public static final s_health_hr_edit_tag_icon_gym:I = 0x7f0204f3

.field public static final s_health_hr_edit_tag_icon_heart:I = 0x7f0204f4

.field public static final s_health_hr_edit_tag_icon_hiking:I = 0x7f0204f5

.field public static final s_health_hr_edit_tag_icon_juice:I = 0x7f0204f6

.field public static final s_health_hr_edit_tag_icon_meal:I = 0x7f0204f7

.field public static final s_health_hr_edit_tag_icon_meeting:I = 0x7f0204f8

.field public static final s_health_hr_edit_tag_icon_park:I = 0x7f0204f9

.field public static final s_health_hr_edit_tag_icon_plane:I = 0x7f0204fa

.field public static final s_health_hr_edit_tag_icon_pleasure:I = 0x7f0204fb

.field public static final s_health_hr_edit_tag_icon_present:I = 0x7f0204fc

.field public static final s_health_hr_edit_tag_icon_relaxing:I = 0x7f0204fd

.field public static final s_health_hr_edit_tag_icon_sad:I = 0x7f0204fe

.field public static final s_health_hr_edit_tag_icon_sick:I = 0x7f0204ff

.field public static final s_health_hr_edit_tag_icon_spade:I = 0x7f020500

.field public static final s_health_hr_edit_tag_icon_star:I = 0x7f020501

.field public static final s_health_hr_edit_tag_icon_train:I = 0x7f020502

.field public static final s_health_hr_edit_tag_icon_wakeup:I = 0x7f020503

.field public static final s_health_hr_list_icon_alcohol:I = 0x7f020504

.field public static final s_health_hr_list_icon_angry:I = 0x7f020505

.field public static final s_health_hr_list_icon_bg:I = 0x7f020506

.field public static final s_health_hr_list_icon_car:I = 0x7f020507

.field public static final s_health_hr_list_icon_clover:I = 0x7f020508

.field public static final s_health_hr_list_icon_coffee:I = 0x7f020509

.field public static final s_health_hr_list_icon_diamond:I = 0x7f02050a

.field public static final s_health_hr_list_icon_fright:I = 0x7f02050b

.field public static final s_health_hr_list_icon_gym:I = 0x7f02050c

.field public static final s_health_hr_list_icon_heart:I = 0x7f02050d

.field public static final s_health_hr_list_icon_juice:I = 0x7f02050e

.field public static final s_health_hr_list_icon_meal:I = 0x7f02050f

.field public static final s_health_hr_list_icon_meeting:I = 0x7f020510

.field public static final s_health_hr_list_icon_park:I = 0x7f020511

.field public static final s_health_hr_list_icon_plane:I = 0x7f020512

.field public static final s_health_hr_list_icon_pleasure:I = 0x7f020513

.field public static final s_health_hr_list_icon_present:I = 0x7f020514

.field public static final s_health_hr_list_icon_relaxing:I = 0x7f020515

.field public static final s_health_hr_list_icon_sad:I = 0x7f020516

.field public static final s_health_hr_list_icon_sick:I = 0x7f020517

.field public static final s_health_hr_list_icon_spade:I = 0x7f020518

.field public static final s_health_hr_list_icon_star:I = 0x7f020519

.field public static final s_health_hr_list_icon_train:I = 0x7f02051a

.field public static final s_health_hr_list_icon_wakeup:I = 0x7f02051b

.field public static final s_health_hr_loglist_icon_bg:I = 0x7f02051c

.field public static final s_health_hr_loglist_icon_ummary_icon_tag:I = 0x7f02051d

.field public static final s_health_hr_measuring_bg:I = 0x7f02051e

.field public static final s_health_hr_measuring_bg_fail:I = 0x7f02051f

.field public static final s_health_hr_measuring_circle_bg_frame:I = 0x7f020520

.field public static final s_health_hr_measuring_circle_bg_frame_fail:I = 0x7f020521

.field public static final s_health_hr_measuring_graph_bg:I = 0x7f020522

.field public static final s_health_hr_measuring_graph_bg_heartmask_300:I = 0x7f020523

.field public static final s_health_hr_measuring_graph_bg_orange:I = 0x7f020524

.field public static final s_health_hr_measuring_graph_bg_orange_heartmask_300:I = 0x7f020525

.field public static final s_health_hr_measuring_heart_logo:I = 0x7f020526

.field public static final s_health_hr_measuring_heart_logo_00:I = 0x7f020527

.field public static final s_health_hr_measuring_heart_logo_01:I = 0x7f020528

.field public static final s_health_hr_measuring_heart_logo_orange:I = 0x7f020529

.field public static final s_health_hr_measuring_heart_logo_orange_01:I = 0x7f02052a

.field public static final s_health_hr_nodata_heart_logo:I = 0x7f02052b

.field public static final s_health_hr_progressbar_bg:I = 0x7f02052c

.field public static final s_health_hr_progressbar_left:I = 0x7f02052d

.field public static final s_health_hr_progressbar_mask:I = 0x7f02052e

.field public static final s_health_hr_progressbar_right:I = 0x7f02052f

.field public static final s_health_hr_summary_icon_angry:I = 0x7f020530

.field public static final s_health_hr_summary_icon_coffee:I = 0x7f020531

.field public static final s_health_hr_summary_icon_drinking:I = 0x7f020532

.field public static final s_health_hr_summary_icon_fearful:I = 0x7f020533

.field public static final s_health_hr_summary_icon_focus:I = 0x7f020534

.field public static final s_health_hr_summary_icon_happy:I = 0x7f020535

.field public static final s_health_hr_summary_icon_inlove:I = 0x7f020536

.field public static final s_health_hr_summary_icon_party:I = 0x7f020537

.field public static final s_health_hr_summary_icon_relaxing:I = 0x7f020538

.field public static final s_health_hr_summary_icon_running:I = 0x7f020539

.field public static final s_health_hr_summary_icon_sad:I = 0x7f02053a

.field public static final s_health_hr_summary_icon_smoking:I = 0x7f02053b

.field public static final s_health_hr_summary_icon_surprise:I = 0x7f02053c

.field public static final s_health_hr_summary_icon_tag:I = 0x7f02053d

.field public static final s_health_hr_summary_icon_tag_nor:I = 0x7f02053e

.field public static final s_health_hr_summary_icon_tag_press:I = 0x7f02053f

.field public static final s_health_hr_summary_icon_tag_selected:I = 0x7f020540

.field public static final s_health_hr_summary_icon_walking:I = 0x7f020541

.field public static final s_health_hr_tag_icon:I = 0x7f020542

.field public static final s_health_hr_tag_icon_bg:I = 0x7f020543

.field public static final s_health_hr_tag_list_icon_alcohol:I = 0x7f020544

.field public static final s_health_hr_tag_list_icon_angry:I = 0x7f020545

.field public static final s_health_hr_tag_list_icon_atwork:I = 0x7f020546

.field public static final s_health_hr_tag_list_icon_car:I = 0x7f020547

.field public static final s_health_hr_tag_list_icon_clover:I = 0x7f020548

.field public static final s_health_hr_tag_list_icon_coffee:I = 0x7f020549

.field public static final s_health_hr_tag_list_icon_diamond:I = 0x7f02054a

.field public static final s_health_hr_tag_list_icon_fright:I = 0x7f02054b

.field public static final s_health_hr_tag_list_icon_heart:I = 0x7f02054c

.field public static final s_health_hr_tag_list_icon_juice:I = 0x7f02054d

.field public static final s_health_hr_tag_list_icon_meal:I = 0x7f02054e

.field public static final s_health_hr_tag_list_icon_meeting:I = 0x7f02054f

.field public static final s_health_hr_tag_list_icon_park:I = 0x7f020550

.field public static final s_health_hr_tag_list_icon_plane:I = 0x7f020551

.field public static final s_health_hr_tag_list_icon_pleasure:I = 0x7f020552

.field public static final s_health_hr_tag_list_icon_present:I = 0x7f020553

.field public static final s_health_hr_tag_list_icon_relaxing:I = 0x7f020554

.field public static final s_health_hr_tag_list_icon_sad:I = 0x7f020555

.field public static final s_health_hr_tag_list_icon_sick:I = 0x7f020556

.field public static final s_health_hr_tag_list_icon_spade:I = 0x7f020557

.field public static final s_health_hr_tag_list_icon_star:I = 0x7f020558

.field public static final s_health_hr_tag_list_icon_train:I = 0x7f020559

.field public static final s_health_hr_tag_list_icon_wakeup:I = 0x7f02055a

.field public static final s_health_icon_add_nor:I = 0x7f02055b

.field public static final s_health_icon_foodview_add:I = 0x7f02055c

.field public static final s_health_icon_foodview_delete:I = 0x7f02055d

.field public static final s_health_icon_medal_weight:I = 0x7f02055e

.field public static final s_health_icon_notification:I = 0x7f02055f

.field public static final s_health_indicator_icon:I = 0x7f020560

.field public static final s_health_indicator_medal_icon:I = 0x7f020561

.field public static final s_health_input_divider:I = 0x7f020562

.field public static final s_health_input_exercise_icon_back_dim:I = 0x7f020563

.field public static final s_health_input_exercise_icon_back_dimfocus:I = 0x7f020564

.field public static final s_health_input_exercise_icon_back_focus:I = 0x7f020565

.field public static final s_health_input_exercise_icon_back_nor:I = 0x7f020566

.field public static final s_health_input_exercise_icon_back_press:I = 0x7f020567

.field public static final s_health_input_exercise_icon_down_dim:I = 0x7f020568

.field public static final s_health_input_exercise_icon_down_dimfocus:I = 0x7f020569

.field public static final s_health_input_exercise_icon_down_focus:I = 0x7f02056a

.field public static final s_health_input_exercise_icon_down_nor:I = 0x7f02056b

.field public static final s_health_input_exercise_icon_down_press:I = 0x7f02056c

.field public static final s_health_input_exercise_icon_next_dim:I = 0x7f02056d

.field public static final s_health_input_exercise_icon_next_dimfocus:I = 0x7f02056e

.field public static final s_health_input_exercise_icon_next_focus:I = 0x7f02056f

.field public static final s_health_input_exercise_icon_next_nor:I = 0x7f020570

.field public static final s_health_input_exercise_icon_next_press:I = 0x7f020571

.field public static final s_health_input_exercise_icon_up_dim:I = 0x7f020572

.field public static final s_health_input_exercise_icon_up_dimpress:I = 0x7f020573

.field public static final s_health_input_exercise_icon_up_focus:I = 0x7f020574

.field public static final s_health_input_exercise_icon_up_nor:I = 0x7f020575

.field public static final s_health_input_exercise_icon_up_press:I = 0x7f020576

.field public static final s_health_intro_btn_bg:I = 0x7f020577

.field public static final s_health_list_icon_ad:I = 0x7f020578

.field public static final s_health_list_icon_aescent:I = 0x7f020579

.field public static final s_health_list_icon_alarm:I = 0x7f02057a

.field public static final s_health_list_icon_alarm01:I = 0x7f02057b

.field public static final s_health_list_icon_average_cadence:I = 0x7f02057c

.field public static final s_health_list_icon_averageheartrate:I = 0x7f02057d

.field public static final s_health_list_icon_averagepace:I = 0x7f02057e

.field public static final s_health_list_icon_averagespeed:I = 0x7f02057f

.field public static final s_health_list_icon_cadence:I = 0x7f020580

.field public static final s_health_list_icon_calorie:I = 0x7f020581

.field public static final s_health_list_icon_date:I = 0x7f020582

.field public static final s_health_list_icon_descent:I = 0x7f020583

.field public static final s_health_list_icon_distance:I = 0x7f020584

.field public static final s_health_list_icon_duration:I = 0x7f020585

.field public static final s_health_list_icon_elevation:I = 0x7f020586

.field public static final s_health_list_icon_elevation_high:I = 0x7f020587

.field public static final s_health_list_icon_elevation_low:I = 0x7f020588

.field public static final s_health_list_icon_exercise:I = 0x7f020589

.field public static final s_health_list_icon_food:I = 0x7f02058a

.field public static final s_health_list_icon_healthy_step:I = 0x7f02058b

.field public static final s_health_list_icon_hr:I = 0x7f02058c

.field public static final s_health_list_icon_inactive_time:I = 0x7f02058d

.field public static final s_health_list_icon_max_cadence:I = 0x7f02058e

.field public static final s_health_list_icon_maximumheartrate:I = 0x7f02058f

.field public static final s_health_list_icon_maximumpace:I = 0x7f020590

.field public static final s_health_list_icon_maximumspeed:I = 0x7f020591

.field public static final s_health_list_icon_motionless:I = 0x7f020592

.field public static final s_health_list_icon_pedometer:I = 0x7f020593

.field public static final s_health_list_icon_rate:I = 0x7f020594

.field public static final s_health_list_icon_running_step:I = 0x7f020595

.field public static final s_health_list_icon_sleep:I = 0x7f020596

.field public static final s_health_list_icon_sleeptime:I = 0x7f020597

.field public static final s_health_list_icon_sp02:I = 0x7f020598

.field public static final s_health_list_icon_steps:I = 0x7f020599

.field public static final s_health_list_icon_tag:I = 0x7f02059a

.field public static final s_health_list_icon_time:I = 0x7f02059b

.field public static final s_health_list_icon_training:I = 0x7f02059c

.field public static final s_health_list_icon_uv:I = 0x7f02059d

.field public static final s_health_list_icon_uv_limit:I = 0x7f02059e

.field public static final s_health_list_icon_uv_spf:I = 0x7f02059f

.field public static final s_health_list_icon_weight:I = 0x7f0205a0

.field public static final s_health_list_section_divider_holo_light:I = 0x7f0205a1

.field public static final s_health_listview_accessory:I = 0x7f0205a2

.field public static final s_health_listview_crown:I = 0x7f0205a3

.field public static final s_health_listview_memo:I = 0x7f0205a4

.field public static final s_health_listview_pedometer:I = 0x7f0205a5

.field public static final s_health_listview_wearable:I = 0x7f0205a6

.field public static final s_health_listview_xid:I = 0x7f0205a7

.field public static final s_health_medication_setgoal:I = 0x7f0205a8

.field public static final s_health_medication_setgoal_pin_green:I = 0x7f0205a9

.field public static final s_health_medication_setgoal_pin_orange:I = 0x7f0205aa

.field public static final s_health_miniicon_01:I = 0x7f0205ab

.field public static final s_health_miniicon_02:I = 0x7f0205ac

.field public static final s_health_miniicon_03:I = 0x7f0205ad

.field public static final s_health_miniicon_04:I = 0x7f0205ae

.field public static final s_health_miniicon_05:I = 0x7f0205af

.field public static final s_health_moreapps_nodata:I = 0x7f0205b0

.field public static final s_health_mypage_icon_lock:I = 0x7f0205b1

.field public static final s_health_mypage_list_calorie:I = 0x7f0205b2

.field public static final s_health_mypage_list_food:I = 0x7f0205b3

.field public static final s_health_mypage_list_pedometer:I = 0x7f0205b4

.field public static final s_health_mypage_upload:I = 0x7f0205b5

.field public static final s_health_mypage_user_mask:I = 0x7f0205b6

.field public static final s_health_noitem:I = 0x7f0205b7

.field public static final s_health_page_turn_summary_p:I = 0x7f0205b8

.field public static final s_health_page_turn_summary_p123:I = 0x7f0205b9

.field public static final s_health_pedometer_chart_crown:I = 0x7f0205ba

.field public static final s_health_pedometer_chart_medal:I = 0x7f0205bb

.field public static final s_health_pedometer_chart_medal_accessory:I = 0x7f0205bc

.field public static final s_health_pedometer_chart_medal_crown:I = 0x7f0205bd

.field public static final s_health_pedometer_chart_medal_crown_accessory:I = 0x7f0205be

.field public static final s_health_pedometer_info_healthy_step:I = 0x7f0205bf

.field public static final s_health_pedometer_info_inactive:I = 0x7f0205c0

.field public static final s_health_pedometer_popup_default:I = 0x7f0205c1

.field public static final s_health_pedometer_rank_list_icon_01:I = 0x7f0205c2

.field public static final s_health_pedometer_reward:I = 0x7f0205c3

.field public static final s_health_pedometer_sub_action_bar_bg_default:I = 0x7f0205c4

.field public static final s_health_pedometer_sub_action_bar_bg_disabled:I = 0x7f0205c5

.field public static final s_health_pedometer_sub_action_bar_bg_focused:I = 0x7f0205c6

.field public static final s_health_pedometer_sub_action_bar_bg_pressed:I = 0x7f0205c7

.field public static final s_health_pedometer_sub_action_bar_bg_rtl_default:I = 0x7f0205c8

.field public static final s_health_pedometer_sub_action_bar_bg_rtl_disabled:I = 0x7f0205c9

.field public static final s_health_pedometer_sub_action_bar_bg_rtl_focused:I = 0x7f0205ca

.field public static final s_health_pedometer_sub_action_bar_bg_rtl_pressed:I = 0x7f0205cb

.field public static final s_health_photo_page_01:I = 0x7f0205cc

.field public static final s_health_photo_page_02:I = 0x7f0205cd

.field public static final s_health_plain_widget_thumnail_4x1_cigna:I = 0x7f0205ce

.field public static final s_health_pressure_noitem:I = 0x7f0205cf

.field public static final s_health_pro_bubble_dim:I = 0x7f0205d0

.field public static final s_health_pro_bubble_focus:I = 0x7f0205d1

.field public static final s_health_pro_bubble_nor:I = 0x7f0205d2

.field public static final s_health_pro_bubble_picker:I = 0x7f0205d3

.field public static final s_health_pro_bubble_picker_dim:I = 0x7f0205d4

.field public static final s_health_pro_bubble_picker_focus:I = 0x7f0205d5

.field public static final s_health_pro_bubble_picker_press:I = 0x7f0205d6

.field public static final s_health_pro_bubble_picker_select:I = 0x7f0205d7

.field public static final s_health_pro_bubble_press:I = 0x7f0205d8

.field public static final s_health_pro_bubble_select:I = 0x7f0205d9

.field public static final s_health_profilesetting_activity_lev1:I = 0x7f0205da

.field public static final s_health_profilesetting_female_body:I = 0x7f0205db

.field public static final s_health_profilesetting_female_l_icon_dim:I = 0x7f0205dc

.field public static final s_health_profilesetting_female_l_icon_nor01:I = 0x7f0205dd

.field public static final s_health_profilesetting_female_s_icon01:I = 0x7f0205de

.field public static final s_health_profilesetting_img_lock:I = 0x7f0205df

.field public static final s_health_profilesetting_lev_1_nor:I = 0x7f0205e0

.field public static final s_health_profilesetting_lev_1_sel:I = 0x7f0205e1

.field public static final s_health_profilesetting_lev_2_nor:I = 0x7f0205e2

.field public static final s_health_profilesetting_lev_2_sel:I = 0x7f0205e3

.field public static final s_health_profilesetting_lev_3_nor:I = 0x7f0205e4

.field public static final s_health_profilesetting_lev_3_sel:I = 0x7f0205e5

.field public static final s_health_profilesetting_lev_4_nor:I = 0x7f0205e6

.field public static final s_health_profilesetting_lev_4_sel:I = 0x7f0205e7

.field public static final s_health_profilesetting_lev_5_nor:I = 0x7f0205e8

.field public static final s_health_profilesetting_lev_5_sel:I = 0x7f0205e9

.field public static final s_health_profilesetting_lev_text_bg:I = 0x7f0205ea

.field public static final s_health_profilesetting_male_body:I = 0x7f0205eb

.field public static final s_health_profilesetting_male_l_icon_dim:I = 0x7f0205ec

.field public static final s_health_profilesetting_male_l_icon_nor:I = 0x7f0205ed

.field public static final s_health_profilesetting_male_l_icon_nor01:I = 0x7f0205ee

.field public static final s_health_profilesetting_male_s_icon01:I = 0x7f0205ef

.field public static final s_health_profilesetting_profile_image:I = 0x7f0205f0

.field public static final s_health_profilesetting_top_check_icon:I = 0x7f0205f1

.field public static final s_health_profilesetting_top_number_nor_1:I = 0x7f0205f2

.field public static final s_health_profilesetting_top_number_nor_2:I = 0x7f0205f3

.field public static final s_health_profilesetting_top_number_nor_3:I = 0x7f0205f4

.field public static final s_health_profilesetting_top_number_nor_4:I = 0x7f0205f5

.field public static final s_health_profilesetting_top_number_nor_5:I = 0x7f0205f6

.field public static final s_health_profilesetting_top_number_nor_6:I = 0x7f0205f7

.field public static final s_health_profilesetting_top_number_nor_7:I = 0x7f0205f8

.field public static final s_health_profilesetting_top_number_sel_1:I = 0x7f0205f9

.field public static final s_health_profilesetting_top_number_sel_2:I = 0x7f0205fa

.field public static final s_health_profilesetting_top_number_sel_3:I = 0x7f0205fb

.field public static final s_health_profilesetting_top_number_sel_4:I = 0x7f0205fc

.field public static final s_health_profilesetting_top_number_sel_5:I = 0x7f0205fd

.field public static final s_health_profilesetting_top_number_sel_6:I = 0x7f0205fe

.field public static final s_health_profilesetting_top_number_sel_7:I = 0x7f0205ff

.field public static final s_health_profilesetting_top_picture_nor:I = 0x7f020600

.field public static final s_health_profilesettings_spinner_default_holo_light:I = 0x7f020601

.field public static final s_health_profilesettings_spinner_focused_holo_light:I = 0x7f020602

.field public static final s_health_profilesettings_spinner_pressed_holo_light:I = 0x7f020603

.field public static final s_health_quickpanel_icon_01:I = 0x7f020604

.field public static final s_health_quickpanel_icon_02:I = 0x7f020605

.field public static final s_health_quickpanel_icon_03:I = 0x7f020606

.field public static final s_health_quickpanel_icon_04:I = 0x7f020607

.field public static final s_health_quickpanel_icon_app:I = 0x7f020608

.field public static final s_health_quickpanel_icon_coach:I = 0x7f020609

.field public static final s_health_quickpanel_icon_cycling:I = 0x7f02060a

.field public static final s_health_quickpanel_icon_exercise:I = 0x7f02060b

.field public static final s_health_quickpanel_icon_hiking:I = 0x7f02060c

.field public static final s_health_quickpanel_icon_inactive:I = 0x7f02060d

.field public static final s_health_quickpanel_icon_m_pedometer:I = 0x7f02060e

.field public static final s_health_quickpanel_icon_pedometer:I = 0x7f02060f

.field public static final s_health_quickpanel_icon_walking:I = 0x7f020610

.field public static final s_health_quickpanel_sub_icon:I = 0x7f020611

.field public static final s_health_quickpanel_sub_icon_gear:I = 0x7f020612

.field public static final s_health_quickpanel_sub_icon_gearfit:I = 0x7f020613

.field public static final s_health_rank_bg:I = 0x7f020614

.field public static final s_health_rank_bg_select:I = 0x7f020615

.field public static final s_health_rank_mask:I = 0x7f020616

.field public static final s_health_sband_icon_sync:I = 0x7f020617

.field public static final s_health_scanning_animation:I = 0x7f020618

.field public static final s_health_setting_account_image:I = 0x7f020619

.field public static final s_health_setting_profile_image:I = 0x7f02061a

.field public static final s_health_setting_profile_image_default:I = 0x7f02061b

.field public static final s_health_setting_sign_out_icon:I = 0x7f02061c

.field public static final s_health_setup_btn_arrow:I = 0x7f02061d

.field public static final s_health_setup_btn_arrow_disabled:I = 0x7f02061e

.field public static final s_health_setup_btn_arrow_pressed:I = 0x7f02061f

.field public static final s_health_setup_icon:I = 0x7f020620

.field public static final s_health_sleep_icon:I = 0x7f020621

.field public static final s_health_sleep_listpopup_title:I = 0x7f020622

.field public static final s_health_sleep_star_off:I = 0x7f020623

.field public static final s_health_sleep_star_on:I = 0x7f020624

.field public static final s_health_splash:I = 0x7f020625

.field public static final s_health_spo2_fitness_level:I = 0x7f020626

.field public static final s_health_stm_graph_triangle_yellowgreen_00:I = 0x7f020627

.field public static final s_health_stm_graph_triangle_yellowgreen_01:I = 0x7f020628

.field public static final s_health_stm_list_icon:I = 0x7f020629

.field public static final s_health_stm_list_icon_completed:I = 0x7f02062a

.field public static final s_health_stm_measuring_circle_bg_frame:I = 0x7f02062b

.field public static final s_health_stm_measuring_circle_bg_frame_orange:I = 0x7f02062c

.field public static final s_health_stm_measuring_logo_01_1:I = 0x7f02062d

.field public static final s_health_stm_measuring_logo_01_2:I = 0x7f02062e

.field public static final s_health_stm_measuring_logo_01_3:I = 0x7f02062f

.field public static final s_health_stm_measuring_logo_orange_1:I = 0x7f020630

.field public static final s_health_stm_measuring_logo_orange_2:I = 0x7f020631

.field public static final s_health_stm_measuring_logo_orange_3:I = 0x7f020632

.field public static final s_health_stm_ready_logo_00:I = 0x7f020633

.field public static final s_health_stm_ready_logo_orange:I = 0x7f020634

.field public static final s_health_stm_summary_progressbar_polygon_02:I = 0x7f020635

.field public static final s_health_stm_summary_progressbar_polygon_04:I = 0x7f020636

.field public static final s_health_stm_summary_progressbar_polygon_05:I = 0x7f020637

.field public static final s_health_sub_btn_cancel_dim:I = 0x7f020638

.field public static final s_health_sub_btn_cancel_focus:I = 0x7f020639

.field public static final s_health_sub_btn_cancel_nor:I = 0x7f02063a

.field public static final s_health_sub_btn_cancel_press:I = 0x7f02063b

.field public static final s_health_sub_btn_cancel_select:I = 0x7f02063c

.field public static final s_health_sub_btn_nor:I = 0x7f02063d

.field public static final s_health_summaray_food_bottom_bg_disabled:I = 0x7f02063e

.field public static final s_health_summaray_food_bottom_bg_focus:I = 0x7f02063f

.field public static final s_health_summaray_food_bottom_bg_nor:I = 0x7f020640

.field public static final s_health_summaray_food_bottom_bg_press:I = 0x7f020641

.field public static final s_health_summaray_food_bottom_bg_select:I = 0x7f020642

.field public static final s_health_summaray_food_bottom_input:I = 0x7f020643

.field public static final s_health_summaray_food_bottom_star:I = 0x7f020644

.field public static final s_health_summaray_food_list_icon_01:I = 0x7f020645

.field public static final s_health_summaray_food_list_icon_01_press:I = 0x7f020646

.field public static final s_health_summaray_food_list_icon_mask01:I = 0x7f020647

.field public static final s_health_summaray_food_list_icon_mask02:I = 0x7f020648

.field public static final s_health_summaray_food_list_icon_mask03:I = 0x7f020649

.field public static final s_health_summaray_food_list_icon_mask04:I = 0x7f02064a

.field public static final s_health_summaray_food_list_plus_btn:I = 0x7f02064b

.field public static final s_health_summaray_food_popup_icon_01:I = 0x7f02064c

.field public static final s_health_summaray_food_popup_icon_01_select:I = 0x7f02064d

.field public static final s_health_summaray_food_popup_icon_02:I = 0x7f02064e

.field public static final s_health_summaray_food_popup_icon_02_select:I = 0x7f02064f

.field public static final s_health_summaray_food_popup_icon_03:I = 0x7f020650

.field public static final s_health_summaray_food_popup_icon_03_select:I = 0x7f020651

.field public static final s_health_summaray_food_popup_icon_04:I = 0x7f020652

.field public static final s_health_summaray_food_popup_icon_04_select:I = 0x7f020653

.field public static final s_health_summary_accessary:I = 0x7f020654

.field public static final s_health_summary_arrow_next_default:I = 0x7f020655

.field public static final s_health_summary_arrow_next_dim:I = 0x7f020656

.field public static final s_health_summary_arrow_next_disablefocused:I = 0x7f020657

.field public static final s_health_summary_arrow_next_focused:I = 0x7f020658

.field public static final s_health_summary_arrow_next_pressed:I = 0x7f020659

.field public static final s_health_summary_arrow_prev_default:I = 0x7f02065a

.field public static final s_health_summary_arrow_prev_dim:I = 0x7f02065b

.field public static final s_health_summary_arrow_prev_disablefocused:I = 0x7f02065c

.field public static final s_health_summary_arrow_prev_focused:I = 0x7f02065d

.field public static final s_health_summary_arrow_prev_pressed:I = 0x7f02065e

.field public static final s_health_summary_button01_dim:I = 0x7f02065f

.field public static final s_health_summary_button01_focus:I = 0x7f020660

.field public static final s_health_summary_button01_normal:I = 0x7f020661

.field public static final s_health_summary_button01_press:I = 0x7f020662

.field public static final s_health_summary_button01_pressselect:I = 0x7f020663

.field public static final s_health_summary_button_dim:I = 0x7f020664

.field public static final s_health_summary_button_disabled:I = 0x7f020665

.field public static final s_health_summary_button_discard:I = 0x7f020666

.field public static final s_health_summary_button_focus:I = 0x7f020667

.field public static final s_health_summary_button_lock:I = 0x7f020668

.field public static final s_health_summary_button_normal:I = 0x7f020669

.field public static final s_health_summary_button_pause:I = 0x7f02066a

.field public static final s_health_summary_button_pause_dim:I = 0x7f02066b

.field public static final s_health_summary_button_plus:I = 0x7f02066c

.field public static final s_health_summary_button_plus_dim:I = 0x7f02066d

.field public static final s_health_summary_button_press:I = 0x7f02066e

.field public static final s_health_summary_button_pressselect:I = 0x7f02066f

.field public static final s_health_summary_button_rec:I = 0x7f020670

.field public static final s_health_summary_button_rec_dim:I = 0x7f020671

.field public static final s_health_summary_button_refresh:I = 0x7f020672

.field public static final s_health_summary_button_select:I = 0x7f020673

.field public static final s_health_summary_button_stop:I = 0x7f020674

.field public static final s_health_summary_button_sync:I = 0x7f020675

.field public static final s_health_summary_button_trophy:I = 0x7f020676

.field public static final s_health_summary_cigna_icon_green:I = 0x7f020677

.field public static final s_health_summary_cigna_icon_orange:I = 0x7f020678

.field public static final s_health_summary_cigna_icon_red:I = 0x7f020679

.field public static final s_health_summary_cigna_stress_icon_green:I = 0x7f02067a

.field public static final s_health_summary_cigna_stress_icon_orange:I = 0x7f02067b

.field public static final s_health_summary_cigna_stress_icon_red:I = 0x7f02067c

.field public static final s_health_summary_combined_icon:I = 0x7f02067d

.field public static final s_health_summary_exercise_icon:I = 0x7f02067e

.field public static final s_health_summary_exercise_icon_cadence:I = 0x7f02067f

.field public static final s_health_summary_exercise_icon_green:I = 0x7f020680

.field public static final s_health_summary_exercise_icon_medal:I = 0x7f020681

.field public static final s_health_summary_exercise_icon_orange:I = 0x7f020682

.field public static final s_health_summary_exercise_icon_red:I = 0x7f020683

.field public static final s_health_summary_exercise_pro_check01_off:I = 0x7f020684

.field public static final s_health_summary_exercise_pro_check01_on:I = 0x7f020685

.field public static final s_health_summary_exercise_pro_drop_arrow:I = 0x7f020686

.field public static final s_health_summary_exercise_pro_drop_arrow01_s:I = 0x7f020687

.field public static final s_health_summary_exercise_pro_icon:I = 0x7f020688

.field public static final s_health_summary_food_icon:I = 0x7f020689

.field public static final s_health_summary_food_icon_green:I = 0x7f02068a

.field public static final s_health_summary_food_icon_medal:I = 0x7f02068b

.field public static final s_health_summary_food_icon_medal_deprive:I = 0x7f02068c

.field public static final s_health_summary_food_icon_orange:I = 0x7f02068d

.field public static final s_health_summary_food_icon_red:I = 0x7f02068e

.field public static final s_health_summary_food_list_btn_line:I = 0x7f02068f

.field public static final s_health_summary_food_list_icon_mask01:I = 0x7f020690

.field public static final s_health_summary_food_list_icon_mask02:I = 0x7f020691

.field public static final s_health_summary_food_list_icon_mask03:I = 0x7f020692

.field public static final s_health_summary_food_list_icon_mask04:I = 0x7f020693

.field public static final s_health_summary_food_medal:I = 0x7f020694

.field public static final s_health_summary_gear_icon:I = 0x7f020695

.field public static final s_health_summary_gear_medal:I = 0x7f020696

.field public static final s_health_summary_help_dim:I = 0x7f020697

.field public static final s_health_summary_help_focus:I = 0x7f020698

.field public static final s_health_summary_help_normal:I = 0x7f020699

.field public static final s_health_summary_help_press:I = 0x7f02069a

.field public static final s_health_summary_help_pressselect:I = 0x7f02069b

.field public static final s_health_summary_pageturn:I = 0x7f02069c

.field public static final s_health_summary_pageturn_exercise_focus:I = 0x7f02069d

.field public static final s_health_summary_pageturn_exercise_nor:I = 0x7f02069e

.field public static final s_health_summary_pageturn_exercise_press:I = 0x7f02069f

.field public static final s_health_summary_pageturn_exercise_select:I = 0x7f0206a0

.field public static final s_health_summary_pageturn_focus:I = 0x7f0206a1

.field public static final s_health_summary_pageturn_food_focus:I = 0x7f0206a2

.field public static final s_health_summary_pageturn_food_nor:I = 0x7f0206a3

.field public static final s_health_summary_pageturn_food_press:I = 0x7f0206a4

.field public static final s_health_summary_pageturn_food_select:I = 0x7f0206a5

.field public static final s_health_summary_pageturn_hr_focus:I = 0x7f0206a6

.field public static final s_health_summary_pageturn_hr_nor:I = 0x7f0206a7

.field public static final s_health_summary_pageturn_hr_press:I = 0x7f0206a8

.field public static final s_health_summary_pageturn_hr_select:I = 0x7f0206a9

.field public static final s_health_summary_pageturn_nor:I = 0x7f0206aa

.field public static final s_health_summary_pageturn_pedometer_focus:I = 0x7f0206ab

.field public static final s_health_summary_pageturn_pedometer_nor:I = 0x7f0206ac

.field public static final s_health_summary_pageturn_pedometer_press:I = 0x7f0206ad

.field public static final s_health_summary_pageturn_pedometer_select:I = 0x7f0206ae

.field public static final s_health_summary_pageturn_press:I = 0x7f0206af

.field public static final s_health_summary_pageturn_select:I = 0x7f0206b0

.field public static final s_health_summary_pageturn_sleep_focus:I = 0x7f0206b1

.field public static final s_health_summary_pageturn_sleep_nor:I = 0x7f0206b2

.field public static final s_health_summary_pageturn_sleep_press:I = 0x7f0206b3

.field public static final s_health_summary_pageturn_sleep_select:I = 0x7f0206b4

.field public static final s_health_summary_pageturn_spo2_focus:I = 0x7f0206b5

.field public static final s_health_summary_pageturn_spo2_nor:I = 0x7f0206b6

.field public static final s_health_summary_pageturn_spo2_press:I = 0x7f0206b7

.field public static final s_health_summary_pageturn_spo2_select:I = 0x7f0206b8

.field public static final s_health_summary_pageturn_stress_focus:I = 0x7f0206b9

.field public static final s_health_summary_pageturn_stress_nor:I = 0x7f0206ba

.field public static final s_health_summary_pageturn_stress_press:I = 0x7f0206bb

.field public static final s_health_summary_pageturn_stress_select:I = 0x7f0206bc

.field public static final s_health_summary_pageturn_thermo_hygrometer_focus:I = 0x7f0206bd

.field public static final s_health_summary_pageturn_thermo_hygrometer_nor:I = 0x7f0206be

.field public static final s_health_summary_pageturn_thermo_hygrometer_press:I = 0x7f0206bf

.field public static final s_health_summary_pageturn_thermo_hygrometer_select:I = 0x7f0206c0

.field public static final s_health_summary_pageturn_uv_focus:I = 0x7f0206c1

.field public static final s_health_summary_pageturn_uv_nor:I = 0x7f0206c2

.field public static final s_health_summary_pageturn_uv_press:I = 0x7f0206c3

.field public static final s_health_summary_pageturn_uv_select:I = 0x7f0206c4

.field public static final s_health_summary_pageturn_weight_focus:I = 0x7f0206c5

.field public static final s_health_summary_pageturn_weight_nor:I = 0x7f0206c6

.field public static final s_health_summary_pageturn_weight_press:I = 0x7f0206c7

.field public static final s_health_summary_pageturn_weight_select:I = 0x7f0206c8

.field public static final s_health_summary_pause_focus:I = 0x7f0206c9

.field public static final s_health_summary_pause_nor:I = 0x7f0206ca

.field public static final s_health_summary_pause_press:I = 0x7f0206cb

.field public static final s_health_summary_pedometer_crown:I = 0x7f0206cc

.field public static final s_health_summary_pedometer_divider:I = 0x7f0206cd

.field public static final s_health_summary_pedometer_download_color:I = 0x7f0206ce

.field public static final s_health_summary_pedometer_healthy_step_icon:I = 0x7f0206cf

.field public static final s_health_summary_pedometer_icon:I = 0x7f0206d0

.field public static final s_health_summary_pedometer_icon_medal:I = 0x7f0206d1

.field public static final s_health_summary_pedometer_inactive_time_icon:I = 0x7f0206d2

.field public static final s_health_summary_pedometer_medal:I = 0x7f0206d3

.field public static final s_health_summary_pedometer_pause_icon:I = 0x7f0206d4

.field public static final s_health_summary_pedometer_sink:I = 0x7f0206d5

.field public static final s_health_summary_play_focus:I = 0x7f0206d6

.field public static final s_health_summary_play_nor:I = 0x7f0206d7

.field public static final s_health_summary_play_press:I = 0x7f0206d8

.field public static final s_health_summary_plus:I = 0x7f0206d9

.field public static final s_health_summary_rewind_focus:I = 0x7f0206da

.field public static final s_health_summary_rewind_nor:I = 0x7f0206db

.field public static final s_health_summary_rewind_press:I = 0x7f0206dc

.field public static final s_health_summary_s_accessary:I = 0x7f0206dd

.field public static final s_health_summary_s_gps:I = 0x7f0206de

.field public static final s_health_summary_s_gps_off:I = 0x7f0206df

.field public static final s_health_summary_sleep_icon_green:I = 0x7f0206e0

.field public static final s_health_summary_sleep_icon_orange:I = 0x7f0206e1

.field public static final s_health_summary_sleep_icon_red:I = 0x7f0206e2

.field public static final s_health_summary_unwind_focus:I = 0x7f0206e3

.field public static final s_health_summary_unwind_nor:I = 0x7f0206e4

.field public static final s_health_summary_unwind_press:I = 0x7f0206e5

.field public static final s_health_summary_weight_devider:I = 0x7f0206e6

.field public static final s_health_summary_weight_devider_01:I = 0x7f0206e7

.field public static final s_health_summary_weight_icon:I = 0x7f0206e8

.field public static final s_health_summary_weight_icon_green:I = 0x7f0206e9

.field public static final s_health_summary_weight_icon_orange:I = 0x7f0206ea

.field public static final s_health_summary_weight_icon_red:I = 0x7f0206eb

.field public static final s_health_summary_weight_icon_s:I = 0x7f0206ec

.field public static final s_health_summary_weight_image01:I = 0x7f0206ed

.field public static final s_health_summary_weight_mask:I = 0x7f0206ee

.field public static final s_health_summary_weight_percent:I = 0x7f0206ef

.field public static final s_health_temperature_acc_b2:I = 0x7f0206f0

.field public static final s_health_temperature_acc_sband:I = 0x7f0206f1

.field public static final s_health_temperature_samsung_gear:I = 0x7f0206f2

.field public static final s_health_temperature_samsung_gear2:I = 0x7f0206f3

.field public static final s_health_temperature_samsung_gearfit:I = 0x7f0206f4

.field public static final s_health_temperature_samsung_neogear:I = 0x7f0206f5

.field public static final s_health_tgh_noitem:I = 0x7f0206f6

.field public static final s_health_thermo_c:I = 0x7f0206f7

.field public static final s_health_thermo_f:I = 0x7f0206f8

.field public static final s_health_thermo_hygrometer_humidity:I = 0x7f0206f9

.field public static final s_health_thermo_hygrometer_icon:I = 0x7f0206fa

.field public static final s_health_thermo_hygrometer_image:I = 0x7f0206fb

.field public static final s_health_thermo_hygrometer_scan:I = 0x7f0206fc

.field public static final s_health_thermo_hygrometer_temperature:I = 0x7f0206fd

.field public static final s_health_thermo_percent:I = 0x7f0206fe

.field public static final s_health_timeline_bg:I = 0x7f0206ff

.field public static final s_health_title_wheel_bg_01:I = 0x7f020700

.field public static final s_health_uv_loglist_icon_spf:I = 0x7f020701

.field public static final s_health_uv_skin_press:I = 0x7f020702

.field public static final s_health_uv_skin_select:I = 0x7f020703

.field public static final s_health_uv_summary_icon_tag_nor:I = 0x7f020704

.field public static final s_health_walk_noitem:I = 0x7f020705

.field public static final s_health_walking_ranking:I = 0x7f020706

.field public static final s_health_week_button_normal:I = 0x7f020707

.field public static final s_health_week_button_select:I = 0x7f020708

.field public static final s_health_weight_noitem:I = 0x7f020709

.field public static final s_health_widget_bg01:I = 0x7f02070a

.field public static final s_health_widget_bg02:I = 0x7f02070b

.field public static final s_health_widget_bg03:I = 0x7f02070c

.field public static final s_health_widget_bg04:I = 0x7f02070d

.field public static final s_health_widget_cigna_icon:I = 0x7f02070e

.field public static final s_health_widget_cigna_logo01:I = 0x7f02070f

.field public static final s_health_widget_engraft_cigna_icon:I = 0x7f020710

.field public static final s_health_widget_engraft_cigna_logo:I = 0x7f020711

.field public static final s_health_widget_engraft_healthystep_icon:I = 0x7f020712

.field public static final s_health_widget_engraft_healthystep_icon_zero:I = 0x7f020713

.field public static final s_health_widget_engraft_heartrate_icon:I = 0x7f020714

.field public static final s_health_widget_engraft_inactive_icon:I = 0x7f020715

.field public static final s_health_widget_engraft_inactive_icon_zero:I = 0x7f020716

.field public static final s_health_widget_engraft_pedometer_icon:I = 0x7f020717

.field public static final s_health_widget_engraft_pedometer_icon_zero:I = 0x7f020718

.field public static final s_health_widget_engraft_pedometer_mask:I = 0x7f020719

.field public static final s_health_widget_engraft_pedometer_medal:I = 0x7f02071a

.field public static final s_health_widget_engraft_pedometer_progress:I = 0x7f02071b

.field public static final s_health_widget_engraft_wearable_icon:I = 0x7f02071c

.field public static final s_health_widget_engraft_wearable_icon_zero:I = 0x7f02071d

.field public static final s_health_widget_engraft_wearable_medal:I = 0x7f02071e

.field public static final s_health_widget_hr_icon:I = 0x7f02071f

.field public static final s_health_widget_mask:I = 0x7f020720

.field public static final s_health_widget_next_icon:I = 0x7f020721

.field public static final s_health_widget_pedometer_gear_icon:I = 0x7f020722

.field public static final s_health_widget_pedometer_gear_icon_02:I = 0x7f020723

.field public static final s_health_widget_pedometer_gear_icon_zero:I = 0x7f020724

.field public static final s_health_widget_pedometer_healthystep_icon:I = 0x7f020725

.field public static final s_health_widget_pedometer_icon:I = 0x7f020726

.field public static final s_health_widget_pedometer_icon_02:I = 0x7f020727

.field public static final s_health_widget_pedometer_icon_02_zero:I = 0x7f020728

.field public static final s_health_widget_pedometer_icon_zero:I = 0x7f020729

.field public static final s_health_widget_pedometer_inactivetime_icon:I = 0x7f02072a

.field public static final s_health_widget_pedometer_initial_icon:I = 0x7f02072b

.field public static final s_health_widget_pedometer_pause_icon:I = 0x7f02072c

.field public static final s_health_widget_pro_icon:I = 0x7f02072d

.field public static final s_health_widget_pro_icon_mask:I = 0x7f02072e

.field public static final s_health_widget_pro_medal:I = 0x7f02072f

.field public static final s_health_widget_sink:I = 0x7f020730

.field public static final s_health_widget_sync:I = 0x7f020731

.field public static final s_health_widget_thumnail_4x1:I = 0x7f020732

.field public static final s_health_widget_thumnail_4x1_cigna:I = 0x7f020733

.field public static final s_health_widget_thumnail_4x1_cigna_tr:I = 0x7f020734

.field public static final s_health_widget_thumnail_4x1_old:I = 0x7f020735

.field public static final s_health_widget_thumnail_4x1_tr:I = 0x7f020736

.field public static final scan_button_selector:I = 0x7f020737

.field public static final scanning_progress_circle:I = 0x7f020738

.field public static final search_cancel_button_selector:I = 0x7f020739

.field public static final search_edit_text_selector:I = 0x7f02073a

.field public static final select_all_bg:I = 0x7f02073b

.field public static final setting_terms_use:I = 0x7f02073c

.field public static final settings_drawable:I = 0x7f02073d

.field public static final settings_list_selector:I = 0x7f02073e

.field public static final share_item_bg_color:I = 0x7f02073f

.field public static final share_popup_item_selector:I = 0x7f020740

.field public static final shealth_uv_skin_type_1:I = 0x7f020741

.field public static final shealth_uv_skin_type_1_disabled:I = 0x7f020742

.field public static final shealth_uv_skin_type_2:I = 0x7f020743

.field public static final shealth_uv_skin_type_2_disabled:I = 0x7f020744

.field public static final shealth_uv_skin_type_3:I = 0x7f020745

.field public static final shealth_uv_skin_type_3_disabled:I = 0x7f020746

.field public static final shealth_uv_skin_type_4:I = 0x7f020747

.field public static final shealth_uv_skin_type_4_disabled:I = 0x7f020748

.field public static final shealth_uv_skin_type_5:I = 0x7f020749

.field public static final shealth_uv_skin_type_5_disabled:I = 0x7f02074a

.field public static final shealth_uv_skin_type_6:I = 0x7f02074b

.field public static final shealth_uv_skin_type_6_disabled:I = 0x7f02074c

.field public static final shestia_chart_icon_extreme:I = 0x7f02074d

.field public static final shestia_chart_icon_high:I = 0x7f02074e

.field public static final shestia_chart_icon_low:I = 0x7f02074f

.field public static final shestia_chart_icon_moderate:I = 0x7f020750

.field public static final shestia_chart_icon_veryhigh:I = 0x7f020751

.field public static final sleep_custom_rating_bar:I = 0x7f020752

.field public static final sleep_loglist_up_down_selector:I = 0x7f020753

.field public static final sleep_s_health_summary_pageturn_sleep:I = 0x7f020754

.field public static final sleep_selector_log_group_expandable_background:I = 0x7f020755

.field public static final spinner_divider:I = 0x7f020756

.field public static final spo2_green:I = 0x7f020757

.field public static final spo2_guide_hestia_5:I = 0x7f020758

.field public static final spo2_guide_hestia_6:I = 0x7f020759

.field public static final spo2_orange:I = 0x7f02075a

.field public static final spo2_selector_dialog_button:I = 0x7f02075b

.field public static final spo2_selector_log_group_expand:I = 0x7f02075c

.field public static final spo2_selector_log_group_expandable_background:I = 0x7f02075d

.field public static final spo2_selector_summary_text:I = 0x7f02075e

.field public static final stit_health_exercise:I = 0x7f02075f

.field public static final stit_health_exercise_light:I = 0x7f020760

.field public static final stit_health_exercise_medal:I = 0x7f020761

.field public static final stit_health_pedometer:I = 0x7f020762

.field public static final stit_health_pedometer_focus:I = 0x7f020763

.field public static final stit_health_pedometer_healthy_step:I = 0x7f020764

.field public static final stit_health_pedometer_healthy_step_focus:I = 0x7f020765

.field public static final stit_health_pedometer_healthy_step_press:I = 0x7f020766

.field public static final stit_health_pedometer_inactive:I = 0x7f020767

.field public static final stit_health_pedometer_inactive_focus:I = 0x7f020768

.field public static final stit_health_pedometer_inactive_press:I = 0x7f020769

.field public static final stit_health_pedometer_medal:I = 0x7f02076a

.field public static final stit_health_pedometer_pause:I = 0x7f02076b

.field public static final stit_health_pedometer_pause_focus:I = 0x7f02076c

.field public static final stit_health_pedometer_pause_press:I = 0x7f02076d

.field public static final stit_health_pedometer_press:I = 0x7f02076e

.field public static final stit_health_sync:I = 0x7f02076f

.field public static final stit_health_wearable:I = 0x7f020770

.field public static final stit_health_wearable_focus:I = 0x7f020771

.field public static final stit_health_wearable_medal:I = 0x7f020772

.field public static final stit_health_wearable_press:I = 0x7f020773

.field public static final stm_gradient_bar_first:I = 0x7f020774

.field public static final stm_gradient_bar_fourth:I = 0x7f020775

.field public static final stm_gradient_bar_last:I = 0x7f020776

.field public static final stm_gradient_bar_second:I = 0x7f020777

.field public static final stm_gradient_bar_third:I = 0x7f020778

.field public static final stm_measuring_green_lightning_1:I = 0x7f020779

.field public static final stm_measuring_green_lightning_2:I = 0x7f02077a

.field public static final stm_measuring_orange_lightning_1:I = 0x7f02077b

.field public static final stm_measuring_orange_lightning_2:I = 0x7f02077c

.field public static final stm_selector_log_group_expand:I = 0x7f02077d

.field public static final stm_selector_log_group_expandable_background:I = 0x7f02077e

.field public static final stm_selector_summary_button:I = 0x7f02077f

.field public static final stm_selector_summary_button_rec:I = 0x7f020780

.field public static final stm_selector_summary_pageturn_default_icon:I = 0x7f020781

.field public static final stm_selector_summary_pageturn_stress_icon:I = 0x7f020782

.field public static final stm_selector_summary_text:I = 0x7f020783

.field public static final stress_guide_t_5:I = 0x7f020784

.field public static final stress_guide_t_6:I = 0x7f020785

.field public static final sub_tab_background_selector:I = 0x7f020786

.field public static final summary_button_left_drawable_selector:I = 0x7f020787

.field public static final summary_button_selector:I = 0x7f020788

.field public static final summary_button_text_color_selector:I = 0x7f020789

.field public static final summary_pageturn_default_icon_selector:I = 0x7f02078a

.field public static final summary_pageturn_exercise_icon_selctor:I = 0x7f02078b

.field public static final summary_pageturn_food_icon_selector:I = 0x7f02078c

.field public static final summary_pageturn_hr_icon_selctor:I = 0x7f02078d

.field public static final summary_pageturn_pedometer_icon_selctor:I = 0x7f02078e

.field public static final summary_pageturn_spo2_icon_selctor:I = 0x7f02078f

.field public static final test_select_list:I = 0x7f020790

.field public static final tgh_selector_summary_text:I = 0x7f020791

.field public static final tgh_summary_pageturn_icon_selctor:I = 0x7f020792

.field public static final time_goal_edit_background_selector:I = 0x7f020793

.field public static final timer_number_dot:I = 0x7f020794

.field public static final toast_frame:I = 0x7f020795

.field public static final transparent:I = 0x7f020796

.field public static final transparent_button_selector:I = 0x7f020797

.field public static final try_it_map:I = 0x7f020798

.field public static final tw_ab_bottom_div_holo_light:I = 0x7f020799

.field public static final tw_ab_bottom_transparent_holo_light:I = 0x7f02079a

.field public static final tw_ab_bottom_transparent_light_holo:I = 0x7f02079b

.field public static final tw_ab_bottom_transparent_mtrl:I = 0x7f02079c

.field public static final tw_ab_bottom_transparent_shadow_holo_light:I = 0x7f02079d

.field public static final tw_ab_solid_shadow_holo_light:I = 0x7f02079e

.field public static final tw_ab_spinner_list_focused_holo_light:I = 0x7f02079f

.field public static final tw_ab_spinner_list_pressed_holo_light:I = 0x7f0207a0

.field public static final tw_ab_transparent_dark_holo:I = 0x7f0207a1

.field public static final tw_ab_transparent_holo_light:I = 0x7f0207a2

.field public static final tw_ab_transparent_light_holo:I = 0x7f0207a3

.field public static final tw_action_bar_icon_add:I = 0x7f0207a4

.field public static final tw_action_bar_icon_add_disabled:I = 0x7f0207a5

.field public static final tw_action_bar_icon_camera:I = 0x7f0207a6

.field public static final tw_action_bar_icon_camera_disabled:I = 0x7f0207a7

.field public static final tw_action_bar_icon_cancel_02_disabled_holo_light:I = 0x7f0207a8

.field public static final tw_action_bar_icon_cancel_02_holo_light:I = 0x7f0207a9

.field public static final tw_action_bar_icon_check_disabled_holo_light:I = 0x7f0207aa

.field public static final tw_action_bar_icon_check_holo_light:I = 0x7f0207ab

.field public static final tw_action_bar_icon_clip:I = 0x7f0207ac

.field public static final tw_action_bar_icon_clip_disabled:I = 0x7f0207ad

.field public static final tw_action_bar_icon_connect:I = 0x7f0207ae

.field public static final tw_action_bar_icon_connect_disabled:I = 0x7f0207af

.field public static final tw_action_bar_icon_delete:I = 0x7f0207b0

.field public static final tw_action_bar_icon_delete_disabled:I = 0x7f0207b1

.field public static final tw_action_bar_icon_disconnect:I = 0x7f0207b2

.field public static final tw_action_bar_icon_disconnect_disabled:I = 0x7f0207b3

.field public static final tw_action_bar_icon_edit_nor:I = 0x7f0207b4

.field public static final tw_action_bar_icon_edit_nor_disabled:I = 0x7f0207b5

.field public static final tw_action_bar_icon_favorites:I = 0x7f0207b6

.field public static final tw_action_bar_icon_favorites_disabled:I = 0x7f0207b7

.field public static final tw_action_bar_icon_filter_disabled_holo_light:I = 0x7f0207b8

.field public static final tw_action_bar_icon_filter_holo_light:I = 0x7f0207b9

.field public static final tw_action_bar_icon_guide_disabled_holo_light:I = 0x7f0207ba

.field public static final tw_action_bar_icon_guide_holo_light:I = 0x7f0207bb

.field public static final tw_action_bar_icon_link:I = 0x7f0207bc

.field public static final tw_action_bar_icon_link_disabled:I = 0x7f0207bd

.field public static final tw_action_bar_icon_listoffavorites:I = 0x7f0207be

.field public static final tw_action_bar_icon_listoffavorites_disabled:I = 0x7f0207bf

.field public static final tw_action_bar_icon_loglist:I = 0x7f0207c0

.field public static final tw_action_bar_icon_loglist_disabled:I = 0x7f0207c1

.field public static final tw_action_bar_icon_map:I = 0x7f0207c2

.field public static final tw_action_bar_icon_map_disabled:I = 0x7f0207c3

.field public static final tw_action_bar_icon_print:I = 0x7f0207c4

.field public static final tw_action_bar_icon_print_disabled:I = 0x7f0207c5

.field public static final tw_action_bar_icon_print_disabled_holo_light:I = 0x7f0207c6

.field public static final tw_action_bar_icon_print_holo_light:I = 0x7f0207c7

.field public static final tw_action_bar_icon_profile:I = 0x7f0207c8

.field public static final tw_action_bar_icon_profile_disabled:I = 0x7f0207c9

.field public static final tw_action_bar_icon_reset_holo_light:I = 0x7f0207ca

.field public static final tw_action_bar_icon_search:I = 0x7f0207cb

.field public static final tw_action_bar_icon_search_disabled:I = 0x7f0207cc

.field public static final tw_action_bar_icon_set_goal_disabled_holo_light:I = 0x7f0207cd

.field public static final tw_action_bar_icon_set_goal_holo_light:I = 0x7f0207ce

.field public static final tw_action_bar_icon_share:I = 0x7f0207cf

.field public static final tw_action_bar_icon_share_disabled:I = 0x7f0207d0

.field public static final tw_action_bar_icon_sortby:I = 0x7f0207d1

.field public static final tw_action_bar_icon_sortby_disabled:I = 0x7f0207d2

.field public static final tw_action_bar_icon_total:I = 0x7f0207d3

.field public static final tw_action_bar_icon_total_disabled:I = 0x7f0207d4

.field public static final tw_action_bar_icon_viewby:I = 0x7f0207d5

.field public static final tw_action_bar_icon_viewby_disabled:I = 0x7f0207d6

.field public static final tw_action_bar_icon_workout:I = 0x7f0207d7

.field public static final tw_action_bar_icon_workout_disabled:I = 0x7f0207d8

.field public static final tw_action_bar_sub_tab_bg_01_holo_light:I = 0x7f0207d9

.field public static final tw_action_bar_sub_tab_bg_holo_light:I = 0x7f0207da

.field public static final tw_action_bar_sub_tab_selected_bg_holo_light:I = 0x7f0207db

.field public static final tw_action_bar_tab_bg_holo_light:I = 0x7f0207dc

.field public static final tw_action_bar_tab_bg_holo_light01:I = 0x7f0207dd

.field public static final tw_action_bar_tab_selected_bg_holo_light:I = 0x7f0207de

.field public static final tw_action_bar_tab_selected_bg_holo_light01:I = 0x7f0207df

.field public static final tw_action_item_background_focused_holo_light:I = 0x7f0207e0

.field public static final tw_action_item_background_pressed_holo_light:I = 0x7f0207e1

.field public static final tw_action_item_background_selected_holo_light:I = 0x7f0207e2

.field public static final tw_app_default_tab_bg_holo_light:I = 0x7f0207e3

.field public static final tw_app_default_tab_selected_bg_holo_light:I = 0x7f0207e4

.field public static final tw_background_dark:I = 0x7f0207e5

.field public static final tw_btn_back_depth_disable_focus_holo_light:I = 0x7f0207e6

.field public static final tw_btn_back_depth_disabled_holo_light:I = 0x7f0207e7

.field public static final tw_btn_back_depth_focus_holo_light:I = 0x7f0207e8

.field public static final tw_btn_back_depth_holo_light:I = 0x7f0207e9

.field public static final tw_btn_back_depth_pressed_holo_light:I = 0x7f0207ea

.field public static final tw_btn_cab_done_focused_holo_light:I = 0x7f0207eb

.field public static final tw_btn_cab_done_pressed_holo_light:I = 0x7f0207ec

.field public static final tw_btn_check_off_disabled_focused_holo_light:I = 0x7f0207ed

.field public static final tw_btn_check_off_disabled_holo_light:I = 0x7f0207ee

.field public static final tw_btn_check_off_focused_holo_light:I = 0x7f0207ef

.field public static final tw_btn_check_off_holo_light:I = 0x7f0207f0

.field public static final tw_btn_check_off_pressed_holo_light:I = 0x7f0207f1

.field public static final tw_btn_check_on_disabled_focused_holo_light:I = 0x7f0207f2

.field public static final tw_btn_check_on_disabled_holo_light:I = 0x7f0207f3

.field public static final tw_btn_check_on_focused_holo_light:I = 0x7f0207f4

.field public static final tw_btn_check_on_holo_light:I = 0x7f0207f5

.field public static final tw_btn_check_on_pressed_holo_light:I = 0x7f0207f6

.field public static final tw_btn_default_disabled_focused_holo_light:I = 0x7f0207f7

.field public static final tw_btn_default_disabled_holo_light:I = 0x7f0207f8

.field public static final tw_btn_default_focused_holo_light:I = 0x7f0207f9

.field public static final tw_btn_default_normal_holo_light:I = 0x7f0207fa

.field public static final tw_btn_default_pressed_holo_light:I = 0x7f0207fb

.field public static final tw_btn_default_selected_holo_light:I = 0x7f0207fc

.field public static final tw_btn_icon_next_holo_light:I = 0x7f0207fd

.field public static final tw_btn_icon_next_holo_light_d:I = 0x7f0207fe

.field public static final tw_btn_icon_next_holo_light_n:I = 0x7f0207ff

.field public static final tw_btn_icon_previous_holo_light:I = 0x7f020800

.field public static final tw_btn_icon_previous_holo_light_d:I = 0x7f020801

.field public static final tw_btn_icon_previous_holo_light_n:I = 0x7f020802

.field public static final tw_btn_next_default_holo_light:I = 0x7f020803

.field public static final tw_btn_next_depth_disabled_focused_holo_light:I = 0x7f020804

.field public static final tw_btn_next_depth_disabled_holo_light:I = 0x7f020805

.field public static final tw_btn_next_depth_focused_holo_light:I = 0x7f020806

.field public static final tw_btn_next_depth_holo_light:I = 0x7f020807

.field public static final tw_btn_next_depth_pressed_holo_light:I = 0x7f020808

.field public static final tw_btn_previous_default_holo_light:I = 0x7f020809

.field public static final tw_btn_radio_off_disabled_focused_holo_light:I = 0x7f02080a

.field public static final tw_btn_radio_off_disabled_holo_light:I = 0x7f02080b

.field public static final tw_btn_radio_off_focused_holo_light:I = 0x7f02080c

.field public static final tw_btn_radio_off_holo_light:I = 0x7f02080d

.field public static final tw_btn_radio_off_pressed_holo_light:I = 0x7f02080e

.field public static final tw_btn_radio_on_disabled_focused_holo_light:I = 0x7f02080f

.field public static final tw_btn_radio_on_disabled_holo_light:I = 0x7f020810

.field public static final tw_btn_radio_on_focused_holo_light:I = 0x7f020811

.field public static final tw_btn_radio_on_holo_light:I = 0x7f020812

.field public static final tw_btn_radio_on_pressed_holo_light:I = 0x7f020813

.field public static final tw_btn_radio_selector:I = 0x7f020814

.field public static final tw_btn_switch_ab_off_bg:I = 0x7f020815

.field public static final tw_btn_switch_ab_on_bg:I = 0x7f020816

.field public static final tw_btn_switch_ab_on_default:I = 0x7f020817

.field public static final tw_btn_switch_ab_on_focused:I = 0x7f020818

.field public static final tw_btn_switch_ab_on_pressed:I = 0x7f020819

.field public static final tw_btn_switch_off_default:I = 0x7f02081a

.field public static final tw_btn_switch_off_focused:I = 0x7f02081b

.field public static final tw_btn_switch_off_pressed:I = 0x7f02081c

.field public static final tw_buttonbarbutton_selector_default_holo_light:I = 0x7f02081d

.field public static final tw_buttonbarbutton_selector_default_l_holo_light:I = 0x7f02081e

.field public static final tw_buttonbarbutton_selector_default_r_holo_light:I = 0x7f02081f

.field public static final tw_buttonbarbutton_selector_disabled_focused_holo_light:I = 0x7f020820

.field public static final tw_buttonbarbutton_selector_disabled_focused_l_holo_light:I = 0x7f020821

.field public static final tw_buttonbarbutton_selector_disabled_focused_r_holo_light:I = 0x7f020822

.field public static final tw_buttonbarbutton_selector_disabled_holo_light:I = 0x7f020823

.field public static final tw_buttonbarbutton_selector_disabled_l_holo_light:I = 0x7f020824

.field public static final tw_buttonbarbutton_selector_disabled_r_holo_light:I = 0x7f020825

.field public static final tw_buttonbarbutton_selector_focused_holo_light:I = 0x7f020826

.field public static final tw_buttonbarbutton_selector_focused_l_holo_light:I = 0x7f020827

.field public static final tw_buttonbarbutton_selector_focused_r_holo_light:I = 0x7f020828

.field public static final tw_buttonbarbutton_selector_pressed_holo_light:I = 0x7f020829

.field public static final tw_buttonbarbutton_selector_pressed_l_holo_light:I = 0x7f02082a

.field public static final tw_buttonbarbutton_selector_pressed_r_holo_light:I = 0x7f02082b

.field public static final tw_buttonbarbutton_selector_selected_holo_light:I = 0x7f02082c

.field public static final tw_buttonbarbutton_selector_selected_l_holo_light:I = 0x7f02082d

.field public static final tw_buttonbarbutton_selector_selected_r_holo_light:I = 0x7f02082e

.field public static final tw_cab_background_top_holo_light:I = 0x7f02082f

.field public static final tw_dialog_bottom_holo_light:I = 0x7f020830

.field public static final tw_dialog_bottom_medium_holo_light:I = 0x7f020831

.field public static final tw_dialog_full_holo_light:I = 0x7f020832

.field public static final tw_dialog_middle_holo_light:I = 0x7f020833

.field public static final tw_dialog_middle_list_line:I = 0x7f020834

.field public static final tw_dialog_title_holo_light:I = 0x7f020835

.field public static final tw_dialog_top_holo_light:I = 0x7f020836

.field public static final tw_dialog_top_medium_holo_light:I = 0x7f020837

.field public static final tw_divider_ab_holo_light:I = 0x7f020838

.field public static final tw_divider_option_popup_holo_light:I = 0x7f020839

.field public static final tw_divider_popup_vertical_holo_light:I = 0x7f02083a

.field public static final tw_divider_vertical_holo_light:I = 0x7f02083b

.field public static final tw_drawer_bg_holo_dark:I = 0x7f02083c

.field public static final tw_drawer_bg_holo_light:I = 0x7f02083d

.field public static final tw_drawer_list_line_holo_light:I = 0x7f02083e

.field public static final tw_drawerlist_disabled_focused_holo_light:I = 0x7f02083f

.field public static final tw_drawerlist_disabled_holo_light:I = 0x7f020840

.field public static final tw_drawerlist_focused_holo_light:I = 0x7f020841

.field public static final tw_drawerlist_pressed_holo_light:I = 0x7f020842

.field public static final tw_drawerlist_section_divider_holo_light2:I = 0x7f020843

.field public static final tw_drawerlist_selected_holo_light:I = 0x7f020844

.field public static final tw_expander_close_01_holo_light:I = 0x7f020845

.field public static final tw_expander_close_default_holo_light:I = 0x7f020846

.field public static final tw_expander_close_disabled_focused_holo_light:I = 0x7f020847

.field public static final tw_expander_close_disabled_holo_light:I = 0x7f020848

.field public static final tw_expander_close_focused_holo_light:I = 0x7f020849

.field public static final tw_expander_list_bg_holo_light:I = 0x7f02084a

.field public static final tw_expander_list_line_holo_light:I = 0x7f02084b

.field public static final tw_expander_open_01_holo_light:I = 0x7f02084c

.field public static final tw_expander_open_default_holo_light:I = 0x7f02084d

.field public static final tw_expander_open_disabled_focused_holo_light:I = 0x7f02084e

.field public static final tw_expander_open_disabled_holo_light:I = 0x7f02084f

.field public static final tw_expander_open_focused_holo_light:I = 0x7f020850

.field public static final tw_ic_ab_back_holo_light:I = 0x7f020851

.field public static final tw_ic_ab_drawer_holo_light:I = 0x7f020852

.field public static final tw_ic_ab_share_holo_light:I = 0x7f020853

.field public static final tw_ic_cab_done_holo_light:I = 0x7f020854

.field public static final tw_ic_cancel:I = 0x7f020855

.field public static final tw_ic_clear_search_api_disabled_holo_light:I = 0x7f020856

.field public static final tw_ic_search:I = 0x7f020857

.field public static final tw_ic_search_dim:I = 0x7f020858

.field public static final tw_ic_search_voice:I = 0x7f020859

.field public static final tw_list_disabled_focused_holo_light:I = 0x7f02085a

.field public static final tw_list_disabled_holo_light:I = 0x7f02085b

.field public static final tw_list_divider_holo_light:I = 0x7f02085c

.field public static final tw_list_focused_holo_light:I = 0x7f02085d

.field public static final tw_list_icon_create01_holo_light:I = 0x7f02085e

.field public static final tw_list_icon_create_disabled_focused_holo_light:I = 0x7f02085f

.field public static final tw_list_icon_create_disabled_holo_light:I = 0x7f020860

.field public static final tw_list_icon_create_focused_holo_light:I = 0x7f020861

.field public static final tw_list_icon_create_holo_light:I = 0x7f020862

.field public static final tw_list_icon_minus_holo_light:I = 0x7f020863

.field public static final tw_list_longpressed_holo_light:I = 0x7f020864

.field public static final tw_list_pressed_holo_light:I = 0x7f020865

.field public static final tw_list_section_divider_focused_holo_light:I = 0x7f020866

.field public static final tw_list_section_divider_holo_light:I = 0x7f020867

.field public static final tw_list_section_divider_holo_light_2:I = 0x7f020868

.field public static final tw_list_section_divider_index_holo_light:I = 0x7f020869

.field public static final tw_list_section_divider_pressed_holo_light:I = 0x7f02086a

.field public static final tw_list_section_divider_selected_holo_light:I = 0x7f02086b

.field public static final tw_list_selected_holo_light:I = 0x7f02086c

.field public static final tw_list_selector_disabled_holo_light:I = 0x7f02086d

.field public static final tw_menu_ab_dropdown_panel_holo_light:I = 0x7f02086e

.field public static final tw_menu_dropdown_panel_holo_light:I = 0x7f02086f

.field public static final tw_menu_hardkey_panel_holo_light:I = 0x7f020870

.field public static final tw_menu_popup_panel_holo_light:I = 0x7f020871

.field public static final tw_no_item_popup_bg_holo_light:I = 0x7f020872

.field public static final tw_progress_bg_holo_light:I = 0x7f020873

.field public static final tw_progress_primary_holo_light:I = 0x7f020874

.field public static final tw_scrollbar_holo_light:I = 0x7f020875

.field public static final tw_search_field_icon_barcode:I = 0x7f020876

.field public static final tw_search_field_icon_voice:I = 0x7f020877

.field public static final tw_searchfiled_background_holo_light:I = 0x7f020878

.field public static final tw_select_all_bg_holo_light:I = 0x7f020879

.field public static final tw_spinner_ab_default_holo_light_am:I = 0x7f02087a

.field public static final tw_spinner_ab_disabled_holo_light_am:I = 0x7f02087b

.field public static final tw_spinner_ab_focused_holo_light:I = 0x7f02087c

.field public static final tw_spinner_ab_focused_holo_light_am:I = 0x7f02087d

.field public static final tw_spinner_ab_pressed_holo_light:I = 0x7f02087e

.field public static final tw_spinner_ab_pressed_holo_light_am:I = 0x7f02087f

.field public static final tw_spinner_default_holo_light:I = 0x7f020880

.field public static final tw_spinner_default_holo_light_am:I = 0x7f020881

.field public static final tw_spinner_disabled_holo_light:I = 0x7f020882

.field public static final tw_spinner_disabled_holo_light_am:I = 0x7f020883

.field public static final tw_spinner_focused_holo_light:I = 0x7f020884

.field public static final tw_spinner_focused_holo_light_am:I = 0x7f020885

.field public static final tw_spinner_list_focused_holo_light:I = 0x7f020886

.field public static final tw_spinner_list_pressed_holo_light:I = 0x7f020887

.field public static final tw_spinner_pressed_holo_light:I = 0x7f020888

.field public static final tw_spinner_pressed_holo_light_am:I = 0x7f020889

.field public static final tw_sub_action_bar_bg_holo_light:I = 0x7f02088a

.field public static final tw_sub_tab_divider_holo_light:I = 0x7f02088b

.field public static final tw_switch_ab_thumb_activated_holo_light:I = 0x7f02088c

.field public static final tw_switch_activation_holo_light:I = 0x7f02088d

.field public static final tw_switch_activation_holo_light_dim:I = 0x7f02088e

.field public static final tw_switch_activation_holo_light_pressed:I = 0x7f02088f

.field public static final tw_switch_bg_disabled_holo_light:I = 0x7f020890

.field public static final tw_switch_bg_focused_holo_light:I = 0x7f020891

.field public static final tw_switch_bg_holo_light:I = 0x7f020892

.field public static final tw_switch_disabled_holo_light:I = 0x7f020893

.field public static final tw_switch_disabled_holo_light_dim:I = 0x7f020894

.field public static final tw_switch_disabled_holo_light_pressed:I = 0x7f020895

.field public static final tw_switch_thumb_activated_holo_light:I = 0x7f020896

.field public static final tw_switch_thumb_activation_disabled_holo_light:I = 0x7f020897

.field public static final tw_switch_thumb_activation_pressed_holo_light:I = 0x7f020898

.field public static final tw_switch_thumb_disabled_holo_light:I = 0x7f020899

.field public static final tw_switch_thumb_holo_light:I = 0x7f02089a

.field public static final tw_switch_thumb_pressed_holo_light:I = 0x7f02089b

.field public static final tw_tab_divider_holo_light:I = 0x7f02089c

.field public static final tw_tab_selected_focused_holo_light:I = 0x7f02089d

.field public static final tw_tab_selected_focused_pressed_holo_light:I = 0x7f02089e

.field public static final tw_tab_selected_pressed_holo_light:I = 0x7f02089f

.field public static final tw_textfield_activated_holo_light:I = 0x7f0208a0

.field public static final tw_textfield_default_holo_light:I = 0x7f0208a1

.field public static final tw_textfield_disabled_focused_holo_light:I = 0x7f0208a2

.field public static final tw_textfield_disabled_holo_light:I = 0x7f0208a3

.field public static final tw_textfield_focused_holo_light:I = 0x7f0208a4

.field public static final tw_textfield_multiline_activated_holo_light:I = 0x7f0208a5

.field public static final tw_textfield_multiline_default_holo_light:I = 0x7f0208a6

.field public static final tw_textfield_multiline_disabled_focused_holo_light:I = 0x7f0208a7

.field public static final tw_textfield_multiline_disabled_holo_light:I = 0x7f0208a8

.field public static final tw_textfield_multiline_focused_holo_light:I = 0x7f0208a9

.field public static final tw_textfield_multiline_pressed_holo_light:I = 0x7f0208aa

.field public static final tw_textfield_multiline_selected_holo_light:I = 0x7f0208ab

.field public static final tw_textfield_pressed_holo_light:I = 0x7f0208ac

.field public static final tw_textfield_search_default_holo_light:I = 0x7f0208ad

.field public static final tw_textfield_search_focused_holo_light:I = 0x7f0208ae

.field public static final tw_textfield_search_pressed_holo_light:I = 0x7f0208af

.field public static final tw_textfield_search_selected_holo_light:I = 0x7f0208b0

.field public static final tw_textfield_selected_holo_light:I = 0x7f0208b1

.field public static final tw_toast_frame_holo_light:I = 0x7f0208b2

.field public static final tw_widget_progressbar:I = 0x7f0208b3

.field public static final tw_widget_progressbar_holo_light:I = 0x7f0208b4

.field public static final tw_widget_progressbar_holo_light_02:I = 0x7f0208b5

.field public static final tw_widget_progressbar_small_2_holo_light:I = 0x7f0208b6

.field public static final uv_guide_hestia_1:I = 0x7f0208b7

.field public static final uv_guide_hestia_2:I = 0x7f0208b8

.field public static final uv_guide_hestia_3:I = 0x7f0208b9

.field public static final uv_guide_hestia_4:I = 0x7f0208ba

.field public static final uv_guide_hestia_5:I = 0x7f0208bb

.field public static final uv_guide_hestia_6:I = 0x7f0208bc

.field public static final uv_guide_t_1:I = 0x7f0208bd

.field public static final uv_guide_t_2:I = 0x7f0208be

.field public static final uv_guide_t_3:I = 0x7f0208bf

.field public static final uv_guide_t_4:I = 0x7f0208c0

.field public static final uv_guide_t_5:I = 0x7f0208c1

.field public static final uv_guide_t_6:I = 0x7f0208c2

.field public static final uv_information_animation:I = 0x7f0208c3

.field public static final uv_selector_summary_pageturn_icon:I = 0x7f0208c4

.field public static final uv_skin_list_selector:I = 0x7f0208c5

.field public static final uv_skin_selector:I = 0x7f0208c6

.field public static final uv_skin_tone_selector_1:I = 0x7f0208c7

.field public static final uv_skin_tone_selector_2:I = 0x7f0208c8

.field public static final uv_skin_tone_selector_3:I = 0x7f0208c9

.field public static final uv_skin_tone_selector_4:I = 0x7f0208ca

.field public static final uv_skin_tone_selector_5:I = 0x7f0208cb

.field public static final uv_skin_tone_selector_6:I = 0x7f0208cc

.field public static final uv_sun_animator:I = 0x7f0208cd

.field public static final vertical_input_module_controller_view_background_green_pin:I = 0x7f0208ce

.field public static final vertical_input_module_controller_view_background_orange_pin:I = 0x7f0208cf

.field public static final vertical_input_module_down_btn_selector:I = 0x7f0208d0

.field public static final vertical_input_module_up_btn_selector:I = 0x7f0208d1

.field public static final walk_accessory_selector:I = 0x7f0208d2

.field public static final walk_bottom_button_selector:I = 0x7f0208d3

.field public static final walk_edit_text_bg:I = 0x7f0208d4

.field public static final walk_edit_text_color_selector:I = 0x7f0208d5

.field public static final walk_everyone_ranking_dialog_button_selector:I = 0x7f0208d6

.field public static final walk_everyone_ranking_dialog_icon_border:I = 0x7f0208d7

.field public static final walk_gradient_progress_item:I = 0x7f0208d8

.field public static final walk_input_icon_back_selector:I = 0x7f0208d9

.field public static final walk_input_icon_next_selector:I = 0x7f0208da

.field public static final walk_list_item_bg_selector:I = 0x7f0208db

.field public static final walk_log_list_group_selector:I = 0x7f0208dc

.field public static final walk_log_selector:I = 0x7f0208dd

.field public static final walk_loglist_up_down_selector:I = 0x7f0208de

.field public static final walk_s_health_blood_glucose_orange_repeat_tile_mode:I = 0x7f0208df

.field public static final walk_s_health_care_btn_selector:I = 0x7f0208e0

.field public static final walk_s_health_first_btn_selector:I = 0x7f0208e1

.field public static final walk_s_health_second_btn_selector:I = 0x7f0208e2

.field public static final walk_share_selector:I = 0x7f0208e3

.field public static final walk_sync_progress:I = 0x7f0208e4

.field public static final walk_sync_progress_cocktail:I = 0x7f0208e5

.field public static final walk_sync_progress_summary:I = 0x7f0208e6

.field public static final walking_icon_default:I = 0x7f0208e7

.field public static final walking_icon_default_dim:I = 0x7f0208e8

.field public static final walking_icon_default_sview:I = 0x7f0208e9

.field public static final walking_icon_goldmedal_1:I = 0x7f0208ea

.field public static final walking_icon_inactive:I = 0x7f0208eb

.field public static final walking_icon_pause:I = 0x7f0208ec

.field public static final walking_icon_step:I = 0x7f0208ed

.field public static final water_handler_nor:I = 0x7f0208ee

.field public static final water_handler_press:I = 0x7f0208ef

.field public static final water_list_gold_medal:I = 0x7f0208f0

.field public static final water_popup_intake_handler:I = 0x7f0208f1

.field public static final weblink_txt_selector:I = 0x7f0208f2

.field public static final welcome_arrow_selector:I = 0x7f0208f3

.field public static final welcome_page_arrow_selector:I = 0x7f0208f4

.field public static final welcome_page_background_selector:I = 0x7f0208f5

.field public static final welcomepage_s_health:I = 0x7f0208f6

.field public static final wgt_chart_pageturn:I = 0x7f0208f7

.field public static final wgt_goal_progress_bar_gradient:I = 0x7f0208f8

.field public static final wgt_next_button_light_selector:I = 0x7f0208f9

.field public static final wgt_previous_button_light_selector:I = 0x7f0208fa

.field public static final wgt_summary_pageturn:I = 0x7f0208fb

.field public static final widget_innerline:I = 0x7f0208fc

.field public static final widget_shadow:I = 0x7f0208fd

.field public static final widget_thumbnail_walk:I = 0x7f0208fe


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
