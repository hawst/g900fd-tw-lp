.class Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;
.super Ljava/lang/Object;
.source "CoachRestoreHelper.java"

# interfaces
.implements Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->cignaBackupRestore(Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V
    .locals 4
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "isSuccess"    # Z
    .param p3, "taskError"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    .param p4, "errorMessage"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x64

    .line 252
    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "restore on Finished"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    if-ne p3, v0, :cond_0

    .line 255
    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cigna Restore successfully completed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->updateProgressPopupValue(I)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$200(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setCignaRestoreTriggerPopup(Landroid/content/Context;Z)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NONE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->currentStep:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$002(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mPopupHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1300(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 273
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 288
    :goto_0
    return-void

    .line 284
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cigna Restore failed with error code : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$200(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setCignaRestoreTriggerPopup(Landroid/content/Context;Z)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->onException(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0
.end method

.method public onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    .locals 9
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "fractionCompleted"    # D

    .prologue
    .line 230
    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1000()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "restore on progress"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const/4 v2, 0x0

    .line 233
    .local v2, "startValue":I
    const/high16 v0, 0x3f800000    # 1.0f

    .line 235
    .local v0, "multFactor":F
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->isBackup:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1200(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 236
    const/16 v2, 0x32

    .line 237
    const/high16 v0, 0x3f000000    # 0.5f

    .line 240
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$400(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/widget/ProgressBar;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$600(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 241
    int-to-double v3, v2

    float-to-double v5, v0

    mul-double/2addr v5, p2

    const-wide/high16 v7, 0x4059000000000000L    # 100.0

    mul-double/2addr v5, v7

    add-double/2addr v3, v5

    double-to-int v1, v3

    .line 242
    .local v1, "percentage":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->updateProgressPopupValue(I)V

    .line 244
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Progress : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    .end local v1    # "percentage":I
    :cond_1
    return-void
.end method

.method public onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V
    .locals 2
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    .prologue
    .line 225
    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "restore started"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    return-void
.end method
