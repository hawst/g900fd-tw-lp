.class public final enum Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;
.super Ljava/lang/Enum;
.source "BackupNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/backup/server/BackupNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BackupType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

.field public static final enum Kies:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

.field public static final enum None:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

.field public static final enum Server:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    const-string v1, "None"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;->None:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    new-instance v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    const-string v1, "Kies"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;->Kies:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    new-instance v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    const-string v1, "Server"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;->Server:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;->None:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;->Kies:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;->Server:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;->$VALUES:[Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;->$VALUES:[Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;

    return-object v0
.end method
