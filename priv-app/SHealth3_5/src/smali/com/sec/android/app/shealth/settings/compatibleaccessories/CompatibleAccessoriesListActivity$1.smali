.class Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$1;
.super Ljava/lang/Object;
.source "CompatibleAccessoriesListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 311
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;

    const-class v3, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 313
    .local v1, "intent":Landroid/content/Intent;
    const/4 v0, 0x0

    .line 314
    .local v0, "deviceDetail":Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    if-eqz v2, :cond_0

    .line 315
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "deviceDetail":Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;
    check-cast v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    .line 318
    .restart local v0    # "deviceDetail":Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;
    :cond_0
    const-string v2, "accessory_device_detail"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->startActivity(Landroid/content/Intent;)V

    .line 321
    return-void
.end method
