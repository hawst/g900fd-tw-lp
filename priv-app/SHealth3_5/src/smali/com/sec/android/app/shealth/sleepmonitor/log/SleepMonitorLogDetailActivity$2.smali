.class Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity$2;
.super Ljava/lang/Object;
.source "SleepMonitorLogDetailActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;->showDeletePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 5
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 407
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 408
    .local v1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;->Seletion_ID:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 412
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 422
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;->setResult(I)V

    .line 423
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;->finish()V

    .line 424
    return-void

    .line 414
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 418
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 420
    .local v0, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_0
.end method
