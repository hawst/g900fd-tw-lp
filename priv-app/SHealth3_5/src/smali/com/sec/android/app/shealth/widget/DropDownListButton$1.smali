.class Lcom/sec/android/app/shealth/widget/DropDownListButton$1;
.super Ljava/lang/Object;
.source "DropDownListButton.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/widget/DropDownListButton;->showDropDownList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/widget/DropDownListButton;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 156
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$000(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$000(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$000(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 159
    .local v0, "fontInfo":[Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$100(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-object v3, v0, v3

    const/4 v4, 0x2

    aget-object v4, v0, v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->setTypeface(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mOnDropDownItemClickListener:Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$200(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mOnDropDownItemClickListener:Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$200(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;

    move-result-object v1

    invoke-interface {v1, p3}, Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;->onItemClick(I)V

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownPopup:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$300(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Landroid/widget/PopupWindow;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownPopup:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$300(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 168
    .end local v0    # "fontInfo":[Ljava/lang/String;
    :cond_1
    return-void
.end method
