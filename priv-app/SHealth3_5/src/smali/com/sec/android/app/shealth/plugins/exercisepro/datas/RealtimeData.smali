.class public Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;
.super Ljava/lang/Object;
.source "RealtimeData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private beat_duration:F

.field private createTime:J

.field private dataType:I

.field private exerciseId:J

.field private heartrate_per_min:F

.field private id:J

.field private speed_per_hour:F

.field private timeZone:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 130
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const-wide/16 v0, -0x2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->id:J

    .line 36
    return-void
.end method

.method public constructor <init>(JJIFFFJI)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "exerciseId"    # J
    .param p5, "dataType"    # I
    .param p6, "speed"    # F
    .param p7, "heart"    # F
    .param p8, "beat"    # F
    .param p9, "createTime"    # J
    .param p11, "timeZone"    # I

    .prologue
    const-wide/16 v0, -0x2

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->id:J

    .line 20
    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 22
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id should not be "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->id:J

    .line 25
    iput-wide p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->exerciseId:J

    .line 26
    iput p5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->dataType:I

    .line 27
    iput p6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->speed_per_hour:F

    .line 28
    iput p7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->heartrate_per_min:F

    .line 29
    iput p8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->beat_duration:F

    .line 30
    iput-wide p9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->createTime:J

    .line 31
    iput p11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->timeZone:I

    .line 32
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->createTime:J

    return-wide v0
.end method

.method public getSpeedPerHour()F
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->speed_per_hour:F

    return v0
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "createTime"    # J

    .prologue
    .line 104
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->createTime:J

    .line 105
    return-void
.end method

.method public setDataType(I)V
    .locals 0
    .param p1, "dataType"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->dataType:I

    .line 66
    return-void
.end method

.method public setExerciseId(J)V
    .locals 0
    .param p1, "exerciseId"    # J

    .prologue
    .line 55
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->exerciseId:J

    .line 56
    return-void
.end method

.method public setHeartRatePerMin(F)V
    .locals 0
    .param p1, "value2"    # F

    .prologue
    .line 85
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->heartrate_per_min:F

    .line 86
    return-void
.end method

.method public setSpeedPerHour(F)V
    .locals 0
    .param p1, "value1"    # F

    .prologue
    .line 75
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->speed_per_hour:F

    .line 76
    return-void
.end method

.method public setTimeZone(I)V
    .locals 0
    .param p1, "timeZone"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->timeZone:I

    .line 115
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 120
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 121
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->exerciseId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 122
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->dataType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->speed_per_hour:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 124
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->heartrate_per_min:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 125
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->beat_duration:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 126
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->createTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 127
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->timeZone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 128
    return-void
.end method
