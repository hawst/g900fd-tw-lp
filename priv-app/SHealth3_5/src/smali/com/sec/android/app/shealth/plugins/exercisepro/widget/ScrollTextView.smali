.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
.super Landroid/widget/TextView;
.source "ScrollTextView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# static fields
.field private static final SCROLL_PIXELS_PER_SECONDS:I = 0x32


# instance fields
.field private mAnimator1:Landroid/animation/ObjectAnimator;

.field private mAnimator2:Landroid/animation/ObjectAnimator;

.field private mScrollSpeed:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 16
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mScrollSpeed:I

    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->init()V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mScrollSpeed:I

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->init()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mScrollSpeed:I

    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->init()V

    .line 31
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->setSingleLine()V

    .line 35
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 37
    const-string/jumbo v0, "translationX"

    new-array v1, v3, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator1:Landroid/animation/ObjectAnimator;

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator1:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator1:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 40
    const-string/jumbo v0, "translationX"

    new-array v1, v3, [F

    fill-array-data v1, :array_1

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator2:Landroid/animation/ObjectAnimator;

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator2:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator2:Landroid/animation/ObjectAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator2:Landroid/animation/ObjectAnimator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator2:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 45
    return-void

    .line 37
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 40
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 99
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator1:Landroid/animation/ObjectAnimator;

    if-ne p1, v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator2:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 95
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 103
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 88
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 11
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 56
    if-eqz p1, :cond_1

    .line 57
    invoke-super/range {p0 .. p5}, Landroid/widget/TextView;->onLayout(ZIIII)V

    .line 58
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->measure(II)V

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->getMeasuredWidth()I

    move-result v6

    .line 60
    .local v6, "w":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 61
    .local v5, "parentWidth":I
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator1:Landroid/animation/ObjectAnimator;

    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 62
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator2:Landroid/animation/ObjectAnimator;

    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    .line 64
    .local v3, "parentPaddingLeft":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    .line 65
    .local v4, "parentPaddingRight":I
    sub-int v7, v5, v3

    sub-int v2, v7, v4

    .line 66
    .local v2, "parentActualWidth":I
    if-gez v2, :cond_0

    .line 67
    move v2, v5

    .line 69
    :cond_0
    if-le v6, v2, :cond_2

    .line 70
    neg-int v7, p2

    add-int v0, v7, v3

    .line 71
    .local v0, "fromX1":I
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator1:Landroid/animation/ObjectAnimator;

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    int-to-float v10, v0

    aput v10, v8, v9

    const/4 v9, 0x1

    sub-int v10, v0, v6

    int-to-float v10, v10

    aput v10, v8, v9

    invoke-virtual {v7, v8}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 72
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator1:Landroid/animation/ObjectAnimator;

    mul-int/lit16 v8, v6, 0x3e8

    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mScrollSpeed:I

    div-int/2addr v8, v9

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 73
    add-int v1, v0, v2

    .line 74
    .local v1, "fromX2":I
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator2:Landroid/animation/ObjectAnimator;

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    int-to-float v10, v1

    aput v10, v8, v9

    const/4 v9, 0x1

    sub-int v10, v0, v6

    int-to-float v10, v10

    aput v10, v8, v9

    invoke-virtual {v7, v8}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 75
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator2:Landroid/animation/ObjectAnimator;

    sub-int v8, v1, v0

    add-int/2addr v8, v6

    mul-int/lit16 v8, v8, 0x3e8

    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mScrollSpeed:I

    div-int/2addr v8, v9

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 76
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator1:Landroid/animation/ObjectAnimator;

    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->start()V

    .line 84
    .end local v0    # "fromX1":I
    .end local v1    # "fromX2":I
    .end local v2    # "parentActualWidth":I
    .end local v3    # "parentPaddingLeft":I
    .end local v4    # "parentPaddingRight":I
    .end local v5    # "parentWidth":I
    .end local v6    # "w":I
    :cond_1
    :goto_0
    return-void

    .line 78
    .restart local v2    # "parentActualWidth":I
    .restart local v3    # "parentPaddingLeft":I
    .restart local v4    # "parentPaddingRight":I
    .restart local v5    # "parentWidth":I
    .restart local v6    # "w":I
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator1:Landroid/animation/ObjectAnimator;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    invoke-virtual {v7, v8}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 79
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator1:Landroid/animation/ObjectAnimator;

    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->end()V

    .line 80
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator2:Landroid/animation/ObjectAnimator;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    invoke-virtual {v7, v8}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 81
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mAnimator2:Landroid/animation/ObjectAnimator;

    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->end()V

    goto :goto_0
.end method

.method public setScrollSpeed(I)V
    .locals 0
    .param p1, "speed"    # I

    .prologue
    .line 48
    if-lez p1, :cond_0

    .line 49
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->mScrollSpeed:I

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->invalidate()V

    .line 52
    :cond_0
    return-void
.end method
