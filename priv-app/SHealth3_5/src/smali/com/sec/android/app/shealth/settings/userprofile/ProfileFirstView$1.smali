.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;
.super Ljava/lang/Object;
.source "ProfileFirstView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const v7, 0x7f09020b

    const v6, 0x7f09020a

    const v5, 0x7f0901f0

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->hideKeyboard()V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->gender:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)I

    move-result v0

    const v1, 0x2e636

    if-ne v0, v1, :cond_0

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205dc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$300(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09081f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901f2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    const v1, 0x2e635

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->gender:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$102(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;I)I

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07011b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$700(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09081e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901f1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    return-void
.end method
