.class public abstract Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;
.super Ljava/lang/Object;
.source "ConnectedDeviceFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;,
        Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    }
.end annotation


# static fields
.field public static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeviceConnectionCheckListener:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

.field private mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "deviceConnectionCheckListener"    # Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$1;-><init>(Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mContext:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceConnectionCheckListener:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceConnectionCheckListener:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->isDeviceConnected()Z

    move-result v0

    return v0
.end method

.method private isAvailable()Z
    .locals 7

    .prologue
    .line 92
    const/4 v1, 0x0

    .line 93
    .local v1, "isAvailable":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->getDeviceType()Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 95
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->getDeviceType()Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->getDeviceType()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->getDeviceType()Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->getDataType()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isAvailable(IIILjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 104
    :cond_0
    :goto_0
    return v1

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 99
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 100
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private isDeviceConnected()Z
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->getDeviceType()Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    move-result-object v2

    .line 110
    .local v2, "deviceType":Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;
    iget-object v6, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v6, :cond_1

    if-eqz v2, :cond_1

    .line 113
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->getDeviceType()I

    move-result v8

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->getDataType()I

    move-result v9

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v1

    .line 117
    .local v1, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 118
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 119
    .local v0, "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->isConnected()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v6

    if-eqz v6, :cond_0

    .line 120
    const/4 v5, 0x1

    .line 132
    .end local v0    # "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v1    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    return v5

    .line 124
    :catch_0
    move-exception v3

    .line 125
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->LOG_TAG:Ljava/lang/String;

    invoke-static {v6, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 126
    .end local v3    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v3

    .line 127
    .local v3, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v6, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->LOG_TAG:Ljava/lang/String;

    invoke-static {v6, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 128
    .end local v3    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_2
    move-exception v3

    .line 129
    .local v3, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    sget-object v6, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->LOG_TAG:Ljava/lang/String;

    invoke-static {v6, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private registerHealthSensor()V
    .locals 3

    .prologue
    .line 75
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 76
    return-void
.end method

.method private unregisterHealthSensor()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 84
    :cond_0
    return-void
.end method


# virtual methods
.method public clearListener()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceConnectionCheckListener:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    .line 88
    return-void
.end method

.method protected abstract getDeviceType()Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;
.end method

.method public release()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->unregisterHealthSensor()V

    .line 72
    return-void
.end method

.method public updateDeviceConnectionStatus()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->unregisterHealthSensor()V

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->registerHealthSensor()V

    .line 65
    return-void
.end method
