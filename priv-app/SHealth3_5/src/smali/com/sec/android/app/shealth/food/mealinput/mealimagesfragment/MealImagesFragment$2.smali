.class Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;
.super Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;
.source "MealImagesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickAction(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 129
    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "gallery button clicked"

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->canAddMoreImages()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$100(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 132
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/app/Activity;)V

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$300(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ADD_PHOTO_BY_ALBUM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 136
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.PICK"

    sget-object v4, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 137
    .local v2, "mMultiIntent":Landroid/content/Intent;
    const-string v3, "image/*"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/app/IFoodConfigurationHolder;

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/app/IFoodConfigurationHolder;->getConfiguration()Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;

    move-result-object v1

    .line 140
    .local v1, "foodConfiguration":Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;
    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;->isMultiSelectedFromGalleryEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 141
    const-string/jumbo v3, "pick-max-item"

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$600(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getImagesCount()I

    move-result v4

    rsub-int/lit8 v4, v4, 0x9

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 143
    const-string/jumbo v3, "multi-pick"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 145
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    .end local v1    # "foodConfiguration":Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;
    .end local v2    # "mMultiIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->showGalleryDisabledPopup()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$700(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V

    goto :goto_0

    .line 150
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->showMaxImagesToast()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$500(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V

    goto :goto_0
.end method
