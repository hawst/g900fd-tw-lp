.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$AutoFocusRunnable;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AutoFocusRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$AutoFocusRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;

    .prologue
    .line 507
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$AutoFocusRunnable;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$AutoFocusRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$AutoFocusRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$AutoFocusRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusCallback:Landroid/hardware/Camera$AutoFocusCallback;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$2200(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera$AutoFocusCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 513
    :cond_0
    return-void
.end method
