.class public Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
.super Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
.source "MealItemData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_UNIT:I = 0x1d4c1


# instance fields
.field private amount:F

.field private foodInfoId:J

.field private mealId:J

.field private unit:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 12
    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    .line 13
    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    .line 14
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    .line 15
    const v0, 0x1d4c1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->unit:I

    .line 35
    return-void
.end method

.method public constructor <init>(JJJF)V
    .locals 4
    .param p1, "id"    # J
    .param p3, "mealId"    # J
    .param p5, "foodInfoId"    # J
    .param p7, "amount"    # F

    .prologue
    const-wide/16 v2, -0x1

    const v1, 0x1d4c1

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(J)V

    .line 12
    iput-wide v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    .line 13
    iput-wide v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    .line 14
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    .line 15
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->unit:I

    .line 27
    iput-wide p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    .line 28
    iput-wide p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    .line 29
    iput p7, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    .line 30
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->unit:I

    .line 31
    return-void
.end method

.method public constructor <init>(JJJFIJJI)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "mealId"    # J
    .param p5, "foodInfoId"    # J
    .param p7, "amount"    # F
    .param p8, "unit"    # I
    .param p9, "createTime"    # J
    .param p11, "updateTime"    # J
    .param p13, "timeZone"    # I

    .prologue
    .line 18
    move-object v1, p0

    move-wide v2, p1

    move-wide/from16 v4, p9

    move-wide/from16 v6, p11

    move/from16 v8, p13

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(JJJI)V

    .line 12
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    .line 13
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    .line 14
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    .line 15
    const v1, 0x1d4c1

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->unit:I

    .line 19
    iput-wide p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    .line 20
    iput-wide p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    .line 21
    move/from16 v0, p7

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    .line 22
    move/from16 v0, p8

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->unit:I

    .line 23
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    if-ne p0, p1, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v1

    .line 116
    :cond_1
    if-eqz p1, :cond_2

    instance-of v3, p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    if-nez v3, :cond_3

    :cond_2
    move v1, v2

    .line 117
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 119
    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 120
    .local v0, "that":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    iget v3, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    iget v4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 121
    :cond_4
    iget-wide v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    iget-wide v5, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_5

    move v1, v2

    .line 122
    goto :goto_0

    .line 124
    :cond_5
    iget-wide v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    iget-wide v5, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_6

    move v1, v2

    .line 125
    goto :goto_0

    .line 127
    :cond_6
    iget v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->unit:I

    iget v4, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->unit:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 128
    goto :goto_0
.end method

.method public getAmount()F
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    return v0
.end method

.method public getFoodInfoId()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    return-wide v0
.end method

.method public getMealId()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    return-wide v0
.end method

.method public getUnit()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->unit:I

    return v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 136
    iget-wide v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    iget-wide v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    ushr-long/2addr v3, v6

    xor-long/2addr v1, v3

    long-to-int v0, v1

    .line 137
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    iget-wide v4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 138
    mul-int/lit8 v2, v0, 0x1f

    iget v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    :goto_0
    add-int v0, v2, v1

    .line 139
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->unit:I

    add-int v0, v1, v2

    .line 140
    return v0

    .line 138
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAmount(F)V
    .locals 0
    .param p1, "amount"    # F

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    .line 65
    return-void
.end method

.method public setFoodInfoId(J)V
    .locals 0
    .param p1, "foodInfoId"    # J

    .prologue
    .line 44
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    .line 45
    return-void
.end method

.method public setMealId(J)V
    .locals 0
    .param p1, "mealId"    # J

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    .line 55
    return-void
.end method

.method public setUnit(I)V
    .locals 0
    .param p1, "unit"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->unit:I

    .line 75
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 81
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->mealId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 82
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->foodInfoId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 83
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->amount:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 84
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->unit:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getCreateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUpdateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getTimeZone()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    return-void
.end method
