.class public interface abstract Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;
.super Ljava/lang/Object;
.source "IFoodConfiguration.java"


# virtual methods
.method public abstract isAddFoodEnabled()Z
.end method

.method public abstract isBarcodeSearchEnabled()Z
.end method

.method public abstract isEditSelectedItemEnabled()Z
.end method

.method public abstract isHorizontalViewEnabled()Z
.end method

.method public abstract isMultiSelectedFromGalleryEnabled()Z
.end method

.method public abstract isMyMealEnabled()Z
.end method

.method public abstract isVoiceSearchEnabled()Z
.end method
