.class Lcom/sec/android/app/shealth/home/HomeActivity$13;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/HomeActivity;->showDevicePopup(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

.field final synthetic val$deviceName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1687
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$13;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$13;->val$deviceName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 5
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 1694
    const v0, 0x7f0804ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$13;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mActivityContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$300(Lcom/sec/android/app/shealth/home/HomeActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09090d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/HomeActivity$13;->val$deviceName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1696
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$13;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    const v0, 0x7f0804ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mDoNotShowAgain:Landroid/widget/CheckBox;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2402(Lcom/sec/android/app/shealth/home/HomeActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 1697
    const v0, 0x7f0804ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/home/HomeActivity$13$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$13$1;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity$13;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1708
    return-void
.end method
