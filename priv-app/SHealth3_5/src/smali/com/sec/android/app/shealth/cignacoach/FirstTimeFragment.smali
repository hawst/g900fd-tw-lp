.class public Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "FirstTimeFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mCignaFirstLoadingLayout:Landroid/widget/RelativeLayout;

.field private mCignaFirstMessageLayout:Landroid/widget/LinearLayout;

.field private mIsScorePresent:Z

.field private mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

.field private rootView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mIsScorePresent:Z

    .line 185
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mIsScorePresent:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mIsScorePresent:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstLoadingLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstMessageLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->updateIntroMessage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->refreshFragmentFocusables()V

    return-void
.end method

.method private moreButtonAction(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 137
    const/4 v0, 0x3

    .line 138
    .local v0, "item_count":I
    new-array v1, v0, [Ljava/lang/String;

    .line 139
    .local v1, "items":[Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090c6d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 140
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090037

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 141
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090039

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 144
    return-void
.end method

.method private updateIntroMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "introMsg"    # Ljava/lang/String;

    .prologue
    .line 165
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->rootView:Landroid/view/View;

    const v2, 0x7f080176

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 166
    .local v0, "firstMsg":Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 168
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVerticalScrollBarEnabled(Z)V

    .line 169
    new-instance v1, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v1}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 170
    invoke-virtual {v0}, Landroid/widget/TextView;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxHeight(I)V

    .line 171
    return-void
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 148
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 151
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 152
    instance-of v1, v0, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;

    if-eqz v1, :cond_0

    .line 153
    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->isMenuShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 154
    const v1, 0x7f100007

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 162
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 48
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->setHasOptionsMenu(Z)V

    .line 50
    const v3, 0x7f03004b

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->rootView:Landroid/view/View;

    .line 51
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->rootView:Landroid/view/View;

    const v4, 0x7f080173

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstLoadingLayout:Landroid/widget/RelativeLayout;

    .line 52
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->rootView:Landroid/view/View;

    const v4, 0x7f080174

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstMessageLayout:Landroid/widget/LinearLayout;

    .line 54
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->getCignaIntroMsg()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "introMsg":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FirstTimeFragment isFirstTimeLoadingComplete: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->isFirstLoadingComplete()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->isFirstLoadingComplete()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 58
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstLoadingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 59
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 61
    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;-><init>(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)V

    new-array v4, v6, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 70
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstMessageLayout:Landroid/widget/LinearLayout;

    const v4, 0x7f080177

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 71
    .local v2, "startBtn":Landroid/widget/Button;
    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstMessageLayout:Landroid/widget/LinearLayout;

    const v4, 0x7f080178

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 85
    .local v1, "laScoreBtn":Landroid/widget/Button;
    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->rootView:Landroid/view/View;

    return-object v3

    .line 64
    .end local v1    # "laScoreBtn":Landroid/widget/Button;
    .end local v2    # "startBtn":Landroid/widget/Button;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstLoadingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 65
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 67
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->updateIntroMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    .line 183
    :cond_0
    return-void
.end method
