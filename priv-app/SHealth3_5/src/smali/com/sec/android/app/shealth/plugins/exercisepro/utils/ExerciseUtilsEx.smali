.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;
.super Ljava/lang/Object;
.source "ExerciseUtilsEx.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Updateitem(Landroid/content/Context;JJIJJJFFILjava/lang/String;I)J
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "exercise_id"    # J
    .param p3, "exerciseInfoId"    # J
    .param p5, "exerciseType"    # I
    .param p6, "startTime"    # J
    .param p8, "endTime"    # J
    .param p10, "millisecond"    # J
    .param p12, "kcal"    # F
    .param p13, "distance"    # F
    .param p14, "cadence"    # I
    .param p15, "comment"    # Ljava/lang/String;
    .param p16, "timeZone"    # I

    .prologue
    .line 49
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 50
    .local v6, "values":Landroid/content/ContentValues;
    const-wide/16 v7, 0x2710

    cmp-long v7, p3, v7

    if-gez v7, :cond_0

    .line 51
    const-wide/16 p3, 0x2af9

    .line 52
    :cond_0
    const-string v7, "exercise_info__id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 53
    if-lez p5, :cond_1

    .line 54
    const-string v7, "exercise_type"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 55
    :cond_1
    const-string v7, "create_time"

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 56
    const-string/jumbo v7, "start_time"

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 57
    const-string v7, "end_time"

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 58
    const-string v7, "duration_millisecond"

    invoke-static/range {p10 .. p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 59
    const-string/jumbo v7, "total_calorie"

    move/from16 v0, p12

    float-to-int v8, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 60
    const-string v7, "distance"

    invoke-static/range {p13 .. p13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 61
    const-string v7, "cadence"

    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 62
    const-string v7, "comment"

    move-object/from16 v0, p15

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 65
    .local v5, "selectionClause":Ljava/lang/String;
    const-wide/16 v3, 0x0

    .line 67
    .local v3, "result":J
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v6, v5, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v7

    int-to-long v3, v7

    .line 75
    .end local v3    # "result":J
    .end local v5    # "selectionClause":Ljava/lang/String;
    :goto_0
    return-wide v3

    .line 68
    .restart local v3    # "result":J
    .restart local v5    # "selectionClause":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 69
    .local v2, "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    :try_start_2
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insert exception -"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 70
    const-wide/16 v3, -0x1

    goto :goto_0

    .line 73
    .end local v2    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    .end local v3    # "result":J
    .end local v5    # "selectionClause":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 74
    .local v1, "e":Ljava/lang/Exception;
    const-string v7, "Exception"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "e.printStackTrace() = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const-wide/16 v3, -0x1

    goto :goto_0
.end method

.method public static clearExerciseData(Landroid/content/Context;)I
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 290
    const/4 v0, 0x0

    .line 291
    .local v0, "rowsDeleted":I
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 293
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "exercise_type != 20003"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 295
    :cond_0
    return v0
.end method

.method public static clearTempData(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 299
    const/4 v0, 0x0

    .line 300
    .local v0, "selectionClause":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exercise__id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 301
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 302
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exercise__id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 303
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 304
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exercise__id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 305
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 306
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 307
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 308
    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealtimeExerciseId(J)V

    .line 309
    return-void
.end method

.method public static deleteExerciseDataForDay(Landroid/content/Context;J)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "time"    # J

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 432
    const/4 v2, 0x0

    .line 435
    .local v2, "rowsDeleted":I
    const-string/jumbo v3, "start_time>=? AND start_time<=? AND end_time>? AND exercise_type != ?"

    .line 436
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v4, 0x4

    new-array v1, v4, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v1, v6

    const-string v4, ""

    aput-object v4, v1, v7

    const-string v4, ""

    aput-object v4, v1, v8

    const-string v4, ""

    aput-object v4, v1, v9

    .line 438
    .local v1, "mSelectionArgs":[Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v6

    .line 439
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v7

    .line 440
    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v8

    .line 441
    const/16 v4, 0x4e23

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v9

    .line 442
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 443
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.shealth.command.deletetotalstep"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 444
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "selectedDate"

    invoke-virtual {v0, v4, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 445
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 446
    return v2
.end method

.method public static deleteItem(Landroid/content/Context;J)J
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "rowId"    # J

    .prologue
    .line 243
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 246
    .local v2, "selectionClause":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    int-to-long v0, v3

    .line 247
    .local v0, "rowsDeleted":J
    return-wide v0
.end method

.method public static getDistanceFromLatLngInMeter(DDDD)D
    .locals 15
    .param p0, "lat1"    # D
    .param p2, "lng1"    # D
    .param p4, "lat2"    # D
    .param p6, "lng2"    # D

    .prologue
    .line 450
    const/16 v0, 0x18e3

    .line 451
    .local v0, "R":I
    sub-double v9, p0, p4

    invoke-static {v9, v10}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v5

    .line 452
    .local v5, "latDistance":D
    sub-double v9, p2, p6

    invoke-static {v9, v10}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v7

    .line 454
    .local v7, "lngDistance":D
    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    div-double v9, v5, v9

    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    move-result-wide v9

    const-wide/high16 v11, 0x4000000000000000L    # 2.0

    div-double v11, v5, v11

    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    move-result-wide v11

    mul-double/2addr v9, v11

    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    move-result-wide v11

    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    mul-double/2addr v11, v13

    const-wide/high16 v13, 0x4000000000000000L    # 2.0

    div-double v13, v7, v13

    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    mul-double/2addr v11, v13

    const-wide/high16 v13, 0x4000000000000000L    # 2.0

    div-double v13, v7, v13

    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    mul-double/2addr v11, v13

    add-double v1, v9, v11

    .line 458
    .local v1, "a":D
    const/16 v9, 0x31c6

    int-to-double v9, v9

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v11

    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v13, v1

    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v13

    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v11

    mul-double v3, v9, v11

    .line 459
    .local v3, "distanceInKm":D
    const-wide v9, 0x408f400000000000L    # 1000.0

    mul-double/2addr v9, v3

    return-wide v9
.end method

.method public static getExerciseData(Landroid/content/Context;J)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    .locals 24
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "rowid"    # J

    .prologue
    .line 254
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 255
    .local v5, "selectionClause":Ljava/lang/String;
    const/16 v22, 0x0

    .line 256
    .local v22, "cursor":Landroid/database/Cursor;
    const/16 v23, 0x0

    .line 258
    .local v23, "exercise":Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 259
    if-eqz v22, :cond_2

    .line 260
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 261
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    .line 263
    new-instance v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    const-string v2, "duration_millisecond"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    const-string/jumbo v2, "total_calorie"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-float v11, v2

    const-string v2, "distance"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v12

    const-string v2, "cadence"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v2, "exercise_info__id"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    const-string v2, "exercise_type"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    const-string/jumbo v2, "start_time"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    const-string v2, "end_time"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v19

    const-string v2, "comment"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-wide/from16 v7, p1

    invoke-direct/range {v6 .. v21}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;-><init>(JJFFIJIJJLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    .end local v23    # "exercise":Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    .local v6, "exercise":Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    :goto_0
    if-eqz v22, :cond_0

    .line 278
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 280
    :cond_0
    return-object v6

    .line 277
    .end local v6    # "exercise":Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    .restart local v23    # "exercise":Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    :catchall_0
    move-exception v2

    if-eqz v22, :cond_1

    .line 278
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v2

    :cond_2
    move-object/from16 v6, v23

    .end local v23    # "exercise":Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    .restart local v6    # "exercise":Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    goto :goto_0
.end method

.method public static insertExerciseActivityItem(Landroid/content/Context;JJ)J
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "exercise_id"    # J
    .param p3, "startTime"    # J

    .prologue
    .line 80
    const-wide/16 v8, -0x1

    .line 81
    .local v8, "exercise_activity_id":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 82
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v6, 0x0

    .line 84
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 85
    if-eqz v6, :cond_0

    .line 86
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 87
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 88
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 92
    :cond_0
    if-eqz v6, :cond_1

    .line 93
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 95
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-lez v0, :cond_3

    move-wide v10, v8

    .line 106
    .end local v8    # "exercise_activity_id":J
    .local v10, "exercise_activity_id":J
    :goto_0
    return-wide v10

    .line 92
    .end local v10    # "exercise_activity_id":J
    .restart local v8    # "exercise_activity_id":J
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 93
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 98
    :cond_3
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 99
    .local v12, "values":Landroid/content/ContentValues;
    const-string v0, "exercise__id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 100
    const-string v0, "duration_min"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 101
    const-string v0, "duration_millisecond"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 102
    const-string/jumbo v0, "start_time"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 103
    const-string v0, "end_time"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 104
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v7

    .line 105
    .local v7, "exerciseActivityURI":Landroid/net/Uri;
    invoke-static {v7}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    move-wide v10, v8

    .line 106
    .end local v8    # "exercise_activity_id":J
    .restart local v10    # "exercise_activity_id":J
    goto :goto_0
.end method

.method public static updateExerciseActivityItem(Landroid/content/Context;JJJLcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)J
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "exercise_id"    # J
    .param p3, "startTime"    # J
    .param p5, "millisecond"    # J
    .param p7, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .prologue
    .line 112
    if-eqz p7, :cond_0

    move-object/from16 v0, p7

    iget-object v8, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    if-nez v8, :cond_1

    :cond_0
    const-wide/16 v4, -0x1

    .line 145
    :goto_0
    return-wide v4

    .line 113
    :cond_1
    const-wide/16 v8, 0x0

    cmp-long v8, p5, v8

    if-gtz v8, :cond_2

    const-wide/16 v4, -0x1

    goto :goto_0

    .line 114
    :cond_2
    const-wide/32 v8, 0xea60

    div-long v1, p5, v8

    .line 115
    .local v1, "duration_min":J
    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-lez v8, :cond_3

    const-wide/16 v8, 0x5a0

    cmp-long v8, v1, v8

    if-ltz v8, :cond_4

    :cond_3
    const-wide/16 v4, -0x1

    goto :goto_0

    .line 117
    :cond_4
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 118
    .local v7, "values":Landroid/content/ContentValues;
    const-string v8, "exercise__id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 119
    const-string v8, "calorie"

    move-object/from16 v0, p7

    iget-object v9, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    float-to-double v9, v9

    invoke-static {v9, v10}, Ljava/lang/Math;->floor(D)D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 120
    const-string v8, "cadence"

    move-object/from16 v0, p7

    iget-object v9, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->stepCount:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 121
    const-string v8, "cadence_type"

    const/16 v9, 0x7532

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 122
    const-string/jumbo v8, "mean_speed_per_hour"

    move-object/from16 v0, p7

    iget-object v9, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averageSpeed:F

    const/high16 v10, 0x45610000    # 3600.0f

    mul-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 123
    const-string/jumbo v8, "max_speed_per_hour"

    move-object/from16 v0, p7

    iget-object v9, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxSpeed:F

    const/high16 v10, 0x45610000    # 3600.0f

    mul-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 124
    move-object/from16 v0, p7

    iget-object v8, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v8, v8, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    const v9, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v8, v8, v9

    if-eqz v8, :cond_5

    .line 125
    const-string v8, "incline_distance"

    move-object/from16 v0, p7

    iget-object v9, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 126
    :cond_5
    move-object/from16 v0, p7

    iget-object v8, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v8, v8, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    const v9, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v8, v8, v9

    if-eqz v8, :cond_6

    .line 127
    const-string v8, "decline_distance"

    move-object/from16 v0, p7

    iget-object v9, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 128
    :cond_6
    const-string v8, "distance"

    move-object/from16 v0, p7

    iget-object v9, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 129
    move-object/from16 v0, p7

    iget-object v8, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v8, v8, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxAltitude:F

    const v9, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v8, v8, v9

    if-eqz v8, :cond_7

    .line 130
    const-string/jumbo v8, "max_altitude"

    move-object/from16 v0, p7

    iget-object v9, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxAltitude:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 131
    :cond_7
    move-object/from16 v0, p7

    iget-object v8, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v8, v8, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->minAltitude:F

    const v9, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v8, v8, v9

    if-eqz v8, :cond_8

    .line 132
    const-string/jumbo v8, "min_altitude"

    move-object/from16 v0, p7

    iget-object v9, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v9, v9, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->minAltitude:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 133
    :cond_8
    const-string/jumbo v8, "start_time"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 134
    const-string v8, "end_time"

    move-object/from16 v0, p7

    iget-wide v9, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 136
    const-string v8, "duration_min"

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 137
    const-string v8, "duration_millisecond"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 140
    :try_start_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "exercise__id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 141
    .local v6, "selectionClause":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v7, v6, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    int-to-long v4, v8

    .line 142
    .local v4, "result":J
    goto/16 :goto_0

    .line 143
    .end local v4    # "result":J
    .end local v6    # "selectionClause":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 144
    .local v3, "e":Ljava/lang/Exception;
    const-string v8, "Exception"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "e.printStackTrace() = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const-wide/16 v4, -0x1

    goto/16 :goto_0
.end method

.method public static updateRealtimeItem(Landroid/content/Context;JJIJJJFFILjava/lang/String;I)J
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "exercise_id"    # J
    .param p3, "exerciseInfoId"    # J
    .param p5, "exerciseType"    # I
    .param p6, "startTime"    # J
    .param p8, "endTime"    # J
    .param p10, "DURATION_MILLISECOND"    # J
    .param p12, "kcal"    # F
    .param p13, "distance"    # F
    .param p14, "cadence"    # I
    .param p15, "comment"    # Ljava/lang/String;
    .param p16, "timeZone"    # I

    .prologue
    .line 171
    const-wide/16 v7, 0x2710

    cmp-long v7, p3, v7

    if-gez v7, :cond_0

    .line 172
    const-wide/16 p3, 0x2af9

    .line 174
    :cond_0
    invoke-static/range {p0 .. p16}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->Updateitem(Landroid/content/Context;JJIJJJFFILjava/lang/String;I)J

    move-result-wide v1

    .line 175
    .local v1, "id":J
    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-ltz v7, :cond_1

    .line 176
    const/4 v5, 0x0

    .line 177
    .local v5, "values":Landroid/content/ContentValues;
    const/4 v4, 0x0

    .line 179
    .local v4, "selectionClause":Ljava/lang/String;
    :try_start_0
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 180
    .end local v5    # "values":Landroid/content/ContentValues;
    .local v6, "values":Landroid/content/ContentValues;
    :try_start_1
    const-string/jumbo v7, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 181
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "exercise__id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 182
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v6, v4, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_e

    .line 189
    :goto_0
    :try_start_2
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 190
    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :try_start_3
    const-string/jumbo v7, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 191
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "exercise__id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 192
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v5, v4, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_3 .. :try_end_3} :catch_d
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_c

    move-object v6, v5

    .line 200
    .end local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "values":Landroid/content/ContentValues;
    :goto_1
    :try_start_4
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V
    :try_end_4
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5

    .line 201
    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :try_start_5
    const-string/jumbo v7, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 202
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "exercise__id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 203
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v5, v4, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_5 .. :try_end_5} :catch_b
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_a

    move-object v6, v5

    .line 211
    .end local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "values":Landroid/content/ContentValues;
    :goto_2
    :try_start_6
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V
    :try_end_6
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7

    .line 212
    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :try_start_7
    const-string/jumbo v7, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 213
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "exercise__id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 214
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v5, v4, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_7
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_7 .. :try_end_7} :catch_9
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8

    .line 226
    .end local v1    # "id":J
    .end local v4    # "selectionClause":Ljava/lang/String;
    .end local v5    # "values":Landroid/content/ContentValues;
    :goto_3
    return-wide v1

    .line 183
    .restart local v1    # "id":J
    .restart local v4    # "selectionClause":Ljava/lang/String;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v3

    .line 184
    .local v3, "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    :goto_4
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insert exception -"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v5

    .line 187
    .end local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "values":Landroid/content/ContentValues;
    goto/16 :goto_0

    .line 185
    .end local v3    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/Exception;
    :goto_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v6, v5

    .end local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "values":Landroid/content/ContentValues;
    goto/16 :goto_0

    .line 193
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v3

    move-object v5, v6

    .line 194
    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v3    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :goto_6
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insert exception -"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v5

    .line 197
    .end local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "values":Landroid/content/ContentValues;
    goto/16 :goto_1

    .line 195
    .end local v3    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    :catch_3
    move-exception v0

    move-object v5, v6

    .line 196
    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v0    # "e":Ljava/lang/Exception;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :goto_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v6, v5

    .end local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "values":Landroid/content/ContentValues;
    goto/16 :goto_1

    .line 204
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v3

    move-object v5, v6

    .line 205
    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v3    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :goto_8
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insert exception -"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v5

    .line 208
    .end local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "values":Landroid/content/ContentValues;
    goto/16 :goto_2

    .line 206
    .end local v3    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    :catch_5
    move-exception v0

    move-object v5, v6

    .line 207
    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v0    # "e":Ljava/lang/Exception;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :goto_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v6, v5

    .end local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "values":Landroid/content/ContentValues;
    goto/16 :goto_2

    .line 215
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_6
    move-exception v3

    move-object v5, v6

    .line 216
    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v3    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :goto_a
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insert exception -"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 217
    .end local v3    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    .end local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "values":Landroid/content/ContentValues;
    :catch_7
    move-exception v0

    move-object v5, v6

    .line 218
    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v0    # "e":Ljava/lang/Exception;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :goto_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 226
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v4    # "selectionClause":Ljava/lang/String;
    .end local v5    # "values":Landroid/content/ContentValues;
    :cond_1
    const-wide/16 v1, -0x1

    goto/16 :goto_3

    .line 217
    .restart local v4    # "selectionClause":Ljava/lang/String;
    .restart local v5    # "values":Landroid/content/ContentValues;
    :catch_8
    move-exception v0

    goto :goto_b

    .line 215
    :catch_9
    move-exception v3

    goto :goto_a

    .line 206
    :catch_a
    move-exception v0

    goto :goto_9

    .line 204
    :catch_b
    move-exception v3

    goto :goto_8

    .line 195
    :catch_c
    move-exception v0

    goto :goto_7

    .line 193
    :catch_d
    move-exception v3

    goto/16 :goto_6

    .line 185
    .end local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "values":Landroid/content/ContentValues;
    :catch_e
    move-exception v0

    move-object v5, v6

    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v5    # "values":Landroid/content/ContentValues;
    goto/16 :goto_5

    .line 183
    .end local v5    # "values":Landroid/content/ContentValues;
    .restart local v6    # "values":Landroid/content/ContentValues;
    :catch_f
    move-exception v3

    move-object v5, v6

    .end local v6    # "values":Landroid/content/ContentValues;
    .restart local v5    # "values":Landroid/content/ContentValues;
    goto/16 :goto_4
.end method

.method public static updateRealtimeItem(Landroid/content/Context;JLjava/lang/String;)J
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "exercise_id"    # J
    .param p3, "comment"    # Ljava/lang/String;

    .prologue
    const-wide/16 v6, -0x1

    .line 150
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 151
    .local v5, "values":Landroid/content/ContentValues;
    const-string v8, "comment"

    invoke-virtual {v5, v8, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string/jumbo v8, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 154
    :try_start_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "_id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 155
    .local v4, "selectionClause":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 157
    .local v2, "result":J
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v5, v4, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    int-to-long v2, v6

    .line 165
    .end local v2    # "result":J
    .end local v4    # "selectionClause":Ljava/lang/String;
    :goto_0
    return-wide v2

    .line 158
    .restart local v2    # "result":J
    .restart local v4    # "selectionClause":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 159
    .local v1, "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    :try_start_2
    sget-object v8, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "insert exception -"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-wide v2, v6

    .line 160
    goto :goto_0

    .line 163
    .end local v1    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    .end local v2    # "result":J
    .end local v4    # "selectionClause":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/Exception;
    const-string v8, "Exception"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "e.printStackTrace() = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v2, v6

    .line 165
    goto :goto_0
.end method
