.class public Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;
.super Landroid/os/AsyncTask;
.source "TrackerDataLoadingAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask$OnFinishListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mOnFinishListener:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask$OnFinishListener;

.field private mProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->mProgressBar:Landroid/widget/ProgressBar;

    .line 9
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->mOnFinishListener:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask$OnFinishListener;

    .line 48
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 7
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/16 v6, 0x64

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 13
    const/4 v0, 0x0

    .line 15
    .local v0, "progress":I
    const/4 v0, 0x0

    :goto_0
    if-gt v0, v6, :cond_0

    .line 16
    const/4 v3, 0x1

    :try_start_0
    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 17
    const-wide/16 v3, 0xa

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 19
    :catch_0
    move-exception v3

    .line 23
    :cond_0
    if-lt v0, v6, :cond_1

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 6
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->mOnFinishListener:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask$OnFinishListener;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->mOnFinishListener:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask$OnFinishListener;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask$OnFinishListener;->onFinish(Z)V

    .line 38
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 6
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 31
    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 6
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method public setOnFinishListener(Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask$OnFinishListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask$OnFinishListener;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->mOnFinishListener:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask$OnFinishListener;

    .line 46
    return-void
.end method

.method public setProgressBar(Landroid/widget/ProgressBar;)V
    .locals 0
    .param p1, "view"    # Landroid/widget/ProgressBar;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->mProgressBar:Landroid/widget/ProgressBar;

    .line 42
    return-void
.end method
