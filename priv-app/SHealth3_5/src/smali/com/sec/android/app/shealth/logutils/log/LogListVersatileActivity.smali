.class public abstract Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.source "LogListVersatileActivity.java"


# static fields
.field private static final DELETE_ID_TAG:Ljava/lang/String; = "_id="

.field private static final EMPTY_STRING:Ljava/lang/String; = ""

.field private static final SHARING_DATA_SEPARATOR:Ljava/lang/Character;

.field private static final TAG:Ljava/lang/String;

.field private static final TAG_ID_SEPARATOR:Ljava/lang/String; = "_"


# instance fields
.field private mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

.field private mCurrentFilter:Lcom/sec/android/app/shealth/logutils/log/LogFilter;

.field private mLogAdapter:Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->SHARING_DATA_SEPARATOR:Ljava/lang/Character;

    .line 45
    const-class v0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->ALL:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;-><init>(Lcom/sec/android/app/shealth/logutils/log/LogFilter;)V

    .line 53
    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/app/shealth/logutils/log/LogFilter;)V
    .locals 0
    .param p1, "filter"    # Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCurrentFilter:Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    .line 57
    return-void
.end method

.method private createAdapter()V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCurrentFilter:Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getGroupCursor(Lcom/sec/android/app/shealth/logutils/log/LogFilter;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCursor:Landroid/database/Cursor;

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "mCursor - returned LogListConcreteDelegate.getGroupCursor can not be NULL"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mLogAdapter:Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;

    .line 107
    return-void
.end method

.method protected static getFilterDisplayNamesAsStringArrayList([Lcom/sec/android/app/shealth/logutils/log/LogFilter;Landroid/content/res/Resources;)Ljava/util/ArrayList;
    .locals 6
    .param p0, "values"    # [Lcom/sec/android/app/shealth/logutils/log/LogFilter;
    .param p1, "resources"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/sec/android/app/shealth/logutils/log/LogFilter;",
            "Landroid/content/res/Resources;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 214
    .local v4, "names":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, p0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/logutils/log/LogFilter;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 215
    .local v1, "filter":Lcom/sec/android/app/shealth/logutils/log/LogFilter;
    invoke-interface {v1}, Lcom/sec/android/app/shealth/logutils/log/LogFilter;->getDisplayNameId()I

    move-result v5

    invoke-virtual {p1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 217
    .end local v1    # "filter":Lcom/sec/android/app/shealth/logutils/log/LogFilter;
    :cond_0
    return-object v4
.end method

.method private setActionBarNormalMode()V
    .locals 3

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/logutils/R$drawable;->tw_action_bar_sub_tab_bg_01_holo_light:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 304
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/logutils/R$string;->log:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 305
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 306
    return-void
.end method


# virtual methods
.method protected applyFilter(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCurrentFilter:Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/logutils/log/LogFilter;->getValues()[Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getFilterRange()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCurrentFilter:Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCurrentFilter:Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getGroupCursor(Lcom/sec/android/app/shealth/logutils/log/LogFilter;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCursor:Landroid/database/Cursor;

    .line 144
    return-void
.end method

.method protected abstract createConcreteDelegate()Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;
.end method

.method protected delete()V
    .locals 0

    .prologue
    .line 254
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->delete()V

    .line 255
    return-void
.end method

.method protected getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mLogAdapter:Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;

    return-object v0
.end method

.method protected final getColumnNameForMemo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    const-string/jumbo v0, "memo_text"

    return-object v0
.end method

.method protected getContextMenuHeader(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 126
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getDeleteList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    .local p1, "selectedTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getDeleteList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getFilterRange()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCurrentFilter:Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/logutils/log/LogFilter;->getValues()[Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getFilterDisplayNamesAsStringArrayList([Lcom/sec/android/app/shealth/logutils/log/LogFilter;Landroid/content/res/Resources;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getFilterType(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCurrentFilter:Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/logutils/log/LogFilter;->getValues()[Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-interface {v1}, Lcom/sec/android/app/shealth/logutils/log/LogFilter;->getDisplayNameId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getIdFromTag(Ljava/lang/String;)Ljava/lang/Long;
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 235
    const-string v1, "_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, "tagSplit":[Ljava/lang/String;
    array-length v1, v0

    if-le v1, v2, :cond_0

    aget-object p1, v0, v2

    .end local p1    # "tag":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    return-object v1
.end method

.method protected getLogDataTypeURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getNoDataImageResource()I
.end method

.method protected getSelectedLogDataForSharing()Ljava/lang/String;
    .locals 4

    .prologue
    .line 259
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .local v1, "itemsIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 261
    .local v2, "tag":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getIdFromTag(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 263
    .end local v2    # "tag":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getShareViaDescriptionsByIds(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method protected getShareViaDescriptionsByIds(Ljava/util/Collection;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 272
    .local p1, "itemsIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 273
    const-string v4, ""

    .line 299
    :goto_0
    return-object v4

    .line 275
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 276
    .local v2, "oldPosition":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_1

    .line 277
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Group Cursor should have items"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 279
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 283
    .local v3, "stringBuilder":Ljava/lang/StringBuilder;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    iget-object v5, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getChildCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 284
    .local v0, "childCursor":Landroid/database/Cursor;
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_4

    .line 285
    :cond_3
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Children Cursor should have items"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 288
    :cond_4
    const-string v4, "_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 289
    .local v1, "itemId":Ljava/lang/Long;
    invoke-interface {p1, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 290
    iget-object v4, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    invoke-virtual {v4, v0, p0}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getShareViaDescription(Landroid/database/Cursor;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    sget-object v4, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->SHARING_DATA_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 293
    :cond_5
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 294
    :cond_6
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 296
    iget-object v4, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 298
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 299
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method protected isMemoVisible(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 65
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 66
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 68
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 69
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 70
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->startActivity(Landroid/content/Intent;)V

    .line 72
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 74
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->createConcreteDelegate()Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->createAdapter()V

    .line 77
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getNoDataImageResource()I

    move-result v1

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->setNoLogImageAndText(II)V

    .line 79
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/logutils/R$menu;->base_log_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 164
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mLogAdapter:Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mLogAdapter:Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->clearData()V

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mLogAdapter:Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;

    .line 98
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onDestroy()V

    .line 99
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 173
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected registerContentObserver()V
    .locals 0

    .prologue
    .line 196
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->registerContentObserver()V

    .line 197
    return-void
.end method

.method public setContentView(I)V
    .locals 5
    .param p1, "layoutResID"    # I

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->setContentView(I)V

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/logutils/R$layout;->log_list_item_divider:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 85
    .local v0, "footer":Landroid/view/View;
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/logutils/R$dimen;->log_list_row_divider_height:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 89
    sget v1, Lcom/sec/android/app/shealth/logutils/R$id;->expandable_listView:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 90
    return-void
.end method

.method protected setUpSelectMode()V
    .locals 3

    .prologue
    .line 310
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->setUpSelectMode()V

    .line 313
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mLogAdapter:Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->getGroupCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 314
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->mLogAdapter:Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->setClickedGroupPosition(IZ)V

    .line 313
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 317
    :cond_0
    return-void
.end method

.method protected showDetailScreen(Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 122
    return-void
.end method

.method protected unregisterContentObserver()V
    .locals 0

    .prologue
    .line 203
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->unregisterContentObserver()V

    .line 204
    return-void
.end method

.method protected updateMemo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "_id"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    .line 138
    return-void
.end method
