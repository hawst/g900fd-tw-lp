.class Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;
.super Ljava/lang/Object;
.source "SelectGoalMissionActivity.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GoalMission"
.end annotation


# instance fields
.field private child:I

.field private grp:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "grpPos"    # I

    .prologue
    .line 819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 821
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->grp:I

    .line 822
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->child:I

    .line 823
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "grpPos"    # I
    .param p2, "childPos"    # I

    .prologue
    .line 812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 814
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->grp:I

    .line 815
    iput p2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->child:I

    .line 816
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 789
    if-ne p0, p1, :cond_1

    .line 806
    :cond_0
    :goto_0
    return v1

    .line 792
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 793
    goto :goto_0

    .line 795
    :cond_2
    instance-of v3, p1, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    if-nez v3, :cond_3

    move v1, v2

    .line 796
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 798
    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    .line 800
    .local v0, "other":Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->child:I

    iget v4, v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->child:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 801
    goto :goto_0

    .line 803
    :cond_4
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->grp:I

    iget v4, v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->grp:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 804
    goto :goto_0
.end method

.method public getChild()I
    .locals 1

    .prologue
    .line 834
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->child:I

    return v0
.end method

.method public getGroup()I
    .locals 1

    .prologue
    .line 829
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->grp:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 777
    const/16 v0, 0x1f

    .line 778
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 779
    .local v1, "result":I
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->child:I

    add-int/lit8 v1, v2, 0x1f

    .line 780
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->grp:I

    add-int v1, v2, v3

    .line 781
    return v1
.end method
