.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$SamsungAccountDialogController;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SamsungAccountDialogController"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 1887
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$15;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1902
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1889
    :pswitch_1
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1892
    new-instance v0, Landroid/content/Intent;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1893
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "activity_name"

    const-string/jumbo v2, "walk_for_life_activity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1894
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    const/16 v2, 0x65

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1895
    const-string/jumbo v1, "samsung account, ok button"

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    goto :goto_0

    .line 1887
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
