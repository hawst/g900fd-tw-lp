.class public Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;
.super Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;
.source "SuggestedListAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMoreDataTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPreLoadSuggestedDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private mPreLoadTotalCount:I

.field private mSuggestedDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p2, "listData":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;>;"
    .local p3, "preLoadData":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;>;"
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mContext:Landroid/content/Context;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mLoadComplete:Z

    .line 39
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    .line 40
    if-eqz p3, :cond_0

    .line 41
    iput-object p3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mPreLoadSuggestedDataList:Ljava/util/List;

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getPreLoadCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mPreLoadTotalCount:I

    .line 44
    :cond_0
    return-void
.end method


# virtual methods
.method public IsNonDivierPosInSection(I)Z
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 280
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getSectionForPosition(I)I

    move-result v2

    .line 281
    .local v2, "section":I
    const/4 v1, 0x0

    .line 282
    .local v1, "lastInSection":I
    const/4 v0, 0x0

    .line 283
    .local v0, "i":I
    const/4 v0, 0x0

    :goto_0
    if-gt v0, v2, :cond_0

    .line 284
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v1, v3

    .line 283
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 287
    :cond_0
    add-int/lit8 v3, v1, -0x1

    if-ne p1, v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_1

    .line 288
    const/4 v3, 0x1

    .line 291
    :goto_1
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method protected bindSectionHeader(Landroid/view/View;IZ)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "displaySectionHeader"    # Z

    .prologue
    const v1, 0x7f080265

    .line 127
    if-eqz p3, :cond_0

    .line 128
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 129
    const v1, 0x7f0801c2

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 131
    .local v0, "sectionTitle":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getSections()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getSectionForPosition(I)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getSections()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getSectionForPosition(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0901fd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 133
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 142
    .end local v0    # "sectionTitle":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public getCount()I
    .locals 4

    .prologue
    .line 48
    const/4 v1, 0x0

    .line 49
    .local v1, "res":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 50
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 51
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v1, v3

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    return v1
.end method

.method public getCurrentView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 146
    move-object v6, p2

    .line 148
    .local v6, "rootView":Landroid/view/View;
    if-nez v6, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f030080

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 156
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f07004f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/view/View;->setBackgroundColor(I)V

    .line 158
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getItem(I)Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    move-result-object v0

    .line 160
    .local v0, "data":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    const v8, 0x7f08026c

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 162
    .local v1, "divider":Landroid/view/View;
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 163
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->IsNonDivierPosInSection(I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 164
    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 171
    :cond_1
    const v8, 0x7f080268

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 172
    .local v3, "iconView":Landroid/widget/ImageView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 174
    const v8, 0x7f080269

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 175
    .local v7, "titleTextView":Landroid/widget/TextView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getTitleString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    const v8, 0x7f08026a

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 182
    .local v2, "extraStringTextView":Landroid/widget/TextView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getExtraString()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getExtraString()Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 183
    :cond_2
    const/16 v8, 0x8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 192
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0008

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 193
    instance-of v8, v0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    if-eqz v8, :cond_6

    move-object v5, v0

    .line 195
    check-cast v5, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 196
    .local v5, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->isMissionRepeatable()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 197
    const v8, 0x7f080266

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const-string v9, "#D2E4F8"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setBackgroundColor(I)V

    .line 215
    .end local v5    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    :cond_3
    :goto_1
    return-object v6

    .line 185
    :cond_4
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getExtraString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 199
    .restart local v5    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    :cond_5
    const v8, 0x7f080266

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1

    .line 203
    .end local v5    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    :cond_6
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getMissionToComplete()I

    move-result v8

    const v9, 0x1869f

    if-ne v8, v9, :cond_7

    .line 204
    const v8, 0x7f080266

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setBackgroundColor(I)V

    .line 209
    :goto_2
    const v8, 0x7f08026b

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 210
    .local v4, "minFrequencyTxt":Landroid/widget/TextView;
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 211
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getMinFrequency()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 206
    .end local v4    # "minFrequencyTxt":Landroid/widget/TextView;
    :cond_7
    const v8, 0x7f080266

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const-string v9, "#D2E4F8"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_2
.end method

.method public getItem(I)Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 59
    .local v0, "c":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 60
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 61
    if-lt p1, v0, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v3, v0

    if-ge p1, v3, :cond_0

    .line 62
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    sub-int v4, p1, v0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 66
    :goto_1
    return-object v3

    .line 64
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v0, v3

    .line 60
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 66
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getItem(I)Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 71
    int-to-long v0, p1

    return-wide v0
.end method

.method public getPositionForSection(I)I
    .locals 4
    .param p1, "section"    # I

    .prologue
    .line 220
    if-gez p1, :cond_0

    const/4 p1, 0x0

    .line 221
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt p1, v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 p1, v3, -0x1

    .line 222
    :cond_1
    const/4 v0, 0x0

    .line 223
    .local v0, "c":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 224
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 225
    if-ne p1, v1, :cond_2

    .line 230
    .end local v0    # "c":I
    :goto_1
    return v0

    .line 228
    .restart local v0    # "c":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v0, v3

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 230
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getPreLoadCount()I
    .locals 4

    .prologue
    .line 257
    const/4 v0, 0x0

    .line 258
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mPreLoadSuggestedDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 259
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 260
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mPreLoadSuggestedDataList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v0, v3

    .line 259
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 262
    :cond_0
    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 235
    const/4 v0, 0x0

    .line 236
    .local v0, "c":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 237
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 238
    if-lt p1, v0, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v3, v0

    if-ge p1, v3, :cond_0

    .line 243
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 241
    .restart local v1    # "i":I
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v0, v3

    .line 237
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 243
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public bridge synthetic getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getSections()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSections()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 248
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v1, v3, [Ljava/lang/String;

    .line 249
    .local v1, "res":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 250
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 251
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    aput-object v3, v1, v0

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 253
    :cond_0
    return-object v1
.end method

.method public setItemsList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 266
    .local p1, "itemsList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->notifyNoMorePages()V

    .line 267
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->mSuggestedDataList:Ljava/util/List;

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->notifyDataSetChanged()V

    .line 269
    return-void
.end method
