.class public Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
.super Ljava/lang/Exception;
.source "PrintUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/PrintUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnableToPrintException"
.end annotation


# static fields
.field public static final ERR_DIR_NOT_FOUND:I = -0x2

.field public static final ERR_FILE_ERROR:I = -0x4

.field public static final ERR_FILE_NOT_FOUND:I = -0x3

.field public static final ERR_INVALID_PARAMETER:I = -0x1

.field public static final ERR_NONE:I = 0x0

.field public static final ERR_NO_PRINT_ACTIVITY:I = -0x5


# instance fields
.field public errorCode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 307
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 304
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;->errorCode:I

    .line 308
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 312
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 304
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;->errorCode:I

    .line 313
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "detailMessage"    # Ljava/lang/String;
    .param p2, "code"    # I

    .prologue
    .line 317
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 304
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;->errorCode:I

    .line 318
    iput p2, p0, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;->errorCode:I

    .line 319
    return-void
.end method
