.class Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->prepareShareView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 29

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mBackGroundView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-lez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mBackGroundView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-gtz v1, :cond_4

    :cond_0
    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->access$300()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x18

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, -0x3e

    aput v27, v2, v26

    const/16 v26, -0x59cb

    aput v26, v2, v25

    const/16 v25, -0x2a

    aput v25, v2, v24

    const/16 v24, -0x56d6

    aput v24, v2, v23

    const/16 v23, -0x34

    aput v23, v2, v22

    const/16 v22, -0x2ee7

    aput v22, v2, v21

    const/16 v21, -0x5e

    aput v21, v2, v20

    const/16 v20, -0x408b

    aput v20, v2, v19

    const/16 v19, -0x61

    aput v19, v2, v18

    const/16 v18, -0x5

    aput v18, v2, v17

    const/16 v17, -0x6e

    aput v17, v2, v16

    const/16 v16, -0x23

    aput v16, v2, v15

    const/16 v15, -0x38d5

    aput v15, v2, v14

    const/16 v14, -0x5d

    aput v14, v2, v13

    const/16 v13, 0xf3b

    aput v13, v2, v12

    const/16 v12, -0x6886

    aput v12, v2, v11

    const/4 v11, -0x8

    aput v11, v2, v10

    const/16 v10, -0x7fc7

    aput v10, v2, v9

    const/16 v9, -0x39

    aput v9, v2, v8

    const/16 v8, -0x1f

    aput v8, v2, v7

    const/16 v7, 0x4426

    aput v7, v2, v6

    const/16 v6, -0x6cdb

    aput v6, v2, v5

    const/16 v5, -0x2f

    aput v5, v2, v3

    const/16 v3, -0x2098

    aput v3, v2, v1

    const/16 v1, 0x18

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, -0x45

    aput v28, v1, v27

    const/16 v27, -0x59bf

    aput v27, v1, v26

    const/16 v26, -0x5a

    aput v26, v1, v25

    const/16 v25, -0x56b9

    aput v25, v1, v24

    const/16 v24, -0x57

    aput v24, v1, v23

    const/16 v23, -0x2ec7

    aput v23, v1, v22

    const/16 v22, -0x2f

    aput v22, v1, v21

    const/16 v21, -0x40e4

    aput v21, v1, v20

    const/16 v20, -0x41

    aput v20, v1, v19

    const/16 v19, -0x74

    aput v19, v1, v18

    const/16 v18, -0x9

    aput v18, v1, v17

    const/16 v17, -0x4c

    aput v17, v1, v16

    const/16 v16, -0x3883

    aput v16, v1, v15

    const/16 v15, -0x39

    aput v15, v1, v14

    const/16 v14, 0xf55

    aput v14, v1, v13

    const/16 v13, -0x68f1

    aput v13, v1, v12

    const/16 v12, -0x69

    aput v12, v1, v11

    const/16 v11, -0x7fb5

    aput v11, v1, v10

    const/16 v10, -0x80

    aput v10, v1, v9

    const/16 v9, -0x76

    aput v9, v1, v8

    const/16 v8, 0x4445

    aput v8, v1, v7

    const/16 v7, -0x6cbc

    aput v7, v1, v6

    const/16 v6, -0x6d

    aput v6, v1, v5

    const/16 v5, -0x20fb

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_2
    return-void

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mBackGroundView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mBackGroundView:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-eqz v3, :cond_5

    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-le v1, v3, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Canvas;->release()V

    goto :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mBackGroundView:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

    invoke-static {v3, v1}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveShareImageToSdCard(Landroid/content/Context;Landroid/graphics/Bitmap;)Ljava/io/File;

    move-result-object v3

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_6
    :goto_3
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-le v1, v4, :cond_7

    invoke-virtual {v2}, Landroid/graphics/Canvas;->release()V

    :cond_7
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onCacheBuildEnd()V

    :try_start_3
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_0
    move-exception v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->share_image_storage_error:I

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :catch_1
    move-exception v1

    goto/16 :goto_2

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_4
.end method
