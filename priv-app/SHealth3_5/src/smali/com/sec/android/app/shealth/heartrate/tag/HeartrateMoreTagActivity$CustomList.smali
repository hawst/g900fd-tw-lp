.class public Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;
.super Landroid/widget/ArrayAdapter;
.source "HeartrateMoreTagActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CustomList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/app/Activity;

.field private final mTags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;",
            ">;"
        }
    .end annotation
.end field

.field private selText:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;Landroid/app/Activity;Ljava/util/ArrayList;Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V
    .locals 2
    .param p2, "context"    # Landroid/app/Activity;
    .param p4, "selTag"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;",
            ">;",
            "Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;",
            ")V"
        }
    .end annotation

    .prologue
    .line 83
    .local p3, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    .line 84
    const v0, 0x7f030161

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->selText:Ljava/lang/String;

    .line 85
    iput-object p2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->mContext:Landroid/app/Activity;

    .line 86
    iput-object p3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->mTags:Ljava/util/ArrayList;

    .line 87
    if-eqz p4, :cond_0

    .line 88
    iget-object v0, p4, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v0, v1, :cond_1

    .line 89
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p4, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->selText:Ljava/lang/String;

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    iget-object v0, p4, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->selText:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->mContext:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->mTags:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 97
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 98
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v10, 0x7f030160

    const/4 v11, 0x0

    invoke-virtual {v2, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 99
    .local v6, "rowView":Landroid/view/View;
    const v10, 0x7f0805de

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 100
    .local v9, "txtTitle":Landroid/widget/TextView;
    const v10, 0x7f0805f9

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 101
    .local v1, "imageView":Landroid/widget/ImageView;
    const v10, 0x7f0805f6

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    .line 104
    .local v4, "mButton":Landroid/widget/RadioButton;
    :try_start_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v10

    const-string v11, "com.sec.android.app.shealth"

    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/ResourceUtil;

    move-result-object v3

    .line 105
    .local v3, "instance":Lcom/sec/android/app/shealth/common/utils/ResourceUtil;
    const-string/jumbo v10, "tw_btn_radio_selector"

    invoke-virtual {v3, v10}, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 106
    .local v0, "btnDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v4, v0}, Landroid/widget/RadioButton;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    .end local v0    # "btnDrawable":Landroid/graphics/drawable/Drawable;
    .end local v3    # "instance":Lcom/sec/android/app/shealth/common/utils/ResourceUtil;
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->mTags:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 115
    .local v7, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    const-string v8, ""

    .line 116
    .local v8, "tagName":Ljava/lang/String;
    iget-object v10, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v11, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v10, v11, :cond_1

    .line 117
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    iget v11, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget v10, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 119
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    iget v11, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 125
    :goto_1
    invoke-virtual {v4, v7}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    .line 126
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 127
    const v10, 0x7f02051c

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 128
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->selText:Ljava/lang/String;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->selText:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_0

    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->selText:Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 129
    const/4 v10, 0x1

    invoke-virtual {v4, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 131
    :cond_0
    new-instance v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;

    invoke-direct {v10, p0, p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;I)V

    invoke-virtual {v6, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    return-object v6

    .line 109
    .end local v7    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .end local v8    # "tagName":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 111
    .local v5, "nne":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v5}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 121
    .end local v5    # "nne":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v7    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .restart local v8    # "tagName":Ljava/lang/String;
    :cond_1
    iget-object v10, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget v10, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 123
    iget-object v8, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    goto :goto_1
.end method
