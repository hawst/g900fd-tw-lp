.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;
.source "SamsungOspSearch.java"


# static fields
.field private static final CATEGORY_ID:Ljava/lang/String; = "categoryId"

.field private static final CATEGORY_SEARCH_TYPE:I = 0x0

.field private static final CONNECTION_TIMEOUT:I = 0x2710

.field private static final COUNTRY:Ljava/lang/String; = "country"

.field private static final FOOD_ID:Ljava/lang/String; = "foodId"

.field private static final HEADER_ACCEPT:Ljava/lang/String; = "Accept"

.field private static final HEADER_ACCEPT_VALUE:Ljava/lang/String; = "application/json"

.field private static final HEADER_APP_ID:Ljava/lang/String; = "reqAppId"

.field private static final HEADER_CONTENT_TYPE:Ljava/lang/String; = "Content-Type"

.field private static final HEADER_CONTENT_TYPE_VALUE:Ljava/lang/String; = "application/json"

.field private static final HEADER_DEVICE_ID:Ljava/lang/String; = "deviceId"

.field private static final KEYWORD:Ljava/lang/String; = "keyword"

.field private static final KOREAN_COUNTRY_CODE:Ljava/lang/String; = "KOR"

.field private static final KOREAN_LANGUAGE_CODE:Ljava/lang/String; = "kor"

.field private static final LANGUAGE:Ljava/lang/String; = "language"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MSC_API_CATEGORY:Ljava/lang/String; = "/platform/food/getCategoryList"

.field private static final MSC_API_EXTRA:Ljava/lang/String; = "/platform/food/getFoodInfo"

.field private static final MSC_API_FOOD_LIST_OF_CATEGORY:Ljava/lang/String; = "/platform/food/getFoodListOfCategory"

.field private static final MSC_API_FOOD_SEARCH_LIST:Ljava/lang/String; = "/platform/food/searchFood"

.field private static final MSC_SERVER_ADDRESS:Ljava/lang/String; = "https://kr-openapi.samsungshealth.com"

.field private static final PAGE_NO:Ljava/lang/String; = "pageNo"

.field private static final PAGE_NUMBER:I = 0x1

.field private static final PAGE_SIZE:Ljava/lang/String; = "pageSize"

.field private static final PER_PAGE_NUMBER:I = 0x1e

.field private static final SEARCH_TYPE:Ljava/lang/String; = "searchType"

.field public static final SEARCH_TYPE_AUTO_COMPLETION:I = 0x1

.field public static final SEARCH_TYPE_SEARCH:I = 0x2

.field public static final SORT_TYPE_ALPHABETICAL_ASC:I = 0x1

.field public static final SORT_TYPE_ALPHABETICAL_DESC:I = 0x2

.field public static final SORT_TYPE_CALORIEL_ASC:I = 0x5

.field public static final SORT_TYPE_CALORIE_DESC:I = 0x6

.field public static final SORT_TYPE_FREQUENCY_ASC:I = 0x3

.field public static final SORT_TYPE_FREQUENCY_DESC:I = 0x4

.field private static final SUB_CATEGORY_SEARCH_TYPE:I = 0x1

.field private static final TYPE:Ljava/lang/String; = "type"


# instance fields
.field private foodParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;-><init>(Landroid/content/Context;)V

    .line 119
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->foodParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->foodParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;->setOnFoodParsingCompleteListener(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser$OnFoodParsingCompleteListener;)V

    .line 127
    return-void
.end method

.method private createExtraFoodInfoRequest(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "foodId"    # Ljava/lang/String;

    .prologue
    .line 249
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 251
    .local v1, "returnObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "foodId"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "ex":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private createFoodByCategorySearchListRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;)Ljava/lang/String;
    .locals 4
    .param p1, "categoryListRequest"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;

    .prologue
    .line 235
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 237
    .local v1, "returnObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "country"

    const-string v3, "KOR"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 238
    const-string v2, "language"

    const-string v3, "kor"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 239
    const-string/jumbo v2, "pageNo"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->getPageNumber()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 240
    const-string/jumbo v2, "pageSize"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->getPerPageNumber()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 241
    const-string v2, "categoryId"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->getSubCategory()Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->getSubCategoryId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 242
    :catch_0
    move-exception v0

    .line 243
    .local v0, "ex":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private createFoodCategorySearchListRequest()Ljava/lang/String;
    .locals 4

    .prologue
    .line 221
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 223
    .local v1, "returnObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "country"

    const-string v3, "KOR"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 224
    const-string v2, "language"

    const-string v3, "kor"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 225
    const-string/jumbo v2, "pageNo"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 226
    const-string/jumbo v2, "pageSize"

    const/16 v3, 0x1e

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 227
    const-string/jumbo v2, "type"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "ex":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private createFoodSearchListRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Ljava/lang/String;
    .locals 4
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    .prologue
    .line 206
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 208
    .local v1, "returnObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "country"

    const-string v3, "KOR"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 209
    const-string v2, "language"

    const-string v3, "kor"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 210
    const-string/jumbo v2, "searchType"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 211
    const-string v2, "keyword"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 212
    const-string/jumbo v2, "pageNo"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getPageNumber()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 213
    const-string/jumbo v2, "pageSize"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getPerPageNumber()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, "ex":Lorg/json/JSONException;
    new-instance v2, Lorg/apache/http/ParseException;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/http/ParseException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private createFoodSubCategorySearchListRequest(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "categoryId"    # Ljava/lang/String;

    .prologue
    .line 259
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 261
    .local v1, "returnObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "country"

    const-string v3, "KOR"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 262
    const-string v2, "language"

    const-string v3, "kor"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 263
    const-string/jumbo v2, "pageNo"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 264
    const-string/jumbo v2, "pageSize"

    const/16 v3, 0x1e

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 265
    const-string/jumbo v2, "type"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 266
    const-string v2, "categoryId"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 267
    :catch_0
    move-exception v0

    .line 268
    .local v0, "ex":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private createHeaders()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 198
    .local v0, "stringMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "Accept"

    const-string v2, "application/json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    const-string/jumbo v1, "reqAppId"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    const-string v1, "deviceId"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    const-string v1, "Content-Type"

    const-string v2, "application/json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    return-object v0
.end method

.method private createJSON(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 3
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 318
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 319
    :catch_0
    move-exception v0

    .line 320
    .local v0, "ex":Lorg/json/JSONException;
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 321
    new-instance v1, Lorg/apache/http/ParseException;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/ParseException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    const-string v0, "1y90e30264"

    return-object v0
.end method

.method private getDeviceId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "deviceId":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string v0, ""

    .end local v0    # "deviceId":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private getStringContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 15
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "postData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 277
    :try_start_0
    new-instance v7, Lorg/apache/http/client/methods/HttpPost;

    new-instance v13, Ljava/net/URI;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v13}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 278
    .local v7, "request":Lorg/apache/http/client/methods/HttpPost;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->createHeaders()Ljava/util/Map;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 279
    .local v10, "s":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-virtual {v7, v13, v14}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 284
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "request":Lorg/apache/http/client/methods/HttpPost;
    .end local v10    # "s":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v3

    .line 285
    .local v3, "e":Ljava/net/URISyntaxException;
    sget-object v13, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v13, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 286
    const/4 v13, 0x0

    .line 313
    .end local v3    # "e":Ljava/net/URISyntaxException;
    :goto_1
    return-object v13

    .line 281
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v7    # "request":Lorg/apache/http/client/methods/HttpPost;
    :cond_0
    :try_start_1
    new-instance v12, Lorg/apache/http/entity/StringEntity;

    const-string v13, "UTF-8"

    move-object/from16 v0, p2

    invoke-direct {v12, v0, v13}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    .local v12, "stringEntity":Lorg/apache/http/entity/StringEntity;
    const-string v13, "application/json;charset=utf-8"

    invoke-virtual {v12, v13}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    .line 283
    invoke-virtual {v7, v12}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0

    .line 289
    new-instance v4, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v4}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 290
    .local v4, "httpParameters":Lorg/apache/http/params/HttpParams;
    sget-object v13, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-static {v4, v13}, Lorg/apache/http/params/HttpProtocolParams;->setVersion(Lorg/apache/http/params/HttpParams;Lorg/apache/http/ProtocolVersion;)V

    .line 291
    const/16 v13, 0x2710

    invoke-static {v4, v13}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 292
    const/16 v13, 0x2710

    invoke-static {v4, v13}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 294
    new-instance v2, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v2, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    .line 295
    .local v2, "client":Lorg/apache/http/client/HttpClient;
    invoke-interface {v2, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    .line 296
    .local v8, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v13

    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v6

    .line 297
    .local v6, "ips":Ljava/io/InputStream;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/InputStreamReader;

    const-string v14, "UTF-8"

    invoke-direct {v13, v6, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 298
    .local v1, "buf":Ljava/io/BufferedReader;
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v13

    invoke-interface {v13}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v13

    const/16 v14, 0xc8

    if-eq v13, v14, :cond_1

    .line 299
    new-instance v13, Lorg/apache/http/ParseException;

    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lorg/apache/http/ParseException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 302
    :cond_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 305
    .local v11, "sb":Ljava/lang/StringBuilder;
    :goto_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .line 306
    .local v9, "s":Ljava/lang/String;
    if-eqz v9, :cond_2

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v13

    if-nez v13, :cond_3

    .line 311
    :cond_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 312
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 313
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_1

    .line 309
    :cond_3
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method


# virtual methods
.method public getIndexOfFirstPage()I
    .locals 1

    .prologue
    .line 332
    const/4 v0, 0x1

    return v0
.end method

.method public getPerPageCount()I
    .locals 1

    .prologue
    .line 327
    const/16 v0, 0x1e

    return v0
.end method

.method public getServerSourceType()I
    .locals 1

    .prologue
    .line 141
    const v0, 0x46cd3

    return v0
.end method

.method protected performAutoCompleteSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;
    .locals 6
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;-><init>(Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->performFoodSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v3

    .line 148
    .local v3, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v1, "hints":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 150
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_0

    .line 155
    .end local v0    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_1
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;

    invoke-direct {v4, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;-><init>(Ljava/util/List;)V

    return-object v4
.end method

.method protected performBarcodeSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 1
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    const/4 v0, 0x0

    return-object v0
.end method

.method protected performCategorySearchRealization()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    const-string v1, "https://kr-openapi.samsungshealth.com/platform/food/getCategoryList"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->createFoodCategorySearchListRequest()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->getStringContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "ret":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->foodParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->createJSON(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;->parseCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    move-result-object v1

    return-object v1
.end method

.method protected performExtraFoodInfoRequestRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 4
    .param p1, "extraFoodInfoRequest"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;->getFoodInfoData()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->createExtraFoodInfoRequest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "postBody":Ljava/lang/String;
    const-string v2, "https://kr-openapi.samsungshealth.com/platform/food/getFoodInfo"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->getStringContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 188
    .local v1, "ret":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->foodParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->createJSON(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;->parseExtraFood(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    move-result-object v2

    return-object v2
.end method

.method protected performFoodSearchByCategoryRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;
    .locals 5
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    const-string v2, "https://kr-openapi.samsungshealth.com/platform/food/getFoodListOfCategory"

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->createFoodByCategorySearchListRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->getStringContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "ret":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->foodParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->createJSON(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;->parseFoodByCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v1

    .line 169
    .local v1, "searchListResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;->getTotalCount()I

    move-result v4

    invoke-direct {v3, v2, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;-><init>(Ljava/util/List;I)V

    return-object v3
.end method

.method protected performFoodSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 3
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    const-string v1, "https://kr-openapi.samsungshealth.com/platform/food/searchFood"

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->createFoodSearchListRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->getStringContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "ret":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->foodParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->createJSON(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;->parseFoodSearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v1

    return-object v1
.end method

.method protected performSubCategorySearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .locals 3
    .param p1, "foodSubCategoryRequest"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    const-string v1, "https://kr-openapi.samsungshealth.com/platform/food/getCategoryList"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;->getCategory()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->createFoodSubCategorySearchListRequest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->getStringContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 181
    .local v0, "ret":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->foodParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;->createJSON(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;->parseSubCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    move-result-object v1

    return-object v1
.end method
