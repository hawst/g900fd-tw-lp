.class public Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
.super Ljava/lang/Object;
.source "MergeCalorie.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "TimeItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;",
        ">;"
    }
.end annotation


# instance fields
.field exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

.field isStart:Z

.field time:J


# direct methods
.method public constructor <init>(JLcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;Z)V
    .locals 0
    .param p1, "time"    # J
    .param p3, "exer"    # Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    .param p4, "isStart"    # Z

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    .line 39
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    .line 40
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->isStart:Z

    .line 41
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;)I
    .locals 4
    .param p1, "o"    # Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    iget-wide v2, p1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 47
    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    .line 48
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    iget-wide v2, p1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 49
    const/4 v0, -0x1

    goto :goto_0

    .line 51
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->compareTo(Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;)I

    move-result v0

    return v0
.end method
