.class Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$6;
.super Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;
.source "SetQuickInputActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;FFI)V
    .locals 0
    .param p2, "x0"    # F
    .param p3, "x1"    # F
    .param p4, "x2"    # I

    .prologue
    .line 410
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$6;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;-><init>(FFI)V

    return-void
.end method


# virtual methods
.method public showAlertToast()V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$6;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # invokes: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->showRangeToast()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$500(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V

    .line 419
    return-void
.end method

.method public showInvalidInputToast()V
    .locals 4

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$6;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$700(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Landroid/widget/Toast;

    move-result-object v0

    if-nez v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$6;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$6;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$6;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09092e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$702(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$6;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$700(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 429
    return-void
.end method
