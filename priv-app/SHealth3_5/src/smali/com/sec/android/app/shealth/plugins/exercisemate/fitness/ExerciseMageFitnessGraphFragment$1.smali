.class Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment$1;
.super Ljava/lang/Object;
.source "ExerciseMageFitnessGraphFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->initDateBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 105
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v0

    .line 106
    .local v0, "newPeriodType":Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0804d1

    if-ne v1, v2, :cond_2

    .line 108
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 118
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v1

    if-eq v1, v0, :cond_1

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->changePeriod(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 123
    :cond_1
    return-void

    .line 110
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0804d2

    if-ne v1, v2, :cond_3

    .line 112
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    goto :goto_0

    .line 114
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0804d3

    if-ne v1, v2, :cond_0

    .line 116
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    goto :goto_0
.end method
