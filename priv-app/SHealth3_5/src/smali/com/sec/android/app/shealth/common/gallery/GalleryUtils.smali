.class public final Lcom/sec/android/app/shealth/common/gallery/GalleryUtils;
.super Ljava/lang/Object;
.source "GalleryUtils.java"


# static fields
.field private static final SCALED_BITMAP_HEIGHT:I = 0x280

.field private static final SCALED_BITMAP_WIDTH:I = 0x1e0


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method private static getBitmapFactoryOptions(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .param p0, "imagePth"    # Ljava/lang/String;

    .prologue
    .line 151
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 152
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 153
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 155
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 156
    return-object v0
.end method

.method public static getBitmapWithReusingByLast(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "imagePath"    # Ljava/lang/String;
    .param p1, "inSampleSize"    # I

    .prologue
    .line 36
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryUtils;->getBitmapFactoryOptions(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    .line 37
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 38
    iput p1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 39
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 40
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method
