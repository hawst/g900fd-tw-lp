.class Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CignaCoachActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GoalMissionEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x1

    .line 398
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 400
    .local v0, "action":Ljava/lang/String;
    const-string v2, "GoalCompleteReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    const-string v2, "com.sec.android.app.shealth.cignacoach.GOAL_COMPLETE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 404
    const-string v2, "intent_category_type"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 408
    .local v1, "categroyType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$3;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 428
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mGoalComplete:Z
    invoke-static {v2, v6}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$002(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;Z)Z

    .line 462
    .end local v1    # "categroyType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :cond_0
    :goto_0
    return-void

    .line 430
    :cond_1
    const-string v2, "com.sec.android.app.shealth.cignacoach.MISSION_COMPLETE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 432
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    const-string v3, "EXTRA_NAME_EXPAND_GOAL_ID"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    # setter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mExpandedGoalId:I
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$102(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;I)I

    .line 434
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 435
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mExpandedGoalId:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->updateList(I)V

    goto :goto_0

    .line 438
    :cond_2
    const-string v2, "com.sec.android.app.shealth.cignacoach.MISSION_ADD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 440
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mMissionAdd:Z
    invoke-static {v2, v6}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$302(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;Z)Z

    .line 442
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    const-string v3, "EXTRA_NAME_EXPAND_GOAL_ID"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    # setter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mExpandedGoalId:I
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$102(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;I)I

    goto :goto_0

    .line 444
    :cond_3
    const-string v2, "com.sec.android.app.shealth.cignacoach.MISSION_PROGRESSED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 446
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    const-string v3, "EXTRA_NAME_EXPAND_GOAL_ID"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    # setter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mExpandedGoalId:I
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$102(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;I)I

    .line 448
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 449
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mExpandedGoalId:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->updateList(I)V

    goto :goto_0

    .line 456
    :cond_4
    const-string v2, "com.cigna.mobile.coach.SCORE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "com.sec.shealth.UPDATE_PROFILE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 458
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCignaSummaryFragment()Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 459
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCignaSummaryFragment()Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScoreByWeightTrackerBR()V

    goto/16 :goto_0

    .line 408
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
