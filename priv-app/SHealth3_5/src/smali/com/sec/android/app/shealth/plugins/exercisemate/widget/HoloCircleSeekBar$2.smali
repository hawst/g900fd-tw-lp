.class Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;
.super Landroid/os/Handler;
.source "HoloCircleSeekBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/high16 v3, 0x43b40000    # 360.0f

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 380
    :goto_0
    return-void

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$100(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCurrentValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$200(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mBackupValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$300(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mBackupValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$300(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;I)I

    .line 369
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$400(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculateAngleFromRadians(I)F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$700(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;I)F

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$602(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;F)F

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$600(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)F

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculatePointerPosition(F)[F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$900(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;F)[F

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->pointerPosition:[F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$802(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;[F)[F

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->invalidate()V

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->stopTask()V

    goto :goto_0

    .line 367
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCurrentValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$200(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;I)I

    goto :goto_1

    .line 375
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    const/4 v1, 0x2

    # += operator for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$112(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;I)I

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$100(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;I)I

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$400(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculateAngleFromRadians(I)F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$700(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;I)F

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$602(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;F)F

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$600(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)F

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculatePointerPosition(F)[F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$900(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;F)[F

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->pointerPosition:[F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->access$802(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;[F)[F

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->invalidate()V

    goto/16 :goto_0
.end method
