.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;
.super Ljava/lang/Object;
.source "CommonCache.java"


# static fields
.field public static infoActivityType:B

.field public static infoBirthDay:I

.field public static infoBirthMonth:I

.field public static infoBirthYear:I

.field public static infoCalorieConsumptionGoal:I

.field public static infoCalorieIntakeGoal:I

.field public static infoDistanceUnit:Ljava/lang/String;

.field public static infoGender:I

.field public static infoGlucoseUnit:Ljava/lang/String;

.field public static infoHeight:F

.field public static infoHeightInch:I

.field public static infoHeightUnit:Ljava/lang/String;

.field public static infoName:Ljava/lang/String;

.field public static infoStepsUnit:Ljava/lang/String;

.field public static infoTemperatureUnit:Ljava/lang/String;

.field public static infoWeekFormat:I

.field public static infoWeight:F

.field public static infoWeightLb:F

.field public static infoWeightUnit:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, -0x1

    .line 18
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoName:Ljava/lang/String;

    .line 19
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoGender:I

    .line 23
    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoWeight:F

    .line 24
    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoWeightLb:F

    .line 25
    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoHeight:F

    .line 26
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoHeightInch:I

    .line 27
    const-string v0, "inch"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoHeightUnit:Ljava/lang/String;

    .line 28
    const-string v0, "lb"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoWeightUnit:Ljava/lang/String;

    .line 29
    const-string v0, "F"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoTemperatureUnit:Ljava/lang/String;

    .line 30
    const-string v0, "km"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    .line 31
    const-string/jumbo v0, "steps"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoStepsUnit:Ljava/lang/String;

    .line 32
    const-string/jumbo v0, "mg/dL"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoGlucoseUnit:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoWeekFormat:I

    .line 34
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoCalorieIntakeGoal:I

    .line 35
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoCalorieConsumptionGoal:I

    .line 36
    const/4 v0, 0x2

    sput-byte v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoActivityType:B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
