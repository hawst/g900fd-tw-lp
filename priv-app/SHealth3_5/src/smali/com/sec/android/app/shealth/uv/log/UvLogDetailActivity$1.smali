.class Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;
.super Ljava/lang/Object;
.source "UvLogDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 232
    instance-of v2, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 234
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 235
    .local v0, "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 253
    .end local v0    # "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_0
    :goto_0
    return-void

    .line 237
    .restart local v0    # "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    const-class v3, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 238
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 239
    const-string v2, "MODE_KEY"

    const-string v3, "Edit"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 240
    const-string v2, "HEART_RATE_ID_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Lcom/sec/android/app/shealth/uv/data/UvData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/uv/data/UvData;->getId()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 241
    const-string v2, "Date"

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mTimeDate:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$100(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const-string v2, "TIME_DATE_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mTime:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$200(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 243
    const-string v2, "DATE_TALK_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->ddate:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$300(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    const-string v2, "STATE_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Lcom/sec/android/app/shealth/uv/data/UvData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/uv/data/UvData;->getState()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 245
    const-string v3, "COMMENT_KEY"

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Lcom/sec/android/app/shealth/uv/data/UvData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/data/UvData;->getComment()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, ""

    :goto_1
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    const-string v2, "SCORE_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Lcom/sec/android/app/shealth/uv/data/UvData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/uv/data/UvData;->getScore()D

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 247
    const-string v2, "RECOMENDED_TIME_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Lcom/sec/android/app/shealth/uv/data/UvData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/uv/data/UvData;->getRecomendedTime()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 248
    const-string v2, "SUN_PROTECTION_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Lcom/sec/android/app/shealth/uv/data/UvData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSunProtection()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 249
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 245
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Lcom/sec/android/app/shealth/uv/data/UvData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/data/UvData;->getComment()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
