.class Lcom/sec/android/app/shealth/home/HomeActivity$9$2;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/HomeActivity$9;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/home/HomeActivity$9;

.field final synthetic val$dialog:Landroid/app/Dialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeActivity$9;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1343
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9$2;->this$1:Lcom/sec/android/app/shealth/home/HomeActivity$9;

    iput-object p2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9$2;->val$dialog:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 1347
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth"

    const-string v4, "HD03"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1348
    const-string v0, "com.sec.shealth.action.EXERCISE"

    .line 1349
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1350
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "show_graph_fragment"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1352
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9$2;->this$1:Lcom/sec/android/app/shealth/home/HomeActivity$9;

    iget-object v2, v2, Lcom/sec/android/app/shealth/home/HomeActivity$9;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 1353
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$9$2;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    .line 1354
    return-void
.end method
