.class Lcom/sec/android/app/shealth/uv/UvActivity$15;
.super Ljava/lang/Object;
.source "UvActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvActivity;->showInfomationDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0

    .prologue
    .line 765
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 5
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getDataCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "skin_type_checked"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 772
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2000(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mDrawerLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1900(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 773
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mDialogHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2100(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 779
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2300(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 780
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2400(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 781
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # setter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2302(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 782
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # setter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2402(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 783
    return-void

    .line 775
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v0

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->needsLocationPopup:Z

    .line 776
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->fromDashboard:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2200(Lcom/sec/android/app/shealth/uv/UvActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/state/UvState;->reStartStates()V

    .line 777
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$15;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0
.end method
