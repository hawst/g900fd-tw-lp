.class public Lcom/sec/android/app/shealth/moreapps/service/HttpConnector;
.super Ljava/lang/Object;
.source "HttpConnector.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ShealthNetworkConnector"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "server"    # Ljava/lang/String;
    .param p3, "API"    # Ljava/lang/String;
    .param p5, "bodyData"    # Ljava/lang/String;
    .param p6, "listener"    # Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/service/health/connectionmanager2/NetException;
        }
    .end annotation

    .prologue
    .line 59
    .local p4, "header":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/moreapps/service/HttpConnector;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;ZLcom/sec/android/service/health/connectionmanager2/IOnResponseListener;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;ZLcom/sec/android/service/health/connectionmanager2/IOnResponseListener;)V
    .locals 23
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "server"    # Ljava/lang/String;
    .param p3, "API"    # Ljava/lang/String;
    .param p5, "bodyData"    # Ljava/lang/String;
    .param p6, "httpsEnabled"    # Z
    .param p7, "listener"    # Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/service/health/connectionmanager2/NetException;
        }
    .end annotation

    .prologue
    .line 63
    .local p4, "header":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 67
    :try_start_0
    move/from16 v0, p6

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/moreapps/service/HttpConnector;->initialize(ZLjava/lang/String;)V

    .line 68
    const-string v3, "ShealthNetworkConnector"

    invoke-static {v3}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v2

    .line 72
    .local v2, "connectionManager":Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    new-instance v7, Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    invoke-direct {v7}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;-><init>()V

    .line 75
    .local v7, "param":Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 76
    .local v11, "hd":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v3, 0x0

    const-string v4, "application/json"

    aput-object v4, v18, v3

    .line 77
    .local v18, "accept":[Ljava/lang/String;
    const-string v3, "Accept"

    move-object/from16 v0, v18

    invoke-virtual {v11, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 80
    .local v20, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/4 v4, 0x0

    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    aput-object v3, v22, v4

    .line 81
    .local v22, "value":[Ljava/lang/String;
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v11, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/sec/android/service/health/connectionmanager2/NetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 100
    .end local v2    # "connectionManager":Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .end local v7    # "param":Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    .end local v11    # "hd":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v18    # "accept":[Ljava/lang/String;
    .end local v20    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v22    # "value":[Ljava/lang/String;
    :catch_0
    move-exception v19

    .line 101
    .local v19, "e":Lcom/sec/android/service/health/connectionmanager2/NetException;
    new-instance v3, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v4, -0x1

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v5

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getException()Ljava/lang/Exception;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/Exception;)V

    throw v3

    .line 84
    .end local v19    # "e":Lcom/sec/android/service/health/connectionmanager2/NetException;
    .restart local v2    # "connectionManager":Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .restart local v7    # "param":Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    .restart local v11    # "hd":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    .restart local v18    # "accept":[Ljava/lang/String;
    .restart local v21    # "i$":Ljava/util/Iterator;
    :cond_0
    const/16 v4, 0x65

    :try_start_1
    sget-object v5, Lcom/sec/android/service/health/connectionmanager2/MethodType;->POST:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p7

    invoke-virtual/range {v2 .. v11}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v13

    .line 96
    .local v13, "requestid":J
    const-wide/16 v3, 0x0

    cmp-long v3, v13, v3

    if-nez v3, :cond_1

    .line 97
    const/16 v15, 0x65

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v12, p7

    invoke-interface/range {v12 .. v17}, Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;->onRequestCancelled(JILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/sec/android/service/health/connectionmanager2/NetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 105
    :cond_1
    return-void

    .line 102
    .end local v2    # "connectionManager":Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .end local v7    # "param":Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    .end local v11    # "hd":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v13    # "requestid":J
    .end local v18    # "accept":[Ljava/lang/String;
    .end local v21    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v19

    .line 103
    .local v19, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v4, -0x1

    const/4 v5, -0x6

    move-object/from16 v0, v19

    invoke-direct {v3, v4, v5, v0}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(IILjava/lang/Exception;)V

    throw v3
.end method

.method public static initialize(ZLjava/lang/String;)V
    .locals 4
    .param p0, "httpsEnabled"    # Z
    .param p1, "server"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/service/health/connectionmanager2/NetException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 47
    const-string v1, "ShealthNetworkConnector"

    const-string v2, "create connectionManager"

    invoke-static {v1, v2}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const-string v1, "ShealthNetworkConnector"

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->createInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 49
    const-string v1, "ShealthNetworkConnector"

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    .line 51
    .local v0, "connectionManager":Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    invoke-virtual {v0, p0, v3, v3}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->initConnectionManager(ZLjava/lang/String;Ljava/lang/String;)Z

    .line 53
    const/16 v1, 0x1bb

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->setAddressWithoutLookUp(Ljava/lang/String;I)Z

    .line 54
    return-void
.end method
