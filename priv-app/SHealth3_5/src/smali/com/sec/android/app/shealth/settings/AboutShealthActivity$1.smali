.class Lcom/sec/android/app/shealth/settings/AboutShealthActivity$1;
.super Ljava/lang/Object;
.source "AboutShealthActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/AboutShealthActivity;

.field final synthetic val$help:Landroid/widget/Button;

.field final synthetic val$koreanPopupButton:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/AboutShealthActivity;Landroid/widget/Button;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/AboutShealthActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$1;->val$help:Landroid/widget/Button;

    iput-object p3, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$1;->val$koreanPopupButton:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 60
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$1;->val$help:Landroid/widget/Button;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$1;->val$koreanPopupButton:Landroid/widget/Button;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->temporarilyDisableClick([Landroid/view/View;)V

    .line 61
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/AboutShealthActivity;

    const-class v2, Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 62
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x30000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/AboutShealthActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->startActivity(Landroid/content/Intent;)V

    .line 64
    return-void
.end method
