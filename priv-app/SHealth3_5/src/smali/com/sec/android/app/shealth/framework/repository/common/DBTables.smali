.class public interface abstract Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.super Ljava/lang/Object;
.source "DBTables.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$MyBluetoohDevices;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$BPedometerTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$TrainingLoadPeakTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$RealTimeSpeedTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$RealTimeHeartBeatTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$MapPathTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$AntDeviceListTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$HealthboardTipsTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$TipInstanceTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$RunninProTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$ImagesTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$ExerciseInfoTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$WeightTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$BloodGlucoseView;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$BloodGlucoseTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$BloodPressureView;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$BloodPressureTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$ExerciseView;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$ExerciseTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$MealItemsTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$MealView;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$MealTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$ExtendedFoodInfoTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$FoodInfoTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$SensorTable;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$GOAL_INSTANCE_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$SleepMonitorView;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$SLEEP_MONITOR_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$USER_PROFILE_SHARE_DATA_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$USER_PROFILE_SHARE_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$USER_DEVICE_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$BLUETOOTH_DEVICES_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$NEWS_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$WIDGET_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$MEMO_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$GOAL_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$TEMP_COMFORT_ZONE_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$ComfortZoneView;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$COMFORT_ZONE_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$TEMP_WALK_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$WalkForLifeView;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$WALK_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$TEMP_SENSOR_TABLE;,
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$CONFIGURATION_TABLE;
    }
.end annotation


# static fields
.field public static final BASE_DB_NAME:Ljava/lang/String; = "base.db"

.field public static final DATABASE_NAME:Ljava/lang/String; = "shealth2.db"

.field public static final PLATFORM_DB_NAME:Ljava/lang/String; = "platform.db"

.field public static final SECURITY_CP_DATABASE_NAME:Ljava/lang/String; = "secure_sec_health.db"

.field public static final SEC_DATABASE_NAME:Ljava/lang/String; = "secure_shealth2.db"

.field public static final SEC_VALIDITY_CHECK_DATABASE_NAME:Ljava/lang/String; = "secure_shealth2.db_temp"
