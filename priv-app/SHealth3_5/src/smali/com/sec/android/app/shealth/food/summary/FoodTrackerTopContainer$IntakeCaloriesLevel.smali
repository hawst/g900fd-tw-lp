.class final enum Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;
.super Ljava/lang/Enum;
.source "FoodTrackerTopContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "IntakeCaloriesLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

.field public static final enum GOAL_ACHIEVED:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

.field public static final enum GOAL_EXCEEDED:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

.field public static final enum LOW_CALORIES:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

.field public static final enum NORMAL_CALORIES:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 262
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    const-string v1, "LOW_CALORIES"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->LOW_CALORIES:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    .line 263
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    const-string v1, "NORMAL_CALORIES"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->NORMAL_CALORIES:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    .line 264
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    const-string v1, "GOAL_ACHIEVED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->GOAL_ACHIEVED:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    .line 265
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    const-string v1, "GOAL_EXCEEDED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->GOAL_EXCEEDED:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    .line 261
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->LOW_CALORIES:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->NORMAL_CALORIES:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->GOAL_ACHIEVED:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->GOAL_EXCEEDED:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->$VALUES:[Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 261
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static getIntakeCaloriesLevel(F)Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;
    .locals 2
    .param p0, "intakeCaloriesRatio"    # F

    .prologue
    .line 268
    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    .line 269
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Intake calories ratio should be not less than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270
    :cond_0
    const v0, 0x3dcccccd    # 0.1f

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_1

    .line 271
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->LOW_CALORIES:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    .line 277
    :goto_0
    return-object v0

    .line 272
    :cond_1
    const v0, 0x3f666666    # 0.9f

    cmpg-float v0, p0, v0

    if-gez v0, :cond_2

    .line 273
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->NORMAL_CALORIES:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    goto :goto_0

    .line 274
    :cond_2
    const v0, 0x3f8ccccd    # 1.1f

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_3

    .line 275
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->GOAL_ACHIEVED:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    goto :goto_0

    .line 277
    :cond_3
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->GOAL_EXCEEDED:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 261
    const-class v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;
    .locals 1

    .prologue
    .line 261
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->$VALUES:[Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    return-object v0
.end method
