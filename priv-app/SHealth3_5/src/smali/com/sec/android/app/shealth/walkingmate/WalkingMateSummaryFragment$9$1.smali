.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;)V
    .locals 0

    .prologue
    .line 1515
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1518
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentEndofDate:J
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$3102(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;J)J

    .line 1519
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$3202(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Ljava/util/Date;)Ljava/util/Date;

    .line 1520
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$3400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$3300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSystemDate(Ljava/util/Date;)V

    .line 1521
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$3600(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$3500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 1522
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$3700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setSelectedText(Ljava/util/Date;)V

    .line 1523
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->stepDataLoad()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$3800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    .line 1524
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->alreadyInitDelay:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateAnimation(I)V

    .line 1525
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setNextAndPrevDates()V

    .line 1526
    return-void
.end method
