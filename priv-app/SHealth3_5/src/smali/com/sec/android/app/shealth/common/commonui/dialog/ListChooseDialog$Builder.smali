.class public Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
.super Ljava/lang/Object;
.source "ListChooseDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected additionalTopTextResId:I

.field protected chooseType:I

.field protected context:Landroid/content/Context;

.field protected descriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private dialogItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;",
            ">;"
        }
    .end annotation
.end field

.field protected disabledList:[I

.field protected flags:I

.field protected isChecked:[Z

.field protected items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected setDefaultContentMargins:Z

.field protected titleStringResId:I

.field protected topTextDescription:Ljava/lang/String;

.field protected topTextResId:I

.field protected type:I


# direct methods
.method public constructor <init>(II)V
    .locals 2
    .param p1, "titleStringResId"    # I
    .param p2, "chooseType"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->titleStringResId:I

    .line 488
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->topTextResId:I

    .line 490
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->additionalTopTextResId:I

    .line 491
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->type:I

    .line 492
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->chooseType:I

    .line 493
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->flags:I

    .line 494
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setDefaultContentMargins:Z

    .line 497
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->dialogItems:Ljava/util/ArrayList;

    .line 498
    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->isChecked:[Z

    .line 499
    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->disabledList:[I

    .line 504
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->titleStringResId:I

    .line 505
    iput p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->chooseType:I

    .line 506
    return-void
.end method


# virtual methods
.method public addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    .locals 0
    .param p1, "flags"    # I

    .prologue
    .line 556
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->flags:I

    .line 557
    return-object p0
.end method

.method public build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    .locals 5

    .prologue
    .line 570
    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->chooseType:I

    const/16 v3, 0xa

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->chooseType:I

    const/16 v3, 0x9

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->chooseType:I

    const/16 v3, 0xc

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->chooseType:I

    const/16 v3, 0xb

    if-eq v2, v3, :cond_0

    .line 572
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$string;->wrong_choose_type_exception:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 574
    :cond_0
    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->chooseType:I

    packed-switch v2, :pswitch_data_0

    .line 587
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->descriptions:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->items:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 589
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->items:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->descriptions:Ljava/util/ArrayList;

    .line 591
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->isChecked:[Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->items:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->isChecked:[Z

    array-length v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->items:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 593
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$string;->different_length_exception:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 577
    :pswitch_0
    const/4 v2, 0x3

    iput v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->type:I

    goto :goto_0

    .line 581
    :pswitch_1
    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->type:I

    goto :goto_0

    .line 584
    :pswitch_2
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->type:I

    goto :goto_0

    .line 595
    :cond_2
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;-><init>()V

    .line 596
    .local v0, "dialog":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->buildGeneralDialogBehaviourBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 597
    .local v1, "dialogBundle":Landroid/os/Bundle;
    const-string/jumbo v2, "text_for_items"

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->items:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 598
    const-string v2, "description_of_items"

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->descriptions:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 599
    const-string v2, "checked_items"

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->isChecked:[Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 600
    const-string v2, "disabled_items"

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->disabledList:[I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 601
    const-string v2, "choose_type"

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->chooseType:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 602
    const-string/jumbo v2, "top_text"

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->topTextResId:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 603
    const-string v2, "additional_top_text"

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->additionalTopTextResId:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 604
    const-string/jumbo v2, "top_description_text"

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->topTextDescription:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    const-string/jumbo v2, "multi_checkbox_dialog"

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->dialogItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 606
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->setArguments(Landroid/os/Bundle;)V

    .line 607
    return-object v0

    .line 574
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected buildGeneralDialogBehaviourBundle()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 612
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 613
    .local v0, "dialogBundle":Landroid/os/Bundle;
    const-string/jumbo v1, "title_res_id"

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->titleStringResId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 614
    const-string/jumbo v1, "type"

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->type:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 615
    const-string v1, "flags"

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->flags:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 616
    const-string/jumbo v1, "set_default_content_margins"

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setDefaultContentMargins:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 617
    return-object v0
.end method

.method public setAdditionalTopTextResId(I)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    .locals 0
    .param p1, "additionalTopTextResId"    # I

    .prologue
    .line 546
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->additionalTopTextResId:I

    .line 547
    return-object p0
.end method

.method public setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    .locals 0
    .param p1, "checked"    # [Z

    .prologue
    .line 516
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->isChecked:[Z

    .line 517
    return-object p0
.end method

.method public setDisabled([I)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    .locals 0
    .param p1, "disabled"    # [I

    .prologue
    .line 522
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->disabledList:[I

    .line 523
    return-object p0
.end method

.method public setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;"
        }
    .end annotation

    .prologue
    .line 510
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->items:Ljava/util/ArrayList;

    .line 511
    return-object p0
.end method

.method public setMultiCheckItem(Ljava/util/ArrayList;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;",
            ">;)",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;"
        }
    .end annotation

    .prologue
    .line 561
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->dialogItems:Ljava/util/ArrayList;

    .line 562
    return-object p0
.end method

.method public setTopTextDescription(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    .locals 0
    .param p1, "topTextDescription"    # Ljava/lang/String;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->topTextDescription:Ljava/lang/String;

    .line 541
    return-object p0
.end method

.method public setTopTextResId(I)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    .locals 0
    .param p1, "topTextResId"    # I

    .prologue
    .line 529
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->topTextResId:I

    .line 530
    return-object p0
.end method
