.class Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList$1;
.super Ljava/lang/Object;
.source "HeartrateLogByTagDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 192
    const v2, 0x7f02086c

    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 194
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    const-class v3, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 195
    .local v0, "mIntent":Landroid/content/Intent;
    const v2, 0x7f0805a9

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 196
    .local v1, "tagName":Ljava/lang/String;
    const-string/jumbo v2, "tag"

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const-string/jumbo v2, "tagName"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 201
    return-void
.end method
