.class Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CloseSystemDialogsIntentReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 46

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/16 v8, -0x7b

    aput v8, v2, v7

    const/16 v7, -0x1992

    aput v7, v2, v6

    const/16 v6, -0x6b

    aput v6, v2, v5

    const/16 v5, -0x46cb

    aput v5, v2, v4

    const/16 v4, -0x24

    aput v4, v2, v3

    const/16 v3, -0x9

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/16 v9, -0x15

    aput v9, v1, v8

    const/16 v8, -0x19ff

    aput v8, v1, v7

    const/16 v7, -0x1a

    aput v7, v1, v6

    const/16 v6, -0x46ac

    aput v6, v1, v5

    const/16 v5, -0x47

    aput v5, v1, v4

    const/16 v4, -0x7b

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :try_start_1
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v1, 0x29

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x551d

    aput v44, v2, v43

    const/16 v43, -0x52dd

    aput v43, v2, v42

    const/16 v42, -0x3c

    aput v42, v2, v41

    const/16 v41, -0x4aa3

    aput v41, v2, v40

    const/16 v40, -0x2a

    aput v40, v2, v39

    const/16 v39, -0xcb7

    aput v39, v2, v38

    const/16 v38, -0x5f

    aput v38, v2, v37

    const/16 v37, -0x6f

    aput v37, v2, v36

    const/16 v36, 0x4a25

    aput v36, v2, v35

    const/16 v35, 0x36a

    aput v35, v2, v34

    const/16 v34, 0xc71

    aput v34, v2, v33

    const/16 v33, -0x1897

    aput v33, v2, v32

    const/16 v32, -0x6f

    aput v32, v2, v31

    const/16 v31, -0x2f

    aput v31, v2, v30

    const/16 v30, -0x5b

    aput v30, v2, v29

    const/16 v29, -0x6a

    aput v29, v2, v28

    const/16 v28, -0xeec

    aput v28, v2, v27

    const/16 v27, -0x5d

    aput v27, v2, v26

    const/16 v26, 0x5f28

    aput v26, v2, v25

    const/16 v25, 0x7131

    aput v25, v2, v24

    const/16 v24, 0x7114

    aput v24, v2, v23

    const/16 v23, 0x1005

    aput v23, v2, v22

    const/16 v22, 0x717e

    aput v22, v2, v21

    const/16 v21, 0x6038

    aput v21, v2, v20

    const/16 v20, 0x6007

    aput v20, v2, v19

    const/16 v19, 0x220f

    aput v19, v2, v18

    const/16 v18, 0x204e

    aput v18, v2, v17

    const/16 v17, 0x4e41

    aput v17, v2, v16

    const/16 v16, 0x5b27

    aput v16, v2, v15

    const/16 v15, 0x7a1f

    aput v15, v2, v14

    const/16 v14, 0xe17

    aput v14, v2, v13

    const/16 v13, 0xb6b

    aput v13, v2, v12

    const/16 v12, -0x1a81

    aput v12, v2, v11

    const/16 v11, -0x6a

    aput v11, v2, v10

    const/16 v10, 0x2043

    aput v10, v2, v9

    const/16 v9, -0x1c8d

    aput v9, v2, v8

    const/16 v8, -0x7a

    aput v8, v2, v7

    const/16 v7, -0x43f4

    aput v7, v2, v6

    const/16 v6, -0x2d

    aput v6, v2, v5

    const/16 v5, -0x3c

    aput v5, v2, v3

    const/16 v3, -0x44

    aput v3, v2, v1

    const/16 v1, 0x29

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x5578

    aput v45, v1, v44

    const/16 v44, -0x52ab

    aput v44, v1, v43

    const/16 v43, -0x53

    aput v43, v1, v42

    const/16 v42, -0x4ac8

    aput v42, v1, v41

    const/16 v41, -0x4b

    aput v41, v1, v40

    const/16 v40, -0xcd4

    aput v40, v1, v39

    const/16 v39, -0xd

    aput v39, v1, v38

    const/16 v38, -0x1

    aput v38, v1, v37

    const/16 v37, 0x4a4a

    aput v37, v1, v36

    const/16 v36, 0x34a

    aput v36, v1, v35

    const/16 v35, 0xc03

    aput v35, v1, v34

    const/16 v34, -0x18f4

    aput v34, v1, v33

    const/16 v33, -0x19

    aput v33, v1, v32

    const/16 v32, -0x48

    aput v32, v1, v31

    const/16 v31, -0x40

    aput v31, v1, v30

    const/16 v30, -0xb

    aput v30, v1, v29

    const/16 v29, -0xe8f

    aput v29, v1, v28

    const/16 v28, -0xf

    aput v28, v1, v27

    const/16 v27, 0x5f5c

    aput v27, v1, v26

    const/16 v26, 0x715f

    aput v26, v1, v25

    const/16 v25, 0x7171

    aput v25, v1, v24

    const/16 v24, 0x1071

    aput v24, v1, v23

    const/16 v23, 0x7110

    aput v23, v1, v22

    const/16 v22, 0x6071

    aput v22, v1, v21

    const/16 v21, 0x6060

    aput v21, v1, v20

    const/16 v20, 0x2260

    aput v20, v1, v19

    const/16 v19, 0x2022

    aput v19, v1, v18

    const/16 v18, 0x4e20

    aput v18, v1, v17

    const/16 v17, 0x5b4e

    aput v17, v1, v16

    const/16 v16, 0x7a5b

    aput v16, v1, v15

    const/16 v15, 0xe7a

    aput v15, v1, v14

    const/16 v14, 0xb0e

    aput v14, v1, v13

    const/16 v13, -0x1af5

    aput v13, v1, v12

    const/16 v12, -0x1b

    aput v12, v1, v11

    const/16 v11, 0x203a

    aput v11, v1, v10

    const/16 v10, -0x1ce0

    aput v10, v1, v9

    const/16 v9, -0x1d

    aput v9, v1, v8

    const/16 v8, -0x4381

    aput v8, v1, v7

    const/16 v7, -0x44

    aput v7, v1, v6

    const/16 v6, -0x58

    aput v6, v1, v5

    const/4 v5, -0x1

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/16 v10, 0x93f

    aput v10, v2, v9

    const/16 v9, 0x766c

    aput v9, v2, v8

    const/16 v8, 0x2e1d

    aput v8, v2, v7

    const/16 v7, -0x12b5

    aput v7, v2, v6

    const/16 v6, -0x80

    aput v6, v2, v5

    const/16 v5, 0x554b

    aput v5, v2, v3

    const/16 v3, 0x573d

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/16 v11, 0x946

    aput v11, v1, v10

    const/16 v10, 0x7609

    aput v10, v1, v9

    const/16 v9, 0x2e76

    aput v9, v1, v8

    const/16 v8, -0x12d2

    aput v8, v1, v7

    const/16 v7, -0x13

    aput v7, v1, v6

    const/16 v6, 0x5524

    aput v6, v1, v5

    const/16 v5, 0x5755

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x6b

    aput v13, v2, v12

    const/16 v12, -0x4f

    aput v12, v2, v11

    const/16 v11, 0x612

    aput v11, v2, v10

    const/16 v10, -0x1499

    aput v10, v2, v9

    const/16 v9, -0x61

    aput v9, v2, v8

    const/16 v8, 0x6f49

    aput v8, v2, v7

    const/16 v7, 0x5d0a

    aput v7, v2, v6

    const/16 v6, -0x78c2

    aput v6, v2, v5

    const/16 v5, -0x1e

    aput v5, v2, v3

    const/16 v3, 0x781a

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x1a

    aput v14, v1, v13

    const/16 v13, -0x3f

    aput v13, v1, v12

    const/16 v12, 0x662

    aput v12, v1, v11

    const/16 v11, -0x14fa

    aput v11, v1, v10

    const/16 v10, -0x15

    aput v10, v1, v9

    const/16 v9, 0x6f27

    aput v9, v1, v8

    const/16 v8, 0x5d6f

    aput v8, v1, v7

    const/16 v7, -0x78a3

    aput v7, v1, v6

    const/16 v6, -0x79

    aput v6, v1, v5

    const/16 v5, 0x7868

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->hideSoftKey()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V

    :cond_1
    :goto_8
    return-void

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_8
.end method
