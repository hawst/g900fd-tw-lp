.class Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$1;
.super Ljava/lang/Object;
.source "TwoButtonMessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->showView(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 93
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 94
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 95
    const-string v1, "EXTRA_NAME_GOAL_ID"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mGoalId:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 96
    const-string v1, "EXTRA_NAME_MISSION_ID"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMissionId:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 97
    const-string v1, "EXTRA_NAME_MISSION_SEQUENCE_ID"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMissionSequenceId:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->startActivity(Landroid/content/Intent;)V

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->finish()V

    .line 100
    return-void
.end method
