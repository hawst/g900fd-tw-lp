.class public Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;
.super Ljava/lang/Object;
.source "AlertDialogListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;
    }
.end annotation


# static fields
.field private static EDIT_TAG_CHECK:Ljava/lang/String;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private checkPosition:I

.field private lvTagList:Landroid/widget/ListView;

.field mActivity:Landroid/support/v4/app/FragmentActivity;

.field mContext:Landroid/content/Context;

.field mDialogItemClickListener:Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;

.field mHeight:I

.field private mSelectedTagName:Ljava/lang/String;

.field private mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field mTagListadapter:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string/jumbo v0, "tagcheck"

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->EDIT_TAG_CHECK:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentActivity;ILcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p3, "resId"    # I
    .param p4, "dialogItemClick"    # Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->lvTagList:Landroid/widget/ListView;

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->checkPosition:I

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mSelectedTagName:Ljava/lang/String;

    .line 51
    const-class v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->TAG:Ljava/lang/String;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mContext:Landroid/content/Context;

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mDialogItemClickListener:Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mActivity:Landroid/support/v4/app/FragmentActivity;

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mHeight:I

    .line 59
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mContext:Landroid/content/Context;

    .line 60
    iput-object p4, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mDialogItemClickListener:Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;

    .line 61
    iput-object p2, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mActivity:Landroid/support/v4/app/FragmentActivity;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->lvTagList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;Landroid/widget/ListView;)Landroid/widget/ListView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;
    .param p1, "x1"    # Landroid/widget/ListView;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->lvTagList:Landroid/widget/ListView;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->checkPosition:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->checkPosition:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;Landroid/content/Context;ILandroid/content/SharedPreferences;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I
    .param p3, "x3"    # Landroid/content/SharedPreferences;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->setcheckedPosition(Landroid/content/Context;ILandroid/content/SharedPreferences;)V

    return-void
.end method

.method private convertDptoPx(F)I
    .locals 4
    .param p1, "dp"    # F

    .prologue
    .line 275
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 276
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 277
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v3

    .line 278
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method

.method private setValueInternal(Ljava/lang/String;Ljava/lang/Object;Landroid/content/SharedPreferences;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;
    .param p3, "mPref"    # Landroid/content/SharedPreferences;

    .prologue
    .line 269
    invoke-interface {p3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 270
    .local v0, "prefsEdit":Landroid/content/SharedPreferences$Editor;
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 271
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 272
    return-void
.end method

.method private setcheckedPosition(Landroid/content/Context;ILandroid/content/SharedPreferences;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "value"    # I
    .param p3, "mPref"    # Landroid/content/SharedPreferences;

    .prologue
    .line 263
    if-nez p1, :cond_0

    .line 266
    :goto_0
    return-void

    .line 265
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->EDIT_TAG_CHECK:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1, p3}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->setValueInternal(Ljava/lang/String;Ljava/lang/Object;Landroid/content/SharedPreferences;)V

    goto :goto_0
.end method


# virtual methods
.method public getCheckedPosition(Landroid/content/Context;Landroid/content/SharedPreferences;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mPref"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v0, 0x0

    .line 256
    if-nez p1, :cond_0

    .line 259
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->EDIT_TAG_CHECK:Ljava/lang/String;

    invoke-interface {p2, v1, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 2

    .prologue
    .line 303
    const/4 v0, 0x0

    .line 304
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 305
    const/4 v0, 0x1

    .line 306
    :cond_0
    return v0
.end method

.method public showChooserDialog(ZLjava/lang/String;)V
    .locals 21
    .param p1, "isTag"    # Z
    .param p2, "tagText"    # Ljava/lang/String;

    .prologue
    .line 65
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string/jumbo v19, "showChooserDialog"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v10

    .line 68
    .local v10, "mHeartRateDatabaseHelper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    move/from16 v8, p1

    .line 69
    .local v8, "isAddTag":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 71
    .local v11, "mPref":Landroid/content/SharedPreferences;
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mSelectedTagName:Ljava/lang/String;

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mSelectedTagName:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mSelectedTagName:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 73
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f090f08

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mSelectedTagName:Ljava/lang/String;

    .line 75
    :cond_1
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v17, "tagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    const/16 v18, 0x5208

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    const/16 v18, 0x5335

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    const/16 v18, 0x526f

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    const/16 v18, 0x526d

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v13, v0, [Ljava/lang/String;

    const/16 v18, 0x0

    const-string v19, "first_r_tag"

    aput-object v19, v13, v18

    const/16 v18, 0x1

    const-string/jumbo v19, "second_r_tag"

    aput-object v19, v13, v18

    const/16 v18, 0x2

    const-string/jumbo v19, "third_r_tag"

    aput-object v19, v13, v18

    .line 84
    .local v13, "recentTagIds":[Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v0, v13

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v6, v0, :cond_3

    .line 85
    aget-object v12, v13, v6

    .line 86
    .local v12, "recentTagId":Ljava/lang/String;
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v11, v12, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v16

    .line 87
    .local v16, "tagId":I
    if-lez v16, :cond_2

    .line 88
    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTagByIndex(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v15

    .line 89
    .local v15, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    if-eqz v15, :cond_2

    .line 90
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    .end local v15    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 94
    .end local v12    # "recentTagId":Ljava/lang/String;
    .end local v16    # "tagId":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mActivity:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    .line 95
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const v18, 0x7f030160

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    .line 96
    .local v14, "rowView":Landroid/view/View;
    const v18, 0x7f0805f7

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 97
    .local v9, "lp":Landroid/widget/LinearLayout;
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v18

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->convertDptoPx(F)I

    move-result v20

    add-int v19, v19, v20

    mul-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mHeight:I

    .line 98
    if-nez v8, :cond_4

    .line 99
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v11}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->getCheckedPosition(Landroid/content/Context;Landroid/content/SharedPreferences;)I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->checkPosition:I

    .line 101
    :cond_4
    new-instance v18, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mActivity:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mSelectedTagName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    move-object/from16 v3, v17

    move-object/from16 v4, v20

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;-><init>(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    .line 103
    new-instance v5, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v5, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 104
    .local v5, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 105
    const v18, 0x7f090f19

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 106
    const v18, 0x7f090c0f

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 107
    const v18, 0x7f030145

    new-instance v19, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$1;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$1;-><init>(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 119
    new-instance v18, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$2;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$2;-><init>(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 131
    new-instance v18, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$3;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$3;-><init>(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 147
    new-instance v18, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$4;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$4;-><init>(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-object/from16 v18, v0

    if-nez v18, :cond_5

    .line 157
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 158
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mActivity:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v19

    const-string v20, "hrm_tag_chooser_dialog"

    invoke-virtual/range {v18 .. v20}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->notifyDataSetChanged()V

    .line 163
    :cond_6
    return-void
.end method

.method public updateStringOnLocaleChange()V
    .locals 7

    .prologue
    .line 283
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->lvTagList:Landroid/widget/ListView;

    if-nez v5, :cond_1

    .line 300
    :cond_0
    return-void

    .line 285
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->getCount()I

    move-result v0

    .line 288
    .local v0, "count":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;->getTagList()Ljava/util/ArrayList;

    move-result-object v2

    .line 290
    .local v2, "tagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    if-eqz v2, :cond_0

    .line 291
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 292
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->lvTagList:Landroid/widget/ListView;

    invoke-virtual {v5, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 293
    .local v4, "view":Landroid/view/View;
    const v5, 0x7f0805de

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 294
    .local v3, "text":Landroid/widget/TextView;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget-object v5, v5, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v5, v6, :cond_2

    .line 295
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget v5, v5, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 291
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 297
    :cond_2
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget-object v5, v5, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
