.class public Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
.super Ljava/lang/Object;
.source "PointDataDisplayDayMonth.java"


# instance fields
.field private count:I

.field private dayData:J

.field private daySleepAvgEfficy:F

.field private daySleepDuration:J

.field private daySleepMaxRiseTime:J

.field private daySleepMinStartTime:J

.field private daySleepMotionlessDuration:J

.field private multiple:Z


# direct methods
.method public constructor <init>(FJJJJJ)V
    .locals 1
    .param p1, "daySleepAvgEfficy"    # F
    .param p2, "daySleepDuration"    # J
    .param p4, "daySleepStartTime"    # J
    .param p6, "daySleepRiseTime"    # J
    .param p8, "daySleepMotionlessDuration"    # J
    .param p10, "dayData"    # J

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->count:I

    .line 17
    iput p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepAvgEfficy:F

    .line 18
    iput-wide p2, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepDuration:J

    .line 19
    iput-wide p4, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepMinStartTime:J

    .line 20
    iput-wide p6, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepMaxRiseTime:J

    .line 21
    iput-wide p8, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepMotionlessDuration:J

    .line 22
    iput-wide p10, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->dayData:J

    .line 23
    return-void
.end method


# virtual methods
.method public addDataDisplayDay(FJJLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;J)V
    .locals 2
    .param p1, "daySleepAvgEfficy"    # F
    .param p2, "daySleepRiseTime"    # J
    .param p4, "daySleepDuration"    # J
    .param p6, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p7, "daySleepMotionlessDuration"    # J

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepAvgEfficy:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepAvgEfficy:F

    .line 27
    iput-wide p2, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepMaxRiseTime:J

    .line 28
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepDuration:J

    add-long/2addr v0, p4

    iput-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepDuration:J

    .line 29
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepMotionlessDuration:J

    add-long/2addr v0, p7

    iput-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepMotionlessDuration:J

    .line 30
    iget v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->count:I

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->multiple:Z

    .line 32
    return-void
.end method

.method public getDayData()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->dayData:J

    return-wide v0
.end method

.method public getDaySleepAvgEfficy()F
    .locals 4

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->multiple:Z

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepMotionlessDuration()J

    move-result-wide v0

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    long-to-float v0, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepDuration()J

    move-result-wide v1

    long-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepAvgEfficy:F

    .line 38
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepAvgEfficy:F

    return v0
.end method

.method public getDaySleepDuration()J
    .locals 4

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepDuration:J

    iget v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->count:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getDaySleepMotionlessDuration()J
    .locals 4

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepMotionlessDuration:J

    iget v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->count:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getDaySleepRiseTime()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepMaxRiseTime:J

    return-wide v0
.end method

.method public getDaySleepStartTime()J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->daySleepMinStartTime:J

    return-wide v0
.end method
