.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;
.super Ljava/lang/Object;
.source "ExerciseProDistanceGoalActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->showAbnormalGoalWarningPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 5
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const v2, 0xf3e58

    const/4 v4, 0x0

    .line 229
    invoke-virtual {p3}, Landroid/app/Dialog;->dismiss()V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValue()F

    move-result v0

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    const v1, 0x4973e580    # 999000.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$402(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;I)I

    .line 236
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$402(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;I)I

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getKmFromMeterByLocale(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setValue(FZ)V

    .line 248
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValueEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 249
    return-void

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    const/16 v1, 0x64

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$402(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;I)I

    goto :goto_0

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->getFloatFromString(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->convertMilesToMeters(F)I

    move-result v0

    if-le v0, v2, :cond_2

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    const v1, 0xf39a1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$402(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;I)I

    .line 245
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$402(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;I)I

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getMilesFromMetersByLocale(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setValue(FZ)V

    goto :goto_1

    .line 243
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    const/16 v1, 0xa1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$402(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;I)I

    goto :goto_2
.end method
