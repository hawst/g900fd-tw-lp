.class public Lcom/sec/android/app/shealth/award/mypagephoto/AwardMyPagePhotoDeleteActivity;
.super Landroid/app/Activity;
.source "AwardMyPagePhotoDeleteActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private deleteMyPagePhoto()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/UDR;->PORTRAIT_PHOTO_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->isFileExist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/UDR;->PORTRAIT_PHOTO_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->deleteFile(Ljava/lang/String;)Z

    .line 45
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/UDR;->LANDSCAPE_PHOTO_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->isFileExist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/UDR;->LANDSCAPE_PHOTO_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->deleteFile(Ljava/lang/String;)Z

    .line 48
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/UDR;->NORMAL_PHOTO_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->isFileExist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/UDR;->NORMAL_PHOTO_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->deleteFile(Ljava/lang/String;)Z

    .line 51
    :cond_2
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/mypagephoto/AwardMyPagePhotoDeleteActivity;->deleteMyPagePhoto()V

    .line 33
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 37
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/mypagephoto/AwardMyPagePhotoDeleteActivity;->finish()V

    .line 39
    return-void
.end method
