.class Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;
.super Ljava/lang/Object;
.source "HeartrateMoreTagActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;I)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    iput p2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f0805f6

    .line 134
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 135
    .local v1, "radioBtn":Landroid/widget/RadioButton;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEmotionTagList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->access$100(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEmotionTagList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->access$100(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "radioBtn":Landroid/widget/RadioButton;
    check-cast v1, Landroid/widget/RadioButton;

    .line 137
    .restart local v1    # "radioBtn":Landroid/widget/RadioButton;
    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEditTagList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->lvEditTagList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "radioBtn":Landroid/widget/RadioButton;
    check-cast v1, Landroid/widget/RadioButton;

    .line 141
    .restart local v1    # "radioBtn":Landroid/widget/RadioButton;
    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 143
    :cond_1
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "radioBtn":Landroid/widget/RadioButton;
    check-cast v1, Landroid/widget/RadioButton;

    .line 144
    .restart local v1    # "radioBtn":Landroid/widget/RadioButton;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 145
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->mContext:Landroid/app/Activity;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->access$300(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;)Landroid/app/Activity;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->val$position:I

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->setcheckedPosition(Landroid/content/Context;I)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;Landroid/content/Context;I)V

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    iget-object v3, v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->this$1:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->mTags:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;->access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList;)Ljava/util/ArrayList;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$CustomList$1;->val$position:I

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->markTag(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->access$600(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V

    .line 148
    return-void
.end method
