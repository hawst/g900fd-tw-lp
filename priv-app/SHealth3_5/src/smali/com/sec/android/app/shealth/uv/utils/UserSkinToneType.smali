.class public Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;
.super Ljava/lang/Object;
.source "UserSkinToneType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;,
        Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;
    }
.end annotation


# static fields
.field private static userSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;


# instance fields
.field private selectedSkinType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

.field private selectedSkinoption:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->selectedSkinoption:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->selectedSkinType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->userSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->userSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    .line 60
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->userSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->userSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    .line 83
    return-void
.end method

.method public getSelectedSkinOption()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->selectedSkinoption:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    return-object v0
.end method

.method public getSelectedSkinType()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->selectedSkinType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    return-object v0
.end method

.method public setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V
    .locals 0
    .param p1, "selectedSkinType"    # Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->selectedSkinoption:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    .line 70
    return-void
.end method

.method public setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V
    .locals 0
    .param p1, "selectedSkinType"    # Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->selectedSkinType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    .line 78
    return-void
.end method
