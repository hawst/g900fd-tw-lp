.class public Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
.super Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
.source "FloatEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;,
        Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;,
        Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;,
        Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;,
        Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;,
        Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnUpdateListener;
    }
.end annotation


# static fields
.field private static final FLOAT_EDIT_TEXT_VALUE_KEY:Ljava/lang/String; = "FLOAT_EDIT_TEXT_VALUE_KEY"

.field private static final SAVED_STATE_KEY:Ljava/lang/String; = "SAVED_STATE_KEY"

.field private static final sDefaultNumberSeparator:Ljava/lang/String; = "."


# instance fields
.field protected mEmptyListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

.field private mFilterPattern:Ljava/util/regex/Pattern;

.field private mFloatInputChangedListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;

.field private mInputValue:Ljava/lang/Float;

.field private mInterval:F

.field private mLanguage:Ljava/lang/String;

.field private mMaxValue:F

.field private mMinValue:F

.field private mMinValueLength:I

.field private mOnSpecialInputListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;

.field private mOnUpdateListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnUpdateListener;

.field private mShouldMissTextWatcher:Z

.field private mSystemNumberSeparator:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;-><init>(Landroid/content/Context;)V

    .line 46
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInterval:F

    .line 47
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    .line 48
    const v0, 0x461c3c00    # 9999.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMaxValue:F

    .line 49
    const/high16 v0, 0x420c0000    # 35.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    .line 51
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mLanguage:Ljava/lang/String;

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->initialize()V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInterval:F

    .line 47
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    .line 48
    const v0, 0x461c3c00    # 9999.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMaxValue:F

    .line 49
    const/high16 v0, 0x420c0000    # 35.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    .line 51
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mLanguage:Ljava/lang/String;

    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->initialize()V

    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 122
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInterval:F

    .line 47
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    .line 48
    const v0, 0x461c3c00    # 9999.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMaxValue:F

    .line 49
    const/high16 v0, 0x420c0000    # 35.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    .line 51
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mLanguage:Ljava/lang/String;

    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->initialize()V

    .line 124
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isFirstZeroStringWrong(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMaxValue:F

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;FLjava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    .param p1, "x1"    # F
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isNeedToReplaceWithMinRangeValue(FLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;F)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    .param p1, "x1"    # F

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setValueIfChanged(F)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->informClientAboutUpdate()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mLanguage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Ljava/util/regex/Pattern;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mFilterPattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mOnSpecialInputListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Ljava/lang/Float;)Ljava/lang/Float;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    .param p1, "x1"    # Ljava/lang/Float;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->informClientAboutValue()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mShouldMissTextWatcher:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mShouldMissTextWatcher:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    .param p1, "x1"    # F

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->changeToValue(F)V

    return-void
.end method

.method private changeToValue(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 391
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    .line 392
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mShouldMissTextWatcher:Z

    .line 393
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->informClientAboutUpdate()V

    .line 394
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->informClientAboutValue()V

    .line 395
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setFloatValue(F)V

    .line 396
    return-void
.end method

.method private informClientAboutUpdate()V
    .locals 2

    .prologue
    .line 399
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mOnUpdateListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnUpdateListener;

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mOnUpdateListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnUpdateListener;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnUpdateListener;->update(F)V

    .line 402
    :cond_0
    return-void
.end method

.method private informClientAboutValue()V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mFloatInputChangedListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mFloatInputChangedListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;->onValueChanged(Ljava/lang/Float;)V

    .line 388
    :cond_0
    return-void
.end method

.method private initialize()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 167
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->updateFilterWatcherValues()V

    .line 168
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$1;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 169
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$1;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 170
    return-void
.end method

.method private isDecimalInterval()Z
    .locals 2

    .prologue
    .line 244
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInterval:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFirstZeroStringWrong(Ljava/lang/String;)Z
    .locals 1
    .param p1, "changedString"    # Ljava/lang/String;

    .prologue
    .line 426
    const-string v0, "0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "0."

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "0"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNeedToReplaceWithMinRangeValue(FLjava/lang/String;)Z
    .locals 12
    .param p1, "changedNumber"    # F
    .param p2, "changedString"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 413
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    .line 414
    .local v0, "changedNumberLength":I
    iget v8, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    const/high16 v9, 0x3f800000    # 1.0f

    cmpg-float v8, v8, v9

    if-gez v8, :cond_3

    move v3, v6

    .line 415
    .local v3, "isMinRangeLessThenOne":Z
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;

    invoke-virtual {p2, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    .line 416
    .local v1, "isEndsWithSeparator":Z
    float-to-double v8, p1

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    iget v10, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    cmpg-double v8, v8, v10

    if-gez v8, :cond_4

    move v2, v6

    .line 417
    .local v2, "isLessThenMinRange":Z
    :goto_1
    iget v8, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValueLength:I

    if-lt v0, v8, :cond_5

    move v4, v6

    .line 418
    .local v4, "isTooBigLength":Z
    :goto_2
    const/4 v8, 0x0

    cmpl-float v8, p1, v8

    if-nez v8, :cond_6

    move v5, v6

    .line 420
    .local v5, "isZeroInput":Z
    :goto_3
    if-nez v3, :cond_1

    if-eqz v1, :cond_0

    if-nez v2, :cond_2

    :cond_0
    if-nez v1, :cond_1

    if-nez v4, :cond_2

    if-nez v5, :cond_2

    :cond_1
    if-eqz v3, :cond_7

    if-nez v1, :cond_7

    if-eqz v4, :cond_7

    :cond_2
    :goto_4
    return v6

    .end local v1    # "isEndsWithSeparator":Z
    .end local v2    # "isLessThenMinRange":Z
    .end local v3    # "isMinRangeLessThenOne":Z
    .end local v4    # "isTooBigLength":Z
    .end local v5    # "isZeroInput":Z
    :cond_3
    move v3, v7

    .line 414
    goto :goto_0

    .restart local v1    # "isEndsWithSeparator":Z
    .restart local v3    # "isMinRangeLessThenOne":Z
    :cond_4
    move v2, v7

    .line 416
    goto :goto_1

    .restart local v2    # "isLessThenMinRange":Z
    :cond_5
    move v4, v7

    .line 417
    goto :goto_2

    .restart local v4    # "isTooBigLength":Z
    :cond_6
    move v5, v7

    .line 418
    goto :goto_3

    .restart local v5    # "isZeroInput":Z
    :cond_7
    move v6, v7

    .line 420
    goto :goto_4
.end method

.method private setInputType()V
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isDecimalInterval()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setInputType(I)V

    goto :goto_0
.end method

.method private setValueIfChanged(F)Z
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    .line 406
    :cond_0
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    .line 407
    const/4 v0, 0x1

    .line 409
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateFilterWatcherValues()V
    .locals 5

    .prologue
    .line 173
    const/4 v2, 0x1

    .line 174
    .local v2, "maxValueLengthReserve":I
    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMaxValue:F

    float-to-int v3, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int v1, v3, v2

    .line 175
    .local v1, "maxValueLength":I
    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValueLength:I

    .line 176
    const/4 v0, 0x1

    .line 177
    .local v0, "decimalDigitsCount":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[0-9]{0,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isDecimalInterval()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "+([.]{1}||[.]{1}[0-9]{1,1})?"

    :goto_0
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mFilterPattern:Ljava/util/regex/Pattern;

    .line 181
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setInputType()V

    .line 182
    return-void

    .line 177
    :cond_0
    const-string v3, ""

    goto :goto_0
.end method


# virtual methods
.method public getEmptyFieldListener()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mEmptyListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    return-object v0
.end method

.method public getFloatValue()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOutOfRange()Z
    .locals 3

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMaxValue:F

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isInRange(FFF)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEditorAction(I)V
    .locals 5
    .param p1, "actionCode"    # I

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mEmptyListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    if-eqz v2, :cond_0

    .line 472
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mEmptyListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;->processEmptyField()V

    .line 488
    :goto_0
    return-void

    .line 476
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mEmptyListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    if-eqz v2, :cond_1

    .line 478
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v4, "."

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 479
    .local v0, "changeString":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->makeFloatValid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 480
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 481
    .local v1, "inputValue":F
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_1

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v2

    if-gez v2, :cond_1

    .line 483
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mEmptyListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;->processEmptyField()V

    goto :goto_0

    .line 487
    .end local v0    # "changeString":Ljava/lang/String;
    .end local v1    # "inputValue":F
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->onEditorAction(I)V

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 2
    .param p1, "focused"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 237
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 238
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getEmptyFieldListener()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    move-result-object v0

    if-nez v0, :cond_1

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setFloatValue(F)V

    .line 241
    :cond_1
    return-void

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x1

    .line 433
    const/4 v4, 0x4

    if-ne p1, v4, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-ne v4, v3, :cond_1

    .line 435
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mEmptyListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    if-eqz v4, :cond_0

    .line 438
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mEmptyListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;->processEmptyField()V

    .line 463
    :goto_0
    return v3

    .line 442
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mEmptyListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    if-eqz v4, :cond_1

    .line 446
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v6, "."

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 447
    .local v0, "changeString":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->makeFloatValid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 448
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 449
    .local v2, "inputValue":F
    const/4 v4, 0x0

    cmpl-float v4, v2, v4

    if-ltz v4, :cond_1

    iget v4, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v4

    if-gez v4, :cond_1

    .line 452
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mEmptyListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;->processEmptyField()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 457
    .end local v0    # "changeString":Ljava/lang/String;
    .end local v2    # "inputValue":F
    :catch_0
    move-exception v1

    .line 459
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v3, "FloatEditText"

    invoke-static {v3, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 463
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 209
    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    .line 210
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "SAVED_STATE_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 211
    const-string v1, "FLOAT_EDIT_TEXT_VALUE_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    const-string v1, "FLOAT_EDIT_TEXT_VALUE_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->changeToValue(F)V

    .line 214
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 199
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 200
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "SAVED_STATE_KEY"

    invoke-super {p0}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 202
    const-string v1, "FLOAT_EDIT_TEXT_VALUE_KEY"

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 204
    :cond_0
    return-object v0
.end method

.method public setEmptyFieldListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;)V
    .locals 0
    .param p1, "emptyFieldListener"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    .prologue
    .line 491
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mEmptyListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    .line 492
    return-void
.end method

.method public setFloatValue(F)V
    .locals 3
    .param p1, "value"    # F

    .prologue
    .line 252
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isDecimalInterval()Z

    move-result v0

    if-nez v0, :cond_1

    .line 253
    :cond_0
    float-to-int v0, p1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    .line 262
    :goto_0
    return-void

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mLanguage:Ljava/lang/String;

    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->roundToTenth(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v2, "."

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 259
    :cond_2
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->roundToTenth(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setIntervalValues(FFF)V
    .locals 0
    .param p1, "interval"    # F
    .param p2, "minValue"    # F
    .param p3, "maxValue"    # F

    .prologue
    .line 191
    iput p2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F

    .line 192
    iput p3, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMaxValue:F

    .line 193
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInterval:F

    .line 194
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->updateFilterWatcherValues()V

    .line 195
    return-void
.end method

.method public setOnFloatInputChangedListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mFloatInputChangedListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;

    .line 140
    return-void
.end method

.method public setOnSpecialCharacterInput(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mOnSpecialInputListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;

    .line 148
    return-void
.end method

.method public setUpdateListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnUpdateListener;)V
    .locals 0
    .param p1, "onUpdateListener"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnUpdateListener;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mOnUpdateListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnUpdateListener;

    .line 132
    return-void
.end method
