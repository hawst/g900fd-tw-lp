.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;
.super Landroid/widget/FrameLayout;
.source "AMapExerciseProMapViewMapFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MapWrapper"
.end annotation


# instance fields
.field dectector:Landroid/view/GestureDetector;

.field isDoubleTapped:Z

.field mapActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;

    .line 40
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->mapActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->isDoubleTapped:Z

    .line 41
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->dectector:Landroid/view/GestureDetector;

    .line 56
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->dectector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 62
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->mapActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->onFling:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->isDoubleTapped:Z

    if-ne v0, v2, :cond_2

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->mapActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->isManuallyDragged:Z

    .line 65
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->isDoubleTapped:Z

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->mapActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->isInCenter:Z

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->mapActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->setLocationModeIcon(Z)V

    .line 68
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 86
    :goto_0
    return v0

    .line 71
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 86
    :cond_3
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 74
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->mapActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->isInCenter:Z

    if-eqz v0, :cond_4

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->mapActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->updateStartLocation()V

    .line 78
    :cond_4
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->dectector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 82
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->mapActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->isInCenter:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->mapActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->onFling:Z

    if-nez v0, :cond_3

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->mapActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->restoreLocationIfNeeded()V

    goto :goto_1

    .line 71
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
