.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;
.super Ljava/lang/Object;
.source "ProfileHeightView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 174
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 176
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getListValue()I

    move-result v1

    if-nez v1, :cond_0

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->checkValueText()Z

    move-result v1

    if-nez v1, :cond_2

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->clearFocus()V

    .line 194
    :goto_0
    return v0

    .line 185
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->checkValueText()Z

    move-result v1

    if-nez v1, :cond_1

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->clearFocus()V

    goto :goto_0

    .line 188
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->checkValueText()Z

    move-result v1

    if-nez v1, :cond_2

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->clearFocus()V

    goto :goto_0

    .line 194
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
