.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;
.super Landroid/os/Handler;
.source "ExerciseProHRMFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iget v1, p1, Landroid/os/Message;->what:I

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mPrevError:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$102(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)I

    .line 171
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 191
    :goto_0
    return-void

    .line 173
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sth  mHandler HANDLER_MESSAGE_READY"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setReadyUI(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V

    goto :goto_0

    .line 177
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sth  mHandler HANDLER_MESSAGE_MEASURING"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setMeasuringUI(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V

    goto :goto_0

    .line 181
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sth  mHandler HANDLER_MESSAGE_MEASURING_FAIL"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setMeasuringFailUI(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V

    goto :goto_0

    .line 185
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sth  mHandler HANDLER_MESSAGE_MEASURING_END"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setMeasuringEndUI()V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->refreshFragmentFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    goto :goto_0

    .line 171
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
