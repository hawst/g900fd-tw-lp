.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$EditMealDialogButtonController;
.super Ljava/lang/Object;
.source "FoodTrackerMainContainer.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditMealDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)V
    .locals 0

    .prologue
    .line 948
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$EditMealDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$1;

    .prologue
    .line 948
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$EditMealDialogButtonController;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 954
    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 958
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$EditMealDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 960
    .local v0, "mealDao":Lcom/sec/android/app/shealth/food/fooddao/MealDao;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$EditMealDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCurrentMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->access$600(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->deleteDataById(J)Z

    .line 962
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$EditMealDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->clearImagesCache()V

    .line 966
    .end local v0    # "mealDao":Lcom/sec/android/app/shealth/food/fooddao/MealDao;
    :cond_0
    return-void
.end method
