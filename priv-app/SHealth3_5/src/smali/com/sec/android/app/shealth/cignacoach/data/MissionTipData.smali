.class public Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;
.super Ljava/lang/Object;
.source "MissionTipData.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private tip:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "tip"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;->title:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;->tip:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public getTip()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;->tip:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setTip(Ljava/lang/String;)V
    .locals 0
    .param p1, "tip"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;->tip:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;->title:Ljava/lang/String;

    .line 34
    return-void
.end method
