.class public final Lcom/sec/android/app/shealth/logutils/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/logutils/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final activity_appear_animation_duration_ms:I = 0x7f0d003d

.field public static final activity_disappear_animation_duration_ms:I = 0x7f0d003e

.field public static final bg_graph_bottom_padding:I = 0x7f0d0026

.field public static final bg_graph_left_padding:I = 0x7f0d0023

.field public static final bg_graph_line_thickness:I = 0x7f0d0022

.field public static final bg_graph_right_padding:I = 0x7f0d0024

.field public static final bg_graph_top_padding:I = 0x7f0d0025

.field public static final bp_graph_body_shadow_thickness:I = 0x7f0d002b

.field public static final bp_graph_body_shadow_width:I = 0x7f0d002a

.field public static final bp_graph_body_width:I = 0x7f0d0029

.field public static final bp_graph_bottom_padding:I = 0x7f0d0034

.field public static final bp_graph_goal_line_text_size:I = 0x7f0d0030

.field public static final bp_graph_label_title_text_size:I = 0x7f0d002f

.field public static final bp_graph_left_padding:I = 0x7f0d0031

.field public static final bp_graph_line_thickness:I = 0x7f0d002c

.field public static final bp_graph_normal_range_max_visible:I = 0x7f0d0028

.field public static final bp_graph_normal_range_min_visible:I = 0x7f0d0027

.field public static final bp_graph_right_padding:I = 0x7f0d0032

.field public static final bp_graph_top_padding:I = 0x7f0d0033

.field public static final bp_graph_x_axis_text_size:I = 0x7f0d002d

.field public static final bp_graph_x_axis_text_space:I = 0x7f0d0036

.field public static final bp_graph_x_axis_width:I = 0x7f0d0035

.field public static final bp_graph_y_axis_text_size:I = 0x7f0d002e

.field public static final bp_graph_y_axis_text_space:I = 0x7f0d0037

.field public static final food_graph_bottom_padding:I = 0x7f0d0011

.field public static final food_graph_left_padding:I = 0x7f0d000e

.field public static final food_graph_line_thickness:I = 0x7f0d0013

.field public static final food_graph_right_padding:I = 0x7f0d000f

.field public static final food_graph_top_padding:I = 0x7f0d0010

.field public static final food_graph_y_axis_text_space:I = 0x7f0d0012

.field public static final food_graph_y_label_text_size:I = 0x7f0d000d

.field public static final ga_dispatchPeriod:I = 0x7f0d0000

.field public static final graph_abandoned_handler_item_offset:I = 0x7f0d001c

.field public static final graph_chart_handler_time_out_delay:I = 0x7f0d001e

.field public static final graph_day_tab_visible_data_offset:I = 0x7f0d0020

.field public static final graph_goal_line_text_size:I = 0x7f0d0018

.field public static final graph_handler_height:I = 0x7f0d0015

.field public static final graph_handler_item_stroke_width:I = 0x7f0d0019

.field public static final graph_handler_item_text_offset:I = 0x7f0d001d

.field public static final graph_handler_item_text_size:I = 0x7f0d001a

.field public static final graph_handler_stroke_width:I = 0x7f0d001b

.field public static final graph_handler_width:I = 0x7f0d0014

.field public static final graph_horizontal_marking_interval:I = 0x7f0d0003

.field public static final graph_horizontal_marks_count:I = 0x7f0d0002

.field public static final graph_hour_tab_visible_data_offset:I = 0x7f0d001f

.field public static final graph_month_tab_visible_data_offset:I = 0x7f0d0021

.field public static final graph_x_axis_text_size:I = 0x7f0d0016

.field public static final graph_y_axis_text_size:I = 0x7f0d0017

.field public static final number_of_columns:I = 0x7f0d0001

.field public static final period_flipper_fragment_type_change_animation_duration_ms:I = 0x7f0d003f

.field public static final summary_view_content_balance_statistics_text_columns:I = 0x7f0d003c

.field public static final summary_view_content_balance_text_columns:I = 0x7f0d003b

.field public static final summary_view_health_care_progress_bar_default_max_value:I = 0x7f0d003a

.field public static final summary_view_health_care_progress_bar_default_min_value:I = 0x7f0d0039

.field public static final summary_view_health_care_progress_bar_default_update_duration:I = 0x7f0d0038

.field public static final water_graph_data_zoom_rate:I = 0x7f0d0006

.field public static final water_graph_normal_range_max_visible:I = 0x7f0d0004

.field public static final water_graph_normal_range_min_visible:I = 0x7f0d0005

.field public static final water_graph_vertical_marks_count:I = 0x7f0d0008

.field public static final water_graph_visible_vertical_marks_count:I = 0x7f0d0007

.field public static final water_graph_y_axis_dimension:I = 0x7f0d0009

.field public static final weight_graph_normal_range_max_visible:I = 0x7f0d000a

.field public static final weight_graph_normal_range_min_visible:I = 0x7f0d000b

.field public static final weight_graph_y_axis_dimension:I = 0x7f0d000c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
