.class public Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;
.super Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;
.source "CignaSummaryFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;
.implements Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;
.implements Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$1;,
        Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAnimationEnable:Z

.field private mCignaExerciseScoreTopMarginView:Landroid/view/View;

.field private mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

.field private mGetScoreAsyncTask:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;

.field private mLifestyleCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

.field private mOverallScore:I

.field private mReceiveBadgeIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mRootView:Landroid/view/View;

.field private mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

.field private mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

.field private mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

.field private mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

.field private mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

.field private mToDoStartBadgeActivity:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;-><init>()V

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mAnimationEnable:Z

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mToDoStartBadgeActivity:Z

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mLifestyleCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 172
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;Lcom/cigna/coach/apiobjects/Scores;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;
    .param p1, "x1"    # Lcom/cigna/coach/apiobjects/Scores;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateHeaderView(Lcom/cigna/coach/apiobjects/Scores;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;ILcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateNoScoreView(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScoreViewTopMargin(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;Ljava/util/Hashtable;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;
    .param p1, "x1"    # Ljava/util/Hashtable;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScore(Ljava/util/Hashtable;)V

    return-void
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mOverallScore:I

    return p1
.end method

.method private getAssessmentComplete(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)I
    .locals 2
    .param p1, "category"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .prologue
    .line 538
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$LIFE_STYLE:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 552
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 540
    :pswitch_0
    const v0, 0x7f090c74

    goto :goto_0

    .line 542
    :pswitch_1
    const v0, 0x7f090c75

    goto :goto_0

    .line 544
    :pswitch_2
    const v0, 0x7f090c71

    goto :goto_0

    .line 546
    :pswitch_3
    const v0, 0x7f090c76

    goto :goto_0

    .line 548
    :pswitch_4
    const v0, 0x7f090c77

    goto :goto_0

    .line 538
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getCategoryScore(Lcom/cigna/coach/apiobjects/Scores;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)I
    .locals 6
    .param p1, "scores"    # Lcom/cigna/coach/apiobjects/Scores;
    .param p2, "categoryTypeRequest"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .prologue
    .line 682
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v4

    .line 683
    .local v4, "targetCategoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Scores;->getCategoryInfo()Ljava/util/Hashtable;

    move-result-object v0

    .line 684
    .local v0, "categoryInfo":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    invoke-virtual {v0}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 686
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 687
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 688
    .local v1, "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    if-ne v1, v4, :cond_0

    .line 689
    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 690
    .local v2, "i":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 693
    .end local v1    # "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v2    # "i":Ljava/lang/Integer;
    :goto_0
    return v5

    :cond_1
    const/4 v5, -0x1

    goto :goto_0
.end method

.method private getCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 2
    .param p1, "categoryTypeRequest"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .prologue
    .line 631
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryTypeRequest:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 643
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    :goto_0
    return-object v0

    .line 633
    :pswitch_0
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    goto :goto_0

    .line 635
    :pswitch_1
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    goto :goto_0

    .line 637
    :pswitch_2
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    goto :goto_0

    .line 639
    :pswitch_3
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    goto :goto_0

    .line 641
    :pswitch_4
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    goto :goto_0

    .line 631
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getCategoryTypeReqest(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    .locals 1
    .param p1, "requestCode"    # I

    .prologue
    .line 648
    sparse-switch p1, :sswitch_data_0

    .line 660
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    :goto_0
    return-object v0

    .line 650
    :sswitch_0
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 652
    :sswitch_1
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 654
    :sswitch_2
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 656
    :sswitch_3
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 658
    :sswitch_4
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 648
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
    .end sparse-switch
.end method

.method private getCategoryTypeReqest(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    .locals 2
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 614
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 626
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    :goto_0
    return-object v0

    .line 616
    :pswitch_0
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 618
    :pswitch_1
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 620
    :pswitch_2
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 622
    :pswitch_3
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 624
    :pswitch_4
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 614
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getScore(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;Z)V
    .locals 3
    .param p1, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    .param p2, "animation"    # Z

    .prologue
    .line 167
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;-><init>(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mGetScoreAsyncTask:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;

    .line 168
    iput-boolean p2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mAnimationEnable:Z

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mGetScoreAsyncTask:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 170
    return-void
.end method

.method private getScoreUpdateStringId(I)I
    .locals 1
    .param p1, "requestCode"    # I

    .prologue
    const v0, 0x7f090c74

    .line 665
    sparse-switch p1, :sswitch_data_0

    .line 677
    :goto_0
    :sswitch_0
    return v0

    .line 669
    :sswitch_1
    const v0, 0x7f090c75

    goto :goto_0

    .line 671
    :sswitch_2
    const v0, 0x7f090c71

    goto :goto_0

    .line 673
    :sswitch_3
    const v0, 0x7f090c76

    goto :goto_0

    .line 675
    :sswitch_4
    const v0, 0x7f090c77

    goto :goto_0

    .line 665
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
    .end sparse-switch
.end method

.method private initScoreView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getImageCache()Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->initScoreView(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;)V

    .line 292
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getImageCache()Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->initScoreView(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;)V

    .line 293
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getImageCache()Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->initScoreView(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;)V

    .line 294
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getImageCache()Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->initScoreView(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;)V

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getImageCache()Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->initScoreView(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;)V

    .line 297
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCurrentGoalCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v1, v0, v3, v3, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setSummaryHeaderMode(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;ZZZ)V

    .line 302
    :goto_0
    return-void

    .line 300
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v3, v3, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setSummaryHeaderMode(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;ZZZ)V

    goto :goto_0
.end method

.method private initScoreViewListner()V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnNoScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnNoScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnNoScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnNoScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnNoScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnScoreLayoutExpandCompleteListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnScoreLayoutExpandCompleteListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnScoreLayoutExpandCompleteListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnScoreLayoutExpandCompleteListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setOnScoreLayoutExpandCompleteListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;)V

    .line 322
    return-void
.end method

.method private showAllCategoryScoreView()V
    .locals 3

    .prologue
    .line 215
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v1

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScores(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v0

    .line 216
    .local v0, "scores":Lcom/cigna/coach/apiobjects/Scores;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateHeaderView(Lcom/cigna/coach/apiobjects/Scores;)V

    .line 217
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v1

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateNoScoreView(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)V

    .line 218
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScoreViewTopMargin(I)V

    .line 219
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Scores;->getCategoryInfo()Ljava/util/Hashtable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScore(Ljava/util/Hashtable;)V

    .line 220
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mOverallScore:I

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->invalidateOptionsMenu()V

    .line 222
    return-void
.end method

.method private startAssessmentActivity(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 5
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 432
    const/4 v2, 0x0

    .line 433
    .local v2, "requestCode":I
    const/4 v0, 0x0

    .line 435
    .local v0, "category":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 460
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 461
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "intent_lifestyle_category"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 462
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 463
    return-void

    .line 437
    .end local v1    # "intent":Landroid/content/Intent;
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 438
    const/16 v2, 0x64

    .line 439
    goto :goto_0

    .line 441
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 442
    const/16 v2, 0xc8

    .line 443
    goto :goto_0

    .line 445
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 446
    const/16 v2, 0x12c

    .line 447
    goto :goto_0

    .line 449
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 450
    const/16 v2, 0x190

    .line 451
    goto :goto_0

    .line 453
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 454
    const/16 v2, 0x1f4

    .line 455
    goto :goto_0

    .line 435
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private startBadgeActivity(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V
    .locals 3
    .param p2, "category"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 556
    .local p1, "badgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 557
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_NEED_UPATE_MAIN_SCORE_ANIMATION"

    const/16 v2, 0x56ce

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 558
    const-string v1, "intent_lifestyle_category"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 559
    const-string v1, "EXTRA_NAME_BADGE_VIEW_MODE"

    const/16 v2, 0x115c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 560
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 562
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->convertCategoryToRequestCode(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 563
    return-void
.end method

.method private startCignaCoachMessageActivity(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 4
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 466
    const/4 v0, 0x0

    .line 467
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x0

    .line 468
    .local v1, "score":I
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 488
    :goto_0
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 489
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v2, "intent_category_type"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 490
    const-string v2, "EXTRA_NAME_SCORE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 491
    const/16 v2, 0x320

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 492
    return-void

    .line 470
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getCurrentScore()I

    move-result v1

    .line 471
    goto :goto_0

    .line 473
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getCurrentScore()I

    move-result v1

    .line 474
    goto :goto_0

    .line 476
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getCurrentScore()I

    move-result v1

    .line 477
    goto :goto_0

    .line 479
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getCurrentScore()I

    move-result v1

    .line 480
    goto :goto_0

    .line 482
    :pswitch_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getCurrentScore()I

    move-result v1

    .line 483
    goto :goto_0

    .line 468
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private stopAnimation()V
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->stopAnimation()V

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->stopAnimation()V

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->stopAnimation()V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->stopAnimation()V

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->stopAnimation()V

    .line 416
    return-void
.end method

.method private updateHeaderView(Lcom/cigna/coach/apiobjects/Scores;)V
    .locals 4
    .param p1, "scores"    # Lcom/cigna/coach/apiobjects/Scores;

    .prologue
    const/4 v3, 0x0

    .line 282
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v0

    if-ltz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v1

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Scores;->getOutOfMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setCurrentScoreInfo(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :goto_0
    return-void

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateNoScoreView(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)V
    .locals 3
    .param p1, "score"    # I
    .param p2, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .prologue
    .line 252
    if-gez p1, :cond_0

    const/4 v0, 0x1

    .line 253
    .local v0, "noScore":Z
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryTypeRequest:[I

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 279
    :goto_1
    return-void

    .line 252
    .end local v0    # "noScore":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 255
    .restart local v0    # "noScore":Z
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setVisibilityNoScoreLayout(Z)V

    .line 256
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setVisibilityNoScoreLayout(Z)V

    .line 257
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setVisibilityNoScoreLayout(Z)V

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setVisibilityNoScoreLayout(Z)V

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setVisibilityNoScoreLayout(Z)V

    goto :goto_1

    .line 262
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setVisibilityNoScoreLayout(Z)V

    goto :goto_1

    .line 265
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setVisibilityNoScoreLayout(Z)V

    goto :goto_1

    .line 268
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setVisibilityNoScoreLayout(Z)V

    goto :goto_1

    .line 271
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setVisibilityNoScoreLayout(Z)V

    goto :goto_1

    .line 274
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setVisibilityNoScoreLayout(Z)V

    goto :goto_1

    .line 253
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private updateScore(Ljava/util/Hashtable;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226
    .local p1, "categoryInfo":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 228
    .local v0, "ect":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 229
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 230
    .local v2, "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual {p1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 231
    .local v1, "i":Ljava/lang/Integer;
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 233
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mAnimationEnable:Z

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->updateScore(IZ)V

    goto :goto_0

    .line 236
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mAnimationEnable:Z

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->updateScore(IZ)V

    goto :goto_0

    .line 239
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mAnimationEnable:Z

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->updateScore(IZ)V

    goto :goto_0

    .line 242
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mAnimationEnable:Z

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->updateScore(IZ)V

    goto :goto_0

    .line 245
    :pswitch_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mAnimationEnable:Z

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->updateScore(IZ)V

    goto :goto_0

    .line 249
    .end local v1    # "i":Ljava/lang/Integer;
    .end local v2    # "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :cond_0
    return-void

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private updateScoreViewTopMargin(I)V
    .locals 1
    .param p1, "score"    # I

    .prologue
    .line 325
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScoreViewTopMargin(ILcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScoreViewTopMargin(ILcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScoreViewTopMargin(ILcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScoreViewTopMargin(ILcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScoreViewTopMargin(ILcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V

    .line 330
    return-void
.end method

.method private updateScoreViewTopMargin(ILcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V
    .locals 4
    .param p1, "score"    # I
    .param p2, "scoreView"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .prologue
    .line 334
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    if-ne v2, v3, :cond_1

    .line 335
    if-gez p1, :cond_0

    .line 336
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mCignaExerciseScoreTopMarginView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 351
    :goto_0
    return-void

    .line 338
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mCignaExerciseScoreTopMarginView:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 341
    :cond_1
    const/4 v1, 0x0

    .line 342
    .local v1, "margin":I
    if-gez p1, :cond_2

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a01ff

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v1, v2

    .line 347
    :goto_1
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 348
    .local v0, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 349
    invoke-virtual {p2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 345
    .end local v0    # "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0200

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v1, v2

    goto :goto_1
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 497
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 499
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 501
    if-eqz p3, :cond_1

    .line 502
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mReceiveBadgeIds:Ljava/util/ArrayList;

    .line 503
    const-string v1, "intent_lifestyle_category"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mLifestyleCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 512
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mReceiveBadgeIds:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mReceiveBadgeIds:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mLifestyleCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v1, :cond_3

    .line 513
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mToDoStartBadgeActivity:Z

    .line 530
    :cond_0
    :goto_1
    return-void

    .line 505
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mReceiveBadgeIds:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 506
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mReceiveBadgeIds:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 507
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mReceiveBadgeIds:Ljava/util/ArrayList;

    .line 509
    :cond_2
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mLifestyleCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    goto :goto_0

    .line 515
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCurrentGoalCount()I

    move-result v1

    if-nez v1, :cond_4

    .line 516
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v2, v1, v4, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setSummaryHeaderMode(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;ZZZ)V

    .line 519
    :cond_4
    const/16 v1, 0x320

    if-ne p1, v1, :cond_5

    .line 520
    if-eqz p3, :cond_0

    .line 521
    const-string v1, "intent_category_type"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 522
    .local v0, "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getCategoryTypeReqest(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    move-result-object v1

    invoke-direct {p0, v1, v4}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getScore(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;Z)V

    goto :goto_1

    .line 525
    .end local v0    # "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getScoreUpdateStringId(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 526
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getCategoryTypeReqest(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    move-result-object v1

    invoke-direct {p0, v1, v5}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getScore(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;Z)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 387
    const/4 v0, 0x0

    .line 389
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 408
    :goto_0
    return-void

    .line 392
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->stopAnimation()V

    .line 394
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 395
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 400
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mGetScoreAsyncTask:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;

    if-eqz v1, :cond_0

    .line 401
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mGetScoreAsyncTask:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->cancel(Z)Z

    .line 404
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->onChangeFragmentBtnClick()V

    goto :goto_0

    .line 389
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080122 -> :sswitch_0
        0x7f0801ff -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 78
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->setHasOptionsMenu(Z)V

    .line 79
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 355
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 358
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 359
    instance-of v1, v0, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;

    if-eqz v1, :cond_0

    .line 360
    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->isMenuShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 361
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mOverallScore:I

    if-ltz v1, :cond_1

    .line 362
    const v1, 0x7f100009

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 373
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    const v1, 0x7f10000a

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 84
    const v4, 0x7f030083

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    .line 86
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    const v5, 0x7f080279

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    .line 87
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    const v5, 0x7f08027b

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .line 88
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    const v5, 0x7f08027c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .line 89
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    const v5, 0x7f08027d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .line 90
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    const v5, 0x7f08027e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .line 91
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    const v5, 0x7f08027f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .line 93
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    const v5, 0x7f08027a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mCignaExerciseScoreTopMarginView:Landroid/view/View;

    .line 95
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    const v5, 0x7f080122

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 96
    .local v2, "cignaMainSetGoalBtnLayout":Landroid/widget/LinearLayout;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090031

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    const v5, 0x7f0801ff

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 98
    .local v1, "cignaMainChangeFragmentBtn":Landroid/widget/ImageButton;
    invoke-virtual {v2, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->initScoreView()V

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->initScoreViewListner()V

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 105
    .local v3, "forcedScoreScreen":Z
    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CignaSummaryFragment onCreateView() forcedScoreScreen: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    if-eqz v3, :cond_0

    .line 108
    sget-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-direct {p0, v4, v8}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getScore(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;Z)V

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 122
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->invalidateOptionsMenu()V

    .line 124
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    return-object v4

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "EXTRA_NAME_FIRST_ASSESSMENT_COMPLETE_CATEGORY"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 112
    .local v0, "category":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->isCignaCoachCompleteAssessment()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 114
    invoke-static {v7}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setisCignaCoachCompleteAssessment(Z)V

    .line 115
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getAssessmentComplete(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v7}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 116
    sget-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-direct {p0, v4, v8}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getScore(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;Z)V

    goto :goto_0

    .line 118
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->showAllCategoryScoreView()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 141
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onDestroy()V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->clearCategoryAnimation()V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->clearCategoryAnimation()V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->clearCategoryAnimation()V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->clearCategoryAnimation()V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->clearCategoryAnimation()V

    .line 147
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 152
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onDestroyView()V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->clearScoreMessageAnimation()V

    .line 154
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 159
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onDetach()V

    .line 160
    return-void
.end method

.method public onNoScoreClick(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 0
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 421
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->startAssessmentActivity(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 422
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 129
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onResume()V

    .line 131
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mToDoStartBadgeActivity:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mLifestyleCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v0, :cond_0

    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->TAG:Ljava/lang/String;

    const-string v1, "Beaucase there is received badge, started BadgeActivity."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mToDoStartBadgeActivity:Z

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mReceiveBadgeIds:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mLifestyleCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->startBadgeActivity(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V

    .line 136
    :cond_0
    return-void
.end method

.method public onScoreClick(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 0
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 427
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->startCignaCoachMessageActivity(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 428
    return-void
.end method

.method public onScoreLayoutExpand()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f080122

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0801ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 600
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreExerciseView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setFocusForFocusableView()V

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreFoodView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setFocusForFocusableView()V

    .line 602
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreSleepView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setFocusForFocusableView()V

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreStressView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setFocusForFocusableView()V

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setFocusForFocusableView()V

    .line 606
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->refreshFragmentFocusables()V

    .line 607
    return-void
.end method

.method public setAnimationEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 610
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mAnimationEnable:Z

    .line 611
    return-void
.end method

.method public updateScoreByWeightTrackerBR()V
    .locals 5

    .prologue
    .line 580
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    if-eqz v2, :cond_0

    .line 581
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getCurrentScore()I

    move-result v1

    .line 582
    .local v1, "oldWeightScore":I
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScores(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getCategoryScore(Lcom/cigna/coach/apiobjects/Scores;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)I

    move-result v0

    .line 586
    .local v0, "newWeightScore":I
    if-eq v1, v0, :cond_0

    .line 587
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mScoreWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    sget-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getImageCache()Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->initScoreView(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;)V

    .line 588
    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getScore(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;Z)V

    .line 591
    .end local v0    # "newWeightScore":I
    .end local v1    # "oldWeightScore":I
    :cond_0
    return-void
.end method
