.class Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment$1;
.super Ljava/lang/Object;
.source "ExerciseProGraphFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutSizeChanged(II)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mSicGraphView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 94
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onLayoutSizeChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    return-void
.end method
