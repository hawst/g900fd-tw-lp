.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$2;
.super Ljava/lang/Object;
.source "ExerciseProMapActivity.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isInCenter:Z

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setLocationModeIcon(Z)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationToCenterInProgress:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$102(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)Z

    .line 245
    return-void
.end method

.method public onFinish()V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isInCenter:Z

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isInCenter:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setLocationModeIcon(Z)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationToCenterInProgress:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$102(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)Z

    .line 238
    return-void
.end method
