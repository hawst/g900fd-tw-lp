.class public final enum Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;
.super Ljava/lang/Enum;
.source "IListItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ListItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

.field public static final enum FOOD:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

.field public static final enum MEAL:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

.field public static final enum MEAL_ITEM:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    const-string v1, "FOOD"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->FOOD:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    .line 61
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    const-string v1, "MEAL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->MEAL:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    .line 65
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    const-string v1, "MEAL_ITEM"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->MEAL_ITEM:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    .line 53
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->FOOD:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->MEAL:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->MEAL_ITEM:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->$VALUES:[Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 53
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->$VALUES:[Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    return-object v0
.end method
