.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$6;
.super Landroid/database/ContentObserver;
.source "FoodTrackerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getContentObserver()Landroid/database/ContentObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 606
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 609
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 610
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initAccordingChanges()V

    .line 613
    :cond_0
    return-void
.end method
