.class public final enum Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
.super Ljava/lang/Enum;
.source "WeightUnitHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WeightType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

.field public static final enum KG_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

.field public static final enum LB_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

.field public static final enum NOT_DEFINED:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;


# instance fields
.field private mFullName:Ljava/lang/String;

.field private mUnitNameId:I

.field private mUntranslatableName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 53
    new-instance v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    const-string v1, "KG_UNIT"

    const-string v2, "kg"

    const v3, 0x7f0900c0

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->KG_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    .line 54
    new-instance v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    const-string v1, "LB_UNIT"

    const-string v2, "lb"

    const v3, 0x7f0900c2

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->LB_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    .line 55
    new-instance v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    const-string v1, "NOT_DEFINED"

    const-string/jumbo v2, "not defined"

    const v3, 0x7f090d0f

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->NOT_DEFINED:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    .line 52
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->KG_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->LB_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->NOT_DEFINED:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->$VALUES:[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "unitId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 87
    iput-object p3, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->mUntranslatableName:Ljava/lang/String;

    .line 88
    iput p4, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->mUnitNameId:I

    .line 89
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->mUntranslatableName:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 52
    const-class v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->$VALUES:[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    return-object v0
.end method


# virtual methods
.method public getFullName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->mFullName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 81
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->mFullName:Ljava/lang/String;

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->mFullName:Ljava/lang/String;

    return-object v0
.end method

.method public getUnitName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->mUnitNameId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
