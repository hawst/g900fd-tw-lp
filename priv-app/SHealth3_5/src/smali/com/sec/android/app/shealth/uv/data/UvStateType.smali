.class public final enum Lcom/sec/android/app/shealth/uv/data/UvStateType;
.super Ljava/lang/Enum;
.source "UvStateType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/uv/data/UvStateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/uv/data/UvStateType;

.field public static final enum EXTREME:Lcom/sec/android/app/shealth/uv/data/UvStateType;

.field public static final enum HIGH:Lcom/sec/android/app/shealth/uv/data/UvStateType;

.field public static final enum INVALID:Lcom/sec/android/app/shealth/uv/data/UvStateType;

.field public static final enum LOW:Lcom/sec/android/app/shealth/uv/data/UvStateType;

.field public static final enum MODERATE:Lcom/sec/android/app/shealth/uv/data/UvStateType;

.field public static final enum VERY_HIGH:Lcom/sec/android/app/shealth/uv/data/UvStateType;


# instance fields
.field private colorResource:I

.field private safetyTime:J

.field private score:D

.field private spfValue:I

.field private state:I

.field private stringResource:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/shealth/uv/data/UvStateType;

    const-string v1, "INVALID"

    const v4, 0x7f090c3e

    const v5, 0x106000d

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/uv/data/UvStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->INVALID:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    .line 7
    new-instance v3, Lcom/sec/android/app/shealth/uv/data/UvStateType;

    const-string v4, "LOW"

    const v7, 0x7f090d94

    const v8, 0x7f07026e

    move v5, v9

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/uv/data/UvStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/sec/android/app/shealth/uv/data/UvStateType;->LOW:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    .line 8
    new-instance v3, Lcom/sec/android/app/shealth/uv/data/UvStateType;

    const-string v4, "MODERATE"

    const v7, 0x7f090d95

    const v8, 0x7f07026f

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/uv/data/UvStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/sec/android/app/shealth/uv/data/UvStateType;->MODERATE:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    .line 9
    new-instance v3, Lcom/sec/android/app/shealth/uv/data/UvStateType;

    const-string v4, "HIGH"

    const v7, 0x7f090d96

    const v8, 0x7f070270

    move v5, v11

    move v6, v11

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/uv/data/UvStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/sec/android/app/shealth/uv/data/UvStateType;->HIGH:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    .line 10
    new-instance v3, Lcom/sec/android/app/shealth/uv/data/UvStateType;

    const-string v4, "VERY_HIGH"

    const v7, 0x7f090d97

    const v8, 0x7f070271

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/uv/data/UvStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/sec/android/app/shealth/uv/data/UvStateType;->VERY_HIGH:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    .line 11
    new-instance v3, Lcom/sec/android/app/shealth/uv/data/UvStateType;

    const-string v4, "EXTREME"

    const/4 v5, 0x5

    const/4 v6, 0x5

    const v7, 0x7f090d98

    const v8, 0x7f070272

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/uv/data/UvStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/sec/android/app/shealth/uv/data/UvStateType;->EXTREME:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    .line 5
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/shealth/uv/data/UvStateType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/data/UvStateType;->INVALID:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/uv/data/UvStateType;->LOW:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/sec/android/app/shealth/uv/data/UvStateType;->MODERATE:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/sec/android/app/shealth/uv/data/UvStateType;->HIGH:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/sec/android/app/shealth/uv/data/UvStateType;->VERY_HIGH:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/uv/data/UvStateType;->EXTREME:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->$VALUES:[Lcom/sec/android/app/shealth/uv/data/UvStateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 2
    .param p3, "state"    # I
    .param p4, "stringResource"    # I
    .param p5, "colorResource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput p3, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->state:I

    .line 22
    iput p4, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->stringResource:I

    .line 23
    iput p5, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->colorResource:I

    .line 24
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->safetyTime:J

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->spfValue:I

    .line 26
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/uv/data/UvStateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/sec/android/app/shealth/uv/data/UvStateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/uv/data/UvStateType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/uv/data/UvStateType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->$VALUES:[Lcom/sec/android/app/shealth/uv/data/UvStateType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/uv/data/UvStateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/uv/data/UvStateType;

    return-object v0
.end method


# virtual methods
.method public getSPFvalue()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->spfValue:I

    return v0
.end method

.method public getSafetyTime()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->safetyTime:J

    return-wide v0
.end method

.method public getScore()D
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->score:D

    return-wide v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->state:I

    return v0
.end method

.method public getStringResource()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->stringResource:I

    return v0
.end method

.method public setSPFvalue(I)V
    .locals 0
    .param p1, "spfValue"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->spfValue:I

    .line 64
    return-void
.end method

.method public setSafetyTime(J)V
    .locals 0
    .param p1, "safetyTime"    # J

    .prologue
    .line 49
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->safetyTime:J

    .line 52
    return-void
.end method

.method public setScore(D)V
    .locals 0
    .param p1, "score"    # D

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->score:D

    .line 42
    return-void
.end method
