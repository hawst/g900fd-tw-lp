.class Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$ResponseListenerHandler;
.super Ljava/lang/Object;
.source "FirstBeatRegisterThread.java"

# interfaces
.implements Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResponseListenerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$ResponseListenerHandler;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$1;

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$ResponseListenerHandler;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;)V

    return-void
.end method


# virtual methods
.method public onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "ex"    # Lcom/sec/android/service/health/connectionmanager2/NetException;
    .param p5, "tag"    # Ljava/lang/Object;
    .param p6, "ReRequest"    # Ljava/lang/Object;

    .prologue
    .line 151
    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getHttpResCode()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$ResponseListenerHandler;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mCondResponse:Landroid/os/ConditionVariable;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;)Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 154
    return-void
.end method

.method public onRequestCancelled(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "tag"    # Ljava/lang/Object;
    .param p5, "ReRequest"    # Ljava/lang/Object;

    .prologue
    .line 158
    return-void
.end method

.method public onResponseReceived(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 13
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "result"    # Ljava/lang/Object;
    .param p5, "tag"    # Ljava/lang/Object;

    .prologue
    .line 117
    const/4 v8, 0x0

    .line 118
    .local v8, "respString":Ljava/lang/String;
    move-object/from16 v0, p4

    instance-of v10, v0, Ljava/lang/String;

    if-eqz v10, :cond_1

    move-object/from16 v8, p4

    .line 119
    check-cast v8, Ljava/lang/String;

    .line 127
    .end local p4    # "result":Ljava/lang/Object;
    :goto_0
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->access$200()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Object Response["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const/16 v10, 0xc8

    move/from16 v0, p3

    if-ne v0, v10, :cond_2

    .line 130
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v4

    .line 131
    .local v4, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v4}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v1

    .line 132
    .local v1, "builder":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v5, Ljava/io/ByteArrayInputStream;

    const-string/jumbo v10, "utf-8"

    invoke-virtual {v8, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 133
    .local v5, "istream":Ljava/io/InputStream;
    invoke-virtual {v1, v5}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v2

    .line 134
    .local v2, "doc":Lorg/w3c/dom/Document;
    const-string v10, "HBase"

    invoke-interface {v2, v10}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    .line 135
    .local v7, "nodes":Lorg/w3c/dom/NodeList;
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 136
    .local v6, "node":Lorg/w3c/dom/Node;
    invoke-interface {v6}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 146
    .end local v1    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v2    # "doc":Lorg/w3c/dom/Document;
    .end local v4    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v5    # "istream":Ljava/io/InputStream;
    .end local v6    # "node":Lorg/w3c/dom/Node;
    .end local v7    # "nodes":Lorg/w3c/dom/NodeList;
    :cond_0
    :goto_1
    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$ResponseListenerHandler;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mCondResponse:Landroid/os/ConditionVariable;
    invoke-static {v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;)Landroid/os/ConditionVariable;

    move-result-object v10

    invoke-virtual {v10}, Landroid/os/ConditionVariable;->open()V

    .line 147
    :goto_2
    return-void

    .line 122
    .restart local p4    # "result":Ljava/lang/Object;
    :cond_1
    :try_start_1
    new-instance v9, Ljava/lang/String;

    check-cast p4, [B

    .end local p4    # "result":Ljava/lang/Object;
    check-cast p4, [B

    move-object/from16 v0, p4

    invoke-direct {v9, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .end local v8    # "respString":Ljava/lang/String;
    .local v9, "respString":Ljava/lang/String;
    move-object v8, v9

    .line 125
    .end local v9    # "respString":Ljava/lang/String;
    .restart local v8    # "respString":Ljava/lang/String;
    goto :goto_0

    .line 123
    :catch_0
    move-exception v3

    .line 124
    .local v3, "e":Ljava/lang/Exception;
    goto :goto_2

    .line 137
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 138
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 140
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_2
    const/16 v10, 0x12c

    move/from16 v0, p3

    if-ne v0, v10, :cond_0

    .line 141
    const-string v10, "\"success\""

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "\"Already activated\""

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 142
    :cond_3
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->access$200()Ljava/lang/String;

    move-result-object v10

    const-string v11, "SharedPreferencesHelper.setFirstbeatAscrActivated(true)"

    invoke-static {v10, v11}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    const/4 v10, 0x1

    invoke-static {v10}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setFirstbeatAscrActivated(Z)V

    goto :goto_1
.end method
