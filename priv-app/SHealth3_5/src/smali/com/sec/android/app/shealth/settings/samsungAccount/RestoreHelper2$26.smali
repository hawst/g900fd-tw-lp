.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showPopupWithProgressBar(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

.field final synthetic val$popup_msg:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;I)V
    .locals 0

    .prologue
    .line 1835
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    iput p2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->val$popup_msg:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1839
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    if-ne v1, v2, :cond_0

    .line 1894
    :goto_0
    return-void

    .line 1841
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4302(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1842
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)I

    move-result v1

    if-nez v1, :cond_1

    const v0, 0x7f090139

    .line 1844
    .local v0, "titleId":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1845
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1846
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f030017

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1866
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1878
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1890
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1502(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1891
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setDoNotDismissOnBackPressed(Z)V

    .line 1892
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->RESTORATION_PROGRESS_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 1893
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v3, "RESTORE_PROGRESS_POPUP"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1842
    .end local v0    # "titleId":I
    :cond_1
    const v0, 0x7f090055

    goto :goto_1
.end method
