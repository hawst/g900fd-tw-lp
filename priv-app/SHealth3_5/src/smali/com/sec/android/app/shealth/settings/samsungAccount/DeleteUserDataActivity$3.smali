.class Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;
.super Ljava/lang/Object;
.source "DeleteUserDataActivity.java"

# interfaces
.implements Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->onActivityResult(IILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V
    .locals 5
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "isSuccess"    # Z
    .param p3, "taskError"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    .param p4, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 136
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    if-ne p3, v2, :cond_1

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->TAG:Ljava/lang/String;

    const-string v3, "Coach EraseDataAndReset succeeded"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :goto_0
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    .line 146
    .local v1, "shcm":Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    const v2, 0x1869c

    :try_start_0
    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;)V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->deleteBackUp(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 211
    :cond_0
    :goto_1
    return-void

    .line 140
    .end local v1    # "shcm":Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Coach EraseDataAndReset failed with error code : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 196
    .restart local v1    # "shcm":Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException occured"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->cancel()V

    goto :goto_1

    .line 204
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->TAG:Ljava/lang/String;

    const-string v3, "Exception occured"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 208
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->cancel()V

    goto :goto_1
.end method

.method public onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    .locals 0
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "fractionCompleted"    # D

    .prologue
    .line 132
    return-void
.end method

.method public onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V
    .locals 2
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->TAG:Ljava/lang/String;

    const-string v1, "Coach EraseDataAndReset started"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->show()V

    .line 129
    :cond_0
    return-void
.end method
