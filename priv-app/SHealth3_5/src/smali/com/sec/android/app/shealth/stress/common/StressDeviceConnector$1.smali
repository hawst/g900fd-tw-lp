.class Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;
.super Ljava/lang/Object;
.source "StressDeviceConnector.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 252
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onJoined"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$100(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$200(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "startReceivingData_2"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$100(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    iget-object v1, v1, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->isRecord:Z

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$100(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->record()V

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$202(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :goto_0
    return-void

    .line 262
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onLeft(I)V
    .locals 5
    .param p1, "error"    # I

    .prologue
    const/4 v4, 0x0

    .line 269
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onLeft : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$100(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$200(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "stopReceivingData_2"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$100(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$202(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;Z)Z

    .line 289
    :goto_0
    return-void

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$202(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;Z)Z

    goto :goto_0

    .line 277
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 278
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$202(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;Z)Z

    goto :goto_0

    .line 279
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v0

    .line 280
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :try_start_3
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$202(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;Z)Z

    goto :goto_0

    .line 281
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 282
    .local v0, "e":Ljava/lang/NullPointerException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$202(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;Z)Z

    goto :goto_0

    .line 283
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v0

    .line 285
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :try_start_5
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$202(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;Z)Z

    goto :goto_0

    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$202(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;Z)Z

    throw v1
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 3
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .param p2, "response"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    .prologue
    .line 293
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Response ---------"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response commandId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getCommandId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response errorDescription : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getErrorDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    return-void
.end method

.method public onStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 302
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response onStateChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    return-void
.end method
