.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$10;
.super Ljava/lang/Object;
.source "RestoreActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V
    .locals 0

    .prologue
    .line 799
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPress(Landroid/app/Activity;)V
    .locals 4
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 803
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cancel is clicked for all work by back key"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 806
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 808
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v1, 0x7f0808b1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 809
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isOngoing:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 810
    return-void
.end method
