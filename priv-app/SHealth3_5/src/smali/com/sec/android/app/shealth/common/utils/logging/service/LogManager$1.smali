.class Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$1;
.super Landroid/content/BroadcastReceiver;
.source "LogManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->registSPPReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V
    .locals 0

    .prologue
    .line 1379
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1382
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    .line 1383
    :cond_0
    const-string v4, "LogManager"

    const-string v5, "Invailid Params"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1442
    :cond_1
    :goto_0
    return-void

    .line 1386
    :cond_2
    const-string v4, "LogManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Receive broadcast msg - action: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1388
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1389
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 1391
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 1392
    const-string v4, "EXTRA_STR_ACTION"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1394
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 1395
    const-string v4, "LogManager"

    const-string v5, "EXTRA_STR_ACTION is null"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1398
    :cond_3
    const-string v4, "LogManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Recieve broadcast msg - extra: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1400
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.logsample.DLC_FILTER"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1401
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 1403
    .restart local v1    # "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 1404
    const-string v4, "EXTRA_STR_ACTION"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1406
    .restart local v0    # "action":Ljava/lang/String;
    if-nez v0, :cond_5

    .line 1407
    const-string v4, "LogManager"

    const-string v5, "EXTRA_STR_ACTION is null"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1410
    :cond_5
    const-string v4, "LogManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Recieve broadcast msg - extra: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1412
    const-string v4, "ACTION_RESULT_REGISTER"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1413
    const-string v4, "EXTRA_RESULT_CODE"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1414
    .local v3, "resultCode":I
    const-string v4, "EXTRA_STR"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1415
    .local v2, "result":Ljava/lang/String;
    const-string v4, "LogManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Register result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->fromInt(I)Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1417
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsResponseReceived:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1302(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Z)Z

    .line 1419
    if-eqz v2, :cond_6

    sget-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_SUCCESS:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1420
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    # invokes: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->setRegistered()V
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1700(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V

    goto/16 :goto_0

    .line 1422
    :cond_6
    const-string v4, "LogManager"

    const-string v5, "DLC registration is failed."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 1423
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->unregistSPPReceiver()V

    goto/16 :goto_0

    .line 1425
    .end local v2    # "result":Ljava/lang/String;
    .end local v3    # "resultCode":I
    :cond_7
    const-string v4, "ACTION_RESULT_DEREGISTER"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1426
    const-string v4, "EXTRA_RESULT_CODE"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1427
    .restart local v3    # "resultCode":I
    const-string v4, "EXTRA_STR"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1428
    .restart local v2    # "result":Ljava/lang/String;
    const-string v4, "LogManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Deregister result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->fromInt(I)Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1430
    if-eqz v2, :cond_1

    sget-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->RESULT_SUCCESS:Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/RegiEvent;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1431
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    # invokes: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->setDeregistered()V
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1800(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V

    goto/16 :goto_0

    .line 1433
    .end local v2    # "result":Ljava/lang/String;
    .end local v3    # "resultCode":I
    :cond_8
    const-string v4, "available"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1434
    const-string v4, "LogManager"

    const-string v5, "ACTION LOG AVAILABLE"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1436
    :cond_9
    const-string/jumbo v4, "unavailable"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1437
    const-string v4, "LogManager"

    const-string v5, "ACTION LOG UNAVAILABLE"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
