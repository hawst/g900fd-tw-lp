.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;
.super Ljava/lang/Object;
.source "ExerciseProMapActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->onNewLocation(Landroid/location/Location;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

.field final synthetic val$distanceFromCenter:D

.field final synthetic val$location:Landroid/location/Location;

.field final synthetic val$thresholdDistance:D


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;DDLandroid/location/Location;)V
    .locals 0

    .prologue
    .line 645
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

    iput-wide p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->val$distanceFromCenter:D

    iput-wide p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->val$thresholdDistance:D

    iput-object p6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->val$location:Landroid/location/Location;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 649
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->val$distanceFromCenter:D

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->val$thresholdDistance:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isManuallyDragged:Z

    if-nez v0, :cond_0

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->playMapFromCacheDB(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)V

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->val$location:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->val$location:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2300()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationCallback:Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    .line 655
    :cond_0
    return-void
.end method
