.class public Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;
.super Landroid/widget/BaseAdapter;
.source "ItemsAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/IListItem;",
        ">",
        "Landroid/widget/BaseAdapter;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field protected static final LIST_ITEM_INDEX_TAG:I = 0x7f0d0043


# instance fields
.field protected context:Landroid/content/Context;

.field protected inflater:Landroid/view/LayoutInflater;

.field protected itemsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation
.end field

.field private onItemClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<TE;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->context:Landroid/content/Context;

    .line 43
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->itemsList:Ljava/util/List;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<TE;>;"
    .local p2, "allItemsList":Ljava/util/List;, "Ljava/util/List<TE;>;"
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;-><init>(Landroid/content/Context;)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 50
    return-void
.end method


# virtual methods
.method public addListItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<TE;>;"
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 59
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 65
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 66
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 70
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 75
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<TE;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 80
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<TE;>;"
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 85
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<TE;>;"
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/IListItem;

    .line 86
    .local v0, "listItem":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/IListItem;, "TE;"
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->inflater:Landroid/view/LayoutInflater;

    invoke-interface {v0, v2, p2, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/IListItem;->getView(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 87
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0d0043

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 88
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    return-object v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 95
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->onItemClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;

    if-eqz v0, :cond_0

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->onItemClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;

    const v0, 0x7f0d0043

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/IListItem;

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;->onItemClick(Ljava/lang/Object;)V

    .line 98
    :cond_0
    return-void
.end method

.method public setOnItemClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<TE;>;"
    .local p1, "onItemClickListener":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener<TE;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->onItemClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;

    .line 102
    return-void
.end method
