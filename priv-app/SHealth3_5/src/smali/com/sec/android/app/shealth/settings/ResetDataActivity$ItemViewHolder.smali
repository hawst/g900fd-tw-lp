.class interface abstract Lcom/sec/android/app/shealth/settings/ResetDataActivity$ItemViewHolder;
.super Ljava/lang/Object;
.source "ResetDataActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/ResetDataActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "ItemViewHolder"
.end annotation


# virtual methods
.method public abstract getItemCheckBox()Landroid/widget/CheckBox;
.end method

.method public abstract getItemContainer()Landroid/widget/RelativeLayout;
.end method

.method public abstract getMainTextView()Landroid/widget/TextView;
.end method

.method public abstract setItemCheckBox(Landroid/widget/CheckBox;)V
.end method

.method public abstract setItemContainer(Landroid/widget/RelativeLayout;)V
.end method

.method public abstract setMainTextView(Landroid/widget/TextView;)V
.end method
