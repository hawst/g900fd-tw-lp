.class public Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HeartrateLogByTagDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;,
        Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;
    }
.end annotation


# instance fields
.field private lvTagDefaultList:Landroid/widget/ListView;

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mStringMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mTagDefaultListadapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;

.field private maxHeartrate:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->lvTagDefaultList:Landroid/widget/ListView;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mStringMap:Ljava/util/HashMap;

    .line 56
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->maxHeartrate:J

    .line 88
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$2;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 367
    return-void
.end method

.method private getStringId(Ljava/lang/String;)I
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 459
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mStringMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mStringMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 461
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setDefaultTagList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 465
    .local p1, "tagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;>;"
    .local p2, "tagIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v7, 0x0

    .line 466
    .local v7, "cursor":Landroid/database/Cursor;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 467
    .local v5, "tagId":I
    const/4 v9, 0x0

    .line 468
    .local v9, "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    const v4, 0x7f02053d

    .line 469
    .local v4, "iconId":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v11, 0x7f090f08

    invoke-virtual {v1, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 471
    .local v6, "tagName":Ljava/lang/String;
    if-lez v5, :cond_1

    .line 472
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTagByIndex(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v10

    .line 473
    .local v10, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    if-eqz v10, :cond_1

    .line 474
    iget v4, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    .line 476
    iget-object v1, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v11, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v1, v11, :cond_4

    .line 477
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v11, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v1, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 484
    .end local v10    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :cond_1
    :goto_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->executeGroupByTagQuery(I)Landroid/database/Cursor;

    move-result-object v7

    .line 485
    if-eqz v7, :cond_8

    .line 487
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_7

    .line 488
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 489
    const-string v1, "AvgMonth"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-long v2, v1

    .line 490
    .local v2, "avgMonth":J
    iget-wide v11, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->maxHeartrate:J

    cmp-long v1, v2, v11

    if-lez v1, :cond_2

    .line 491
    iput-wide v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->maxHeartrate:J

    .line 492
    :cond_2
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    const/4 v1, -0x1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;-><init>(IJIILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 493
    .end local v9    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .local v0, "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    const-wide/16 v11, 0x0

    cmp-long v1, v2, v11

    if-lez v1, :cond_3

    .line 494
    :try_start_1
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 497
    .end local v2    # "avgMonth":J
    :cond_3
    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 500
    :goto_3
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 501
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 479
    .end local v0    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .restart local v9    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .restart local v10    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :cond_4
    iget-object v6, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    goto :goto_1

    .line 500
    .end local v10    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :catchall_0
    move-exception v1

    move-object v0, v9

    .end local v9    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .restart local v0    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    :goto_4
    if-eqz v7, :cond_5

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v11

    if-nez v11, :cond_5

    .line 501
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1

    .line 506
    .end local v0    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .end local v4    # "iconId":I
    .end local v5    # "tagId":I
    .end local v6    # "tagName":Ljava/lang/String;
    :cond_6
    return-void

    .line 500
    .restart local v0    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .restart local v4    # "iconId":I
    .restart local v5    # "tagId":I
    .restart local v6    # "tagName":Ljava/lang/String;
    :catchall_1
    move-exception v1

    goto :goto_4

    .end local v0    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .restart local v9    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    :cond_7
    move-object v0, v9

    .end local v9    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .restart local v0    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    goto :goto_2

    .end local v0    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .restart local v9    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    :cond_8
    move-object v0, v9

    .end local v9    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .restart local v0    # "mTagEntry":Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    goto :goto_3
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 360
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090f13

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 362
    return-void
.end method

.method disableScroll(Landroid/widget/ListView;)V
    .locals 7
    .param p1, "mListVw"    # Landroid/widget/ListView;

    .prologue
    const/4 v6, 0x0

    .line 440
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 441
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_2

    .line 442
    const/4 v3, 0x0

    .line 443
    .local v3, "mHeight":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 444
    const/4 v5, 0x0

    invoke-interface {v0, v1, v5, p1}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 445
    .local v2, "listItem":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 446
    invoke-virtual {v2, v6, v6}, Landroid/view/View;->measure(II)V

    .line 447
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v3, v5

    .line 443
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 451
    .end local v2    # "listItem":Landroid/view/View;
    :cond_1
    invoke-virtual {p1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 452
    .local v4, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p1}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v5

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    mul-int/2addr v5, v6

    add-int/2addr v5, v3

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 453
    invoke-virtual {p1, v4}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 455
    .end local v1    # "i":I
    .end local v3    # "mHeight":I
    .end local v4    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_2
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "mCurrentConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 348
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 350
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f090f31

    const v5, 0x7f090f30

    const v2, 0x7f090f2f

    const v4, 0x7f090f2e

    const v3, 0x7f090f2d

    .line 62
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mStringMap:Ljava/util/HashMap;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mStringMap:Ljava/util/HashMap;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mStringMap:Ljava/util/HashMap;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mStringMap:Ljava/util/HashMap;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mStringMap:Ljava/util/HashMap;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    const v0, 0x7f03013c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->setContentView(I)V

    .line 72
    const v0, 0x7f08052b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->lvTagDefaultList:Landroid/widget/ListView;

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->lvTagDefaultList:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$1;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->lvTagDefaultList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 85
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 337
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 340
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 327
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 329
    return-void
.end method

.method protected onResume()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v2, 0x0

    .line 227
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 228
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 230
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 231
    .local v7, "customTagSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 232
    .local v12, "tagDefaultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 234
    .local v13, "tagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/LinkedHashMap;

    invoke-direct {v9}, Ljava/util/LinkedHashMap;-><init>()V

    .line 237
    .local v9, "mContainsTag":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/Integer;Ljava/lang/Boolean;>;"
    const/16 v0, 0x5208

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    const/16 v0, 0x5335

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    const/16 v0, 0x526f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    const/16 v0, 0x526d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    const/16 v0, 0x52d1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    const/16 v0, 0x52d2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    const/16 v0, 0x52d3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    const/16 v0, 0x52d4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    const/16 v0, 0x52d5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    const/16 v0, 0x52d6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    const/16 v0, 0x5338

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    const/16 v0, 0x5336

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    const/16 v0, 0x5337

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    const/4 v6, 0x0

    .line 252
    .local v6, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v0

    if-lez v0, :cond_7

    .line 254
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDistinctTags()Landroid/database/Cursor;

    move-result-object v6

    .line 256
    if-eqz v6, :cond_4

    .line 257
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 258
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 259
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_3

    .line 261
    const-string/jumbo v0, "tag_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 262
    const/4 v11, 0x0

    .line 263
    .local v11, "tag":I
    const-string/jumbo v0, "tag_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 265
    if-lez v11, :cond_0

    const/16 v0, 0x4e20

    if-eq v11, v0, :cond_0

    .line 266
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 267
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    .end local v11    # "tag":I
    :cond_0
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 279
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 280
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 269
    .restart local v11    # "tag":I
    :cond_2
    :try_start_1
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 276
    .end local v11    # "tag":I
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 279
    :cond_4
    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_5

    .line 280
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 288
    :cond_5
    :goto_2
    invoke-virtual {v9}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 289
    .local v8, "it":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 290
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 291
    .local v10, "pairs":Ljava/util/Map$Entry;
    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 292
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 293
    :cond_6
    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 284
    .end local v8    # "it":Ljava/util/Iterator;
    .end local v10    # "pairs":Ljava/util/Map$Entry;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->finish()V

    goto :goto_2

    .line 296
    .restart local v8    # "it":Ljava/util/Iterator;
    :cond_8
    invoke-direct {p0, v12, v13}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->setDefaultTagList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 297
    invoke-direct {p0, v12, v7}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->setDefaultTagList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 299
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_9

    .line 300
    const-string v0, "HeartrateLogByTagDetailActivity"

    const-string v1, "0 returning"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->finish()V

    .line 303
    :cond_9
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_a

    .line 304
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;-><init>()V

    invoke-static {v12, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 305
    invoke-static {v12}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 307
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    .line 309
    .local v3, "tagsDefault":[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;

    iget-wide v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->maxHeartrate:J

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;Landroid/app/Activity;[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;J)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mTagDefaultListadapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->lvTagDefaultList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->mTagDefaultListadapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->lvTagDefaultList:Landroid/widget/ListView;

    invoke-virtual {v0, v14}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 316
    .end local v3    # "tagsDefault":[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->lvTagDefaultList:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->disableScroll(Landroid/widget/ListView;)V

    .line 319
    return-void
.end method
