.class public final Lcom/sec/android/app/shealth/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final about_korean_popup:I = 0x7f030000

.field public static final about_shealth:I = 0x7f030001

.field public static final accessory_item:I = 0x7f030002

.field public static final action_bar_spinner:I = 0x7f030003

.field public static final action_bar_switch:I = 0x7f030004

.field public static final action_share_home_top:I = 0x7f030005

.field public static final activity_grid:I = 0x7f030006

.field public static final alert_dialog:I = 0x7f030007

.field public static final alert_dialog_alert_content_view:I = 0x7f030008

.field public static final alert_dialog_alert_text_view:I = 0x7f030009

.field public static final alert_dialog_hs:I = 0x7f03000a

.field public static final alert_dialog_list:I = 0x7f03000b

.field public static final alert_dialog_row:I = 0x7f03000c

.field public static final alert_dialog_title:I = 0x7f03000d

.field public static final auto_backup_account_layout:I = 0x7f03000e

.field public static final auto_backup_item:I = 0x7f03000f

.field public static final award_activity_item:I = 0x7f030010

.field public static final award_back_up_item_layout:I = 0x7f030011

.field public static final award_dialog_list_item:I = 0x7f030012

.field public static final award_medal_line_item:I = 0x7f030013

.field public static final award_summary_view_main:I = 0x7f030014

.field public static final award_today_activity_item:I = 0x7f030015

.field public static final backup_account_layout:I = 0x7f030016

.field public static final backup_progress_popup:I = 0x7f030017

.field public static final blank_layout_for_popup:I = 0x7f030018

.field public static final blank_screen:I = 0x7f030019

.field public static final bmi_user_profile_result:I = 0x7f03001a

.field public static final bt_item:I = 0x7f03001b

.field public static final calendar_button_medal_image:I = 0x7f03001c

.field public static final calendar_day_button:I = 0x7f03001d

.field public static final calendar_horizontal_divider:I = 0x7f03001e

.field public static final calendar_month:I = 0x7f03001f

.field public static final calendar_month_year_button:I = 0x7f030020

.field public static final calendar_vertical_divider:I = 0x7f030021

.field public static final calendar_week_row:I = 0x7f030022

.field public static final calenderrefactor:I = 0x7f030023

.field public static final cancel_ok_buttons:I = 0x7f030024

.field public static final cancel_ok_buttons_bottom:I = 0x7f030025

.field public static final cigna_about_cigna:I = 0x7f030026

.field public static final cigna_assessment_page_4button_view:I = 0x7f030027

.field public static final cigna_assessment_page_activity:I = 0x7f030028

.field public static final cigna_assessment_page_controller_view:I = 0x7f030029

.field public static final cigna_badge_achieve_activity:I = 0x7f03002a

.field public static final cigna_badge_list_activity:I = 0x7f03002b

.field public static final cigna_badge_list_item:I = 0x7f03002c

.field public static final cigna_calendar_view:I = 0x7f03002d

.field public static final cigna_category_header:I = 0x7f03002e

.field public static final cigna_coach_message_activity:I = 0x7f03002f

.field public static final cigna_coach_message_gauge_layout_:I = 0x7f030030

.field public static final cigna_complete_badge_layout:I = 0x7f030031

.field public static final cigna_complete_header_view:I = 0x7f030032

.field public static final cigna_current_goal_bottom_layout:I = 0x7f030033

.field public static final cigna_current_goal_child_item_view:I = 0x7f030034

.field public static final cigna_current_goal_fragment:I = 0x7f030035

.field public static final cigna_current_goal_group_item_view:I = 0x7f030036

.field public static final cigna_current_goal_list_header:I = 0x7f030037

.field public static final cigna_current_mission_detail_activity:I = 0x7f030038

.field public static final cigna_current_mission_detail_details_layout:I = 0x7f030039

.field public static final cigna_current_mission_detail_header_view:I = 0x7f03003a

.field public static final cigna_current_mission_detail_tips_expandable_child_item_view:I = 0x7f03003b

.field public static final cigna_current_mission_detail_tips_expandable_group_item_view:I = 0x7f03003c

.field public static final cigna_current_mission_detail_tips_layout:I = 0x7f03003d

.field public static final cigna_current_mission_detail_tips_title_item_view:I = 0x7f03003e

.field public static final cigna_current_mission_detail_tracking_day_of_week:I = 0x7f03003f

.field public static final cigna_current_mission_detail_tracking_layout:I = 0x7f030040

.field public static final cigna_custom_header_view:I = 0x7f030041

.field public static final cigna_custom_two_info_of_header:I = 0x7f030042

.field public static final cigna_delete_child_item_view:I = 0x7f030043

.field public static final cigna_delete_goal_mission_activity:I = 0x7f030044

.field public static final cigna_delete_group_item_view:I = 0x7f030045

.field public static final cigna_divider_horizontal_list:I = 0x7f030046

.field public static final cigna_expandable_child_item_view:I = 0x7f030047

.field public static final cigna_expandable_group_item_view:I = 0x7f030048

.field public static final cigna_expandablelist_group:I = 0x7f030049

.field public static final cigna_expandablelist_row:I = 0x7f03004a

.field public static final cigna_first_time_fragment:I = 0x7f03004b

.field public static final cigna_first_time_loading_layout:I = 0x7f03004c

.field public static final cigna_first_time_no_score_fragment:I = 0x7f03004d

.field public static final cigna_full_width_popup_layout:I = 0x7f03004e

.field public static final cigna_goal_complete_activity:I = 0x7f03004f

.field public static final cigna_goal_history_activity:I = 0x7f030050

.field public static final cigna_goal_history_header:I = 0x7f030051

.field public static final cigna_height_input_module:I = 0x7f030052

.field public static final cigna_intro_screen:I = 0x7f030053

.field public static final cigna_item_composer_header:I = 0x7f030054

.field public static final cigna_library_base_list_activity:I = 0x7f030055

.field public static final cigna_library_base_list_empty_view:I = 0x7f030056

.field public static final cigna_library_base_list_group_item:I = 0x7f030057

.field public static final cigna_library_base_list_item:I = 0x7f030058

.field public static final cigna_library_base_list_item_no_icon:I = 0x7f030059

.field public static final cigna_library_favorites_delete_list_activity:I = 0x7f03005a

.field public static final cigna_library_favorites_list_activity:I = 0x7f03005b

.field public static final cigna_library_home_activity:I = 0x7f03005c

.field public static final cigna_library_home_article_tip_view:I = 0x7f03005d

.field public static final cigna_library_home_fratment:I = 0x7f03005e

.field public static final cigna_library_home_search_activity:I = 0x7f03005f

.field public static final cigna_library_home_search_field_view:I = 0x7f030060

.field public static final cigna_library_search_edit_text:I = 0x7f030061

.field public static final cigna_library_tip_article_detail_activity:I = 0x7f030062

.field public static final cigna_lifestyle_fragment_2bottom_layout:I = 0x7f030063

.field public static final cigna_lifestyle_fragment_bg_activity:I = 0x7f030064

.field public static final cigna_lifestyle_fragment_bottom_layout:I = 0x7f030065

.field public static final cigna_lifestyle_get_your_score_layout:I = 0x7f030066

.field public static final cigna_lifestyle_multi_score_info_area_layout:I = 0x7f030067

.field public static final cigna_lifestyle_no_score_fragment:I = 0x7f030068

.field public static final cigna_lifestyle_one_score_fragment:I = 0x7f030069

.field public static final cigna_lifestyle_score_message_layout:I = 0x7f03006a

.field public static final cigna_lifestyle_score_view:I = 0x7f03006b

.field public static final cigna_logview:I = 0x7f03006c

.field public static final cigna_main_bottom_layout:I = 0x7f03006d

.field public static final cigna_main_score_view_layout:I = 0x7f03006e

.field public static final cigna_mission_complete_activity:I = 0x7f03006f

.field public static final cigna_no_current_goal_view:I = 0x7f030070

.field public static final cigna_no_data:I = 0x7f030071

.field public static final cigna_notification_2button_layout:I = 0x7f030072

.field public static final cigna_notification_5button_message:I = 0x7f030073

.field public static final cigna_reassessment_completed:I = 0x7f030074

.field public static final cigna_reassessment_goal_warning:I = 0x7f030075

.field public static final cigna_reassessment_less_than6:I = 0x7f030076

.field public static final cigna_reengaging_missions_fragment:I = 0x7f030077

.field public static final cigna_select_all_layout:I = 0x7f030078

.field public static final cigna_spinner_child_layout:I = 0x7f030079

.field public static final cigna_spinner_group_layout:I = 0x7f03007a

.field public static final cigna_suggest_no_data:I = 0x7f03007b

.field public static final cigna_suggested_activity:I = 0x7f03007c

.field public static final cigna_suggested_detail_activity:I = 0x7f03007d

.field public static final cigna_suggested_detail_contents_goal:I = 0x7f03007e

.field public static final cigna_suggested_detail_contents_mission:I = 0x7f03007f

.field public static final cigna_suggested_list_item:I = 0x7f030080

.field public static final cigna_suggested_list_top:I = 0x7f030081

.field public static final cigna_suggested_mission_detail_normal_header:I = 0x7f030082

.field public static final cigna_summary_fragment:I = 0x7f030083

.field public static final cigna_terms_of_use:I = 0x7f030084

.field public static final cigna_terms_of_use_body:I = 0x7f030085

.field public static final cigna_tracker_data_loading_popup:I = 0x7f030086

.field public static final cigna_weight_input_module:I = 0x7f030087

.field public static final cigna_widget_no_score_mission:I = 0x7f030088

.field public static final cigna_widget_no_score_no_mission:I = 0x7f030089

.field public static final cigna_widget_plain_no_score_mission:I = 0x7f03008a

.field public static final cigna_widget_plain_no_score_no_mission:I = 0x7f03008b

.field public static final cigna_widget_plain_score_mission:I = 0x7f03008c

.field public static final cigna_widget_plain_score_no_mission:I = 0x7f03008d

.field public static final cigna_widget_score_mission:I = 0x7f03008e

.field public static final cigna_widget_socre_no_mission:I = 0x7f03008f

.field public static final coach_calendar_day_button:I = 0x7f030090

.field public static final coach_calendar_month:I = 0x7f030091

.field public static final coach_calendar_week_row:I = 0x7f030092

.field public static final coach_restore_fail_popup:I = 0x7f030093

.field public static final comfort_level_area:I = 0x7f030094

.field public static final comfort_level_initial_popup:I = 0x7f030095

.field public static final comfort_level_popup_content_view:I = 0x7f030096

.field public static final comfort_level_popup_discard_changes:I = 0x7f030097

.field public static final comfort_level_popup_short_value:I = 0x7f030098

.field public static final comfort_zone_range:I = 0x7f030099

.field public static final common_listview_title:I = 0x7f03009a

.field public static final compatible_accessories_screen:I = 0x7f03009b

.field public static final compatible_device_connect:I = 0x7f03009c

.field public static final compatible_device_detail_layout:I = 0x7f03009d

.field public static final connectivity_activity:I = 0x7f03009e

.field public static final custom_actionbar:I = 0x7f03009f

.field public static final custom_button_with_icon:I = 0x7f0300a0

.field public static final custom_food_nutrient_item_view:I = 0x7f0300a1

.field public static final data_connection_popup_content:I = 0x7f0300a2

.field public static final data_connection_popup_content_chn:I = 0x7f0300a3

.field public static final data_connection_popup_content_fail:I = 0x7f0300a4

.field public static final date_selector_date_view:I = 0x7f0300a5

.field public static final datebar:I = 0x7f0300a6

.field public static final datepicker_shealth:I = 0x7f0300a7

.field public static final delete_userdata_layout:I = 0x7f0300a8

.field public static final device_list:I = 0x7f0300a9

.field public static final devise_connected_status_view:I = 0x7f0300aa

.field public static final dialog_list:I = 0x7f0300ab

.field public static final drawer_listview_header:I = 0x7f0300ac

.field public static final drawer_menu_view:I = 0x7f0300ad

.field public static final dropdown_list_popup_item:I = 0x7f0300ae

.field public static final edit_favorite_group:I = 0x7f0300af

.field public static final edit_favorites_item:I = 0x7f0300b0

.field public static final edit_favorites_view:I = 0x7f0300b1

.field public static final edit_profile:I = 0x7f0300b2

.field public static final ex_exercise_connectivity_info_layout:I = 0x7f0300b3

.field public static final ex_exercise_watches_tab_layout:I = 0x7f0300b4

.field public static final ex_expandablelist_row:I = 0x7f0300b5

.field public static final ex_graph_anime_exercisemate_legenda:I = 0x7f0300b6

.field public static final ex_graph_point_information:I = 0x7f0300b7

.field public static final ex_graph_point_information_fitness:I = 0x7f0300b8

.field public static final ex_graphviewfitness:I = 0x7f0300b9

.field public static final exercise_log_list_group_header:I = 0x7f0300ba

.field public static final exercise_mate_information_layout:I = 0x7f0300bb

.field public static final export_complete_popup:I = 0x7f0300bc

.field public static final export_data_list_item:I = 0x7f0300bd

.field public static final export_progress_popup:I = 0x7f0300be

.field public static final food_add_custom_food:I = 0x7f0300bf

.field public static final food_add_custom_food_header:I = 0x7f0300c0

.field public static final food_add_to_my_food_popup:I = 0x7f0300c1

.field public static final food_barcode_search_activity_layout:I = 0x7f0300c2

.field public static final food_bottom_button_previous_and_next:I = 0x7f0300c3

.field public static final food_camera_layout:I = 0x7f0300c4

.field public static final food_category_header_item_view:I = 0x7f0300c5

.field public static final food_checked_button:I = 0x7f0300c6

.field public static final food_date_time_buttons:I = 0x7f0300c7

.field public static final food_default_list_divider:I = 0x7f0300c8

.field public static final food_divider_horizontal:I = 0x7f0300c9

.field public static final food_easy_portion_size_popup_layout:I = 0x7f0300ca

.field public static final food_edit_my_meal_acttivity_layout:I = 0x7f0300cb

.field public static final food_expandable_list_item:I = 0x7f0300cc

.field public static final food_gallery_component:I = 0x7f0300cd

.field public static final food_graph_information_area:I = 0x7f0300ce

.field public static final food_graph_information_area_for_hour:I = 0x7f0300cf

.field public static final food_grid_image_holder:I = 0x7f0300d0

.field public static final food_grid_images:I = 0x7f0300d1

.field public static final food_grid_with_items_01:I = 0x7f0300d2

.field public static final food_grid_with_items_02:I = 0x7f0300d3

.field public static final food_grid_with_items_03:I = 0x7f0300d4

.field public static final food_grid_with_items_04:I = 0x7f0300d5

.field public static final food_grid_with_items_05:I = 0x7f0300d6

.field public static final food_grid_with_items_06:I = 0x7f0300d7

.field public static final food_grid_with_items_07:I = 0x7f0300d8

.field public static final food_grid_with_items_08:I = 0x7f0300d9

.field public static final food_grid_with_items_09:I = 0x7f0300da

.field public static final food_image_pager:I = 0x7f0300db

.field public static final food_list_category_header:I = 0x7f0300dc

.field public static final food_list_item:I = 0x7f0300dd

.field public static final food_list_items_header:I = 0x7f0300de

.field public static final food_list_loading_layout:I = 0x7f0300df

.field public static final food_list_popup_item:I = 0x7f0300e0

.field public static final food_list_title:I = 0x7f0300e1

.field public static final food_meal_activity_layout:I = 0x7f0300e2

.field public static final food_meal_activity_top_fragment_layout:I = 0x7f0300e3

.field public static final food_meal_images_fragment_layout:I = 0x7f0300e4

.field public static final food_meal_input_add_button:I = 0x7f0300e5

.field public static final food_meal_input_add_image_part:I = 0x7f0300e6

.field public static final food_meal_input_edit_memo:I = 0x7f0300e7

.field public static final food_meal_input_memo:I = 0x7f0300e8

.field public static final food_meal_input_top:I = 0x7f0300e9

.field public static final food_meal_item_view_layout:I = 0x7f0300ea

.field public static final food_meal_items_fragment_layout:I = 0x7f0300eb

.field public static final food_meal_items_part:I = 0x7f0300ec

.field public static final food_meal_plan_creator:I = 0x7f0300ed

.field public static final food_meal_plan_date_buttons_from_and_to:I = 0x7f0300ee

.field public static final food_meal_plan_edit:I = 0x7f0300ef

.field public static final food_meal_plan_list:I = 0x7f0300f0

.field public static final food_meal_plan_list_item:I = 0x7f0300f1

.field public static final food_meal_plan_preview:I = 0x7f0300f2

.field public static final food_meal_plan_preview_meal:I = 0x7f0300f3

.field public static final food_meal_plan_select_meal_type:I = 0x7f0300f4

.field public static final food_meal_plan_set_period:I = 0x7f0300f5

.field public static final food_meal_time_container:I = 0x7f0300f6

.field public static final food_my_food_activity:I = 0x7f0300f7

.field public static final food_my_food_list_divider:I = 0x7f0300f8

.field public static final food_no_data:I = 0x7f0300f9

.field public static final food_no_network_connection_dialog:I = 0x7f0300fa

.field public static final food_nutrition_block:I = 0x7f0300fb

.field public static final food_nutrition_info_list_footer:I = 0x7f0300fc

.field public static final food_nutrition_item:I = 0x7f0300fd

.field public static final food_nutrition_layout:I = 0x7f0300fe

.field public static final food_photo_search_image:I = 0x7f0300ff

.field public static final food_pick_activity:I = 0x7f030100

.field public static final food_pick_category_content_fragment:I = 0x7f030101

.field public static final food_pick_category_fragment:I = 0x7f030102

.field public static final food_pick_category_list_item:I = 0x7f030103

.field public static final food_pick_frequent_fragment:I = 0x7f030104

.field public static final food_pick_my_food_add_my_food:I = 0x7f030105

.field public static final food_pick_my_food_fragment:I = 0x7f030106

.field public static final food_pick_my_food_fragment_child_list_item:I = 0x7f030107

.field public static final food_pick_no_data_layout:I = 0x7f030108

.field public static final food_pick_search_edit_text:I = 0x7f030109

.field public static final food_pick_search_fragment:I = 0x7f03010a

.field public static final food_pick_search_fragment_tab:I = 0x7f03010b

.field public static final food_pick_selected_item:I = 0x7f03010c

.field public static final food_portion_size_handler_view_layout:I = 0x7f03010d

.field public static final food_portion_size_input_module:I = 0x7f03010e

.field public static final food_portion_size_slider_view_layout:I = 0x7f03010f

.field public static final food_presices_portion_size_pupup_laoyut:I = 0x7f030110

.field public static final food_quick_input_image_panel:I = 0x7f030111

.field public static final food_quickinput_meal_row:I = 0x7f030112

.field public static final food_quickinput_popup:I = 0x7f030113

.field public static final food_selected_panel_layout:I = 0x7f030114

.field public static final food_set_goal_activity:I = 0x7f030115

.field public static final food_set_goal_input_module:I = 0x7f030116

.field public static final food_set_quick_input_activity:I = 0x7f030117

.field public static final food_set_quickinput_popup:I = 0x7f030118

.field public static final food_total_calories_pan_layout:I = 0x7f030119

.field public static final food_tracker_base_fragment:I = 0x7f03011a

.field public static final food_tracker_summary_view_bottom_container:I = 0x7f03011b

.field public static final food_tracker_summary_view_main_container:I = 0x7f03011c

.field public static final food_tracker_summary_view_main_container_item:I = 0x7f03011d

.field public static final food_tracker_summary_view_top_container:I = 0x7f03011e

.field public static final graph_circle_diagram:I = 0x7f03011f

.field public static final graph_information_area:I = 0x7f030120

.field public static final graph_information_area_item:I = 0x7f030121

.field public static final graph_information_area_item_view:I = 0x7f030122

.field public static final graphdatebar:I = 0x7f030123

.field public static final graphview:I = 0x7f030124

.field public static final grid_hover_layout:I = 0x7f030125

.field public static final health_care_summary_content_view:I = 0x7f030126

.field public static final health_care_summary_progress_view:I = 0x7f030127

.field public static final heartrate_gear_help:I = 0x7f030128

.field public static final help_guide_activity:I = 0x7f030129

.field public static final help_guide_item:I = 0x7f03012a

.field public static final home_burnt_popup:I = 0x7f03012b

.field public static final home_connect_device:I = 0x7f03012c

.field public static final home_edit_icon_layout:I = 0x7f03012d

.field public static final home_fragment_layout:I = 0x7f03012e

.field public static final home_hr_state_bar:I = 0x7f03012f

.field public static final home_icon_layout:I = 0x7f030130

.field public static final home_latest_data_list_item_layout:I = 0x7f030131

.field public static final home_shortcut_layout:I = 0x7f030132

.field public static final home_spo2_state_bar:I = 0x7f030133

.field public static final home_stress_state_bar:I = 0x7f030134

.field public static final home_sync_progressbar:I = 0x7f030135

.field public static final home_wallpaper_chooser_layout:I = 0x7f030136

.field public static final home_wallpaper_item:I = 0x7f030137

.field public static final hover_grid_child:I = 0x7f030138

.field public static final hover_information_popup:I = 0x7f030139

.field public static final hover_tooltip_popup:I = 0x7f03013a

.field public static final hover_zoom_in_popup:I = 0x7f03013b

.field public static final hrm_activity_log_by_tag:I = 0x7f03013c

.field public static final hrm_activity_log_detail:I = 0x7f03013d

.field public static final hrm_activity_log_input:I = 0x7f03013e

.field public static final hrm_activity_more_tag:I = 0x7f03013f

.field public static final hrm_activity_tag_add_input:I = 0x7f030140

.field public static final hrm_activity_tag_list:I = 0x7f030141

.field public static final hrm_dialog_error:I = 0x7f030142

.field public static final hrm_dialog_errorr:I = 0x7f030143

.field public static final hrm_dialog_information:I = 0x7f030144

.field public static final hrm_dialog_tag_chooser:I = 0x7f030145

.field public static final hrm_dialog_warning:I = 0x7f030146

.field public static final hrm_dialog_warning_nosensor:I = 0x7f030147

.field public static final hrm_expandablelist_group:I = 0x7f030148

.field public static final hrm_expandablelist_row:I = 0x7f030149

.field public static final hrm_fragment_summary_new:I = 0x7f03014a

.field public static final hrm_graph_anime_legenda:I = 0x7f03014b

.field public static final hrm_graph_infomation_area:I = 0x7f03014c

.field public static final hrm_graph_infomation_area_day_month:I = 0x7f03014d

.field public static final hrm_graph_infomation_area_new:I = 0x7f03014e

.field public static final hrm_information:I = 0x7f03014f

.field public static final hrm_log_tagdetail_item:I = 0x7f030150

.field public static final hrm_scover_info_dialog:I = 0x7f030151

.field public static final hrm_scover_main:I = 0x7f030152

.field public static final hrm_scover_main_error_dialog:I = 0x7f030153

.field public static final hrm_scover_main_measure:I = 0x7f030154

.field public static final hrm_scover_main_text:I = 0x7f030155

.field public static final hrm_scover_profile_toast:I = 0x7f030156

.field public static final hrm_scover_profile_toast_bg:I = 0x7f030157

.field public static final hrm_scover_title:I = 0x7f030158

.field public static final hrm_summary_cancel_button:I = 0x7f030159

.field public static final hrm_summary_circle:I = 0x7f03015a

.field public static final hrm_summary_layout_first_new:I = 0x7f03015b

.field public static final hrm_summary_layout_second_new:I = 0x7f03015c

.field public static final hrm_summary_layout_third_new:I = 0x7f03015d

.field public static final hrm_summary_start_button:I = 0x7f03015e

.field public static final hrm_tag_chooser:I = 0x7f03015f

.field public static final hrm_taglist_item:I = 0x7f030160

.field public static final hrm_taglist_row:I = 0x7f030161

.field public static final information_bmi:I = 0x7f030162

.field public static final information_hrm:I = 0x7f030163

.field public static final information_recommended:I = 0x7f030164

.field public static final init_profile_card_bottom:I = 0x7f030165

.field public static final init_profile_card_top:I = 0x7f030166

.field public static final initial_profile_activity:I = 0x7f030167

.field public static final initial_profile_intro:I = 0x7f030168

.field public static final initial_profile_samsung_account:I = 0x7f030169

.field public static final initial_profile_start_layout:I = 0x7f03016a

.field public static final initial_set_password:I = 0x7f03016b

.field public static final input_activity:I = 0x7f03016c

.field public static final inputmodule_horizontal_layout:I = 0x7f03016d

.field public static final inputmodule_horizontal_middle_layout:I = 0x7f03016e

.field public static final inputmodule_horizontal_top_layout:I = 0x7f03016f

.field public static final inputmodule_vertical_layout:I = 0x7f030170

.field public static final inputmodule_vertical_middle_layout:I = 0x7f030171

.field public static final inputmodule_vertical_top_layout:I = 0x7f030172

.field public static final inputmodule_vertical_top_title_layout:I = 0x7f030173

.field public static final legend_area_with_triangular_button_decoration:I = 0x7f030174

.field public static final list_choose_dialog_title:I = 0x7f030175

.field public static final list_display_dialog_item:I = 0x7f030176

.field public static final list_view_item:I = 0x7f030177

.field public static final loading_dialog:I = 0x7f030178

.field public static final lock_screen_activity:I = 0x7f030179

.field public static final log_fragment:I = 0x7f03017a

.field public static final log_list_group_header:I = 0x7f03017b

.field public static final log_list_item_divider:I = 0x7f03017c

.field public static final log_list_row:I = 0x7f03017d

.field public static final manual_webview:I = 0x7f03017e

.field public static final more_apps_main:I = 0x7f03017f

.field public static final multi_checkbox_dialog_item:I = 0x7f030180

.field public static final my_food_list_divider:I = 0x7f030181

.field public static final mygoals_connect_device:I = 0x7f030182

.field public static final notice_layout:I = 0x7f030183

.field public static final opensouce_license:I = 0x7f030184

.field public static final paired_devices_settings_layout:I = 0x7f030185

.field public static final pincode_activity:I = 0x7f030186

.field public static final popup_dialog_checkbox_1:I = 0x7f030187

.field public static final popup_dialog_checkbox_2:I = 0x7f030188

.field public static final popup_dialog_loading:I = 0x7f030189

.field public static final popup_dialog_not_supported_applist:I = 0x7f03018a

.field public static final popup_edittext:I = 0x7f03018b

.field public static final popup_hint:I = 0x7f03018c

.field public static final privacy_policy:I = 0x7f03018d

.field public static final privacy_policy_body:I = 0x7f03018e

.field public static final privacy_policy_body_au:I = 0x7f03018f

.field public static final privacy_policy_body_kor:I = 0x7f030190

.field public static final pro_data_drop_text:I = 0x7f030191

.field public static final pro_details_list_layout:I = 0x7f030192

.field public static final pro_exercise_details_photo:I = 0x7f030193

.field public static final pro_exercise_image_pager:I = 0x7f030194

.field public static final pro_exercise_memo:I = 0x7f030195

.field public static final pro_exercise_pro_activity:I = 0x7f030196

.field public static final pro_exercise_pro_amap_activity:I = 0x7f030197

.field public static final pro_exercise_pro_amap_data_layout:I = 0x7f030198

.field public static final pro_exercise_pro_amap_layout:I = 0x7f030199

.field public static final pro_exercise_pro_amap_status_fragment:I = 0x7f03019a

.field public static final pro_exercise_pro_audio_guide_checkbox:I = 0x7f03019b

.field public static final pro_exercise_pro_audio_guide_layout:I = 0x7f03019c

.field public static final pro_exercise_pro_card_view_layout:I = 0x7f03019d

.field public static final pro_exercise_pro_data_layout:I = 0x7f03019e

.field public static final pro_exercise_pro_details_activity:I = 0x7f03019f

.field public static final pro_exercise_pro_details_fragment:I = 0x7f0301a0

.field public static final pro_exercise_pro_goal_layout:I = 0x7f0301a1

.field public static final pro_exercise_pro_goal_list:I = 0x7f0301a2

.field public static final pro_exercise_pro_hrm_main:I = 0x7f0301a3

.field public static final pro_exercise_pro_hrm_main_measure:I = 0x7f0301a4

.field public static final pro_exercise_pro_hrm_main_null:I = 0x7f0301a5

.field public static final pro_exercise_pro_hrm_main_text:I = 0x7f0301a6

.field public static final pro_exercise_pro_hrm_result:I = 0x7f0301a7

.field public static final pro_exercise_pro_hrm_result_new:I = 0x7f0301a8

.field public static final pro_exercise_pro_kcal_summary_layout:I = 0x7f0301a9

.field public static final pro_exercise_pro_map_activity:I = 0x7f0301aa

.field public static final pro_exercise_pro_map_layout:I = 0x7f0301ab

.field public static final pro_exercise_pro_max_hr_row_item:I = 0x7f0301ac

.field public static final pro_exercise_pro_max_hr_row_item_manual:I = 0x7f0301ad

.field public static final pro_exercise_pro_music_layout:I = 0x7f0301ae

.field public static final pro_exercise_pro_my_workout_map_activity:I = 0x7f0301af

.field public static final pro_exercise_pro_operate_layout:I = 0x7f0301b0

.field public static final pro_exercise_pro_realtime_controller_layout:I = 0x7f0301b1

.field public static final pro_exercise_pro_share_format:I = 0x7f0301b2

.field public static final pro_exercise_pro_state_summary_bar:I = 0x7f0301b3

.field public static final pro_exercise_pro_status_fragment:I = 0x7f0301b4

.field public static final pro_exercise_pro_time_goal:I = 0x7f0301b5

.field public static final pro_exercise_pro_time_goal_vn:I = 0x7f0301b6

.field public static final pro_exercise_pro_training_effect_goal:I = 0x7f0301b7

.field public static final pro_exercise_pro_training_effect_goal_progress_bar:I = 0x7f0301b8

.field public static final pro_exercise_pro_training_effect_guide:I = 0x7f0301b9

.field public static final pro_exercise_pro_training_effect_guide_item:I = 0x7f0301ba

.field public static final pro_exercise_pro_two_bottom_buttons:I = 0x7f0301bb

.field public static final pro_exercise_pro_workout_info_activity:I = 0x7f0301bc

.field public static final pro_exercisepro_map_data:I = 0x7f0301bd

.field public static final pro_fitness_ant_popup_content_view:I = 0x7f0301be

.field public static final pro_graph_anime_exercisepro_legenda:I = 0x7f0301bf

.field public static final pro_graph_point_information:I = 0x7f0301c0

.field public static final pro_grid_photo_holder:I = 0x7f0301c1

.field public static final pro_grid_with_photo_01:I = 0x7f0301c2

.field public static final pro_grid_with_photo_02:I = 0x7f0301c3

.field public static final pro_grid_with_photo_03:I = 0x7f0301c4

.field public static final pro_grid_with_photo_04:I = 0x7f0301c5

.field public static final pro_grid_with_photo_05:I = 0x7f0301c6

.field public static final pro_grid_with_photo_06:I = 0x7f0301c7

.field public static final pro_grid_with_photo_07:I = 0x7f0301c8

.field public static final pro_grid_with_photo_08:I = 0x7f0301c9

.field public static final pro_grid_with_photo_09:I = 0x7f0301ca

.field public static final pro_inputmodule_horizontal_layout:I = 0x7f0301cb

.field public static final pro_list_item_dual_layout:I = 0x7f0301cc

.field public static final pro_list_item_layout:I = 0x7f0301cd

.field public static final pro_list_popup_item:I = 0x7f0301ce

.field public static final pro_listpopup_layout:I = 0x7f0301cf

.field public static final pro_max_heartrate_mode_select_activity:I = 0x7f0301d0

.field public static final pro_photo_horizontal_scrollview:I = 0x7f0301d1

.field public static final pro_popup_edit_layout:I = 0x7f0301d2

.field public static final pro_realtime_bottom_button:I = 0x7f0301d3

.field public static final pro_realtime_coach_flipper_item:I = 0x7f0301d4

.field public static final pro_realtime_drop_orb:I = 0x7f0301d5

.field public static final pro_realtime_drop_text:I = 0x7f0301d6

.field public static final pro_realtime_goal_text:I = 0x7f0301d7

.field public static final pro_realtime_hold_bottom:I = 0x7f0301d8

.field public static final pro_realtime_indoor_bottom:I = 0x7f0301d9

.field public static final pro_realtime_list_text:I = 0x7f0301da

.field public static final pro_realtime_map_data:I = 0x7f0301db

.field public static final pro_realtime_outdoor_bottom:I = 0x7f0301dc

.field public static final pro_realtime_outdoor_map_bottom:I = 0x7f0301dd

.field public static final pro_realtime_two_bottom_buttons:I = 0x7f0301de

.field public static final pro_realtime_visual_guidance_status:I = 0x7f0301df

.field public static final pro_slider_bubble_glide:I = 0x7f0301e0

.field public static final pro_statusfragment_activity_type_layout:I = 0x7f0301e1

.field public static final profile_bmi_layout:I = 0x7f0301e2

.field public static final profile_body_information_dialog_content:I = 0x7f0301e3

.field public static final profile_card_fifth:I = 0x7f0301e4

.field public static final profile_card_first:I = 0x7f0301e5

.field public static final profile_card_fourth:I = 0x7f0301e6

.field public static final profile_card_second:I = 0x7f0301e7

.field public static final profile_card_third:I = 0x7f0301e8

.field public static final profile_height_input_module:I = 0x7f0301e9

.field public static final profile_information_guide:I = 0x7f0301ea

.field public static final profile_information_guide_item:I = 0x7f0301eb

.field public static final profile_information_guide_item_calory:I = 0x7f0301ec

.field public static final profile_init_card:I = 0x7f0301ed

.field public static final profile_more_option_list_item:I = 0x7f0301ee

.field public static final profile_result_card:I = 0x7f0301ef

.field public static final profile_spinner_item_layout:I = 0x7f0301f0

.field public static final profile_spinner_main_layout:I = 0x7f0301f1

.field public static final profile_weight_input_module:I = 0x7f0301f2

.field public static final radio_button_item_normal:I = 0x7f0301f3

.field public static final reset_data_activity:I = 0x7f0301f4

.field public static final reset_data_list_item:I = 0x7f0301f5

.field public static final restore_account_layout:I = 0x7f0301f6

.field public static final samsung_account_desc_fragment:I = 0x7f0301f7

.field public static final samsung_account_layout:I = 0x7f0301f8

.field public static final samsung_account_signin_activity_layout:I = 0x7f0301f9

.field public static final scanning_fragment:I = 0x7f0301fa

.field public static final set_security_settings_activity:I = 0x7f0301fb

.field public static final settings_screen:I = 0x7f0301fc

.field public static final share_dialog_list:I = 0x7f0301fd

.field public static final share_item:I = 0x7f0301fe

.field public static final shealth_adding_food_items:I = 0x7f0301ff

.field public static final shealth_favorites:I = 0x7f030200

.field public static final shealth_heartrate:I = 0x7f030201

.field public static final shealth_installing_more_apps:I = 0x7f030202

.field public static final shealth_managing_weight:I = 0x7f030203

.field public static final shealth_measuring_stress_level:I = 0x7f030204

.field public static final shealth_measuring_uv_level:I = 0x7f030205

.field public static final shealth_measuring_uv_level_for_gear:I = 0x7f030206

.field public static final shealth_pedometer_activity:I = 0x7f030207

.field public static final shealth_recording_workout:I = 0x7f030208

.field public static final shealth_setting_goals_missions:I = 0x7f030209

.field public static final shealth_spo2:I = 0x7f03020a

.field public static final shealth_spo2_non_medical:I = 0x7f03020b

.field public static final shealth_state_summary_bar:I = 0x7f03020c

.field public static final shealth_state_summary_bar_type2:I = 0x7f03020d

.field public static final shealth_tgh:I = 0x7f03020e

.field public static final shealth_viewing_sleep_data:I = 0x7f03020f

.field public static final sleep_expandablelist_group:I = 0x7f030210

.field public static final sleep_expandablelist_row:I = 0x7f030211

.field public static final sleep_graph_anime_sleep_legenda:I = 0x7f030212

.field public static final sleep_graph_point_information:I = 0x7f030213

.field public static final sleep_graphview:I = 0x7f030214

.field public static final sleep_logdetail:I = 0x7f030215

.field public static final sleep_logdetail_divider:I = 0x7f030216

.field public static final sleep_summary_view_sleep_monitor_c:I = 0x7f030217

.field public static final sleep_warning_dialog_view:I = 0x7f030218

.field public static final spinner_child_view:I = 0x7f030219

.field public static final spinner_header_view:I = 0x7f03021a

.field public static final splashscreenactivity:I = 0x7f03021b

.field public static final spo2_activity_log_detail:I = 0x7f03021c

.field public static final spo2_activity_log_input:I = 0x7f03021d

.field public static final spo2_dialog_error:I = 0x7f03021e

.field public static final spo2_dialog_warning:I = 0x7f03021f

.field public static final spo2_expandablelist_group:I = 0x7f030220

.field public static final spo2_expandablelist_row:I = 0x7f030221

.field public static final spo2_fragment_summary_new:I = 0x7f030222

.field public static final spo2_graph_anime_legenda:I = 0x7f030223

.field public static final spo2_graph_infomation_area:I = 0x7f030224

.field public static final spo2_information:I = 0x7f030225

.field public static final spo2_information_message:I = 0x7f030226

.field public static final spo2_information_non_medical:I = 0x7f030227

.field public static final spo2_state_summary_small_bar:I = 0x7f030228

.field public static final spo2_summary_circle:I = 0x7f030229

.field public static final spo2_summary_discard_button:I = 0x7f03022a

.field public static final spo2_summary_layout_first_new:I = 0x7f03022b

.field public static final spo2_summary_layout_fourth_new:I = 0x7f03022c

.field public static final spo2_summary_layout_second_new:I = 0x7f03022d

.field public static final spo2_summary_layout_third_new:I = 0x7f03022e

.field public static final spo2_summary_start_button:I = 0x7f03022f

.field public static final stm_activity_log_detail:I = 0x7f030230

.field public static final stm_activity_log_input:I = 0x7f030231

.field public static final stm_dialog_error:I = 0x7f030232

.field public static final stm_dialog_warning:I = 0x7f030233

.field public static final stm_expandablelist_group:I = 0x7f030234

.field public static final stm_expandablelist_row:I = 0x7f030235

.field public static final stm_fragment_summary:I = 0x7f030236

.field public static final stm_graph_anime_legenda:I = 0x7f030237

.field public static final stm_graph_infomation_area:I = 0x7f030238

.field public static final stm_state_detail_bar:I = 0x7f030239

.field public static final stm_state_graph_bar:I = 0x7f03023a

.field public static final stm_state_log_bar:I = 0x7f03023b

.field public static final stm_state_summary_bar:I = 0x7f03023c

.field public static final stm_state_summary_small_bar:I = 0x7f03023d

.field public static final stm_summary_discard_button:I = 0x7f03023e

.field public static final stm_summary_layout_first:I = 0x7f03023f

.field public static final stm_summary_layout_second:I = 0x7f030240

.field public static final stm_summary_layout_third:I = 0x7f030241

.field public static final stm_summary_start_button:I = 0x7f030242

.field public static final summary_fragment:I = 0x7f030243

.field public static final terms_of_use:I = 0x7f030244

.field public static final terms_of_use_body:I = 0x7f030245

.field public static final terms_of_use_body_au:I = 0x7f030246

.field public static final terms_of_use_body_kor:I = 0x7f030247

.field public static final test_dbkey_dialog_01:I = 0x7f030248

.field public static final test_dbkey_dialog_02:I = 0x7f030249

.field public static final textview_item:I = 0x7f03024a

.field public static final tgh_action_bar:I = 0x7f03024b

.field public static final tgh_actionbar_popup_list_item:I = 0x7f03024c

.field public static final tgh_activity_log_detail:I = 0x7f03024d

.field public static final tgh_activity_log_input:I = 0x7f03024e

.field public static final tgh_expandablelist_group:I = 0x7f03024f

.field public static final tgh_expandablelist_row:I = 0x7f030250

.field public static final tgh_graph_anime_legenda:I = 0x7f030251

.field public static final tgh_graph_infomation_area:I = 0x7f030252

.field public static final tgh_health_care_summary_content_view:I = 0x7f030253

.field public static final tgh_summary_content_bar:I = 0x7f030254

.field public static final tgh_summary_header_status:I = 0x7f030255

.field public static final thermohygrometer_input:I = 0x7f030256

.field public static final thermohygrometer_summary_fragment:I = 0x7f030257

.field public static final thermohygrometer_summary_fragment_bottom:I = 0x7f030258

.field public static final timepicker_shealth:I = 0x7f030259

.field public static final unit_settings_activity:I = 0x7f03025a

.field public static final unit_settings_item:I = 0x7f03025b

.field public static final uv_activity_log_detail:I = 0x7f03025c

.field public static final uv_activity_log_input:I = 0x7f03025d

.field public static final uv_china_recommended_medium_message:I = 0x7f03025e

.field public static final uv_china_recommended_strong_message:I = 0x7f03025f

.field public static final uv_china_recommended_stronger_message:I = 0x7f030260

.field public static final uv_china_recommended_weak_message:I = 0x7f030261

.field public static final uv_china_recommended_weakest_message:I = 0x7f030262

.field public static final uv_dialog_error:I = 0x7f030263

.field public static final uv_dialog_recommendmessage:I = 0x7f030264

.field public static final uv_dialog_skin_type:I = 0x7f030265

.field public static final uv_dialog_spf:I = 0x7f030266

.field public static final uv_dialog_warning:I = 0x7f030267

.field public static final uv_dialog_warning_nosensor:I = 0x7f030268

.field public static final uv_expandablelist_group:I = 0x7f030269

.field public static final uv_expandablelist_row:I = 0x7f03026a

.field public static final uv_fragment_summary:I = 0x7f03026b

.field public static final uv_graph_anime_legenda:I = 0x7f03026c

.field public static final uv_graph_infomation_area:I = 0x7f03026d

.field public static final uv_recommended_extreme_message:I = 0x7f03026e

.field public static final uv_recommended_high_message:I = 0x7f03026f

.field public static final uv_recommended_low_message:I = 0x7f030270

.field public static final uv_recommended_moderate_message:I = 0x7f030271

.field public static final uv_recommended_veryhigh_message:I = 0x7f030272

.field public static final uv_state_summary_bar:I = 0x7f030273

.field public static final uv_state_summary_chinese_bar:I = 0x7f030274

.field public static final uv_summary_discard_button:I = 0x7f030275

.field public static final uv_summary_layout_first:I = 0x7f030276

.field public static final uv_summary_layout_second:I = 0x7f030277

.field public static final uv_summary_layout_third:I = 0x7f030278

.field public static final uv_summary_start_button:I = 0x7f030279

.field public static final walk_app_easywidget_walk_mate:I = 0x7f03027a

.field public static final walk_appwidget_plain_walk_mate:I = 0x7f03027b

.field public static final walk_appwidget_plain_walk_mate_cigna:I = 0x7f03027c

.field public static final walk_appwidget_plain_walk_mate_cigna_t:I = 0x7f03027d

.field public static final walk_appwidget_plain_walk_mate_initial:I = 0x7f03027e

.field public static final walk_appwidget_plain_walk_mate_t:I = 0x7f03027f

.field public static final walk_appwidget_walk_mate:I = 0x7f030280

.field public static final walk_appwidget_walk_mate_clear_cover:I = 0x7f030281

.field public static final walk_appwidget_walk_mate_initial:I = 0x7f030282

.field public static final walk_appwidget_walk_mate_old:I = 0x7f030283

.field public static final walk_appwidget_walk_mate_old_initial:I = 0x7f030284

.field public static final walk_cocktailwidget_walk_mate:I = 0x7f030285

.field public static final walk_details_activity:I = 0x7f030286

.field public static final walk_everyone_rankings_footer_item:I = 0x7f030287

.field public static final walk_everyone_rankings_item:I = 0x7f030288

.field public static final walk_everyone_rankings_item_additional:I = 0x7f030289

.field public static final walk_everyone_rankings_layout:I = 0x7f03028a

.field public static final walk_expandablelist_group:I = 0x7f03028b

.field public static final walk_expandablelist_row:I = 0x7f03028c

.field public static final walk_first_bottom_button:I = 0x7f03028d

.field public static final walk_goal_activity:I = 0x7f03028e

.field public static final walk_gradient_progressbar:I = 0x7f03028f

.field public static final walk_graph_anime_walk_for_lie_legenda:I = 0x7f030290

.field public static final walk_graph_point_information:I = 0x7f030291

.field public static final walk_graph_walking_mate_day:I = 0x7f030292

.field public static final walk_grapth_circle_diagram:I = 0x7f030293

.field public static final walk_inactive_time_activity:I = 0x7f030294

.field public static final walk_inactive_time_set_time_activity:I = 0x7f030295

.field public static final walk_inactive_time_set_time_setting:I = 0x7f030296

.field public static final walk_information_activity:I = 0x7f030297

.field public static final walk_information_item:I = 0x7f030298

.field public static final walk_inputmodule_horizontal_layout:I = 0x7f030299

.field public static final walk_list_popup_item:I = 0x7f03029a

.field public static final walk_loading_image_view:I = 0x7f03029b

.field public static final walk_lockscreen_appwidget_pedometer_small:I = 0x7f03029c

.field public static final walk_log_gear_details_activity:I = 0x7f03029d

.field public static final walk_log_gear_title_textview:I = 0x7f03029e

.field public static final walk_mate_details_item:I = 0x7f03029f

.field public static final walk_mate_gear_details_item:I = 0x7f0302a0

.field public static final walk_my_rankings_age:I = 0x7f0302a1

.field public static final walk_my_rankings_header:I = 0x7f0302a2

.field public static final walk_my_rankings_layout:I = 0x7f0302a3

.field public static final walk_my_rankings_world:I = 0x7f0302a4

.field public static final walk_my_rankings_zero:I = 0x7f0302a5

.field public static final walk_notification_activity:I = 0x7f0302a6

.field public static final walk_pedometer_inactive_time_setting:I = 0x7f0302a7

.field public static final walk_pedometer_set_time_setting:I = 0x7f0302a8

.field public static final walk_pedometer_setting:I = 0x7f0302a9

.field public static final walk_progress_marker_text_switcher:I = 0x7f0302aa

.field public static final walk_progress_marker_view:I = 0x7f0302ab

.field public static final walk_ranking_tabs:I = 0x7f0302ac

.field public static final walk_second_bottom_button:I = 0x7f0302ad

.field public static final walk_statistics_dialog:I = 0x7f0302ae

.field public static final walk_statistics_dialog_item:I = 0x7f0302af

.field public static final walk_statistics_dialog_item_my:I = 0x7f0302b0

.field public static final walk_statistics_dialog_text_item:I = 0x7f0302b1

.field public static final walk_summary_fragment:I = 0x7f0302b2

.field public static final walk_sync_bottom_button:I = 0x7f0302b3

.field public static final walk_view_step_count_layout:I = 0x7f0302b4

.field public static final wallpaper_apps_grid:I = 0x7f0302b5

.field public static final weight_activity_log_detail:I = 0x7f0302b6

.field public static final weight_activity_log_input:I = 0x7f0302b7

.field public static final wgt_accessory_summary_fragment:I = 0x7f0302b8

.field public static final wgt_goal_activity:I = 0x7f0302b9

.field public static final wgt_goal_add_new_goal_view:I = 0x7f0302ba

.field public static final wgt_goal_no_data_view:I = 0x7f0302bb

.field public static final wgt_goal_with_data_fragment:I = 0x7f0302bc

.field public static final wgt_graph_information_area:I = 0x7f0302bd

.field public static final wgt_indicator:I = 0x7f0302be

.field public static final wgt_information:I = 0x7f0302bf

.field public static final wgt_input:I = 0x7f0302c0

.field public static final wgt_log_detail_memo:I = 0x7f0302c1

.field public static final wgt_next_previous_button:I = 0x7f0302c2

.field public static final wgt_preloaded_summary_fragment:I = 0x7f0302c3

.field public static final wgt_set_goal_calorie_activity:I = 0x7f0302c4

.field public static final wgt_set_goal_calorie_activity_header:I = 0x7f0302c5

.field public static final wgt_set_goal_title_header:I = 0x7f0302c6

.field public static final wgt_summary_fragment:I = 0x7f0302c7

.field public static final wgt_summary_fragment_bottom_view:I = 0x7f0302c8

.field public static final wgt_summary_top_view:I = 0x7f0302c9

.field public static final wgt_summary_update_button:I = 0x7f0302ca


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
