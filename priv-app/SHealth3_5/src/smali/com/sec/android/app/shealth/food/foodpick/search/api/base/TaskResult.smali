.class public final enum Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;
.super Ljava/lang/Enum;
.source "TaskResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

.field public static final enum ERROR_BAD_REQUEST:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

.field public static final enum ERROR_CONNECTION:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

.field public static final enum ERROR_PARSE:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

.field public static final enum NO_BARCODE_FOUND:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

.field public static final enum SUCCESS:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;


# instance fields
.field isCompleted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->SUCCESS:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    const-string v1, "ERROR_PARSE"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->ERROR_PARSE:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    const-string v1, "ERROR_CONNECTION"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->ERROR_CONNECTION:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    const-string v1, "NO_BARCODE_FOUND"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->NO_BARCODE_FOUND:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    const-string v1, "ERROR_BAD_REQUEST"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->ERROR_BAD_REQUEST:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    .line 18
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->SUCCESS:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->ERROR_PARSE:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->ERROR_CONNECTION:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->NO_BARCODE_FOUND:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->ERROR_BAD_REQUEST:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->$VALUES:[Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .param p3, "isCompleted"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->isCompleted:Z

    .line 25
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->$VALUES:[Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    return-object v0
.end method
