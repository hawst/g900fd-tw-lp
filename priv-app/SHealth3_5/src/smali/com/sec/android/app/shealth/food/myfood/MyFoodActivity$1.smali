.class Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;
.super Ljava/lang/Object;
.source "MyFoodActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->addSelectSpinner()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "selectedItemView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 456
    if-lez p3, :cond_1

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    const v2, 0x7f080454

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 459
    .local v0, "cb":Landroid/widget/CheckBox;
    if-eqz v0, :cond_2

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090071

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 461
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 462
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$400(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->getExpandableListAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->checkAllItems(Z)V

    .line 470
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->refreshActivityState()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1900(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V

    .line 473
    .end local v0    # "cb":Landroid/widget/CheckBox;
    :cond_1
    return-void

    .line 464
    .restart local v0    # "cb":Landroid/widget/CheckBox;
    :cond_2
    if-eqz v0, :cond_0

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090073

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 466
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 467
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$400(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->getExpandableListAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->checkAllItems(Z)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 478
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
