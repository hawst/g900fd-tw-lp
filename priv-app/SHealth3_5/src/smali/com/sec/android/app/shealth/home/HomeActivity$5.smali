.class Lcom/sec/android/app/shealth/home/HomeActivity$5;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/HomeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 863
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$5;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 866
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$5;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 867
    .local v0, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/sec/android/app/shealth/home/HomeActivity;>;"
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->getInstance()Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/wearable/WearableSyncStatusManager;->getIsSycning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 868
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WearableSyncStatusManager getIsSycning : true"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/home/HomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$5;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncTimeoutRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$700(Lcom/sec/android/app/shealth/home/HomeActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 870
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/home/HomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$5;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncTimeoutRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$700(Lcom/sec/android/app/shealth/home/HomeActivity;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v3, 0x2710

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 880
    :goto_0
    return-void

    .line 872
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WearableSyncStatusManager getIsSycning : false"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/home/HomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$5;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncTimeoutRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$700(Lcom/sec/android/app/shealth/home/HomeActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 874
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    # setter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$802(Lcom/sec/android/app/shealth/home/HomeActivity;Z)Z

    .line 875
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    # setter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$902(Lcom/sec/android/app/shealth/home/HomeActivity;I)I

    .line 876
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$5;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$800(Lcom/sec/android/app/shealth/home/HomeActivity;)Z

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/home/HomeActivity;->setSyncProgressbarStatus(Z)V
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1000(Lcom/sec/android/app/shealth/home/HomeActivity;Z)V

    .line 877
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/HomeFragment;->getHomeLatestData()Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$800(Lcom/sec/android/app/shealth/home/HomeActivity;)Z

    move-result v1

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setReadyForSync(Z)V

    .line 878
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/HomeFragment;->getHomeLatestData()Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->updateView(Ljava/lang/Boolean;)V

    goto :goto_0
.end method
