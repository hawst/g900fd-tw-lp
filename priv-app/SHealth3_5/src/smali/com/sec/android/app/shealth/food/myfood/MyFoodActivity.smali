.class public Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "MyFoodActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$3;,
        Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$SpinnerCustomAdapter;,
        Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;,
        Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;
    }
.end annotation


# static fields
.field private static final ADD_CUSTOM_FOOD_CODE:I = 0x4d2

.field private static final DONE_BUTTON_INDEX:I = 0x0

.field private static final IS_DELETE_MODE:Ljava/lang/String; = "IS_DELETE_MODE"

.field private static final IS_DELETE_MODE_BY_LONG_PRESS:Ljava/lang/String; = "IS_DELETE_MODE_BY_LONG_PRESS"

.field private static final NO_RESOURCE:I = 0x0

.field private static final SINGLE_ITEM:I = 0x1

.field private static final sDeletePopupTag:Ljava/lang/String; = "DELETE_POPUP"

.field private static final sEditMyMealCode:I = 0x3039


# instance fields
.field private mDeleteModeActionBarButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

.field private mGeneralAction:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;

.field private mIsDeleteMode:Z

.field private mIsDeleteModeByLongPress:Z

.field private mSelectCountSpinner:Landroid/widget/Spinner;

.field private mSelectDataAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 585
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectCountSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->addSelectSpinner()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;
    .param p1, "x1"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->updateSelectSpinner(I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->setDoneButtonEnabled(Z)V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->refreshActivityState()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->addCustomFoodToMyFood()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->toggleActivityState(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->toggleActivityState()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->removeSelectSpinner()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->showDeletePopup()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteModeByLongPress:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteModeByLongPress:Z

    return p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mDeleteModeActionBarButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    return-object p1
.end method

.method private addCustomFoodToMyFood()V
    .locals 1

    .prologue
    .line 149
    invoke-static {p0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isMyFoodItemsLimitExceed(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    invoke-static {p0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->showToastAboutMyFoodItemsLimitExceeded(Landroid/content/Context;)V

    .line 154
    :goto_0
    return-void

    .line 153
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->addCustomFoodToMyFood(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private addCustomFoodToMyFood(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 157
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 159
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 161
    :cond_0
    const-string v1, "SHOULD_ALWAYS_SAVE_TO_MY_FOOD"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 162
    const/16 v1, 0x4d2

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 163
    return-void
.end method

.method private addSelectSpinner()V
    .locals 4

    .prologue
    .line 447
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectCountSpinner:Landroid/widget/Spinner;

    if-nez v0, :cond_0

    .line 449
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030003

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectCountSpinner:Landroid/widget/Spinner;

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectCountSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;-><init>(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 484
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    .line 487
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_2

    .line 489
    new-instance v0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$SpinnerCustomAdapter;

    const v1, 0x7f03021a

    const v2, 0x7f08080a

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$SpinnerCustomAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x7f030219

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 493
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectCountSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 494
    return-void
.end method

.method private getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f080417

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    return-object v0
.end method

.method private initFoodExpandableListFragment()V
    .locals 4

    .prologue
    const v3, 0x7f080417

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 171
    .local v0, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 172
    .local v1, "myFoodFragment":Landroid/support/v4/app/Fragment;
    if-nez v1, :cond_0

    .line 173
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    .end local v1    # "myFoodFragment":Landroid/support/v4/app/Fragment;
    invoke-direct {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;-><init>()V

    .line 174
    .restart local v1    # "myFoodFragment":Landroid/support/v4/app/Fragment;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->prepareParamsForExpandableListFragment()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 175
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 177
    :cond_0
    return-void
.end method

.method private prepareParamsForExpandableListFragment()Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 191
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 192
    .local v3, "params":Landroid/os/Bundle;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 193
    .local v0, "holdersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;>;"
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    const v4, 0x7f090944

    invoke-direct {v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;-><init>(I)V

    .line 194
    .local v1, "myFoodCategory":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsCheckable(Z)V

    .line 195
    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsExpandable(Z)V

    .line 196
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyFoodCategoryCreator;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyFoodCategoryCreator;-><init>()V

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setFoodCategoryListItemsCreator(Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;)V

    .line 197
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    const v4, 0x7f090977

    invoke-direct {v2, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;-><init>(I)V

    .line 199
    .local v2, "myMealCategory":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsCheckable(Z)V

    .line 200
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsExpandable(Z)V

    .line 201
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyMealCategoryCreator;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyMealCategoryCreator;-><init>()V

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setFoodCategoryListItemsCreator(Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;)V

    .line 202
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    const-string v4, "CATEGORY_HOLDERS_LIST"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 204
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    if-eqz v4, :cond_1

    .line 205
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mGeneralAction:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;

    invoke-interface {v4, v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;->prepareActionForAddMyFoodButton(Landroid/os/Bundle;)V

    .line 209
    :goto_1
    return-object v3

    :cond_0
    move v4, v5

    .line 200
    goto :goto_0

    .line 207
    :cond_1
    const-string/jumbo v4, "single_category_title_visible"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method private refreshActivityState()V
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mGeneralAction:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;->initActionBarWithDoneButton()V

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mGeneralAction:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;->refreshActionBarWithDoneButton()V

    .line 219
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->refreshFocusables()V

    .line 220
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mGeneralAction:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;->initDefaultActionBar()V

    goto :goto_0
.end method

.method private removeSelectSpinner()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 539
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeSpinnerView()V

    .line 540
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    .line 541
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 547
    :cond_0
    return-void
.end method

.method private setDoneButtonEnabled(Z)V
    .locals 4
    .param p1, "isEnabled"    # Z

    .prologue
    const/4 v3, 0x0

    .line 141
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mDeleteModeActionBarButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mDeleteModeActionBarButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 146
    :goto_0
    return-void

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeActionButton(I)V

    goto :goto_0
.end method

.method private showDeletePopup()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 551
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09011c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 552
    .local v0, "periodText":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->getCheckedItemsAmount()I

    move-result v1

    if-le v1, v3, :cond_0

    .line 553
    const v1, 0x7f09011d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->getCheckedItemsAmount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 558
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f090035

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "DELETE_POPUP"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 563
    return-void
.end method

.method private toggleActivityState()V
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->toggleActivityState(Z)V

    .line 181
    return-void
.end method

.method private toggleActivityState(Z)V
    .locals 2
    .param p1, "dataSetWasInvalidated"    # Z

    .prologue
    .line 184
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    .line 185
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->prepareParamsForExpandableListFragment()Landroid/os/Bundle;

    move-result-object v0

    .line 186
    .local v0, "params":Landroid/os/Bundle;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->updateFragmentState(Landroid/os/Bundle;Z)V

    .line 187
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->refreshActivityState()V

    .line 188
    return-void

    .line 184
    .end local v0    # "params":Landroid/os/Bundle;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateExpandableListFragment()V
    .locals 3

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->prepareParamsForExpandableListFragment()Landroid/os/Bundle;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->updateFragmentState(Landroid/os/Bundle;Z)V

    .line 242
    return-void
.end method

.method private updateSelectSpinner(I)V
    .locals 7
    .param p1, "selectedCounts"    # I

    .prologue
    const v6, 0x7f090073

    const v5, 0x7f090071

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectCountSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_1

    .line 500
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->addSelectSpinner()V

    .line 502
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    if-nez v0, :cond_2

    .line 504
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    .line 506
    :cond_2
    if-ltz p1, :cond_6

    .line 508
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setCustomViewVisibility(Z)V

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    const v1, 0x7f090074

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 511
    if-nez p1, :cond_4

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 525
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 526
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectCountSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 535
    :goto_1
    return-void

    .line 516
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->getExpandableListAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getGroupCount()I

    move-result v0

    if-ne p1, v0, :cond_5

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 520
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->getExpandableListAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getGroupCount()I

    move-result v0

    if-ge p1, v0, :cond_3

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 530
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setCustomViewVisibility(Z)V

    .line 531
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->disableActionBarButton(I)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_1
.end method


# virtual methods
.method public closeScreen()V
    .locals 0

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->onBackPressed()V

    .line 247
    return-void
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 109
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 567
    new-instance v0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$2;-><init>(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V

    return-object v0
.end method

.method public isDeleteMode()Z
    .locals 1

    .prologue
    .line 306
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 224
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 225
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 226
    sparse-switch p1, :sswitch_data_0

    .line 235
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wrong request code"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 228
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->updateExpandableListFragment()V

    .line 229
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->refreshActivityState()V

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 232
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->updateExpandableListFragment()V

    goto :goto_0

    .line 226
    :sswitch_data_0
    .sparse-switch
        0x4d2 -> :sswitch_0
        0x3039 -> :sswitch_1
    .end sparse-switch
.end method

.method public onAddCustomFoodClick()Z
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x0

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    if-eqz v0, :cond_1

    .line 252
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteModeByLongPress:Z

    if-eqz v0, :cond_0

    .line 253
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteModeByLongPress:Z

    .line 255
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->toggleActivityState()V

    .line 259
    :goto_0
    return-void

    .line 257
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 89
    const v0, 0x7f0300f7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->setContentView(I)V

    .line 90
    new-instance v0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;-><init>(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mGeneralAction:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;

    .line 92
    if-nez p1, :cond_0

    .line 93
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->initFoodExpandableListFragment()V

    .line 99
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 100
    return-void

    .line 95
    :cond_0
    const-string v0, "IS_DELETE_MODE"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    .line 96
    const-string v0, "IS_DELETE_MODE_BY_LONG_PRESS"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteModeByLongPress:Z

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method public onHeaderClick()V
    .locals 1

    .prologue
    .line 298
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mGeneralAction:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;->refreshActionBarWithDoneButton()V

    .line 300
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->getCheckedItemsAmount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->updateSelectSpinner(I)V

    .line 302
    :cond_0
    return-void
.end method

.method public onListItemLongClick(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z
    .locals 2
    .param p1, "selectedPanelHolder"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .prologue
    const/4 v0, 0x1

    .line 288
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    if-nez v1, :cond_0

    .line 289
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteModeByLongPress:Z

    .line 290
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->toggleActivityState()V

    .line 293
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 127
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->refreshActivityState()V

    .line 128
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 119
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 120
    const-string v0, "IS_DELETE_MODE"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 121
    const-string v0, "IS_DELETE_MODE_BY_LONG_PRESS"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteModeByLongPress:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 122
    return-void
.end method

.method public onSelectedPanelHolderSelected(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z
    .locals 11
    .param p1, "selectedPanelHolder"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .prologue
    const-wide/high16 v3, -0x8000000000000000L

    const/4 v10, 0x1

    .line 263
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z

    if-nez v1, :cond_0

    .line 264
    new-instance v8, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getMealItemDatas()Ljava/util/List;

    move-result-object v1

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 265
    .local v8, "mealItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v10, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getMealId()J

    move-result-wide v1

    const-wide/16 v5, -0x1

    cmp-long v1, v1, v5

    if-nez v1, :cond_1

    .line 266
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 267
    .local v9, "params":Landroid/os/Bundle;
    const-string v1, "MY_FOOD_INFO_DATA_ID"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getId()J

    move-result-wide v2

    invoke-virtual {v9, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 268
    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->addCustomFoodToMyFood(Landroid/os/Bundle;)V

    .line 277
    .end local v8    # "mealItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    .end local v9    # "params":Landroid/os/Bundle;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->refreshActivityState()V

    .line 278
    return v10

    .line 269
    .restart local v8    # "mealItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getCategory()Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->MEAL_DATA:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    if-ne v1, v2, :cond_0

    .line 270
    new-instance v7, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;

    invoke-direct {v7, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 271
    .local v7, "intent":Landroid/content/Intent;
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getId()J

    move-result-wide v1

    move-wide v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;-><init>(JJJ)V

    .line 273
    .local v0, "mealDataHolder":Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
    const-string v1, "MEAL_DATA_HOLDER"

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 274
    const/16 v1, 0x3039

    invoke-virtual {p0, v7, v1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method setActionBarTitle(I)V
    .locals 1
    .param p1, "titleId"    # I

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 132
    return-void
.end method

.method setActionBarTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 136
    return-void
.end method
