.class Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;
.super Landroid/database/ContentObserver;
.source "EditFavoritesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/EditFavoritesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RegistryContentObserver"
.end annotation


# instance fields
.field private mActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/home/EditFavoritesActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 575
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 576
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "activity"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    .prologue
    .line 580
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 581
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    .line 582
    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    return-object v0
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x1

    .line 587
    const-string v0, "EditFavorites"

    const-string v1, "RegistryContentObserver- On Change "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    # setter for: Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isChanged:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->access$202(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;Z)Z

    .line 591
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isPaused:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->access$800(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    new-instance v1, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver$1;-><init>(Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 609
    :cond_0
    :goto_0
    return-void

    .line 605
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    # setter for: Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->hasDataChanged:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->access$1102(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;Z)Z

    goto :goto_0
.end method
