.class public Lcom/sec/android/app/shealth/common/utils/UnitHelper;
.super Lcom/sec/android/app/shealth/common/utils/SharedPreferencesHelper;
.source "UnitHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/utils/UnitHelper$TEMPERATURE_TYPE;,
        Lcom/sec/android/app/shealth/common/utils/UnitHelper$DISTANCE_TYPE;,
        Lcom/sec/android/app/shealth/common/utils/UnitHelper$HEIGHT_TYPE;,
        Lcom/sec/android/app/shealth/common/utils/UnitHelper$WEIGHT_TYPE;
    }
.end annotation


# static fields
.field public static final BLOOD_GLUCOSE_META_CONSTANT:I = 0x8

.field private static final CELSIUS_TO_FAHRENHEIT_MULTIPLIER:F = 1.8f

.field private static final CELSIUS_TO_FAHRENHEIT_SUMMAND:I = 0x20

.field private static final CM_IN_FT:F = 30.48006f

.field private static final CM_IN_INCH:F = 2.54f

.field private static final CM_IN_METER:F = 100.0f

.field public static final DISTANCE_META_CONSTANT:I = 0x2

.field private static final FAHRENHEIT_TO_CELSIUS_MULTIPLIER:F = 0.5555556f

.field private static final FAHRENHEIT_TO_CELSIUS_SUMMAND:I = -0x20

.field private static final FT_IN_CM:F = 0.032808334f

.field private static final GLUCOSE_CONVERT_VALUE:F = 18.0182f

.field private static final INCHES_IN_CM:F = 0.39370078f

.field private static final KG_IN_LB:F = 0.45359236f

.field private static final LB_IN_KG:F = 2.2046227f

.field public static final TEMPERATURE_META_CONSTANT:I = 0x4

.field public static final WATER_META_CONSTANT:I = 0x10

.field public static final WEIGHT_META_CONSTANT:I = 0x1


# instance fields
.field public context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/utils/SharedPreferencesHelper;-><init>(Landroid/content/Context;)V

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->context:Landroid/content/Context;

    .line 22
    return-void
.end method

.method public static celsiusToFahrenheit(F)F
    .locals 2
    .param p0, "valueInCelsius"    # F

    .prologue
    .line 239
    const v0, 0x3fe66666    # 1.8f

    mul-float/2addr v0, p0

    const/high16 v1, 0x42000000    # 32.0f

    add-float/2addr v0, v1

    return v0
.end method

.method public static convertCmToFeet(F)F
    .locals 1
    .param p0, "valueInCm"    # F

    .prologue
    .line 215
    const v0, 0x41f3d72a

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertCmToInch(F)F
    .locals 1
    .param p0, "valueInInch"    # F

    .prologue
    .line 165
    const v0, 0x40228f5c    # 2.54f

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertCmToMeter(F)F
    .locals 1
    .param p0, "valueInCm"    # F

    .prologue
    .line 176
    const/high16 v0, 0x42c80000    # 100.0f

    div-float v0, p0, v0

    return v0
.end method

.method public static convertFeetToCm(F)F
    .locals 1
    .param p0, "valueInFeet"    # F

    .prologue
    .line 203
    const v0, 0x3d066208

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertInchToCm(F)F
    .locals 1
    .param p0, "valueInCm"    # F

    .prologue
    .line 153
    const v0, 0x3ec99326

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertKMsToMeters(I)F
    .locals 2
    .param p0, "valueInKMs"    # I

    .prologue
    .line 263
    int-to-float v0, p0

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public static convertKgToLb(F)F
    .locals 1
    .param p0, "valueInKg"    # F

    .prologue
    .line 99
    const v0, 0x3ee83d42

    div-float v0, p0, v0

    return v0
.end method

.method public static convertKgToUnit(FLjava/lang/String;)F
    .locals 2
    .param p0, "valueInKg"    # F
    .param p1, "unit"    # Ljava/lang/String;

    .prologue
    const/high16 v1, 0x41200000    # 10.0f

    .line 123
    const-string v0, "kg"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToLb(F)F

    move-result p0

    .line 126
    :cond_0
    mul-float v0, p0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    return v0
.end method

.method public static convertLbToKg(F)F
    .locals 1
    .param p0, "valueInLb"    # F

    .prologue
    .line 111
    const v0, 0x400d188a

    div-float v0, p0, v0

    return v0
.end method

.method public static convertMetersToKM(J)D
    .locals 2
    .param p0, "distanceInMeters"    # J

    .prologue
    .line 251
    long-to-float v0, p0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    return-wide v0
.end method

.method public static convertMetersToMiles(I)F
    .locals 2
    .param p0, "valueInMeters"    # I

    .prologue
    .line 276
    int-to-float v0, p0

    const v1, 0x3a22e36f

    mul-float/2addr v0, v1

    return v0
.end method

.method public static convertMgIntoMmol(F)F
    .locals 1
    .param p0, "valueInMg"    # F

    .prologue
    .line 301
    const v0, 0x41902546    # 18.0182f

    div-float v0, p0, v0

    return v0
.end method

.method public static convertMilesToMeters(F)I
    .locals 1
    .param p0, "valueInMiles"    # F

    .prologue
    .line 289
    const v0, 0x3a22e36f

    div-float v0, p0, v0

    float-to-int v0, v0

    return v0
.end method

.method public static convertMmolIntoMg(F)F
    .locals 1
    .param p0, "valueInMmol"    # F

    .prologue
    .line 313
    const v0, 0x41902546    # 18.0182f

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertUnitToCm(FLjava/lang/String;)F
    .locals 1
    .param p0, "value"    # F
    .param p1, "unit"    # Ljava/lang/String;

    .prologue
    .line 188
    const-string v0, "cm"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertInchToCm(F)F

    move-result p0

    .line 191
    :cond_0
    return p0
.end method

.method public static convertUnitToKg(FLjava/lang/String;)F
    .locals 1
    .param p0, "value"    # F
    .param p1, "unit"    # Ljava/lang/String;

    .prologue
    .line 138
    const-string v0, "kg"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertLbToKg(F)F

    move-result p0

    .line 141
    :cond_0
    return p0
.end method

.method public static fahrenheitToCelsius(F)F
    .locals 2
    .param p0, "valueInFh"    # F

    .prologue
    .line 227
    const/high16 v0, -0x3e000000    # -32.0f

    add-float/2addr v0, p0

    const v1, 0x3f0e38e4

    mul-float/2addr v0, v1

    return v0
.end method

.method public static getGlucoseUnit(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "defaultUnit"    # Ljava/lang/String;

    .prologue
    .line 519
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getGlucoseUnit()Ljava/lang/String;

    move-result-object v0

    .line 520
    .local v0, "glucoseUnit":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 521
    move-object v0, p1

    .line 523
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getDistanceUnit()Ljava/lang/String;
    .locals 2

    .prologue
    .line 392
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isUSAModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    const-string v0, "distance_unit"

    const-string/jumbo v1, "mi"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 395
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "distance_unit"

    const-string v1, "km"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getGlucoseUnit()Ljava/lang/String;
    .locals 2

    .prologue
    .line 404
    const-string v0, "glucose_unit"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeightUnit()Ljava/lang/String;
    .locals 2

    .prologue
    .line 322
    const-string v0, "height_unit"

    const-string v1, "cm"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeightUnit(I)Ljava/lang/String;
    .locals 1
    .param p1, "intUnit"    # I

    .prologue
    .line 331
    packed-switch p1, :pswitch_data_0

    .line 338
    const-string v0, ""

    :goto_0
    return-object v0

    .line 333
    :pswitch_0
    const-string v0, "cm"

    goto :goto_0

    .line 335
    :pswitch_1
    const-string v0, "ft, inch"

    goto :goto_0

    .line 331
    :pswitch_data_0
    .packed-switch 0x249f1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getTemperatureUnit()Ljava/lang/String;
    .locals 2

    .prologue
    .line 374
    const-string/jumbo v0, "temperature_unit"

    const-string v1, "C"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWaterUnit()Ljava/lang/String;
    .locals 2

    .prologue
    .line 413
    const-string/jumbo v0, "water_unit"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWeekFormat()Ljava/lang/String;
    .locals 2

    .prologue
    .line 383
    const-string/jumbo v0, "week_format"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWeightUnit()Ljava/lang/String;
    .locals 2

    .prologue
    .line 348
    const-string/jumbo v0, "weight_unit"

    const-string v1, "kg"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWeightUnit(I)Ljava/lang/String;
    .locals 1
    .param p1, "intUnit"    # I

    .prologue
    .line 357
    packed-switch p1, :pswitch_data_0

    .line 364
    const-string v0, ""

    :goto_0
    return-object v0

    .line 359
    :pswitch_0
    const-string v0, "kg"

    goto :goto_0

    .line 361
    :pswitch_1
    const-string v0, "lb"

    goto :goto_0

    .line 357
    :pswitch_data_0
    .packed-switch 0x1fbd1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public putDistanceUnit(Ljava/lang/String;)V
    .locals 1
    .param p1, "distanceUnit"    # Ljava/lang/String;

    .prologue
    .line 490
    const-string v0, "distance_unit"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->writeString(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    return-void
.end method

.method public putGlucoseUnit(Ljava/lang/String;)V
    .locals 1
    .param p1, "glucoseUnit"    # Ljava/lang/String;

    .prologue
    .line 499
    const-string v0, "glucose_unit"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->writeString(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    return-void
.end method

.method public putHeightunit(Ljava/lang/String;)V
    .locals 3
    .param p1, "heightUnit"    # Ljava/lang/String;

    .prologue
    .line 424
    const-string v1, "cm"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 426
    const v0, 0x249f1

    .line 437
    .local v0, "value":I
    :goto_0
    const-string v1, "height_unit"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getHeightUnit(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->writeString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    return-void

    .line 428
    .end local v0    # "value":I
    :cond_0
    const-string v1, "ft, inch"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 430
    const v0, 0x249f2

    .restart local v0    # "value":I
    goto :goto_0

    .line 434
    .end local v0    # "value":I
    :cond_1
    const/4 v0, -0x1

    .restart local v0    # "value":I
    goto :goto_0
.end method

.method public putTemperatureUnit(Ljava/lang/String;)V
    .locals 1
    .param p1, "temperatureUnit"    # Ljava/lang/String;

    .prologue
    .line 472
    const-string/jumbo v0, "temperature_unit"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->writeString(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    return-void
.end method

.method public putWaterUnit(Ljava/lang/String;)V
    .locals 1
    .param p1, "waterUnit"    # Ljava/lang/String;

    .prologue
    .line 508
    const-string/jumbo v0, "water_unit"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->writeString(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    return-void
.end method

.method public putWeekFormat(Ljava/lang/String;)V
    .locals 1
    .param p1, "weekFormat"    # Ljava/lang/String;

    .prologue
    .line 481
    const-string/jumbo v0, "week_format"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->writeString(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    return-void
.end method

.method public putweightUnit(Ljava/lang/String;)V
    .locals 3
    .param p1, "weightUnit"    # Ljava/lang/String;

    .prologue
    .line 449
    const-string v1, "kg"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 451
    const v0, 0x1fbd1

    .line 462
    .local v0, "value":I
    :goto_0
    const-string/jumbo v1, "weight_unit"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getWeightUnit(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->writeString(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    return-void

    .line 453
    .end local v0    # "value":I
    :cond_0
    const-string v1, "lb"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 455
    const v0, 0x1fbd2

    .restart local v0    # "value":I
    goto :goto_0

    .line 459
    .end local v0    # "value":I
    :cond_1
    const/4 v0, -0x1

    .restart local v0    # "value":I
    goto :goto_0
.end method
