.class Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;
.super Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
.source "MyRankingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method protected processMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/16 v1, 0x21

    .line 131
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 154
    :goto_0
    return-void

    .line 133
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->sendRequestForNewUserToken()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$000(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    goto :goto_0

    .line 136
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->sendRequestForUserToken()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$100(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    goto :goto_0

    .line 139
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->removeMessages(I)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->dismissLoadingDialog()V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateViewsWithLatestValues()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->refreshFragmentFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$400(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    goto :goto_0

    .line 145
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->removeMessages(I)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->dismissLoadingDialog()V

    .line 147
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isMyRankingDataAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadLastSuccessfullyReceivedData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$500(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    goto :goto_0

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->showUpdateFail()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$600(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    goto :goto_0

    .line 131
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xd -> :sswitch_1
        0x17 -> :sswitch_2
        0x21 -> :sswitch_3
    .end sparse-switch
.end method

.method protected storeMessage(Landroid/os/Message;)Z
    .locals 1
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 126
    const/4 v0, 0x1

    return v0
.end method
