.class final Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;
.super Ljava/lang/Object;
.source "UtilsCommon.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->refreshLastTakenPhoto(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private mScannerConnection:Landroid/media/MediaScannerConnection;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$scanPath:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 817
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;->val$scanPath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 821
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;->val$context:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 822
    new-instance v0, Landroid/media/MediaScannerConnection;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;->val$context:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;->mScannerConnection:Landroid/media/MediaScannerConnection;

    .line 823
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;->mScannerConnection:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->connect()V

    .line 825
    :cond_0
    return-void
.end method


# virtual methods
.method public onMediaScannerConnected()V
    .locals 3

    .prologue
    .line 829
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;->mScannerConnection:Landroid/media/MediaScannerConnection;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;->val$scanPath:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onMediaScannerConnected() scanPath="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;->val$scanPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    return-void
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 835
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onScanCompleted() scanPath="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;->val$scanPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;->mScannerConnection:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->disconnect()V

    .line 837
    return-void
.end method
