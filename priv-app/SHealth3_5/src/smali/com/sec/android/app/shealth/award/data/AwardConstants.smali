.class public Lcom/sec/android/app/shealth/award/data/AwardConstants;
.super Ljava/lang/Object;
.source "AwardConstants.java"


# static fields
.field public static final AWARD:I = 0x14

.field public static final BLOOD_GLUCOSE:I = 0xa

.field public static final BLOOD_PRESSURE:I = 0xb

.field public static final BODY_FAT:I = 0x12

.field public static final BODY_TEMPERATURE:I = 0xc

.field public static final BURNT_CALORIES:I = 0x61

.field public static final CAREGIVER:I = 0xf

.field public static final CHALLENGE:I = 0xe

.field public static final CIGNA_COACH:I = 0x10

.field public static final DEFAULT:I = -0x1

.field public static final DEFAULT_FEATURE:I = 0x62

.field public static final DOSAGE:I = 0xd

.field public static final ECG:I = 0x11

.field public static final EXERCISE_MATE:I = 0x2

.field public static final EXERCISE_PRO:I = 0x1

.field public static final FOOD_TRACKER:I = 0x3

.field public static final HEART_BEAT:I = 0x6

.field public static final HEART_RATE:I = 0x15

.field public static final SECTION:I = 0x63

.field public static final SLEEP:I = 0x8

.field public static final SPO2:I = 0x13

.field public static final STRESS:I = 0x7

.field public static final THERMO_HYGROMETER:I = 0x9

.field public static final UV:I = 0x16

.field public static final WALKING_MATE:I = 0x0

.field public static final WATER_INTAKE:I = 0x5

.field public static final WEIGHT:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
