.class public Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;
.super Landroid/app/Dialog;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SyncingDialog"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 659
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 660
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x1

    .line 664
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 665
    new-instance v7, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v7}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 666
    .local v7, "lpWindow":Landroid/view/WindowManager$LayoutParams;
    const/4 v1, 0x2

    iput v1, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 667
    const v1, 0x3e99999a    # 0.3f

    iput v1, v7, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 668
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 669
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;->requestWindowFeature(I)Z

    .line 670
    const v1, 0x7f030178

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;->setContentView(I)V

    .line 671
    const v1, 0x7f08064a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 672
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;->setCancelable(Z)V

    .line 673
    const v1, 0x7f0902af

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;->setText(I)V

    .line 674
    const v1, 0x7f08064b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 675
    .local v8, "receivingView":Landroid/view/View;
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 676
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 677
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 678
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 679
    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 680
    invoke-virtual {v8, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 681
    return-void
.end method

.method protected setText(I)V
    .locals 2
    .param p1, "resourceId"    # I

    .prologue
    .line 684
    const v0, 0x7f08064c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 685
    return-void
.end method
