.class public Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
.source "StressSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/stress/StressSensorListener;


# static fields
.field public static final TAG:Ljava/lang/String;

.field public static final TEST_EMAIL_ADDRESS:Ljava/lang/String; = "samstresstest@outlook.com"

.field static effectAudio:[I

.field public static isDialogDissmised:Z


# instance fields
.field age:I

.field animateType:I

.field animationFadeIn:Landroid/view/animation/AnimationSet;

.field animationFadeOut:Landroid/view/animation/AnimationSet;

.field private bpmAverage:I

.field private bpmDatas:[I

.field public dataCollection:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private errorImages:[I

.field private errorImagesArray:[Ljava/lang/String;

.field fadeIn:Landroid/view/animation/Animation;

.field fadeOut:Landroid/view/animation/Animation;

.field isImage1Found:Z

.field isImage2Found:Z

.field private isTalkBackCount:Z

.field private mAnimationViewErr0:Landroid/widget/ImageView;

.field private mAnimationViewErr1:Landroid/widget/ImageView;

.field private mCircleAnimationSet:Landroid/view/animation/AnimationSet;

.field private mContext:Landroid/content/Context;

.field public mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mDataCollectedProgressPercent:Landroid/widget/TextView;

.field private mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

.field private mDeviceSupportCoverSDK:Z

.field private mDiscardButton:Landroid/widget/TextView;

.field private mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mErrorDialogMsg1:Landroid/widget/TextView;

.field private mErrorDialogMsg2:Landroid/widget/TextView;

.field private mErrorDialogMsg3:Landroid/widget/TextView;

.field private mErrorDialogMsg4:Landroid/widget/TextView;

.field private mErrorView:Landroid/view/View;

.field private mFinishedLogSent:Z

.field private mFirstLayout:Landroid/widget/FrameLayout;

.field private mFirstMessage:Landroid/widget/TextView;

.field private mFirstReadyLayout:Landroid/widget/LinearLayout;

.field private mFirstReadyMessage:Landroid/widget/TextView;

.field private mFirstReadyText:Landroid/widget/TextView;

.field private mFirstStateLayout:Landroid/widget/LinearLayout;

.field public mFirstStatus:Landroid/widget/TextView;

.field public mHandler:Landroid/os/Handler;

.field private mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

.field private mHigh:Landroid/widget/TextView;

.field private mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

.field private mIsFromTimeout:Z

.field private mIsGreen:Z

.field private mIsOnConfigChanged:Z

.field private mLightningLeftAnimator:Landroid/animation/ValueAnimator;

.field private mLightningRightAnimator:Landroid/animation/ValueAnimator;

.field private mLow:Landroid/widget/TextView;

.field private mMeasuringAniLinear:Landroid/widget/LinearLayout;

.field private mScaleAnimator:Landroid/animation/ValueAnimator;

.field private mScover:Lcom/samsung/android/sdk/cover/Scover;

.field private mSecondCenterIcon:Landroid/widget/ImageView;

.field private mSecondCenterIconBig:Landroid/widget/ImageView;

.field private mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

.field private mSecondCenterIconBigLeftLightning:Landroid/widget/ImageView;

.field private mSecondCenterIconBigRightLightning:Landroid/widget/ImageView;

.field private mSecondCenterIconFinish:Landroid/widget/ImageView;

.field private mSecondIcon:Landroid/widget/ImageView;

.field private mSecondLayout:Landroid/widget/FrameLayout;

.field private mSecondMeasuringProgressView:Landroid/widget/ImageView;

.field private mSecondMeasuringProgressView1:Landroid/widget/ImageView;

.field private mStartButton:Landroid/widget/TextView;

.field mState:Lcom/sec/android/app/shealth/stress/state/StressState;

.field private mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

.field private mStateFinished:Lcom/sec/android/app/shealth/stress/state/StressState;

.field private mStateMeasureFailed:Lcom/sec/android/app/shealth/stress/state/StressState;

.field private mStateMeasureWarning:Lcom/sec/android/app/shealth/stress/state/StressState;

.field private mStateMeasuring:Lcom/sec/android/app/shealth/stress/state/StressState;

.field private mStateReady:Lcom/sec/android/app/shealth/stress/state/StressState;

.field private mStateSmallBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummarySmallBar;

.field private mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

.field private mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

.field private mStressScore:Landroid/widget/TextView;

.field private mThirdBpm:Landroid/widget/TextView;

.field private mThirdDate:Landroid/widget/TextView;

.field private mThirdDiscard:Landroid/widget/LinearLayout;

.field private mThirdGraph:Landroid/widget/ImageButton;

.field private mThirdPreviousLayout:Landroid/widget/LinearLayout;

.field private mThirdStart:Landroid/widget/LinearLayout;

.field private mThirdStartDiscard:Landroid/widget/LinearLayout;

.field private mThirdTime:Landroid/widget/TextView;

.field public percent:I

.field private pool:Landroid/media/SoundPool;

.field public preSumRRI:I

.field public sumRRI:I

.field private toggleFlag:Z

.field private wasShowInformationDialog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    const-class v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    .line 123
    const/4 v0, 0x5

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->effectAudio:[I

    .line 173
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isDialogDissmised:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v1, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;-><init>()V

    .line 145
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->toggleFlag:Z

    .line 166
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImages:[I

    .line 167
    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "hr_fail_t_1"

    aput-object v1, v0, v3

    const-string v1, "hr_fail_t_2"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "hr_fail_t_3"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImagesArray:[Ljava/lang/String;

    .line 174
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isImage1Found:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isImage2Found:Z

    .line 179
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    .line 190
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceSupportCoverSDK:Z

    .line 194
    iput v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->bpmAverage:I

    .line 198
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsOnConfigChanged:Z

    .line 203
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFinishedLogSent:Z

    .line 209
    new-instance v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHandler:Landroid/os/Handler;

    .line 724
    new-instance v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

    .line 1118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    .line 1130
    iput v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->sumRRI:I

    .line 1131
    iput v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->preSumRRI:I

    .line 1132
    iput v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    .line 1134
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isTalkBackCount:Z

    .line 1243
    iput v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->age:I

    return-void

    .line 166
    :array_0
    .array-data 4
        0x7f0201f2
        0x7f0201f3
        0x7f0201f4
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setReadyUI(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measuringCircleAnimationStop(Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->showGraphFragment()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->playSound(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/stress/StressActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setTextForFirstMessageById(I)V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/animation/AnimationSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Landroid/view/animation/AnimationSet;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconFinish:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->maskingImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->lightningImageChange()V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # J

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->stressLightningAlphaAnimation(J)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/media/SoundPool;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->pool:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$3402(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg1:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$3502(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg2:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$3602(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg3:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$3702(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg4:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningLeftAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningRightAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;
    .param p2, "x2"    # Landroid/widget/ImageView;
    .param p3, "x3"    # [I
    .param p4, "x4"    # [Ljava/lang/String;
    .param p5, "x5"    # I
    .param p6, "x6"    # Z

    .prologue
    .line 118
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    return-void
.end method

.method static synthetic access$4100(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->stressLightningLeftAnitmaion()V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->stressLightningRightAnitmaion()V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsGreen:Z

    return v0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLeftLightning:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->stressTranslateAnimation(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$4600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigRightLightning:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStartDiscard:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsOnConfigChanged:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsOnConfigChanged:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->initDialogAnimation()V

    return-void
.end method

.method private animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    .locals 12
    .param p1, "imageView1"    # Landroid/widget/ImageView;
    .param p2, "imageView2"    # Landroid/widget/ImageView;
    .param p3, "images"    # [I
    .param p4, "ImageArr"    # [Ljava/lang/String;
    .param p5, "imageIndex"    # I
    .param p6, "forever"    # Z

    .prologue
    .line 1654
    move/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->initFadingAnimationEffect(I)V

    .line 1655
    invoke-virtual {p1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1656
    .local v9, "ImgTag1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1657
    .local v10, "ImgTag2":Ljava/lang/String;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isImage1Found:Z

    .line 1658
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isImage2Found:Z

    .line 1659
    aget-object v1, p4, p5

    invoke-virtual {v9, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1660
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isImage1Found:Z

    .line 1661
    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1662
    invoke-virtual {p2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1663
    array-length v1, p3

    add-int/lit8 v1, v1, -0x1

    move/from16 v0, p5

    if-ne v1, v0, :cond_2

    .line 1664
    const/4 v1, 0x0

    aget v1, p3, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1665
    const/4 v1, 0x0

    aget-object v1, p4, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1670
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeOut:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1671
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1673
    :cond_0
    aget-object v1, p4, p5

    invoke-virtual {v10, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1674
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isImage2Found:Z

    .line 1675
    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1676
    invoke-virtual {p2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1677
    array-length v1, p3

    add-int/lit8 v1, v1, -0x1

    move/from16 v0, p5

    if-ne v0, v1, :cond_3

    .line 1679
    const/4 v1, 0x0

    aget v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1680
    const/4 v1, 0x0

    aget-object v1, p4, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1685
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeOut:Landroid/view/animation/AnimationSet;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1686
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1689
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeIn:Landroid/view/animation/AnimationSet;

    new-instance v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$16;

    invoke-direct {v2, p0, p2, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$16;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1709
    iget-object v11, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeOut:Landroid/view/animation/AnimationSet;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$17;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move/from16 v6, p5

    move-object/from16 v7, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$17;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/ImageView;Landroid/widget/ImageView;[II[Ljava/lang/String;Z)V

    invoke-virtual {v11, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1736
    return-void

    .line 1667
    :cond_2
    add-int/lit8 v1, p5, 0x1

    aget v1, p3, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1668
    add-int/lit8 v1, p5, 0x1

    aget-object v1, p4, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 1682
    :cond_3
    add-int/lit8 v1, p5, 0x1

    aget v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1683
    add-int/lit8 v1, p5, 0x1

    aget-object v1, p4, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private calculateStressData(I)Lcom/sec/android/app/shealth/stress/data/StressStateType;
    .locals 11
    .param p1, "age"    # I

    .prologue
    .line 1366
    sget-object v4, Lcom/sec/android/app/shealth/stress/data/StressStateType;->INVALID:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    .line 1374
    .local v4, "stateType":Lcom/sec/android/app/shealth/stress/data/StressStateType;
    const/4 v8, 0x1

    invoke-static {p1, v8}, Lcom/sec/dmc/hsl/android/stressanalyzer/StressAnalyzer;->calcStressScore(II)D

    move-result-wide v5

    .line 1376
    .local v5, "stressScore":D
    invoke-static {}, Lcom/sec/dmc/hsl/android/stressanalyzer/StressAnalyzer;->getCalculatedStressIndex()I

    move-result v7

    .line 1378
    .local v7, "stressState":I
    invoke-static {}, Lcom/sec/android/app/shealth/stress/data/StressStateType;->values()[Lcom/sec/android/app/shealth/stress/data/StressStateType;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/stress/data/StressStateType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 1379
    .local v1, "each":Lcom/sec/android/app/shealth/stress/data/StressStateType;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/data/StressStateType;->getState()I

    move-result v8

    if-ne v8, v7, :cond_0

    .line 1380
    move-object v4, v1

    .line 1381
    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/stress/data/StressStateType;->setScore(D)V

    .line 1378
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1384
    .end local v1    # "each":Lcom/sec/android/app/shealth/stress/data/StressStateType;
    :cond_1
    sget-object v8, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "calculateStressData : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1385
    return-object v4
.end method

.method private firstAnimation(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 783
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    if-nez v0, :cond_0

    .line 813
    :goto_0
    return-void

    .line 786
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$4;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private getUserAge()I
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x1

    .line 1347
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 1349
    .local v3, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1350
    .local v1, "birth":Ljava/util/Calendar;
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1352
    .local v4, "today":Ljava/util/Calendar;
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 1354
    .local v0, "bDate":Ljava/util/Date;
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1355
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1357
    const/4 v2, 0x0

    .line 1358
    .local v2, "factor":I
    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 1359
    const/4 v2, -0x1

    .line 1362
    :cond_0
    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    sub-int/2addr v5, v6

    add-int/2addr v5, v2

    return v5
.end method

.method private initAoudioFile()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 1448
    new-instance v0, Landroid/media/SoundPool;

    invoke-direct {v0, v5, v6, v4}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->pool:Landroid/media/SoundPool;

    .line 1449
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f06002c

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v4

    .line 1450
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f060027

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v5

    .line 1451
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->effectAudio:[I

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f06002d

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 1452
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f060028

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v6

    .line 1453
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->effectAudio:[I

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f06002a

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 1454
    return-void
.end method

.method private initDialogAnimation()V
    .locals 3

    .prologue
    .line 1591
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1592
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 1593
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;

    const v1, 0x7f0809d1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;

    .line 1594
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;

    const v1, 0x7f0809d2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;

    .line 1595
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImages:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1596
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImages:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1599
    :cond_0
    return-void
.end method

.method private initFadingAnimationEffect(I)V
    .locals 7
    .param p1, "imageIndex"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1628
    const/16 v0, 0x1f4

    .line 1629
    .local v0, "fadeInDuration":I
    const/16 v2, 0x5dc

    .line 1630
    .local v2, "timeBetween":I
    const/16 v1, 0x1f4

    .line 1632
    .local v1, "fadeOutDuration":I
    const/4 v3, 0x4

    if-eq p1, v3, :cond_0

    const/4 v3, 0x5

    if-eq p1, v3, :cond_0

    const/4 v3, 0x6

    if-ne p1, v3, :cond_1

    .line 1633
    :cond_0
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeIn:Landroid/view/animation/Animation;

    .line 1634
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v4, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeOut:Landroid/view/animation/Animation;

    .line 1640
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeIn:Landroid/view/animation/Animation;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1641
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeIn:Landroid/view/animation/Animation;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1642
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeIn:Landroid/view/animation/Animation;

    const/16 v4, 0x514

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1644
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeOut:Landroid/view/animation/Animation;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1645
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeOut:Landroid/view/animation/Animation;

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1646
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeOut:Landroid/view/animation/Animation;

    int-to-long v4, v1

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1647
    new-instance v3, Landroid/view/animation/AnimationSet;

    invoke-direct {v3, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeIn:Landroid/view/animation/AnimationSet;

    .line 1648
    new-instance v3, Landroid/view/animation/AnimationSet;

    invoke-direct {v3, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeOut:Landroid/view/animation/AnimationSet;

    .line 1649
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeIn:Landroid/view/animation/AnimationSet;

    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeIn:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1650
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeOut:Landroid/view/animation/AnimationSet;

    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeOut:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1651
    return-void

    .line 1636
    :cond_1
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeIn:Landroid/view/animation/Animation;

    .line 1637
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->fadeOut:Landroid/view/animation/Animation;

    goto :goto_0
.end method

.method private initSCover()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 303
    sget-object v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string v2, "initSCover"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    new-instance v1, Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct {v1}, Lcom/samsung/android/sdk/cover/Scover;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    .line 306
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/Scover;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 312
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceSupportCoverSDK:Z

    if-eqz v1, :cond_0

    .line 313
    new-instance v1, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 314
    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 362
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 365
    :cond_0
    return-void

    .line 307
    :catch_0
    move-exception v0

    .line 308
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceSupportCoverSDK:Z

    goto :goto_0

    .line 309
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 310
    .local v0, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceSupportCoverSDK:Z

    goto :goto_0
.end method

.method private initView(Landroid/view/View;)Landroid/view/View;
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v7, 0x7f09020b

    const v6, 0x7f09020a

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 414
    const v0, 0x7f0805d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    .line 415
    const v0, 0x7f0805d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    .line 416
    const v0, 0x7f0809fd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;

    .line 418
    const v0, 0x7f0809f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLow:Landroid/widget/TextView;

    .line 419
    const v0, 0x7f0809f2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHigh:Landroid/widget/TextView;

    .line 420
    const v0, 0x7f080a06

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStartButton:Landroid/widget/TextView;

    .line 421
    const v0, 0x7f0809f5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDiscardButton:Landroid/widget/TextView;

    .line 422
    const v0, 0x7f0809f8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstReadyText:Landroid/widget/TextView;

    .line 423
    const v0, 0x7f0809f9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstReadyMessage:Landroid/widget/TextView;

    .line 425
    const v0, 0x7f0809fc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    .line 427
    const v0, 0x7f0809fb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressScore:Landroid/widget/TextView;

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressScore:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 430
    const v0, 0x7f0809f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstLayout:Landroid/widget/FrameLayout;

    .line 431
    const v0, 0x7f0809f7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    .line 432
    const v0, 0x7f0809fa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    .line 433
    const v0, 0x7f0805ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    .line 434
    const v0, 0x7f0805cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    .line 437
    const v0, 0x7f0805da

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

    .line 438
    const v0, 0x7f080595

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    .line 439
    const v0, 0x7f0805bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    .line 440
    const v0, 0x7f0805c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    .line 443
    const v0, 0x7f0809c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    .line 444
    const v0, 0x7f0805c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    .line 445
    const v0, 0x7f080a00

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLeftLightning:Landroid/widget/ImageView;

    .line 446
    const v0, 0x7f0809ff

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigRightLightning:Landroid/widget/ImageView;

    .line 448
    const v0, 0x7f0809fe

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconFinish:Landroid/widget/ImageView;

    .line 453
    const v0, 0x7f080a01

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    .line 454
    const v0, 0x7f080a04

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStart:Landroid/widget/LinearLayout;

    .line 455
    const v0, 0x7f080a03

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090f4d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStart:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901ea

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 461
    const v0, 0x7f080a02

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStartDiscard:Landroid/widget/LinearLayout;

    .line 462
    const v0, 0x7f0805e5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    .line 463
    const v0, 0x7f0809c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/custom/StressStateSummarySmallBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateSmallBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummarySmallBar;

    .line 464
    const v0, 0x7f0805ec

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdDate:Landroid/widget/TextView;

    .line 465
    const v0, 0x7f0809cd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdTime:Landroid/widget/TextView;

    .line 466
    const v0, 0x7f0805ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdGraph:Landroid/widget/ImageButton;

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStart:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setSoundEffectsEnabled(Z)V

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStart:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setSoundEffectsEnabled(Z)V

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 472
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdGraph:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 475
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 479
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->setGesturesEnabled(Z)V

    .line 487
    return-object p1
.end method

.method private isSamsungAccount()Z
    .locals 10

    .prologue
    .line 1892
    const/4 v6, 0x0

    .line 1893
    .local v6, "isSamAccount":Z
    iget-object v9, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 1894
    .local v0, "accm":Landroid/accounts/AccountManager;
    if-nez v0, :cond_0

    .line 1895
    const/4 v9, 0x0

    .line 1908
    :goto_0
    return v9

    .line 1897
    :cond_0
    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 1898
    .local v2, "accounts":[Landroid/accounts/Account;
    sget-object v4, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    .line 1900
    .local v4, "emailPattern":Ljava/util/regex/Pattern;
    move-object v3, v2

    .local v3, "arr$":[Landroid/accounts/Account;
    array-length v7, v3

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v7, :cond_2

    aget-object v1, v3, v5

    .line 1901
    .local v1, "account":Landroid/accounts/Account;
    iget-object v9, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1902
    iget-object v8, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 1903
    .local v8, "possibleEmail":Ljava/lang/String;
    const-string/jumbo v9, "samstresstest@outlook.com"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1904
    const/4 v6, 0x1

    .line 1900
    .end local v8    # "possibleEmail":Ljava/lang/String;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .end local v1    # "account":Landroid/accounts/Account;
    :cond_2
    move v9, v6

    .line 1908
    goto :goto_0
.end method

.method private lightningImageChange()V
    .locals 2

    .prologue
    .line 1871
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLeftLightning:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsGreen:Z

    if-eqz v0, :cond_0

    const v0, 0x7f02062d

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1873
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigRightLightning:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsGreen:Z

    if-eqz v0, :cond_1

    const v0, 0x7f02062e

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1875
    return-void

    .line 1871
    :cond_0
    const v0, 0x7f020630

    goto :goto_0

    .line 1873
    :cond_1
    const v0, 0x7f020631

    goto :goto_1
.end method

.method private maskingImage(II)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "drawable"    # I
    .param p2, "colorResource"    # I

    .prologue
    const/4 v7, 0x0

    .line 1335
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1336
    .local v1, "mask":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1337
    .local v3, "result":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1338
    .local v0, "mCanvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 1339
    .local v2, "paint":Landroid/graphics/Paint;
    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 1340
    invoke-virtual {v0, p2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1341
    invoke-virtual {v0, v1, v7, v7, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1342
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 1343
    return-object v3
.end method

.method private measuringCircleAnimationStart(Z)V
    .locals 2
    .param p1, "isFailed"    # Z

    .prologue
    .line 827
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 855
    return-void
.end method

.method private measuringCircleAnimationStop(Z)V
    .locals 3
    .param p1, "isEnd"    # Z

    .prologue
    .line 859
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 860
    .local v0, "fadeOut":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 861
    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$7;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 883
    if-eqz p1, :cond_1

    .line 884
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    if-eqz v1, :cond_0

    .line 885
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 895
    :cond_0
    :goto_0
    return-void

    .line 888
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 889
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 890
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    if-eqz v1, :cond_0

    .line 891
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1}, Landroid/view/animation/AnimationSet;->cancel()V

    .line 892
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    goto :goto_0
.end method

.method private playSound(I)V
    .locals 3
    .param p1, "effectAudio"    # I

    .prologue
    .line 1458
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1460
    .local v0, "aManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-nez v1, :cond_1

    .line 1477
    :cond_0
    :goto_0
    return-void

    .line 1464
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->pool:Landroid/media/SoundPool;

    if-eqz v1, :cond_0

    .line 1465
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    new-instance v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$12;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$12;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;I)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/stress/StressActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private recycleDrawable(Landroid/graphics/drawable/AnimationDrawable;)V
    .locals 5
    .param p1, "drawable"    # Landroid/graphics/drawable/AnimationDrawable;

    .prologue
    const/4 v4, 0x0

    .line 1878
    if-nez p1, :cond_0

    .line 1889
    :goto_0
    return-void

    .line 1880
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 1881
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/graphics/drawable/AnimationDrawable;->selectDrawable(I)Z

    .line 1882
    invoke-virtual {p1}, Landroid/graphics/drawable/AnimationDrawable;->getNumberOfFrames()I

    move-result v0

    .line 1883
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_1

    .line 1884
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/AnimationDrawable;->getFrame(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1886
    .local v1, "currentDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1883
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1888
    .end local v1    # "currentDrawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {p1, v4}, Landroid/graphics/drawable/AnimationDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    goto :goto_0
.end method

.method private scaleAnimationStarter(Z)V
    .locals 3
    .param p1, "isGreen"    # Z

    .prologue
    .line 1389
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "scaleAnimationStarter isGreen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1391
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1392
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1393
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3eeb851f    # 0.46f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 1394
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3f851eb8    # 1.04f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 1395
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    .line 1396
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1397
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$10;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1406
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1435
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1439
    :goto_0
    return-void

    .line 1437
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const v0, 0x7f020633

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f020634

    goto :goto_1

    .line 1395
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3fc28f5c    # 1.52f
    .end array-data
.end method

.method private setReadyUI(I)V
    .locals 8
    .param p1, "animation"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 947
    sget-object v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setReadyUI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 950
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 951
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 952
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 953
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 954
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setVisibility(I)V

    .line 955
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->resetProgress()V

    .line 956
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;->setVisibility(I)V

    .line 957
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 958
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 959
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconFinish:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 960
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconFinish:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 961
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconFinish:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 963
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v2, 0x7f020633

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 964
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040022

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 965
    .local v0, "alphaAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 967
    if-nez p1, :cond_1

    .line 968
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstLayout:Landroid/widget/FrameLayout;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->firstAnimation(Landroid/view/View;)V

    .line 969
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 970
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 976
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v2, 0x7f020522

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 977
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStartDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 979
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->updateBpmDataView(Z)V

    .line 980
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 981
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    const v2, 0x8000

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 983
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFinishedLogSent:Z

    .line 984
    return-void

    .line 972
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 973
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setTextForFirstMessageById(I)V
    .locals 4
    .param p1, "resource"    # I

    .prologue
    const/4 v3, 0x1

    .line 286
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 287
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x64

    if-le v1, v2, :cond_0

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 294
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    return-void

    .line 289
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x23

    if-le v1, v2, :cond_1

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v2, 0x41980000    # 19.0f

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 292
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v2, 0x41b80000    # 23.0f

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method private showGraphFragment()V
    .locals 4

    .prologue
    .line 775
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->switchFragmentToGraph()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 779
    return-void

    .line 776
    :catch_0
    move-exception v0

    .line 777
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be instance of HeartrateActivity"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private stressLightningAlphaAnimation(J)V
    .locals 3
    .param p1, "startOffset"    # J

    .prologue
    .line 1743
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1744
    .local v0, "alphaAnimation":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 1745
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/AlphaAnimation;->setStartOffset(J)V

    .line 1746
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 1747
    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$18;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$18;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1763
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLeftLightning:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1764
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigRightLightning:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1765
    return-void
.end method

.method private stressLightningLeftAnitmaion()V
    .locals 3

    .prologue
    .line 1768
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningLeftAnimator:Landroid/animation/ValueAnimator;

    .line 1769
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningLeftAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1770
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningLeftAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x48e

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 1771
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningLeftAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$19;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$19;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1784
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningLeftAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$20;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$20;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1804
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningLeftAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1805
    return-void

    .line 1768
    :array_0
    .array-data 4
        0x0
        0x2710
    .end array-data
.end method

.method private stressLightningRightAnitmaion()V
    .locals 3

    .prologue
    .line 1808
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningRightAnimator:Landroid/animation/ValueAnimator;

    .line 1809
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningRightAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1810
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningRightAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 1811
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningRightAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$21;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$21;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1824
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningRightAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$22;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$22;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1843
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningRightAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1844
    return-void

    .line 1808
    :array_0
    .array-data 4
        0x0
        0x2710
    .end array-data
.end method

.method private stressTranslateAnimation(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1848
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const v6, -0x42333333    # -0.1f

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 1851
    .local v0, "translateAnimation":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x14d

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1852
    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 1853
    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$23;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$23;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1867
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1868
    return-void
.end method


# virtual methods
.method public clearDataCollection()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1121
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1122
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1124
    :cond_0
    iput v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->sumRRI:I

    .line 1125
    iput v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->preSumRRI:I

    .line 1126
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1128
    return-void
.end method

.method public clearDeviceConnector()V
    .locals 1

    .prologue
    .line 597
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->stopSensor()V

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    if-eqz v0, :cond_0

    .line 599
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    .line 601
    :cond_0
    return-void
.end method

.method public dataCollectingUI(II)V
    .locals 11
    .param p1, "rateData"    # I
    .param p2, "interval"    # I

    .prologue
    const/16 v10, 0x7530

    const v9, 0x8000

    const/16 v8, 0x64

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1136
    sget-object v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string v3, "dataCollectingUI"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1137
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1139
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1140
    sget-object v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string v3, "StressAnalyzer initializeStressAnalyzer"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1141
    invoke-static {v10}, Lcom/sec/dmc/hsl/android/stressanalyzer/StressAnalyzer;->initializeStressAnalyzer(I)V

    .line 1143
    :cond_0
    invoke-static {p2}, Lcom/sec/dmc/hsl/android/stressanalyzer/StressAnalyzer;->addRRInterval(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->sumRRI:I

    .line 1144
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->preSumRRI:I

    if-nez v2, :cond_1

    .line 1145
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->sumRRI:I

    iput v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->preSumRRI:I

    .line 1147
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Measurement Dur: %d (%d)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->sumRRI:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->sumRRI:I

    int-to-double v4, v4

    const-wide v6, 0x40dd4c0000000000L    # 30000.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->sumRRI:I

    iget v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->preSumRRI:I

    if-le v2, v3, :cond_4

    .line 1150
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->sumRRI:I

    int-to-double v2, v2

    const-wide v4, 0x40dd4c0000000000L    # 30000.0

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    .line 1151
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    if-le v2, v8, :cond_2

    .line 1152
    iput v8, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    .line 1155
    :cond_2
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->sumRRI:I

    if-lt v2, v10, :cond_5

    .line 1156
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    const-string v3, "100%"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1157
    iput v8, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    .line 1196
    :cond_3
    :goto_0
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->sumRRI:I

    iput v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->preSumRRI:I

    .line 1198
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    if-eqz v2, :cond_4

    .line 1200
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->toggleFlag:Z

    if-nez v2, :cond_b

    .line 1202
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->updateProgressBar(I)V

    .line 1203
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->toggleFlag:Z

    if-nez v2, :cond_a

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->toggleFlag:Z

    .line 1210
    :goto_2
    iget v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    if-lt v0, v8, :cond_4

    .line 1212
    iput v8, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    .line 1213
    iget v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->updateProgressBar(I)V

    .line 1217
    :cond_4
    return-void

    .line 1160
    :cond_5
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    if-lez v2, :cond_6

    .line 1161
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->startCountDownTimer()V

    .line 1163
    :cond_6
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    const/16 v3, 0x5f

    if-ge v2, v3, :cond_9

    .line 1164
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    const/16 v3, 0x32

    if-gt v2, v3, :cond_7

    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    const/16 v3, 0x19

    if-le v2, v3, :cond_7

    .line 1165
    const v2, 0x7f090c33

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setTextForFirstMessageById(I)V

    .line 1166
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isTalkBackCount:Z

    if-eqz v2, :cond_7

    .line 1167
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    .line 1168
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isTalkBackCount:Z

    .line 1171
    :cond_7
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    const/16 v3, 0x50

    if-gt v2, v3, :cond_8

    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    const/16 v3, 0x32

    if-le v2, v3, :cond_8

    .line 1172
    const v2, 0x7f090c34

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setTextForFirstMessageById(I)V

    .line 1173
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isTalkBackCount:Z

    if-nez v2, :cond_8

    .line 1174
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isTalkBackCount:Z

    .line 1175
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    .line 1179
    :cond_8
    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    const/16 v3, 0x5f

    if-gt v2, v3, :cond_3

    iget v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    const/16 v3, 0x50

    if-le v2, v3, :cond_3

    .line 1180
    const v2, 0x7f090c35

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setTextForFirstMessageById(I)V

    .line 1181
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isTalkBackCount:Z

    if-eqz v2, :cond_3

    .line 1182
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    .line 1183
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isTalkBackCount:Z

    goto/16 :goto_0

    .line 1187
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    const v3, 0x7f090c32

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1188
    const v2, 0x7f090c36

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setTextForFirstMessageById(I)V

    .line 1189
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isTalkBackCount:Z

    if-nez v2, :cond_3

    .line 1190
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isTalkBackCount:Z

    .line 1191
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 1203
    goto/16 :goto_1

    .line 1207
    :cond_b
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->toggleFlag:Z

    if-nez v2, :cond_c

    :goto_3
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->toggleFlag:Z

    goto/16 :goto_2

    :cond_c
    move v0, v1

    goto :goto_3
.end method

.method public dataCollectionSet(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1231
    .local p1, "collection":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    .line 1232
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->bpmDatas:[I

    .line 1233
    const-wide/16 v0, 0x0

    .line 1234
    .local v0, "average":D
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 1235
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-double v3, v3

    add-double/2addr v0, v3

    .line 1236
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->bpmDatas:[I

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v4, v2

    .line 1234
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1238
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    int-to-double v3, v3

    div-double v3, v0, v3

    double-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->bpmAverage:I

    .line 1239
    sget-object v3, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->effectAudio:[I

    const/4 v4, 0x4

    aget v3, v3, v4

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->playSound(I)V

    .line 1240
    sget-object v3, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "size : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " avg : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->bpmAverage:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1241
    return-void
.end method

.method public deleteDailyData()V
    .locals 3

    .prologue
    .line 1444
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->deleteDailyDataByTime(J)Z

    .line 1445
    return-void
.end method

.method public firstAnimationClear()V
    .locals 2

    .prologue
    .line 816
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$5;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 824
    return-void
.end method

.method protected getCalendarActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 653
    const-class v0, Lcom/sec/android/app/shealth/stress/calendar/StressCalendarActivity;

    return-object v0
.end method

.method protected getColumnNameForTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 643
    const-string/jumbo v0, "sample_time"

    return-object v0
.end method

.method protected getContentURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 638
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getContentView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 266
    sget-object v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string v2, "getContentView"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    .line 269
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 270
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030236

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->initView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1

    .prologue
    .line 658
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method public getState()Lcom/sec/android/app/shealth/stress/state/StressState;
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    return-object v0
.end method

.method public getStateFinished()Lcom/sec/android/app/shealth/stress/state/StressState;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateFinished:Lcom/sec/android/app/shealth/stress/state/StressState;

    return-object v0
.end method

.method public getStateMeasureFailed()Lcom/sec/android/app/shealth/stress/state/StressState;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateMeasureFailed:Lcom/sec/android/app/shealth/stress/state/StressState;

    return-object v0
.end method

.method public getStateMeasureWarning()Lcom/sec/android/app/shealth/stress/state/StressState;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateMeasureWarning:Lcom/sec/android/app/shealth/stress/state/StressState;

    return-object v0
.end method

.method public getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateMeasuring:Lcom/sec/android/app/shealth/stress/state/StressState;

    return-object v0
.end method

.method public getStateReady()Lcom/sec/android/app/shealth/stress/state/StressState;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateReady:Lcom/sec/android/app/shealth/stress/state/StressState;

    return-object v0
.end method

.method public initStressState()V
    .locals 1

    .prologue
    .line 376
    new-instance v0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/state/StressStateReady;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateReady:Lcom/sec/android/app/shealth/stress/state/StressState;

    .line 377
    new-instance v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateMeasuring:Lcom/sec/android/app/shealth/stress/state/StressState;

    .line 378
    new-instance v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateMeasureWarning:Lcom/sec/android/app/shealth/stress/state/StressState;

    .line 379
    new-instance v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureFailed;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureFailed;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateMeasureFailed:Lcom/sec/android/app/shealth/stress/state/StressState;

    .line 380
    new-instance v0, Lcom/sec/android/app/shealth/stress/state/StressStateFinished;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/state/StressStateFinished;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateFinished:Lcom/sec/android/app/shealth/stress/state/StressState;

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateReady:Lcom/sec/android/app/shealth/stress/state/StressState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    .line 382
    return-void
.end method

.method public measureFinishAnimation()V
    .locals 2

    .prologue
    .line 898
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 944
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 245
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onAttach(Landroid/app/Activity;)V

    .line 246
    sget-object v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onAttach"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    check-cast p1, Lcom/sec/android/app/shealth/stress/StressActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    .line 249
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 250
    .local v0, "model":Ljava/lang/String;
    const-string v1, "SM-G850F"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G850S"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G850A"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G850L"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G850W"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 253
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImagesArray:[Ljava/lang/String;

    const-string v2, "hr_fail_alpha_1"

    aput-object v2, v1, v3

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImagesArray:[Ljava/lang/String;

    const-string v2, "hr_fail_alpha_2"

    aput-object v2, v1, v4

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImagesArray:[Ljava/lang/String;

    const-string v2, "hr_fail_alpha_3"

    aput-object v2, v1, v5

    .line 257
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImages:[I

    const v2, 0x7f0201ef

    aput v2, v1, v3

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImages:[I

    const v2, 0x7f0201f0

    aput v2, v1, v4

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImages:[I

    const v2, 0x7f0201f1

    aput v2, v1, v5

    .line 262
    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1913
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1914
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1915
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 1917
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsOnConfigChanged:Z

    .line 1919
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_1

    .line 1920
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->updateErrorDialogStringOnLocaleChange()V

    .line 1923
    :cond_1
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    instance-of v0, v0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;

    if-eqz v0, :cond_2

    .line 1924
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstReadyText:Landroid/widget/TextView;

    const v1, 0x7f090c05

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1925
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstReadyMessage:Landroid/widget/TextView;

    const v1, 0x7f090c22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1927
    :cond_2
    return-void
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;)V
    .locals 5
    .param p1, "hrm"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;

    .prologue
    .line 1481
    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->heartRate:I

    .line 1482
    .local v1, "rate":I
    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->interval:J

    long-to-int v0, v2

    .line 1484
    .local v0, "interval":I
    sget-object v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onDataReceived hrm.heartRate : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1485
    sget-object v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onDataReceived hrm.interval : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1487
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateNextState(II)V

    .line 1490
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 576
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->release()V

    .line 579
    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    if-eqz v0, :cond_1

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->stopCountDownTimer()V

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->stopMeasuring()V

    .line 587
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDiscardButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdGraph:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 590
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroy()V

    .line 593
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 540
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->initDialogAnimation()V

    .line 545
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceSupportCoverSDK:Z

    if-eqz v0, :cond_1

    .line 546
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 548
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->clearDataCollection()V

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    if-eqz v0, :cond_2

    .line 550
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->stopCountDownTimer()V

    .line 551
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->stopMeasuring()V

    .line 554
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    if-eqz v0, :cond_3

    .line 555
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/state/StressState;->terminateState()V

    .line 558
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->firstAnimationClear()V

    .line 559
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    .line 560
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measuringCircleAnimationStop(Z)V

    .line 562
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 564
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setVisibility(I)V

    .line 565
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->resetProgress()V

    .line 567
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    if-eqz v0, :cond_4

    .line 568
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->setSendedStartLogging(Z)V

    .line 571
    :cond_4
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onPause()V

    .line 572
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 492
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onResume()V

    .line 493
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->initAoudioFile()V

    .line 495
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->initSCover()V

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 504
    :cond_0
    iput v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    .line 506
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsOnConfigChanged:Z

    if-eqz v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdGraph:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 510
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->wasShowInformationDialog:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getCallFromHome()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "stress_warning_checked"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->callShowInformationDialog()V

    .line 512
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->wasShowInformationDialog:Z

    .line 532
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_3

    .line 533
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->startDialogAnimation()V

    .line 536
    :cond_3
    return-void

    .line 513
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getDataCount()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    instance-of v0, v0, Lcom/sec/android/app/shealth/stress/state/StressStateFinished;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    if-eqz v0, :cond_5

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;->setVisibility(I)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStartDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    const/16 v1, -0x3e9

    const/16 v2, -0x7d1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateNextState(II)V

    .line 519
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 521
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    instance-of v0, v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureFailed;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    if-eqz v0, :cond_6

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/state/StressState;->reStartStates()V

    goto :goto_0

    .line 523
    :cond_6
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsOnConfigChanged:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v0, :cond_2

    .line 524
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->showErrorDialog(I)V

    .line 525
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsOnConfigChanged:Z

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 648
    const-string v0, "Date"

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 649
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 276
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onStart()V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLow:Landroid/widget/TextView;

    const v1, 0x7f090c3c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHigh:Landroid/widget/TextView;

    const v1, 0x7f090c3d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStartButton:Landroid/widget/TextView;

    const v1, 0x7f090050

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDiscardButton:Landroid/widget/TextView;

    const v1, 0x7f090f4d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 283
    return-void
.end method

.method protected onSytemDateChanged()V
    .locals 6

    .prologue
    .line 624
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 625
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 626
    .local v0, "sytemChangedTime":J
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 627
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 628
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->updateBpmDataView(Z)V

    .line 630
    .end local v0    # "sytemChangedTime":J
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 1936
    return-void
.end method

.method public onTimeout()V
    .locals 3

    .prologue
    .line 1931
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    const/4 v1, -0x6

    const/16 v2, -0x7d1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateNextState(II)V

    .line 1932
    return-void
.end method

.method public restartCurrentState(I)V
    .locals 1
    .param p1, "animation"    # I

    .prologue
    .line 673
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/state/StressState;->reStartStates()V

    .line 674
    return-void
.end method

.method public setIsFromTimeout(Z)V
    .locals 0
    .param p1, "isFromTimeout"    # Z

    .prologue
    .line 206
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsFromTimeout:Z

    .line 207
    return-void
.end method

.method public setMeasuringEndUI()V
    .locals 12

    .prologue
    const/4 v8, 0x5

    const/4 v11, 0x1

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 1245
    iput v9, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    .line 1246
    sget-object v5, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "setMeasuringEndUI"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1247
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFinishedLogSent:Z

    if-nez v5, :cond_0

    .line 1248
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    const-string v6, "com.sec.android.app.shealth.stress"

    const-string v7, "ST02"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1249
    iput-boolean v11, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFinishedLogSent:Z

    .line 1251
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    if-eqz v5, :cond_1

    .line 1252
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->setSendedStartLogging(Z)V

    .line 1254
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->stopCountDownTimer()V

    .line 1256
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getUserAge()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->age:I

    .line 1258
    iget v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->age:I

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->calculateStressData(I)Lcom/sec/android/app/shealth/stress/data/StressStateType;

    move-result-object v0

    .line 1260
    .local v0, "stateType":Lcom/sec/android/app/shealth/stress/data/StressStateType;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isSamsungAccount()Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v5, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v6, "eng"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1261
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/data/StressStateType;->getScore()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    .line 1262
    .local v1, "stresScore":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v8, :cond_2

    .line 1263
    invoke-virtual {v1, v9, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1264
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressScore:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090f47

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1265
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressScore:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1268
    .end local v1    # "stresScore":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_6

    .line 1269
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    iget v6, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->bpmAverage:I

    invoke-virtual {v5, v0, v6}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->insertData(Lcom/sec/android/app/shealth/stress/data/StressStateType;I)V

    .line 1270
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->clearDataCollection()V

    .line 1280
    :cond_4
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/data/StressStateType;->getScore()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;->moveToPolygon(D)V

    .line 1281
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;->setVisibility(I)V

    .line 1284
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1286
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    if-eqz v5, :cond_5

    .line 1287
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/stress/StressActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/16 v6, 0x80

    invoke-virtual {v5, v6}, Landroid/view/Window;->clearFlags(I)V

    .line 1289
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1290
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1292
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1293
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconFinish:Landroid/widget/ImageView;

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1295
    invoke-direct {p0, v11}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measuringCircleAnimationStop(Z)V

    .line 1297
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1298
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    const v6, 0x7f090c06

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1299
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070253

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1301
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1305
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1306
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v5, v10}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setVisibility(I)V

    .line 1307
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->resetProgress()V

    .line 1310
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStartDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1311
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStart:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1312
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1313
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->refreshFragmentFocusables()V

    .line 1315
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    new-instance v6, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$9;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$9;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    const-wide/16 v7, 0xa

    invoke-virtual {v5, v6, v7, v8}, Landroid/widget/ImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1325
    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->updateBpmDataView(Z)V

    .line 1328
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningLeftAnimator:Landroid/animation/ValueAnimator;

    invoke-static {}, Landroid/animation/ValueAnimator;->clearAllAnimations()V

    .line 1329
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningRightAnimator:Landroid/animation/ValueAnimator;

    invoke-static {}, Landroid/animation/ValueAnimator;->clearAllAnimations()V

    .line 1330
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1331
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    const v6, 0x8000

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 1332
    return-void

    .line 1274
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    iget-object v6, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getLastTwoDataByTime(J)Ljava/util/ArrayList;

    move-result-object v2

    .line 1275
    .local v2, "stressLastDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/stress/data/StressData;>;"
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 1276
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/stress/data/StressData;->getScore()D

    move-result-wide v3

    .line 1277
    .local v3, "stressLastScore":D
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/shealth/stress/data/StressStateType;->setScore(D)V

    goto/16 :goto_0
.end method

.method public setMeasuringFailUI(I)V
    .locals 4
    .param p1, "errorType"    # I

    .prologue
    const/16 v3, 0x8

    .line 1052
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringFailUI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1053
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    if-eqz v0, :cond_0

    .line 1054
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1057
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1058
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->clearDataCollection()V

    .line 1061
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1062
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconFinish:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1063
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1064
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setVisibility(I)V

    .line 1065
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->resetProgress()V

    .line 1066
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v0, :cond_2

    .line 1067
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->showErrorDialog(I)V

    .line 1070
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1071
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningLeftAnimator:Landroid/animation/ValueAnimator;

    invoke-static {}, Landroid/animation/ValueAnimator;->clearAllAnimations()V

    .line 1072
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningRightAnimator:Landroid/animation/ValueAnimator;

    invoke-static {}, Landroid/animation/ValueAnimator;->clearAllAnimations()V

    .line 1073
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    .line 1074
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    if-eqz v0, :cond_3

    .line 1075
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setProgressColor(I)V

    .line 1077
    :cond_3
    return-void
.end method

.method public setMeasuringUI(I)V
    .locals 8
    .param p1, "isShowAnimation"    # I

    .prologue
    const v7, 0x106000c

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 989
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringUI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isTalkBackCount:Z

    .line 991
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->getSendedStartLogging()Z

    move-result v0

    if-nez v0, :cond_0

    .line 992
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->setSendedStartLogging(Z)V

    .line 993
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.app.shealth.stress"

    const-string v2, "ST01"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressScore:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 998
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    if-eqz v0, :cond_1

    .line 999
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1001
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1002
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;->setVisibility(I)V

    .line 1003
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1005
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsGreen:Z

    .line 1006
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->lightningImageChange()V

    .line 1007
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->scaleAnimationStarter(Z)V

    .line 1009
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    if-lez v0, :cond_2

    .line 1010
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setProgressColor(I)V

    .line 1011
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1012
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1015
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    if-nez v0, :cond_4

    .line 1016
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measuringCircleAnimationStart(Z)V

    .line 1025
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    const v1, 0x7f02062f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1027
    if-nez p1, :cond_5

    .line 1028
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstLayout:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->firstAnimation(Landroid/view/View;)V

    .line 1029
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1030
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1043
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020522

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1046
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStartDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1047
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1048
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 1049
    return-void

    .line 1018
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    .line 1020
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1021
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1032
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1033
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1034
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1035
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1037
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    const v1, 0x7f090c04

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1038
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1039
    const v0, 0x7f090c23

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setTextForFirstMessageById(I)V

    .line 1040
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method

.method public setMeasuringWarningUI(I)V
    .locals 6
    .param p1, "errorType"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1080
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringWarningUI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1081
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->stopCountDownTimer()V

    .line 1084
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsGreen:Z

    .line 1085
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->lightningImageChange()V

    .line 1086
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->scaleAnimationStarter(Z)V

    .line 1088
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    if-nez v0, :cond_0

    .line 1090
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measuringCircleAnimationStart(Z)V

    .line 1097
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    const v1, 0x7f090c04

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1098
    const v0, 0x7f090c24

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setTextForFirstMessageById(I)V

    .line 1099
    iget v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    if-eqz v0, :cond_1

    .line 1100
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070189

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setProgressColor(I)V

    .line 1101
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1102
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1110
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070252

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1111
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020524

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1112
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v1, 0x7f020634

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1113
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    const v1, 0x7f020632

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1114
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1115
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 1116
    return-void

    .line 1092
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1093
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1105
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method protected setNextAndPrevDates()V
    .locals 0

    .prologue
    .line 634
    return-void
.end method

.method public setState(Lcom/sec/android/app/shealth/stress/state/StressState;)V
    .locals 0
    .param p1, "nextState"    # Lcom/sec/android/app/shealth/stress/state/StressState;

    .prologue
    .line 389
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    .line 391
    return-void
.end method

.method public showErrorDialog(I)V
    .locals 7
    .param p1, "errorType"    # I

    .prologue
    const/4 v6, 0x0

    .line 1495
    sget-object v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "showErrorDialog : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1496
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsFromTimeout:Z

    if-nez v1, :cond_1

    .line 1497
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.app.shealth.stress"

    const-string v3, "ST05"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1501
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->effectAudio:[I

    const/4 v2, 0x3

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->playSound(I)V

    .line 1503
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v2, 0x7f020522

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1505
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measuringCircleAnimationStop(Z)V

    .line 1507
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    if-eqz v1, :cond_0

    .line 1508
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1510
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    if-nez v1, :cond_2

    .line 1588
    :goto_1
    return-void

    .line 1499
    :cond_1
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsFromTimeout:Z

    goto :goto_0

    .line 1532
    :cond_2
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1533
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1534
    const v1, 0x7f0907ae

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1535
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1536
    const v1, 0x7f030232

    new-instance v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1554
    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$14;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$14;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1562
    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1585
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1586
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->startDialogAnimation()V

    .line 1587
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/StressActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "stm_error_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public startDialogAnimation()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1616
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1617
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImages:[I

    aget v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1618
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1619
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImages:[I

    aget v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1620
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1621
    iput v6, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animateType:I

    .line 1622
    sput-boolean v5, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isDialogDissmised:Z

    .line 1623
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImages:[I

    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->errorImagesArray:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    .line 1625
    :cond_0
    return-void
.end method

.method public startSensor()V
    .locals 3

    .prologue
    .line 663
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/stress/StressSensorListener;Z)V

    .line 664
    return-void
.end method

.method public stopSensor()V
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    if-eqz v0, :cond_0

    .line 668
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->stopMeasuring()V

    .line 670
    :cond_0
    return-void
.end method

.method public terminateCurrentState()V
    .locals 1

    .prologue
    .line 1739
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/state/StressState;->terminateState()V

    .line 1740
    return-void
.end method

.method public updateBpmDataView(Z)V
    .locals 10
    .param p1, "isMeasuring"    # Z

    .prologue
    const/16 v6, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 677
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getLastTwoDataByTime(J)Ljava/util/ArrayList;

    move-result-object v2

    .line 679
    .local v2, "stressDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/stress/data/StressData;>;"
    if-eqz p1, :cond_2

    .line 681
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 682
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 721
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setNextAndPrevDates()V

    .line 722
    return-void

    .line 684
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 685
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 686
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateSmallBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummarySmallBar;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/data/StressData;->getScore()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/stress/custom/StressStateSummarySmallBar;->moveToPolygon(D)V

    .line 687
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdDate:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/data/StressData;->getSampleTime()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 688
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy/MM/dd"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 689
    .local v1, "format":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/data/StressData;->getSampleTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 690
    .local v0, "date":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdDate:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 691
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdTime:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/data/StressData;->getSampleTime()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 696
    .end local v0    # "date":Ljava/lang/String;
    .end local v1    # "format":Ljava/text/SimpleDateFormat;
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_3

    iget v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->bpmAverage:I

    if-nez v3, :cond_3

    .line 697
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 698
    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_4

    iget v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->bpmAverage:I

    if-lez v3, :cond_4

    .line 701
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 706
    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    .line 707
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 708
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 709
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateSmallBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummarySmallBar;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/data/StressData;->getScore()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/stress/custom/StressStateSummarySmallBar;->moveToPolygon(D)V

    .line 710
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdDate:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/data/StressData;->getSampleTime()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 711
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy/MM/dd"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 712
    .restart local v1    # "format":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/data/StressData;->getSampleTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 713
    .restart local v0    # "date":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdDate:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 714
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdTime:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/data/StressData;->getSampleTime()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 716
    .end local v0    # "date":Ljava/lang/String;
    .end local v1    # "format":Ljava/text/SimpleDateFormat;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public updateErrorDialogStringOnLocaleChange()V
    .locals 3

    .prologue
    .line 1602
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1603
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v0

    .line 1604
    .local v0, "content":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1605
    const v1, 0x7f0800b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090047

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1606
    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0907ae

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1607
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg1:Landroid/widget/TextView;

    const v2, 0x7f090c28

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1608
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg2:Landroid/widget/TextView;

    const v2, 0x7f090c27

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1609
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg3:Landroid/widget/TextView;

    const v2, 0x7f090c25

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1610
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg4:Landroid/widget/TextView;

    const v2, 0x7f090c2d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1613
    .end local v0    # "content":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public updateProgressBar(I)V
    .locals 3
    .param p1, "endPercent"    # I

    .prologue
    const/4 v1, 0x0

    .line 1221
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measuringCircleAnimationStop(Z)V

    .line 1222
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setVisibility(I)V

    .line 1223
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    if-eqz v0, :cond_0

    .line 1224
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressActivity:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setProgressColor(I)V

    .line 1226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHoloCircularProgressBar:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    int-to-float v1, p1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setProgressRange(F)V

    .line 1227
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1228
    return-void
.end method
