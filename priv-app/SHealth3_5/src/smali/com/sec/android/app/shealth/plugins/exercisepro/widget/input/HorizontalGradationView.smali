.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;
.super Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;
.source "HorizontalGradationView.java"


# static fields
.field private static final DISTANCE_TO_REDRAW_GRADATION_VIEW_HORIZONTAL:I = 0x3e8


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method private getViewCenterXPosition()I
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method


# virtual methods
.method protected drawAbnormalRangeElements(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const v10, 0x7f0a0107

    const/4 v8, 0x0

    const/4 v9, -0x1

    .line 60
    const/4 v6, 0x0

    .line 62
    .local v6, "normalRangeLeftPos":Ljava/lang/Integer;
    const/4 v7, 0x0

    .line 64
    .local v7, "normalRangeRightPos":Ljava/lang/Integer;
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mMaxNormalRangeIndex:I

    if-eq v0, v9, :cond_0

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getViewCenterXPosition()I

    move-result v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mGradationArrayList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mMaxNormalRangeIndex:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getOffsetMovement()I

    move-result v0

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 68
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_4

    move v1, v8

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int v2, v0, v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredHeight()I

    move-result v4

    move-object v0, p0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->drawAbnormalRangeBottomLine(IIIILandroid/graphics/Canvas;)V

    .line 82
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mMinNormalRangeIndex:I

    if-eq v0, v9, :cond_1

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getViewCenterXPosition()I

    move-result v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mGradationArrayList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mMinNormalRangeIndex:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getOffsetMovement()I

    move-result v0

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int v2, v0, v1

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredWidth()I

    move-result v1

    if-gt v0, v1, :cond_5

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredHeight()I

    move-result v4

    move-object v0, p0

    move v1, v8

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->drawAbnormalRangeBottomLine(IIIILandroid/graphics/Canvas;)V

    .line 100
    :cond_1
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getViewCenterXPosition()I

    move-result v1

    if-le v0, v1, :cond_3

    :cond_2
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getViewCenterXPosition()I

    move-result v1

    if-lt v0, v1, :cond_6

    .line 104
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mOnBackgroundChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnBackgroundChangedListener;

    const v1, 0x7f0201ed

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnBackgroundChangedListener;->onBackgroundChanged(I)V

    .line 112
    :goto_2
    return-void

    .line 68
    :cond_4
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/16 :goto_0

    .line 86
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredWidth()I

    move-result v3

    goto :goto_1

    .line 108
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mOnBackgroundChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnBackgroundChangedListener;

    const v1, 0x7f0201ec

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnBackgroundChangedListener;->onBackgroundChanged(I)V

    goto :goto_2
.end method

.method protected drawGradationBars(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;

    .line 156
    .local v9, "object":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
    invoke-virtual {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->isBig()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->isMiddle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0100

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 162
    .local v7, "heightOffset":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v0, v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v1, v7

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mTextHalfHeight:I

    sub-int v6, v0, v1

    .line 163
    .local v6, "Y":I
    invoke-virtual {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getValue()F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mMaxInputRange:I

    int-to-float v1, v1

    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getValue()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getViewCenterXPosition()I

    move-result v1

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getOffsetMovement()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    int-to-float v2, v6

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 181
    .end local v6    # "Y":I
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getViewCenterXPosition()I

    move-result v0

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getOffsetMovement()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v1, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredHeight()I

    move-result v0

    int-to-float v2, v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getViewCenterXPosition()I

    move-result v0

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getOffsetMovement()I

    move-result v3

    add-int/2addr v0, v3

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v0, v7

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 171
    .end local v7    # "heightOffset":I
    :cond_1
    invoke-virtual {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->isBig()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->isMiddle()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0101

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .restart local v7    # "heightOffset":I
    goto :goto_1

    .line 177
    .end local v7    # "heightOffset":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0102

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .restart local v7    # "heightOffset":I
    goto :goto_1

    .line 195
    .end local v7    # "heightOffset":I
    .end local v9    # "object":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
    :cond_3
    return-void
.end method

.method protected drawNormalBottomLine(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 128
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0107

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getMeasuredHeight()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 140
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mDrawableGreen:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ColorDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mDrawableGreen:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 144
    return-void
.end method

.method protected initGradationBarDistance()V
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mBetweenGradationBarsDistance:I

    .line 38
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mDistanceToRedrawGradationView:I

    .line 40
    return-void
.end method

.method protected putTouchCoords(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 211
    :cond_0
    return-void
.end method
