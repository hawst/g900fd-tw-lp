.class Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$GetContentAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AwardSetPhotoContentInitializationListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GetContentAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p2, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;>;"
    const v0, 0x7f030012

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 97
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v4, 0x7f080060

    .line 102
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 103
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_0

    .line 104
    const v2, 0x7f030012

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 105
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$GetContentAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;

    .line 106
    .local v1, "requestInfo":Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, v1, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;->labelResource:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v2, v1, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;->labelResource:Ljava/lang/String;

    invoke-virtual {p2, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 108
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 114
    :goto_0
    return-object p2

    .line 110
    .end local v1    # "requestInfo":Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;

    .line 111
    .restart local v1    # "requestInfo":Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, v1, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;->labelResource:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v2, v1, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;->labelResource:Ljava/lang/String;

    invoke-virtual {p2, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
