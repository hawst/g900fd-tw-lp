.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutInfoActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSelectedItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getOnSelectedItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSelectedItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

.field final synthetic val$dialogTag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1099
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->val$dialogTag:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSelected([Z)V
    .locals 28
    .param p1, "isChecked"    # [Z

    .prologue
    .line 1103
    const-string v3, "FACEBOOK"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->val$dialogTag:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1104
    const/4 v3, 0x0

    aget-boolean v3, p1, v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    aget-boolean v3, p1, v3

    if-nez v3, :cond_1

    .line 1161
    :cond_0
    :goto_0
    return-void

    .line 1108
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1109
    .local v9, "imageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v3, 0x0

    aget-boolean v3, p1, v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    aget-boolean v3, p1, v3

    if-eqz v3, :cond_2

    .line 1111
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->getImageList()Ljava/util/List;

    move-result-object v20

    .line 1112
    .local v20, "extraImageList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v23

    .line 1113
    .local v23, "imageCount":I
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_1
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_3

    .line 1114
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 1115
    .local v19, "data":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    new-instance v3, Ljava/io/File;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1113
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 1117
    .end local v19    # "data":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .end local v20    # "extraImageList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    .end local v22    # "i":I
    .end local v23    # "imageCount":I
    :cond_2
    const/4 v3, 0x0

    aget-boolean v3, p1, v3

    if-eqz v3, :cond_5

    .line 1128
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getActionBarTitle(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;)I

    move-result v5

    .line 1130
    .local v5, "title":I
    const/4 v3, 0x0

    aget-boolean v3, p1, v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_6

    .line 1131
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    const v6, 0x7f0807cb

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/LinearLayout;

    .line 1132
    .local v24, "lin_layout1":Landroid/widget/LinearLayout;
    invoke-virtual/range {v24 .. v24}, Landroid/widget/LinearLayout;->buildDrawingCache()V

    .line 1133
    invoke-virtual/range {v24 .. v24}, Landroid/widget/LinearLayout;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v25

    .line 1135
    .local v25, "lo1":Landroid/graphics/Bitmap;
    const/16 v18, 0x0

    .line 1136
    .local v18, "config":Landroid/graphics/Bitmap$Config;
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v27

    .line 1137
    .local v27, "w":I
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    .line 1138
    .local v21, "h":I
    if-nez v18, :cond_4

    .line 1139
    sget-object v18, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1141
    :cond_4
    sget-object v18, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 1142
    move/from16 v0, v27

    move/from16 v1, v21

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1144
    .local v4, "screenShot":Landroid/graphics/Bitmap;
    new-instance v26, Landroid/graphics/Canvas;

    move-object/from16 v0, v26

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1145
    .local v26, "newCanvas":Landroid/graphics/Canvas;
    const/4 v3, -0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1146
    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1148
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getTime()J

    move-result-wide v6

    const/4 v8, -0x1

    sget-object v10, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->FaceBook:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    invoke-static/range {v3 .. v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->createShareViewForSNS(Landroid/app/Activity;Landroid/graphics/Bitmap;IJILjava/util/List;Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;)V

    goto/16 :goto_0

    .line 1119
    .end local v4    # "screenShot":Landroid/graphics/Bitmap;
    .end local v5    # "title":I
    .end local v18    # "config":Landroid/graphics/Bitmap$Config;
    .end local v21    # "h":I
    .end local v24    # "lin_layout1":Landroid/widget/LinearLayout;
    .end local v25    # "lo1":Landroid/graphics/Bitmap;
    .end local v26    # "newCanvas":Landroid/graphics/Canvas;
    .end local v27    # "w":I
    :cond_5
    const/4 v3, 0x1

    aget-boolean v3, p1, v3

    if-eqz v3, :cond_3

    .line 1120
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->getImageList()Ljava/util/List;

    move-result-object v20

    .line 1121
    .restart local v20    # "extraImageList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v23

    .line 1122
    .restart local v23    # "imageCount":I
    const/16 v22, 0x0

    .restart local v22    # "i":I
    :goto_2
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_3

    .line 1123
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 1124
    .restart local v19    # "data":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    new-instance v3, Ljava/io/File;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1122
    add-int/lit8 v22, v22, 0x1

    goto :goto_2

    .line 1151
    .end local v19    # "data":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .end local v20    # "extraImageList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    .end local v22    # "i":I
    .end local v23    # "imageCount":I
    .restart local v5    # "title":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getTime()J

    move-result-wide v13

    const/4 v15, -0x1

    sget-object v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->FaceBook:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    move v12, v5

    move-object/from16 v16, v9

    invoke-static/range {v10 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->createShareViewForSNS(Landroid/app/Activity;Landroid/graphics/Bitmap;IJILjava/util/List;Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;)V

    goto/16 :goto_0

    .line 1155
    .end local v5    # "title":I
    .end local v9    # "imageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_7
    const-string v3, "TWITTER"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;->val$dialogTag:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1156
    const/16 v22, 0x0

    .restart local v22    # "i":I
    :goto_3
    move-object/from16 v0, p1

    array-length v3, v0

    move/from16 v0, v22

    if-ge v0, v3, :cond_0

    .line 1157
    aget-boolean v3, p1, v22

    if-eqz v3, :cond_8

    .line 1156
    :cond_8
    add-int/lit8 v22, v22, 0x1

    goto :goto_3
.end method
