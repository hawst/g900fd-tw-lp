.class Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$2;
.super Ljava/lang/Object;
.source "UvLogDetailActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->showDeletePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$400(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->mUvData:Lcom/sec/android/app/shealth/uv/data/UvData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;)Lcom/sec/android/app/shealth/uv/data/UvData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/data/UvData;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->deleteRowById(Ljava/lang/String;)Z

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->setResult(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;->finish()V

    .line 265
    return-void
.end method
