.class Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;
.super Ljava/lang/Object;
.source "CompatibleDeviceConnectActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrapperEventListener"
.end annotation


# instance fields
.field public mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 0
    .param p2, "sensorDevice"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 340
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341
    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 342
    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 5
    .param p1, "error"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 347
    if-nez p1, :cond_0

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    const v2, 0x7f090791

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$600(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->dismissDialog()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$000(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)V

    .line 354
    :goto_0
    return-void

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    const v2, 0x7f090790

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$600(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Ljava/lang/String;)V

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->dismissDialog()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$000(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)V

    goto :goto_0
.end method

.method public onLeft(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 360
    const-string v0, "CompatibleDeviceConnectActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WrapperEventListener:onLeft status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 0
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .param p2, "response"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    .prologue
    .line 372
    return-void
.end method

.method public onStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 366
    const-string v0, "CompatibleDeviceConnectActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WrapperEventListener:onStateChanged status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    return-void
.end method
