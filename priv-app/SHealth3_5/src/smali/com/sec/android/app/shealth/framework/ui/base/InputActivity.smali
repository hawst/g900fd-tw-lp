.class public abstract Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$5;,
        Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$TimeChangeReceiver;,
        Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$DiscardDialogButtonController;
    }
.end annotation


# static fields
.field protected static final DISCARD_CHANGES:Ljava/lang/String; = "DISCARD_CHANGES"

.field private static final MAX_LINE:I = 0x3


# instance fields
.field private date:Ljava/util/Date;

.field private dateFormatter:Ljava/text/SimpleDateFormat;

.field private dateTimeFormatter:Ljava/text/SimpleDateFormat;

.field private isSystemTimeChanged:Z

.field private mCal:Ljava/util/Calendar;

.field protected mContainer:Landroid/widget/FrameLayout;

.field protected mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field protected mMemoHeader:Landroid/widget/TextView;

.field protected mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

.field protected mMode:Ljava/lang/String;

.field protected mSelectedDate:Ljava/lang/String;

.field protected mSelectedTime:Ljava/lang/String;

.field private timeChangeReceiver:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$TimeChangeReceiver;

.field private timeFormatter:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mCal:Ljava/util/Calendar;

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->date:Ljava/util/Date;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->dateTimeFormatter:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->dateFormatter:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->timeFormatter:Ljava/text/SimpleDateFormat;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->isSystemTimeChanged:Z

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDialogControllerMap:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->saveInputChanges()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->hideParentKeyboard(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)Ljava/util/Calendar;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mCal:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)Ljava/text/SimpleDateFormat;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->dateFormatter:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->setSelectedDateTime()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)Ljava/text/SimpleDateFormat;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->timeFormatter:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->isSystemTimeChanged:Z

    return p1
.end method

.method private hideParentKeyboard(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method private saveInputChanges()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->dateFormatter:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getMeasureTimeInMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mSelectedDate:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->timeFormatter:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getMeasureTimeInMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mSelectedTime:Ljava/lang/String;

    const-string v0, "Add"

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onSaveButtonSelect(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Edit"

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onSaveButtonSelect(Z)V

    goto :goto_0
.end method

.method private setSelectedDateTime()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->dateTimeFormatter:Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mSelectedDate:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mSelectedTime:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->date:Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mCal:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->date:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mCal:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->setMeasureTime(J)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public closeScreen()V
    .locals 1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->isInputChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->saveInputChanges()V

    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->closeScreen()V

    return-void
.end method

.method protected customizeActionBar()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->cancel:I

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setISOKCancelType(Z)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->save:I

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setISOKCancelType(Z)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    new-array v2, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v1

    aput-object v1, v2, v5

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    return-void
.end method

.method protected abstract getContentView()Landroid/view/View;
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method protected getMemoContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNegativeButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPositiveButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    .locals 2

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;-><init>()V

    const-string v1, "date_picker_dialog"

    invoke-virtual {v1, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->set:I

    iput v1, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->textResId:I

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V

    iput-object v1, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->btnClickListener:Landroid/view/View$OnClickListener;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v1, "time_picker_dialog"

    invoke-virtual {v1, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->set:I

    iput v1, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->textResId:I

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$4;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V

    iput-object v1, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->btnClickListener:Landroid/view/View$OnClickListener;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initDateTimeViews()V
    .locals 2

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->date_time_buttons:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    return-void
.end method

.method protected abstract isInputChanged()Z
.end method

.method protected makeDiscardDialog(II)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 2

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->isInputChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->saveInputChanges()V

    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->input_activity:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->setContentView(I)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->memo_view:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->memo_title:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mMemoHeader:Landroid/widget/TextView;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->container:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->getContentView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->btn_time:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->btn_date:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mSelectedTime:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mSelectedDate:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mMemoHeader:Landroid/widget/TextView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->notes:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mMemoHeader:Landroid/widget/TextView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->notes:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_header:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->initDateTimeViews()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->setSelectedDateTime()V

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$TimeChangeReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$TimeChangeReceiver;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->timeChangeReceiver:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$TimeChangeReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIME_SET"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->timeChangeReceiver:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$TimeChangeReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->max_characters_alert:I

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;-><init>(Landroid/content/Context;I)V

    const/16 v1, 0x32

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->addLimit(ILandroid/widget/EditText;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->refreshFocusables()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->timeChangeReceiver:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$TimeChangeReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->isSystemTimeChanged:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->setMeasureTime(J)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->isSystemTimeChanged:Z

    :cond_0
    return-void
.end method

.method protected abstract onSaveButtonSelect(Z)V
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected showDiscardDialog()V
    .locals 3

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->discard_changes:I

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->discard_changes_q:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->makeDiscardDialog(II)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "DISCARD_CHANGES"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public showMemoTextCount()V
    .locals 0

    return-void
.end method
