.class public Lcom/sec/android/app/shealth/framework/repository/MigrationService;
.super Landroid/app/IntentService;
.source "MigrationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;
    }
.end annotation


# static fields
.field public static final KEY_SECURE:Ljava/lang/String; = "isSecure"

.field private static final NITIFICATION_ID:I = 0x270f

.field public static final PROFILE_DATA_MIGRATION_FAILED:Ljava/lang/String; = "profile_data_migration_failed"

.field public static final PROFILE_DATA_MIGRATION_SUCCESSFUL:Ljava/lang/String; = "profile_data_migration_successful"

.field private static final PROGRESS:Ljava/lang/String; = "progress"

.field private static final RESTORE_SHARED_PREF:Ljava/lang/String; = "shealth_restore_status"

.field private static final RESTORE_STATUS_KEY:Ljava/lang/String; = "isRestored"

.field public static final SHEALTH_RESTORE_END_BROADCAST:Ljava/lang/String; = "com.sec.android.app.shealth.RESTORE_END"

.field public static final SHEALTH_RESTORE_PROGRESS_BROADCAST:Ljava/lang/String; = "com.sec.android.app.shealth.RESTORE_PROGRESS"

.field public static final SHEALTH_RESTORE_START_BROADCAST:Ljava/lang/String; = "com.sec.android.app.shealth.RESTORE_START"

.field private static final TAG:Ljava/lang/String;

.field public static volatile isRestoreInProgress:Z


# instance fields
.field private HPASSWD:[B

.field private final NEW_VERSION:I

.field upgradeStage:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->TAG:Ljava/lang/String;

    .line 45
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->isRestoreInProgress:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 58
    const-string v0, "RestoreService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 29
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->NEW_VERSION:I

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->HPASSWD:[B

    .line 59
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->TAG:Ljava/lang/String;

    const-string v1, "RestoreService"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    return-void
.end method

.method public static getHPasswd()[B
    .locals 3

    .prologue
    .line 188
    new-instance v0, Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/common/AESEncryption;-><init>()V

    .line 189
    .local v0, "mAes":Lcom/sec/android/service/health/cp/common/AESEncryption;
    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/common/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, "passwd":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    return-object v2
.end method

.method public static isAlreadyRestored(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 203
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 204
    .local v0, "extras":Landroid/os/Bundle;
    const-string v3, "key"

    const-string v4, "SHEALTH_2_5_EXISTS_KIES"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string/jumbo v3, "value"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v5, "CONFIG_OPTION_GET"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 208
    .local v2, "retVal":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 209
    .local v1, "isIncluded":Z
    if-eqz v2, :cond_0

    .line 210
    const-string/jumbo v3, "value"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 213
    :cond_0
    return v1
.end method

.method public static isRestoreInProgress()Z
    .locals 1

    .prologue
    .line 49
    sget-boolean v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->isRestoreInProgress:Z

    return v0
.end method

.method private sendCompletedBraodcast(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "isKiesRestore"    # Ljava/lang/Boolean;

    .prologue
    .line 118
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 119
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.RESTORE_END"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 120
    .local v0, "endMsg":Landroid/content/Intent;
    const-string v1, "IS_KIES_RESTORE_MODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 124
    .end local v0    # "endMsg":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private sendProgressBroadcast(I)V
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 112
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.RESTORE_PROGRESS_PERCENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 113
    .local v0, "progressIntent":Landroid/content/Intent;
    const-string/jumbo v1, "progress_percent"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 115
    return-void
.end method

.method private updateRestoreStatus(Z)V
    .locals 4
    .param p1, "status"    # Z

    .prologue
    .line 196
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " updateRestoreSharedPrefStatus "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "shealth_restore_status"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 198
    .local v0, "restoreSharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "isRestored"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 199
    return-void
.end method

.method private upgradeDatabase(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 148
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "upgradeDatabase"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;-><init>(Landroid/content/Context;)V

    .line 151
    .local v2, "restoreHelper":Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;
    const-string v3, "isSecure"

    invoke-virtual {p1, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 152
    .local v1, "isSecure":Z
    const/4 v0, 0x0

    .line 153
    .local v0, "db":Lsamsung/database/sqlite/SecSQLiteDatabase;
    if-nez v1, :cond_0

    .line 155
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;->getWritableDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    .line 167
    :goto_0
    new-instance v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "IS_KIES_RESTORE_MODE"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v3, v0, v4, v5}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->doMigration(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;Z)V

    .line 168
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "upgradeDatabase end"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    return-void

    .line 159
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->HPASSWD:[B

    if-nez v3, :cond_1

    .line 161
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getHPasswd()[B

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->HPASSWD:[B

    .line 163
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->HPASSWD:[B

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;->getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    goto :goto_0
.end method

.method private upgradeFileManager()V
    .locals 3

    .prologue
    .line 181
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "upgradeFileManager"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/file/FileManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/file/FileManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/repository/file/FileManager;->initialize(Landroid/content/Context;)V

    .line 183
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/file/FileManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/file/FileManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/repository/file/FileManager;->onUpgrade(II)Z

    .line 184
    return-void
.end method

.method private upgradeSharedPreference(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "isKiesRestore"    # Ljava/lang/Boolean;

    .prologue
    .line 173
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "upgradeSharedPreference"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->initialize(Landroid/content/Context;)V

    .line 175
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->setUpgradeMode(Z)V

    .line 176
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->onUpgrade(II)Z

    .line 177
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 65
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "onHandleIntent"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const-string v4, "IS_KIES_RESTORE_MODE"

    invoke-virtual {p1, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 67
    .local v1, "isKiesRestore":Ljava/lang/Boolean;
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "is Kies restore : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.shealth.RESTORE_PROGRESS"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 72
    .local v2, "progressIntent":Landroid/content/Intent;
    :try_start_0
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.shealth.RESTORE_START"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    .local v3, "startMsg":Landroid/content/Intent;
    const-string v4, "IS_KIES_RESTORE_MODE"

    const-string v5, "IS_KIES_RESTORE_MODE"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 75
    const/4 v4, 0x1

    sput-boolean v4, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->isRestoreInProgress:Z

    .line 77
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->PREFERENCES:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    iput-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->upgradeStage:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    .line 78
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->upgradeSharedPreference(Ljava/lang/Boolean;)V

    .line 79
    const-string/jumbo v4, "progress"

    const-string/jumbo v5, "profile_data_migration_successful"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 81
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->FILES:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    iput-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->upgradeStage:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->upgradeFileManager()V

    .line 83
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->DB:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    iput-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->upgradeStage:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->upgradeDatabase(Landroid/content/Intent;)V

    .line 86
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->updateRestoreStatus(Z)V

    .line 87
    const/4 v4, 0x0

    sput-boolean v4, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->isRestoreInProgress:Z

    .line 88
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->sendCompletedBraodcast(Ljava/lang/Boolean;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    .end local v3    # "startMsg":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " Exception occures while restoring "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    sput-boolean v7, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->isRestoreInProgress:Z

    .line 94
    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->upgradeStage:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->PREFERENCES:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    if-ne v4, v5, :cond_1

    .line 96
    const-string/jumbo v4, "progress"

    const-string/jumbo v5, "profile_data_migration_failed"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 99
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->upgradeStage:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->DB:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    if-ne v4, v5, :cond_0

    .line 101
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->TAG:Ljava/lang/String;

    const-string v5, "2.5 db is not correct"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/16 v4, 0x64

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->sendProgressBroadcast(I)V

    .line 103
    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->updateRestoreStatus(Z)V

    .line 104
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->sendCompletedBraodcast(Ljava/lang/Boolean;)V

    goto :goto_0
.end method
