.class public Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ExerciseProWorkoutInfoActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISelectedItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$SnsType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final TEMP_IMAGE_PREFIX:Ljava/lang/String; = "temp_"

.field private static final TEMP_IMAGE_TIME_FORMAT:Ljava/lang/String; = "HH_mm_ss_SSS"


# instance fields
.field private IMAGE_CAPTURE_DATA:Ljava/lang/String;

.field private IMAGE_CAPTURE_URI:Ljava/lang/String;

.field private SPACE_FOR_UNIT:Ljava/lang/String;

.field private aMapDetailMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

.field private btnCamera:Landroid/widget/ImageButton;

.field private btnGallery:Landroid/widget/ImageButton;

.field private dataType:I

.field private detailMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

.field private final facebookTag:Ljava/lang/String;

.field public intent:Landroid/content/Intent;

.field private isNeedInit:Z

.field mBtnClickListener:Landroid/view/View$OnClickListener;

.field private mBtnFacebook:Landroid/widget/ImageView;

.field private mBtnTwitter:Landroid/widget/ImageView;

.field private mCalorieView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

.field private mCalories:F

.field private mComment:Ljava/lang/String;

.field private mContent:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mDistance:F

.field private mDistanceView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

.field private mDummyFocus:Landroid/widget/EditText;

.field private mDuration:J

.field private mDurationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

.field private mEditMode:Z

.field private mEndMode:Z

.field private mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

.field private mExerciseInfoId:J

.field private mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

.field private mExerciseRowId:J

.field private mFbRequestCount:I

.field private mFbResultCount:I

.field private mImageCaptureUri:Landroid/net/Uri;

.field private mLastTakePhoto:Ljava/lang/String;

.field private mMapFrame:Landroid/widget/FrameLayout;

.field private mNotesHeader:Landroid/widget/TextView;

.field private mPicturesHeader:Landroid/widget/TextView;

.field private mPlaceMode:I

.field private mScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/NoInterceptScrollViewWithMapView;

.field private mShareResultLayout:Landroid/widget/LinearLayout;

.field private mSnsType:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$SnsType;

.field private mSync_time:J

.field private mTimeAndDate:J

.field private mTimeAndDateView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

.field mapClickListener:Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;

.field private memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

.field private original_file:Ljava/io/File;

.field private photo:Landroid/graphics/Bitmap;

.field private photoContainer:Landroid/widget/FrameLayout;

.field private final twitterTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 122
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    .line 124
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z

    .line 139
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->isNeedInit:Z

    .line 162
    const-string v0, "FACEBOOK"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->facebookTag:Ljava/lang/String;

    .line 163
    const-string v0, "TWITTER"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->twitterTag:Ljava/lang/String;

    .line 165
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$SnsType;->FaceBook:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$SnsType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mSnsType:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$SnsType;

    .line 168
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mLastTakePhoto:Ljava/lang/String;

    .line 169
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->SPACE_FOR_UNIT:Ljava/lang/String;

    .line 171
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mPicturesHeader:Landroid/widget/TextView;

    .line 172
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mNotesHeader:Landroid/widget/TextView;

    .line 177
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 179
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->photo:Landroid/graphics/Bitmap;

    .line 180
    const-string v0, "imageCaptureUri"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    .line 181
    const-string/jumbo v0, "photo_data"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    .line 294
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mapClickListener:Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;

    .line 353
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    .line 1167
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->startMyWorkoutMapActivity()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    return-wide v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->updatePhotoGrid()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;
    .param p1, "x1"    # I

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->startExerciseProPhotoGallery(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    return-object v0
.end method

.method static synthetic access$1400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;Ljava/lang/String;JZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J
    .param p4, "x3"    # Z

    .prologue
    .line 113
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->saveInfoAndUpdateViewGrid(Ljava/lang/String;JZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->saveExerciseData()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->startMoreDetailActivity()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->checkChangesAndExit()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    .prologue
    .line 113
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->dataType:I

    return v0
.end method

.method private checkChangesAndExit()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 810
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    if-eqz v1, :cond_1

    .line 811
    const-string v0, ""

    .line 813
    .local v0, "comment":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->getText()Ljava/lang/String;

    move-result-object v0

    .line 815
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getComment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->areStringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 816
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->showResetPopup()V

    .line 825
    .end local v0    # "comment":Ljava/lang/String;
    :goto_0
    return-void

    .line 818
    .restart local v0    # "comment":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->setResult(I)V

    .line 819
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    goto :goto_0

    .line 822
    .end local v0    # "comment":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->setResult(I)V

    .line 823
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    goto :goto_0
.end method

.method private createSaveCropFile()Landroid/net/Uri;
    .locals 9

    .prologue
    .line 1263
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1264
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 1265
    .local v3, "time":J
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v7, "HH_mm_ss_SSS"

    invoke-direct {v2, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1266
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "temp_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1267
    .local v6, "url":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/SHealth/Exercise/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1268
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1269
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 1270
    .local v5, "uri":Landroid/net/Uri;
    return-object v5
.end method

.method private deleteTempFile()V
    .locals 2

    .prologue
    .line 1249
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 1251
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1253
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1255
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1258
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private doTakePhotoAction()V
    .locals 3

    .prologue
    .line 1238
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->deleteTempFile()V

    .line 1239
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1240
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 1241
    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1242
    const-string/jumbo v1, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1243
    const/16 v1, 0x4e20

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1244
    return-void
.end method

.method private getDatas(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    .line 303
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->intent:Landroid/content/Intent;

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->intent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_workout_end_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->intent:Landroid/content/Intent;

    const-string/jumbo v1, "pick_result"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->intent:Landroid/content/Intent;

    const-string v1, "data_type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->dataType:I

    .line 307
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    if-nez v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->intent:Landroid/content/Intent;

    const-string v1, "edit"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z

    .line 320
    :goto_0
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 321
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->initLayout()V

    .line 322
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->updateExerciseDetails()V

    .line 326
    :goto_1
    return-void

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->intent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mPlaceMode:I

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 312
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    .line 313
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    const-string v1, "getDatas : savedInstanceState is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :goto_2
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDatas mComment = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 315
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    const-string v1, "getDatas : savedInstanceState is not null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 324
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    goto :goto_1
.end method

.method private initLayout()V
    .locals 6

    .prologue
    const v5, 0x7f09020b

    const/16 v4, 0x8

    .line 218
    const v0, 0x7f0803a8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/NoInterceptScrollViewWithMapView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/NoInterceptScrollViewWithMapView;

    .line 219
    const v0, 0x7f0807ca

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mMapFrame:Landroid/widget/FrameLayout;

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/NoInterceptScrollViewWithMapView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mMapFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/NoInterceptScrollViewWithMapView;->setMapContainer(Landroid/view/View;)V

    .line 221
    const v0, 0x7f0807cc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDateView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDateView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a50

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setTitle(Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDateView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    const v1, 0x7f020582

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setIconSrc(I)V

    .line 225
    const v0, 0x7f0807cd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDurationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDurationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a1b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setTitle(Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDurationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    const v1, 0x7f020404

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setIconSrc(I)V

    .line 229
    const v0, 0x7f0807ce

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDistanceView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDistanceView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090132

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setTitle(Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDistanceView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    const v1, 0x7f020403

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setIconSrc(I)V

    .line 233
    const v0, 0x7f0807cf

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalorieView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalorieView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a19

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setTitle(Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalorieView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    const v1, 0x7f020402

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setIconSrc(I)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalorieView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setDividerVisible(Z)V

    .line 238
    const v0, 0x7f0807d1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mShareResultLayout:Landroid/widget/LinearLayout;

    .line 239
    const v0, 0x7f0807d2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mBtnFacebook:Landroid/widget/ImageView;

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mBtnFacebook:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    const v0, 0x7f0807d3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mBtnTwitter:Landroid/widget/ImageView;

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mBtnTwitter:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    const v0, 0x7f08073d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->setEditMode(Z)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->setText(Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    const v1, 0x7f0803e6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mNotesHeader:Landroid/widget/TextView;

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mNotesHeader:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mMapFrame:Landroid/widget/FrameLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090a36

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09109d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 260
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;

    const v1, 0x7f0900b2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;-><init>(Landroid/content/Context;I)V

    const/16 v1, 0x32

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->addLimit(ILandroid/widget/EditText;)V

    .line 262
    const v0, 0x7f080632

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->photoContainer:Landroid/widget/FrameLayout;

    .line 263
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mShareResultLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mShareResultLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 267
    const v0, 0x7f0807c8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDummyFocus:Landroid/widget/EditText;

    .line 268
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->updatePhotoGrid()V

    .line 269
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->initPhotoController()V

    .line 270
    const v0, 0x7f0807d0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 271
    return-void
.end method

.method private initMapFragment()V
    .locals 7

    .prologue
    const v6, 0x7f0807ca

    const/4 v5, 0x1

    .line 274
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v1, :cond_0

    .line 275
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->intent:Landroid/content/Intent;

    invoke-direct {v1, v5, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;-><init>(ZJLandroid/content/Intent;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->aMapDetailMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->aMapDetailMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setOnMapClickListener(Lcom/amap/api/maps2d/AMap$OnMapClickListener;)V

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 283
    .local v0, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->aMapDetailMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    const-string v2, "exercise_pro_map_layout"

    invoke-virtual {v0, v6, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 284
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 292
    :goto_0
    return-void

    .line 286
    .end local v0    # "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->intent:Landroid/content/Intent;

    invoke-direct {v1, v5, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;-><init>(ZJLandroid/content/Intent;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->detailMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->detailMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mapClickListener:Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setOnMapClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;)V

    .line 288
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 289
    .restart local v0    # "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->detailMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    const-string v2, "exercise_pro_map_layout"

    invoke-virtual {v0, v6, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 290
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method private initPhotoController()V
    .locals 3

    .prologue
    const v2, 0x7f080737

    .line 900
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    if-eqz v0, :cond_1

    .line 901
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 902
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 904
    :cond_1
    const v0, 0x7f0806f1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->btnCamera:Landroid/widget/ImageButton;

    .line 905
    const v0, 0x7f0806f3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->btnGallery:Landroid/widget/ImageButton;

    .line 906
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->btnCamera:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$11;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 915
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->btnGallery:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$12;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 924
    return-void
.end method

.method private isFileSupported(Ljava/lang/String;)Z
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 769
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/Constants;->SUPPORTED_FILE_PREFIXES:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v6, v0, v1

    .line 770
    .local v6, "prefix":Ljava/lang/String;
    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 773
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 774
    .local v5, "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v8, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 775
    invoke-static {p1, v5}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 777
    iget v2, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 778
    .local v2, "imageHeight":I
    iget v3, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 779
    .local v3, "imageWidth":I
    if-lez v2, :cond_0

    if-lez v3, :cond_0

    .line 781
    iput-boolean v9, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 782
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v10

    iput v10, v5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 783
    invoke-static {p1, v5}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 784
    .local v7, "resultantBitmap":Landroid/graphics/Bitmap;
    if-eqz v7, :cond_0

    .line 786
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 792
    .end local v2    # "imageHeight":I
    .end local v3    # "imageWidth":I
    .end local v5    # "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v6    # "prefix":Ljava/lang/String;
    .end local v7    # "resultantBitmap":Landroid/graphics/Bitmap;
    :goto_1
    return v8

    .line 769
    .restart local v6    # "prefix":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v6    # "prefix":Ljava/lang/String;
    :cond_1
    move v8, v9

    .line 792
    goto :goto_1
.end method

.method private saveExerciseData()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 539
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->getText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    .line 540
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 541
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    .line 543
    :cond_0
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    invoke-static {p0, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->updateRealtimeItem(Landroid/content/Context;JLjava/lang/String;)J

    move-result-wide v0

    .line 544
    .local v0, "result":J
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mSync_time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mSync_time:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mDuration = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDuration:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    cmp-long v2, v0, v6

    if-ltz v2, :cond_1

    .line 546
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealtimeExerciseId(J)V

    .line 548
    const v2, 0x7f090835

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 550
    :cond_1
    return-void
.end method

.method private saveInfoAndUpdateViewGrid(Ljava/lang/String;JZ)V
    .locals 1
    .param p1, "picturePath"    # Ljava/lang/String;
    .param p2, "exerciseId"    # J
    .param p4, "editMode"    # Z

    .prologue
    .line 760
    if-eqz p1, :cond_0

    .line 761
    new-instance v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;-><init>(Ljava/lang/String;)V

    .line 762
    .local v0, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    invoke-virtual {v0, p2, p3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setExerciseId(J)V

    .line 763
    invoke-static {p0, v0, p4}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->insertTempitem(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;Z)J

    .line 764
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->updatePhotoGrid()V

    .line 766
    .end local v0    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_0
    return-void
.end method

.method private showDeleteDialog()V
    .locals 3

    .prologue
    .line 430
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    const v2, 0x7f090035

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v1, 0x7f090ac2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "delete_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 444
    return-void
.end method

.method private showResetPopup()V
    .locals 4

    .prologue
    .line 829
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 830
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f090081

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090082

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 839
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "discard_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 840
    return-void
.end method

.method private startExerciseProPhotoGallery(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->startExerciseProPhotoGallery(Landroid/content/Context;Ljava/util/List;I)V

    .line 1004
    return-void
.end method

.method private startMoreDetailActivity()V
    .locals 4

    .prologue
    .line 609
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->getText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    .line 610
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 611
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    .line 612
    :cond_0
    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->updateRealtimeItem(Landroid/content/Context;JLjava/lang/String;)J

    .line 613
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 614
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "pick_result"

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 615
    const-string v1, "edit"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 616
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 617
    const/16 v1, 0x7e

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 618
    return-void
.end method

.method private startMyWorkoutMapActivity()V
    .locals 4

    .prologue
    .line 996
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMyWorkoutMapActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 997
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "data_type"

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->dataType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 998
    const-string/jumbo v1, "pick_result"

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 999
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 1000
    return-void
.end method

.method private updateExerciseDetails()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v8, 0x1

    const v10, 0x7f08007e

    const/4 v9, 0x0

    .line 553
    iget-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    invoke-static {p0, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->getExerciseData(Landroid/content/Context;J)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    .line 554
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    if-nez v5, :cond_1

    .line 606
    :cond_0
    :goto_0
    return-void

    .line 556
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getExerciseId()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseInfoId:J

    .line 557
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getTime()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDate:J

    .line 558
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getmillisecond()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDuration:J

    .line 559
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getDistance()F

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDistance:F

    .line 560
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getCalories()F

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalories:F

    .line 561
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getComment()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    .line 563
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v1, v5, Landroid/util/DisplayMetrics;->density:F

    .line 564
    .local v1, "density":F
    const/high16 v5, 0x42340000    # 45.0f

    mul-float/2addr v5, v1

    float-to-int v4, v5

    .line 566
    .local v4, "marginInPixel":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDateView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 567
    .local v3, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 569
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDateView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->requestLayout()V

    .line 570
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDateView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDate:J

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormat(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setDateText(Ljava/lang/String;)V

    .line 571
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDateView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDate:J

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setValueText(Ljava/lang/String;)V

    .line 572
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDateView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setVisibility(I)V

    .line 573
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mTimeAndDateView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setDateVisible(Z)V

    .line 575
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDurationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .end local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 576
    .restart local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 577
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDurationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->requestLayout()V

    .line 578
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDurationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDuration:J

    invoke-static {v6, v7, p0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SecToString(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setValueText(Ljava/lang/String;)V

    .line 579
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDurationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->valueText:Landroid/widget/TextView;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDuration:J

    invoke-static {v6, v7, p0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SecToContentDescription(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 580
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDurationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setVisibility(I)V

    .line 582
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDistance:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-static {v11, v6, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->SPACE_FOR_UNIT:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDistance:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-static {v11, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v6

    invoke-static {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 584
    .local v2, "distanceValue":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDistanceView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .end local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 585
    .restart local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 586
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDistanceView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->requestLayout()V

    .line 587
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDistanceView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setValueText(Ljava/lang/String;)V

    .line 588
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDistanceView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const v8, 0x7f090132

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 590
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDistanceView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setVisibility(I)V

    .line 592
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalories:F

    float-to-int v6, v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->SPACE_FOR_UNIT:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x4

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalories:F

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v6

    invoke-static {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 594
    .local v0, "caloriesValue":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalorieView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .end local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 595
    .restart local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 596
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalorieView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->requestLayout()V

    .line 597
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalorieView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setValueText(Ljava/lang/String;)V

    .line 598
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalorieView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const v8, 0x7f090954

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 600
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalorieView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setVisibility(I)V

    .line 601
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mCalorieView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setDividerVisible(Z)V

    .line 603
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 604
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->setText(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private updatePhotoGrid()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 854
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    if-nez v0, :cond_2

    .line 855
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    if-eqz v0, :cond_1

    .line 856
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;-><init>(Landroid/content/Context;JZZ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    .line 861
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$10;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->setExerciseGridItemListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;)V

    .line 885
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->photoContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 886
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->photoContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 889
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->getPhotoGridView()Landroid/view/View;

    move-result-object v6

    .line 890
    .local v6, "content":Landroid/view/View;
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->viewSize()I

    move-result v0

    if-lez v0, :cond_3

    .line 891
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->photoContainer:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 892
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->photoContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 896
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->refreshFocusables()V

    .line 897
    return-void

    .line 858
    .end local v6    # "content":Landroid/view/View;
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;-><init>(Landroid/content/Context;JZZ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    goto :goto_0

    .line 882
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->updatePhotoData(J)V

    goto :goto_1

    .line 894
    .restart local v6    # "content":Landroid/view/View;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->photoContainer:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_2
.end method


# virtual methods
.method public closeScreen()V
    .locals 0

    .prologue
    .line 980
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->saveExerciseData()V

    .line 981
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->startMoreDetailActivity()V

    .line 982
    return-void
.end method

.method createSetSharePopup(I)V
    .locals 29
    .param p1, "id"    # I

    .prologue
    .line 1007
    const/16 v27, 0x0

    .line 1008
    .local v27, "popupTag":Ljava/lang/String;
    new-instance v18, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v3, 0x7f0901f4

    const/16 v6, 0xb

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 1010
    .local v18, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 1011
    .local v23, "listItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v3, 0x7f0807d2

    move/from16 v0, p1

    if-ne v0, v3, :cond_3

    .line 1013
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v26

    .line 1015
    .local v26, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.facebook.katana"

    const/16 v6, 0x80

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1023
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->getImageList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-gtz v3, :cond_2

    .line 1025
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getActionBarTitle(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;)I

    move-result v5

    .line 1027
    .local v5, "title":I
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1028
    .local v9, "imageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const v3, 0x7f0807cb

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/LinearLayout;

    .line 1029
    .local v22, "lin_layout1":Landroid/widget/LinearLayout;
    invoke-virtual/range {v22 .. v22}, Landroid/widget/LinearLayout;->buildDrawingCache()V

    .line 1030
    invoke-virtual/range {v22 .. v22}, Landroid/widget/LinearLayout;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v24

    .line 1032
    .local v24, "lo1":Landroid/graphics/Bitmap;
    const/16 v19, 0x0

    .line 1033
    .local v19, "config":Landroid/graphics/Bitmap$Config;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v28

    .line 1034
    .local v28, "w":I
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    .line 1035
    .local v21, "h":I
    if-nez v19, :cond_0

    .line 1036
    sget-object v19, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1038
    :cond_0
    sget-object v19, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 1039
    move/from16 v0, v28

    move/from16 v1, v21

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1041
    .local v4, "screenShot":Landroid/graphics/Bitmap;
    new-instance v25, Landroid/graphics/Canvas;

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1042
    .local v25, "newCanvas":Landroid/graphics/Canvas;
    const/4 v3, -0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1043
    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v3, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1045
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getTime()J

    move-result-wide v6

    const/4 v8, -0x1

    sget-object v10, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->FaceBook:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    move-object/from16 v3, p0

    invoke-static/range {v3 .. v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->createShareViewForSNS(Landroid/app/Activity;Landroid/graphics/Bitmap;IJILjava/util/List;Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;)V

    .line 1095
    .end local v4    # "screenShot":Landroid/graphics/Bitmap;
    .end local v5    # "title":I
    .end local v9    # "imageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v19    # "config":Landroid/graphics/Bitmap$Config;
    .end local v21    # "h":I
    .end local v22    # "lin_layout1":Landroid/widget/LinearLayout;
    .end local v24    # "lo1":Landroid/graphics/Bitmap;
    .end local v25    # "newCanvas":Landroid/graphics/Canvas;
    .end local v26    # "pm":Landroid/content/pm/PackageManager;
    .end local v28    # "w":I
    :cond_1
    :goto_0
    return-void

    .line 1016
    .restart local v26    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v20

    .line 1017
    .local v20, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual/range {v20 .. v20}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 1018
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    const-string/jumbo v7, "market://details?id=com.facebook.katana"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1050
    .end local v20    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    const-string v3, "Workout Summary"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1051
    const-string v3, "Photo"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1052
    const-string v27, "FACEBOOK"

    .line 1054
    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 1055
    const/4 v3, 0x2

    new-array v3, v3, [Z

    fill-array-data v3, :array_0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 1058
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v3, v6, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 1060
    .end local v26    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    const v3, 0x7f0807d3

    move/from16 v0, p1

    if-ne v0, v3, :cond_1

    .line 1062
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v26

    .line 1064
    .restart local v26    # "pm":Landroid/content/pm/PackageManager;
    :try_start_1
    const-string v3, "com.twitter.android"

    const/16 v6, 0x80

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1072
    const v3, 0x7f0807cb

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/LinearLayout;

    .line 1073
    .restart local v22    # "lin_layout1":Landroid/widget/LinearLayout;
    invoke-virtual/range {v22 .. v22}, Landroid/widget/LinearLayout;->buildDrawingCache()V

    .line 1074
    invoke-virtual/range {v22 .. v22}, Landroid/widget/LinearLayout;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v24

    .line 1076
    .restart local v24    # "lo1":Landroid/graphics/Bitmap;
    const/16 v19, 0x0

    .line 1077
    .restart local v19    # "config":Landroid/graphics/Bitmap$Config;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v28

    .line 1078
    .restart local v28    # "w":I
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    .line 1079
    .restart local v21    # "h":I
    if-nez v19, :cond_4

    .line 1080
    sget-object v19, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1082
    :cond_4
    sget-object v19, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 1083
    move/from16 v0, v28

    move/from16 v1, v21

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1085
    .restart local v4    # "screenShot":Landroid/graphics/Bitmap;
    new-instance v25, Landroid/graphics/Canvas;

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1086
    .restart local v25    # "newCanvas":Landroid/graphics/Canvas;
    const/4 v3, -0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1087
    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v3, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1089
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getActionBarTitle(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;)I

    move-result v5

    .line 1091
    .restart local v5    # "title":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getTime()J

    move-result-wide v13

    const/4 v15, -0x1

    const/16 v16, 0x0

    sget-object v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->Twitter:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    move-object/from16 v10, p0

    move-object v11, v4

    move v12, v5

    invoke-static/range {v10 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->createShareViewForSNS(Landroid/app/Activity;Landroid/graphics/Bitmap;IJILjava/util/List;Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;)V

    goto/16 :goto_0

    .line 1065
    .end local v4    # "screenShot":Landroid/graphics/Bitmap;
    .end local v5    # "title":I
    .end local v19    # "config":Landroid/graphics/Bitmap$Config;
    .end local v21    # "h":I
    .end local v22    # "lin_layout1":Landroid/widget/LinearLayout;
    .end local v24    # "lo1":Landroid/graphics/Bitmap;
    .end local v25    # "newCanvas":Landroid/graphics/Canvas;
    .end local v28    # "w":I
    :catch_1
    move-exception v20

    .line 1066
    .restart local v20    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual/range {v20 .. v20}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 1067
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    const-string/jumbo v7, "market://details?id=com.twitter.android"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1055
    :array_0
    .array-data 1
        0x1t
        0x0t
    .end array-data
.end method

.method protected customizeActionBar()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 457
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 458
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getActionBarTitle(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;)I

    move-result v6

    .line 459
    .local v6, "title":I
    if-lez v6, :cond_0

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 463
    :cond_0
    iget-boolean v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->isNeedInit:Z

    if-eqz v7, :cond_1

    .line 536
    :goto_0
    return-void

    .line 466
    :cond_1
    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$5;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    .line 478
    .local v5, "saveButtonListener":Landroid/view/View$OnClickListener;
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    .line 492
    .local v2, "cancleButtonListener":Landroid/view/View$OnClickListener;
    new-instance v4, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$7;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    .line 505
    .local v4, "editButtonListener":Landroid/view/View$OnClickListener;
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    .line 527
    .local v3, "deleteButtonListener":Landroid/view/View$OnClickListener;
    iget-boolean v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    if-nez v7, :cond_2

    iget-boolean v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z

    if-eqz v7, :cond_3

    .line 528
    :cond_2
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v7, 0x7f0207a9

    invoke-direct {v0, v7, v9, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;)V

    .line 529
    .local v0, "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v7, 0x7f09004f

    invoke-direct {v1, v9, v7, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;)V

    .line 530
    .local v1, "actionButton2":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v7

    new-array v8, v10, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v8, v9

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    goto :goto_0

    .line 532
    .end local v0    # "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .end local v1    # "actionButton2":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    :cond_3
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v7, 0x7f020299

    invoke-direct {v0, v7, v9, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;)V

    .line 533
    .restart local v0    # "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v7, 0x7f02029f

    invoke-direct {v1, v7, v9, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;)V

    .line 534
    .restart local v1    # "actionButton2":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v7

    new-array v8, v10, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v0, v8, v9

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    goto :goto_0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 448
    const-string/jumbo v0, "share_via_popup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 449
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;-><init>(Landroid/content/Context;)V

    .line 451
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1276
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v0, :cond_0

    .line 1277
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->aMapDetailMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

    move-result-object v0

    .line 1279
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->detailMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

    move-result-object v0

    goto :goto_0
.end method

.method public getOnSelectedItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSelectedItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1099
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$13;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;Ljava/lang/String;)V

    return-object v0
.end method

.method protected isNinePopup()Z
    .locals 5

    .prologue
    const/16 v4, 0x9

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 927
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->viewSize()I

    move-result v2

    if-ge v2, v4, :cond_1

    :cond_0
    move v0, v1

    .line 933
    :goto_0
    return v0

    .line 931
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090f73

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v0

    invoke-virtual {v2, v3, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 15
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 623
    const/16 v2, 0x7e

    move/from16 v0, p1

    if-ne v0, v2, :cond_4

    .line 624
    const/16 v2, 0x5dc

    move/from16 v0, p2

    if-ne v0, v2, :cond_1

    .line 625
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    .line 757
    :cond_0
    :goto_0
    return-void

    .line 626
    :cond_1
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_2

    .line 627
    move/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->setResult(I)V

    .line 628
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    goto :goto_0

    .line 629
    :cond_2
    const/16 v2, 0xc8

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 630
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->updateExerciseDetails()V

    .line 631
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->updatePhotoGrid()V

    .line 632
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    goto :goto_0

    .line 634
    :cond_3
    if-nez p2, :cond_0

    .line 635
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    goto :goto_0

    .line 637
    :cond_4
    const/16 v2, 0x64

    move/from16 v0, p1

    if-ne v0, v2, :cond_7

    .line 638
    if-eqz p2, :cond_0

    .line 640
    const/4 v5, 0x0

    .line 642
    .local v5, "picturePath":Ljava/lang/String;
    if-eqz p3, :cond_5

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 643
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v14

    .line 644
    .local v14, "uri":Landroid/net/Uri;
    invoke-static {v14, p0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 653
    .end local v14    # "uri":Landroid/net/Uri;
    :goto_1
    if-eqz v5, :cond_0

    .line 654
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 655
    const v2, 0x7f090990

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 646
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mLastTakePhoto:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 649
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mLastTakePhoto:Ljava/lang/String;

    .line 650
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mLastTakePhoto:Ljava/lang/String;

    goto :goto_1

    .line 658
    :cond_6
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file://"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 660
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mSnsType:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$SnsType;

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$SnsType;->FaceBook:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$SnsType;

    if-ne v2, v3, :cond_0

    goto/16 :goto_0

    .line 668
    .end local v5    # "picturePath":Ljava/lang/String;
    :cond_7
    const/16 v2, 0x99

    move/from16 v0, p1

    if-ne v0, v2, :cond_8

    .line 669
    const/4 v2, -0x1

    move-object/from16 v0, p3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->setResult(ILandroid/content/Intent;)V

    .line 670
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    goto/16 :goto_0

    .line 671
    :cond_8
    const/16 v2, 0x4e20

    move/from16 v0, p1

    if-ne v0, v2, :cond_a

    .line 672
    if-eqz p2, :cond_0

    .line 675
    const/4 v5, 0x0

    .line 677
    .restart local v5    # "picturePath":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v2, :cond_9

    .line 679
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-static {v2, p0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 687
    :goto_2
    if-eqz v5, :cond_0

    .line 689
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file://"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 690
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getPathToImage()Ljava/lang/String;

    move-result-object v6

    iget-wide v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    iget-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z

    move-object v3, p0

    move-object v4, p0

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JZ)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 681
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mLastTakePhoto:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 684
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mLastTakePhoto:Ljava/lang/String;

    .line 685
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mLastTakePhoto:Ljava/lang/String;

    goto :goto_2

    .line 694
    .end local v5    # "picturePath":Ljava/lang/String;
    :cond_a
    const/16 v2, 0x4e21

    move/from16 v0, p1

    if-ne v0, v2, :cond_10

    .line 695
    if-eqz p2, :cond_0

    .line 698
    if-eqz p3, :cond_0

    .line 699
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 700
    .local v13, "inputImageCountByPath":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v10, 0x0

    .line 701
    .local v10, "arrayList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    if-eqz p3, :cond_b

    .line 702
    const-string/jumbo v2, "selectedItems"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 703
    if-nez v10, :cond_b

    .line 704
    new-instance v10, Ljava/util/ArrayList;

    .end local v10    # "arrayList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 705
    .restart local v10    # "arrayList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 708
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 711
    :cond_b
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/net/Uri;

    .line 712
    .restart local v14    # "uri":Landroid/net/Uri;
    invoke-static {v14, p0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 714
    .restart local v5    # "picturePath":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 716
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    const-string v3, "File path is empty"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 718
    :cond_c
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->isFileSupported(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 720
    sget-object v2, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->GALLERY_PATH:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 722
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exercise dir, picturePath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z

    invoke-direct {p0, v5, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->saveInfoAndUpdateViewGrid(Ljava/lang/String;JZ)V

    .line 725
    invoke-static {p0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->refreshLastTakenPhoto(Landroid/content/Context;)V

    goto :goto_3

    .line 727
    :cond_d
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getPathToImage()Ljava/lang/String;

    move-result-object v6

    .line 728
    .local v6, "newFilePath":Ljava/lang/String;
    const/4 v12, 0x0

    .line 729
    .local v12, "imageCount":I
    invoke-interface {v13, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_e

    .line 730
    add-int/lit8 v12, v12, 0x1

    .line 731
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v13, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 740
    :goto_4
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "other dir, picturePath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;

    iget-wide v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J

    iget-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z

    move-object v3, p0

    move-object v4, p0

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JZ)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$ExerciseImageScaleCopyTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_3

    .line 733
    :cond_e
    invoke-interface {v13, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 734
    add-int/lit8 v12, v12, 0x1

    .line 735
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v13, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 736
    const-string v2, ".jpg"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 745
    .end local v6    # "newFilePath":Ljava/lang/String;
    .end local v12    # "imageCount":I
    :cond_f
    const v2, 0x7f090990

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_3

    .line 751
    .end local v5    # "picturePath":Ljava/lang/String;
    .end local v10    # "arrayList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "inputImageCountByPath":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v14    # "uri":Landroid/net/Uri;
    :cond_10
    if-eqz p2, :cond_0

    .line 754
    move/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->setResult(ILandroid/content/Intent;)V

    .line 755
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    goto/16 :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 986
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    if-eqz v0, :cond_0

    .line 987
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->saveExerciseData()V

    .line 988
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->setResult(I)V

    .line 989
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    .line 993
    :goto_0
    return-void

    .line 992
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 188
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 189
    iput-object p0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mContext:Landroid/content/Context;

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0301bc

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mContent:Landroid/view/View;

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mContent:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->setContentView(Landroid/view/View;)V

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 194
    .local v0, "window":Landroid/view/Window;
    if-eqz v0, :cond_0

    .line 196
    const/16 v1, 0x400

    const/16 v2, 0x500

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 201
    :cond_0
    const v1, 0x7f0806ee

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mPicturesHeader:Landroid/widget/TextView;

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mPicturesHeader:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09007f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901fd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 207
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onCreate..."

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getDatas(Landroid/os/Bundle;)V

    .line 209
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->initLayout()V

    .line 210
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->initMapFragment()V

    .line 211
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->isNeedInit:Z

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->customizeActionBar()V

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mDummyFocus:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 215
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 400
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z

    if-eqz v1, :cond_0

    .line 401
    const/4 v1, 0x0

    .line 404
    :goto_0
    return v1

    .line 402
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 403
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f10001d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 404
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 974
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy ....."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 976
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 410
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 424
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 413
    :sswitch_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/sec/android/app/shealth/common/utils/PrintUtils;->printSnapShot(Ljava/lang/String;Landroid/app/Activity;)V
    :try_end_0
    .catch Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 414
    :catch_0
    move-exception v0

    .line 415
    .local v0, "e":Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;->printStackTrace()V

    .line 417
    const v2, 0x7f0900e5

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 421
    .end local v0    # "e":Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->showDeleteDialog()V

    goto :goto_0

    .line 410
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080c8d -> :sswitch_1
        0x7f080ca7 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 343
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 344
    const-string v1, "bundleTag"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 345
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 346
    const-string v1, "comment"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    .line 347
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->setText(Ljava/lang/String;)V

    .line 350
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onRestoreInstanceState........ : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    :goto_0
    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    return-void

    .line 350
    :cond_2
    const-string v1, ""

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 330
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 331
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 332
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "onSaveInstanceState .."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->getText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 334
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->memoEditView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    .line 336
    :cond_0
    const-string v1, "comment"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const-string v1, "bundleTag"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 339
    return-void
.end method

.method public setCameraBtnClick()V
    .locals 2

    .prologue
    .line 937
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->viewSize()I

    move-result v0

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 939
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->doTakePhotoAction()V

    .line 941
    :cond_0
    return-void
.end method

.method public setGalleryBtnClick(I)V
    .locals 3
    .param p1, "RequestCode"    # I

    .prologue
    .line 944
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->viewSize()I

    move-result v0

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 945
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->viewSize()I

    move-result v0

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->TAG:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mContext:Landroid/content/Context;

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->setGalleryBtnClick(IILjava/lang/String;Landroid/content/Context;)V

    .line 947
    :cond_0
    return-void
.end method
