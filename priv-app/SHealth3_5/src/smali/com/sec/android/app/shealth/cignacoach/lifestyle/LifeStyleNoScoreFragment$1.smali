.class Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment$1;
.super Ljava/lang/Object;
.source "LifeStyleNoScoreFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 58
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->mRemainCategory:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->access$000(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;)[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->mRemainCategory:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->access$000(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;)[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_0

    .line 60
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 61
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "intent_lifestyle_categorys"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->mRemainCategory:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->access$000(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;)[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 64
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
