.class public Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;
.super Ljava/lang/Object;
.source "AverageValuesHolder.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mMemoComment:Ljava/lang/String;

.field private mPeriodStart:J

.field private mTimeValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;->TAG:Ljava/lang/String;

    .line 50
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;->mTimeValue:I

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;->mPeriodStart:J

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;->mMemoComment:Ljava/lang/String;

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>([FIJ)V
    .locals 0
    .param p1, "avgData"    # [F
    .param p2, "timeValue"    # I
    .param p3, "periodStart"    # J

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;->mTimeValue:I

    .line 26
    iput-wide p3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;->mPeriodStart:J

    .line 27
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public getTimeValue()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;->mTimeValue:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;->mTimeValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 46
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;->mPeriodStart:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/AverageValuesHolder;->mMemoComment:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 48
    return-void
.end method
