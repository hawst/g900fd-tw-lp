.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$1;
.super Ljava/lang/Object;
.source "ProfileSecondView.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 153
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    move-result-object v0

    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setDescendantFocusability(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setFocusableInTouchMode(Z)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->hideWeightKeyboard()V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->requestFocus()Z

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    move-result-object v0

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setDescendantFocusability(I)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setFocusableInTouchMode(Z)V

    .line 163
    :cond_0
    return v2
.end method
