.class public interface abstract Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;
.super Ljava/lang/Object;
.source "IGoogleAnalyticsEasyTracker.java"


# virtual methods
.method public abstract activityStart(Landroid/app/Activity;)V
.end method

.method public abstract activityStop(Landroid/app/Activity;)V
.end method

.method public abstract getTracker(Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;
.end method

.method public abstract send(Landroid/os/Bundle;)V
.end method

.method public abstract setCustomDimension(ILjava/lang/String;)V
.end method

.method public abstract setCustomMetric(ILjava/lang/String;)V
.end method
