.class public Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;
.super Landroid/widget/RelativeLayout;
.source "QuickInputImagePanel.java"


# static fields
.field private static final BREAKFAST_INDEX:I = 0x0

.field private static final DINNER_INDEX:I = 0x2

.field private static final LARGE_INDEX:I = 0x3

.field private static final LUNCH_INDEX:I = 0x1

.field private static final MEAL_SIZE:I = 0x4

.field private static final MEDIUM_INDEX:I = 0x2

.field private static final SKIPPED_INDEX:I = 0x0

.field private static final SMALL_INDEX:I = 0x1

.field private static final SNACKS_INDEX:I = 0x3

.field private static final STATE_PARENT:Ljava/lang/String; = "state_parent"


# instance fields
.field private final LARGE:Ljava/lang/String;

.field private final MEDIUM:Ljava/lang/String;

.field private final SELECTED:Ljava/lang/String;

.field private final SKIPPED:Ljava/lang/String;

.field private final SMALL:Ljava/lang/String;

.field private config:Landroid/content/res/Configuration;

.field private mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

.field private mCalorieValues:[I

.field private mEditTextContainer:Landroid/widget/LinearLayout;

.field private mEditTextwatcher:Landroid/text/TextWatcher;

.field mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

.field private mIncorrectRangeToast:Landroid/widget/Toast;

.field private mInvalidInputToast:Landroid/widget/Toast;

.field private mIsSkippedSelected:Z

.field private mLargeIcon:Landroid/widget/ImageView;

.field private mLargeMealLayout:Landroid/widget/RelativeLayout;

.field private mMediumIcon:Landroid/widget/ImageView;

.field private mMediumMealLayout:Landroid/widget/RelativeLayout;

.field private mMinMaxIntegerInputFilter:Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

.field private mSelectedIndex:I

.field private mSkippedIcon:Landroid/widget/ImageView;

.field private mSkippedMealLayout:Landroid/widget/RelativeLayout;

.field private mSmallIcon:Landroid/widget/ImageView;

.field private mSmallMealLayout:Landroid/widget/RelativeLayout;

.field private mTotalCalorieValue:I

.field private mViewContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x2

    .line 76
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 59
    iput v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SKIPPED:Ljava/lang/String;

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SMALL:Ljava/lang/String;

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->MEDIUM:Ljava/lang/String;

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->LARGE:Ljava/lang/String;

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SELECTED:Ljava/lang/String;

    .line 395
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;

    const/4 v1, 0x0

    const v2, 0x461c3c00    # 9999.0f

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMinMaxIntegerInputFilter:Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    .line 433
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$9;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEditTextwatcher:Landroid/text/TextWatcher;

    .line 610
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    .line 77
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->init()V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x2

    .line 83
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    iput v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SKIPPED:Ljava/lang/String;

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SMALL:Ljava/lang/String;

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->MEDIUM:Ljava/lang/String;

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->LARGE:Ljava/lang/String;

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SELECTED:Ljava/lang/String;

    .line 395
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;

    const/4 v1, 0x0

    const v2, 0x461c3c00    # 9999.0f

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMinMaxIntegerInputFilter:Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    .line 433
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$9;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEditTextwatcher:Landroid/text/TextWatcher;

    .line 610
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    .line 84
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    .line 85
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->init()V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x2

    .line 90
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    iput v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SKIPPED:Ljava/lang/String;

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SMALL:Ljava/lang/String;

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->MEDIUM:Ljava/lang/String;

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->LARGE:Ljava/lang/String;

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SELECTED:Ljava/lang/String;

    .line 395
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;

    const/4 v1, 0x0

    const v2, 0x461c3c00    # 9999.0f

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMinMaxIntegerInputFilter:Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    .line 433
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$9;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEditTextwatcher:Landroid/text/TextWatcher;

    .line 610
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    .line 91
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->init()V

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->resetAllViews()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setSkippedIntakeView()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setSmallIntakeView()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setMediumIntakeView()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setLargeIntakeView()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->showMinMaxToast()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mInvalidInputToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mInvalidInputToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    return-object v0
.end method

.method private getIntCalorieValue(Landroid/widget/TextView;)I
    .locals 3
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 357
    const/4 v0, 0x0

    .line 358
    .local v0, "calValue":I
    if-eqz p1, :cond_0

    .line 360
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 363
    .local v1, "textValue":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 371
    .end local v1    # "textValue":Ljava/lang/String;
    :cond_0
    :goto_0
    return v0

    .line 365
    .restart local v1    # "textValue":Ljava/lang/String;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 98
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 99
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const v1, 0x7f030111

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->initViews()V

    .line 101
    return-void
.end method

.method private initViews()V
    .locals 4

    .prologue
    .line 106
    const v0, 0x7f080481

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSkippedMealLayout:Landroid/widget/RelativeLayout;

    .line 107
    const v0, 0x7f080484

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallMealLayout:Landroid/widget/RelativeLayout;

    .line 108
    const v0, 0x7f080488

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumMealLayout:Landroid/widget/RelativeLayout;

    .line 109
    const v0, 0x7f08048c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeMealLayout:Landroid/widget/RelativeLayout;

    .line 110
    const v0, 0x7f080482

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSkippedIcon:Landroid/widget/ImageView;

    .line 111
    const v0, 0x7f080485

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallIcon:Landroid/widget/ImageView;

    .line 112
    const v0, 0x7f080489

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumIcon:Landroid/widget/ImageView;

    .line 113
    const v0, 0x7f08048d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeIcon:Landroid/widget/ImageView;

    .line 114
    const v0, 0x7f08047d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEditTextContainer:Landroid/widget/LinearLayout;

    .line 115
    const v0, 0x7f08047e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const v3, 0x461c3c00    # 9999.0f

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setIntervalValues(FFF)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setEmptyFieldListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMinMaxIntegerInputFilter:Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    new-instance v1, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$1;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    new-instance v1, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$2;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 150
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setIconSelection()V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSkippedMealLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$3;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallMealLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$4;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumMealLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$5;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeMealLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$6;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->isHardKeyBoardPresent(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEditTextContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02019e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEditTextContainer:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$7;-><init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEditTextContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private resetAllViews()V
    .locals 5

    .prologue
    const v4, 0x7f070214

    const/high16 v3, -0x1000000

    const/4 v2, 0x0

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSkippedIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSkippedIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SKIPPED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSkippedMealLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SKIPPED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SMALL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallMealLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SMALL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->MEDIUM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumMealLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->MEDIUM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->LARGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeMealLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->LARGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 257
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mIsSkippedSelected:Z

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallMealLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080486

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumMealLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08048a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeMealLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08048e

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSkippedMealLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080483

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallMealLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080487

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumMealLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08048b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeMealLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08048f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 267
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    .line 269
    return-void
.end method

.method private setIconSelection()V
    .locals 1

    .prologue
    .line 589
    iget v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    packed-switch v0, :pswitch_data_0

    .line 605
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->resetAllViews()V

    .line 608
    :goto_0
    return-void

    .line 593
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setSkippedIntakeView()V

    goto :goto_0

    .line 596
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setSmallIntakeView()V

    goto :goto_0

    .line 599
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setMediumIntakeView()V

    goto :goto_0

    .line 602
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setLargeIntakeView()V

    goto :goto_0

    .line 589
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setLargeIntakeView()V
    .locals 4

    .prologue
    const v3, 0x7f08048e

    .line 551
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->resetAllViews()V

    .line 552
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeIcon:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 553
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeMealLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->LARGE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SELECTED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeIcon:Landroid/widget/ImageView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->LARGE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SELECTED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 555
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeMealLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeMealLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08048f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07010b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 557
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeMealLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getIntCalorieValue(Landroid/widget/TextView;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mTotalCalorieValue:I

    .line 559
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setTotalCalorieText()V

    .line 560
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    .line 561
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 563
    return-void
.end method

.method private setMediumIntakeView()V
    .locals 4

    .prologue
    const v3, 0x7f08048a

    .line 536
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->resetAllViews()V

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumIcon:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 538
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumMealLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->MEDIUM:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SELECTED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumIcon:Landroid/widget/ImageView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->MEDIUM:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SELECTED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 540
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumMealLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumMealLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08048b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07010b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumMealLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getIntCalorieValue(Landroid/widget/TextView;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mTotalCalorieValue:I

    .line 544
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setTotalCalorieText()V

    .line 545
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    .line 546
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 548
    return-void
.end method

.method private setSkippedIntakeView()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 509
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->resetAllViews()V

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSkippedIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSkippedMealLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SKIPPED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SELECTED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSkippedIcon:Landroid/widget/ImageView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SKIPPED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SELECTED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 513
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mIsSkippedSelected:Z

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSkippedMealLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080483

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07010b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 515
    iput v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mTotalCalorieValue:I

    .line 516
    iput v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    .line 517
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setTotalCalorieText()V

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 519
    return-void
.end method

.method private setSmallIntakeView()V
    .locals 5

    .prologue
    const v4, 0x7f080486

    const/4 v3, 0x1

    .line 522
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->resetAllViews()V

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 524
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallMealLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SMALL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SELECTED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 525
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallIcon:Landroid/widget/ImageView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SMALL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->SELECTED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 526
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallMealLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 527
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallMealLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080487

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07010b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallMealLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getIntCalorieValue(Landroid/widget/TextView;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mTotalCalorieValue:I

    .line 530
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setTotalCalorieText()V

    .line 531
    iput v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 533
    return-void
.end method

.method private setTotalCalorieText()V
    .locals 3

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mTotalCalorieValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    .line 347
    return-void
.end method

.method private showMinMaxToast()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 628
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mIncorrectRangeToast:Landroid/widget/Toast;

    if-nez v1, :cond_0

    .line 631
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    const v2, 0x7f09098d

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/16 v4, 0x270f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 635
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;

    invoke-static {v1, v0, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mIncorrectRangeToast:Landroid/widget/Toast;

    .line 639
    .end local v0    # "text":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mIncorrectRangeToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 642
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mIncorrectRangeToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 645
    :cond_1
    return-void
.end method

.method private updateCalorieValueText(I)V
    .locals 5
    .param p1, "calorie"    # I

    .prologue
    .line 311
    div-int/lit8 v1, p1, 0x2

    .line 312
    .local v1, "smallMealCalorie":I
    add-int v0, p1, v1

    .line 314
    .local v0, "largeMealCalorie":I
    iget v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    packed-switch v2, :pswitch_data_0

    .line 330
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setTotalCalorieText()V

    .line 332
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isFocused()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEditTextContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 333
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setSelection(I)V

    .line 335
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSmallMealLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f080486

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mMediumMealLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f08048a

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 337
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mLargeMealLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f08048e

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    return-void

    .line 317
    :pswitch_0
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mTotalCalorieValue:I

    goto/16 :goto_0

    .line 320
    :pswitch_1
    iput v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mTotalCalorieValue:I

    goto/16 :goto_0

    .line 323
    :pswitch_2
    iput p1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mTotalCalorieValue:I

    goto/16 :goto_0

    .line 326
    :pswitch_3
    iput v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mTotalCalorieValue:I

    goto/16 :goto_0

    .line 314
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getEmptyFieldListener()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    return-object v0
.end method

.method public getQuickInputCalorieValue()I
    .locals 4

    .prologue
    .line 381
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 382
    .local v2, "textValue":Ljava/lang/String;
    const/4 v0, 0x0

    .line 385
    .local v0, "calValue":I
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 392
    :goto_0
    return v0

    .line 387
    :catch_0
    move-exception v1

    .line 389
    .local v1, "e":Ljava/lang/NumberFormatException;
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSKippedSelected()Z
    .locals 1

    .prologue
    .line 430
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mIsSkippedSelected:Z

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 578
    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    .line 579
    .local v0, "savedState":Landroid/os/Bundle;
    const-string v2, "QUICK_INPUT_SELECTED_INDEX"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    .line 580
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setIconSelection()V

    .line 581
    const-string/jumbo v2, "state_parent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 582
    .local v1, "superState":Landroid/os/Parcelable;
    invoke-super {p0, v1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 584
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 568
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 569
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 570
    .local v0, "state":Landroid/os/Bundle;
    const-string/jumbo v2, "state_parent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 571
    const-string v2, "QUICK_INPUT_SELECTED_INDEX"

    iget v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mSelectedIndex:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 572
    return-object v0
.end method

.method public selectIcon(III)V
    .locals 5
    .param p1, "calorie"    # I
    .param p2, "quickInputType"    # I
    .param p3, "mealType"    # I

    .prologue
    .line 459
    iput p1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mTotalCalorieValue:I

    .line 460
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setTotalCalorieText()V

    .line 461
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    array-length v3, v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 464
    const/4 v1, 0x0

    .line 465
    .local v1, "mediumCalorie":I
    packed-switch p3, :pswitch_data_0

    .line 481
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->resetAllViews()V

    .line 482
    if-nez p1, :cond_1

    const v3, 0x46cd7

    if-ne p2, v3, :cond_1

    .line 484
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setSkippedIntakeView()V

    .line 505
    .end local v1    # "mediumCalorie":I
    :cond_0
    :goto_1
    return-void

    .line 468
    .restart local v1    # "mediumCalorie":I
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    const/4 v4, 0x0

    aget v1, v3, v4

    .line 469
    goto :goto_0

    .line 471
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    const/4 v4, 0x1

    aget v1, v3, v4

    .line 472
    goto :goto_0

    .line 474
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    const/4 v4, 0x2

    aget v1, v3, v4

    .line 475
    goto :goto_0

    .line 477
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    const/4 v4, 0x3

    aget v1, v3, v4

    goto :goto_0

    .line 488
    :cond_1
    div-int/lit8 v2, v1, 0x2

    .line 489
    .local v2, "smallMealCalorie":I
    add-int v0, v1, v2

    .line 490
    .local v0, "largeMealCalorie":I
    if-ne p1, v1, :cond_2

    .line 492
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setMediumIntakeView()V

    goto :goto_1

    .line 494
    :cond_2
    if-ne p1, v2, :cond_3

    .line 496
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setSmallIntakeView()V

    goto :goto_1

    .line 498
    :cond_3
    if-ne p1, v0, :cond_0

    .line 500
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setLargeIntakeView()V

    goto :goto_1

    .line 465
    nop

    :pswitch_data_0
    .packed-switch 0x186a1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public varargs setCalories([I)V
    .locals 0
    .param p1, "calories"    # [I

    .prologue
    .line 273
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    .line 274
    return-void
.end method

.method public updateCalorieValues(I)V
    .locals 2
    .param p1, "mealType"    # I

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    array-length v0, v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 285
    packed-switch p1, :pswitch_data_0

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 288
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->updateCalorieValueText(I)V

    goto :goto_0

    .line 291
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->updateCalorieValueText(I)V

    goto :goto_0

    .line 294
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->updateCalorieValueText(I)V

    goto :goto_0

    .line 297
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValues:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->updateCalorieValueText(I)V

    goto :goto_0

    .line 285
    :pswitch_data_0
    .packed-switch 0x186a1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
