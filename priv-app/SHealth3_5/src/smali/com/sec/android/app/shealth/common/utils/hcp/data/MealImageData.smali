.class public Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
.super Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
.source "MealImageData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mFilePath:Ljava/lang/String;

.field private mMealId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 2
    .param p1, "mealId"    # J
    .param p3, "filePath"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 26
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mMealId:J

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mFilePath:Ljava/lang/String;

    .line 48
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mMealId:J

    .line 49
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mFilePath:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;JJJI)V
    .locals 8
    .param p1, "mealId"    # J
    .param p3, "filePath"    # Ljava/lang/String;
    .param p4, "id"    # J
    .param p6, "createTime"    # J
    .param p8, "updateTime"    # J
    .param p10, "timeZone"    # I

    .prologue
    .line 38
    move-object v0, p0

    move-wide v1, p4

    move-wide v3, p6

    move-wide/from16 v5, p8

    move/from16 v7, p10

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(JJJI)V

    .line 26
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mMealId:J

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mFilePath:Ljava/lang/String;

    .line 39
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mMealId:J

    .line 40
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mFilePath:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 103
    if-ne p0, p1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v1

    .line 106
    :cond_1
    if-eqz p1, :cond_2

    instance-of v3, p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    if-nez v3, :cond_3

    :cond_2
    move v1, v2

    .line 107
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 109
    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .line 110
    .local v0, "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    iget-wide v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mMealId:J

    iget-wide v5, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mMealId:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    move v1, v2

    .line 111
    goto :goto_0

    .line 113
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mFilePath:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mFilePath:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 114
    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getMealId()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mMealId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 122
    const-wide/16 v2, 0x1f

    iget-wide v4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mMealId:J

    mul-long v3, v2, v4

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mFilePath:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mFilePath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_0
    int-to-long v5, v2

    add-long v0, v3, v5

    .line 123
    .local v0, "result":J
    long-to-int v2, v0

    return v2

    .line 122
    .end local v0    # "result":J
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setMealId(J)V
    .locals 0
    .param p1, "mealId"    # J

    .prologue
    .line 63
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mMealId:J

    .line 64
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mMealId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->mFilePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getCreateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getUpdateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getTimeZone()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 81
    return-void
.end method
