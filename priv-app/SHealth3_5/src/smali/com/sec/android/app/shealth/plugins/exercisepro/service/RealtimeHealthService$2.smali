.class Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$2;
.super Ljava/lang/Object;
.source "RealtimeHealthService.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGoalEvnet(III)V
    .locals 7
    .param p1, "eventType"    # I
    .param p2, "eventValue"    # I
    .param p3, "value"    # I

    .prologue
    .line 226
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v4

    monitor-enter v4

    .line 227
    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    .line 228
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput p2, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->paceGuide:I

    .line 229
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .line 230
    .local v1, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    invoke-interface {v1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateTEPaceGuide(I)V

    goto :goto_0

    .line 243
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 232
    :cond_0
    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    .line 233
    int-to-float v3, p3

    const/high16 v5, 0x41200000    # 10.0f

    div-float v2, v3, v5

    .line 234
    .local v2, "teValue":F
    :try_start_1
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GOAL_EVENT_TE_PACE  value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .line 236
    .restart local v1    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/16 v3, 0x17

    const/4 v5, 0x0

    invoke-interface {v1, v3, v2, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    goto :goto_1

    .line 238
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    .end local v2    # "teValue":F
    :cond_1
    const/4 v3, 0x3

    if-ne p1, v3, :cond_2

    .line 239
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .line 240
    .restart local v1    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/16 v3, 0x18

    int-to-float v5, p3

    const/4 v6, 0x0

    invoke-interface {v1, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V

    goto :goto_2

    .line 243
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :cond_2
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244
    return-void
.end method
