.class Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;
.super Ljava/lang/Object;
.source "CompatibleDeviceConnectActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->isHRMFeature(Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "S013"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->dismissDialog()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$000(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$102(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$100(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->setCancelable(Z)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$100(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900a4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->show(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$100(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;-><init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->startDiscoveringSensorDevices()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$500(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)V

    .line 189
    return-void
.end method
