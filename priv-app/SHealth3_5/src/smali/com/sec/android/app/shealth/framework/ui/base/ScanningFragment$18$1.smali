.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 40

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0xa

    :try_start_0
    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x10

    aput v12, v2, v11

    const/16 v11, 0x511d

    aput v11, v2, v10

    const/16 v10, -0x7ec8

    aput v10, v2, v9

    const/16 v9, -0x9

    aput v9, v2, v8

    const/16 v8, 0x6217

    aput v8, v2, v7

    const/16 v7, 0x4307

    aput v7, v2, v6

    const/16 v6, -0x1f0

    aput v6, v2, v5

    const/16 v5, -0x64

    aput v5, v2, v4

    const/4 v4, -0x3

    aput v4, v2, v3

    const/16 v3, 0x4030

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x6b

    aput v13, v1, v12

    const/16 v12, 0x517e

    aput v12, v1, v11

    const/16 v11, -0x7eaf

    aput v11, v1, v10

    const/16 v10, -0x7f

    aput v10, v1, v9

    const/16 v9, 0x6265

    aput v9, v1, v8

    const/16 v8, 0x4362

    aput v8, v1, v7

    const/16 v7, -0x1bd

    aput v7, v1, v6

    const/4 v6, -0x2

    aput v6, v1, v5

    const/16 v5, -0x6c

    aput v5, v1, v4

    const/16 v4, 0x405c

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0xf

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0x1f05

    aput v18, v2, v17

    const/16 v17, 0x3d31

    aput v17, v2, v16

    const/16 v16, -0x67b4

    aput v16, v2, v15

    const/4 v15, -0x3

    aput v15, v2, v14

    const/16 v14, 0x5a01

    aput v14, v2, v13

    const/16 v13, 0x747a

    aput v13, v2, v12

    const/16 v12, 0x601a

    aput v12, v2, v11

    const/16 v11, -0x54ff

    aput v11, v2, v10

    const/16 v10, -0x38

    aput v10, v2, v9

    const/16 v9, -0x75

    aput v9, v2, v8

    const/16 v8, -0x34ff

    aput v8, v2, v7

    const/16 v7, -0x47

    aput v7, v2, v6

    const/16 v6, -0x22

    aput v6, v2, v5

    const/16 v5, -0x40

    aput v5, v2, v3

    const/16 v3, -0x42bb

    aput v3, v2, v1

    const/16 v1, 0xf

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0x1f2b

    aput v19, v1, v18

    const/16 v18, 0x3d1f

    aput v18, v1, v17

    const/16 v17, -0x67c3

    aput v17, v1, v16

    const/16 v16, -0x68

    aput v16, v1, v15

    const/16 v15, 0x5a73

    aput v15, v1, v14

    const/16 v14, 0x745a

    aput v14, v1, v13

    const/16 v13, 0x6074

    aput v13, v1, v12

    const/16 v12, -0x54a0

    aput v12, v1, v11

    const/16 v11, -0x55

    aput v11, v1, v10

    const/16 v10, -0x28

    aput v10, v1, v9

    const/16 v9, -0x348b

    aput v9, v1, v8

    const/16 v8, -0x35

    aput v8, v1, v7

    const/16 v7, -0x41

    aput v7, v1, v6

    const/16 v6, -0x4c

    aput v6, v1, v5

    const/16 v5, -0x42ca

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_4

    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->checkIsHrmSwitchReq:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHRM:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->leaveDeviceWithOutRefresh(Ljava/lang/String;Z)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;Z)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectToastReq:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5302(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHrmUIName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isPairingNeeded:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$3402(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->pairDeviceId:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$3502(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;)Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x6370

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x630c

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1402(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsJoining:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2302(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->connecting:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->show(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->setCancelable(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->selectedDeviceId:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$3702(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;)Ljava/lang/String;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceConnectivityType()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v4, v4, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$3100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)I

    move-result v4

    const/16 v5, 0x28

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v6, v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v6, v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentScanListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->startScan(ILjava/util/ArrayList;IILcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;)V

    :cond_1
    :goto_6
    return-void

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :catch_0
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x2f

    aput v12, v2, v11

    const/16 v11, 0x6c65

    aput v11, v2, v10

    const/16 v10, 0x3f05

    aput v10, v2, v9

    const/16 v9, -0x78b7

    aput v9, v2, v8

    const/16 v8, -0xb

    aput v8, v2, v7

    const/16 v7, -0x5d

    aput v7, v2, v6

    const/16 v6, -0x13

    aput v6, v2, v5

    const/16 v5, -0x2a99

    aput v5, v2, v4

    const/16 v4, -0x44

    aput v4, v2, v3

    const/16 v3, -0x74

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x4c

    aput v13, v1, v12

    const/16 v12, 0x6c06

    aput v12, v1, v11

    const/16 v11, 0x3f6c

    aput v11, v1, v10

    const/16 v10, -0x78c1

    aput v10, v1, v9

    const/16 v9, -0x79

    aput v9, v1, v8

    const/16 v8, -0x3a

    aput v8, v1, v7

    const/16 v7, -0x42

    aput v7, v1, v6

    const/16 v6, -0x2afb

    aput v6, v1, v5

    const/16 v5, -0x2b

    aput v5, v1, v4

    const/16 v4, -0x20

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v4, v1

    if-lt v3, v4, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v4, v1

    if-lt v3, v4, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x3a

    aput v38, v2, v37

    const/16 v37, -0x73

    aput v37, v2, v36

    const/16 v36, -0x38

    aput v36, v2, v35

    const/16 v35, -0x449f

    aput v35, v2, v34

    const/16 v34, -0x65

    aput v34, v2, v33

    const/16 v33, -0x7b

    aput v33, v2, v32

    const/16 v32, -0x6ec8

    aput v32, v2, v31

    const/16 v31, -0x4f

    aput v31, v2, v30

    const/16 v30, -0x6b

    aput v30, v2, v29

    const/16 v29, 0x7e1a

    aput v29, v2, v28

    const/16 v28, -0x20ee

    aput v28, v2, v27

    const/16 v27, -0x4d

    aput v27, v2, v26

    const/16 v26, -0x6a

    aput v26, v2, v25

    const/16 v25, 0x624e

    aput v25, v2, v24

    const/16 v24, -0x68ea

    aput v24, v2, v23

    const/16 v23, -0x7

    aput v23, v2, v22

    const/16 v22, -0x61

    aput v22, v2, v21

    const/16 v21, -0x7b

    aput v21, v2, v20

    const/16 v20, -0x4b

    aput v20, v2, v19

    const/16 v19, -0x148a

    aput v19, v2, v18

    const/16 v18, -0x72

    aput v18, v2, v17

    const/16 v17, -0x5

    aput v17, v2, v16

    const/16 v16, -0x18

    aput v16, v2, v15

    const/16 v15, 0x3363

    aput v15, v2, v14

    const/16 v14, -0x58bf

    aput v14, v2, v13

    const/16 v13, -0x1f

    aput v13, v2, v12

    const/16 v12, -0x5f

    aput v12, v2, v11

    const/16 v11, -0xb

    aput v11, v2, v10

    const/16 v10, -0x5494

    aput v10, v2, v9

    const/16 v9, -0x3b

    aput v9, v2, v8

    const/16 v8, -0x10

    aput v8, v2, v7

    const/16 v7, -0x5a

    aput v7, v2, v6

    const/16 v6, -0x75

    aput v6, v2, v5

    const/16 v5, 0x256e

    aput v5, v2, v3

    const/16 v3, 0x5f48

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x56

    aput v39, v1, v38

    const/16 v38, -0x1f

    aput v38, v1, v37

    const/16 v37, -0x43

    aput v37, v1, v36

    const/16 v36, -0x44f1

    aput v36, v1, v35

    const/16 v35, -0x45

    aput v35, v1, v34

    const/16 v34, -0xa

    aput v34, v1, v33

    const/16 v33, -0x6eaf

    aput v33, v1, v32

    const/16 v32, -0x6f

    aput v32, v1, v31

    const/16 v31, -0x19

    aput v31, v1, v30

    const/16 v30, 0x7e7f

    aput v30, v1, v29

    const/16 v29, -0x2082

    aput v29, v1, v28

    const/16 v28, -0x21

    aput v28, v1, v27

    const/16 v27, -0x7

    aput v27, v1, v26

    const/16 v26, 0x623c

    aput v26, v1, v25

    const/16 v25, -0x689e

    aput v25, v1, v24

    const/16 v24, -0x69

    aput v24, v1, v23

    const/16 v23, -0x10

    aput v23, v1, v22

    const/16 v22, -0x3a

    aput v22, v1, v21

    const/16 v21, -0x3f

    aput v21, v1, v20

    const/16 v20, -0x14e8

    aput v20, v1, v19

    const/16 v19, -0x15

    aput v19, v1, v18

    const/16 v18, -0x6a

    aput v18, v1, v17

    const/16 v17, -0x71

    aput v17, v1, v16

    const/16 v16, 0x3302

    aput v16, v1, v15

    const/16 v15, -0x58cd

    aput v15, v1, v14

    const/16 v14, -0x59

    aput v14, v1, v13

    const/16 v13, -0x3a

    aput v13, v1, v12

    const/16 v12, -0x65

    aput v12, v1, v11

    const/16 v11, -0x54fb

    aput v11, v1, v10

    const/16 v10, -0x55

    aput v10, v1, v9

    const/16 v9, -0x62

    aput v9, v1, v8

    const/16 v8, -0x39

    aput v8, v1, v7

    const/16 v7, -0x18

    aput v7, v1, v6

    const/16 v6, 0x253d

    aput v6, v1, v5

    const/16 v5, 0x5f25

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_4

    goto/16 :goto_6

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_6

    :cond_8
    :try_start_3
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_9
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_3 .. :try_end_3} :catch_4

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_6

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_6

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto/16 :goto_6
.end method
