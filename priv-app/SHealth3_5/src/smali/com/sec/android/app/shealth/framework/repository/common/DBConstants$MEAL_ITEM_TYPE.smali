.class public final Lcom/sec/android/app/shealth/framework/repository/common/DBConstants$MEAL_ITEM_TYPE;
.super Ljava/lang/Object;
.source "DBConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MEAL_ITEM_TYPE"
.end annotation


# static fields
.field public static final DEFAULT_VALUE:I = -0x1

.field public static final EXTRA_LARGE:I = 0x4

.field public static final EXTRA_LARGE_COEFFICIENT:F = 1.5f

.field public static final ID:I = 0x3eb

.field public static final LARGE:I = 0x3

.field public static final LARGE_COEFFICIENT:F = 1.3f

.field public static final MEDIUM:I = 0x2

.field public static final MEDIUM_COEFFICIENT:F = 1.0f

.field public static final ROW_ID:I = 0x7d3

.field public static final SMALL:I = 0x1

.field public static final SMALL_COEFFICIENT:F = 0.7f

.field public static final TINY:I = 0x0

.field public static final TINY_COEFFICIENT:F = 0.5f

.field public static final UNIT_DEFAULT:I = 0x0

.field public static final UNIT_G:I = 0x1

.field public static final UNIT_OZ:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
