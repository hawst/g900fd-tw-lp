.class public Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "LifeStyleReassessmentActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCancelUnalignedGoalDescription:Ljava/lang/String;

.field private mCancelUnalignedGoalMessage:Ljava/lang/String;

.field private mReassessCompletedFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentCompletedFragment;

.field private mReassessGoalWarningDescription:Ljava/lang/String;

.field private mReassessGoalWarningFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessGoalWarningFragment;

.field private mReassessGoalWarningMessage:Ljava/lang/String;

.field private mReassessLessThan6Fragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentLessThan6Fragment;

.field private mReassessLessThanSix:Ljava/lang/String;

.field private mReassessLessThanSixDescription:Ljava/lang/String;

.field private mReassessNowFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentNowFragment;

.field mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThanSix:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThanSixDescription:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessGoalWarningMessage:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessGoalWarningDescription:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mCancelUnalignedGoalMessage:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mCancelUnalignedGoalDescription:Ljava/lang/String;

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mState:I

    return-void
.end method

.method private checkReassessStatus()I
    .locals 8

    .prologue
    .line 101
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "startLifestyleReassessment"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/4 v0, -0x1

    .line 103
    .local v0, "ReassessStatus":I
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCoachMsgsForLifestyleReassessment()Ljava/util/List;

    move-result-object v3

    .line 106
    .local v3, "lcm":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz v3, :cond_8

    .line 107
    :try_start_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 108
    .local v4, "lcmSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_3

    .line 110
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_1

    .line 108
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 114
    :cond_1
    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "LifestyleReassess msg type: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "LifestyleReassess msg message: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "LifestyleReassess msg description: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v5

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_WITH_GOALS_WARNING:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v5, v6, :cond_2

    .line 118
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessGoalWarningMessage:Ljava/lang/String;

    .line 119
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessGoalWarningDescription:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 142
    .end local v2    # "i":I
    .end local v4    # "lcmSize":I
    :catch_0
    move-exception v1

    .line 144
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 147
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return v0

    .line 120
    .restart local v2    # "i":I
    .restart local v4    # "lcmSize":I
    :cond_2
    :try_start_1
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v5

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_WARNING:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v5, v6, :cond_0

    .line 121
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThanSix:Ljava/lang/String;

    .line 122
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThanSixDescription:Ljava/lang/String;

    goto/16 :goto_1

    .line 126
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThanSixDescription:Ljava/lang/String;

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessGoalWarningDescription:Ljava/lang/String;

    if-nez v5, :cond_5

    .line 127
    :cond_4
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->TAG:Ljava/lang/String;

    const-string v6, "LifestyleReassess Official Reassess without Goal Warning "

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->startOfficialReassess()V

    goto :goto_2

    .line 130
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThanSix:Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThanSixDescription:Ljava/lang/String;

    if-nez v5, :cond_7

    .line 132
    :cond_6
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->TAG:Ljava/lang/String;

    const-string v6, "LifestyleReassess Goal Warning "

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const/4 v0, 0x1

    goto :goto_2

    .line 136
    :cond_7
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->TAG:Ljava/lang/String;

    const-string v6, "LifestyleReassess Less Than Six "

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const/4 v0, 0x0

    goto :goto_2

    .line 140
    .end local v2    # "i":I
    .end local v4    # "lcmSize":I
    :cond_8
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->TAG:Ljava/lang/String;

    const-string v6, "coach msg list is null"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private processBadgeData(Landroid/content/Intent;)V
    .locals 3
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 242
    if-eqz p1, :cond_1

    .line 243
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 244
    .local v0, "receiveBadgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 245
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->startBadgeActivity(Ljava/util/ArrayList;)V

    .line 248
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 251
    .end local v0    # "receiveBadgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_1
    return-void
.end method

.method private startBadgeActivity(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 254
    .local p1, "badgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 255
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_BADGE_VIEW_MODE"

    const/16 v2, 0x115c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 256
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 257
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 258
    return-void
.end method

.method private startCignaCoachActivity()V
    .locals 3

    .prologue
    .line 232
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 233
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 234
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    const-string v1, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 236
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 237
    return-void
.end method

.method private startOfficialReassess()V
    .locals 4

    .prologue
    .line 261
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 262
    .local v1, "remainCategory":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;>;"
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 269
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 270
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "intent_lifestyle_categorys"

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 271
    const-string v2, "EXTRA_NAME_REASSESSMENT_EXIST_CURRENT_GOAL"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 272
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->finish()V

    .line 274
    return-void
.end method


# virtual methods
.method public closeScreen()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessCompletedFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentCompletedFragment;

    if-eqz v0, :cond_0

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->startCignaCoachActivity()V

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected customizeActionBar()V
    .locals 3

    .prologue
    .line 152
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090351

    const v2, 0x7f090350

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(II)V

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 157
    return-void
.end method

.method public getCancelUnalignedGoalDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mCancelUnalignedGoalDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getCancelUnalignedGoalMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mCancelUnalignedGoalMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getReassessGoalWarningDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessGoalWarningDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getReassessGoalWarningMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessGoalWarningMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getReassessLessThanSixDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThanSixDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getReassessLessThanSixMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThanSix:Ljava/lang/String;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onBackPressed()V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessCompletedFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentCompletedFragment;

    if-eqz v0, :cond_0

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->startCignaCoachActivity()V

    .line 96
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v2, 0x7f030064

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->setContentView(I)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->customizeActionBar()V

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 47
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_3

    const-string v2, "intent_reassess_state"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 48
    const-string v2, "intent_reassess_state"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 49
    .local v0, "ReassessStatus":I
    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    .line 50
    const-string v2, "intent_cancel_goal_unaligned_message"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mCancelUnalignedGoalMessage:Ljava/lang/String;

    .line 51
    const-string v2, "intent_cancel_goal_unaligned_description"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mCancelUnalignedGoalDescription:Ljava/lang/String;

    .line 52
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->showReassessmentFragment(I)V

    .line 67
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 69
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->processBadgeData(Landroid/content/Intent;)V

    .line 72
    :cond_1
    return-void

    .line 54
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->finish()V

    goto :goto_0

    .line 59
    .end local v0    # "ReassessStatus":I
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->checkReassessStatus()I

    move-result v0

    .line 60
    .restart local v0    # "ReassessStatus":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 61
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->showReassessmentFragment(I)V

    goto :goto_0

    .line 62
    :cond_4
    if-nez v0, :cond_0

    .line 63
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->showReassessmentFragment(I)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 79
    return-void
.end method

.method public showReassessmentFragment(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    const v2, 0x7f0801e3

    .line 162
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mState:I

    if-ne v0, p1, :cond_0

    .line 204
    :goto_0
    return-void

    .line 164
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mState:I

    .line 166
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mState:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 168
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThan6Fragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentLessThan6Fragment;

    if-nez v0, :cond_1

    .line 169
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentLessThan6Fragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentLessThan6Fragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThan6Fragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentLessThan6Fragment;

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessLessThan6Fragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentLessThan6Fragment;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 175
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessGoalWarningFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessGoalWarningFragment;

    if-nez v0, :cond_2

    .line 176
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessGoalWarningFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessGoalWarningFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessGoalWarningFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessGoalWarningFragment;

    .line 179
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessGoalWarningFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessGoalWarningFragment;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 182
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessNowFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentNowFragment;

    if-nez v0, :cond_3

    .line 183
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentNowFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentNowFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessNowFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentNowFragment;

    .line 186
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessNowFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentNowFragment;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 189
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessCompletedFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentCompletedFragment;

    if-nez v0, :cond_4

    .line 190
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentCompletedFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentCompletedFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessCompletedFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentCompletedFragment;

    .line 193
    :cond_4
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->TAG:Ljava/lang/String;

    const-string v1, "REASSESSMENT_COMPLETED"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->mReassessCompletedFragment:Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentCompletedFragment;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    .line 166
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
