.class public Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "FirstTimeNoScoreFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mRootView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    return-void
.end method

.method private initListner(Landroid/view/View;)V
    .locals 11
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 48
    const v10, 0x7f08017a

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 49
    .local v1, "cignaNoScoreExerciseLayout":Landroid/widget/LinearLayout;
    const v10, 0x7f08017f

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 50
    .local v3, "cignaNoScoreFoodLayout":Landroid/widget/LinearLayout;
    const v10, 0x7f080184

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 51
    .local v5, "cignaNoScoreSleepLayout":Landroid/widget/LinearLayout;
    const v10, 0x7f080189

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 52
    .local v7, "cignaNoScoreStressLayout":Landroid/widget/LinearLayout;
    const v10, 0x7f08018e

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 54
    .local v9, "cignaNoScoreWeightLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    invoke-virtual {v3, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    invoke-virtual {v5, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    invoke-virtual {v7, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    invoke-virtual {v9, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const v10, 0x7f08017c

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 61
    .local v0, "cignaNoScoreExerciseIconBg":Landroid/widget/ImageView;
    const v10, 0x7f080181

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 62
    .local v2, "cignaNoScoreFoodIconBg":Landroid/widget/ImageView;
    const v10, 0x7f080186

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 63
    .local v4, "cignaNoScoreSleepIconBg":Landroid/widget/ImageView;
    const v10, 0x7f08018b

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 64
    .local v6, "cignaNoScoreStressIconBg":Landroid/widget/ImageView;
    const v10, 0x7f080190

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 66
    .local v8, "cignaNoScoreWeightIconBg":Landroid/widget/ImageView;
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    invoke-virtual {v4, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    invoke-virtual {v6, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    invoke-virtual {v8, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void
.end method

.method private initNoScoreMsssage(Landroid/view/View;)V
    .locals 3
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 74
    const v1, 0x7f080192

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 76
    .local v0, "cignaFirstTimeNoScoreTxt":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v1

    sget-object v2, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CORNER_SCORE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCoachesCornerMsg(Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 122
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 124
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 125
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 127
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->startActivity(Landroid/content/Intent;)V

    .line 131
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 82
    const/4 v0, 0x0

    .line 83
    .local v0, "category":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    const/4 v2, 0x0

    .line 85
    .local v2, "requestCode":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 115
    :goto_0
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 116
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "intent_lifestyle_category"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 117
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 118
    return-void

    .line 88
    .end local v1    # "intent":Landroid/content/Intent;
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 89
    const/16 v2, 0x64

    .line 90
    goto :goto_0

    .line 93
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 94
    const/16 v2, 0xc8

    .line 95
    goto :goto_0

    .line 98
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 99
    const/16 v2, 0x12c

    .line 100
    goto :goto_0

    .line 103
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 104
    const/16 v2, 0x190

    .line 105
    goto :goto_0

    .line 108
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 109
    const/16 v2, 0x1f4

    .line 110
    goto :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x7f08017a
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->setHasOptionsMenu(Z)V

    .line 33
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 135
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 137
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 138
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 139
    instance-of v1, v0, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;

    if-eqz v1, :cond_0

    .line 140
    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->isMenuShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 142
    const v1, 0x7f100006

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 150
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    const v0, 0x7f03004d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->mRootView:Landroid/view/View;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->initListner(Landroid/view/View;)V

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->initNoScoreMsssage(Landroid/view/View;)V

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;->mRootView:Landroid/view/View;

    return-object v0
.end method
