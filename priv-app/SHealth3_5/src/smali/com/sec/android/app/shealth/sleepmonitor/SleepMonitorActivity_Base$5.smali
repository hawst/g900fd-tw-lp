.class Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;
.super Landroid/os/Handler;
.source "SleepMonitorActivity_Base.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x1

    .line 224
    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 225
    .local v1, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    .line 242
    .local v4, "transaction":Landroid/support/v4/app/FragmentTransaction;
    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    invoke-static {v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$200(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v5

    instance-of v5, v5, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    if-eqz v5, :cond_2

    .line 243
    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z
    invoke-static {v5, v8}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$302(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;Z)Z

    .line 244
    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;
    invoke-static {v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 245
    .local v2, "graphFragment":Landroid/support/v4/app/Fragment;
    if-nez v2, :cond_0

    .line 247
    const v5, 0x7f080091

    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;
    invoke-static {v6}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;
    invoke-static {v7}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
    invoke-static {v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 270
    .end local v2    # "graphFragment":Landroid/support/v4/app/Fragment;
    :cond_1
    :goto_1
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 271
    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->invalidateOptionsMenu()V

    .line 273
    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # invokes: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$600(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 275
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 276
    return-void

    .line 250
    .restart local v2    # "graphFragment":Landroid/support/v4/app/Fragment;
    :catch_0
    move-exception v0

    .line 251
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 256
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    .end local v2    # "graphFragment":Landroid/support/v4/app/Fragment;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    invoke-static {v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$200(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v5

    instance-of v5, v5, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    if-eqz v5, :cond_1

    .line 257
    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    const/4 v6, 0x0

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$302(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;Z)Z

    .line 258
    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
    invoke-static {v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .line 259
    .local v3, "summaryFragment":Landroid/support/v4/app/Fragment;
    if-nez v3, :cond_3

    .line 261
    const v5, 0x7f080091

    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
    invoke-static {v6}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
    invoke-static {v7}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 268
    :cond_3
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;
    invoke-static {v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1

    .line 264
    :catch_1
    move-exception v0

    .line 265
    .restart local v0    # "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_2
.end method
