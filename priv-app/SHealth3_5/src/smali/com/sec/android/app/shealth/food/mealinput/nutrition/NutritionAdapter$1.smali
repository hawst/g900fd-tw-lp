.class Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;
.super Ljava/lang/Object;
.source "NutritionAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->initDropDownButton(Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

.field final synthetic val$itemView:Landroid/view/View;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->val$itemView:Landroid/view/View;

    iput p3, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->val$itemView:Landroid/view/View;

    const v2, 0x7f08042a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 93
    .local v0, "dropDownContainer":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->toggleDropdownState(Landroid/view/View;Landroid/view/View;ZI)V
    invoke-static {v1, p1, v0, v4, v4}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->access$000(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;Landroid/view/View;Landroid/view/View;ZI)V

    .line 102
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mLastSelectedDropDownIcon:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->access$100(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mLastSelectedDropDownContainer:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->access$200(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mLastSelectedDropDownIcon:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->access$100(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mLastSelectedDropDownContainer:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->access$200(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;)Landroid/view/View;

    move-result-object v3

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->toggleDropdownState(Landroid/view/View;Landroid/view/View;ZI)V
    invoke-static {v1, v2, v3, v4, v4}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->access$000(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;Landroid/view/View;Landroid/view/View;ZI)V

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;->val$position:I

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->toggleDropdownState(Landroid/view/View;Landroid/view/View;ZI)V
    invoke-static {v1, p1, v0, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->access$000(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;Landroid/view/View;Landroid/view/View;ZI)V

    goto :goto_0
.end method
