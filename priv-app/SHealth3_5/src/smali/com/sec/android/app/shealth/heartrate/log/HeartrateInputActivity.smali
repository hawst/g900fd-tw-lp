.class public Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;
.source "HeartrateInputActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;
    }
.end annotation


# static fields
.field private static final CHOOSE_TAG:I = 0x93

.field private static final SHOW_TAG_DIALOG:I = 0x1

.field private static final SHOW_TAG_DIALOG_TIME:J = 0xc8L


# instance fields
.field private initTag:I

.field private mBpmLayout:Landroid/widget/LinearLayout;

.field private mBpmRange:Landroid/widget/TextView;

.field private mChangedIconId:I

.field private mChangedTagId:I

.field private mChangedTagName:Ljava/lang/String;

.field private mComment:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mId:J

.field mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

.field private mPref:Landroid/content/SharedPreferences;

.field private mTag:Landroid/widget/TextView;

.field private mTagBottomView:Landroid/view/View;

.field private mTagIcon:Landroid/widget/ImageView;

.field private mTagIconId:I

.field private mTagIconListener:Landroid/view/View$OnClickListener;

.field private mTagSelectIcon:Landroid/widget/ImageView;

.field private mTagText:Ljava/lang/String;

.field private mTagTextId:I

.field private mtagLayout:Landroid/widget/LinearLayout;

.field private tagName:Ljava/lang/String;

.field private view:Landroid/view/View;

.field private width:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;-><init>()V

    .line 81
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->initTag:I

    .line 83
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedIconId:I

    .line 84
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;

    .line 87
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->width:[I

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    .line 152
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$2;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mHandler:Landroid/os/Handler;

    .line 385
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$4;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagIconListener:Landroid/view/View$OnClickListener;

    .line 490
    return-void

    .line 87
    :array_0
    .array-data 4
        0x57
        0x41
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->startTagChooser()V

    return-void
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagIconId:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->launchMoreTagActivity()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagIconListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedIconId:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedIconId:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    return p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagText:Ljava/lang/String;

    return-object p1
.end method

.method private isTagInfoChanged()Z
    .locals 2

    .prologue
    .line 296
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->initTag:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    .line 297
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchMoreTagActivity()V
    .locals 2

    .prologue
    .line 474
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 475
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x94

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 476
    return-void
.end method

.method private startTagChooser()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090c0d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 308
    .local v2, "share":Ljava/lang/String;
    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    if-eqz v3, :cond_2

    .line 309
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 313
    :goto_0
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)V

    .line 377
    .local v0, "dialogItemClick":Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getApplicationContext()Landroid/content/Context;

    const-string v3, "input_method"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 378
    .local v1, "mgr":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 379
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v1, v3, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 380
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    if-nez v3, :cond_1

    .line 381
    new-instance v3, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f030145

    invoke-direct {v3, v4, p0, v5, v0}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentActivity;ILcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    .line 382
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    invoke-virtual {v3, v6, v2}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->showChooserDialog(ZLjava/lang/String;)V

    .line 383
    return-void

    .line 311
    .end local v0    # "dialogItemClick":Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;
    .end local v1    # "mgr":Landroid/view/inputmethod/InputMethodManager;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagText:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected getContentView()Landroid/view/View;
    .locals 12

    .prologue
    const v11, 0x7f080535

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 162
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 163
    .local v2, "model":Ljava/lang/String;
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x13

    if-gt v5, v6, :cond_0

    const-string v5, "SM-G9098"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "SM-G9092"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 166
    const v5, 0x7f0a05f0

    invoke-static {p0, v5}, Lcom/sec/android/app/shealth/heartrate/utils/SupportUtils;->setScrollContainerHeight(Landroid/app/Activity;I)V

    .line 168
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mPref:Landroid/content/SharedPreferences;

    .line 169
    const-string v5, ""

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagText:Ljava/lang/String;

    .line 170
    const-string v5, ""

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mComment:Ljava/lang/String;

    .line 171
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 172
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f03013e

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    .line 173
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    const v6, 0x7f08053e

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "HEART_RATE_ID_KEY"

    const-wide/16 v7, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mId:J

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "TAG_ICON_ID"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagIconId:I

    .line 176
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataById(Ljava/lang/String;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserDeviceId()Ljava/lang/String;

    move-result-object v4

    .line 180
    .local v4, "userDeviceId":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 181
    const-string v5, "10008"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 182
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 183
    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagIconId:I

    if-nez v5, :cond_1

    .line 184
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mId:J

    const/16 v8, 0x4e20

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->addTagToHeartRateLog(JI)V

    .line 191
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    const v6, 0x7f08053b

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mBpmLayout:Landroid/widget/LinearLayout;

    .line 192
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    const v6, 0x7f08053d

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mBpmRange:Landroid/widget/TextView;

    .line 194
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    const v6, 0x7f080540

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagSelectIcon:Landroid/widget/ImageView;

    .line 196
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    invoke-virtual {v5, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    .line 197
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagIconListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagSelectIcon:Landroid/widget/ImageView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 200
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 202
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "MODE_KEY"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mMode:Ljava/lang/String;

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "COMMENT_KEY"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mComment:Ljava/lang/String;

    .line 207
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    const v6, 0x7f080541

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagBottomView:Landroid/view/View;

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "TAG_TEXT_ID"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "TAG_TEXT"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagText:Ljava/lang/String;

    .line 213
    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    if-eqz v5, :cond_4

    .line 214
    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->initTag:I

    .line 215
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    if-eqz v5, :cond_2

    .line 217
    const v5, 0x7f080534

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;

    .line 219
    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    .line 227
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    const v6, 0x7f08053a

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "TIME_DATE_KEY"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    const v6, 0x7f08053c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "HEART_RATE_KEY"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f0900d2

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;-><init>()V

    .line 231
    .local v1, "mHeartrateData":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "HEART_RATE_KEY"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 232
    .local v0, "mHeartRate":I
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getMaleOrFemale()I

    move-result v5

    invoke-virtual {v1, v5, v9}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile(II)[I

    move-result-object v3

    .line 233
    .local v3, "percentile":[I
    sget-object v5, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v5, v6, :cond_6

    .line 235
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mBpmRange:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 251
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    return-object v5

    .line 187
    .end local v0    # "mHeartRate":I
    .end local v1    # "mHeartrateData":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    .end local v3    # "percentile":[I
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto/16 :goto_0

    .line 222
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagText:Ljava/lang/String;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagText:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_5

    .line 223
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 225
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    const v6, 0x7f090c0d

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 239
    .restart local v0    # "mHeartRate":I
    .restart local v1    # "mHeartrateData":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    .restart local v3    # "percentile":[I
    :cond_6
    aget v5, v3, v9

    if-lt v0, v5, :cond_7

    aget v5, v3, v10

    if-gt v0, v5, :cond_7

    .line 240
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mBpmRange:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 241
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mBpmRange:Landroid/widget/TextView;

    const v6, 0x7f090c21

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 246
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mBpmRange:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mBpmRange:Landroid/widget/TextView;

    const v6, 0x7f090c20

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2
.end method

.method protected isInputChanged()Z
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mComment:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->isTagInfoChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 395
    const-string v2, ""

    .line 396
    .local v2, "tag":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 471
    :cond_0
    :goto_0
    return-void

    .line 399
    :pswitch_0
    const/4 v3, 0x0

    .line 401
    .local v3, "tagItem":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mPref:Landroid/content/SharedPreferences;

    const-string v5, "from_more_tag"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 402
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mPref:Landroid/content/SharedPreferences;

    const-string v5, "has_more_default_tag_pref"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 403
    .local v1, "hasDefault":Z
    if-eqz v1, :cond_2

    .line 405
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v5, "more_tag_id"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I

    .line 406
    const-string v4, ""

    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;

    .line 414
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 415
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "from_more_tag"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 416
    const-string/jumbo v4, "more_tag_name"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 417
    const-string/jumbo v4, "more_tag_id"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 418
    const-string/jumbo v4, "more_tag_icon_id"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 419
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 422
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "hasDefault":Z
    :cond_1
    iget v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I

    invoke-static {v4, p0}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->updateRecentTagPreference(ILandroid/content/Context;)V

    .line 423
    const-string v4, ""

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 424
    iget v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I

    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v3

    .line 425
    if-eqz v3, :cond_0

    iget v4, v3, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    if-lez v4, :cond_0

    .line 426
    iget v4, v3, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    iput v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    .line 436
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    if-eqz v4, :cond_6

    .line 438
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    const v5, 0x7f08053e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;

    .line 441
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;

    const v5, 0x7f080535

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    .line 444
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 445
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 446
    iget v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    if-lez v4, :cond_4

    .line 447
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 410
    .restart local v1    # "hasDefault":Z
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v5, "more_tag_id"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I

    .line 411
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v5, "more_tag_name"

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;

    .line 412
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v5, "more_tag_icon_id"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedIconId:I

    goto/16 :goto_1

    .line 431
    .end local v1    # "hasDefault":Z
    :cond_3
    iput v7, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I

    .line 432
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagText:Ljava/lang/String;

    .line 433
    iget v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedIconId:I

    iput v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagIconId:I

    goto :goto_2

    .line 449
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;

    if-eqz v4, :cond_5

    .line 450
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 451
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 456
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;

    const v5, 0x7f090c0d

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 461
    :cond_6
    const-string v4, "InputActivity"

    const-string/jumbo v5, "view is null"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 396
    :pswitch_data_0
    .packed-switch 0x94
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onCreate(Landroid/os/Bundle;)V

    .line 97
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v4

    if-nez v4, :cond_0

    .line 99
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 100
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v4, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 102
    const/high16 v4, 0x4000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 103
    const/high16 v4, 0x20000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 104
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->startActivity(Landroid/content/Intent;)V

    .line 106
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-static {v4}, Landroid/os/Process;->killProcess(I)V

    .line 110
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "hrm_tag_chooser_dialog"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 111
    .local v0, "df":Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1

    .line 112
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 113
    const/4 v0, 0x0

    .line 115
    :cond_1
    const v4, 0x7f0803e8

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 116
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mComment:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 118
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setSelection(I)V

    .line 119
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    new-instance v5, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$1;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 131
    if-eqz p1, :cond_3

    .line 132
    const-string v4, "isTagListDialogVisible"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 133
    .local v2, "isDialogshown":Ljava/lang/Boolean;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 134
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x1

    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 136
    .end local v2    # "isDialogshown":Ljava/lang/Boolean;
    :cond_3
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 137
    .local v3, "model":Ljava/lang/String;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-gt v4, v5, :cond_5

    const-string v4, "SM-G9098"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "SM-G9092"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 148
    :cond_4
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->assistActivity(Landroid/app/Activity;)V

    .line 151
    :cond_5
    return-void
.end method

.method protected onSaveButtonSelect(Z)V
    .locals 12
    .param p1, "isAdd"    # Z

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 269
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/inputmethod/InputMethodManager;

    .line 270
    .local v8, "mgr":Landroid/view/inputmethod/InputMethodManager;
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {v8, v0, v11}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 273
    .local v7, "mCommString":Ljava/lang/String;
    const/4 v9, 0x1

    .line 274
    .local v9, "success":Z
    if-eqz v7, :cond_0

    .line 275
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->updateCommentById(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    .line 276
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->isTagInfoChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedIconId:I

    if-nez v0, :cond_4

    .line 278
    if-eqz v9, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget-wide v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mId:J

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->updateTagFromHeartRateLogById(JI)Z

    move-result v0

    if-eqz v0, :cond_3

    move v9, v10

    .line 281
    :goto_0
    const-string v0, "HeartRateInputACtivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DB UPDATED = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->isInputChanged()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 284
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090835

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v10}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 287
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 288
    .local v6, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v0, "save_to_logs"

    invoke-interface {v6, v0, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 289
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->finish()V

    .line 293
    return-void

    .end local v6    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_3
    move v9, v11

    .line 278
    goto :goto_0

    .line 280
    :cond_4
    if-eqz v9, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget-wide v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mId:J

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;

    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedIconId:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->updateTagFromHeartRateLogById(JILjava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    move v9, v10

    :goto_1
    goto :goto_0

    :cond_5
    move v9, v11

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 481
    const-string v1, "isTagListDialogVisible"

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 482
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 483
    return-void

    .line 481
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 257
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onStart()V

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getApplicationContext()Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 260
    .local v0, "mgr":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 263
    .end local v0    # "mgr":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method
