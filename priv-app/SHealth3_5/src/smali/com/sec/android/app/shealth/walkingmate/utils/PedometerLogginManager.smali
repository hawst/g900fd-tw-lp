.class public Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager;
.super Ljava/lang/Object;
.source "PedometerLogginManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager$1;,
        Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager$pedometerLoggingManagerHodler;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager$1;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager;-><init>()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager;->TAG:Ljava/lang/String;

    const-string v1, "getInstance"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager$pedometerLoggingManagerHodler;->sInstance:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerLogginManager;

    return-object v0
.end method
