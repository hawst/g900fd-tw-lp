.class public Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "SuggestedMissionsDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$1;,
        Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$SuggestedSpinnerAdapter;,
        Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$FrequencyOnItemSelectedListener;
    }
.end annotation


# static fields
.field private static final REQUEST_CODE_CALENDAR_START:I = 0x1001

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mFrequenceStrs:[Ljava/lang/String;

.field private mFrequencyIds:[I

.field private mGetDateTime:J

.field private mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

.field private mSelectedFrequencyID:I

.field private mSpinnerAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$SuggestedSpinnerAdapter;

.field private mSuggestedFrequenceSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;

.field private mSuggestedFrequenceSpinnerLayout:Landroid/widget/RelativeLayout;

.field private mSuggestedFrequencyTxt:Landroid/widget/TextView;

.field private mSuggestedFrequencyTxtLayout:Landroid/widget/RelativeLayout;

.field private startdateBtn:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    .line 310
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSelectedFrequencyID:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;)Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$SuggestedSpinnerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSpinnerAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$SuggestedSpinnerAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mFrequencyIds:[I

    return-object v0
.end method

.method private createFrequencyArry([Ljava/lang/String;II)V
    .locals 4
    .param p1, "orgStringArray"    # [Ljava/lang/String;
    .param p2, "frequencyMin"    # I
    .param p3, "frequencyMax"    # I

    .prologue
    .line 166
    sub-int v2, p3, p2

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mFrequenceStrs:[Ljava/lang/String;

    .line 167
    sub-int v2, p3, p2

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mFrequencyIds:[I

    .line 169
    const/4 v1, 0x0

    .line 170
    .local v1, "index":I
    add-int/lit8 v0, p2, -0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mFrequenceStrs:[Ljava/lang/String;

    aget-object v3, p1, v0

    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mFrequencyIds:[I

    add-int/lit8 v3, v0, 0x1

    aput v3, v2, v1

    .line 173
    add-int/lit8 v1, v1, 0x1

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175
    :cond_0
    return-void
.end method

.method private getMissionData()Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 322
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 323
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 324
    const-string v4, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 325
    .local v0, "goalId":I
    const-string v4, "EXTRA_NAME_MISSION_ID"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 327
    .local v3, "missionId":I
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->getAvailableMissionData(II)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 331
    .end local v0    # "goalId":I
    .end local v3    # "missionId":I
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getMissionDetailView()Landroid/view/View;
    .locals 6

    .prologue
    .line 103
    const v4, 0x7f03007f

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 105
    .local v1, "missionDetailView":Landroid/view/View;
    const v4, 0x7f080272

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 106
    .local v3, "topTitle":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getTitleString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    const v4, 0x7f080275

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequencyTxtLayout:Landroid/widget/RelativeLayout;

    .line 109
    const v4, 0x7f080273

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequenceSpinnerLayout:Landroid/widget/RelativeLayout;

    .line 110
    const v4, 0x7f080276

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequencyTxt:Landroid/widget/TextView;

    .line 111
    const v4, 0x7f080274

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequenceSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getFrequencyDefID()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSelectedFrequencyID:I

    .line 114
    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSelectedFrequencyID:I

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->updateSpinnerTxt(I)V

    .line 116
    const v4, 0x7f080278

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->startdateBtn:Landroid/widget/Button;

    .line 117
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->startdateBtn:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    const v4, 0x7f080261

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 120
    .local v0, "description":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getWhatToDo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    const v4, 0x7f080263

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 123
    .local v2, "rationale":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getWhyDoIt()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    return-object v1
.end method

.method private updateSpinnerTxt(I)V
    .locals 7
    .param p1, "selectIndex"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    .line 131
    .local v6, "frequencyStringArray":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getFrequencyDefID()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getFrequencyMin()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getFrequencyDefID()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getFrequencyMax()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequenceSpinnerLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequencyTxtLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequencyTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getFrequencyDefID()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v6, v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    :goto_0
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequenceSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;->setSelection(I)V

    .line 155
    :cond_0
    return-void

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequenceSpinnerLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequencyTxtLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getFrequencyMin()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getFrequencyMax()I

    move-result v1

    invoke-direct {p0, v6, v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->createFrequencyArry([Ljava/lang/String;II)V

    .line 146
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$SuggestedSpinnerAdapter;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequenceSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;

    const v4, 0x7f03007a

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mFrequenceStrs:[Ljava/lang/String;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$SuggestedSpinnerAdapter;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;I[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSpinnerAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$SuggestedSpinnerAdapter;

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequenceSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSpinnerAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$SuggestedSpinnerAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSuggestedFrequenceSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$FrequencyOnItemSelectedListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$FrequencyOnItemSelectedListener;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0
.end method

.method private updateStartBtnTxtUpdate(J)V
    .locals 4
    .param p1, "timeMills"    # J

    .prologue
    .line 291
    const v1, 0x7f080278

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 292
    .local v0, "startdateBtn":Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isToday(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    const v1, 0x7f09005d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 299
    :goto_0
    return-void

    .line 294
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isTomorrow(J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 295
    const v1, 0x7f090c53

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 297
    :cond_1
    iget-wide v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getMonthLen3DateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 159
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090c78

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 162
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 273
    if-eqz p3, :cond_0

    .line 274
    const-string/jumbo v0, "returnTime"

    const-wide/16 v1, 0x0

    invoke-virtual {p3, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    .line 276
    packed-switch p1, :pswitch_data_0

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 278
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 279
    iget-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->updateStartBtnTxtUpdate(J)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    iget-wide v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setStartDateTimeMillis(J)V

    goto :goto_0

    .line 276
    nop

    :pswitch_data_0
    .packed-switch 0x1001
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 179
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 181
    :sswitch_0
    iget-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 182
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    .line 185
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    invoke-direct {v6, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 190
    .local v6, "cal_intent":Landroid/content/Intent;
    const-string/jumbo v0, "period_type"

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 191
    const-string v0, "data_type"

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    const-string v0, "current_time"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    invoke-virtual {v6, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 193
    const-string v0, "backupStartTime"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    invoke-virtual {v6, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 194
    const-wide/32 v8, 0x5265c00

    .line 195
    .local v8, "oneDay":J
    const-string v0, "backupEndTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x6

    mul-long/2addr v3, v8

    add-long/2addr v1, v3

    invoke-virtual {v6, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 196
    const-string v0, "backupInputButtonType"

    const-string v1, "StartDatePicker"

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const/16 v0, 0x1001

    invoke-virtual {p0, v6, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 201
    .end local v6    # "cal_intent":Landroid/content/Intent;
    .end local v8    # "oneDay":J
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 204
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onBackPressed()V

    goto :goto_0

    .line 209
    :sswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 230
    :goto_1
    new-instance v7, Landroid/content/Intent;

    const-string v0, "com.sec.android.app.shealth.cignacoach.MISSION_ADD"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 231
    .local v7, "intent":Landroid/content/Intent;
    const-string v0, "EXTRA_NAME_EXPAND_GOAL_ID"

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v1

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 232
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->UpdateGoalStatus(ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 238
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getId()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSelectedFrequencyID:I

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getStartDateTimeMillis()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->setUserMission(IIILcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;J)V

    .line 241
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;->setGoalID(I)V

    .line 242
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/LastUserGoalMissionInfo;->setMissionID(I)V

    .line 244
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SuggestedMissionsDetailActivity goaID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " missionID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    new-instance v7, Landroid/content/Intent;

    .end local v7    # "intent":Landroid/content/Intent;
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {v7, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 247
    .restart local v7    # "intent":Landroid/content/Intent;
    const/high16 v0, 0x24000000

    invoke-virtual {v7, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 248
    const-string v0, "intent_need_cigna_main_update"

    const/4 v1, 0x1

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 249
    const-string v0, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 250
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 211
    .end local v7    # "intent":Landroid/content/Intent;
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.cignacoach"

    const-string v2, "0011"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 214
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.cignacoach"

    const-string v2, "0012"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 217
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.cignacoach"

    const-string v2, "0013"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 220
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.cignacoach"

    const-string v2, "0014"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 223
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.cignacoach"

    const-string v2, "0015"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 179
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080257 -> :sswitch_1
        0x7f080258 -> :sswitch_2
        0x7f080278 -> :sswitch_0
    .end sparse-switch

    .line 209
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const v6, 0x7f03007d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->setContentView(I)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 65
    .local v4, "intent":Landroid/content/Intent;
    if-eqz v4, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getMissionData()Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 69
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    if-nez v6, :cond_0

    .line 70
    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->TAG:Ljava/lang/String;

    const-string v7, "MissionData is null."

    invoke-static {v6, v7}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->finish()V

    .line 100
    :goto_0
    return-void

    .line 76
    :cond_0
    if-eqz p1, :cond_1

    .line 77
    const-string v6, "SELECTED_TIME"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    .line 82
    :goto_1
    const v6, 0x7f080255

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;

    .line 83
    .local v3, "headerView":Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;
    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;->setVisibility(I)V

    .line 85
    const v6, 0x7f080256

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 86
    .local v2, "contentsContainer":Landroid/widget/LinearLayout;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getMissionDetailView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 88
    const v6, 0x7f080257

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 89
    .local v0, "cancelBtn":Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    const v6, 0x7f080258

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 92
    .local v5, "okBtn":Landroid/widget/Button;
    const v6, 0x7f090c8e

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(I)V

    .line 93
    invoke-virtual {v5, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    const v6, 0x7f080271

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 96
    .local v1, "categoryIcon":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getCignaGoalIconIdByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 98
    iget-wide v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->updateStartBtnTxtUpdate(J)V

    .line 99
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    iget-wide v7, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setStartDateTimeMillis(J)V

    goto :goto_0

    .line 79
    .end local v0    # "cancelBtn":Landroid/widget/Button;
    .end local v1    # "categoryIcon":Landroid/widget/ImageView;
    .end local v2    # "contentsContainer":Landroid/widget/LinearLayout;
    .end local v3    # "headerView":Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;
    .end local v5    # "okBtn":Landroid/widget/Button;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 336
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 338
    const-string v0, "SELECTED_TIME"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mGetDateTime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 339
    return-void
.end method
