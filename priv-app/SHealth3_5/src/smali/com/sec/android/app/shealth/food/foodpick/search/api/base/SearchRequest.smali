.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;
.super Ljava/lang/Object;
.source "SearchRequest.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final REQUEST_ENCODE_STANDARD:Ljava/lang/String; = "utf-8"


# instance fields
.field private final mStringRequest:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->LOG_TAG:Ljava/lang/String;

    .line 92
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->mStringRequest:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "requestString"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->mStringRequest:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public getEncodedStringRequest()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    :try_start_0
    const-string/jumbo v1, "utf-8"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->getEncodedStringRequest(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 65
    :goto_0
    return-object v1

    .line 62
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->LOG_TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->mStringRequest:Ljava/lang/String;

    goto :goto_0
.end method

.method public getEncodedStringRequest(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "charSetName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->mStringRequest:Ljava/lang/String;

    invoke-static {v0, p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "+"

    const-string v2, "%20"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringRequest()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->mStringRequest:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->mStringRequest:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 90
    return-void
.end method
