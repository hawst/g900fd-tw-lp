.class Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;
.super Ljava/lang/Object;
.source "UpdateTermsOfUse.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 75
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->isAgreeCheck:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->access$100(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    const/4 v0, 0x0

    .line 79
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mPrivacyPolicyValue:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->access$200(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 80
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 81
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "termOfUseVersion"

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mTermsOfUseValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->access$300(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 82
    const-string/jumbo v1, "privacyVersion"

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mPrivacyPolicyValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->access$200(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->startActivity(Landroid/content/Intent;)V

    .line 94
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 85
    .restart local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 86
    .restart local v0    # "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->startActivity(Landroid/content/Intent;)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mTermsOfUseValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->access$300(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)F

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setTermsOfUseVersionCode(Landroid/content/Context;F)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->finish()V

    goto :goto_0

    .line 92
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    const v3, 0x7f0908c3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
