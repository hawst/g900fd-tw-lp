.class public Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "FavoritesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$MyOnCheckedChildChangeListener;,
        Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;
    }
.end annotation


# instance fields
.field private mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

.field private mChildList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mGroupList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p2, "groupList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p3, "childList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;>;"
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 30
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;->NORMAL:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mContext:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mGroupList:Ljava/util/ArrayList;

    .line 35
    iput-object p3, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mChildList:Ljava/util/ArrayList;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;)Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    return-object v0
.end method

.method private allChildItemCheck(Z)V
    .locals 5
    .param p1, "check"    # Z

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getGroupCount()I

    move-result v4

    .line 178
    .local v4, "groupCount":I
    const/4 v3, 0x0

    .local v3, "group":I
    :goto_0
    if-ge v3, v4, :cond_1

    .line 180
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getChildrenCount(I)I

    move-result v1

    .line 181
    .local v1, "childCount":I
    const/4 v0, 0x0

    .local v0, "child":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 182
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getChild(II)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v2

    .line 183
    .local v2, "childData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setChecked(Z)V

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 178
    .end local v2    # "childData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 186
    .end local v0    # "child":I
    .end local v1    # "childCount":I
    :cond_1
    return-void
.end method

.method private isEndPositionOfGourp(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 159
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getChildrenCount(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_0

    .line 160
    const/4 v0, 0x1

    .line 163
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCheckedItemCount()I
    .locals 7

    .prologue
    .line 189
    const/4 v0, 0x0

    .line 190
    .local v0, "checkedCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getGroupCount()I

    move-result v3

    .line 202
    .local v3, "groupCount":I
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mChildList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 203
    .local v2, "childList1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 204
    .local v1, "child":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 205
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    .end local v1    # "child":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    .end local v2    # "childList1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_2
    return v0
.end method

.method public getChild(II)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mChildList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mChildList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    goto :goto_0
.end method

.method public bridge synthetic getChild(II)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # I

    .prologue
    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getChild(II)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 53
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getChild(II)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v1

    .line 61
    .local v1, "childData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    if-nez p4, :cond_0

    .line 63
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mContext:Landroid/content/Context;

    const v7, 0x7f030058

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 66
    :cond_0
    const v6, 0x7f0801c6

    invoke-virtual {p4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 67
    .local v3, "iconView":Landroid/widget/ImageView;
    const v6, 0x7f0801c7

    invoke-virtual {p4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 68
    .local v5, "titleView":Landroid/widget/TextView;
    const v6, 0x7f0801c8

    invoke-virtual {p4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 69
    .local v4, "subInformationView":Landroid/widget/TextView;
    const v6, 0x7f080162

    invoke-virtual {p4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 70
    .local v0, "checkBox":Landroid/widget/CheckBox;
    const v6, 0x7f080129

    invoke-virtual {p4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 72
    .local v2, "divider":Landroid/view/View;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getCategory()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 73
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getCategoryId()I

    move-result v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryImageResource;->getLibraryCategoryImageResource(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 78
    :goto_0
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getTitle()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 79
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getSubCategory()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 83
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getSubCategory()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    :goto_1
    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 90
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    sget-object v7, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;->DELETE:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    if-ne v6, v7, :cond_2

    .line 91
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 92
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 93
    new-instance v6, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$MyOnCheckedChildChangeListener;

    invoke-direct {v6, p0, v1, p1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$MyOnCheckedChildChangeListener;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;I)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 95
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 96
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 102
    :cond_2
    :goto_2
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->isEndPositionOfGourp(II)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 103
    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 108
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getGroupCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ne p1, v6, :cond_3

    .line 109
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 111
    :cond_3
    return-object p4

    .line 75
    :cond_4
    const v6, 0x7f02020b

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 86
    :cond_5
    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 98
    :cond_6
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2

    .line 105
    :cond_7
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method public getChildrenCount(I)I
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mChildList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mChildList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mChildList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic getGroup(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getGroup(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGroup(I)Ljava/lang/String;
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mGroupList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mGroupList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mGroupList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mGroupList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mGroupList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 131
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 136
    if-nez p3, :cond_0

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f030057

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 138
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, Landroid/view/View;->setClickable(Z)V

    .line 141
    :cond_0
    const v1, 0x7f0801c5

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 142
    .local v0, "title":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mGroupList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    return-object p3
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 154
    const/4 v0, 0x1

    return v0
.end method

.method public setOnCheckedCountListener(Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    .line 211
    return-void
.end method

.method public setSelectAll()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->allChildItemCheck(Z)V

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->notifyDataSetChanged()V

    .line 169
    return-void
.end method

.method public setType(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;)V
    .locals 0
    .param p1, "type"    # Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    .line 40
    return-void
.end method

.method public setUnSelectAll()V
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->allChildItemCheck(Z)V

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->notifyDataSetChanged()V

    .line 174
    return-void
.end method
