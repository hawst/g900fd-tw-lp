.class public Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;
.super Ljava/lang/Object;
.source "FileCopyUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;
    }
.end annotation


# static fields
.field public static final ONE_STEP_SIZE:J = 0x1000L

.field private static TAG:Ljava/lang/String;

.field public static fileSizeSum:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->TAG:Ljava/lang/String;

    .line 18
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->fileSizeSum:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method private static backupFile(Ljava/io/File;Ljava/io/File;)V
    .locals 10
    .param p0, "from"    # Ljava/io/File;
    .param p1, "to"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 92
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " Copying Directory  src= "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 95
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    .line 97
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    .line 99
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    .line 118
    :cond_1
    :goto_0
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 119
    .local v3, "fileList":[Ljava/io/File;
    if-eqz v3, :cond_6

    .line 121
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_6

    aget-object v2, v0, v4

    .line 125
    .local v2, "file":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, p1, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 126
    .local v1, "childTo":Ljava/io/File;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->backupFile(Ljava/io/File;Ljava/io/File;)V

    .line 121
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 104
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "childTo":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fileList":[Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_2
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_old"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 105
    .local v6, "old":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 107
    invoke-static {v6}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->deleteRecursive(Ljava/io/File;)V

    .line 109
    :cond_3
    invoke-virtual {p1, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 111
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": Couldn\'t rename directory "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 113
    :cond_4
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 115
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    goto :goto_0

    .line 133
    .end local v6    # "old":Ljava/io/File;
    :cond_5
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_7

    .line 135
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file \"from\" doest not exist: from: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; to: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_6
    :goto_2
    return-void

    .line 139
    :cond_7
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    if-eqz v7, :cond_8

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_8

    .line 141
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    .line 145
    :cond_8
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 147
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_old"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 148
    .restart local v6    # "old":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 150
    invoke-static {v6}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->deleteRecursive(Ljava/io/File;)V

    .line 152
    :cond_9
    invoke-virtual {p1, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 155
    .end local v6    # "old":Ljava/io/File;
    :cond_a
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->copyFileStream(Ljava/io/File;Ljava/io/File;)V

    goto :goto_2
.end method

.method private static backupFile(Ljava/io/FileInputStream;Ljava/io/FileOutputStream;)V
    .locals 14
    .param p0, "from"    # Ljava/io/FileInputStream;
    .param p1, "to"    # Ljava/io/FileOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x1000

    .line 41
    :try_start_0
    invoke-virtual {p0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 42
    .local v0, "fromChannel":Ljava/nio/channels/FileChannel;
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v5

    .line 43
    .local v5, "toChannel":Ljava/nio/channels/FileChannel;
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v9

    .line 44
    .local v9, "sizeFull":J
    const-wide/16 v1, 0x0

    .local v1, "sizeCurrent":J
    const-wide/16 v6, 0x0

    .line 46
    .local v6, "bytesTransferred":J
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-nez v3, :cond_3

    cmp-long v3, v1, v9

    if-gez v3, :cond_3

    .line 48
    add-long v3, v1, v12

    cmp-long v3, v3, v9

    if-gez v3, :cond_2

    .line 50
    const-wide/16 v3, 0x1000

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    move-result-wide v6

    .line 51
    add-long/2addr v1, v12

    .line 58
    :goto_1
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Bytes transferred: "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63
    .end local v0    # "fromChannel":Ljava/nio/channels/FileChannel;
    .end local v1    # "sizeCurrent":J
    .end local v5    # "toChannel":Ljava/nio/channels/FileChannel;
    .end local v6    # "bytesTransferred":J
    .end local v9    # "sizeFull":J
    :catch_0
    move-exception v8

    .line 65
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    if-eqz p0, :cond_0

    .line 70
    invoke-virtual {p0}, Ljava/io/FileInputStream;->close()V

    .line 71
    :cond_0
    if-eqz p1, :cond_1

    .line 72
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V

    .line 74
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    return-void

    .line 55
    .restart local v0    # "fromChannel":Ljava/nio/channels/FileChannel;
    .restart local v1    # "sizeCurrent":J
    .restart local v5    # "toChannel":Ljava/nio/channels/FileChannel;
    .restart local v6    # "bytesTransferred":J
    .restart local v9    # "sizeFull":J
    :cond_2
    sub-long v3, v9, v1

    :try_start_2
    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    move-result-wide v6

    .line 56
    move-wide v1, v9

    goto :goto_1

    .line 61
    :cond_3
    sget-wide v3, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->fileSizeSum:J

    add-long/2addr v3, v1

    sput-wide v3, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->fileSizeSum:J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 69
    if-eqz p0, :cond_4

    .line 70
    invoke-virtual {p0}, Ljava/io/FileInputStream;->close()V

    .line 71
    :cond_4
    if-eqz p1, :cond_1

    .line 72
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V

    goto :goto_2

    .line 69
    .end local v0    # "fromChannel":Ljava/nio/channels/FileChannel;
    .end local v1    # "sizeCurrent":J
    .end local v5    # "toChannel":Ljava/nio/channels/FileChannel;
    .end local v6    # "bytesTransferred":J
    .end local v9    # "sizeFull":J
    :catchall_0
    move-exception v3

    if-eqz p0, :cond_5

    .line 70
    invoke-virtual {p0}, Ljava/io/FileInputStream;->close()V

    .line 71
    :cond_5
    if-eqz p1, :cond_6

    .line 72
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V

    :cond_6
    throw v3
.end method

.method public static backupFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "from"    # Ljava/lang/String;
    .param p1, "to"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->backupFile(Ljava/io/File;Ljava/io/File;)V

    .line 79
    return-void
.end method

.method private static copyFileStream(Ljava/io/File;Ljava/io/File;)V
    .locals 8
    .param p0, "from"    # Ljava/io/File;
    .param p1, "to"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Copying File from = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    const/4 v1, 0x0

    .line 168
    .local v1, "fromStream":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 171
    .local v3, "toStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    .end local v1    # "fromStream":Ljava/io/FileInputStream;
    .local v2, "fromStream":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 173
    .end local v3    # "toStream":Ljava/io/FileOutputStream;
    .local v4, "toStream":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->backupFile(Ljava/io/FileInputStream;Ljava/io/FileOutputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 181
    if-eqz v2, :cond_0

    .line 182
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 183
    :cond_0
    if-eqz v4, :cond_5

    .line 184
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    move-object v3, v4

    .end local v4    # "toStream":Ljava/io/FileOutputStream;
    .restart local v3    # "toStream":Ljava/io/FileOutputStream;
    move-object v1, v2

    .line 186
    .end local v2    # "fromStream":Ljava/io/FileInputStream;
    .restart local v1    # "fromStream":Ljava/io/FileInputStream;
    :cond_1
    :goto_0
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 181
    if-eqz v1, :cond_2

    .line 182
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 183
    :cond_2
    if-eqz v3, :cond_1

    .line 184
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    goto :goto_0

    .line 181
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v1, :cond_3

    .line 182
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 183
    :cond_3
    if-eqz v3, :cond_4

    .line 184
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    :cond_4
    throw v5

    .line 181
    .end local v1    # "fromStream":Ljava/io/FileInputStream;
    .restart local v2    # "fromStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "fromStream":Ljava/io/FileInputStream;
    .restart local v1    # "fromStream":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v1    # "fromStream":Ljava/io/FileInputStream;
    .end local v3    # "toStream":Ljava/io/FileOutputStream;
    .restart local v2    # "fromStream":Ljava/io/FileInputStream;
    .restart local v4    # "toStream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v5

    move-object v3, v4

    .end local v4    # "toStream":Ljava/io/FileOutputStream;
    .restart local v3    # "toStream":Ljava/io/FileOutputStream;
    move-object v1, v2

    .end local v2    # "fromStream":Ljava/io/FileInputStream;
    .restart local v1    # "fromStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 175
    .end local v1    # "fromStream":Ljava/io/FileInputStream;
    .restart local v2    # "fromStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "fromStream":Ljava/io/FileInputStream;
    .restart local v1    # "fromStream":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v1    # "fromStream":Ljava/io/FileInputStream;
    .end local v3    # "toStream":Ljava/io/FileOutputStream;
    .restart local v2    # "fromStream":Ljava/io/FileInputStream;
    .restart local v4    # "toStream":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v0

    move-object v3, v4

    .end local v4    # "toStream":Ljava/io/FileOutputStream;
    .restart local v3    # "toStream":Ljava/io/FileOutputStream;
    move-object v1, v2

    .end local v2    # "fromStream":Ljava/io/FileInputStream;
    .restart local v1    # "fromStream":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v1    # "fromStream":Ljava/io/FileInputStream;
    .end local v3    # "toStream":Ljava/io/FileOutputStream;
    .restart local v2    # "fromStream":Ljava/io/FileInputStream;
    .restart local v4    # "toStream":Ljava/io/FileOutputStream;
    :cond_5
    move-object v3, v4

    .end local v4    # "toStream":Ljava/io/FileOutputStream;
    .restart local v3    # "toStream":Ljava/io/FileOutputStream;
    move-object v1, v2

    .end local v2    # "fromStream":Ljava/io/FileInputStream;
    .restart local v1    # "fromStream":Ljava/io/FileInputStream;
    goto :goto_0
.end method

.method public static deleteRecursive(Ljava/io/File;)V
    .locals 8
    .param p0, "fileOrDirectory"    # Ljava/io/File;

    .prologue
    .line 194
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 196
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 197
    .local v2, "children":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 199
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 200
    .local v1, "child":Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->deleteRecursive(Ljava/io/File;)V

    .line 199
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 203
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "child":Ljava/io/File;
    .end local v2    # "children":[Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v5

    if-nez v5, :cond_1

    .line 205
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Couldn\'t delete file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :cond_1
    return-void
.end method
