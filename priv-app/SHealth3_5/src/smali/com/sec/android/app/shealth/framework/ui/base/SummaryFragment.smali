.class public abstract Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/shealth/framework/ui/common/IDateChangeCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$TimeChangeReceiver;
    }
.end annotation


# static fields
.field private static REQUEST_CODE_CALENDAR:I


# instance fields
.field private isCalendarCancelClicked:Z

.field private mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

.field private mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

.field private mTimeChangedReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$TimeChangeReceiver;

.field protected summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

.field private swipeDetector:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;

.field private view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x64

    sput v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->REQUEST_CODE_CALENDAR:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->isCalendarCancelClicked:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    sget v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->REQUEST_CODE_CALENDAR:I

    return v0
.end method

.method private getSwipeDetector()Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->swipeDetector:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;-><init>(Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->swipeDetector:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->swipeDetector:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;

    return-object v0
.end method

.method private registerTimeChangeReceiver()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mTimeChangedReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$TimeChangeReceiver;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$TimeChangeReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$TimeChangeReceiver;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mTimeChangedReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$TimeChangeReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mTimeChangedReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$TimeChangeReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method private unregisterTimeChangeReceiver()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mTimeChangedReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$TimeChangeReceiver;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mTimeChangedReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$TimeChangeReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mTimeChangedReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$TimeChangeReceiver;

    :cond_0
    return-void
.end method


# virtual methods
.method protected getCalendarActivityClass()Ljava/lang/Class;
    .locals 1

    const-class v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    return-object v0
.end method

.method protected abstract getColumnNameForTime()Ljava/lang/String;
.end method

.method protected abstract getContentURI()Landroid/net/Uri;
.end method

.method protected abstract getContentView(Landroid/content/Context;)Landroid/view/View;
.end method

.method protected getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    return-object v0
.end method

.method public getSelectedDate()Ljava/util/Date;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->REQUEST_CODE_CALENDAR:I

    if-ne v0, p1, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "calendar_result"

    const-wide/16 v1, -0x1

    invoke-virtual {p3, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDateChanged(Ljava/util/Date;)V

    :cond_0
    sget v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->REQUEST_CODE_CALENDAR:I

    if-ne v0, p1, :cond_1

    if-nez p3, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->isCalendarCancelClicked:Z

    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setPrevSelectedDate(Ljava/util/Date;)V

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getCalendarActivityClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "period_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getContentURI()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "data_type"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getContentURI()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getColumnNameForTime()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v1, "column_name"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getColumnNameForTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    const-string v1, "current_time"

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->REQUEST_CODE_CALENDAR:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->summary_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->view:Landroid/view/View;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->registerTimeChangeReceiver()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->view:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->date_selector:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->view:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->date_selector:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->registerDateChangeListener(Lcom/sec/android/app/shealth/framework/ui/common/IDateChangeCallback;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->view:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->summary_frag_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->setGesturesEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getContentView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->date_switcher:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->setDateSelectorDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->setDateSelectorDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setDateFormat(Ljava/text/SimpleDateFormat;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getSwipeDetector()Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->addObserver(Lcom/sec/android/app/shealth/framework/ui/gesture/GestureObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->view:Landroid/view/View;

    return-object v0
.end method

.method public onDateChanged(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->refreshFragmentFocusables()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->unregisterTimeChangeReceiver()V

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->isCalendarCancelClicked:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->isCalendarCancelClicked:Z

    goto :goto_0
.end method

.method protected abstract onSytemDateChanged()V
.end method

.method protected setDateSelectorDateFormat()Ljava/text/SimpleDateFormat;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract setNextAndPrevDates()V
.end method
