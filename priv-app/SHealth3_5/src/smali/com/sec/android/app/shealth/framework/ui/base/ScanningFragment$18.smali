.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getView(Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;Z)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

.field final synthetic val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

.field final synthetic val$deviceID:Ljava/lang/String;

.field final synthetic val$item:Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$item:Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;

    iput-object p4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$deviceID:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 15

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isFromPairedDB()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isConnected()Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, 0x2

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->do_you_want_t0_connect_q:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$item:Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;->deviceName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v0

    const/16 v2, 0x2718

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->checkIsHrmSwitchReq:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5102(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->connected_device_exist_msg:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->connect_accessory:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHRM:Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5202(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;)Ljava/lang/String;

    :cond_1
    :goto_0
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;)V

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;)V

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const/4 v0, 0x7

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/16 v10, -0x6e

    aput v10, v1, v9

    const/16 v9, -0x60

    aput v9, v1, v8

    const/16 v8, -0x54f7

    aput v8, v1, v7

    const/16 v7, -0x3b

    aput v7, v1, v6

    const/16 v6, -0x32f4

    aput v6, v1, v5

    const/16 v5, -0x5e

    aput v5, v1, v2

    const/16 v2, -0x73c5

    aput v2, v1, v0

    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/16 v11, -0x1a

    aput v11, v0, v10

    const/16 v10, -0x3d

    aput v10, v0, v9

    const/16 v9, -0x5494

    aput v9, v0, v8

    const/16 v8, -0x55

    aput v8, v0, v7

    const/16 v7, -0x329e

    aput v7, v0, v6

    const/16 v6, -0x33

    aput v6, v0, v5

    const/16 v5, -0x73a8

    aput v5, v0, v2

    const/4 v2, 0x0

    :goto_1
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_2
    array-length v5, v0

    if-lt v2, v5, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_3
    return-void

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->disconnect_accessory:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->s_will_be_disconnected:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->disconnect:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x1d

    aput v13, v1, v12

    const/16 v12, -0x5f

    aput v12, v1, v11

    const/16 v11, -0x48

    aput v11, v1, v10

    const/16 v10, -0x3997

    aput v10, v1, v9

    const/16 v9, -0x58

    aput v9, v1, v8

    const/16 v8, -0x22a4

    aput v8, v1, v7

    const/16 v7, -0x42

    aput v7, v1, v6

    const/4 v6, -0x5

    aput v6, v1, v5

    const/16 v5, -0x2d95

    aput v5, v1, v2

    const/16 v2, -0x4a

    aput v2, v1, v0

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x69

    aput v14, v0, v13

    const/16 v13, -0x3e

    aput v13, v0, v12

    const/16 v12, -0x23

    aput v12, v0, v11

    const/16 v11, -0x39f9

    aput v11, v0, v10

    const/16 v10, -0x3a

    aput v10, v0, v9

    const/16 v9, -0x22cd

    aput v9, v0, v8

    const/16 v8, -0x23

    aput v8, v0, v7

    const/16 v7, -0x78

    aput v7, v0, v6

    const/16 v6, -0x2dfe

    aput v6, v0, v5

    const/16 v5, -0x2e

    aput v5, v0, v2

    const/4 v2, 0x0

    :goto_4
    array-length v5, v0

    if-lt v2, v5, :cond_5

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_5
    array-length v5, v0

    if-lt v2, v5, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_5
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isConnected()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->checkIsHrmSwitchReq:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5102(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, 0x2

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->do_you_want_t0_connect_q:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$item:Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;->deviceName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v0

    const/16 v2, 0x2718

    if-ne v0, v2, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-lez v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v4

    if-ne v3, v4, :cond_8

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->checkIsHrmSwitchReq:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5102(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->connected_device_exist_msg:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->connect_accessory:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHRM:Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5202(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;)Ljava/lang/String;

    :cond_9
    :goto_6
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;)V

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$5;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;)V

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const/4 v0, 0x7

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/16 v10, -0x77bb

    aput v10, v1, v9

    const/16 v9, -0x15

    aput v9, v1, v8

    const/16 v8, -0x35

    aput v8, v1, v7

    const/16 v7, -0x1a

    aput v7, v1, v6

    const/16 v6, -0x6f

    aput v6, v1, v5

    const/16 v5, -0x64

    aput v5, v1, v2

    const/16 v2, -0x47c4

    aput v2, v1, v0

    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/16 v11, -0x77cf

    aput v11, v0, v10

    const/16 v10, -0x78

    aput v10, v0, v9

    const/16 v9, -0x52

    aput v9, v0, v8

    const/16 v8, -0x78

    aput v8, v0, v7

    const/4 v7, -0x1

    aput v7, v0, v6

    const/16 v6, -0xd

    aput v6, v0, v5

    const/16 v5, -0x47a1

    aput v5, v0, v2

    const/4 v2, 0x0

    :goto_7
    array-length v5, v0

    if-lt v2, v5, :cond_a

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_8
    array-length v5, v0

    if-lt v2, v5, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_a
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_b
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_c
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->disconnect_accessory:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->s_will_be_disconnected:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->disconnect:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$6;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x50

    aput v13, v1, v12

    const/16 v12, -0x12

    aput v12, v1, v11

    const/16 v11, -0x54f9

    aput v11, v1, v10

    const/16 v10, -0x3b

    aput v10, v1, v9

    const/16 v9, -0x76f4

    aput v9, v1, v8

    const/16 v8, -0x1a

    aput v8, v1, v7

    const/16 v7, -0x6c

    aput v7, v1, v6

    const/16 v6, -0x63ed

    aput v6, v1, v5

    const/16 v5, -0xb

    aput v5, v1, v2

    const/16 v2, -0x51

    aput v2, v1, v0

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x3c

    aput v14, v0, v13

    const/16 v13, -0x73

    aput v13, v0, v12

    const/16 v12, -0x549e

    aput v12, v0, v11

    const/16 v11, -0x55

    aput v11, v0, v10

    const/16 v10, -0x769e

    aput v10, v0, v9

    const/16 v9, -0x77

    aput v9, v0, v8

    const/16 v8, -0x9

    aput v8, v0, v7

    const/16 v7, -0x63a0

    aput v7, v0, v6

    const/16 v6, -0x64

    aput v6, v0, v5

    const/16 v5, -0x35

    aput v5, v0, v2

    const/4 v2, 0x0

    :goto_9
    array-length v5, v0

    if-lt v2, v5, :cond_d

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_a
    array-length v5, v0

    if-lt v2, v5, :cond_e

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_d
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    :cond_e
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :catch_0
    move-exception v0

    goto/16 :goto_3

    :catch_1
    move-exception v0

    goto/16 :goto_0

    :catch_2
    move-exception v0

    goto/16 :goto_6
.end method
