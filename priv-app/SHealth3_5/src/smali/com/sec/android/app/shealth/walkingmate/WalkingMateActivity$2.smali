.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;
.super Ljava/lang/Object;
.source "WalkingMateActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V
    .locals 0

    .prologue
    .line 586
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 589
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    .line 590
    .local v0, "curViewStepCount":I
    if-eq v0, p1, :cond_0

    .line 591
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.walkingmate"

    const-string v4, "W013"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    if-eqz v2, :cond_2

    .line 599
    if-nez p1, :cond_1

    .line 600
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    const/16 v3, 0x2719

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->saveViewCount(I)V

    .line 601
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_MOBILE:I
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$300()I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->saveViewStepCountToMainSettingDB(I)V

    .line 614
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateFromDB()V

    .line 616
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNeedDeviceSyncGuidePopup(Z)V

    .line 618
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->initCirclePercentValue()V

    .line 619
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->refreshgoal()V

    .line 620
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setNextAndPrevDates()V

    .line 622
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->invalidateOptionsMenu()V

    .line 623
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->onUpdateScreen()V

    .line 624
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->syncRequestByActivity()V

    .line 626
    return-void

    .line 603
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDeviceItemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Ljava/util/ArrayList;

    move-result-object v2

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    .line 604
    .local v1, "item":Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    iget v3, v1, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;->deviceConstant:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->saveViewCount(I)V

    .line 605
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_WEARABLE:I
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$500()I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->saveViewStepCountToMainSettingDB(I)V

    goto :goto_0

    .line 610
    .end local v1    # "item":Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDeviceItemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    .line 611
    .restart local v1    # "item":Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    iget v3, v1, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;->deviceConstant:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->saveViewCount(I)V

    .line 612
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_WEARABLE:I
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$500()I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->saveViewStepCountToMainSettingDB(I)V

    goto :goto_0
.end method
