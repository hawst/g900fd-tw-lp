.class public Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;
.super Ljava/lang/Object;
.source "ImagePagerHolder.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public imagePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;->imagePath:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;->imagePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    return-void
.end method
