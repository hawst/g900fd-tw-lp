.class public Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;
.super Landroid/widget/LinearLayout;
.source "HomeSpO2StateBar.java"


# static fields
.field private static FIRST_MAX_VALUE:D

.field private static FIRST_MIN_VALUE:D

.field private static LENGTH:I

.field private static SECOND_MAX_VALUE:D

.field private static SECOND_MIN_VALUE:D

.field private static SIZE_ICON:I

.field private static THIRD_MAX_VALUE:D

.field private static THIRD_MIN_VALUE:D


# instance fields
.field private ivPolygon:Landroid/widget/ImageView;

.field private llInfo:Landroid/widget/LinearLayout;

.field private tvAvg:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const/16 v0, 0x3a

    sput v0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->LENGTH:I

    .line 26
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->SIZE_ICON:I

    .line 28
    const-wide v0, 0x4051800000000000L    # 70.0

    sput-wide v0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->FIRST_MIN_VALUE:D

    .line 29
    const-wide v0, 0x4056800000000000L    # 90.0

    sput-wide v0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->FIRST_MAX_VALUE:D

    .line 31
    sget-wide v0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->FIRST_MAX_VALUE:D

    sput-wide v0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->SECOND_MIN_VALUE:D

    .line 32
    const-wide v0, 0x4057c00000000000L    # 95.0

    sput-wide v0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->SECOND_MAX_VALUE:D

    .line 34
    sget-wide v0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->SECOND_MAX_VALUE:D

    sput-wide v0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->THIRD_MIN_VALUE:D

    .line 35
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    sput-wide v0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->THIRD_MAX_VALUE:D

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const v0, 0x7f030133

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 43
    const v0, 0x7f080516

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->llInfo:Landroid/widget/LinearLayout;

    .line 44
    const v0, 0x7f080518

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->ivPolygon:Landroid/widget/ImageView;

    .line 45
    const v0, 0x7f080517

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->tvAvg:Landroid/widget/TextView;

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->setGrapghLegend()V

    .line 50
    return-void
.end method

.method private convertDptoPx(I)I
    .locals 5
    .param p1, "dp"    # I

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 79
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 80
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    int-to-float v3, p1

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, v3, v4

    .line 81
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method

.method public static getLeftMargin(D)D
    .locals 10
    .param p0, "value"    # D

    .prologue
    .line 61
    const-wide/16 v2, 0x0

    .line 63
    .local v2, "leftMargin":D
    sget-wide v4, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->FIRST_MIN_VALUE:D

    cmpl-double v4, p0, v4

    if-ltz v4, :cond_0

    sget-wide v4, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->FIRST_MAX_VALUE:D

    cmpg-double v4, p0, v4

    if-gez v4, :cond_0

    .line 64
    sget v4, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->LENGTH:I

    int-to-double v4, v4

    sget-wide v6, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->FIRST_MAX_VALUE:D

    sget-wide v8, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->FIRST_MIN_VALUE:D

    sub-double/2addr v6, v8

    div-double v0, v4, v6

    .line 65
    .local v0, "dValue":D
    sget-wide v4, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->FIRST_MIN_VALUE:D

    sub-double v4, p0, v4

    mul-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v4, v4

    int-to-double v2, v4

    .line 74
    :goto_0
    return-wide v2

    .line 66
    .end local v0    # "dValue":D
    :cond_0
    sget-wide v4, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->SECOND_MIN_VALUE:D

    cmpl-double v4, p0, v4

    if-ltz v4, :cond_1

    sget-wide v4, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->SECOND_MAX_VALUE:D

    cmpg-double v4, p0, v4

    if-gez v4, :cond_1

    .line 67
    sget v4, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->LENGTH:I

    int-to-double v4, v4

    sget-wide v6, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->SECOND_MAX_VALUE:D

    sget-wide v8, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->SECOND_MIN_VALUE:D

    sub-double/2addr v6, v8

    div-double v0, v4, v6

    .line 68
    .restart local v0    # "dValue":D
    sget-wide v4, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->SECOND_MIN_VALUE:D

    sub-double v4, p0, v4

    mul-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v4, v4

    sget v5, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->LENGTH:I

    add-int/2addr v4, v5

    int-to-double v2, v4

    .line 69
    goto :goto_0

    .line 70
    .end local v0    # "dValue":D
    :cond_1
    sget v4, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->LENGTH:I

    int-to-double v4, v4

    sget-wide v6, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->THIRD_MAX_VALUE:D

    sget-wide v8, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->THIRD_MIN_VALUE:D

    sub-double/2addr v6, v8

    div-double v0, v4, v6

    .line 71
    .restart local v0    # "dValue":D
    sget-wide v4, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->THIRD_MIN_VALUE:D

    sub-double v4, p0, v4

    mul-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v4, v4

    sget v5, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->LENGTH:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    int-to-double v2, v4

    goto :goto_0
.end method


# virtual methods
.method public changePolygonImage(I)V
    .locals 1
    .param p1, "res"    # I

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->ivPolygon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 92
    :cond_0
    return-void
.end method

.method public moveToPolygon(D)V
    .locals 5
    .param p1, "value"    # D

    .prologue
    const/4 v3, -0x2

    .line 53
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->getLeftMargin(D)D

    move-result-wide v0

    .line 54
    .local v0, "leftMargin":D
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 55
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v3, v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->convertDptoPx(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 56
    const/4 v3, 0x3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 57
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->llInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    return-void
.end method

.method public setGrapghLegend()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/high16 v5, 0x41800000    # 16.0f

    const v4, 0x7f08051d

    const v3, 0x7f08051a

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "70%"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    const v0, 0x7f08051b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 98
    const v0, 0x7f08051c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 99
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "100%"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    :goto_0
    return-void

    .line 101
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->mContext:Landroid/content/Context;

    const v2, 0x7f090a69

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 103
    const v0, 0x7f08051b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 104
    const v0, 0x7f08051c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 105
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->mContext:Landroid/content/Context;

    const v2, 0x7f090a6b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method public setGraphAverageVisibility(Z)V
    .locals 3
    .param p1, "isVisibility"    # Z

    .prologue
    const/4 v0, 0x0

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->tvAvg:Landroid/widget/TextView;

    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 87
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
