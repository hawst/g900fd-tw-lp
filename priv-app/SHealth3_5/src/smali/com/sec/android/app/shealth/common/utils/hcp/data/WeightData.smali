.class public Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
.super Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;
.source "WeightData.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivityMetabolicRate:F

.field private mBasalMetabolicRate:F

.field private mBodyMassIndex:F

.field private mBodyWater:F

.field private mBodyYear:F

.field private mBoneMass:F

.field private mFat:F

.field private mHeight:F

.field private mMuscleMass:F

.field private mSkeletalMuscle:F

.field private mVisceralFat:F

.field private mWeight:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 249
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(FJ)V
    .locals 1
    .param p1, "weight"    # F
    .param p2, "sampleTime"    # J

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 108
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;-><init>(J)V

    .line 24
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mFat:F

    .line 25
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mHeight:F

    .line 26
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mWeight:F

    .line 27
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyMassIndex:F

    .line 28
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBasalMetabolicRate:F

    .line 29
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mActivityMetabolicRate:F

    .line 30
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyYear:F

    .line 31
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyWater:F

    .line 32
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mVisceralFat:F

    .line 33
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mMuscleMass:F

    .line 34
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mSkeletalMuscle:F

    .line 35
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBoneMass:F

    .line 109
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->setWeight(F)V

    .line 110
    return-void
.end method

.method public constructor <init>(FLjava/lang/String;J)V
    .locals 0
    .param p1, "weight"    # F
    .param p2, "comment"    # Ljava/lang/String;
    .param p3, "sampleTime"    # J

    .prologue
    .line 95
    invoke-direct {p0, p1, p3, p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;-><init>(FJ)V

    .line 96
    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->setComment(Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;IJJJIFFFFFFFFFFFF)V
    .locals 2
    .param p1, "id"    # J
    .param p3, "userDeviceId"    # Ljava/lang/String;
    .param p4, "comment"    # Ljava/lang/String;
    .param p5, "inputSourceType"    # I
    .param p6, "sampleTime"    # J
    .param p8, "createTime"    # J
    .param p10, "updateTime"    # J
    .param p12, "timeZone"    # I
    .param p13, "fat"    # F
    .param p14, "height"    # F
    .param p15, "weight"    # F
    .param p16, "bmi"    # F
    .param p17, "bmr"    # F
    .param p18, "activityMetabolicRate"    # F
    .param p19, "bodyYear"    # F
    .param p20, "bodyWater"    # F
    .param p21, "visceralFat"    # F
    .param p22, "muscleMass"    # F
    .param p23, "skeletalMuscle"    # F
    .param p24, "boneMass"    # F

    .prologue
    .line 70
    invoke-direct/range {p0 .. p12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;-><init>(JLjava/lang/String;Ljava/lang/String;IJJJI)V

    .line 24
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mFat:F

    .line 25
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mHeight:F

    .line 26
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mWeight:F

    .line 27
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyMassIndex:F

    .line 28
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBasalMetabolicRate:F

    .line 29
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mActivityMetabolicRate:F

    .line 30
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyYear:F

    .line 31
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyWater:F

    .line 32
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mVisceralFat:F

    .line 33
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mMuscleMass:F

    .line 34
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mSkeletalMuscle:F

    .line 35
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBoneMass:F

    .line 71
    iput p13, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mFat:F

    .line 72
    move/from16 v0, p14

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mHeight:F

    .line 73
    move/from16 v0, p15

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mWeight:F

    .line 74
    move/from16 v0, p16

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyMassIndex:F

    .line 75
    move/from16 v0, p17

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBasalMetabolicRate:F

    .line 76
    move/from16 v0, p18

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mActivityMetabolicRate:F

    .line 77
    move/from16 v0, p19

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyYear:F

    .line 78
    move/from16 v0, p20

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyWater:F

    .line 79
    move/from16 v0, p21

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mVisceralFat:F

    .line 80
    move/from16 v0, p22

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mMuscleMass:F

    .line 81
    move/from16 v0, p23

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mSkeletalMuscle:F

    .line 82
    move/from16 v0, p24

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBoneMass:F

    .line 83
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;-><init>(Landroid/os/Parcel;)V

    .line 24
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mFat:F

    .line 25
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mHeight:F

    .line 26
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mWeight:F

    .line 27
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyMassIndex:F

    .line 28
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBasalMetabolicRate:F

    .line 29
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mActivityMetabolicRate:F

    .line 30
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyYear:F

    .line 31
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyWater:F

    .line 32
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mVisceralFat:F

    .line 33
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mMuscleMass:F

    .line 34
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mSkeletalMuscle:F

    .line 35
    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBoneMass:F

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mFat:F

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mHeight:F

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mWeight:F

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyMassIndex:F

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBasalMetabolicRate:F

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mActivityMetabolicRate:F

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyYear:F

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyWater:F

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mVisceralFat:F

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mMuscleMass:F

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mSkeletalMuscle:F

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBoneMass:F

    .line 131
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData$1;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public getActivityMetabolicRate()F
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mActivityMetabolicRate:F

    return v0
.end method

.method public getBasalMetabolicRate()F
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBasalMetabolicRate:F

    return v0
.end method

.method public getBodyFat()F
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mFat:F

    return v0
.end method

.method public getBodyMassIndex()F
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyMassIndex:F

    return v0
.end method

.method public getBodyWater()F
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyWater:F

    return v0
.end method

.method public getBodyYear()F
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyYear:F

    return v0
.end method

.method public getBoneMass()F
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBoneMass:F

    return v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mHeight:F

    return v0
.end method

.method public getMuscleMass()F
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mMuscleMass:F

    return v0
.end method

.method public getSkeletalMuscle()F
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mSkeletalMuscle:F

    return v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mWeight:F

    return v0
.end method

.method public getViscFat()F
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mVisceralFat:F

    return v0
.end method

.method public setBodyMassIndex(F)V
    .locals 0
    .param p1, "bmi"    # F

    .prologue
    .line 139
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mBodyMassIndex:F

    .line 140
    return-void
.end method

.method public setHeight(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 148
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mHeight:F

    .line 149
    return-void
.end method

.method public setWeight(F)V
    .locals 0
    .param p1, "weight"    # F

    .prologue
    .line 217
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->checkValue(F)V

    .line 218
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->mWeight:F

    .line 219
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 264
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 265
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBodyFat()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getHeight()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 267
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBodyMassIndex()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 269
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBasalMetabolicRate()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 270
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getActivityMetabolicRate()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBodyYear()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBodyWater()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getViscFat()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getMuscleMass()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSkeletalMuscle()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBoneMass()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 277
    return-void
.end method
