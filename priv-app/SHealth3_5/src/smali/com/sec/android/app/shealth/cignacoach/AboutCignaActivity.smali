.class public Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "AboutCignaActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected mAboutCigna:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090c60

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 51
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;->TAG:Ljava/lang/String;

    const-string v1, "LocaleChangeReceiver action onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    const v0, 0x7f030026

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;->setContentView(I)V

    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;->customizeActionBar()V

    .line 25
    const v0, 0x7f0800ba

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;->mAboutCigna:Landroid/widget/TextView;

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;->mAboutCigna:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getAboutMsg()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 27
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;->setIntent(Landroid/content/Intent;)V

    .line 39
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 44
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onRestart()V

    .line 45
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 31
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 33
    return-void
.end method
