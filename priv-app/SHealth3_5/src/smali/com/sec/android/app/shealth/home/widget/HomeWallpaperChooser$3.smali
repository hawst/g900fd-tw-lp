.class Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$3;
.super Ljava/lang/Object;
.source "HomeWallpaperChooser.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$3;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 251
    instance-of v2, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 252
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 254
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    if-nez v2, :cond_1

    .line 255
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$3;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->finish()V

    .line 264
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_0
    :goto_0
    return-void

    .line 256
    .restart local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 257
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 258
    .local v1, "data":Landroid/content/Intent;
    const-string/jumbo v2, "wallpaper_resource_id"

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$3;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    # getter for: Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->currentWallpaper:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->access$200(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 259
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$3;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->setResult(ILandroid/content/Intent;)V

    .line 260
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$3;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->finish()V

    goto :goto_0
.end method
