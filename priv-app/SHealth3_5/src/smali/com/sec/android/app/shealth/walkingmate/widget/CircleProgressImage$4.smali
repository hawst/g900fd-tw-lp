.class Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;
.super Ljava/lang/Object;
.source "CircleProgressImage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->setProgress(F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

.field final synthetic val$percent:F


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;F)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->val$percent:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 133
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->obj:Ljava/lang/Object;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$400(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 134
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->isExcuting:Z
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$500(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 135
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    const/4 v10, 0x0

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->isExcuting:Z
    invoke-static {v8, v10}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$502(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;Z)Z

    .line 137
    :cond_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWidth:I
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$700(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mHeight:I
    invoke-static {v10}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)I

    move-result v10

    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v10, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$602(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 140
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateCanvas:Landroid/graphics/Canvas;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$900(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Canvas;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateBitmap:Landroid/graphics/Bitmap;
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$600(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 141
    const/4 v1, 0x0

    .local v1, "degree":F
    :goto_0
    const/high16 v8, 0x43b40000    # 360.0f

    cmpg-float v8, v1, v8

    if-gez v8, :cond_5

    .line 142
    const/4 v5, 0x0

    .line 144
    .local v5, "radius":I
    :goto_1
    int-to-double v8, v5

    const-wide v10, 0x401921fb54442d18L    # 6.283185307179586

    float-to-double v12, v1

    mul-double/2addr v10, v12

    const-wide v12, 0x4076800000000000L    # 360.0

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v3, v8

    .line 145
    .local v3, "dy":I
    int-to-double v8, v5

    const-wide v10, 0x401921fb54442d18L    # 6.283185307179586

    float-to-double v12, v1

    mul-double/2addr v10, v12

    const-wide v12, 0x4076800000000000L    # 360.0

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v2, v8

    .line 146
    .local v2, "dx":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mCenter:Landroid/graphics/Point;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$1000(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    mul-int/lit8 v9, v2, -0x1

    sub-int v6, v8, v9

    .line 147
    .local v6, "x":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mCenter:Landroid/graphics/Point;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$1000(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->y:I

    sub-int v7, v8, v3

    .line 148
    .local v7, "y":I
    if-ltz v6, :cond_1

    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWidth:I
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$700(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)I

    move-result v8

    if-ge v6, v8, :cond_1

    if-ltz v7, :cond_1

    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mHeight:I
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)I

    move-result v8

    if-lt v7, v8, :cond_2

    .line 141
    :cond_1
    const/high16 v8, 0x3e800000    # 0.25f

    add-float/2addr v1, v8

    goto :goto_0

    .line 137
    .end local v1    # "degree":F
    .end local v2    # "dx":I
    .end local v3    # "dy":I
    .end local v5    # "radius":I
    .end local v6    # "x":I
    .end local v7    # "y":I
    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 151
    .restart local v1    # "degree":F
    .restart local v2    # "dx":I
    .restart local v3    # "dy":I
    .restart local v5    # "radius":I
    .restart local v6    # "x":I
    .restart local v7    # "y":I
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mMask:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$1100(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    .line 152
    .local v4, "pixel":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mColorTransparent:I
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$1200(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)I

    move-result v8

    if-eq v4, v8, :cond_3

    .line 153
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateCanvas:Landroid/graphics/Canvas;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$900(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Canvas;

    move-result-object v9

    int-to-float v10, v6

    int-to-float v11, v7

    const/high16 v8, 0x43b40000    # 360.0f

    iget v12, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->val$percent:F

    mul-float/2addr v8, v12

    float-to-int v8, v8

    int-to-float v8, v8

    cmpg-float v8, v1, v8

    if-gez v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWhitePaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$1300(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Paint;

    move-result-object v8

    :goto_2
    invoke-virtual {v9, v10, v11, v8}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 155
    :cond_3
    add-int/lit8 v5, v5, 0x1

    .line 156
    goto/16 :goto_1

    .line 153
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mGreenPaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$1400(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Paint;

    move-result-object v8

    goto :goto_2

    .line 158
    .end local v2    # "dx":I
    .end local v3    # "dy":I
    .end local v4    # "pixel":I
    .end local v5    # "radius":I
    .end local v6    # "x":I
    .end local v7    # "y":I
    :cond_5
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$600(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-static {v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 159
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    iget-object v8, v8, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    iget-object v9, v9, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateHandler:Landroid/os/Handler;

    const/4 v10, 0x0

    invoke-virtual {v9, v10, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 160
    return-void
.end method
