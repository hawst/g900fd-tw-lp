.class Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector$4;
.super Landroid/os/CountDownTimer;
.source "UvDeviceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 420
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 430
    # getter for: Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onFinish!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->mUvSensorListener:Lcom/sec/android/app/shealth/uv/UvSensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->access$500(Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;)Lcom/sec/android/app/shealth/uv/UvSensorListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/uv/UvSensorListener;->onTimeout()V

    .line 432
    return-void
.end method

.method public onTick(J)V
    .locals 2
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 424
    # getter for: Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onTick: "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->mUvSensorListener:Lcom/sec/android/app/shealth/uv/UvSensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->access$500(Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;)Lcom/sec/android/app/shealth/uv/UvSensorListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/uv/UvSensorListener;->onTick(J)V

    .line 426
    return-void
.end method
