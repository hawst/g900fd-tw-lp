.class public Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "BadgeAchieveActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final HANDLE_MSG_FINISH:I = 0x2

.field private static final HANDLE_MSG_INIT:I = 0x0

.field private static final HANDLE_MSG_NEXT:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

.field private mBadgeId:I

.field private mBadgeView:Landroid/widget/ImageView;

.field private mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

.field private mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

.field private mFinishDelayHandler:Landroid/os/Handler;

.field private mGoalId:I

.field private mHowShowScoreScreen:I

.field private mInformationQuery:I

.field private mLeftBtn:Landroid/widget/Button;

.field private mMissionId:I

.field private mOkBtn:Landroid/widget/Button;

.field private mOneBtnArea:Landroid/widget/RelativeLayout;

.field private mReceiveBadgeIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mRightBtn:Landroid/widget/Button;

.field private mRunnable:Ljava/lang/Runnable;

.field private mTwoBtnArea:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeId:I

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mGoalId:I

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mMissionId:I

    .line 52
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    .line 53
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    .line 55
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 56
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mHowShowScoreScreen:I

    .line 339
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;

    .line 385
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->setPostDelay()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->updateBadgeIcon()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->updateCompleteHeaderView()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mHowShowScoreScreen:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->startCignaCoachActivity()V

    return-void
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 153
    const v0, 0x7f0800e7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    .line 155
    const v0, 0x7f0800e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mOneBtnArea:Landroid/widget/RelativeLayout;

    .line 156
    const v0, 0x7f0800e9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mOkBtn:Landroid/widget/Button;

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    const v0, 0x7f0800ea

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mTwoBtnArea:Landroid/widget/RelativeLayout;

    .line 161
    const v0, 0x7f0800eb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mLeftBtn:Landroid/widget/Button;

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mLeftBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    const v0, 0x7f0800ec

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mRightBtn:Landroid/widget/Button;

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mRightBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    return-void
.end method

.method private setPostDelay()V
    .locals 4

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 337
    return-void
.end method

.method private startCignaCoachActivity()V
    .locals 3

    .prologue
    .line 327
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 328
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 329
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    const-string v1, "EXTRA_NAME_FIRST_ASSESSMENT_COMPLETE_CATEGORY"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 331
    const-string v1, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 332
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->startActivity(Landroid/content/Intent;)V

    .line 333
    return-void
.end method

.method private updateBadgeIcon()V
    .locals 2

    .prologue
    .line 172
    const v0, 0x7f0800e6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeView:Landroid/widget/ImageView;

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getBadgeImage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getBadgeImage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getCignaMissionBadgeByName(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 186
    :goto_0
    return-void

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeView:Landroid/widget/ImageView;

    const v1, 0x7f02005a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private updateButton()V
    .locals 5

    .prologue
    const v4, 0x7f090047

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 190
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    const/16 v1, 0xd05

    if-ne v0, v1, :cond_1

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mOneBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mTwoBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    const/16 v1, 0x115c

    if-ne v0, v1, :cond_2

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(I)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mOneBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mTwoBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 197
    :cond_2
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    const/16 v1, 0x457

    if-ne v0, v1, :cond_3

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mOneBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mTwoBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mRightBtn:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(I)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mLeftBtn:Landroid/widget/Button;

    const v1, 0x7f090c7b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 202
    :cond_3
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    const/16 v1, 0x8ae

    if-ne v0, v1, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mOneBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mTwoBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mOkBtn:Landroid/widget/Button;

    const v1, 0x7f090c4e

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method private updateCompleteHeaderView()V
    .locals 3

    .prologue
    .line 210
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->BADGE:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setCompleteType(Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;)V

    .line 211
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    if-eqz v1, :cond_0

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getTitleString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setExclamations(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getExtraString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 217
    .local v0, "BadgeRewardTime":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getAchieveDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getAchieveTime()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    const-string v2, ""

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setProgressMessage(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 222
    .end local v0    # "BadgeRewardTime":Ljava/lang/StringBuilder;
    :cond_0
    return-void
.end method


# virtual methods
.method public closeScreen()V
    .locals 2

    .prologue
    .line 126
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mHowShowScoreScreen:I

    const/16 v1, 0x2b67

    if-ne v0, v1, :cond_0

    .line 127
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->startCignaCoachActivity()V

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->finish()V

    .line 135
    :goto_0
    return-void

    .line 129
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mHowShowScoreScreen:I

    const/16 v1, 0x56ce

    if-ne v0, v1, :cond_1

    .line 130
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->setResult(I)V

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->finish()V

    goto :goto_0

    .line 133
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected customizeActionBar()V
    .locals 3

    .prologue
    .line 288
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 289
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 291
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_1

    .line 297
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 294
    .restart local v0    # "intent":Landroid/content/Intent;
    :cond_1
    const-string v1, "EXTRA_NAME_BADGE_VIEW_MODE"

    const/16 v2, 0xd05

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 113
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mHowShowScoreScreen:I

    const/16 v1, 0x2b67

    if-ne v0, v1, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->startCignaCoachActivity()V

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->finish()V

    .line 122
    :goto_0
    return-void

    .line 116
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mHowShowScoreScreen:I

    const/16 v1, 0x56ce

    if-ne v0, v1, :cond_1

    .line 117
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->setResult(I)V

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->finish()V

    goto :goto_0

    .line 120
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 301
    const/4 v0, 0x0

    .line 302
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 324
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 304
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getRemainBadgeCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 305
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 306
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getRemainBadgeCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 311
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-class v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 312
    .restart local v0    # "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 313
    const-string v1, "EXTRA_NAME_GOAL_ID"

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mGoalId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 314
    const-string v1, "EXTRA_NAME_MISSION_FROM_WHERE"

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 315
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->startActivity(Landroid/content/Intent;)V

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->finish()V

    goto :goto_0

    .line 319
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090835

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->finish()V

    goto :goto_0

    .line 302
    nop

    :pswitch_data_0
    .packed-switch 0x7f0800e9
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v5, 0xd05

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 61
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v1, 0x7f03002a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->setContentView(I)V

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 66
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->finish()V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    const-string v1, "EXTRA_NAME_BADGE_VIEW_MODE"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    .line 73
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 76
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, p0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getBadgeDataById(Landroid/content/Context;I)Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    .line 78
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->setRemainBadgeCount(I)V

    .line 82
    :cond_2
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    if-ne v1, v5, :cond_4

    .line 83
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->setRemainBadgeCount(I)V

    .line 100
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->initLayout()V

    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->updateBadgeIcon()V

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->updateButton()V

    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->updateCompleteHeaderView()V

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->setCustomActionBar()V

    .line 106
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    if-eq v1, v5, :cond_0

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 84
    :cond_4
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    const/16 v2, 0x115c

    if-ne v1, v2, :cond_5

    .line 86
    const-string v1, "intent_lifestyle_category"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 87
    const-string v1, "EXTRA_NAME_NEED_UPATE_MAIN_SCORE_ANIMATION"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mHowShowScoreScreen:I

    .line 88
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BadgeAchieveActivity mCategory : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BadgeAchieveActivity mHowShowScoreScreen : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mHowShowScoreScreen:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 90
    :cond_5
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    const/16 v2, 0x457

    if-ne v1, v2, :cond_6

    .line 91
    const-string v1, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mGoalId:I

    .line 92
    const-string v1, "EXTRA_NAME_MISSION_ID"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mMissionId:I

    goto :goto_1

    .line 95
    :cond_6
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    const/16 v2, 0x8ae

    if-ne v1, v2, :cond_3

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 283
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 139
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onDestroy()V

    .line 141
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->clearBadgeCount()V

    .line 144
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :goto_0
    return-void

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setCustomActionBar()V
    .locals 6

    .prologue
    const v4, 0x7f090033

    const/4 v5, 0x0

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    .line 227
    .local v2, "actoiniBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    if-nez v2, :cond_0

    .line 273
    :goto_0
    return-void

    .line 229
    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 231
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v3

    if-nez v3, :cond_1

    .line 232
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V

    .line 254
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207cf

    invoke-direct {v1, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 255
    .local v1, "actionBarButtonMoreBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 256
    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 257
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 262
    .end local v0    # "actionBarButtonListener":Landroid/view/View$OnClickListener;
    .end local v1    # "actionBarButtonMoreBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    :cond_1
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    const/16 v4, 0x115c

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    const/16 v4, 0x457

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mInformationQuery:I

    const/16 v4, 0x8ae

    if-ne v3, v4, :cond_3

    .line 265
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f090c5b

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    goto :goto_0

    .line 267
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getTitleString()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getTitleString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 270
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f0902f2

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    goto :goto_0
.end method
