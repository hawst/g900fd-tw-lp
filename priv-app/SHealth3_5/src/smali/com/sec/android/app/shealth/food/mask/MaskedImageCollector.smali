.class public Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;
.super Ljava/lang/Object;
.source "MaskedImageCollector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;,
        Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$OnFinishCreatingMaskListener;,
        Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;
    }
.end annotation


# instance fields
.field private mBitmapProvider:Lcom/sec/android/app/shealth/food/mask/BitmapProvider;

.field private mContext:Landroid/content/Context;

.field private mMaskCreatingTasks:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;",
            ">;"
        }
    .end annotation
.end field

.field private mMaskToolsArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools;",
            ">;"
        }
    .end annotation
.end field

.field private mMaskedBitmapsProvider:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;

.field private final mTasks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mContext:Landroid/content/Context;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mTasks:Ljava/util/HashMap;

    .line 43
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskToolsArray:Landroid/util/SparseArray;

    .line 44
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskCreatingTasks:Landroid/util/SparseArray;

    .line 45
    new-instance v0, Lcom/sec/android/app/shealth/food/mask/BitmapProvider;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mask/BitmapProvider;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mBitmapProvider:Lcom/sec/android/app/shealth/food/mask/BitmapProvider;

    .line 46
    new-instance v0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskedBitmapsProvider:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;Lcom/sec/android/app/shealth/food/mask/MaskTools;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/mask/MaskTools;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->collectBitmaps(Lcom/sec/android/app/shealth/food/mask/MaskTools;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;)Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskedBitmapsProvider:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;
    .param p2, "x2"    # Landroid/graphics/Bitmap;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->invokeCallback(Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskToolsArray:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskCreatingTasks:Landroid/util/SparseArray;

    return-object v0
.end method

.method private addListenerToCollectBitmapsOnFinishCreatingMask(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V
    .locals 1
    .param p1, "task"    # Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;
    .param p3, "listener"    # Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 99
    .local p2, "images":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$1;

    invoke-direct {v0, p0, p2, p3}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$1;-><init>(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->addOnFinishCreatingListener(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$OnFinishCreatingMaskListener;)V

    .line 106
    return-void
.end method

.method private collectBitmaps(Lcom/sec/android/app/shealth/food/mask/MaskTools;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V
    .locals 3
    .param p1, "maskTools"    # Lcom/sec/android/app/shealth/food/mask/MaskTools;
    .param p3, "listener"    # Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 118
    .local p2, "images":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    new-instance v1, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;

    invoke-direct {v1, p0, p2, p3}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;-><init>(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mBitmapProvider:Lcom/sec/android/app/shealth/food/mask/BitmapProvider;

    invoke-virtual {p1, p2, v1, v2}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->createCollectingBitmapTask(Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;Lcom/sec/android/app/shealth/food/mask/BitmapProvider;)Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;

    move-result-object v0

    .line 128
    .local v0, "task":Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 131
    .end local v0    # "task":Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;
    :cond_0
    return-void
.end method

.method private invokeCallback(Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-interface {p1, p2}, Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;->onDrawingResult(Landroid/graphics/Bitmap;)V

    .line 84
    :cond_0
    return-void
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskedBitmapsProvider:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;->clearLruCache()V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mBitmapProvider:Lcom/sec/android/app/shealth/food/mask/BitmapProvider;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mask/BitmapProvider;->clearLruCache()V

    .line 116
    return-void
.end method

.method public registerTask(ILjava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V
    .locals 5
    .param p1, "maskId"    # I
    .param p3, "listener"    # Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    .local p2, "images":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mTasks:Ljava/util/HashMap;

    const/4 v4, 0x0

    invoke-virtual {v3, p3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskedBitmapsProvider:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;->getImage(Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 60
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 61
    invoke-direct {p0, p3, v0}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->invokeCallback(Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;Landroid/graphics/Bitmap;)V

    .line 62
    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->removeCallback(Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V

    .line 64
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskToolsArray:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/mask/MaskTools;

    .line 65
    .local v2, "maskTools":Lcom/sec/android/app/shealth/food/mask/MaskTools;
    if-nez v2, :cond_2

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskCreatingTasks:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;

    .line 67
    .local v1, "maskCreatingTask":Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;
    if-nez v1, :cond_1

    .line 68
    new-instance v1, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;

    .end local v1    # "maskCreatingTask":Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;
    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;-><init>(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;I)V

    .line 69
    .restart local v1    # "maskCreatingTask":Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;
    invoke-direct {p0, v1, p2, p3}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->addListenerToCollectBitmapsOnFinishCreatingMask(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V

    .line 70
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskCreatingTasks:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 71
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 78
    .end local v1    # "maskCreatingTask":Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;
    :goto_0
    return-void

    .line 73
    .restart local v1    # "maskCreatingTask":Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;
    :cond_1
    invoke-direct {p0, v1, p2, p3}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->addListenerToCollectBitmapsOnFinishCreatingMask(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V

    goto :goto_0

    .line 76
    .end local v1    # "maskCreatingTask":Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;
    :cond_2
    invoke-direct {p0, v2, p2, p3}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->collectBitmaps(Lcom/sec/android/app/shealth/food/mask/MaskTools;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V

    goto :goto_0
.end method

.method public removeCallback(Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;

    .prologue
    .line 91
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;

    .line 92
    .local v0, "task":Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;
    if-eqz v0, :cond_0

    .line 93
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->cancel(Z)Z

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    return-void
.end method
