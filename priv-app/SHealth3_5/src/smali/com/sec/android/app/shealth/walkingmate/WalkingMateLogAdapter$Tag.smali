.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;
.super Ljava/lang/Object;
.source "WalkingMateLogAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Tag"
.end annotation


# instance fields
.field private mTag:Ljava/lang/String;

.field private mUniqueTag:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "device_info"    # Ljava/lang/String;

    .prologue
    .line 545
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 546
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;->mTag:Ljava/lang/String;

    .line 547
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;->mUniqueTag:Ljava/lang/String;

    .line 548
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method public uniqueTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;->mUniqueTag:Ljava/lang/String;

    return-object v0
.end method
