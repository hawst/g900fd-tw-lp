.class public Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;
.super Landroid/widget/ArrayAdapter;
.source "HeartrateTagListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CustomList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mCheckedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mIconResourceIds:[Ljava/lang/Integer;

.field private final mTagNames:[Ljava/lang/String;

.field private mTagSel:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Landroid/app/Activity;[Ljava/lang/String;[Ljava/lang/Integer;ZLjava/util/HashSet;)V
    .locals 6
    .param p2, "context"    # Landroid/app/Activity;
    .param p3, "tagNames"    # [Ljava/lang/String;
    .param p4, "iconResourceIds"    # [Ljava/lang/Integer;
    .param p5, "isAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Integer;",
            "Z",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 522
    .local p6, "tagSel":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    .line 523
    const v4, 0x7f030161

    invoke-direct {p0, p2, v4, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 524
    iput-object p3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mTagNames:[Ljava/lang/String;

    .line 525
    iput-object p4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mIconResourceIds:[Ljava/lang/Integer;

    .line 526
    if-eqz p6, :cond_0

    .line 527
    iput-object p6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mTagSel:Ljava/util/HashSet;

    .line 531
    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mCheckedList:Ljava/util/ArrayList;

    .line 533
    if-eqz p3, :cond_1

    .line 534
    move-object v0, p3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 535
    .local v3, "tag":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mCheckedList:Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 534
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 529
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "tag":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mTagSel:Ljava/util/HashSet;

    goto :goto_0

    .line 538
    :cond_1
    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;

    .prologue
    .line 516
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mCheckedList:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 542
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 543
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030160

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 544
    .local v4, "rowView":Landroid/view/View;
    const v6, 0x7f0805de

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 545
    .local v5, "txtTitle":Landroid/widget/TextView;
    const v6, 0x7f0805f9

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 546
    .local v1, "imageView":Landroid/widget/ImageView;
    const v6, 0x7f0805f8

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 547
    .local v0, "checkBox":Landroid/widget/CheckBox;
    const v6, 0x7f0805f6

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    .line 548
    .local v3, "mButton":Landroid/widget/RadioButton;
    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 549
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mTagNames:[Ljava/lang/String;

    aget-object v6, v6, p1

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 550
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mTagNames:[Ljava/lang/String;

    aget-object v6, v6, p1

    invoke-virtual {v4, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 551
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mIconResourceIds:[Ljava/lang/Integer;

    aget-object v6, v6, p1

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 552
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagIconId:[Ljava/lang/Integer;
    invoke-static {v6}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$1300(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)[Ljava/lang/Integer;

    move-result-object v6

    aget-object v6, v6, p1

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 554
    new-instance v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList$1;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 566
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v6}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->isMenuDeleteMode()Z

    move-result v6

    if-nez v6, :cond_0

    .line 567
    invoke-virtual {v4, v9}, Landroid/view/View;->setLongClickable(Z)V

    .line 568
    new-instance v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList$2;

    invoke-direct {v6, p0, v4, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList$2;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;Landroid/view/View;Landroid/widget/ImageView;)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 604
    :goto_0
    return-object v4

    .line 575
    :cond_0
    invoke-virtual {v4, v8}, Landroid/view/View;->setLongClickable(Z)V

    .line 576
    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 578
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mCheckedList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 579
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mTagNames:[Ljava/lang/String;

    aget-object v6, v6, p1

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 580
    new-instance v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList$3;

    invoke-direct {v6, p0, p1, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList$3;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;ILandroid/widget/CheckBox;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 590
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mTagSel:Ljava/util/HashSet;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mTagSel:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;->mTagSel:Ljava/util/HashSet;

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 592
    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 594
    :cond_1
    new-instance v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList$4;

    invoke-direct {v6, p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList$4;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$CustomList;Landroid/widget/CheckBox;)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
