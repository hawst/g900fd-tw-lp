.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
.super Ljava/lang/Object;
.source "WalkingMateGearLogDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DetailGearData"
.end annotation


# instance fields
.field mCalories:F

.field mCustomName:Ljava/lang/String;

.field mDailySteps:J

.field mDeviceName:Ljava/lang/String;

.field mDistance:F

.field mDuration:J

.field mHealthyPace:J

.field mIsSupportHealhtyPace:Z

.field mRunningSteps:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JFFI)V
    .locals 2
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "customName"    # Ljava/lang/String;
    .param p3, "dailySteps"    # J
    .param p5, "distance"    # F
    .param p6, "calories"    # F
    .param p7, "runningSteps"    # I

    .prologue
    const-wide/16 v0, -0x1

    .line 668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDeviceName:Ljava/lang/String;

    .line 670
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mCustomName:Ljava/lang/String;

    .line 671
    iput-wide p3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDailySteps:J

    .line 672
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDuration:J

    .line 673
    iput p5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDistance:F

    .line 674
    iput p6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mCalories:F

    .line 675
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mHealthyPace:J

    .line 676
    iput p7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mRunningSteps:I

    .line 677
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mIsSupportHealhtyPace:Z

    .line 678
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JJFFJI)V
    .locals 1
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "customName"    # Ljava/lang/String;
    .param p3, "dailySteps"    # J
    .param p5, "duration"    # J
    .param p7, "distance"    # F
    .param p8, "calories"    # F
    .param p9, "healthyPace"    # J
    .param p11, "runningSteps"    # I

    .prologue
    .line 656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 657
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDeviceName:Ljava/lang/String;

    .line 658
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mCustomName:Ljava/lang/String;

    .line 659
    iput-wide p3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDailySteps:J

    .line 660
    iput-wide p5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDuration:J

    .line 661
    iput p7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDistance:F

    .line 662
    iput p8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mCalories:F

    .line 663
    iput-wide p9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mHealthyPace:J

    .line 664
    iput p11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mRunningSteps:I

    .line 665
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mIsSupportHealhtyPace:Z

    .line 666
    return-void
.end method
