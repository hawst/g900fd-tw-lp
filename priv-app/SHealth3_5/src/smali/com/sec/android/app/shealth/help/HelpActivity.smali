.class public Lcom/sec/android/app/shealth/help/HelpActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HelpActivity.java"


# instance fields
.field private isListViewOpen:Z

.field private itemClickListener:Landroid/view/View$OnClickListener;

.field private mGuideListContainer:Landroid/widget/LinearLayout;

.field private mTitle:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTitleAction:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTitleButton:Landroid/widget/RelativeLayout;

.field private titleClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitle:Ljava/util/ArrayList;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->isListViewOpen:Z

    .line 125
    new-instance v0, Lcom/sec/android/app/shealth/help/HelpActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/help/HelpActivity$1;-><init>(Lcom/sec/android/app/shealth/help/HelpActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->itemClickListener:Landroid/view/View$OnClickListener;

    .line 137
    new-instance v0, Lcom/sec/android/app/shealth/help/HelpActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/help/HelpActivity$2;-><init>(Lcom/sec/android/app/shealth/help/HelpActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->titleClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/help/HelpActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/help/HelpActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/help/HelpActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/help/HelpActivity;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->isListViewOpen:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/help/HelpActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/help/HelpActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->isListViewOpen:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/help/HelpActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/help/HelpActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mGuideListContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/help/HelpActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/help/HelpActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleButton:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private checkAvailableFeature(I)Z
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "com.sec.shealth.help.action.HEART_RATE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHRAvailablilty(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 122
    :goto_0
    return v0

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "com.sec.shealth.help.action.SPO2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getSpO2Availablilty(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    if-eq v0, v2, :cond_4

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "com.sec.shealth.help.action.TGH"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getTGHAvailablilty(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 104
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "com.sec.shealth.help.action.SLEEP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    if-ne v0, v2, :cond_6

    move v0, v1

    .line 106
    goto :goto_0

    :cond_4
    move v0, v1

    .line 100
    goto :goto_0

    :cond_5
    move v0, v1

    .line 103
    goto :goto_0

    .line 107
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "com.sec.shealth.help.action.UV"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getUVAvailablilty(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->UV:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    if-ne v0, v2, :cond_8

    :cond_7
    move v0, v1

    .line 109
    goto :goto_0

    .line 110
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "com.sec.shealth.help.action.STRESS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    if-eq v0, v2, :cond_a

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHRAvailablilty(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 115
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "com.sec.shealth.help.action.COACH"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    .line 116
    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 114
    goto/16 :goto_0

    .line 117
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "com.sec.shealth.help.action.FOOD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 119
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "com.sec.shealth.help.action.WEIGHT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 122
    :cond_d
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private initializeGuideList()V
    .locals 2

    .prologue
    .line 64
    const v0, 0x7f0804e7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/HelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mGuideListContainer:Landroid/widget/LinearLayout;

    .line 65
    const v0, 0x7f0804e3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/HelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleButton:Landroid/widget/RelativeLayout;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleButton:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->titleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleButton:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/HelpActivity;->initializeGuidement()V

    .line 69
    return-void
.end method

.method private initializeGuidement()V
    .locals 7

    .prologue
    .line 72
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 73
    .local v1, "layoutInflater":Landroid/view/LayoutInflater;
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitle:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e001f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 74
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleAction:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0020

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 75
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mGuideListContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 77
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitle:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 78
    const v4, 0x7f03012a

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 79
    .local v3, "view":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->itemClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 81
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/View;->setFocusable(Z)V

    .line 83
    const v4, 0x7f0804e5

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 84
    .local v2, "textView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mTitle:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/help/HelpActivity;->checkAvailableFeature(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 87
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpActivity;->mGuideListContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 77
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    .end local v2    # "textView":Landroid/widget/TextView;
    .end local v3    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 157
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090039

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 160
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v0, 0x7f030129

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/HelpActivity;->setContentView(I)V

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/HelpActivity;->initializeGuideList()V

    .line 55
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 48
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 60
    return-void
.end method
