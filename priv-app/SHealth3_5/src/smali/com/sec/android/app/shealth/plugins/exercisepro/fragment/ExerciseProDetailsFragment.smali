.class public Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;
.super Landroid/support/v4/app/Fragment;
.source "ExerciseProDetailsFragment.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$ExerciseImageScaleCopyTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final TEMP_IMAGE_PREFIX:Ljava/lang/String; = "temp_"

.field private static final TEMP_IMAGE_TIME_FORMAT:Ljava/lang/String; = "HH_mm_ss_SSS"

.field private static mBackupDataType:I

.field private static mBackupRowId:J


# instance fields
.field private IMAGE_CAPTURE_DATA:Ljava/lang/String;

.field private IMAGE_CAPTURE_URI:Ljava/lang/String;

.field private LastTakePhoto:Ljava/lang/String;

.field public SPACE_FOR_UNIT:Ljava/lang/String;

.field afterImageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field

.field beforeImageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field

.field private btnCamera:Landroid/widget/ImageButton;

.field private btnGallery:Landroid/widget/ImageButton;

.field private isShownDeletePopup:Z

.field private mActivity:Landroid/app/Activity;

.field private mAvgHrm:F

.field private mAvgSpeed:F

.field private mComment:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDataType:I

.field private mDeclineAscent:D

.field private mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

.field private mEditMode:Z

.field private mEndMode:Z

.field private mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

.field private mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

.field private mExerciseType:I

.field private mImageCaptureUri:Landroid/net/Uri;

.field private mInclineAscent:D

.field private mInputTypeLayout:Landroid/widget/LinearLayout;

.field private mInputTypeTxt:Landroid/widget/TextView;

.field private mIntent:Landroid/content/Intent;

.field private mMaxElevation:D

.field private mMaxHrm:F

.field private mMaxSpeed:F

.field private mMinElevation:D

.field private mNotesHeader:Landroid/widget/TextView;

.field private mPicturesHeader:Landroid/widget/TextView;

.field private mRowId:J

.field private measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

.field private original_file:Ljava/io/File;

.field private photo:Landroid/graphics/Bitmap;

.field private photoContainer:Landroid/widget/FrameLayout;

.field private updateNeeded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 135
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mComment:Ljava/lang/String;

    .line 82
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->SPACE_FOR_UNIT:Ljava/lang/String;

    .line 84
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    .line 93
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    .line 102
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    .line 106
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updateNeeded:Z

    .line 116
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->isShownDeletePopup:Z

    .line 117
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->LastTakePhoto:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->beforeImageList:Ljava/util/List;

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->afterImageList:Ljava/util/List;

    .line 123
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 125
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->photo:Landroid/graphics/Bitmap;

    .line 126
    const-string v0, "imageCaptureUri"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    .line 127
    const-string/jumbo v0, "photo_data"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    .line 137
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mBackupDataType:I

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    .line 138
    sget-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mBackupRowId:J

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    .line 139
    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;Landroid/content/Intent;)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "rowId"    # J
    .param p4, "commont"    # Ljava/lang/String;
    .param p5, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 142
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mComment:Ljava/lang/String;

    .line 82
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->SPACE_FOR_UNIT:Ljava/lang/String;

    .line 84
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    .line 93
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    .line 102
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    .line 106
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updateNeeded:Z

    .line 116
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->isShownDeletePopup:Z

    .line 117
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->LastTakePhoto:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->beforeImageList:Ljava/util/List;

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->afterImageList:Ljava/util/List;

    .line 123
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 125
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->photo:Landroid/graphics/Bitmap;

    .line 126
    const-string v0, "imageCaptureUri"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    .line 127
    const-string/jumbo v0, "photo_data"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    .line 143
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    .line 144
    iput-wide p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    .line 145
    if-eqz p4, :cond_0

    .line 146
    iput-object p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mComment:Ljava/lang/String;

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updateNeeded:Z

    .line 149
    :cond_0
    iput-object p5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mIntent:Landroid/content/Intent;

    .line 151
    sput p1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mBackupDataType:I

    .line 152
    sput-wide p2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mBackupRowId:J

    .line 153
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updatePhotoGrid()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;
    .param p1, "x1"    # I

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->startExerciseProPhotoGallery(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->btnGallery:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->doTakePhotoAction()V

    return-void
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;Ljava/lang/String;JZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J
    .param p4, "x3"    # Z

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->saveInfoAndUpdateViewGrid(Ljava/lang/String;JZ)V

    return-void
.end method

.method private createSaveCropFile()Landroid/net/Uri;
    .locals 9

    .prologue
    .line 1177
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1178
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 1179
    .local v3, "time":J
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v7, "HH_mm_ss_SSS"

    invoke-direct {v2, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1180
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "temp_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1181
    .local v6, "url":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/SHealth/Exercise/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1182
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1183
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 1184
    .local v5, "uri":Landroid/net/Uri;
    return-object v5
.end method

.method private deleteTempFile()V
    .locals 2

    .prologue
    .line 1163
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->photo:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 1165
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1167
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1169
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1172
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method

.method public static disableClickFor(JLandroid/view/View;)V
    .locals 1
    .param p0, "millis"    # J
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1037
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setClickable(Z)V

    .line 1038
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$4;

    invoke-direct {v0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$4;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v0, p0, p1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1044
    return-void
.end method

.method private doTakePhotoAction()V
    .locals 3

    .prologue
    .line 1152
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->deleteTempFile()V

    .line 1153
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1154
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 1155
    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1156
    const-string/jumbo v1, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1157
    const/16 v1, 0x4e20

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1158
    return-void
.end method

.method private getElevationActivityElevationDB()V
    .locals 7

    .prologue
    .line 741
    const/4 v6, 0x0

    .line 743
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 744
    .local v2, "projection":[Ljava/lang/String;
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 745
    .local v3, "selectionClause":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 746
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 747
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 748
    const-string/jumbo v0, "max_altitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 749
    const-string/jumbo v0, "max_altitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxElevation:D

    .line 750
    :cond_0
    const-string/jumbo v0, "min_altitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 751
    const-string/jumbo v0, "min_altitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMinElevation:D

    .line 753
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getElevationActivityElevationDB  mMaxElevation["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxElevation:D

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "][mMinElevation"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMinElevation:D

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "]"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 756
    :cond_2
    if-eqz v6, :cond_3

    .line 757
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 759
    :cond_3
    return-void

    .line 756
    .end local v3    # "selectionClause":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 757
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private getExtras()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_workout_end_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "pick_result"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "pick_type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseType:I

    .line 190
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseType:I

    if-nez v0, :cond_0

    .line 191
    const/16 v0, 0x4650

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseType:I

    .line 195
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    if-nez v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mIntent:Landroid/content/Intent;

    const-string v1, "edit"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    .line 198
    :cond_1
    return-void
.end method

.method private getPaceValue(FZ)Ljava/lang/String;
    .locals 12
    .param p1, "time"    # F
    .param p2, "isAvg"    # Z

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 485
    const-string v6, "0\'00\""

    .line 486
    .local v6, "value":Ljava/lang/String;
    const/4 v7, 0x0

    cmpg-float v7, p1, v7

    if-gez v7, :cond_0

    move-object v7, v6

    .line 503
    :goto_0
    return-object v7

    .line 489
    :cond_0
    const v7, 0x45bb8000    # 6000.0f

    cmpl-float v7, p1, v7

    if-ltz v7, :cond_1

    .line 490
    const-string v7, "--\'--\""

    goto :goto_0

    .line 492
    :cond_1
    const-wide/16 v0, 0x3c

    .line 493
    .local v0, "MINUTE":J
    const-string v7, "%d"

    new-array v8, v11, [Ljava/lang/Object;

    long-to-float v9, v0

    div-float v9, p1, v9

    float-to-int v9, v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 494
    .local v2, "minuteText":Ljava/lang/String;
    const-string v7, "%02d"

    new-array v8, v11, [Ljava/lang/Object;

    long-to-float v9, v0

    rem-float v9, p1, v9

    float-to-int v9, v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 495
    .local v3, "secondText":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 496
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v5, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 497
    .local v5, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    .line 499
    .local v4, "strLanguage":Ljava/lang/String;
    if-eqz v4, :cond_2

    const-string v7, "ar"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 501
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_2
    move-object v7, v6

    .line 503
    goto/16 :goto_0
.end method

.method private hideKeyboard()V
    .locals 3

    .prologue
    .line 1076
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1077
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1078
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1080
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method private init(Landroid/view/View;)V
    .locals 0
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    .line 270
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initMeasuresContainer(Landroid/view/View;)V

    .line 271
    return-void
.end method

.method private initAscent()V
    .locals 3

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0 m"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setAscent(Ljava/lang/String;Z)V

    .line 370
    return-void
.end method

.method private initAvgHeartRate()V
    .locals 4

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0900d2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setAvgHeartRateInDetails(Ljava/lang/String;ZI)V

    .line 358
    return-void
.end method

.method private initAvgPace()V
    .locals 3

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0\'00\" /mi"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setAvgPace(Ljava/lang/String;Z)V

    .line 349
    return-void
.end method

.method private initAvgSpeed()V
    .locals 3

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0 km/h"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setAvgSpeed(Ljava/lang/String;Z)V

    .line 341
    return-void
.end method

.method private initBurntCalories()V
    .locals 3

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setBurntCalories(Ljava/lang/String;Z)V

    .line 337
    return-void
.end method

.method private initBurntCaloriesforOthers()V
    .locals 3

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setBurntCaloriesforOthers(Ljava/lang/String;Z)V

    .line 329
    return-void
.end method

.method private initDecent()V
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0 m"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setDecent(Ljava/lang/String;Z)V

    .line 378
    return-void
.end method

.method private initDistance()V
    .locals 3

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0 km"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setDistance(Ljava/lang/String;Z)V

    .line 333
    return-void
.end method

.method private initDuration()V
    .locals 4

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "00:00:00"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f090216

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setDuration(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 325
    return-void
.end method

.method private initHighElevation()V
    .locals 3

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0 m"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setHighElevation(Ljava/lang/String;Z)V

    .line 374
    return-void
.end method

.method private initLowElevation()V
    .locals 3

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0 m"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setLowElevation(Ljava/lang/String;Z)V

    .line 366
    return-void
.end method

.method private initMaxHeartRate()V
    .locals 4

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0900d2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setMaxHeartRateInDetails(Ljava/lang/String;ZI)V

    .line 362
    return-void
.end method

.method private initMaxPace()V
    .locals 3

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0\'00\" /mi"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setMaxPace(Ljava/lang/String;Z)V

    .line 353
    return-void
.end method

.method private initMaxSpeed()V
    .locals 3

    .prologue
    .line 344
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0 km/h"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setMaxSpeed(Ljava/lang/String;Z)V

    .line 345
    return-void
.end method

.method private initMeasuresContainer(Landroid/view/View;)V
    .locals 2
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    .line 279
    const v0, 0x7f08073c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->requestFocus()Z

    .line 281
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initTimeAndDate()V

    .line 282
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initDuration()V

    .line 286
    const/16 v0, 0x4650

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseType:I

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseType:I

    const/16 v1, 0x4655

    if-gt v0, v1, :cond_0

    .line 287
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initDistance()V

    .line 288
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initBurntCalories()V

    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initAvgSpeed()V

    .line 290
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initMaxSpeed()V

    .line 291
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initAvgPace()V

    .line 292
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initMaxPace()V

    .line 293
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initLowElevation()V

    .line 294
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initHighElevation()V

    .line 295
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initAscent()V

    .line 296
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initDecent()V

    .line 297
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initAvgHeartRate()V

    .line 298
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initMaxHeartRate()V

    .line 315
    :goto_0
    return-void

    .line 302
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initBurntCaloriesforOthers()V

    .line 303
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 310
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initAvgHeartRate()V

    .line 311
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initMaxHeartRate()V

    goto :goto_0

    .line 303
    nop

    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private initPhotoController(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1002
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    if-eqz v0, :cond_1

    .line 1003
    :cond_0
    const v0, 0x7f080737

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1005
    :cond_1
    const v0, 0x7f0806ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mPicturesHeader:Landroid/widget/TextView;

    .line 1006
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mPicturesHeader:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1011
    const v0, 0x7f0806f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->btnCamera:Landroid/widget/ImageButton;

    .line 1012
    const v0, 0x7f0806f3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->btnGallery:Landroid/widget/ImageButton;

    .line 1013
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->btnCamera:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1024
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->btnGallery:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1034
    return-void
.end method

.method private initTimeAndDate()V
    .locals 4

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    const-string v1, "0"

    const-string v2, "PM"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setTimeAndDate(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 320
    return-void
.end method

.method private isFileSupported(Ljava/lang/String;)Z
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 926
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/Constants;->SUPPORTED_FILE_PREFIXES:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v6, v0, v1

    .line 927
    .local v6, "prefix":Ljava/lang/String;
    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 930
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 931
    .local v5, "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v8, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 932
    invoke-static {p1, v5}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 934
    iget v2, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 935
    .local v2, "imageHeight":I
    iget v3, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 936
    .local v3, "imageWidth":I
    if-lez v2, :cond_0

    if-lez v3, :cond_0

    .line 938
    iput-boolean v9, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 939
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v10

    iput v10, v5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 940
    invoke-static {p1, v5}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 941
    .local v7, "resultantBitmap":Landroid/graphics/Bitmap;
    if-eqz v7, :cond_0

    .line 943
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 949
    .end local v2    # "imageHeight":I
    .end local v3    # "imageWidth":I
    .end local v5    # "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v6    # "prefix":Ljava/lang/String;
    .end local v7    # "resultantBitmap":Landroid/graphics/Bitmap;
    :goto_1
    return v8

    .line 926
    .restart local v6    # "prefix":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v6    # "prefix":Ljava/lang/String;
    :cond_1
    move v8, v9

    .line 949
    goto :goto_1
.end method

.method private saveInfoAndUpdateViewGrid(Ljava/lang/String;JZ)V
    .locals 2
    .param p1, "picturePath"    # Ljava/lang/String;
    .param p2, "exerciseId"    # J
    .param p4, "editMode"    # Z

    .prologue
    .line 917
    if-eqz p1, :cond_0

    .line 918
    new-instance v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;-><init>(Ljava/lang/String;)V

    .line 919
    .local v0, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    invoke-virtual {v0, p2, p3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setExerciseId(J)V

    .line 920
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, p4}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->insertTempitem(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;Z)J

    .line 921
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updatePhotoGrid()V

    .line 923
    .end local v0    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_0
    return-void
.end method

.method private setAscent(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "elevationStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 461
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 464
    return-void
.end method

.method private setAvgHeartRate(Ljava/lang/String;ZI)V
    .locals 1
    .param p1, "avgHeartRateStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z
    .param p3, "dataType"    # I

    .prologue
    .line 437
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 440
    return-void
.end method

.method private setAvgPace(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "avgPaceStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 425
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 427
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueTextForPace(Ljava/lang/String;)V

    .line 428
    return-void
.end method

.method private setAvgSpeed(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "avgSpeedStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 413
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 416
    return-void
.end method

.method private setBurntCalories(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "burntCaloriesStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 407
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 410
    return-void
.end method

.method private setBurntCaloriesforOthers(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "burntCaloriesStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 395
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->calorieItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setValueText(Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method private setDescent(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "elevationStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 467
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->ascentAndDecent:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 471
    return-void
.end method

.method private setDistance(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "distanceStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 401
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->distanceAndCalorie:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 404
    return-void
.end method

.method private setDuration(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "durationStr"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .param p3, "fInit"    # Z

    .prologue
    .line 388
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->setDuration(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->durationItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setValueText(Ljava/lang/String;)V

    .line 391
    return-void
.end method

.method private setHighElevation(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "highElevationStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 449
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 452
    return-void
.end method

.method private setLowElevation(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "lowElevationStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 455
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->lowElevAndhighElev:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setLeftValueText(Ljava/lang/String;)V

    .line 458
    return-void
.end method

.method private setMaxHeartRate(Ljava/lang/String;ZI)V
    .locals 1
    .param p1, "mxHeartRateStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z
    .param p3, "dataType"    # I

    .prologue
    .line 443
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageHeartAndMaxHeart:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 446
    return-void
.end method

.method private setMaxPace(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "maxPaceStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 431
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averagePaceAndMaxPace:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueTextForPace(Ljava/lang/String;)V

    .line 434
    return-void
.end method

.method private setMaxSpeed(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "maxSpeedStr"    # Ljava/lang/String;
    .param p2, "fInit"    # Z

    .prologue
    .line 419
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->averageSpdAndMaxSpd:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemDualView;->setRightValueText(Ljava/lang/String;)V

    .line 422
    return-void
.end method

.method private setRealtimeDataFromStatic(J)V
    .locals 3
    .param p1, "ExerciseId"    # J

    .prologue
    .line 507
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    invoke-static {v1, v2, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getWorkoutStatics(Landroid/content/Context;ZJ)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;

    move-result-object v0

    .line 508
    .local v0, "stastics":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;
    iget v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxHR:F

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxHrm:F

    .line 509
    iget v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->avgHR:F

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mAvgHrm:F

    .line 510
    iget v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxSpeed:F

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxSpeed:F

    .line 511
    iget v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->avgSpeed:F

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mAvgSpeed:F

    .line 512
    iget-wide v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->minAltitude:D

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMinElevation:D

    .line 513
    iget-wide v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxAltitude:D

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxElevation:D

    .line 514
    iget-wide v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->declineAscent:D

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDeclineAscent:D

    .line 515
    iget-wide v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->inclineAscent:D

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mInclineAscent:D

    .line 516
    return-void
.end method

.method private setSpeedText(ZLjava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1, "kmUnit"    # Z
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "unit"    # Ljava/lang/String;
    .param p4, "isAvgSpeed"    # Z

    .prologue
    .line 687
    if-eqz p4, :cond_0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mAvgSpeed:F

    .line 689
    .local v1, "mSpeed":F
    :goto_0
    if-eqz p1, :cond_1

    .line 691
    const/high16 v4, 0x42c80000    # 100.0f

    div-float v4, v1, v4

    float-to-int v4, v4

    mul-int/lit8 v4, v4, 0x64

    int-to-float v0, v4

    .line 692
    .local v0, "fSpeed":F
    float-to-long v4, v0

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getKmFromMeter(J)Ljava/lang/String;

    move-result-object p2

    .line 693
    const v4, 0x7f09080f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 701
    .end local v0    # "fSpeed":F
    :goto_1
    if-eqz p4, :cond_2

    .line 702
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setAvgSpeed(Ljava/lang/String;Z)V

    .line 706
    :goto_2
    if-eqz p1, :cond_4

    .line 707
    const/high16 v4, 0x447a0000    # 1000.0f

    div-float v4, v1, v4

    const v5, 0x3dcccccd    # 0.1f

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_3

    .line 708
    const/high16 v4, 0x45610000    # 3600.0f

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float v5, v1, v5

    div-float/2addr v4, v5

    invoke-direct {p0, v4, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getPaceValue(FZ)Ljava/lang/String;

    move-result-object p2

    .line 712
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0900c7

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 722
    :goto_4
    if-eqz p4, :cond_6

    .line 723
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setAvgPace(Ljava/lang/String;Z)V

    .line 726
    :goto_5
    return-void

    .line 687
    .end local v1    # "mSpeed":F
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxSpeed:F

    goto :goto_0

    .line 695
    .restart local v1    # "mSpeed":F
    :cond_1
    float-to-double v4, v1

    const-wide v6, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double v2, v4, v6

    .line 696
    .local v2, "mi":D
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    mul-double/2addr v4, v2

    double-to-int v4, v4

    int-to-double v4, v4

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    div-double v2, v4, v6

    .line 697
    new-instance v4, Ljava/text/DecimalFormat;

    const-string v5, "0.0"

    invoke-direct {v4, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p2

    .line 698
    const v4, 0x7f090810

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 704
    .end local v2    # "mi":D
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setMaxSpeed(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 710
    :cond_3
    const-string p2, "--\'--\""

    goto :goto_3

    .line 714
    :cond_4
    const/high16 v4, 0x447a0000    # 1000.0f

    div-float v4, v1, v4

    const v5, 0x3dcccccd    # 0.1f

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_5

    .line 715
    const/high16 v4, 0x45610000    # 3600.0f

    const v5, 0x3a22e36f

    mul-float/2addr v5, v1

    div-float/2addr v4, v5

    invoke-direct {p0, v4, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getPaceValue(FZ)Ljava/lang/String;

    move-result-object p2

    .line 719
    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0900cc

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_4

    .line 717
    :cond_5
    const-string p2, "--\'--\""

    goto :goto_6

    .line 725
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setMaxPace(Ljava/lang/String;Z)V

    goto/16 :goto_5
.end method

.method private setTimeAndDate(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "dateStr"    # Ljava/lang/String;
    .param p2, "timeStr"    # Ljava/lang/String;
    .param p3, "fInit"    # Z

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->timeAndDateItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setDateText(Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->measureItemsContainer:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MeasureItemsContainer;->timeAndDateItem:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseItemView;->setValueText(Ljava/lang/String;)V

    .line 385
    return-void
.end method

.method private startExerciseProPhotoGallery(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 998
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-static {v0, v1, p1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->startExerciseProPhotoGallery(Landroid/content/Context;Ljava/util/List;I)V

    .line 999
    return-void
.end method

.method private updateExerciseDetails()V
    .locals 15

    .prologue
    .line 519
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v11

    const-string v12, "km"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 521
    .local v4, "kmUnit":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    iget-wide v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->getExerciseData(Landroid/content/Context;J)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    .line 522
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    if-nez v11, :cond_0

    .line 684
    :goto_0
    return-void

    .line 524
    :cond_0
    iget-wide v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setRealtimeDataFromStatic(J)V

    .line 527
    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    packed-switch v11, :pswitch_data_0

    .line 544
    :cond_1
    :goto_1
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getElevationActivityElevationDB()V

    .line 552
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getTime()J

    move-result-wide v7

    .line 553
    .local v7, "time":J
    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormat(J)Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-direct {p0, v11, v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setTimeAndDate(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 555
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getmillisecond()J

    move-result-wide v2

    .line 556
    .local v2, "duration":J
    const-wide/16 v11, 0x1

    cmp-long v11, v2, v11

    if-gez v11, :cond_2

    .line 557
    const-wide/16 v2, 0x1

    .line 559
    :cond_2
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 560
    .local v10, "value":Ljava/lang/String;
    const v11, 0x7f0900ea

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 562
    .local v9, "unit":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    invoke-static {v2, v3, v11}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SecToString(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    invoke-static {v2, v3, v12}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SecToContentDescription(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-direct {p0, v11, v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setDuration(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 564
    const/4 v11, 0x2

    iget-object v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getDistance()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;Z)Ljava/lang/String;

    move-result-object v10

    .line 565
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    const/4 v12, 0x2

    iget-object v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getDistance()F

    move-result v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 567
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setDistance(Ljava/lang/String;Z)V

    .line 569
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getCalories()F

    move-result v1

    .line 570
    .local v1, "cal":F
    const/4 v11, 0x4

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v10

    .line 572
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    const/4 v12, 0x4

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 574
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setBurntCaloriesforOthers(Ljava/lang/String;Z)V

    .line 575
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setBurntCalories(Ljava/lang/String;Z)V

    .line 576
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportHRM()Z

    move-result v11

    if-nez v11, :cond_3

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    const/16 v12, 0x2726

    if-eq v11, v12, :cond_3

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    const/16 v12, 0x272e

    if-eq v11, v12, :cond_3

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    const/16 v12, 0x2723

    if-eq v11, v12, :cond_3

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    const/16 v12, 0x2730

    if-ne v11, v12, :cond_4

    .line 578
    :cond_3
    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mAvgHrm:F

    float-to-int v11, v11

    if-lez v11, :cond_7

    .line 579
    const/4 v11, 0x5

    iget v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mAvgHrm:F

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v10

    .line 583
    :goto_2
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    const/4 v12, 0x5

    iget v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mAvgHrm:F

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 585
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->SPACE_FOR_UNIT:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    invoke-direct {p0, v11, v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setAvgHeartRate(Ljava/lang/String;ZI)V

    .line 586
    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxHrm:F

    float-to-int v11, v11

    if-lez v11, :cond_8

    .line 587
    const/4 v11, 0x5

    iget v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxHrm:F

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v10

    .line 591
    :goto_3
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    const/4 v12, 0x5

    iget v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxHrm:F

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 593
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->SPACE_FOR_UNIT:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    invoke-direct {p0, v11, v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setMaxHeartRate(Ljava/lang/String;ZI)V

    .line 596
    :cond_4
    const/4 v11, 0x0

    invoke-direct {p0, v4, v10, v9, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setSpeedText(ZLjava/lang/String;Ljava/lang/String;Z)V

    .line 597
    const/4 v11, 0x1

    invoke-direct {p0, v4, v10, v9, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setSpeedText(ZLjava/lang/String;Ljava/lang/String;Z)V

    .line 601
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isAvailableBarometer(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_5

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    const/16 v12, 0x2724

    if-eq v11, v12, :cond_5

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    const/16 v12, 0x2728

    if-eq v11, v12, :cond_5

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    const/16 v12, 0x2726

    if-eq v11, v12, :cond_5

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    const/16 v12, 0x272e

    if-eq v11, v12, :cond_5

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    const/16 v12, 0x2723

    if-eq v11, v12, :cond_5

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    const/16 v12, 0x272f

    if-eq v11, v12, :cond_5

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    const/16 v12, 0x2730

    if-ne v11, v12, :cond_c

    .line 605
    :cond_5
    iget-wide v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMinElevation:D

    const-wide/16 v13, 0x0

    cmpl-double v11, v11, v13

    if-nez v11, :cond_9

    iget-wide v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxElevation:D

    const-wide/16 v13, 0x0

    cmpl-double v11, v11, v13

    if-nez v11, :cond_9

    .line 606
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    const/4 v12, 0x6

    iget-wide v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMinElevation:D

    double-to-float v13, v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 609
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "0 "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 610
    .local v6, "minStr":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "0 "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 643
    .local v5, "maxStr":Ljava/lang/String;
    :goto_4
    const/4 v11, 0x0

    invoke-direct {p0, v6, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setLowElevation(Ljava/lang/String;Z)V

    .line 644
    const/4 v11, 0x0

    invoke-direct {p0, v5, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setHighElevation(Ljava/lang/String;Z)V

    .line 646
    sget-boolean v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->SamsungLocationMonitor:Z

    if-eqz v11, :cond_6

    .line 647
    iget-wide v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDeclineAscent:D

    const-wide/16 v13, 0x0

    cmpg-double v11, v11, v13

    if-gtz v11, :cond_e

    iget-wide v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mInclineAscent:D

    const-wide/16 v13, 0x0

    cmpg-double v11, v11, v13

    if-gtz v11, :cond_e

    .line 648
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isAvailableBarometer(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 649
    const-string v10, "0.00"

    .line 655
    :goto_5
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    const/16 v12, 0x15

    iget-wide v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDeclineAscent:D

    double-to-float v13, v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 658
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 659
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 673
    :goto_6
    const/4 v11, 0x0

    invoke-direct {p0, v6, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setAscent(Ljava/lang/String;Z)V

    .line 674
    const/4 v11, 0x0

    invoke-direct {p0, v5, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setDescent(Ljava/lang/String;Z)V

    .line 678
    :cond_6
    iget-boolean v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    iget-boolean v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    or-int/2addr v11, v12

    if-eqz v11, :cond_f

    .line 679
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->setEditMode(Z)V

    goto/16 :goto_0

    .line 534
    .end local v1    # "cal":F
    .end local v2    # "duration":J
    .end local v5    # "maxStr":Ljava/lang/String;
    .end local v6    # "minStr":Ljava/lang/String;
    .end local v7    # "time":J
    .end local v9    # "unit":Ljava/lang/String;
    .end local v10    # "value":Ljava/lang/String;
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updateExerciseFromActivityDB()V

    .line 535
    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->fetchExerciseAccessoryName(I)Ljava/lang/String;

    move-result-object v0

    .line 537
    .local v0, "accessoryName":Ljava/lang/String;
    const-string v11, ""

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 538
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mInputTypeLayout:Landroid/widget/LinearLayout;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 539
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mInputTypeTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090a8c

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v0, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 581
    .end local v0    # "accessoryName":Ljava/lang/String;
    .restart local v1    # "cal":F
    .restart local v2    # "duration":J
    .restart local v7    # "time":J
    .restart local v9    # "unit":Ljava/lang/String;
    .restart local v10    # "value":Ljava/lang/String;
    :cond_7
    const-string v10, "--"

    goto/16 :goto_2

    .line 589
    :cond_8
    const-string v10, "--"

    goto/16 :goto_3

    .line 612
    :cond_9
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    const/4 v12, 0x6

    iget-wide v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMinElevation:D

    double-to-float v13, v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 615
    iget-wide v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMinElevation:D

    const-wide/16 v13, 0x0

    cmpl-double v11, v11, v13

    if-nez v11, :cond_a

    .line 616
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "0 "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 624
    .restart local v6    # "minStr":Ljava/lang/String;
    :goto_7
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    const/4 v12, 0x6

    iget-wide v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxElevation:D

    double-to-float v13, v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 627
    iget-wide v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxElevation:D

    const-wide/16 v13, 0x0

    cmpl-double v11, v11, v13

    if-nez v11, :cond_b

    .line 628
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "0 "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "maxStr":Ljava/lang/String;
    goto/16 :goto_4

    .line 619
    .end local v5    # "maxStr":Ljava/lang/String;
    .end local v6    # "minStr":Ljava/lang/String;
    :cond_a
    const/4 v11, 0x6

    iget-wide v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMinElevation:D

    double-to-float v12, v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v10

    .line 621
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "minStr":Ljava/lang/String;
    goto :goto_7

    .line 631
    :cond_b
    const/4 v11, 0x6

    iget-wide v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxElevation:D

    double-to-float v12, v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v10

    .line 633
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "maxStr":Ljava/lang/String;
    goto/16 :goto_4

    .line 637
    .end local v5    # "maxStr":Ljava/lang/String;
    .end local v6    # "minStr":Ljava/lang/String;
    :cond_c
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    const/4 v12, 0x6

    iget-wide v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMinElevation:D

    double-to-float v13, v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 640
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "-- "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 641
    .restart local v6    # "minStr":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "-- "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "maxStr":Ljava/lang/String;
    goto/16 :goto_4

    .line 651
    :cond_d
    const-string v10, "--"

    goto/16 :goto_5

    .line 661
    :cond_e
    const/16 v11, 0x15

    iget-wide v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDeclineAscent:D

    double-to-float v12, v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v10

    .line 663
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    const/16 v12, 0x15

    iget-wide v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDeclineAscent:D

    double-to-float v13, v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 666
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 667
    const/16 v11, 0x15

    iget-wide v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mInclineAscent:D

    double-to-float v12, v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v10

    .line 668
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    const/16 v12, 0x15

    iget-wide v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mInclineAscent:D

    double-to-float v13, v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 671
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_6

    .line 681
    :cond_f
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->setEditMode(Z)V

    .line 682
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getComment()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mComment:Ljava/lang/String;

    goto/16 :goto_0

    .line 527
    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateExerciseFromActivityDB()V
    .locals 8

    .prologue
    .line 761
    const/4 v6, 0x0

    .line 763
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 764
    .local v2, "projection":[Ljava/lang/String;
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 765
    .local v3, "selectionClause":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 766
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 767
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 768
    const-string/jumbo v0, "max_speed_per_hour"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxSpeed:F

    .line 769
    const-string/jumbo v0, "mean_speed_per_hour"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mAvgSpeed:F

    .line 770
    const-string/jumbo v0, "max_heart_rate_per_min"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mMaxHrm:F

    .line 771
    const-string/jumbo v0, "mean_heart_rate_per_min"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mAvgHrm:F

    .line 772
    const-string v0, "distance"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v7

    .line 773
    .local v7, "distance":F
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->setDistance(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 776
    .end local v7    # "distance":F
    :cond_0
    if-eqz v6, :cond_1

    .line 777
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 779
    :cond_1
    return-void

    .line 776
    .end local v3    # "selectionClause":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 777
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private updatePhotoGrid()V
    .locals 6

    .prologue
    .line 953
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    if-nez v1, :cond_2

    .line 954
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    if-eqz v1, :cond_1

    .line 955
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-wide v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;-><init>(Landroid/content/Context;JZ)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    .line 960
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->setExerciseGridItemListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;)V

    .line 984
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->photoContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 985
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->photoContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 988
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->getPhotoGridView()Landroid/view/View;

    move-result-object v0

    .line 989
    .local v0, "content":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->viewSize()I

    move-result v1

    if-lez v1, :cond_3

    .line 990
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->photoContainer:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 991
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->photoContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 995
    :goto_2
    return-void

    .line 957
    .end local v0    # "content":Landroid/view/View;
    :cond_1
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-wide v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;-><init>(Landroid/content/Context;JZ)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    goto :goto_0

    .line 981
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->updatePhotoData(J)V

    goto :goto_1

    .line 993
    .restart local v0    # "content":Landroid/view/View;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->photoContainer:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_2
.end method


# virtual methods
.method public deleteIfExtraImagesChanged()V
    .locals 4

    .prologue
    .line 1132
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->getImageList()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->afterImageList:Ljava/util/List;

    .line 1133
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->afterImageList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1134
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->afterImageList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 1135
    .local v1, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->beforeImageList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1136
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->afterImageList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->deleteExtraAddedPhoto(Ljava/lang/String;Landroid/content/Context;)V

    .line 1133
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1139
    .end local v1    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_1
    return-void
.end method

.method public fetchExerciseAccessoryName(I)Ljava/lang/String;
    .locals 10
    .param p1, "type"    # I

    .prologue
    .line 783
    const-string v7, ""

    .line 784
    .local v7, "deviceName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 785
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select _id, custom_name from user_device where  user_device.[_id]=(select exercise_device_info.[user_device__id] from exercise_device_info where exercise__id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 787
    .local v3, "selectionClause":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 788
    if-eqz v6, :cond_2

    .line 789
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 790
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 791
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 792
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 793
    .local v9, "index":Ljava/lang/String;
    const-string v0, "_"

    invoke-virtual {v9, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    .line 794
    const-string v0, "_"

    invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 795
    .local v8, "deviceType":I
    if-ne v8, p1, :cond_0

    .line 796
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 798
    .end local v8    # "deviceType":I
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 803
    .end local v9    # "index":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 804
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 803
    :cond_2
    if-eqz v6, :cond_3

    .line 804
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 806
    :cond_3
    return-object v7
.end method

.method public getExerciseImageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->getImageList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMemoText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 729
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected isNinePopup()Z
    .locals 6

    .prologue
    const/16 v5, 0x9

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1047
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->viewSize()I

    move-result v2

    if-ge v2, v5, :cond_1

    :cond_0
    move v0, v1

    .line 1053
    :goto_0
    return v0

    .line 1051
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090f73

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v0

    invoke-virtual {v3, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public isPhotChanged()Z
    .locals 4

    .prologue
    .line 1118
    const/4 v1, 0x0

    .line 1119
    .local v1, "isPhotoChanged":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->getImageList()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->afterImageList:Ljava/util/List;

    .line 1120
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->afterImageList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->beforeImageList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 1121
    const/4 v1, 0x1

    .line 1128
    :cond_0
    return v1

    .line 1123
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->beforeImageList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1124
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->beforeImageList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->afterImageList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1125
    const/4 v1, 0x1

    .line 1123
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 14
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 820
    const/16 v1, 0x99

    if-ne p1, v1, :cond_1

    .line 821
    const/4 v1, -0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_0

    .line 822
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    if-nez v1, :cond_0

    .line 823
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updateNeeded:Z

    .line 914
    :cond_0
    :goto_0
    return-void

    .line 826
    :cond_1
    const/16 v1, 0x4e20

    if-ne p1, v1, :cond_3

    .line 827
    if-eqz p2, :cond_0

    .line 830
    const/4 v4, 0x0

    .line 832
    .local v4, "picturePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v1, :cond_2

    .line 834
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 842
    :goto_1
    if-eqz v4, :cond_0

    .line 850
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v2, v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 851
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$ExerciseImageScaleCopyTask;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getPathToImage()Ljava/lang/String;

    move-result-object v5

    iget-wide v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    iget-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    move-object v2, p0

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$ExerciseImageScaleCopyTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JZ)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$ExerciseImageScaleCopyTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 836
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->LastTakePhoto:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 839
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->LastTakePhoto:Ljava/lang/String;

    .line 840
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->LastTakePhoto:Ljava/lang/String;

    goto :goto_1

    .line 854
    .end local v4    # "picturePath":Ljava/lang/String;
    :cond_3
    const/16 v1, 0x4e21

    if-ne p1, v1, :cond_0

    .line 855
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    .line 856
    if-eqz p2, :cond_0

    .line 858
    if-eqz p3, :cond_0

    .line 859
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 860
    .local v12, "inputImageCountByPath":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v9, 0x0

    .line 861
    .local v9, "arrayList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    if-eqz p3, :cond_4

    .line 862
    const-string/jumbo v1, "selectedItems"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .line 863
    if-nez v9, :cond_4

    .line 864
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "arrayList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 865
    .restart local v9    # "arrayList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 868
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 871
    :cond_4
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    .line 872
    .local v13, "uri":Landroid/net/Uri;
    if-eqz v13, :cond_5

    .line 874
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    invoke-static {v13, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 876
    .restart local v4    # "picturePath":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 877
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->TAG:Ljava/lang/String;

    const-string v2, "File path is empty"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    if-eqz v13, :cond_6

    .line 880
    invoke-virtual {v13}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 883
    :cond_6
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->isFileSupported(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 885
    sget-object v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->GALLERY_PATH:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 887
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exercise dir, picturePath="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    invoke-direct {p0, v4, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->saveInfoAndUpdateViewGrid(Ljava/lang/String;JZ)V

    .line 890
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->refreshLastTakenPhoto(Landroid/content/Context;)V

    goto :goto_2

    .line 892
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getPathToImage()Ljava/lang/String;

    move-result-object v5

    .line 893
    .local v5, "newFilePath":Ljava/lang/String;
    const/4 v11, 0x0

    .line 894
    .local v11, "imageCount":I
    invoke-interface {v12, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_8

    .line 895
    add-int/lit8 v11, v11, 0x1

    .line 896
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v12, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 904
    :goto_3
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "other dir, picturePath="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$ExerciseImageScaleCopyTask;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    iget-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    move-object v2, p0

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$ExerciseImageScaleCopyTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JZ)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$ExerciseImageScaleCopyTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_2

    .line 898
    :cond_8
    invoke-interface {v12, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 899
    add-int/lit8 v11, v11, 0x1

    .line 900
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v12, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 901
    const-string v1, ".jpg"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    .line 909
    .end local v5    # "newFilePath":Ljava/lang/String;
    .end local v11    # "imageCount":I
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f090990

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 261
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 262
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mActivity:Landroid/app/Activity;

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    .line 264
    return-void
.end method

.method public onBackPress()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1066
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    if-eqz v0, :cond_1

    .line 1067
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 1071
    :goto_0
    return v2

    .line 1070
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 161
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mIntent:Landroid/content/Intent;

    .line 165
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getExtras()V

    .line 166
    if-eqz p1, :cond_0

    .line 167
    const-string v0, "mDeletePopup"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->isShownDeletePopup:Z

    .line 169
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 212
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.plugins.exercisemate"

    const-string v4, "E013"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0301a0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 217
    .local v1, "content":Landroid/view/View;
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->init(Landroid/view/View;)V

    .line 218
    const v2, 0x7f08073d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    or-int/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->setEditMode(Z)V

    .line 220
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mComment:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->setText(Ljava/lang/String;)V

    .line 221
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    const v3, 0x7f0803e6

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mNotesHeader:Landroid/widget/TextView;

    .line 222
    const v2, 0x7f08073a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mInputTypeLayout:Landroid/widget/LinearLayout;

    .line 223
    const v2, 0x7f08073b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mInputTypeTxt:Landroid/widget/TextView;

    .line 224
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mNotesHeader:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09007c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09020b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0901fd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 229
    const v2, 0x7f08073e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Space;

    .line 231
    .local v0, "blankSpace":Landroid/widget/Space;
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEndMode:Z

    or-int/2addr v2, v3

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mComment:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mComment:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_0

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->setVisibility(I)V

    .line 233
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Space;->setVisibility(I)V

    .line 236
    :cond_0
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0900b2

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;-><init>(Landroid/content/Context;I)V

    const/16 v3, 0x32

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->getEditText()Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->addLimit(ILandroid/widget/EditText;)V

    .line 237
    const v2, 0x7f080632

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->photoContainer:Landroid/widget/FrameLayout;

    .line 238
    if-eqz p3, :cond_1

    .line 239
    const-string v2, "mDataType"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    .line 240
    const-string/jumbo v2, "mRowId"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    .line 242
    :cond_1
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 243
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updateExerciseDetails()V

    .line 246
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updatePhotoGrid()V

    .line 247
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->getImageList()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->beforeImageList:Ljava/util/List;

    .line 248
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initPhotoController(Landroid/view/View;)V

    .line 249
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    if-eqz v2, :cond_2

    .line 250
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 256
    :cond_2
    return-object v1

    .line 245
    :cond_3
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "there is no exercise id in intent"

    invoke-static {v2, v3}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->hideKeyboard()V

    .line 202
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->isShownDeletePopup:Z

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 208
    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 810
    if-eqz p1, :cond_0

    .line 811
    const-string v0, "mComment"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mComment:Ljava/lang/String;

    .line 812
    const-string v0, "mDataType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    .line 813
    const-string/jumbo v0, "mRowId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    .line 815
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 173
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 175
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updateNeeded:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    if-nez v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mComment:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mComment:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->setText(Ljava/lang/String;)V

    .line 179
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updateNeeded:Z

    .line 182
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->updateExerciseDetails()V

    .line 183
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 734
    const-string v0, "mComment"

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mComment:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    const-string v0, "mDataType"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mDataType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 736
    const-string/jumbo v0, "mRowId"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 737
    const-string v0, "mDeletePopup"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->isShownDeletePopup:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 738
    return-void
.end method

.method public retainPreviousImages()V
    .locals 4

    .prologue
    .line 1142
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->beforeImageList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1143
    new-instance v1, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->beforeImageList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;-><init>(Ljava/lang/String;)V

    .line 1144
    .local v1, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mRowId:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setExerciseId(J)V

    .line 1145
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mContext:Landroid/content/Context;

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMode:Z

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->insertTempitem(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;Z)J

    .line 1142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1147
    .end local v1    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_0
    return-void
.end method

.method public setCameraBtnClick()V
    .locals 2

    .prologue
    .line 1057
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/16 v1, 0x4e20

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->startCamera(Landroid/app/Activity;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->LastTakePhoto:Ljava/lang/String;

    .line 1059
    return-void
.end method

.method public setGalleryBtnClick()V
    .locals 4

    .prologue
    .line 1062
    const/16 v0, 0x4e21

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mExerciseProGridView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->viewSize()I

    move-result v1

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->setGalleryBtnClick(IILjava/lang/String;Landroid/content/Context;)V

    .line 1063
    return-void
.end method

.method public setMenoText(Ljava/lang/String;)V
    .locals 1
    .param p1, "Text"    # Ljava/lang/String;

    .prologue
    .line 1082
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1083
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->mEditMemoView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->setText(Ljava/lang/String;)V

    .line 1085
    :cond_0
    return-void
.end method
