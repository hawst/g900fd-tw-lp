.class Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;
.super Landroid/widget/BaseAdapter;
.source "AutoBackupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IntervalItemAdapter"
.end annotation


# instance fields
.field private mIntervalList:[J

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Landroid/content/Context;[J)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "objects"    # [J

    .prologue
    .line 417
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 418
    iput-object p3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->mIntervalList:[J

    .line 419
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->mIntervalList:[J

    array-length v0, v0

    return v0
.end method

.method public getInterval(J)Ljava/lang/String;
    .locals 8
    .param p1, "mIntervalSecond"    # J

    .prologue
    const-wide/32 v6, 0x15180

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 437
    div-long v0, p1, v6

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 438
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    const v2, 0x7f090d23

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    div-long v3, p1, v6

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 440
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    const v2, 0x7f090d24

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    const-wide/16 v3, 0xe10

    div-long v3, p1, v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->mIntervalList:[J

    aget-wide v0, v0, p1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 433
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 447
    if-nez p2, :cond_0

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;-><init>()V

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1402(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v1

    const v0, 0x7f080055

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v1, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mIntervalItemLayout:Landroid/widget/RelativeLayout;

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v1

    const v0, 0x7f080056

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mIntervalText:Landroid/widget/TextView;

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v1

    const v0, 0x7f080057

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, v1, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mRadioButton:Landroid/widget/RadioButton;

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    const v1, 0x7f080058

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mDivider:Landroid/view/View;

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 457
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1402(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mIntervalText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->mIntervalList:[J

    aget-wide v1, v1, p1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->getInterval(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->mIntervalList:[J

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mDivider:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 466
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSwitch:Landroid/widget/Switch;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1500(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Landroid/widget/Switch;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-nez v0, :cond_3

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->mIntervalList:[J

    aget-wide v0, v0, p1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSyncInterval:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 473
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mIntervalItemLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mIntervalText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mIntervalText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 495
    :goto_2
    return-object p2

    .line 463
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mDivider:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 470
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    .line 479
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->mIntervalList:[J

    aget-wide v0, v0, p1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSyncInterval:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mClickState:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1600(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mIntervalItemLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestFocus()Z

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mClickState:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1602(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Z)Z

    .line 492
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mIntervalItemLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {v1, v2, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;I)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 488
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->vh:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$ViewHodler;->mRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_3
.end method
