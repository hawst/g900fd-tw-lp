.class public Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "SuggestedGoalsDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mGoalData:Lcom/cigna/coach/apiobjects/GoalInfo;

.field private mGoalDetailHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;

.field private mSelectedGoalDetailImage:Ljava/lang/String;

.field private mSelectedGoalId:I

.field private mSelectedListIndex:I

.field private mSuggestedCancleBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    return-void
.end method

.method private convertToHeightValue(F)Ljava/lang/String;
    .locals 8
    .param p1, "height"    # F

    .prologue
    .line 266
    const/4 v2, 0x0

    .line 268
    .local v2, "retun_value":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    if-nez v3, :cond_0

    .line 269
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 272
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getHeightUnit()Ljava/lang/String;

    move-result-object v1

    .line 274
    .local v1, "heightUnit":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v3, "cm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 275
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%.1f"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0901cf

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 284
    :goto_0
    return-object v2

    .line 278
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getFeetandInch(F)Ljava/util/HashMap;

    move-result-object v0

    .line 279
    .local v0, "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "feet"

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900d3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "inch"

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0900d4

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0900bd

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private convertToWeightValue(F)Ljava/lang/String;
    .locals 10
    .param p1, "weight"    # F

    .prologue
    const v9, 0x7f0e0027

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 288
    const/4 v0, 0x0

    .line 289
    .local v0, "retun_value":Ljava/lang/String;
    const/high16 v3, 0x447a0000    # 1000.0f

    div-float v2, p1, v3

    .line 291
    .local v2, "weight_value":F
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    if-nez v3, :cond_0

    .line 292
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 294
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getWeightUnit()Ljava/lang/String;

    move-result-object v1

    .line 295
    .local v1, "weightUnit":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v3, "kg"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 296
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%.1f"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 302
    :goto_0
    return-object v0

    .line 299
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%.1f"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToLb(F)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getFeetandInch(F)Ljava/util/HashMap;
    .locals 5
    .param p0, "value"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v4, 0x41400000    # 12.0f

    .line 307
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 308
    .local v1, "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertInchToCm(F)F

    move-result v2

    .line 309
    .local v2, "inch":F
    div-float v3, v2, v4

    float-to-int v0, v3

    .line 310
    .local v0, "feet":I
    rem-float/2addr v2, v4

    .line 311
    if-nez v0, :cond_0

    const/high16 v3, 0x41000000    # 8.0f

    cmpg-float v3, v2, v3

    if-gez v3, :cond_0

    .line 312
    const/high16 v2, 0x41000000    # 8.0f

    .line 313
    :cond_0
    const-string v3, "feet"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    const-string v3, "inch"

    float-to-int v4, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    return-object v1
.end method

.method private getGoalData()Lcom/cigna/coach/apiobjects/GoalInfo;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 321
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 322
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 323
    const-string v2, "intent_extra_index"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mSelectedListIndex:I

    .line 324
    const-string v2, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mSelectedGoalId:I

    .line 325
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mSelectedGoalId:I

    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->getAvailableGoalData(I)Lcom/cigna/coach/apiobjects/GoalInfo;

    move-result-object v0

    .line 330
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setGoalsData()V
    .locals 25

    .prologue
    .line 108
    const v22, 0x7f080259

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 109
    .local v8, "goalName":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalData:Lcom/cigna/coach/apiobjects/GoalInfo;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoal()Ljava/lang/String;

    move-result-object v9

    .line 110
    .local v9, "goalNameTxt":Ljava/lang/String;
    if-eqz v9, :cond_0

    .line 111
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    :cond_0
    const v22, 0x7f08025a

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 115
    .local v6, "frequency":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalData:Lcom/cigna/coach/apiobjects/GoalInfo;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoalFrequency()Ljava/lang/String;

    move-result-object v7

    .line 116
    .local v7, "frequencyTxt":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 117
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    :cond_1
    const v22, 0x7f08025b

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 125
    .local v15, "response":Landroid/widget/TextView;
    const/16 v16, 0x0

    .line 127
    .local v16, "responseTxt":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalData:Lcom/cigna/coach/apiobjects/GoalInfo;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/apiobjects/GoalInfo;->getQuestionsAnswerGroup()Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionAnswer()Ljava/util/List;

    move-result-object v14

    .line 129
    .local v14, "qaList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    if-eqz v14, :cond_2

    .line 130
    sget-object v22, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->TAG:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "setGoalsData category :  "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalData:Lcom/cigna/coach/apiobjects/GoalInfo;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/cigna/coach/apiobjects/GoalInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalData:Lcom/cigna/coach/apiobjects/GoalInfo;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/apiobjects/GoalInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-ne v0, v1, :cond_7

    .line 134
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_2

    .line 136
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v14, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    .line 137
    .local v12, "qa0":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-interface {v14, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    .line 139
    .local v13, "qa1":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    invoke-virtual {v12}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v2

    .line 140
    .local v2, "answer0":Lcom/cigna/coach/apiobjects/Answer;
    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v3

    .line 142
    .local v3, "answer1":Lcom/cigna/coach/apiobjects/Answer;
    const/4 v10, 0x0

    .line 143
    .local v10, "height":Ljava/lang/String;
    const/16 v17, 0x0

    .line 144
    .local v17, "weight":Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 145
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/Answer;->getAnswer()Ljava/lang/String;

    move-result-object v10

    .line 149
    :goto_0
    if-eqz v3, :cond_6

    .line 150
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/Answer;->getAnswer()Ljava/lang/String;

    move-result-object v17

    .line 155
    :goto_1
    if-eqz v10, :cond_2

    if-eqz v17, :cond_2

    .line 156
    invoke-static {v10}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v11

    .line 157
    .local v11, "height_value":F
    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v18

    .line 158
    .local v18, "weight_value":F
    const/16 v22, 0x0

    cmpl-float v22, v11, v22

    if-lez v22, :cond_2

    const/16 v22, 0x0

    cmpl-float v22, v18, v22

    if-lez v22, :cond_2

    .line 159
    const-string v4, ""

    .line 160
    .local v4, "description_height":Ljava/lang/String;
    const-string v5, ""

    .line 161
    .local v5, "description_weight":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f090339

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f09033b

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 163
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->convertToHeightValue(F)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\n"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->convertToWeightValue(F)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 187
    .end local v2    # "answer0":Lcom/cigna/coach/apiobjects/Answer;
    .end local v3    # "answer1":Lcom/cigna/coach/apiobjects/Answer;
    .end local v4    # "description_height":Ljava/lang/String;
    .end local v5    # "description_weight":Ljava/lang/String;
    .end local v10    # "height":Ljava/lang/String;
    .end local v11    # "height_value":F
    .end local v12    # "qa0":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .end local v13    # "qa1":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .end local v17    # "weight":Ljava/lang/String;
    .end local v18    # "weight_value":F
    :cond_2
    :goto_2
    if-eqz v16, :cond_3

    .line 188
    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    :cond_3
    const v22, 0x7f08025c

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 192
    .local v20, "whyDoItTitle":Landroid/widget/TextView;
    const v22, 0x7f09033e

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 194
    const v22, 0x7f08025d

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 195
    .local v19, "whyDoIt":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalData:Lcom/cigna/coach/apiobjects/GoalInfo;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/apiobjects/GoalInfo;->getWhyDoIt()Ljava/lang/String;

    move-result-object v21

    .line 196
    .local v21, "whyDoItTxt":Ljava/lang/String;
    if-eqz v21, :cond_4

    .line 197
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    :cond_4
    return-void

    .line 147
    .end local v19    # "whyDoIt":Landroid/widget/TextView;
    .end local v20    # "whyDoItTitle":Landroid/widget/TextView;
    .end local v21    # "whyDoItTxt":Ljava/lang/String;
    .restart local v2    # "answer0":Lcom/cigna/coach/apiobjects/Answer;
    .restart local v3    # "answer1":Lcom/cigna/coach/apiobjects/Answer;
    .restart local v10    # "height":Ljava/lang/String;
    .restart local v12    # "qa0":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .restart local v13    # "qa1":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .restart local v17    # "weight":Ljava/lang/String;
    :cond_5
    sget-object v22, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->TAG:Ljava/lang/String;

    const-string v23, "Height answer is null"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 152
    :cond_6
    sget-object v22, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->TAG:Ljava/lang/String;

    const-string v23, "Weight answer is null"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 172
    .end local v2    # "answer0":Lcom/cigna/coach/apiobjects/Answer;
    .end local v3    # "answer1":Lcom/cigna/coach/apiobjects/Answer;
    .end local v10    # "height":Ljava/lang/String;
    .end local v12    # "qa0":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .end local v13    # "qa1":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .end local v17    # "weight":Ljava/lang/String;
    :cond_7
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v22

    if-lez v22, :cond_2

    .line 174
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v14, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    .line 176
    .restart local v12    # "qa0":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    invoke-virtual {v12}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v2

    .line 178
    .restart local v2    # "answer0":Lcom/cigna/coach/apiobjects/Answer;
    if-eqz v2, :cond_8

    .line 179
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerSummary()Ljava/lang/String;

    move-result-object v16

    goto :goto_2

    .line 181
    :cond_8
    sget-object v22, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->TAG:Ljava/lang/String;

    const-string v23, "Answer is null"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private setHeaderBgImageForLifeStyle()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalDetailHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;

    if-nez v0, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mSelectedGoalDetailImage:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mSelectedGoalDetailImage:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalDetailHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mSelectedGoalDetailImage:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getGoalImageResourceID(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;->setBackgoundImage(I)V

    goto :goto_0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 88
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090c56

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 91
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 203
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 205
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 208
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onBackPressed()V

    goto :goto_0

    .line 212
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 213
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mSelectedGoalId:I

    sget-object v2, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->UpdateGoalStatus(ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 215
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 216
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 217
    const-string v1, "EXTRA_NAME_GOAL_ID"

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mSelectedGoalId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 218
    const-string v1, "EXTRA_NAME_MISSION_FROM_WHERE"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 219
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x7f080257
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v4, 0x7f03007d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->setContentView(I)V

    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getGoalData()Lcom/cigna/coach/apiobjects/GoalInfo;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalData:Lcom/cigna/coach/apiobjects/GoalInfo;

    .line 57
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalData:Lcom/cigna/coach/apiobjects/GoalInfo;

    if-nez v4, :cond_0

    .line 58
    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->TAG:Ljava/lang/String;

    const-string v5, "Goal Data is null!"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->finish()V

    .line 84
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalData:Lcom/cigna/coach/apiobjects/GoalInfo;

    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoalImage()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mSelectedGoalDetailImage:Ljava/lang/String;

    .line 65
    const v4, 0x7f080255

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalDetailHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->setHeaderBgImageForLifeStyle()V

    .line 68
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalDetailHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;->setVisibility(I)V

    .line 70
    const v4, 0x7f080256

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 71
    .local v1, "contentsContainer":Landroid/widget/LinearLayout;
    const v4, 0x7f03007e

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 72
    .local v2, "goalContents":Landroid/view/View;
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 74
    const v4, 0x7f080257

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 75
    .local v0, "cancelBtn":Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    const v4, 0x7f080258

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 78
    .local v3, "okBtn":Landroid/widget/Button;
    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->mGoalData:Lcom/cigna/coach/apiobjects/GoalInfo;

    if-eqz v4, :cond_1

    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->setGoalsData()V

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->setActionBarNormalMode()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 104
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onDestroy()V

    .line 105
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 100
    return-void
.end method

.method public setActionBarNormalMode()V
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 236
    .local v0, "actoiniBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    if-nez v0, :cond_0

    .line 255
    :goto_0
    return-void

    .line 238
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    goto :goto_0
.end method
