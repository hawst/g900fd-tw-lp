.class public final Lcom/sec/android/app/shealth/weight/common/WeightConstants;
.super Ljava/lang/Object;
.source "WeightConstants.java"


# static fields
.field public static final ACC_KEY:Ljava/lang/String; = "accessory"

.field public static final ACTIONBAR_EDIT:I = 0x0

.field public static final APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

.field public static final COMMENT_KEY:Ljava/lang/String; = "COMMENT_KEY"

.field public static final EDIT:Ljava/lang/String; = "Edit"

.field public static final FT_INCH_FORMAT:Ljava/lang/String; = "%1$d \' %2$d \""

.field public static final INCHES_IN_FT:I = 0xc

.field public static final LOG_KEY:Ljava/lang/String; = "log"

.field public static final LOSE_ONE_UNIT:F = 1.0f

.field public static final MODE_KEY:Ljava/lang/String; = "MODE_KEY"

.field public static final REQUEST_FOR_GOAL:I = 0x2713

.field public static final REQUEST_FOR_LOG:I = 0x2714

.field public static final TIME_DATE_KEY:Ljava/lang/String; = "TIME_DATE_KEY"

.field public static final VALUE_NOT_SET:I = -0x1

.field public static final WEIGHT_CHARTTAB:Ljava/lang/String; = "WEIGHT_CHARTTAB"

.field public static final WEIGHT_ID_KEY:Ljava/lang/String; = "WEIGHT_ID_KEY"

.field public static final WEIGHT_KEY:Ljava/lang/String; = "weight"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sput-object v0, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method
