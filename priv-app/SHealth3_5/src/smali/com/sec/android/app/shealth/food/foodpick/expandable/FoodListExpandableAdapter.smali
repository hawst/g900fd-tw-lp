.class public Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "FoodListExpandableAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$3;,
        Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener;,
        Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener;,
        Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener;,
        Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;
    }
.end annotation


# static fields
.field private static final CATEGORY_HEADER_VIEWS_AMOUNT:I = 0x1

.field protected static final LIST_ITEM_TAG:I = 0x7f0d0044

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mInflater:Landroid/view/LayoutInflater;

.field private mIsSingleCategoryCaptionVisible:Z

.field protected mItemsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOnFavoriteClickListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOnItemClickListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOnItemLongClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Map;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p2, "allItems":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;>;"
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mIsSingleCategoryCaptionVisible:Z

    .line 78
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mContext:Landroid/content/Context;

    .line 79
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 80
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    .line 81
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnFavoriteClickListeners:Ljava/util/Set;

    .line 82
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnItemClickListeners:Ljava/util/Set;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnFavoriteClickListeners:Ljava/util/Set;

    return-object v0
.end method

.method private getItemTypeForPosition(I)Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;
    .locals 8
    .param p1, "position"    # I

    .prologue
    .line 417
    const/4 v4, 0x0

    .line 419
    .local v4, "previousItemsCount":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 420
    .local v0, "category":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .line 421
    .local v1, "headerDataHolder":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 422
    .local v3, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    if-lt p1, v4, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->isHeaderVisible()Z

    move-result v5

    if-nez v5, :cond_0

    .line 424
    add-int/lit8 v4, v4, -0x1

    .line 426
    :cond_0
    if-ne p1, v4, :cond_1

    .line 427
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;

    invoke-direct {v5, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V

    .line 430
    :goto_1
    return-object v5

    .line 428
    :cond_1
    if-le p1, v4, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    add-int/2addr v5, v4

    if-gt p1, v5, :cond_2

    .line 430
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;

    sub-int v6, p1, v4

    add-int/lit8 v6, v6, -0x1

    invoke-direct {v5, v1, v6}, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;I)V

    goto :goto_1

    .line 433
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    add-int/2addr v4, v5

    .line 434
    add-int/lit8 v4, v4, 0x1

    .line 435
    goto :goto_0

    .line 436
    .end local v0    # "category":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;>;"
    .end local v1    # "headerDataHolder":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    .end local v3    # "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    :cond_3
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Position "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is out of current adapters\'s range "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getGroupCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method private static inflateHeader(Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/ViewGroup;
    .locals 4
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 441
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300de

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 442
    .local v0, "convertView":Landroid/view/ViewGroup;
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->updateHeaderTitle(Landroid/view/View;Ljava/lang/String;)V

    .line 443
    return-object v0
.end method

.method private initExpandableIndicatorListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Landroid/widget/ExpandableListView;I)V
    .locals 2
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    .param p2, "parent"    # Landroid/widget/ExpandableListView;
    .param p3, "groupPosition"    # I

    .prologue
    .line 343
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getExpandableIndicator()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$1;

    invoke-direct {v1, p0, p2, p3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;Landroid/widget/ExpandableListView;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357
    return-void
.end method

.method private initFavoriteButtonListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V
    .locals 2
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    .param p2, "categoryHolder"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .prologue
    .line 360
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getFavoriteWrapper()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 386
    return-void
.end method

.method private isHeaderVisible()Z
    .locals 1

    .prologue
    .line 329
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mIsSingleCategoryCaptionVisible:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->isOnlyOneCategoryContainData()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isOnlyOneCategoryContainData()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 333
    const/4 v0, 0x0

    .line 334
    .local v0, "categoryCounter":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 335
    .local v2, "mapItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 336
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 339
    .end local v2    # "mapItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;>;"
    :cond_1
    if-ne v0, v4, :cond_2

    move v3, v4

    :goto_1
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private varargs makeFormattedToast(I[Ljava/lang/String;)V
    .locals 3
    .param p1, "stringResId"    # I
    .param p2, "params"    # [Ljava/lang/String;

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 414
    return-void
.end method

.method private static updateHeaderTitle(Landroid/view/View;Ljava/lang/String;)V
    .locals 3
    .param p0, "convertView"    # Landroid/view/View;
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 447
    const v1, 0x7f0803a1

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 448
    .local v0, "titleLabel":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 449
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 455
    :goto_0
    return-void

    .line 452
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v2, "convertView is not of type it has to be"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addOnFavoriteClickListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 539
    .local p1, "onFavoriteClickListener":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnFavoriteClickListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 540
    return-void
.end method

.method public addOnItemClickListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 491
    .local p1, "onItemClickListener":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnItemClickListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 492
    return-void
.end method

.method public areAllItemsChecked()Z
    .locals 2

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getAllGroupItems()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getCheckedItems()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkAllItems(Z)V
    .locals 3
    .param p1, "isChecked"    # Z

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getAllGroupItems()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .line 260
    .local v1, "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->setChecked(Z)V

    goto :goto_0

    .line 262
    .end local v1    # "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    :cond_0
    return-void
.end method

.method public getAllGroupItems()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 285
    .local v0, "allList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 286
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 288
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    :cond_0
    return-object v0
.end method

.method public getCheckedItems()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 293
    .local v0, "allCheckedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getAllGroupItems()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .line 294
    .local v2, "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 295
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 298
    .end local v2    # "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    :cond_1
    return-object v0
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 139
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 149
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 192
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getItemTypeForPosition(I)Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;

    move-result-object v9

    .line 193
    .local v9, "typedPosition":Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$3;->$SwitchMap$com$sec$android$app$shealth$food$foodpick$expandable$FoodListExpandableAdapter$ItemType:[I

    iget-object v2, v9, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->itemType:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 206
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not supported item type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v9, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->itemType:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Category header cannot be expanded!"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :pswitch_1
    iget-object v6, v9, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->categoryHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .line 198
    .local v6, "categoryHolder":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget v2, v9, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->positionInList:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/food/foodpick/expandable/Expandable;

    .line 200
    .local v8, "expandableListItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/Expandable;
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;

    invoke-direct {v4, v6}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V

    .line 201
    .local v4, "listItemStateHolder":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;
    invoke-interface {v8}, Lcom/sec/android/app/shealth/food/foodpick/expandable/Expandable;->getChildList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .local v1, "child":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    move-object v0, p0

    move-object v2, p4

    move-object v3, p5

    move v5, p1

    .line 202
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getViewFromListItem(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Landroid/view/View;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;I)Landroid/view/View;

    move-result-object v7

    .line 208
    .local v7, "childview":Landroid/view/View;
    return-object v7

    .line 193
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getChildrenCount(I)I
    .locals 6
    .param p1, "groupPosition"    # I

    .prologue
    const/4 v3, 0x0

    .line 100
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getItemTypeForPosition(I)Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;

    move-result-object v2

    .line 101
    .local v2, "typedPosition":Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;
    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$3;->$SwitchMap$com$sec$android$app$shealth$food$foodpick$expandable$FoodListExpandableAdapter$ItemType:[I

    iget-object v5, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->itemType:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 114
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Not supported item type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->itemType:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 105
    :pswitch_0
    iget-object v0, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->categoryHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .line 106
    .local v0, "categoryHolder":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->areItemsExpandable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 107
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    iget v4, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->positionInList:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/Expandable;

    .line 109
    .local v1, "expandableListItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/Expandable;
    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/Expandable;->getChildList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .line 111
    .end local v0    # "categoryHolder":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    .end local v1    # "expandableListItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/Expandable;
    :cond_0
    :pswitch_1
    return v3

    .line 101
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 134
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 4

    .prologue
    .line 87
    const/4 v0, 0x0

    .line 88
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 89
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v0, v3

    .line 90
    goto :goto_0

    .line 91
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->isHeaderVisible()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 93
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    add-int/2addr v0, v3

    .line 95
    :cond_1
    return v0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 144
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupType(I)I
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 223
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getItemTypeForPosition(I)Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->itemType:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getGroupTypeCount()I
    .locals 1

    .prologue
    .line 218
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;->values()[Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getItemTypeForPosition(I)Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;

    move-result-object v9

    .line 161
    .local v9, "typedPosition":Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$3;->$SwitchMap$com$sec$android$app$shealth$food$foodpick$expandable$FoodListExpandableAdapter$ItemType:[I

    iget-object v2, v9, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->itemType:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 184
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not supported item type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v9, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->itemType:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :pswitch_0
    move-object v7, p3

    .line 164
    .local v7, "groupView":Landroid/view/View;
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mContext:Landroid/content/Context;

    iget-object v2, v9, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->categoryHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->getCaptionId()I

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getCaptionForCategoryInListView(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 166
    .local v8, "headerCaption":Ljava/lang/String;
    if-nez v7, :cond_0

    .line 167
    invoke-static {p4, v8}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->inflateHeader(Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v7

    .line 186
    .end local v8    # "headerCaption":Ljava/lang/String;
    .end local p4    # "parent":Landroid/view/ViewGroup;
    :goto_0
    return-object v7

    .line 169
    .restart local v8    # "headerCaption":Ljava/lang/String;
    .restart local p4    # "parent":Landroid/view/ViewGroup;
    :cond_0
    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->updateHeaderTitle(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .line 173
    .end local v7    # "groupView":Landroid/view/View;
    .end local v8    # "headerCaption":Ljava/lang/String;
    :pswitch_1
    iget-object v6, v9, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->categoryHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .line 174
    .local v6, "categoryHolder":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget v2, v9, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->positionInList:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .line 176
    .local v1, "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;

    invoke-direct {v4, v6}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V

    .line 177
    .local v4, "listItemStateHolder":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;
    invoke-virtual {v4, p2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->setExpanded(Z)V

    move-object v0, p0

    move-object v2, p3

    move-object v3, p4

    move v5, p1

    .line 178
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getViewFromListItem(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Landroid/view/View;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;I)Landroid/view/View;

    move-result-object v7

    .line 180
    .restart local v7    # "groupView":Landroid/view/View;
    check-cast p4, Landroid/widget/ExpandableListView;

    .end local p4    # "parent":Landroid/view/ViewGroup;
    invoke-direct {p0, v1, p4, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->initExpandableIndicatorListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Landroid/widget/ExpandableListView;I)V

    .line 181
    invoke-direct {p0, v1, v6}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->initFavoriteButtonListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V

    goto :goto_0

    .line 161
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getTotalChildCount()I
    .locals 3

    .prologue
    .line 124
    const/4 v1, 0x0

    .line 125
    .local v1, "mTotalCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getGroupCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 127
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getChildrenCount(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129
    :cond_0
    return v1
.end method

.method protected getViewFromListItem(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Landroid/view/View;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;I)Landroid/view/View;
    .locals 2
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "stateHolder"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;
    .param p5, "groupPosition"    # I

    .prologue
    .line 321
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p1, v1, p2, p3, p4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getView(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/view/View;

    move-result-object v0

    .line 322
    .local v0, "listItemView":Landroid/view/View;
    const v1, 0x7f0d0044

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 323
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 324
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 325
    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x1

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 213
    const/4 v0, 0x1

    return v0
.end method

.method public isItemsListEmpty()Z
    .locals 1

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getAllGroupItems()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isSingleCategoryCaptionVisible()Z
    .locals 1

    .prologue
    .line 307
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mIsSingleCategoryCaptionVisible:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 466
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnItemClickListeners:Ljava/util/Set;

    if-eqz v2, :cond_0

    .line 467
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnItemClickListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener;

    .line 468
    .local v1, "listener":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    const v2, 0x7f0d0044

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener;->onItemClick(Ljava/lang/Object;)V

    goto :goto_0

    .line 471
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 475
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnItemLongClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener;

    if-eqz v0, :cond_0

    .line 476
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnItemLongClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener;

    const v0, 0x7f0d0044

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener;->onItemLongClick(Ljava/lang/Object;)Z

    move-result v0

    .line 478
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected performClickOnFavoriteButton(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V
    .locals 6
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    .param p2, "currentCategory"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 395
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->isFavorite()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 396
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->removeFromFavorites()V

    .line 397
    const v2, 0x7f0909b9

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->makeFormattedToast(I[Ljava/lang/String;)V

    .line 398
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->containsFavorites()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 399
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 410
    :cond_0
    return-void

    .line 402
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->addToFavorites()V

    .line 403
    const v2, 0x7f0909ba

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->makeFormattedToast(I[Ljava/lang/String;)V

    .line 404
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .line 405
    .local v0, "category":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->containsFavorites()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 406
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->getFoodCategoryListItemsCreator()Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;->getFoodListItems(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public removeAllListeners()V
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnFavoriteClickListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnItemClickListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 484
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnItemLongClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener;

    .line 485
    return-void
.end method

.method public removeOnFavoriteClickListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 550
    .local p1, "onFavoriteClickListener":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnFavoriteClickListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 551
    return-void
.end method

.method public restoreCheckedStateForListItems(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 238
    .local p1, "savedList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getAllGroupItems()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .line 239
    .local v2, "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getListItemId()J

    move-result-wide v3

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getListItemType()Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    move-result-object v5

    invoke-direct {v0, v3, v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;-><init>(JLcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;)V

    .line 240
    .local v0, "dataHolder":Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;
    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->setChecked(Z)V

    goto :goto_0

    .line 242
    .end local v0    # "dataHolder":Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;
    .end local v2    # "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    :cond_0
    return-void
.end method

.method public setCheckable(Z)V
    .locals 3
    .param p1, "isCheckable"    # Z

    .prologue
    .line 250
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .line 251
    .local v0, "category":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsCheckable(Z)V

    goto :goto_0

    .line 253
    .end local v0    # "category":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    :cond_0
    return-void
.end method

.method public setIsSingleCategoryCaptionVisible(Z)V
    .locals 0
    .param p1, "isCategoryCaptionVisible"    # Z

    .prologue
    .line 316
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mIsSingleCategoryCaptionVisible:Z

    .line 317
    return-void
.end method

.method public setItemsMap(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 230
    .local p1, "allItems":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mItemsMap:Ljava/util/Map;

    .line 231
    return-void
.end method

.method public setOnItemLongClickListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 510
    .local p1, "onItemLongClickListener":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnItemLongClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener;

    .line 511
    return-void
.end method
