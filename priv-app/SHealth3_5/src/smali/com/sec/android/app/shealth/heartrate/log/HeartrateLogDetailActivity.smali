.class public Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HeartrateLogDetailActivity.java"


# static fields
.field private static final CHOOSE_TAG:I = 0x93


# instance fields
.field private actionbarClickListener:Landroid/view/View$OnClickListener;

.field private mAccessoryType:Landroid/widget/TextView;

.field private mBpm:Landroid/widget/TextView;

.field private mBpmLayout:Landroid/widget/LinearLayout;

.field private mBpmRange:Landroid/widget/TextView;

.field private mComment:Landroid/widget/TextView;

.field private mCommentHead:Landroid/view/View;

.field private mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mIconId:I

.field private mNotes:Landroid/widget/TextView;

.field private mPref:Landroid/content/SharedPreferences;

.field private mTag:Landroid/widget/TextView;

.field private mTagName:Ljava/lang/String;

.field private mTimeDate:Landroid/widget/TextView;

.field private memoIcon:Landroid/widget/ImageView;

.field private mtagLayout:Landroid/widget/LinearLayout;

.field private tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

.field private width:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mIconId:I

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTagName:Ljava/lang/String;

    .line 80
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->width:[I

    .line 220
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    return-void

    .line 80
    :array_0
    .array-data 4
        0x57
        0x41
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    return-object v0
.end method

.method private initView(Ljava/lang/String;)V
    .locals 14
    .param p1, "heartrateId"    # Ljava/lang/String;

    .prologue
    const v13, 0x7f080536

    const v12, 0x7f0900d2

    const/4 v11, 0x1

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 91
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 92
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v5, p1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataById(Ljava/lang/String;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    .line 94
    const v5, 0x7f08052e

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    .line 95
    const v5, 0x7f08052f

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    .line 96
    const v5, 0x7f080531

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mBpm:Landroid/widget/TextView;

    .line 97
    const v5, 0x7f080532

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mBpmRange:Landroid/widget/TextView;

    .line 98
    const v5, 0x7f080539

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mComment:Landroid/widget/TextView;

    .line 99
    const v5, 0x7f080537

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mNotes:Landroid/widget/TextView;

    .line 100
    invoke-virtual {p0, v13}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mCommentHead:Landroid/view/View;

    .line 101
    const v5, 0x7f080530

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mBpmLayout:Landroid/widget/LinearLayout;

    .line 102
    const v5, 0x7f080534

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mtagLayout:Landroid/widget/LinearLayout;

    .line 104
    const v5, 0x7f080535

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTag:Landroid/widget/TextView;

    .line 105
    const v5, 0x7f0803e4

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->memoIcon:Landroid/widget/ImageView;

    .line 107
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserDeviceId()Ljava/lang/String;

    move-result-object v4

    .line 108
    .local v4, "userDeviceId":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 109
    const-string v5, "10008"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 110
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v6

    invoke-static {p0, v6, v7}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getDateTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getMaleOrFemale()I

    move-result v6

    invoke-virtual {v5, v6, v9}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile(II)[I

    move-result-object v3

    .line 120
    .local v3, "percentile":[I
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 121
    .local v1, "mHeartRate":I
    sget-object v5, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v5, v6, :cond_3

    .line 123
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mBpmRange:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mBpm:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0, v12}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getComment()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getComment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_5

    .line 145
    new-instance v2, Landroid/text/SpannableString;

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getComment()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 146
    .local v2, "mSpannableString":Landroid/text/SpannableString;
    new-instance v5, Landroid/text/style/LeadingMarginSpan$Standard;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a06a7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-direct {v5, v6, v9}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(II)V

    invoke-virtual {v2, v5, v9, v11, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 147
    const v5, 0x7f080538

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 148
    .local v0, "MemoLayout":Landroid/widget/RelativeLayout;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f09007c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09020b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getComment()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mComment:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->memoIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 158
    .end local v0    # "MemoLayout":Landroid/widget/RelativeLayout;
    .end local v2    # "mSpannableString":Landroid/text/SpannableString;
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mCommentHead:Landroid/view/View;

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    .line 159
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mNotes:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTag(J)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090c0d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTagName:Ljava/lang/String;

    .line 163
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    if-eqz v5, :cond_1

    .line 164
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget v5, v5, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mIconId:I

    .line 165
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget-object v5, v5, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v5, v6, :cond_6

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget v6, v6, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTagName:Ljava/lang/String;

    .line 172
    :cond_1
    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mtagLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 173
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTag:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 174
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTag:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTagName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    invoke-virtual {p0, v13}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 176
    return-void

    .line 113
    .end local v1    # "mHeartRate":I
    .end local v3    # "percentile":[I
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090a8c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v11, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAccessoryName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 128
    .restart local v1    # "mHeartRate":I
    .restart local v3    # "percentile":[I
    :cond_3
    aget v5, v3, v9

    if-lt v1, v5, :cond_4

    aget v5, v3, v11

    if-gt v1, v5, :cond_4

    .line 129
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mBpmRange:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mBpm:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0, v12}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mBpmRange:Landroid/widget/TextView;

    const v6, 0x7f090c21

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 136
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mBpmRange:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 137
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mBpm:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0, v12}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mBpmRange:Landroid/widget/TextView;

    const v6, 0x7f090c20

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 154
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->memoIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 155
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mComment:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 168
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget-object v5, v5, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTagName:Ljava/lang/String;

    goto/16 :goto_3
.end method

.method private showDeletePopup()V
    .locals 3

    .prologue
    .line 247
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09078e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$2;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 257
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 212
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 213
    const/4 v0, 0x0

    .line 214
    .local v0, "emptyActionItemTextId":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f09005b

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v3, 0x0

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f020299

    const v6, 0x7f090040

    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 217
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    const v0, 0x7f03013d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->setContentView(I)V

    .line 86
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mPref:Landroid/content/SharedPreferences;

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "HEART_RATE_ID_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->initView(Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100015

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 193
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 198
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 207
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 200
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.heartrate"

    const-string v2, "HR10"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->prepareShareView()V

    goto :goto_0

    .line 204
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->showDeletePopup()V

    goto :goto_0

    .line 198
    :pswitch_data_0
    .packed-switch 0x7f080c8d
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 180
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mPref:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "save_to_logs"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 183
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v1, "save_to_logs"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 184
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->finish()V

    .line 187
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getDateTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    return-void
.end method
