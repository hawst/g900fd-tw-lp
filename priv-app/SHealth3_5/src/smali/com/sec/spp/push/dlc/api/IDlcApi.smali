.class public interface abstract Lcom/sec/spp/push/dlc/api/IDlcApi;
.super Ljava/lang/Object;
.source "IDlcApi.java"


# static fields
.field public static final LEN_OF_CATEGORY:I = 0x3

.field public static final LEN_OF_SVC_CODE:I = 0x3

.field public static final LEN_OF_USER_ID:I = 0xa

.field public static final MAX_LEN_OF_ACTIVITY:I = 0x14

.field public static final MAX_LEN_OF_BODY:I = 0x400

.field public static final RC_APPLICATION_BLOCKED:I = -0x3

.field public static final RC_INVALID_PARAMETER:I = -0x1

.field public static final RC_NOT_PERMITTED:I = -0x4

.field public static final RC_NOT_REGISTERED:I = -0x6

.field public static final RC_SUCCESS:I = 0x0

.field public static final RC_SVC_UNAVAILABLE:I = -0x2

.field public static final RC_URGENT_NOT_ALLOWED:I = -0x5
