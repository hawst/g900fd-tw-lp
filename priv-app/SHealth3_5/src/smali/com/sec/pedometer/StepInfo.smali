.class public Lcom/sec/pedometer/StepInfo;
.super Ljava/lang/Object;
.source "StepInfo.java"


# instance fields
.field private calorie:F

.field private stepCount:J

.field private stepLength:F

.field private stepSpeed:F

.field private stepStatus:I

.field private totalStepLength:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/pedometer/StepInfo;->stepCount:J

    .line 13
    iput v2, p0, Lcom/sec/pedometer/StepInfo;->stepLength:F

    .line 14
    iput v2, p0, Lcom/sec/pedometer/StepInfo;->totalStepLength:F

    .line 15
    iput v2, p0, Lcom/sec/pedometer/StepInfo;->stepSpeed:F

    .line 16
    iput v2, p0, Lcom/sec/pedometer/StepInfo;->calorie:F

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/pedometer/StepInfo;->stepStatus:I

    .line 18
    return-void
.end method


# virtual methods
.method public getCalories()F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/pedometer/StepInfo;->calorie:F

    return v0
.end method

.method public getStepCount()J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/sec/pedometer/StepInfo;->stepCount:J

    return-wide v0
.end method

.method public getStepLength()F
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/pedometer/StepInfo;->stepLength:F

    return v0
.end method

.method public getStepSpeed()F
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/pedometer/StepInfo;->stepSpeed:F

    return v0
.end method

.method public getStepStatus()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/pedometer/StepInfo;->stepStatus:I

    return v0
.end method

.method public getTotalStepLength()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/pedometer/StepInfo;->totalStepLength:F

    return v0
.end method
