.class public Lcom/sec/pedometer/UserInfo;
.super Ljava/lang/Object;
.source "UserInfo.java"


# instance fields
.field private gender:I

.field private height:F

.field private weight:F


# direct methods
.method public constructor <init>(FFI)V
    .locals 0
    .param p1, "height"    # F
    .param p2, "weight"    # F
    .param p3, "gender"    # I

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput p1, p0, Lcom/sec/pedometer/UserInfo;->height:F

    .line 9
    iput p2, p0, Lcom/sec/pedometer/UserInfo;->weight:F

    .line 10
    iput p3, p0, Lcom/sec/pedometer/UserInfo;->gender:I

    .line 11
    return-void
.end method


# virtual methods
.method public getGender()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/pedometer/UserInfo;->gender:I

    return v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/pedometer/UserInfo;->height:F

    return v0
.end method

.method public getWeight()F
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/sec/pedometer/UserInfo;->weight:F

    return v0
.end method

.method public setGender(I)V
    .locals 0
    .param p1, "gender"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/sec/pedometer/UserInfo;->gender:I

    .line 29
    return-void
.end method

.method public setHeight(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 16
    iput p1, p0, Lcom/sec/pedometer/UserInfo;->height:F

    .line 17
    return-void
.end method

.method public setWeight(F)V
    .locals 0
    .param p1, "weight"    # F

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/pedometer/UserInfo;->weight:F

    .line 23
    return-void
.end method
