.class public Lcom/sec/pedometer/Pedometer;
.super Ljava/lang/Object;
.source "Pedometer.java"


# static fields
.field private static pedometer:Lcom/sec/pedometer/Pedometer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4
    new-instance v0, Lcom/sec/pedometer/Pedometer;

    invoke-direct {v0}, Lcom/sec/pedometer/Pedometer;-><init>()V

    sput-object v0, Lcom/sec/pedometer/Pedometer;->pedometer:Lcom/sec/pedometer/Pedometer;

    .line 6
    const-string v0, "Pedometer"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 7
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private native PedometerDetectStep([FFI)Z
.end method

.method private native PedometerFin()V
.end method

.method private native PedometerGetStepInfo(Lcom/sec/pedometer/StepInfo;)V
.end method

.method private native PedometerGetUserInfo(Lcom/sec/pedometer/UserInfo;)V
.end method

.method private native PedometerInit(Lcom/sec/pedometer/UserInfo;)V
.end method

.method private native PedometerSetUserInfo(Lcom/sec/pedometer/UserInfo;)V
.end method

.method private native PedometerVersion()I
.end method

.method public static getInstance()Lcom/sec/pedometer/Pedometer;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/pedometer/Pedometer;->pedometer:Lcom/sec/pedometer/Pedometer;

    return-object v0
.end method


# virtual methods
.method public DetectStep([FFI)Z
    .locals 1
    .param p1, "acc"    # [F
    .param p2, "baro"    # F
    .param p3, "samplingtime"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/pedometer/Pedometer;->PedometerDetectStep([FFI)Z

    move-result v0

    return v0
.end method

.method public Finalize()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/pedometer/Pedometer;->PedometerFin()V

    .line 42
    return-void
.end method

.method public GetStepInfo(Lcom/sec/pedometer/StepInfo;)V
    .locals 0
    .param p1, "stepinfo"    # Lcom/sec/pedometer/StepInfo;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/pedometer/Pedometer;->PedometerGetStepInfo(Lcom/sec/pedometer/StepInfo;)V

    .line 30
    return-void
.end method

.method public GetUserInfo(Lcom/sec/pedometer/UserInfo;)V
    .locals 0
    .param p1, "userinfo"    # Lcom/sec/pedometer/UserInfo;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/pedometer/Pedometer;->PedometerGetUserInfo(Lcom/sec/pedometer/UserInfo;)V

    .line 36
    return-void
.end method

.method public GetVersion()I
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/pedometer/Pedometer;->PedometerVersion()I

    move-result v0

    return v0
.end method

.method public Init(Lcom/sec/pedometer/UserInfo;)V
    .locals 0
    .param p1, "userinfo"    # Lcom/sec/pedometer/UserInfo;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/pedometer/Pedometer;->PedometerInit(Lcom/sec/pedometer/UserInfo;)V

    .line 24
    return-void
.end method

.method public SetUserInfo(Lcom/sec/pedometer/UserInfo;)V
    .locals 0
    .param p1, "userinfo"    # Lcom/sec/pedometer/UserInfo;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/pedometer/Pedometer;->PedometerSetUserInfo(Lcom/sec/pedometer/UserInfo;)V

    .line 33
    return-void
.end method
