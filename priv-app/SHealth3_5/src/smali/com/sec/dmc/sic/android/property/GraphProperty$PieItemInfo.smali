.class public Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
.super Ljava/lang/Object;
.source "GraphProperty.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/sic/android/property/GraphProperty;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PieItemInfo"
.end annotation


# instance fields
.field color:I

.field labelStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field labelVisible:Z

.field percentLabel:Ljava/lang/String;

.field separatorColor:I

.field separatorWidth:I

.field textCenterRadiusRatio:F

.field textGuidelineVisible:Z

.field final synthetic this$0:Lcom/sec/dmc/sic/android/property/GraphProperty;

.field valuePercentStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field valueStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field valueVisible:Z


# direct methods
.method constructor <init>(Lcom/sec/dmc/sic/android/property/GraphProperty;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 95
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->this$0:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->color:I

    .line 97
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 98
    iput-boolean v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelVisible:Z

    .line 100
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textCenterRadiusRatio:F

    .line 101
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 102
    iput-boolean v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueVisible:Z

    .line 104
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valuePercentStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 106
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorColor:I

    .line 107
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorWidth:I

    .line 109
    iput-boolean v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textGuidelineVisible:Z

    .line 110
    return-void
.end method

.method private getOuterType()Lcom/sec/dmc/sic/android/property/GraphProperty;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->this$0:Lcom/sec/dmc/sic/android/property/GraphProperty;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 140
    if-ne p0, p1, :cond_1

    .line 184
    :cond_0
    :goto_0
    return v1

    .line 142
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 143
    goto :goto_0

    .line 144
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 145
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 146
    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 147
    .local v0, "other":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->getOuterType()Lcom/sec/dmc/sic/android/property/GraphProperty;

    move-result-object v3

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->getOuterType()Lcom/sec/dmc/sic/android/property/GraphProperty;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/dmc/sic/android/property/GraphProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 148
    goto :goto_0

    .line 149
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->color:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->color:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 150
    goto :goto_0

    .line 151
    :cond_5
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_6

    .line 152
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_7

    move v1, v2

    .line 153
    goto :goto_0

    .line 154
    :cond_6
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 155
    goto :goto_0

    .line 156
    :cond_7
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelVisible:Z

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 157
    goto :goto_0

    .line 158
    :cond_8
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->percentLabel:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 159
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->percentLabel:Ljava/lang/String;

    if-eqz v3, :cond_a

    move v1, v2

    .line 160
    goto :goto_0

    .line 161
    :cond_9
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->percentLabel:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->percentLabel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 162
    goto :goto_0

    .line 163
    :cond_a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorColor:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorColor:I

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 164
    goto :goto_0

    .line 165
    :cond_b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorWidth:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorWidth:I

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 166
    goto :goto_0

    .line 167
    :cond_c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textCenterRadiusRatio:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 168
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textCenterRadiusRatio:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 167
    if-eq v3, v4, :cond_d

    move v1, v2

    .line 169
    goto :goto_0

    .line 170
    :cond_d
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textGuidelineVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textGuidelineVisible:Z

    if-eq v3, v4, :cond_e

    move v1, v2

    .line 171
    goto/16 :goto_0

    .line 172
    :cond_e
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valuePercentStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_f

    .line 173
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valuePercentStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_10

    move v1, v2

    .line 174
    goto/16 :goto_0

    .line 175
    :cond_f
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valuePercentStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valuePercentStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 176
    goto/16 :goto_0

    .line 177
    :cond_10
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_11

    .line 178
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_12

    move v1, v2

    .line 179
    goto/16 :goto_0

    .line 180
    :cond_11
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    move v1, v2

    .line 181
    goto/16 :goto_0

    .line 182
    :cond_12
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueVisible:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 183
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v5, 0x4d5

    const/16 v4, 0x4cf

    const/4 v3, 0x0

    .line 114
    const/16 v0, 0x1f

    .line 115
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 116
    .local v1, "result":I
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->getOuterType()Lcom/sec/dmc/sic/android/property/GraphProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->hashCode()I

    move-result v2

    add-int/lit8 v1, v2, 0x1f

    .line 117
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->color:I

    add-int v1, v2, v6

    .line 118
    mul-int/lit8 v6, v1, 0x1f

    .line 119
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_0

    move v2, v3

    .line 118
    :goto_0
    add-int v1, v6, v2

    .line 120
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelVisible:Z

    if-eqz v2, :cond_1

    move v2, v4

    :goto_1
    add-int v1, v6, v2

    .line 121
    mul-int/lit8 v6, v1, 0x1f

    .line 122
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->percentLabel:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    .line 121
    :goto_2
    add-int v1, v6, v2

    .line 123
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorColor:I

    add-int v1, v2, v6

    .line 124
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorWidth:I

    add-int v1, v2, v6

    .line 125
    mul-int/lit8 v2, v1, 0x1f

    .line 126
    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textCenterRadiusRatio:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    .line 125
    add-int v1, v2, v6

    .line 127
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textGuidelineVisible:Z

    if-eqz v2, :cond_3

    move v2, v4

    :goto_3
    add-int v1, v6, v2

    .line 128
    mul-int/lit8 v6, v1, 0x1f

    .line 130
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valuePercentStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_4

    move v2, v3

    .line 128
    :goto_4
    add-int v1, v6, v2

    .line 132
    mul-int/lit8 v2, v1, 0x1f

    .line 133
    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v6, :cond_5

    .line 132
    :goto_5
    add-int v1, v2, v3

    .line 134
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueVisible:Z

    if-eqz v3, :cond_6

    :goto_6
    add-int v1, v2, v4

    .line 135
    return v1

    .line 119
    :cond_0
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    move v2, v5

    .line 120
    goto :goto_1

    .line 122
    :cond_2
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->percentLabel:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    move v2, v5

    .line 127
    goto :goto_3

    .line 130
    :cond_4
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valuePercentStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 131
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto :goto_4

    .line 133
    :cond_5
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v3

    goto :goto_5

    :cond_6
    move v4, v5

    .line 134
    goto :goto_6
.end method
