.class public Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "ValueMarkingProperty.java"


# instance fields
.field public final VALUE_MARKING_HANDLER_OVER_VISIBLE:I

.field public final VALUE_MARKING_NORMAL_VISIBLE:I

.field public final VALUE_MARKING_SHOW_ALL:I

.field public final VALUE_MARKING_SHOW_NOT_ALL:I

.field private mBitmapList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mEnableHittest:Z

.field private mHandlerOverColorA:F

.field private mHandlerOverColorB:F

.field private mHandlerOverColorG:F

.field private mHandlerOverColorR:F

.field private mHandlerOverInRangeImage:Landroid/graphics/Bitmap;

.field private mHandlerOverOutRangeImage:Landroid/graphics/Bitmap;

.field private mInRangeColorA:F

.field private mInRangeColorB:F

.field private mInRangeColorG:F

.field private mInRangeColorR:F

.field private mInRangeImage:Landroid/graphics/Bitmap;

.field private mListener:Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

.field private mMarkingShowOption:I

.field private mNormalColorA:F

.field private mNormalColorB:F

.field private mNormalColorG:F

.field private mNormalColorR:F

.field private mNormalImage:Landroid/graphics/Bitmap;

.field private mNumBitmaps:I

.field private mPressedColorA:F

.field private mPressedColorB:F

.field private mPressedColorG:F

.field private mPressedColorR:F

.field private mPressedImage:Landroid/graphics/Bitmap;

.field private mRadius:F

.field private mSpotShowList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mValueMarkingID:I

.field private mVisibleOption:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 66
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 51
    iput v5, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->VALUE_MARKING_NORMAL_VISIBLE:I

    .line 52
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->VALUE_MARKING_HANDLER_OVER_VISIBLE:I

    .line 55
    iput v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->VALUE_MARKING_SHOW_ALL:I

    .line 56
    iput v5, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->VALUE_MARKING_SHOW_NOT_ALL:I

    .line 59
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mBitmapList:Ljava/util/ArrayList;

    .line 61
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mSpotShowList:Ljava/util/ArrayList;

    .line 62
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    .line 69
    iput-boolean v5, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->enable:Z

    .line 70
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->visible:Z

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mValueMarkingID:I

    .line 74
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mRadius:F

    .line 76
    iput v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorR:F

    .line 77
    iput v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorG:F

    .line 78
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorB:F

    .line 79
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorA:F

    .line 81
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorR:F

    .line 82
    iput v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorG:F

    .line 83
    iput v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorB:F

    .line 84
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorA:F

    .line 86
    iput v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorR:F

    .line 87
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorG:F

    .line 88
    iput v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorB:F

    .line 89
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorA:F

    .line 91
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorR:F

    .line 92
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorG:F

    .line 93
    iput v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorB:F

    .line 94
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorA:F

    .line 96
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalImage:Landroid/graphics/Bitmap;

    .line 97
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeImage:Landroid/graphics/Bitmap;

    .line 98
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverInRangeImage:Landroid/graphics/Bitmap;

    .line 99
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverOutRangeImage:Landroid/graphics/Bitmap;

    .line 100
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedImage:Landroid/graphics/Bitmap;

    .line 102
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mVisibleOption:I

    .line 105
    iput v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mMarkingShowOption:I

    .line 106
    iput v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNumBitmaps:I

    .line 108
    iput-boolean v5, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mEnableHittest:Z

    .line 109
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 407
    if-ne p0, p1, :cond_1

    .line 525
    :cond_0
    :goto_0
    return v1

    .line 409
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 410
    goto :goto_0

    .line 411
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 412
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 413
    check-cast v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    .line 414
    .local v0, "other":Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 416
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 418
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 420
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 422
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mBitmapList:Ljava/util/ArrayList;

    if-nez v3, :cond_4

    .line 423
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mBitmapList:Ljava/util/ArrayList;

    if-eqz v3, :cond_5

    move v1, v2

    .line 424
    goto :goto_0

    .line 425
    :cond_4
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mBitmapList:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 426
    goto :goto_0

    .line 427
    :cond_5
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mEnableHittest:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mEnableHittest:Z

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 428
    goto :goto_0

    .line 429
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorA:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 430
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 429
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 431
    goto :goto_0

    .line 432
    :cond_7
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 433
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 432
    if-eq v3, v4, :cond_8

    move v1, v2

    .line 434
    goto :goto_0

    .line 435
    :cond_8
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 436
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 435
    if-eq v3, v4, :cond_9

    move v1, v2

    .line 437
    goto :goto_0

    .line 438
    :cond_9
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 439
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 438
    if-eq v3, v4, :cond_a

    move v1, v2

    .line 440
    goto/16 :goto_0

    .line 441
    :cond_a
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverInRangeImage:Landroid/graphics/Bitmap;

    if-nez v3, :cond_b

    .line 442
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverInRangeImage:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_c

    move v1, v2

    .line 443
    goto/16 :goto_0

    .line 444
    :cond_b
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverInRangeImage:Landroid/graphics/Bitmap;

    .line 445
    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverInRangeImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 446
    goto/16 :goto_0

    .line 447
    :cond_c
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverOutRangeImage:Landroid/graphics/Bitmap;

    if-nez v3, :cond_d

    .line 448
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverOutRangeImage:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_e

    move v1, v2

    .line 449
    goto/16 :goto_0

    .line 450
    :cond_d
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverOutRangeImage:Landroid/graphics/Bitmap;

    .line 451
    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverOutRangeImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 452
    goto/16 :goto_0

    .line 453
    :cond_e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorA:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 454
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 453
    if-eq v3, v4, :cond_f

    move v1, v2

    .line 455
    goto/16 :goto_0

    .line 456
    :cond_f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 457
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 456
    if-eq v3, v4, :cond_10

    move v1, v2

    .line 458
    goto/16 :goto_0

    .line 459
    :cond_10
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 460
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 459
    if-eq v3, v4, :cond_11

    move v1, v2

    .line 461
    goto/16 :goto_0

    .line 462
    :cond_11
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 463
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 462
    if-eq v3, v4, :cond_12

    move v1, v2

    .line 464
    goto/16 :goto_0

    .line 465
    :cond_12
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeImage:Landroid/graphics/Bitmap;

    if-nez v3, :cond_13

    .line 466
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeImage:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_14

    move v1, v2

    .line 467
    goto/16 :goto_0

    .line 468
    :cond_13
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeImage:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    move v1, v2

    .line 469
    goto/16 :goto_0

    .line 470
    :cond_14
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    if-nez v3, :cond_15

    .line 471
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    if-eqz v3, :cond_16

    move v1, v2

    .line 472
    goto/16 :goto_0

    .line 473
    :cond_15
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    move v1, v2

    .line 474
    goto/16 :goto_0

    .line 475
    :cond_16
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mMarkingShowOption:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mMarkingShowOption:I

    if-eq v3, v4, :cond_17

    move v1, v2

    .line 476
    goto/16 :goto_0

    .line 477
    :cond_17
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorA:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 478
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 477
    if-eq v3, v4, :cond_18

    move v1, v2

    .line 479
    goto/16 :goto_0

    .line 480
    :cond_18
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 481
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 480
    if-eq v3, v4, :cond_19

    move v1, v2

    .line 482
    goto/16 :goto_0

    .line 483
    :cond_19
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 484
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 483
    if-eq v3, v4, :cond_1a

    move v1, v2

    .line 485
    goto/16 :goto_0

    .line 486
    :cond_1a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 487
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 486
    if-eq v3, v4, :cond_1b

    move v1, v2

    .line 488
    goto/16 :goto_0

    .line 489
    :cond_1b
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalImage:Landroid/graphics/Bitmap;

    if-nez v3, :cond_1c

    .line 490
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalImage:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_1d

    move v1, v2

    .line 491
    goto/16 :goto_0

    .line 492
    :cond_1c
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalImage:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    move v1, v2

    .line 493
    goto/16 :goto_0

    .line 494
    :cond_1d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNumBitmaps:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNumBitmaps:I

    if-eq v3, v4, :cond_1e

    move v1, v2

    .line 495
    goto/16 :goto_0

    .line 496
    :cond_1e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorA:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 497
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 496
    if-eq v3, v4, :cond_1f

    move v1, v2

    .line 498
    goto/16 :goto_0

    .line 499
    :cond_1f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 500
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 499
    if-eq v3, v4, :cond_20

    move v1, v2

    .line 501
    goto/16 :goto_0

    .line 502
    :cond_20
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 503
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 502
    if-eq v3, v4, :cond_21

    move v1, v2

    .line 504
    goto/16 :goto_0

    .line 505
    :cond_21
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 506
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 505
    if-eq v3, v4, :cond_22

    move v1, v2

    .line 507
    goto/16 :goto_0

    .line 508
    :cond_22
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedImage:Landroid/graphics/Bitmap;

    if-nez v3, :cond_23

    .line 509
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedImage:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_24

    move v1, v2

    .line 510
    goto/16 :goto_0

    .line 511
    :cond_23
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedImage:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_24

    move v1, v2

    .line 512
    goto/16 :goto_0

    .line 513
    :cond_24
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mRadius:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 514
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mRadius:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 513
    if-eq v3, v4, :cond_25

    move v1, v2

    .line 515
    goto/16 :goto_0

    .line 516
    :cond_25
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mSpotShowList:Ljava/util/ArrayList;

    if-nez v3, :cond_26

    .line 517
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mSpotShowList:Ljava/util/ArrayList;

    if-eqz v3, :cond_27

    move v1, v2

    .line 518
    goto/16 :goto_0

    .line 519
    :cond_26
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mSpotShowList:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mSpotShowList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_27

    move v1, v2

    .line 520
    goto/16 :goto_0

    .line 521
    :cond_27
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mValueMarkingID:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mValueMarkingID:I

    if-eq v3, v4, :cond_28

    move v1, v2

    .line 522
    goto/16 :goto_0

    .line 523
    :cond_28
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mVisibleOption:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mVisibleOption:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 524
    goto/16 :goto_0
.end method

.method public getBitmapList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mBitmapList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHandlerOverColorAlpha()F
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorA:F

    return v0
.end method

.method public getHandlerOverColorB()F
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorB:F

    return v0
.end method

.method public getHandlerOverColorG()F
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorG:F

    return v0
.end method

.method public getHandlerOverColorR()F
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorR:F

    return v0
.end method

.method public getHandlerOverInRangeImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverInRangeImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getHandlerOverOutRangleImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverOutRangeImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getInRangeColorAlpha()F
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorA:F

    return v0
.end method

.method public getInRangeColorB()F
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorB:F

    return v0
.end method

.method public getInRangeColorG()F
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorG:F

    return v0
.end method

.method public getInRangeColorR()F
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorR:F

    return v0
.end method

.method public getInRangeImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getListener()Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    return-object v0
.end method

.method public getMarkingShowOption()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mMarkingShowOption:I

    return v0
.end method

.method public getNormalColorAlpha()F
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorA:F

    return v0
.end method

.method public getNormalColorB()F
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorB:F

    return v0
.end method

.method public getNormalColorG()F
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorG:F

    return v0
.end method

.method public getNormalColorR()F
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorR:F

    return v0
.end method

.method public getNormalImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getPressedColorAlpha()F
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorA:F

    return v0
.end method

.method public getPressedColorB()F
    .locals 1

    .prologue
    .line 265
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorB:F

    return v0
.end method

.method public getPressedColorG()F
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorG:F

    return v0
.end method

.method public getPressedColorR()F
    .locals 1

    .prologue
    .line 255
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorR:F

    return v0
.end method

.method public getPressedImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getRadius()F
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mRadius:F

    return v0
.end method

.method public getSpotShowList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mSpotShowList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getValueMarkingID()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mValueMarkingID:I

    return v0
.end method

.method public getVisibleOption()I
    .locals 1

    .prologue
    .line 309
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mVisibleOption:I

    return v0
.end method

.method public handlerOverColor(FFFF)V
    .locals 0
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 200
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorR:F

    .line 201
    iput p2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorG:F

    .line 202
    iput p3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorB:F

    .line 203
    iput p4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorA:F

    .line 204
    return-void
.end method

.method public handlerOverColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 208
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorR:F

    .line 209
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorG:F

    .line 210
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorB:F

    .line 211
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorA:F

    .line 212
    return-void
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 354
    const/16 v0, 0x1f

    .line 355
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 356
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    add-int/lit8 v1, v2, 0x2

    .line 357
    mul-int/lit8 v2, v1, 0x1f

    add-int/lit8 v1, v2, 0x1

    .line 358
    mul-int/lit8 v2, v1, 0x1f

    add-int/lit8 v1, v2, 0x0

    .line 359
    mul-int/lit8 v2, v1, 0x1f

    add-int/lit8 v1, v2, 0x1

    .line 360
    mul-int/lit8 v4, v1, 0x1f

    .line 361
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mBitmapList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    move v2, v3

    .line 360
    :goto_0
    add-int v1, v4, v2

    .line 362
    mul-int/lit8 v4, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mEnableHittest:Z

    if-eqz v2, :cond_1

    const/16 v2, 0x4cf

    :goto_1
    add-int v1, v4, v2

    .line 363
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 364
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 365
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 366
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 367
    mul-int/lit8 v4, v1, 0x1f

    .line 369
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverInRangeImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_2

    move v2, v3

    .line 367
    :goto_2
    add-int v1, v4, v2

    .line 371
    mul-int/lit8 v4, v1, 0x1f

    .line 373
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverOutRangeImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_3

    move v2, v3

    .line 371
    :goto_3
    add-int v1, v4, v2

    .line 375
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 376
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 377
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 378
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 379
    mul-int/lit8 v4, v1, 0x1f

    .line 380
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_4

    move v2, v3

    .line 379
    :goto_4
    add-int v1, v4, v2

    .line 381
    mul-int/lit8 v4, v1, 0x1f

    .line 382
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    if-nez v2, :cond_5

    move v2, v3

    .line 381
    :goto_5
    add-int v1, v4, v2

    .line 383
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mMarkingShowOption:I

    add-int v1, v2, v4

    .line 384
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 385
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 386
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 387
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 388
    mul-int/lit8 v4, v1, 0x1f

    .line 389
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_6

    move v2, v3

    .line 388
    :goto_6
    add-int v1, v4, v2

    .line 390
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNumBitmaps:I

    add-int v1, v2, v4

    .line 391
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 392
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 393
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 394
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 395
    mul-int/lit8 v4, v1, 0x1f

    .line 396
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_7

    move v2, v3

    .line 395
    :goto_7
    add-int v1, v4, v2

    .line 397
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mRadius:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 398
    mul-int/lit8 v2, v1, 0x1f

    .line 399
    iget-object v4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mSpotShowList:Ljava/util/ArrayList;

    if-nez v4, :cond_8

    .line 398
    :goto_8
    add-int v1, v2, v3

    .line 400
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mValueMarkingID:I

    add-int v1, v2, v3

    .line 401
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mVisibleOption:I

    add-int v1, v2, v3

    .line 402
    return v1

    .line 361
    :cond_0
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 362
    :cond_1
    const/16 v2, 0x4d5

    goto/16 :goto_1

    .line 370
    :cond_2
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverInRangeImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 374
    :cond_3
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverOutRangeImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 380
    :cond_4
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 382
    :cond_5
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 389
    :cond_6
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 396
    :cond_7
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    .line 399
    :cond_8
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mSpotShowList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->hashCode()I

    move-result v3

    goto :goto_8
.end method

.method public isEnableHittest()Z
    .locals 1

    .prologue
    .line 345
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mEnableHittest:Z

    return v0
.end method

.method public pressedColor(FFFF)V
    .locals 0
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 235
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorR:F

    .line 236
    iput p2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorG:F

    .line 237
    iput p3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorB:F

    .line 238
    iput p4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorA:F

    .line 239
    return-void
.end method

.method public pressedColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 243
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorR:F

    .line 244
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorG:F

    .line 245
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorB:F

    .line 246
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedColorA:F

    .line 247
    return-void
.end method

.method public setBitmapList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 325
    .local p1, "bitmapList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mBitmapList:Ljava/util/ArrayList;

    .line 326
    return-void
.end method

.method public setEnableHittest(Z)V
    .locals 0
    .param p1, "enableHittest"    # Z

    .prologue
    .line 349
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mEnableHittest:Z

    .line 350
    return-void
.end method

.method public setHandlerOverInRangeImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "handlerOverInRangeImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 289
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverInRangeImage:Landroid/graphics/Bitmap;

    .line 290
    return-void
.end method

.method public setHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "handlerOverOutRangeImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 297
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mHandlerOverOutRangeImage:Landroid/graphics/Bitmap;

    .line 298
    return-void
.end method

.method public setInRangeImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "inRangeImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeImage:Landroid/graphics/Bitmap;

    .line 282
    return-void
.end method

.method public setListener(Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    .line 342
    return-void
.end method

.method public setMarkingShowOption(I)V
    .locals 0
    .param p1, "markingShowOption"    # I

    .prologue
    .line 317
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mMarkingShowOption:I

    .line 318
    return-void
.end method

.method public setNormalColor(FFFF)V
    .locals 0
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 130
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorR:F

    .line 131
    iput p2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorG:F

    .line 132
    iput p3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorB:F

    .line 133
    iput p4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorA:F

    .line 134
    return-void
.end method

.method public setNormalColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 138
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorR:F

    .line 139
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorG:F

    .line 140
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorB:F

    .line 141
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalColorA:F

    .line 142
    return-void
.end method

.method public setNormalImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "normalImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 273
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mNormalImage:Landroid/graphics/Bitmap;

    .line 274
    return-void
.end method

.method public setPressedImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "pressedImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 305
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mPressedImage:Landroid/graphics/Bitmap;

    .line 306
    return-void
.end method

.method public setRadius(F)V
    .locals 0
    .param p1, "radius"    # F

    .prologue
    .line 125
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mRadius:F

    .line 126
    return-void
.end method

.method public setSpotShowList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 333
    .local p1, "spotShowList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;>;"
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mSpotShowList:Ljava/util/ArrayList;

    .line 334
    return-void
.end method

.method public setValueMarkingID(I)V
    .locals 0
    .param p1, "mValueMarkingID"    # I

    .prologue
    .line 116
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mValueMarkingID:I

    .line 117
    return-void
.end method

.method public setVisibleOption(I)V
    .locals 0
    .param p1, "visibleOption"    # I

    .prologue
    .line 313
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mVisibleOption:I

    .line 314
    return-void
.end method

.method public setinRangeColor(FFFF)V
    .locals 0
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 165
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorR:F

    .line 166
    iput p2, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorG:F

    .line 167
    iput p3, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorB:F

    .line 168
    iput p4, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorA:F

    .line 169
    return-void
.end method

.method public setinRangeColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 173
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorR:F

    .line 174
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorG:F

    .line 175
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorB:F

    .line 176
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->mInRangeColorA:F

    .line 177
    return-void
.end method
