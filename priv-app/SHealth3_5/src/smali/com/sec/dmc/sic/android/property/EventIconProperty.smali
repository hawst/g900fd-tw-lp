.class public Lcom/sec/dmc/sic/android/property/EventIconProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "EventIconProperty.java"


# instance fields
.field protected mBitmapList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field protected mEventIconID:I

.field protected mListener:Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

.field protected mNumBitmaps:I

.field protected mOffsetX:F

.field protected mOffsetY:F

.field protected mScalable:Z

.field protected mVisible:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mBitmapList:Ljava/util/ArrayList;

    .line 21
    iput-boolean v1, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mVisible:Z

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mEventIconID:I

    .line 23
    iput v2, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetX:F

    .line 24
    iput v2, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetY:F

    .line 25
    iput v1, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mNumBitmaps:I

    .line 26
    iput-boolean v1, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mScalable:Z

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 105
    if-ne p0, p1, :cond_1

    .line 134
    :cond_0
    :goto_0
    return v1

    .line 107
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 108
    goto :goto_0

    .line 109
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 110
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 111
    check-cast v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    .line 112
    .local v0, "other":Lcom/sec/dmc/sic/android/property/EventIconProperty;
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mBitmapList:Ljava/util/ArrayList;

    if-nez v3, :cond_4

    .line 113
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mBitmapList:Ljava/util/ArrayList;

    if-eqz v3, :cond_5

    move v1, v2

    .line 114
    goto :goto_0

    .line 115
    :cond_4
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mBitmapList:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 116
    goto :goto_0

    .line 117
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mEventIconID:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mEventIconID:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 118
    goto :goto_0

    .line 119
    :cond_6
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

    if-nez v3, :cond_7

    .line 120
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

    if-eqz v3, :cond_8

    move v1, v2

    .line 121
    goto :goto_0

    .line 122
    :cond_7
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 123
    goto :goto_0

    .line 124
    :cond_8
    iget v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mNumBitmaps:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mNumBitmaps:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 125
    goto :goto_0

    .line 126
    :cond_9
    iget v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetX:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 127
    iget v4, v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetX:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 126
    if-eq v3, v4, :cond_a

    move v1, v2

    .line 128
    goto :goto_0

    .line 129
    :cond_a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetY:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 130
    iget v4, v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetY:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 129
    if-eq v3, v4, :cond_b

    move v1, v2

    .line 131
    goto :goto_0

    .line 132
    :cond_b
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mVisible:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 133
    goto :goto_0
.end method

.method public getEventIconBitmapList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mBitmapList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getListener()Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

    return-object v0
.end method

.method public getOffsetX()F
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetX:F

    return v0
.end method

.method public getOffsetY()F
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetY:F

    return v0
.end method

.method public getScalable()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mScalable:Z

    return v0
.end method

.method public getSeriesID()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mEventIconID:I

    return v0
.end method

.method public getVisible()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mVisible:Z

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 89
    const/16 v0, 0x1f

    .line 90
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 91
    .local v1, "result":I
    mul-int/lit8 v4, v1, 0x1f

    .line 92
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mBitmapList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    move v2, v3

    .line 91
    :goto_0
    add-int v1, v4, v2

    .line 93
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mEventIconID:I

    add-int v1, v2, v4

    .line 94
    mul-int/lit8 v2, v1, 0x1f

    .line 95
    iget-object v4, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

    if-nez v4, :cond_1

    .line 94
    :goto_1
    add-int v1, v2, v3

    .line 96
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mNumBitmaps:I

    add-int v1, v2, v3

    .line 97
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetX:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 98
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetY:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 99
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mVisible:Z

    if-eqz v2, :cond_2

    const/16 v2, 0x4cf

    :goto_2
    add-int v1, v3, v2

    .line 100
    return v1

    .line 92
    :cond_0
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->hashCode()I

    move-result v2

    goto :goto_0

    .line 95
    :cond_1
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto :goto_1

    .line 99
    :cond_2
    const/16 v2, 0x4d5

    goto :goto_2
.end method

.method public setEventIconBitmapList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "bitmaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mBitmapList:Ljava/util/ArrayList;

    .line 71
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mNumBitmaps:I

    .line 72
    return-void
.end method

.method public setListener(Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mListener:Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

    .line 63
    return-void
.end method

.method public setOffsetX(F)V
    .locals 0
    .param p1, "offset"    # F

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetX:F

    .line 51
    return-void
.end method

.method public setOffsetY(F)V
    .locals 0
    .param p1, "offset"    # F

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mOffsetY:F

    .line 59
    return-void
.end method

.method public setScalable(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mScalable:Z

    .line 84
    return-void
.end method

.method public setSeriesID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mEventIconID:I

    .line 43
    return-void
.end method

.method public setVisible(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/EventIconProperty;->mVisible:Z

    .line 35
    return-void
.end method
