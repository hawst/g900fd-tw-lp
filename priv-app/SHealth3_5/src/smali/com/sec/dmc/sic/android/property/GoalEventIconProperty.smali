.class public Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "GoalEventIconProperty.java"


# instance fields
.field protected mBitmap:Landroid/graphics/Bitmap;

.field protected mBitmapHeight:F

.field protected mBitmapWidth:F

.field protected mNeedScale:Z

.field protected mOffsetX:F

.field protected mOffsetY:F

.field protected mSeriesID:I

.field protected mValue:F

.field protected mVisible:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 16
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 17
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mVisible:Z

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mSeriesID:I

    .line 19
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetX:F

    .line 20
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetY:F

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmap:Landroid/graphics/Bitmap;

    .line 22
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapWidth:F

    .line 23
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapHeight:F

    .line 24
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mNeedScale:Z

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 85
    if-ne p0, p1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v1

    .line 87
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 88
    goto :goto_0

    .line 89
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 90
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 91
    check-cast v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    .line 92
    .local v0, "other":Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v3, :cond_4

    .line 93
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_5

    move v1, v2

    .line 94
    goto :goto_0

    .line 95
    :cond_4
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 96
    goto :goto_0

    .line 97
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapHeight:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 98
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapHeight:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 97
    if-eq v3, v4, :cond_6

    move v1, v2

    .line 99
    goto :goto_0

    .line 100
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 101
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 100
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 102
    goto :goto_0

    .line 103
    :cond_7
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mNeedScale:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mNeedScale:Z

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 104
    goto :goto_0

    .line 105
    :cond_8
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetX:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 106
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetX:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 105
    if-eq v3, v4, :cond_9

    move v1, v2

    .line 107
    goto :goto_0

    .line 108
    :cond_9
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetY:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 109
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetY:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 108
    if-eq v3, v4, :cond_a

    move v1, v2

    .line 110
    goto :goto_0

    .line 111
    :cond_a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mSeriesID:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mSeriesID:I

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 112
    goto :goto_0

    .line 113
    :cond_b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mValue:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mValue:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 114
    goto/16 :goto_0

    .line 115
    :cond_c
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mVisible:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 116
    goto/16 :goto_0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBitmapHeight()F
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapHeight:F

    return v0
.end method

.method public getBitmapWidth()F
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapWidth:F

    return v0
.end method

.method public getOffsetX()F
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetX:F

    return v0
.end method

.method public getOffsetY()F
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetY:F

    return v0
.end method

.method public getScalable()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mNeedScale:Z

    return v0
.end method

.method public getSeriesID()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mSeriesID:I

    return v0
.end method

.method public getVisible()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mVisible:Z

    return v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    .line 69
    const/16 v0, 0x1f

    .line 70
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 71
    .local v1, "result":I
    mul-int/lit8 v5, v1, 0x1f

    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v5, v2

    .line 72
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapHeight:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 73
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapWidth:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 74
    mul-int/lit8 v5, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mNeedScale:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v5, v2

    .line 75
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetX:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 76
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetY:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 77
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mSeriesID:I

    add-int v1, v2, v5

    .line 78
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mValue:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 79
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v5, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mVisible:Z

    if-eqz v5, :cond_2

    :goto_2
    add-int v1, v2, v3

    .line 80
    return v1

    .line 71
    :cond_0
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    move v2, v4

    .line 74
    goto :goto_1

    :cond_2
    move v3, v4

    .line 79
    goto :goto_2
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmap:Landroid/graphics/Bitmap;

    .line 65
    return-void
.end method

.method public setBitmapHeight(F)V
    .locals 0
    .param p1, "mBitmapHeight"    # F

    .prologue
    .line 133
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapHeight:F

    .line 134
    return-void
.end method

.method public setBitmapWidth(F)V
    .locals 0
    .param p1, "mBitmapWidth"    # F

    .prologue
    .line 125
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mBitmapWidth:F

    .line 126
    return-void
.end method

.method public setOffsetX(F)V
    .locals 0
    .param p1, "offset"    # F

    .prologue
    .line 48
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetX:F

    .line 49
    return-void
.end method

.method public setOffsetY(F)V
    .locals 0
    .param p1, "offset"    # F

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mOffsetY:F

    .line 57
    return-void
.end method

.method public setScalable(Z)V
    .locals 0
    .param p1, "scalable"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mNeedScale:Z

    .line 142
    return-void
.end method

.method public setSeriesID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mSeriesID:I

    .line 41
    return-void
.end method

.method public setVisible(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->mVisible:Z

    .line 33
    return-void
.end method
