.class public Lcom/sec/dmc/sic/android/view/NonTimeChart;
.super Ljava/lang/Object;
.source "NonTimeChart.java"

# interfaces
.implements Lcom/sec/dmc/sic/android/view/INonTimeChart;


# instance fields
.field private mChartHandle:J

.field private mStartIndex:I

.field private mValidCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mStartIndex:I

    .line 20
    iput v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mValidCount:I

    .line 21
    return-void
.end method


# virtual methods
.method public addData(IILjava/util/List;)V
    .locals 4
    .param p1, "seriesID"    # I
    .param p2, "seriesDataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p3, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 59
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p3}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v1, p1, v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->AddStringData(JII[Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 63
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p3}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v1, p1, v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->AddStringCandleData(JII[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public addDataFront(IILjava/util/List;)V
    .locals 4
    .param p1, "seriesID"    # I
    .param p2, "seriesDataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p3, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 75
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p3}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v1, p1, v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->AddStringDataFront(JII[Ljava/lang/Object;)V

    goto :goto_0

    .line 77
    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 79
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p3}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v1, p1, v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->AddStringCandleDataFront(JII[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public clearData()V
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->ClearData(J)V

    .line 104
    return-void
.end method

.method public getChartHandle()J
    .locals 2

    .prologue
    .line 25
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    return-wide v0
.end method

.method public prepareChangeData(IILjava/util/List;)V
    .locals 4
    .param p1, "seriesID"    # I
    .param p2, "seriesDataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p3, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 91
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p3}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v1, p1, v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->ChangeStringData(JII[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setChartHandle(J)V
    .locals 0
    .param p1, "ChartHandle"    # J

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    .line 29
    return-void
.end method

.method public setHandlerStartIndex(I)V
    .locals 2
    .param p1, "handlerStartIndex"    # I

    .prologue
    .line 109
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    invoke-static {v0, v1, p1}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetHandlerStartIndex(JI)V

    .line 110
    return-void
.end method

.method public setStartIndex(I)V
    .locals 4
    .param p1, "startIndex"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mStartIndex:I

    .line 42
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    iget v2, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mStartIndex:I

    iget v3, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mValidCount:I

    invoke-static {v0, v1, v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetStartVisual(JII)V

    .line 43
    return-void
.end method

.method public setStartValidCount(I)V
    .locals 4
    .param p1, "validCount"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mValidCount:I

    .line 48
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    iget v2, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mStartIndex:I

    iget v3, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mValidCount:I

    invoke-static {v0, v1, v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetStartVisual(JII)V

    .line 49
    return-void
.end method

.method public setStartVisual(II)V
    .locals 4
    .param p1, "startIndex"    # I
    .param p2, "validCount"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mStartIndex:I

    .line 35
    iput p2, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mValidCount:I

    .line 36
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mChartHandle:J

    iget v2, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mStartIndex:I

    iget v3, p0, Lcom/sec/dmc/sic/android/view/NonTimeChart;->mValidCount:I

    invoke-static {v0, v1, v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetStartVisual(JII)V

    .line 37
    return-void
.end method
