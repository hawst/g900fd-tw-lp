.class public Lcom/sec/dmc/sic/android/property/BaseProperty;
.super Ljava/lang/Object;
.source "BaseProperty.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field enable:Z

.field visible:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/BaseProperty;->visible:Z

    .line 60
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/BaseProperty;->enable:Z

    .line 61
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 99
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    if-ne p0, p1, :cond_1

    .line 124
    :cond_0
    :goto_0
    return v1

    .line 115
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 116
    goto :goto_0

    .line 117
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 118
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 119
    check-cast v0, Lcom/sec/dmc/sic/android/property/BaseProperty;

    .line 120
    .local v0, "other":Lcom/sec/dmc/sic/android/property/BaseProperty;
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/BaseProperty;->enable:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/BaseProperty;->enable:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 121
    goto :goto_0

    .line 122
    :cond_4
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/BaseProperty;->visible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/BaseProperty;->visible:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 123
    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    .line 104
    const/16 v0, 0x1f

    .line 105
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 106
    .local v1, "result":I
    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/BaseProperty;->enable:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 107
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v5, p0, Lcom/sec/dmc/sic/android/property/BaseProperty;->visible:Z

    if-eqz v5, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 108
    return v1

    :cond_0
    move v2, v4

    .line 106
    goto :goto_0

    :cond_1
    move v3, v4

    .line 107
    goto :goto_1
.end method

.method public isEnable()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/BaseProperty;->enable:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/BaseProperty;->visible:Z

    return v0
.end method

.method public setEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/BaseProperty;->enable:Z

    .line 85
    return-void
.end method

.method public setVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/BaseProperty;->visible:Z

    .line 76
    return-void
.end method
