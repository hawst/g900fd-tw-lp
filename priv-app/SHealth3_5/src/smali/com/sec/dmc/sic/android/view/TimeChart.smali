.class public Lcom/sec/dmc/sic/android/view/TimeChart;
.super Ljava/lang/Object;
.source "TimeChart.java"

# interfaces
.implements Lcom/sec/dmc/sic/android/view/ITimeChart;


# instance fields
.field private mChartHandle:J

.field private mCurrentMainMarking:I

.field private mCurrentScaleIntervalMainMarking:I

.field private mCurrentSicTimeDepthLevel:I

.field private mCurrentStartDate:D


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v2, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentSicTimeDepthLevel:I

    .line 26
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    long-to-double v0, v0

    iput-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentStartDate:D

    .line 27
    iput v2, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentScaleIntervalMainMarking:I

    .line 28
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentMainMarking:I

    .line 29
    return-void
.end method


# virtual methods
.method public addData(IILjava/util/List;)V
    .locals 1
    .param p1, "streamID"    # I
    .param p2, "seriesDataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p3, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/dmc/sic/android/view/TimeChart;->addDataWithAni(IILjava/util/List;I)V

    .line 83
    return-void
.end method

.method public addDataWithAni(IILjava/util/List;I)V
    .locals 10
    .param p1, "streamID"    # I
    .param p2, "seriesDataType"    # I
    .param p4, "duration"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p3, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-gtz v7, :cond_0

    .line 179
    :goto_0
    return-void

    .line 92
    :cond_0
    const/4 v1, 0x0

    .line 93
    .local v1, "dataSize":I
    const/4 v7, 0x1

    if-ne p2, v7, :cond_4

    .line 95
    const/16 v1, 0x18

    .line 102
    :cond_1
    :goto_1
    const/4 v4, 0x0

    .line 103
    .local v4, "multiDataCount":I
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 105
    const/4 v7, 0x0

    invoke-interface {p3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getMultiValues()Ljava/util/LinkedList;

    move-result-object v5

    .line 106
    .local v5, "tempMultiValues":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v4

    .line 109
    .end local v5    # "tempMultiValues":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    :cond_2
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    mul-int/2addr v7, v1

    add-int/lit8 v7, v7, 0x10

    mul-int/lit8 v8, v4, 0x8

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v9

    mul-int/2addr v8, v9

    add-int/2addr v7, v8

    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 110
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 112
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 113
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 114
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 116
    const/4 v7, 0x1

    if-ne p2, v7, :cond_8

    .line 118
    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 121
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-lt v2, v7, :cond_5

    .line 129
    if-lez v4, :cond_3

    .line 131
    const/4 v2, 0x0

    :goto_3
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-lt v2, v7, :cond_6

    .line 168
    .end local v2    # "i":I
    :cond_3
    if-nez p4, :cond_9

    .line 169
    :try_start_0
    iget-wide v7, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    invoke-static {v7, v8, v0}, Lcom/sec/dmc/sic/jni/ChartJNI;->AddData(JLjava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_4
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 97
    .end local v0    # "byteBuffer":Ljava/nio/ByteBuffer;
    .end local v4    # "multiDataCount":I
    :cond_4
    const/4 v7, 0x2

    if-ne p2, v7, :cond_1

    .line 99
    const/16 v1, 0x28

    goto :goto_1

    .line 123
    .restart local v0    # "byteBuffer":Ljava/nio/ByteBuffer;
    .restart local v2    # "i":I
    .restart local v4    # "multiDataCount":I
    :cond_5
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    .line 124
    .local v6, "tempSavedData":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getValue()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 125
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 126
    const-wide/16 v7, 0x0

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 121
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 133
    .end local v6    # "tempSavedData":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_6
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getMultiValues()Ljava/util/LinkedList;

    move-result-object v5

    .line 134
    .restart local v5    # "tempMultiValues":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_5
    if-lt v3, v4, :cond_7

    .line 131
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 136
    :cond_7
    invoke-virtual {v5, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 134
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 142
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v5    # "tempMultiValues":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    :cond_8
    const/4 v7, 0x2

    if-ne p2, v7, :cond_3

    .line 144
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 147
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_6
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 149
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;

    .line 157
    .local v6, "tempSavedData":Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->getValueOpen()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 158
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->getValueHigh()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 159
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->getValueLow()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 160
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->getValueClose()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 161
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->getTime()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 147
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 171
    .end local v2    # "i":I
    .end local v6    # "tempSavedData":Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;
    :cond_9
    :try_start_1
    iget-wide v7, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    invoke-static {v7, v8, v0, p4}, Lcom/sec/dmc/sic/jni/ChartJNI;->AddDataWithAni(JLjava/nio/ByteBuffer;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    .line 173
    :catch_0
    move-exception v7

    goto/16 :goto_4
.end method

.method public clearData()V
    .locals 2

    .prologue
    .line 276
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->ClearData(J)V

    .line 278
    return-void
.end method

.method public deleteData(IID)V
    .locals 6
    .param p1, "streamID"    # I
    .param p2, "seriesDataType"    # I
    .param p3, "epochTimeTarget"    # D

    .prologue
    .line 183
    int-to-long v0, p1

    move v2, p2

    move v3, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/dmc/sic/jni/ChartJNI;->DeleteData(JIID)V

    .line 184
    return-void
.end method

.method public getChartHandle()J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    return-wide v0
.end method

.method public getEpochLeftEndofData()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->GetLeftEndofTimeChart(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getEpochRightEndofData()J
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->GetRightEndofTimeChart(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public prepareChangeData(IILjava/util/List;)V
    .locals 10
    .param p1, "streamID"    # I
    .param p2, "seriesDataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 190
    .local p3, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-gtz v7, :cond_0

    .line 270
    :goto_0
    return-void

    .line 193
    :cond_0
    const/4 v1, 0x0

    .line 194
    .local v1, "dataSize":I
    const/4 v7, 0x1

    if-ne p2, v7, :cond_4

    .line 196
    const/16 v1, 0x18

    .line 203
    :cond_1
    :goto_1
    const/4 v4, 0x0

    .line 204
    .local v4, "multiDataCount":I
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 206
    const/4 v7, 0x0

    invoke-interface {p3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getMultiValues()Ljava/util/LinkedList;

    move-result-object v5

    .line 207
    .local v5, "tempMultiValues":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v4

    .line 210
    .end local v5    # "tempMultiValues":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    :cond_2
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    mul-int/2addr v7, v1

    add-int/lit8 v7, v7, 0x10

    mul-int/lit8 v8, v4, 0x8

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v9

    mul-int/2addr v8, v9

    add-int/2addr v7, v8

    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 211
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 213
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 214
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 215
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 217
    const/4 v7, 0x1

    if-ne p2, v7, :cond_8

    .line 219
    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 222
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-lt v2, v7, :cond_5

    .line 230
    if-lez v4, :cond_3

    .line 232
    const/4 v2, 0x0

    :goto_3
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-lt v2, v7, :cond_6

    .line 262
    .end local v2    # "i":I
    :cond_3
    :try_start_0
    iget-wide v7, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    invoke-static {v7, v8, v0}, Lcom/sec/dmc/sic/jni/ChartJNI;->ChangeData(JLjava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_4
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 198
    .end local v0    # "byteBuffer":Ljava/nio/ByteBuffer;
    .end local v4    # "multiDataCount":I
    :cond_4
    const/4 v7, 0x2

    if-ne p2, v7, :cond_1

    .line 200
    const/16 v1, 0x28

    goto :goto_1

    .line 224
    .restart local v0    # "byteBuffer":Ljava/nio/ByteBuffer;
    .restart local v2    # "i":I
    .restart local v4    # "multiDataCount":I
    :cond_5
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    .line 225
    .local v6, "tempSavedData":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getValue()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 226
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 227
    const-wide/16 v7, 0x0

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 222
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 234
    .end local v6    # "tempSavedData":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_6
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getMultiValues()Ljava/util/LinkedList;

    move-result-object v5

    .line 235
    .restart local v5    # "tempMultiValues":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_5
    if-lt v3, v4, :cond_7

    .line 232
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 237
    :cond_7
    invoke-virtual {v5, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 235
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 243
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v5    # "tempMultiValues":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    :cond_8
    const/4 v7, 0x2

    if-ne p2, v7, :cond_3

    .line 245
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 248
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_6
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 250
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;

    .line 252
    .local v6, "tempSavedData":Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->getValueOpen()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 253
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->getValueHigh()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 254
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->getValueLow()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 255
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->getValueClose()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 256
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->getTime()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 248
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 264
    .end local v2    # "i":I
    .end local v6    # "tempSavedData":Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;
    :catch_0
    move-exception v7

    goto/16 :goto_4
.end method

.method public setChartHandle(J)V
    .locals 0
    .param p1, "ChartHandle"    # J

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    .line 37
    return-void
.end method

.method public setHandlerStartDate(D)V
    .locals 2
    .param p1, "handlerStartDate"    # D

    .prologue
    .line 284
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    invoke-static {v0, v1, p1, p2}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetHandlerStartDate(JD)V

    .line 285
    return-void
.end method

.method public setScrollRangeDepthLevel(I)V
    .locals 2
    .param p1, "timeLevel"    # I

    .prologue
    .line 291
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    invoke-static {v0, v1, p1}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetScrollRangeDepthLevel(JI)V

    .line 292
    return-void
.end method

.method public setScrollRangeType(I)V
    .locals 2
    .param p1, "rangeType"    # I

    .prologue
    .line 298
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    invoke-static {v0, v1, p1}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetScrollRangeType(JI)V

    .line 299
    return-void
.end method

.method public setStartDate(D)V
    .locals 7
    .param p1, "startDate"    # D

    .prologue
    .line 59
    iput-wide p1, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentStartDate:D

    .line 60
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    iget v2, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentSicTimeDepthLevel:I

    iget-wide v3, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentStartDate:D

    iget v5, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentScaleIntervalMainMarking:I

    iget v6, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentMainMarking:I

    invoke-static/range {v0 .. v6}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetStartDateVisual(JIDII)V

    .line 61
    return-void
.end method

.method public setStartDepthLevel(I)V
    .locals 7
    .param p1, "level"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentSicTimeDepthLevel:I

    .line 54
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    iget v2, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentSicTimeDepthLevel:I

    iget-wide v3, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentStartDate:D

    iget v5, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentScaleIntervalMainMarking:I

    iget v6, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentMainMarking:I

    invoke-static/range {v0 .. v6}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetStartDateVisual(JIDII)V

    .line 55
    return-void
.end method

.method public setStartMainMarking(II)V
    .locals 7
    .param p1, "scaleIntervalMainMarking"    # I
    .param p2, "nMainMarkingCount"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentScaleIntervalMainMarking:I

    .line 66
    iput p2, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentMainMarking:I

    .line 67
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    iget v2, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentSicTimeDepthLevel:I

    iget-wide v3, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentStartDate:D

    iget v5, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentScaleIntervalMainMarking:I

    iget v6, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentMainMarking:I

    invoke-static/range {v0 .. v6}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetStartDateVisual(JIDII)V

    .line 68
    return-void
.end method

.method public setStartVisual(IDII)V
    .locals 7
    .param p1, "level"    # I
    .param p2, "startDate"    # D
    .param p4, "scaleIntervalMainMarking"    # I
    .param p5, "nMainMarkingCount"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentSicTimeDepthLevel:I

    .line 44
    iput-wide p2, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentStartDate:D

    .line 45
    iput p4, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentScaleIntervalMainMarking:I

    .line 46
    iput p5, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentMainMarking:I

    .line 48
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    iget v2, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentSicTimeDepthLevel:I

    iget-wide v3, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentStartDate:D

    iget v5, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentScaleIntervalMainMarking:I

    iget v6, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mCurrentMainMarking:I

    invoke-static/range {v0 .. v6}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetStartDateVisual(JIDII)V

    .line 49
    return-void
.end method

.method public setUserScrollRange(JJ)V
    .locals 6
    .param p1, "userLeftStart"    # J
    .param p3, "userRightEnd"    # J

    .prologue
    .line 305
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/view/TimeChart;->mChartHandle:J

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetUserScrollRange(JJJ)V

    .line 306
    return-void
.end method
