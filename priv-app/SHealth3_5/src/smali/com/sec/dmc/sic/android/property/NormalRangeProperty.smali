.class public Lcom/sec/dmc/sic/android/property/NormalRangeProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "NormalRangeProperty.java"


# static fields
.field public static final NORMALRANGE_TYPE_X:I = 0x0

.field public static final NORMALRANGE_TYPE_Y:I = 0x1


# instance fields
.field final M_VALUE_MIN:F

.field mMaxValue:F

.field mMinValue:F

.field mNormalRangeID:I

.field mNormalRangeType:I

.field mRangeColorA:F

.field mRangeColorB:F

.field mRangeColorG:F

.field mRangeColorR:F

.field mRangeFillColorA:F

.field mRangeFillColorB:F

.field mRangeFillColorG:F

.field mRangeFillColorR:F

.field mSeriesID:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const v1, -0x803662    # -3.4E38f

    .line 38
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 34
    iput v1, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->M_VALUE_MIN:F

    .line 40
    iput-boolean v5, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->enable:Z

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->visible:Z

    .line 42
    iput v4, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mSeriesID:I

    .line 43
    iput v4, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeID:I

    .line 44
    iput v1, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMinValue:F

    .line 45
    iput v1, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMaxValue:F

    .line 47
    iput v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorR:F

    .line 48
    iput v2, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorG:F

    .line 49
    iput v2, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorB:F

    .line 50
    iput v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorA:F

    .line 52
    const v0, 0x3f64e4e5

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorR:F

    .line 53
    const v0, 0x3f79f9fa

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorG:F

    .line 54
    const v0, 0x3f58d8d9

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorB:F

    .line 55
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorA:F

    .line 57
    iput v5, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeType:I

    .line 58
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const v5, -0x803662    # -3.4E38f

    const/4 v2, 0x0

    .line 196
    if-ne p0, p1, :cond_1

    .line 242
    :cond_0
    :goto_0
    return v1

    .line 198
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 199
    goto :goto_0

    .line 200
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 201
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 202
    check-cast v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    .line 203
    .local v0, "other":Lcom/sec/dmc/sic/android/property/NormalRangeProperty;
    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 204
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 203
    if-eq v3, v4, :cond_4

    move v1, v2

    .line 205
    goto :goto_0

    .line 206
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMaxValue:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 207
    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMaxValue:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 206
    if-eq v3, v4, :cond_5

    move v1, v2

    .line 208
    goto :goto_0

    .line 209
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMinValue:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 210
    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMinValue:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 209
    if-eq v3, v4, :cond_6

    move v1, v2

    .line 211
    goto :goto_0

    .line 212
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeID:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeID:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 213
    goto :goto_0

    .line 214
    :cond_7
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeType:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeType:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 215
    goto :goto_0

    .line 216
    :cond_8
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorA:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 217
    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 216
    if-eq v3, v4, :cond_9

    move v1, v2

    .line 218
    goto :goto_0

    .line 219
    :cond_9
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 220
    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 219
    if-eq v3, v4, :cond_a

    move v1, v2

    .line 221
    goto :goto_0

    .line 222
    :cond_a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 223
    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 222
    if-eq v3, v4, :cond_b

    move v1, v2

    .line 224
    goto/16 :goto_0

    .line 225
    :cond_b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 226
    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 225
    if-eq v3, v4, :cond_c

    move v1, v2

    .line 227
    goto/16 :goto_0

    .line 228
    :cond_c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorA:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 229
    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 228
    if-eq v3, v4, :cond_d

    move v1, v2

    .line 230
    goto/16 :goto_0

    .line 231
    :cond_d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 232
    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 231
    if-eq v3, v4, :cond_e

    move v1, v2

    .line 233
    goto/16 :goto_0

    .line 234
    :cond_e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 235
    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 234
    if-eq v3, v4, :cond_f

    move v1, v2

    .line 236
    goto/16 :goto_0

    .line 237
    :cond_f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 238
    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 237
    if-eq v3, v4, :cond_10

    move v1, v2

    .line 239
    goto/16 :goto_0

    .line 240
    :cond_10
    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mSeriesID:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mSeriesID:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 241
    goto/16 :goto_0
.end method

.method public getColorInRangeAlpha()F
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorA:F

    return v0
.end method

.method public getColorInRangeB()F
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorB:F

    return v0
.end method

.method public getColorInRangeG()F
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorG:F

    return v0
.end method

.method public getColorInRangeR()F
    .locals 1

    .prologue
    .line 252
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorR:F

    return v0
.end method

.method public getFillAlpha()F
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorA:F

    return v0
.end method

.method public getFillColorB()F
    .locals 1

    .prologue
    .line 282
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorB:F

    return v0
.end method

.method public getFillColorG()F
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorG:F

    return v0
.end method

.method public getFillColorR()F
    .locals 1

    .prologue
    .line 272
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorR:F

    return v0
.end method

.method public getMaxValue()F
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMaxValue:F

    return v0
.end method

.method public getMinValue()F
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMinValue:F

    return v0
.end method

.method public getNormalRangeID()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeID:I

    return v0
.end method

.method public getNormalRangeType()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeType:I

    return v0
.end method

.method public getSeriesID()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mSeriesID:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 175
    const/16 v0, 0x1f

    .line 176
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 177
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    const v3, -0x803662    # -3.4E38f

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 178
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMaxValue:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 179
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMinValue:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 180
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeID:I

    add-int v1, v2, v3

    .line 181
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeType:I

    add-int v1, v2, v3

    .line 182
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorA:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 183
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 184
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 185
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 186
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorA:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 187
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 188
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 189
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 190
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mSeriesID:I

    add-int v1, v2, v3

    .line 191
    return v1
.end method

.method public setColorInNormalRange(FFFF)V
    .locals 0
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 94
    iput p1, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorR:F

    .line 95
    iput p2, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorG:F

    .line 96
    iput p3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorB:F

    .line 97
    iput p4, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorA:F

    .line 98
    return-void
.end method

.method public setColorInNormalRange(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 138
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorR:F

    .line 139
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorG:F

    .line 140
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorB:F

    .line 141
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeColorA:F

    .line 143
    return-void
.end method

.method public setMaxValue(F)V
    .locals 0
    .param p1, "mMaxValue"    # F

    .prologue
    .line 81
    iput p1, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMaxValue:F

    .line 82
    return-void
.end method

.method public setMinValue(F)V
    .locals 0
    .param p1, "mMinValue"    # F

    .prologue
    .line 73
    iput p1, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mMinValue:F

    .line 74
    return-void
.end method

.method public setNormalRangeFillColor(FFFF)V
    .locals 0
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 147
    iput p1, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorR:F

    .line 148
    iput p2, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorG:F

    .line 149
    iput p3, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorB:F

    .line 150
    iput p4, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorA:F

    .line 151
    return-void
.end method

.method public setNormalRangeFillColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 155
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorR:F

    .line 156
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorG:F

    .line 157
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorB:F

    .line 158
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mRangeFillColorA:F

    .line 159
    return-void
.end method

.method public setNormalRangeID(I)V
    .locals 0
    .param p1, "mNormalRangeID"    # I

    .prologue
    .line 170
    iput p1, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeID:I

    .line 171
    return-void
.end method

.method public setNormalRangeType(I)V
    .locals 0
    .param p1, "mRangeType"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mNormalRangeType:I

    .line 90
    return-void
.end method

.method public setSeriesID(I)V
    .locals 0
    .param p1, "mSeriesID"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->mSeriesID:I

    .line 66
    return-void
.end method
