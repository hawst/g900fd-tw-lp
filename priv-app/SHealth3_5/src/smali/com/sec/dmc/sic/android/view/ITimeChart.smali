.class public interface abstract Lcom/sec/dmc/sic/android/view/ITimeChart;
.super Ljava/lang/Object;
.source "ITimeChart.java"


# virtual methods
.method public abstract addData(IILjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract addDataWithAni(IILjava/util/List;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;I)V"
        }
    .end annotation
.end method

.method public abstract clearData()V
.end method

.method public abstract deleteData(IID)V
.end method

.method public abstract prepareChangeData(IILjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setHandlerStartDate(D)V
.end method

.method public abstract setScrollRangeDepthLevel(I)V
.end method

.method public abstract setScrollRangeType(I)V
.end method

.method public abstract setStartVisual(IDII)V
.end method

.method public abstract setUserScrollRange(JJ)V
.end method
