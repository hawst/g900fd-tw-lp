.class public interface abstract Lcom/sec/dmc/sic/android/view/INonTimeChart;
.super Ljava/lang/Object;
.source "INonTimeChart.java"


# virtual methods
.method public abstract addData(IILjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract addDataFront(IILjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract clearData()V
.end method

.method public abstract prepareChangeData(IILjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setHandlerStartIndex(I)V
.end method

.method public abstract setStartVisual(II)V
.end method
