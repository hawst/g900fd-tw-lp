.class public Lcom/sec/dmc/sic/android/property/GraphProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "GraphProperty.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    }
.end annotation


# instance fields
.field areaFillAlpha:F

.field areaFillColorB:F

.field areaFillColorG:F

.field areaFillColorR:F

.field private areaGradientColors:[I

.field private areaGradientValues:[F

.field disconnectedBySeperator:Z

.field fillAlpha:F

.field fillColorB:F

.field fillColorG:F

.field fillColorR:F

.field private fillGradientColors:[I

.field private fillGradientValues:[F

.field graphID:I

.field mBarAlign:I

.field mBarWidth:F

.field mBarWidthInTime:J

.field mCandleBodyDecreaseColor:I

.field mCandleBodyIncreaseColor:I

.field mCandleBodyWidth:F

.field mCandleLineCloseColor:I

.field mCandleLineHighColor:I

.field mCandleLineLowColor:I

.field mCandleLineOpenColor:I

.field mCandleLineThickness:F

.field mCandleShadowColor:I

.field mCandleShadowThickness:F

.field mCandleShadowWidth:F

.field mStackedBarColors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mStackedBarItemNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field pieHoleRadiusRatio:F

.field pieItemInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field pieRadiusRatio:F

.field pieStartAngleRadian:F

.field spotColorA:F

.field spotColorB:F

.field spotColorG:F

.field spotColorR:F

.field spotEnable:Z

.field spotPosX:F

.field spotWidth:F

.field strokeWidth:F

.field textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/high16 v1, -0x1000000

    const/4 v2, 0x0

    .line 230
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 232
    iput v4, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->graphID:I

    .line 234
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorR:F

    .line 235
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorG:F

    .line 236
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorB:F

    .line 237
    iput v5, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillAlpha:F

    .line 239
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->strokeWidth:F

    .line 240
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->disconnectedBySeperator:Z

    .line 242
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotWidth:F

    .line 243
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotEnable:Z

    .line 244
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotWidth:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotPosX:F

    .line 245
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorR:F

    .line 246
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorG:F

    .line 247
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorB:F

    .line 248
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorA:F

    .line 251
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 253
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieRadiusRatio:F

    .line 255
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    .line 257
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarColors:Ljava/util/ArrayList;

    .line 259
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieHoleRadiusRatio:F

    .line 262
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowWidth:F

    .line 263
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowThickness:F

    .line 264
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowColor:I

    .line 266
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyWidth:F

    .line 267
    const/high16 v0, -0x10000

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyIncreaseColor:I

    .line 268
    const v0, -0xffff01

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyDecreaseColor:I

    .line 269
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineThickness:F

    .line 270
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineHighColor:I

    .line 271
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineLowColor:I

    .line 272
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineOpenColor:I

    .line 273
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineCloseColor:I

    .line 275
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarAlign:I

    .line 276
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidth:F

    .line 277
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidthInTime:J

    .line 279
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorR:F

    .line 280
    iput v5, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorG:F

    .line 281
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorB:F

    .line 282
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillAlpha:F

    .line 284
    iput-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientColors:[I

    .line 285
    iput-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientValues:[F

    .line 287
    iput-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientColors:[I

    .line 288
    iput-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientValues:[F

    .line 289
    return-void
.end method

.method private readyPieItemInfo(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 423
    :goto_0
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 427
    return-void

    .line 424
    :cond_0
    new-instance v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    invoke-direct {v0, p0}, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;-><init>(Lcom/sec/dmc/sic/android/property/GraphProperty;)V

    .line 425
    .local v0, "a":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public addStackedBarItemColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 695
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarColors:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 696
    return-void
.end method

.method public addStackedBarItemName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 700
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarItemNames:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 701
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 860
    if-ne p0, p1, :cond_1

    .line 988
    :cond_0
    :goto_0
    return v1

    .line 862
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 863
    goto :goto_0

    .line 864
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 865
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 866
    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty;

    .line 867
    .local v0, "other":Lcom/sec/dmc/sic/android/property/GraphProperty;
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 868
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 867
    if-eq v3, v4, :cond_4

    move v1, v2

    .line 869
    goto :goto_0

    .line 870
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 871
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 870
    if-eq v3, v4, :cond_5

    move v1, v2

    .line 872
    goto :goto_0

    .line 873
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 874
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 873
    if-eq v3, v4, :cond_6

    move v1, v2

    .line 875
    goto :goto_0

    .line 876
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 877
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 876
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 878
    goto :goto_0

    .line 879
    :cond_7
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientColors:[I

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientColors:[I

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 880
    goto :goto_0

    .line 881
    :cond_8
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientValues:[F

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientValues:[F

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 882
    goto :goto_0

    .line 883
    :cond_9
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->disconnectedBySeperator:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->disconnectedBySeperator:Z

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 884
    goto :goto_0

    .line 885
    :cond_a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 886
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 885
    if-eq v3, v4, :cond_b

    move v1, v2

    .line 887
    goto/16 :goto_0

    .line 888
    :cond_b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 889
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 888
    if-eq v3, v4, :cond_c

    move v1, v2

    .line 890
    goto/16 :goto_0

    .line 891
    :cond_c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 892
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 891
    if-eq v3, v4, :cond_d

    move v1, v2

    .line 893
    goto/16 :goto_0

    .line 894
    :cond_d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 895
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 894
    if-eq v3, v4, :cond_e

    move v1, v2

    .line 896
    goto/16 :goto_0

    .line 897
    :cond_e
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientColors:[I

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientColors:[I

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    .line 898
    goto/16 :goto_0

    .line 899
    :cond_f
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientValues:[F

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientValues:[F

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 900
    goto/16 :goto_0

    .line 901
    :cond_10
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->graphID:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->graphID:I

    if-eq v3, v4, :cond_11

    move v1, v2

    .line 902
    goto/16 :goto_0

    .line 903
    :cond_11
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarAlign:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarAlign:I

    if-eq v3, v4, :cond_12

    move v1, v2

    .line 904
    goto/16 :goto_0

    .line 905
    :cond_12
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 906
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 905
    if-eq v3, v4, :cond_13

    move v1, v2

    .line 907
    goto/16 :goto_0

    .line 908
    :cond_13
    iget-wide v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidthInTime:J

    iget-wide v5, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidthInTime:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_14

    move v1, v2

    .line 909
    goto/16 :goto_0

    .line 910
    :cond_14
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyDecreaseColor:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyDecreaseColor:I

    if-eq v3, v4, :cond_15

    move v1, v2

    .line 911
    goto/16 :goto_0

    .line 912
    :cond_15
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyIncreaseColor:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyIncreaseColor:I

    if-eq v3, v4, :cond_16

    move v1, v2

    .line 913
    goto/16 :goto_0

    .line 914
    :cond_16
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 915
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 914
    if-eq v3, v4, :cond_17

    move v1, v2

    .line 916
    goto/16 :goto_0

    .line 917
    :cond_17
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineCloseColor:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineCloseColor:I

    if-eq v3, v4, :cond_18

    move v1, v2

    .line 918
    goto/16 :goto_0

    .line 919
    :cond_18
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineHighColor:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineHighColor:I

    if-eq v3, v4, :cond_19

    move v1, v2

    .line 920
    goto/16 :goto_0

    .line 921
    :cond_19
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineLowColor:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineLowColor:I

    if-eq v3, v4, :cond_1a

    move v1, v2

    .line 922
    goto/16 :goto_0

    .line 923
    :cond_1a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineOpenColor:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineOpenColor:I

    if-eq v3, v4, :cond_1b

    move v1, v2

    .line 924
    goto/16 :goto_0

    .line 925
    :cond_1b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineThickness:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 926
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineThickness:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 925
    if-eq v3, v4, :cond_1c

    move v1, v2

    .line 927
    goto/16 :goto_0

    .line 928
    :cond_1c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowColor:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowColor:I

    if-eq v3, v4, :cond_1d

    move v1, v2

    .line 929
    goto/16 :goto_0

    .line 930
    :cond_1d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowThickness:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 931
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowThickness:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 930
    if-eq v3, v4, :cond_1e

    move v1, v2

    .line 932
    goto/16 :goto_0

    .line 933
    :cond_1e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 934
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 933
    if-eq v3, v4, :cond_1f

    move v1, v2

    .line 935
    goto/16 :goto_0

    .line 936
    :cond_1f
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarColors:Ljava/util/ArrayList;

    if-nez v3, :cond_20

    .line 937
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarColors:Ljava/util/ArrayList;

    if-eqz v3, :cond_21

    move v1, v2

    .line 938
    goto/16 :goto_0

    .line 939
    :cond_20
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarColors:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarColors:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_21

    move v1, v2

    .line 940
    goto/16 :goto_0

    .line 941
    :cond_21
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarItemNames:Ljava/util/ArrayList;

    if-nez v3, :cond_22

    .line 942
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarItemNames:Ljava/util/ArrayList;

    if-eqz v3, :cond_23

    move v1, v2

    .line 943
    goto/16 :goto_0

    .line 944
    :cond_22
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarItemNames:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarItemNames:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_23

    move v1, v2

    .line 945
    goto/16 :goto_0

    .line 946
    :cond_23
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieHoleRadiusRatio:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 947
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieHoleRadiusRatio:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 946
    if-eq v3, v4, :cond_24

    move v1, v2

    .line 948
    goto/16 :goto_0

    .line 949
    :cond_24
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    if-nez v3, :cond_25

    .line 950
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    if-eqz v3, :cond_26

    move v1, v2

    .line 951
    goto/16 :goto_0

    .line 952
    :cond_25
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_26

    move v1, v2

    .line 953
    goto/16 :goto_0

    .line 954
    :cond_26
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieRadiusRatio:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 955
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieRadiusRatio:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 954
    if-eq v3, v4, :cond_27

    move v1, v2

    .line 956
    goto/16 :goto_0

    .line 957
    :cond_27
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieStartAngleRadian:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 958
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieStartAngleRadian:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 957
    if-eq v3, v4, :cond_28

    move v1, v2

    .line 959
    goto/16 :goto_0

    .line 960
    :cond_28
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorA:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 961
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 960
    if-eq v3, v4, :cond_29

    move v1, v2

    .line 962
    goto/16 :goto_0

    .line 963
    :cond_29
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 964
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 963
    if-eq v3, v4, :cond_2a

    move v1, v2

    .line 965
    goto/16 :goto_0

    .line 966
    :cond_2a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 967
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 966
    if-eq v3, v4, :cond_2b

    move v1, v2

    .line 968
    goto/16 :goto_0

    .line 969
    :cond_2b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 970
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 969
    if-eq v3, v4, :cond_2c

    move v1, v2

    .line 971
    goto/16 :goto_0

    .line 972
    :cond_2c
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotEnable:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotEnable:Z

    if-eq v3, v4, :cond_2d

    move v1, v2

    .line 973
    goto/16 :goto_0

    .line 974
    :cond_2d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotPosX:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 975
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotPosX:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 974
    if-eq v3, v4, :cond_2e

    move v1, v2

    .line 976
    goto/16 :goto_0

    .line 977
    :cond_2e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 978
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 977
    if-eq v3, v4, :cond_2f

    move v1, v2

    .line 979
    goto/16 :goto_0

    .line 980
    :cond_2f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->strokeWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 981
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->strokeWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 980
    if-eq v3, v4, :cond_30

    move v1, v2

    .line 982
    goto/16 :goto_0

    .line 983
    :cond_30
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_31

    .line 984
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_0

    move v1, v2

    .line 985
    goto/16 :goto_0

    .line 986
    :cond_31
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 987
    goto/16 :goto_0
.end method

.method public getAreaFillAlpha()F
    .locals 1

    .prologue
    .line 760
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillAlpha:F

    return v0
.end method

.method public getAreaFillColorB()F
    .locals 1

    .prologue
    .line 752
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorB:F

    return v0
.end method

.method public getAreaFillColorG()F
    .locals 1

    .prologue
    .line 744
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorG:F

    return v0
.end method

.method public getAreaFillColorR()F
    .locals 1

    .prologue
    .line 736
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorR:F

    return v0
.end method

.method public getAreaGradientValues()[F
    .locals 1

    .prologue
    .line 792
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientValues:[F

    return-object v0
.end method

.method public getAreaGradientdColors()[I
    .locals 1

    .prologue
    .line 784
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientColors:[I

    return-object v0
.end method

.method public getBarAlign()I
    .locals 1

    .prologue
    .line 704
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarAlign:I

    return v0
.end method

.method public getBarWidth()F
    .locals 1

    .prologue
    .line 712
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidth:F

    return v0
.end method

.method public getBarWidthInTime()J
    .locals 2

    .prologue
    .line 720
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidthInTime:J

    return-wide v0
.end method

.method public getDisconnectionBySeperator()Z
    .locals 1

    .prologue
    .line 354
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->disconnectedBySeperator:Z

    return v0
.end method

.method public getFillAlpha()F
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillAlpha:F

    return v0
.end method

.method public getFillColorB()F
    .locals 1

    .prologue
    .line 322
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorB:F

    return v0
.end method

.method public getFillColorG()F
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorG:F

    return v0
.end method

.method public getFillColorR()F
    .locals 1

    .prologue
    .line 306
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorR:F

    return v0
.end method

.method public getFillGradientColors()[I
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientColors:[I

    return-object v0
.end method

.method public getFillGradientValues()[F
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientValues:[F

    return-object v0
.end method

.method public getGraphID()I
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->graphID:I

    return v0
.end method

.method public getPieHoleRadiusRatio()F
    .locals 1

    .prologue
    .line 571
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieHoleRadiusRatio:F

    return v0
.end method

.method public getPieItemColor(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 536
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 538
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 539
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iget v1, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->color:I

    return v1
.end method

.method public getPieItemSeparatorColor(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 550
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 552
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 553
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iget v1, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorColor:I

    return v1
.end method

.method public getPieItemSeparatorWidth(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 564
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 566
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 567
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iget v1, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorWidth:I

    return v1
.end method

.method public getPieLabelVisible(I)Z
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 479
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 481
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 482
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iget-boolean v1, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelVisible:Z

    return v1
.end method

.method public getPiePercentLabel(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 430
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 432
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 433
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iget-object v1, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->percentLabel:Ljava/lang/String;

    return-object v1
.end method

.method public getPieRadiusRatio()F
    .locals 1

    .prologue
    .line 415
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieRadiusRatio:F

    return v0
.end method

.method public getPieStartAngleRadian()F
    .locals 1

    .prologue
    .line 525
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieStartAngleRadian:F

    return v0
.end method

.method public getPieTextCenterRadiusRatio(I)F
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 458
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 460
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 461
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iget v1, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textCenterRadiusRatio:F

    return v1
.end method

.method public getPieTextGuidelineVisible(I)Z
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 444
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 446
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 447
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iget-boolean v1, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textGuidelineVisible:Z

    return v1
.end method

.method public getPieValueVisible(I)Z
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 507
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 509
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 510
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iget-boolean v1, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueVisible:Z

    return v1
.end method

.method public getSpotEnable()Z
    .locals 1

    .prologue
    .line 370
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotEnable:Z

    return v0
.end method

.method public getSpotPosX()F
    .locals 1

    .prologue
    .line 378
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotPosX:F

    return v0
.end method

.method public getSpotWidth()F
    .locals 1

    .prologue
    .line 362
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotWidth:F

    return v0
.end method

.method public getStackedBarItemName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 686
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarItemNames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 687
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarItemNames:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 690
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStrokeWidth()F
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->strokeWidth:F

    return v0
.end method

.method public getTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getmCandleBodyDecreaseColor()I
    .locals 1

    .prologue
    .line 627
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyDecreaseColor:I

    return v0
.end method

.method public getmCandleBodyIncreaseColor()I
    .locals 1

    .prologue
    .line 619
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyIncreaseColor:I

    return v0
.end method

.method public getmCandleBodyWidth()F
    .locals 1

    .prologue
    .line 611
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyWidth:F

    return v0
.end method

.method public getmCandleLineCloseColor()I
    .locals 1

    .prologue
    .line 667
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineCloseColor:I

    return v0
.end method

.method public getmCandleLineHighColor()I
    .locals 1

    .prologue
    .line 643
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineHighColor:I

    return v0
.end method

.method public getmCandleLineLowColor()I
    .locals 1

    .prologue
    .line 651
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineLowColor:I

    return v0
.end method

.method public getmCandleLineOpenColor()I
    .locals 1

    .prologue
    .line 659
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineOpenColor:I

    return v0
.end method

.method public getmCandleLineThickness()F
    .locals 1

    .prologue
    .line 635
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineThickness:F

    return v0
.end method

.method public getmCandleShadowColor()I
    .locals 1

    .prologue
    .line 595
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowColor:I

    return v0
.end method

.method public getmCandleShadowThickness()F
    .locals 1

    .prologue
    .line 587
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowThickness:F

    return v0
.end method

.method public getmCandleShadowWidth()F
    .locals 1

    .prologue
    .line 579
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowWidth:F

    return v0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v5, 0x0

    .line 801
    const/16 v0, 0x1f

    .line 802
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 803
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 804
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 805
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 806
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 807
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientColors:[I

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([I)I

    move-result v6

    add-int v1, v2, v6

    .line 808
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientValues:[F

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([F)I

    move-result v6

    add-int v1, v2, v6

    .line 809
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->disconnectedBySeperator:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 810
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 811
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 812
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 813
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 814
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientColors:[I

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([I)I

    move-result v6

    add-int v1, v2, v6

    .line 815
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientValues:[F

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([F)I

    move-result v6

    add-int v1, v2, v6

    .line 816
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->graphID:I

    add-int v1, v2, v6

    .line 817
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarAlign:I

    add-int v1, v2, v6

    .line 818
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidth:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 819
    mul-int/lit8 v2, v1, 0x1f

    .line 820
    iget-wide v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidthInTime:J

    iget-wide v8, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidthInTime:J

    const/16 v10, 0x20

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    .line 819
    add-int v1, v2, v6

    .line 821
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyDecreaseColor:I

    add-int v1, v2, v6

    .line 822
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyIncreaseColor:I

    add-int v1, v2, v6

    .line 823
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyWidth:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 824
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineCloseColor:I

    add-int v1, v2, v6

    .line 825
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineHighColor:I

    add-int v1, v2, v6

    .line 826
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineLowColor:I

    add-int v1, v2, v6

    .line 827
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineOpenColor:I

    add-int v1, v2, v6

    .line 828
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineThickness:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 829
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowColor:I

    add-int v1, v2, v6

    .line 830
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowThickness:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 831
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowWidth:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 832
    mul-int/lit8 v6, v1, 0x1f

    .line 834
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarColors:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    move v2, v5

    .line 832
    :goto_1
    add-int v1, v6, v2

    .line 836
    mul-int/lit8 v6, v1, 0x1f

    .line 838
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarItemNames:Ljava/util/ArrayList;

    if-nez v2, :cond_2

    move v2, v5

    .line 836
    :goto_2
    add-int v1, v6, v2

    .line 840
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieHoleRadiusRatio:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 841
    mul-int/lit8 v6, v1, 0x1f

    .line 842
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    if-nez v2, :cond_3

    move v2, v5

    .line 841
    :goto_3
    add-int v1, v6, v2

    .line 843
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieRadiusRatio:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 844
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieStartAngleRadian:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 845
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorA:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 846
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 847
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 848
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 849
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotEnable:Z

    if-eqz v6, :cond_4

    :goto_4
    add-int v1, v2, v3

    .line 850
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotPosX:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 851
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 852
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->strokeWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 853
    mul-int/lit8 v2, v1, 0x1f

    .line 854
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_5

    .line 853
    :goto_5
    add-int v1, v2, v5

    .line 855
    return v1

    :cond_0
    move v2, v4

    .line 809
    goto/16 :goto_0

    .line 834
    :cond_1
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarColors:Ljava/util/ArrayList;

    .line 835
    invoke-virtual {v2}, Ljava/util/ArrayList;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 838
    :cond_2
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mStackedBarItemNames:Ljava/util/ArrayList;

    .line 839
    invoke-virtual {v2}, Ljava/util/ArrayList;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 842
    :cond_3
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->hashCode()I

    move-result v2

    goto/16 :goto_3

    :cond_4
    move v3, v4

    .line 849
    goto :goto_4

    .line 854
    :cond_5
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v5

    goto :goto_5
.end method

.method public setAlign(I)V
    .locals 0
    .param p1, "align"    # I

    .prologue
    .line 708
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarAlign:I

    .line 709
    return-void
.end method

.method public setAreaFillAlpha(F)V
    .locals 0
    .param p1, "a"    # F

    .prologue
    .line 764
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillAlpha:F

    .line 765
    return-void
.end method

.method public setAreaFillColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 729
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorR:F

    .line 730
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorG:F

    .line 731
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorB:F

    .line 732
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillAlpha:F

    .line 733
    return-void
.end method

.method public setAreaFillColorB(F)V
    .locals 0
    .param p1, "fillColorB"    # F

    .prologue
    .line 756
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorB:F

    .line 757
    return-void
.end method

.method public setAreaFillColorG(F)V
    .locals 0
    .param p1, "fillColorG"    # F

    .prologue
    .line 748
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorG:F

    .line 749
    return-void
.end method

.method public setAreaFillColorR(F)V
    .locals 0
    .param p1, "fillColorR"    # F

    .prologue
    .line 740
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaFillColorR:F

    .line 741
    return-void
.end method

.method public setAreaGradientValues([F)V
    .locals 0
    .param p1, "areaGradientValues"    # [F

    .prologue
    .line 796
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientValues:[F

    .line 797
    return-void
.end method

.method public setAreaGradientdColors([I)V
    .locals 0
    .param p1, "areaGradientColors"    # [I

    .prologue
    .line 788
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->areaGradientColors:[I

    .line 789
    return-void
.end method

.method public setBarWidth(F)V
    .locals 0
    .param p1, "barWidth"    # F

    .prologue
    .line 716
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidth:F

    .line 717
    return-void
.end method

.method public setBarWidthInTime(J)V
    .locals 0
    .param p1, "barWidthInTime"    # J

    .prologue
    .line 724
    iput-wide p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mBarWidthInTime:J

    .line 725
    return-void
.end method

.method public setDisconnectionBySeperator(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 358
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->disconnectedBySeperator:Z

    .line 359
    return-void
.end method

.method public setFillAlpha(F)V
    .locals 0
    .param p1, "fillAlpha"    # F

    .prologue
    .line 342
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillAlpha:F

    .line 343
    return-void
.end method

.method public setFillColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 330
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorR:F

    .line 331
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorG:F

    .line 332
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorB:F

    .line 333
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillAlpha:F

    .line 335
    return-void
.end method

.method public setFillColorB(F)V
    .locals 0
    .param p1, "fillColorB"    # F

    .prologue
    .line 326
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorB:F

    .line 327
    return-void
.end method

.method public setFillColorG(F)V
    .locals 0
    .param p1, "fillColorG"    # F

    .prologue
    .line 318
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorG:F

    .line 319
    return-void
.end method

.method public setFillColorR(F)V
    .locals 0
    .param p1, "fillColorR"    # F

    .prologue
    .line 310
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillColorR:F

    .line 311
    return-void
.end method

.method public setFillGradientColors([I)V
    .locals 0
    .param p1, "fillGradientColors"    # [I

    .prologue
    .line 772
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientColors:[I

    .line 773
    return-void
.end method

.method public setFillGradientValues([F)V
    .locals 0
    .param p1, "fillGradientValues"    # [F

    .prologue
    .line 780
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->fillGradientValues:[F

    .line 781
    return-void
.end method

.method public setGraphID(I)V
    .locals 0
    .param p1, "graphID"    # I

    .prologue
    .line 302
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->graphID:I

    .line 303
    return-void
.end method

.method public setPieHoleRadiusRatio(F)V
    .locals 0
    .param p1, "a"    # F

    .prologue
    .line 575
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieHoleRadiusRatio:F

    .line 576
    return-void
.end method

.method public setPieItemColor(II)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "color"    # I

    .prologue
    .line 529
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 531
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 532
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iput p2, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->color:I

    .line 533
    return-void
.end method

.method public setPieItemSeparatorColor(II)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "color"    # I

    .prologue
    .line 543
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 545
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 546
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iput p2, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorColor:I

    .line 547
    return-void
.end method

.method public setPieItemSeparatorWidth(II)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "width"    # I

    .prologue
    .line 557
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 559
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 560
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iput p2, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->separatorWidth:I

    .line 561
    return-void
.end method

.method public setPieLabelTextStyle(ILcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 472
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 474
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 475
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iput-object p2, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 476
    return-void
.end method

.method public setPieLabelVisible(IZ)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "a"    # Z

    .prologue
    .line 486
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 488
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 489
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iput-boolean p2, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->labelVisible:Z

    .line 490
    return-void
.end method

.method public setPiePercentLabel(ILjava/lang/String;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "a"    # Ljava/lang/String;

    .prologue
    .line 437
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 439
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 440
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iput-object p2, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->percentLabel:Ljava/lang/String;

    .line 441
    return-void
.end method

.method public setPieRadiusRatio(F)V
    .locals 0
    .param p1, "a"    # F

    .prologue
    .line 419
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieRadiusRatio:F

    .line 420
    return-void
.end method

.method public setPieStartAngleRadian(F)V
    .locals 0
    .param p1, "a"    # F

    .prologue
    .line 521
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieStartAngleRadian:F

    .line 522
    return-void
.end method

.method public setPieTextCenterRadiusRatio(IF)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "a"    # F

    .prologue
    .line 465
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 467
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 468
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iput p2, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textCenterRadiusRatio:F

    .line 469
    return-void
.end method

.method public setPieTextGuidelineVisible(IZ)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "a"    # Z

    .prologue
    .line 451
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 453
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 454
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iput-boolean p2, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->textGuidelineVisible:Z

    .line 455
    return-void
.end method

.method public setPieValuePercentTextStyle(ILcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 500
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 502
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 503
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iput-object p2, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valuePercentStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 504
    return-void
.end method

.method public setPieValueTextStyle(ILcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 493
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 495
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 496
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iput-object p2, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 497
    return-void
.end method

.method public setPieValueVisible(IZ)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "a"    # Z

    .prologue
    .line 514
    invoke-direct {p0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->readyPieItemInfo(I)V

    .line 516
    iget-object v1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->pieItemInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;

    .line 517
    .local v0, "b":Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;
    iput-boolean p2, v0, Lcom/sec/dmc/sic/android/property/GraphProperty$PieItemInfo;->valueVisible:Z

    .line 518
    return-void
.end method

.method public setSpotColor(FFFF)V
    .locals 0
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 386
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorR:F

    .line 387
    iput p2, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorG:F

    .line 388
    iput p3, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorB:F

    .line 389
    iput p4, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorA:F

    .line 390
    return-void
.end method

.method public setSpotColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 393
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorR:F

    .line 394
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorG:F

    .line 395
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorB:F

    .line 396
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotColorA:F

    .line 397
    return-void
.end method

.method public setSpotEnable(Z)V
    .locals 0
    .param p1, "spotEnable"    # Z

    .prologue
    .line 374
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotEnable:Z

    .line 375
    return-void
.end method

.method public setSpotPosX(F)V
    .locals 0
    .param p1, "spotPosX"    # F

    .prologue
    .line 382
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotPosX:F

    .line 383
    return-void
.end method

.method public setSpotWidth(F)V
    .locals 0
    .param p1, "spotWidth"    # F

    .prologue
    .line 366
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->spotWidth:F

    .line 367
    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 0
    .param p1, "strokeWidth"    # F

    .prologue
    .line 350
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->strokeWidth:F

    .line 351
    return-void
.end method

.method public setTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 411
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 412
    return-void
.end method

.method public setmCandleBodyDecreaseColor(I)V
    .locals 0
    .param p1, "mCandleBodyDecreaseColor"    # I

    .prologue
    .line 631
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyDecreaseColor:I

    .line 632
    return-void
.end method

.method public setmCandleBodyIncreaseColor(I)V
    .locals 0
    .param p1, "mCandleBodyIncreaseColor"    # I

    .prologue
    .line 623
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyIncreaseColor:I

    .line 624
    return-void
.end method

.method public setmCandleBodyWidth(F)V
    .locals 0
    .param p1, "mCandleBodyWidth"    # F

    .prologue
    .line 615
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleBodyWidth:F

    .line 616
    return-void
.end method

.method public setmCandleLineCloseColor(I)V
    .locals 0
    .param p1, "mCandleLineCloseColor"    # I

    .prologue
    .line 671
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineCloseColor:I

    .line 672
    return-void
.end method

.method public setmCandleLineHighColor(I)V
    .locals 0
    .param p1, "mCandleLineHighColor"    # I

    .prologue
    .line 647
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineHighColor:I

    .line 648
    return-void
.end method

.method public setmCandleLineLowColor(I)V
    .locals 0
    .param p1, "mCandleLineLowColor"    # I

    .prologue
    .line 655
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineLowColor:I

    .line 656
    return-void
.end method

.method public setmCandleLineOpenColor(I)V
    .locals 0
    .param p1, "mCandleLineOpenColor"    # I

    .prologue
    .line 663
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineOpenColor:I

    .line 664
    return-void
.end method

.method public setmCandleLineThickness(F)V
    .locals 0
    .param p1, "mCandleLineThickness"    # F

    .prologue
    .line 639
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleLineThickness:F

    .line 640
    return-void
.end method

.method public setmCandleShadowColor(I)V
    .locals 0
    .param p1, "mCandleShadowColor"    # I

    .prologue
    .line 599
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowColor:I

    .line 600
    return-void
.end method

.method public setmCandleShadowThickness(F)V
    .locals 0
    .param p1, "mCandleShadowThickness"    # F

    .prologue
    .line 591
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowThickness:F

    .line 592
    return-void
.end method

.method public setmCandleShadowWidth(F)V
    .locals 0
    .param p1, "mCandleShadowWidth"    # F

    .prologue
    .line 583
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphProperty;->mCandleShadowWidth:F

    .line 584
    return-void
.end method
