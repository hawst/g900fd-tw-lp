.class public Lcom/sec/dmc/sic/android/property/SeriesProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "SeriesProperty.java"


# instance fields
.field fixedMaxY:Z

.field fixedMaxYValue:F

.field fixedMinY:Z

.field fixedMinYValue:F

.field sereisName:Ljava/lang/String;

.field seriesID:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 26
    iput v2, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->seriesID:I

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinY:Z

    .line 28
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxY:Z

    .line 29
    iput v1, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinYValue:F

    .line 30
    iput v1, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxYValue:F

    .line 31
    const-string/jumbo v0, "series"

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->sereisName:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 104
    if-ne p0, p1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v1

    .line 106
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 107
    goto :goto_0

    .line 108
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 109
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 110
    check-cast v0, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    .line 111
    .local v0, "other":Lcom/sec/dmc/sic/android/property/SeriesProperty;
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxY:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxY:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 112
    goto :goto_0

    .line 113
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxYValue:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 114
    iget v4, v0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxYValue:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 113
    if-eq v3, v4, :cond_5

    move v1, v2

    .line 115
    goto :goto_0

    .line 116
    :cond_5
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinY:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinY:Z

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 117
    goto :goto_0

    .line 118
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinYValue:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 119
    iget v4, v0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinYValue:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 118
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 120
    goto :goto_0

    .line 121
    :cond_7
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->sereisName:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 122
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->sereisName:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v1, v2

    .line 123
    goto :goto_0

    .line 124
    :cond_8
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->sereisName:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->sereisName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 125
    goto :goto_0

    .line 126
    :cond_9
    iget v3, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->seriesID:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->seriesID:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 127
    goto :goto_0
.end method

.method public getFixedMaxYValue()F
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxYValue:F

    return v0
.end method

.method public getFixedMinYValue()F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinYValue:F

    return v0
.end method

.method public getSereisName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->sereisName:Ljava/lang/String;

    return-object v0
.end method

.method public getSeriesID()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->seriesID:I

    return v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    .line 90
    const/16 v0, 0x1f

    .line 91
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 92
    .local v1, "result":I
    mul-int/lit8 v5, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxY:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v5, v2

    .line 93
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxYValue:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 94
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v5, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinY:Z

    if-eqz v5, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 95
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinYValue:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 96
    mul-int/lit8 v3, v1, 0x1f

    .line 97
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->sereisName:Ljava/lang/String;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    .line 96
    :goto_2
    add-int v1, v3, v2

    .line 98
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->seriesID:I

    add-int v1, v2, v3

    .line 99
    return v1

    :cond_0
    move v2, v4

    .line 92
    goto :goto_0

    :cond_1
    move v3, v4

    .line 94
    goto :goto_1

    .line 97
    :cond_2
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->sereisName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public isFixedMaxY()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxY:Z

    return v0
.end method

.method public isFixedMinY()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinY:Z

    return v0
.end method

.method public setFixedMaxY(Z)V
    .locals 0
    .param p1, "fixedMaxY"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxY:Z

    .line 62
    return-void
.end method

.method public setFixedMaxYValue(F)V
    .locals 0
    .param p1, "fixedMaxYValue"    # F

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMaxYValue:F

    .line 78
    return-void
.end method

.method public setFixedMinY(Z)V
    .locals 0
    .param p1, "fixedMinY"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinY:Z

    .line 58
    return-void
.end method

.method public setFixedMinYValue(F)V
    .locals 0
    .param p1, "fixedMinYValue"    # F

    .prologue
    .line 69
    iput p1, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->fixedMinYValue:F

    .line 70
    return-void
.end method

.method public setSereisName(Ljava/lang/String;)V
    .locals 0
    .param p1, "sereisName"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->sereisName:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setSeriesID(I)V
    .locals 0
    .param p1, "seriesID"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/sec/dmc/sic/android/property/SeriesProperty;->seriesID:I

    .line 46
    return-void
.end method
