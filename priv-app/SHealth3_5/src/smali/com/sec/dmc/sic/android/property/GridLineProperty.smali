.class public Lcom/sec/dmc/sic/android/property/GridLineProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "GridLineProperty.java"


# instance fields
.field fillAlpha:F

.field fillColorB:F

.field fillColorG:F

.field fillColorR:F

.field image:Landroid/graphics/Bitmap;

.field manualMarkingValue:I

.field strokeWidth:F

.field useManualMarking:Z

.field visibleIndex:[I

.field visibleIndexCount:I

.field xLineVisible:Z

.field yLineVisible:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 84
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->enable:Z

    .line 85
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->visible:Z

    .line 87
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->xLineVisible:Z

    .line 88
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->yLineVisible:Z

    .line 90
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->useManualMarking:Z

    .line 92
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorR:F

    .line 93
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorG:F

    .line 94
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorB:F

    .line 95
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillAlpha:F

    .line 97
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->strokeWidth:F

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->image:Landroid/graphics/Bitmap;

    .line 110
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 252
    if-ne p0, p1, :cond_1

    .line 291
    :cond_0
    :goto_0
    return v1

    .line 254
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 255
    goto :goto_0

    .line 256
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 257
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 258
    check-cast v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;

    .line 259
    .local v0, "other":Lcom/sec/dmc/sic/android/property/GridLineProperty;
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 260
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 259
    if-eq v3, v4, :cond_4

    move v1, v2

    .line 261
    goto :goto_0

    .line 262
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 263
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 262
    if-eq v3, v4, :cond_5

    move v1, v2

    .line 264
    goto :goto_0

    .line 265
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 266
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 265
    if-eq v3, v4, :cond_6

    move v1, v2

    .line 267
    goto :goto_0

    .line 268
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 269
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 268
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 270
    goto :goto_0

    .line 271
    :cond_7
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->image:Landroid/graphics/Bitmap;

    if-nez v3, :cond_8

    .line 272
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->image:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_9

    move v1, v2

    .line 273
    goto :goto_0

    .line 274
    :cond_8
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->image:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 275
    goto :goto_0

    .line 276
    :cond_9
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->manualMarkingValue:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->manualMarkingValue:I

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 277
    goto :goto_0

    .line 278
    :cond_a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->strokeWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 279
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->strokeWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 278
    if-eq v3, v4, :cond_b

    move v1, v2

    .line 280
    goto/16 :goto_0

    .line 281
    :cond_b
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->useManualMarking:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->useManualMarking:Z

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 282
    goto/16 :goto_0

    .line 283
    :cond_c
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->visibleIndex:[I

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->visibleIndex:[I

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    .line 284
    goto/16 :goto_0

    .line 285
    :cond_d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->visibleIndexCount:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->visibleIndexCount:I

    if-eq v3, v4, :cond_e

    move v1, v2

    .line 286
    goto/16 :goto_0

    .line 287
    :cond_e
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->xLineVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->xLineVisible:Z

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 288
    goto/16 :goto_0

    .line 289
    :cond_f
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->yLineVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->yLineVisible:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 290
    goto/16 :goto_0
.end method

.method public getFillAlpha()F
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillAlpha:F

    return v0
.end method

.method public getFillColorB()F
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorB:F

    return v0
.end method

.method public getFillColorG()F
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorG:F

    return v0
.end method

.method public getFillColorR()F
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorR:F

    return v0
.end method

.method public getImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->image:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getManualMarkingValue()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->manualMarkingValue:I

    return v0
.end method

.method public getStrokeWidth()F
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->strokeWidth:F

    return v0
.end method

.method public getUseManualMarking()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->useManualMarking:Z

    return v0
.end method

.method public getVisibleIndex()[I
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->visibleIndex:[I

    return-object v0
.end method

.method public getVisibleIndexCount()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->visibleIndexCount:I

    return v0
.end method

.method public getXLineVisible()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->xLineVisible:Z

    return v0
.end method

.method public getYLineVisible()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->yLineVisible:Z

    return v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    .line 232
    const/16 v0, 0x1f

    .line 233
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 234
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillAlpha:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 235
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorB:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 236
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorG:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 237
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorR:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 238
    mul-int/lit8 v5, v1, 0x1f

    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->image:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v5, v2

    .line 239
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->manualMarkingValue:I

    add-int v1, v2, v5

    .line 240
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->strokeWidth:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 241
    mul-int/lit8 v5, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->useManualMarking:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v5, v2

    .line 242
    mul-int/lit8 v2, v1, 0x1f

    iget-object v5, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->visibleIndex:[I

    invoke-static {v5}, Ljava/util/Arrays;->hashCode([I)I

    move-result v5

    add-int v1, v2, v5

    .line 243
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->visibleIndexCount:I

    add-int v1, v2, v5

    .line 244
    mul-int/lit8 v5, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->xLineVisible:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v5, v2

    .line 245
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v5, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->yLineVisible:Z

    if-eqz v5, :cond_3

    :goto_3
    add-int v1, v2, v3

    .line 246
    return v1

    .line 238
    :cond_0
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    move v2, v4

    .line 241
    goto :goto_1

    :cond_2
    move v2, v4

    .line 244
    goto :goto_2

    :cond_3
    move v3, v4

    .line 245
    goto :goto_3
.end method

.method public setFillAlpha(F)V
    .locals 0
    .param p1, "fillAlpha"    # F

    .prologue
    .line 169
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillAlpha:F

    .line 170
    return-void
.end method

.method public setFillColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 173
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorR:F

    .line 174
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorR:F

    .line 175
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorR:F

    .line 176
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillAlpha:F

    .line 177
    return-void
.end method

.method public setFillColorB(F)V
    .locals 0
    .param p1, "fillColorB"    # F

    .prologue
    .line 161
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorB:F

    .line 162
    return-void
.end method

.method public setFillColorG(F)V
    .locals 0
    .param p1, "fillColorG"    # F

    .prologue
    .line 153
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorG:F

    .line 154
    return-void
.end method

.method public setFillColorR(F)V
    .locals 0
    .param p1, "fillColorR"    # F

    .prologue
    .line 145
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->fillColorR:F

    .line 146
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->image:Landroid/graphics/Bitmap;

    .line 227
    return-void
.end method

.method public setManualMarkingValue(I)V
    .locals 0
    .param p1, "manualMarkingValue"    # I

    .prologue
    .line 217
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->manualMarkingValue:I

    .line 218
    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 0
    .param p1, "strokeWidth"    # F

    .prologue
    .line 184
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->strokeWidth:F

    .line 185
    return-void
.end method

.method public setUseManualMarking(Z)V
    .locals 0
    .param p1, "useManualMarking"    # Z

    .prologue
    .line 193
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->useManualMarking:Z

    .line 194
    return-void
.end method

.method public setVisibleIndex([I)V
    .locals 0
    .param p1, "visibleIndex"    # [I

    .prologue
    .line 209
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->visibleIndex:[I

    .line 210
    return-void
.end method

.method public setVisibleIndexCount(I)V
    .locals 0
    .param p1, "visibleIndexCount"    # I

    .prologue
    .line 201
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->visibleIndexCount:I

    .line 202
    return-void
.end method

.method public setXLineVisible(Z)V
    .locals 0
    .param p1, "xLineVisible"    # Z

    .prologue
    .line 126
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->xLineVisible:Z

    .line 127
    return-void
.end method

.method public setYLineVisible(Z)V
    .locals 0
    .param p1, "yLineVisible"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/GridLineProperty;->yLineVisible:Z

    .line 137
    return-void
.end method
