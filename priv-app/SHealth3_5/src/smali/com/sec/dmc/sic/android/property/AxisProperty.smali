.class public Lcom/sec/dmc/sic/android/property/AxisProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "AxisProperty.java"


# static fields
.field public static final AXIS_DIRECTTION_BOTTOM:I = 0x3

.field public static final AXIS_DIRECTTION_LEFT:I = 0x0

.field public static final AXIS_DIRECTTION_RIGHT:I = 0x1

.field public static final AXIS_DIRECTTION_TOP:I = 0x2

.field public static final AXIS_TYPE_SECOND_Y:I = 0x2

.field public static final AXIS_TYPE_X:I = 0x0

.field public static final AXIS_TYPE_Y:I = 0x1

.field public static final LABLE_TITLE_ALIGN_BOTTOM:I = 0x1

.field public static final LABLE_TITLE_ALIGN_CENTER:I = 0x2

.field public static final LABLE_TITLE_ALIGN_TOP:I = 0x0

.field public static final TITLE_ALIGN_BOTTOM:I = 0x3

.field public static final TITLE_ALIGN_CENTER:I = 0x4

.field public static final TITLE_ALIGN_LEFT:I = 0x0

.field public static final TITLE_ALIGN_RIGHT:I = 0x1

.field public static final TITLE_ALIGN_TOP:I = 0x2

.field public static final XAXIS_LABEL_ALIGN_DOWN:I = 0x3

.field public static final XAXIS_LABEL_ALIGN_UP:I = 0x2

.field public static final YAXIS_LABEL_ALIGN_LEFT:I = 0x0

.field public static final YAXIS_LABEL_ALIGN_RIGHT:I = 0x1

.field public static final YAXIS_ROUNDING_ROUND:I = 0x0

.field public static final YAXIS_ROUNDING_ROUNDDOWN:I = 0x2

.field public static final YAXIS_ROUNDING_ROUNDUP:I = 0x1


# instance fields
.field associatedSeriesIDs:[I

.field associatedSeriesIDsCount:I

.field axisID:I

.field fillAlpha:F

.field fillColorB:F

.field fillColorG:F

.field fillColorR:F

.field mCustomYLabelStrs:[Ljava/lang/String;

.field mCustomYLabelStrsCount:I

.field mDateFormat:Ljava/lang/String;

.field mFirstDayOfWeekend:I

.field mLabelTitleAlign:I

.field mLabelTitleOffset:F

.field mLabelTitleText:Ljava/lang/String;

.field mLabelTitleTextBitmap:Landroid/graphics/Bitmap;

.field mLabelTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field mSecondDayOfWeekend:I

.field mSeparatorDateFormat:Ljava/lang/String;

.field mSeparatorLineThickness:F

.field mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field mSeparatorVisible:Z

.field mSpacingAlign:F

.field mSpacingAxisLine:F

.field mTitleText:Ljava/lang/String;

.field mTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field mUseColorForWeekend:Z

.field mXAxisLabelAlign:I

.field mXAxisTitleAlign:I

.field mYAxisLabelAlign:I

.field mYAxisMaxRoundDigit:I

.field mYAxisMaxRoundMagnification:F

.field mYAxisMaxRounding:I

.field mYAxisMinRoundDigit:I

.field mYAxisMinRoundMagnification:F

.field mYAxisMinRounding:I

.field mYAxisReversed:Z

.field mYAxisTitleAlign:I

.field mYLabelOffsets:[F

.field mYlabelOffsetsCount:I

.field mainTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field manualMarkingValue:I

.field separatorSpacingLine:I

.field separatorSpacingTop:I

.field strokeWidth:F

.field subTextSpace:F

.field subTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field subTextUse:Z

.field textSpace:F

.field textStyleForWeekend1:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field textStyleForWeekend2:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field typeDirection:I

.field typeXY:I

.field useManualMarking:Z

.field visibleAxisLine:Z

.field visibleAxisText:Z

.field visibleIndex:[I

.field visibleIndexCount:I

.field visibleMarking:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 192
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 80
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDs:[I

    .line 196
    iput v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDsCount:I

    .line 198
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisLine:Z

    .line 199
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisText:Z

    .line 200
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleMarking:Z

    .line 201
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->useManualMarking:Z

    .line 203
    iput v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleIndexCount:I

    .line 206
    iput v4, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorR:F

    .line 207
    iput v4, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorG:F

    .line 208
    iput v4, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorB:F

    .line 209
    iput v1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillAlpha:F

    .line 211
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->strokeWidth:F

    .line 213
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mainTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 214
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 216
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleText:Ljava/lang/String;

    .line 217
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 219
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleText:Ljava/lang/String;

    .line 221
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisLabelAlign:I

    .line 222
    iput v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisLabelAlign:I

    .line 224
    iput v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYlabelOffsetsCount:I

    .line 226
    iput v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisTitleAlign:I

    .line 227
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisTitleAlign:I

    .line 229
    iput v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleAlign:I

    .line 230
    iput v4, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleOffset:F

    .line 232
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAxisLine:F

    .line 233
    iput v4, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAlign:F

    .line 235
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 237
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorVisible:Z

    .line 238
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingTop:I

    .line 239
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingLine:I

    .line 240
    iput v1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorLineThickness:F

    .line 242
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 243
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 244
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 245
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    const/16 v1, 0xff

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 246
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 248
    iput-object v5, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mDateFormat:Ljava/lang/String;

    .line 249
    iput-object v5, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorDateFormat:Ljava/lang/String;

    .line 250
    iput v4, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textSpace:F

    .line 251
    iput v4, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextSpace:F

    .line 253
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextUse:Z

    .line 254
    iput v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mCustomYLabelStrsCount:I

    .line 256
    iput-object v5, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextBitmap:Landroid/graphics/Bitmap;

    .line 258
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mUseColorForWeekend:Z

    .line 259
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mFirstDayOfWeekend:I

    .line 260
    iput v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSecondDayOfWeekend:I

    .line 262
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend1:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 263
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend2:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 265
    iput v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRounding:I

    .line 266
    iput v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundDigit:I

    .line 267
    const v0, 0x3f8ccccd    # 1.1f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundMagnification:F

    .line 269
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRounding:I

    .line 270
    iput v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundDigit:I

    .line 271
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundMagnification:F

    .line 273
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisReversed:Z

    .line 274
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 919
    if-ne p0, p1, :cond_1

    .line 1091
    :cond_0
    :goto_0
    return v1

    .line 921
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 922
    goto :goto_0

    .line 923
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 924
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 925
    check-cast v0, Lcom/sec/dmc/sic/android/property/AxisProperty;

    .line 926
    .local v0, "other":Lcom/sec/dmc/sic/android/property/AxisProperty;
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDs:[I

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDs:[I

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 927
    goto :goto_0

    .line 928
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDsCount:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDsCount:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 929
    goto :goto_0

    .line 930
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->axisID:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->axisID:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 931
    goto :goto_0

    .line 932
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 933
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 932
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 934
    goto :goto_0

    .line 935
    :cond_7
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 936
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 935
    if-eq v3, v4, :cond_8

    move v1, v2

    .line 937
    goto :goto_0

    .line 938
    :cond_8
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 939
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 938
    if-eq v3, v4, :cond_9

    move v1, v2

    .line 940
    goto :goto_0

    .line 941
    :cond_9
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 942
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 941
    if-eq v3, v4, :cond_a

    move v1, v2

    .line 943
    goto :goto_0

    .line 944
    :cond_a
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mCustomYLabelStrs:[Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mCustomYLabelStrs:[Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 945
    goto :goto_0

    .line 946
    :cond_b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mCustomYLabelStrsCount:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mCustomYLabelStrsCount:I

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 947
    goto/16 :goto_0

    .line 948
    :cond_c
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mDateFormat:Ljava/lang/String;

    if-nez v3, :cond_d

    .line 949
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mDateFormat:Ljava/lang/String;

    if-eqz v3, :cond_e

    move v1, v2

    .line 950
    goto/16 :goto_0

    .line 951
    :cond_d
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mDateFormat:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mDateFormat:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 952
    goto/16 :goto_0

    .line 953
    :cond_e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mFirstDayOfWeekend:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mFirstDayOfWeekend:I

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 954
    goto/16 :goto_0

    .line 955
    :cond_f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleAlign:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleAlign:I

    if-eq v3, v4, :cond_10

    move v1, v2

    .line 956
    goto/16 :goto_0

    .line 957
    :cond_10
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleOffset:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 958
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleOffset:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 957
    if-eq v3, v4, :cond_11

    move v1, v2

    .line 959
    goto/16 :goto_0

    .line 960
    :cond_11
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleText:Ljava/lang/String;

    if-nez v3, :cond_12

    .line 961
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleText:Ljava/lang/String;

    if-eqz v3, :cond_13

    move v1, v2

    .line 962
    goto/16 :goto_0

    .line 963
    :cond_12
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleText:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    move v1, v2

    .line 964
    goto/16 :goto_0

    .line 965
    :cond_13
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextBitmap:Landroid/graphics/Bitmap;

    if-nez v3, :cond_14

    .line 966
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_15

    move v1, v2

    .line 967
    goto/16 :goto_0

    .line 968
    :cond_14
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextBitmap:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    move v1, v2

    .line 969
    goto/16 :goto_0

    .line 970
    :cond_15
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_16

    .line 971
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_17

    move v1, v2

    .line 972
    goto/16 :goto_0

    .line 973
    :cond_16
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    move v1, v2

    .line 974
    goto/16 :goto_0

    .line 975
    :cond_17
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSecondDayOfWeekend:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSecondDayOfWeekend:I

    if-eq v3, v4, :cond_18

    move v1, v2

    .line 976
    goto/16 :goto_0

    .line 977
    :cond_18
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorDateFormat:Ljava/lang/String;

    if-nez v3, :cond_19

    .line 978
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorDateFormat:Ljava/lang/String;

    if-eqz v3, :cond_1a

    move v1, v2

    .line 979
    goto/16 :goto_0

    .line 980
    :cond_19
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorDateFormat:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorDateFormat:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1a

    move v1, v2

    .line 981
    goto/16 :goto_0

    .line 982
    :cond_1a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorLineThickness:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 983
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorLineThickness:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 982
    if-eq v3, v4, :cond_1b

    move v1, v2

    .line 984
    goto/16 :goto_0

    .line 985
    :cond_1b
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_1c

    .line 986
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_1d

    move v1, v2

    .line 987
    goto/16 :goto_0

    .line 988
    :cond_1c
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    move v1, v2

    .line 989
    goto/16 :goto_0

    .line 990
    :cond_1d
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorVisible:Z

    if-eq v3, v4, :cond_1e

    move v1, v2

    .line 991
    goto/16 :goto_0

    .line 992
    :cond_1e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAlign:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 993
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAlign:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 992
    if-eq v3, v4, :cond_1f

    move v1, v2

    .line 994
    goto/16 :goto_0

    .line 995
    :cond_1f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAxisLine:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 996
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAxisLine:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 995
    if-eq v3, v4, :cond_20

    move v1, v2

    .line 997
    goto/16 :goto_0

    .line 998
    :cond_20
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleText:Ljava/lang/String;

    if-nez v3, :cond_21

    .line 999
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleText:Ljava/lang/String;

    if-eqz v3, :cond_22

    move v1, v2

    .line 1000
    goto/16 :goto_0

    .line 1001
    :cond_21
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleText:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    move v1, v2

    .line 1002
    goto/16 :goto_0

    .line 1003
    :cond_22
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_23

    .line 1004
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_24

    move v1, v2

    .line 1005
    goto/16 :goto_0

    .line 1006
    :cond_23
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_24

    move v1, v2

    .line 1007
    goto/16 :goto_0

    .line 1008
    :cond_24
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mUseColorForWeekend:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mUseColorForWeekend:Z

    if-eq v3, v4, :cond_25

    move v1, v2

    .line 1009
    goto/16 :goto_0

    .line 1010
    :cond_25
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisLabelAlign:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisLabelAlign:I

    if-eq v3, v4, :cond_26

    move v1, v2

    .line 1011
    goto/16 :goto_0

    .line 1012
    :cond_26
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisTitleAlign:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisTitleAlign:I

    if-eq v3, v4, :cond_27

    move v1, v2

    .line 1013
    goto/16 :goto_0

    .line 1014
    :cond_27
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisLabelAlign:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisLabelAlign:I

    if-eq v3, v4, :cond_28

    move v1, v2

    .line 1015
    goto/16 :goto_0

    .line 1016
    :cond_28
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundDigit:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundDigit:I

    if-eq v3, v4, :cond_29

    move v1, v2

    .line 1017
    goto/16 :goto_0

    .line 1018
    :cond_29
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundMagnification:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 1019
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundMagnification:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 1018
    if-eq v3, v4, :cond_2a

    move v1, v2

    .line 1020
    goto/16 :goto_0

    .line 1021
    :cond_2a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRounding:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRounding:I

    if-eq v3, v4, :cond_2b

    move v1, v2

    .line 1022
    goto/16 :goto_0

    .line 1023
    :cond_2b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundDigit:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundDigit:I

    if-eq v3, v4, :cond_2c

    move v1, v2

    .line 1024
    goto/16 :goto_0

    .line 1025
    :cond_2c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundMagnification:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 1026
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundMagnification:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 1025
    if-eq v3, v4, :cond_2d

    move v1, v2

    .line 1027
    goto/16 :goto_0

    .line 1028
    :cond_2d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRounding:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRounding:I

    if-eq v3, v4, :cond_2e

    move v1, v2

    .line 1029
    goto/16 :goto_0

    .line 1030
    :cond_2e
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisReversed:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisReversed:Z

    if-eq v3, v4, :cond_2f

    move v1, v2

    .line 1031
    goto/16 :goto_0

    .line 1032
    :cond_2f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisTitleAlign:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisTitleAlign:I

    if-eq v3, v4, :cond_30

    move v1, v2

    .line 1033
    goto/16 :goto_0

    .line 1034
    :cond_30
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYLabelOffsets:[F

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYLabelOffsets:[F

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v3

    if-nez v3, :cond_31

    move v1, v2

    .line 1035
    goto/16 :goto_0

    .line 1036
    :cond_31
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYlabelOffsetsCount:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYlabelOffsetsCount:I

    if-eq v3, v4, :cond_32

    move v1, v2

    .line 1037
    goto/16 :goto_0

    .line 1038
    :cond_32
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mainTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_33

    .line 1039
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mainTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_34

    move v1, v2

    .line 1040
    goto/16 :goto_0

    .line 1041
    :cond_33
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mainTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mainTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_34

    move v1, v2

    .line 1042
    goto/16 :goto_0

    .line 1043
    :cond_34
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->manualMarkingValue:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->manualMarkingValue:I

    if-eq v3, v4, :cond_35

    move v1, v2

    .line 1044
    goto/16 :goto_0

    .line 1045
    :cond_35
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingLine:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingLine:I

    if-eq v3, v4, :cond_36

    move v1, v2

    .line 1046
    goto/16 :goto_0

    .line 1047
    :cond_36
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingTop:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingTop:I

    if-eq v3, v4, :cond_37

    move v1, v2

    .line 1048
    goto/16 :goto_0

    .line 1049
    :cond_37
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->strokeWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 1050
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->strokeWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 1049
    if-eq v3, v4, :cond_38

    move v1, v2

    .line 1051
    goto/16 :goto_0

    .line 1052
    :cond_38
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextSpace:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 1053
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextSpace:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 1052
    if-eq v3, v4, :cond_39

    move v1, v2

    .line 1054
    goto/16 :goto_0

    .line 1055
    :cond_39
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_3a

    .line 1056
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_3b

    move v1, v2

    .line 1057
    goto/16 :goto_0

    .line 1058
    :cond_3a
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3b

    move v1, v2

    .line 1059
    goto/16 :goto_0

    .line 1060
    :cond_3b
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextUse:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextUse:Z

    if-eq v3, v4, :cond_3c

    move v1, v2

    .line 1061
    goto/16 :goto_0

    .line 1062
    :cond_3c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textSpace:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 1063
    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textSpace:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 1062
    if-eq v3, v4, :cond_3d

    move v1, v2

    .line 1064
    goto/16 :goto_0

    .line 1065
    :cond_3d
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend1:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_3e

    .line 1066
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend1:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_3f

    move v1, v2

    .line 1067
    goto/16 :goto_0

    .line 1068
    :cond_3e
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend1:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend1:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3f

    move v1, v2

    .line 1069
    goto/16 :goto_0

    .line 1070
    :cond_3f
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend2:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_40

    .line 1071
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend2:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_41

    move v1, v2

    .line 1072
    goto/16 :goto_0

    .line 1073
    :cond_40
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend2:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend2:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_41

    move v1, v2

    .line 1074
    goto/16 :goto_0

    .line 1075
    :cond_41
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->typeDirection:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->typeDirection:I

    if-eq v3, v4, :cond_42

    move v1, v2

    .line 1076
    goto/16 :goto_0

    .line 1077
    :cond_42
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->typeXY:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->typeXY:I

    if-eq v3, v4, :cond_43

    move v1, v2

    .line 1078
    goto/16 :goto_0

    .line 1079
    :cond_43
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->useManualMarking:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->useManualMarking:Z

    if-eq v3, v4, :cond_44

    move v1, v2

    .line 1080
    goto/16 :goto_0

    .line 1081
    :cond_44
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisLine:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisLine:Z

    if-eq v3, v4, :cond_45

    move v1, v2

    .line 1082
    goto/16 :goto_0

    .line 1083
    :cond_45
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisText:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisText:Z

    if-eq v3, v4, :cond_46

    move v1, v2

    .line 1084
    goto/16 :goto_0

    .line 1085
    :cond_46
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleIndex:[I

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleIndex:[I

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_47

    move v1, v2

    .line 1086
    goto/16 :goto_0

    .line 1087
    :cond_47
    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleIndexCount:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleIndexCount:I

    if-eq v3, v4, :cond_48

    move v1, v2

    .line 1088
    goto/16 :goto_0

    .line 1089
    :cond_48
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleMarking:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleMarking:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 1090
    goto/16 :goto_0
.end method

.method public getAssociatedSeriesIDs()[I
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDs:[I

    return-object v0
.end method

.method public getAssociatedSeriesIDsCount()I
    .locals 1

    .prologue
    .line 312
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDsCount:I

    return v0
.end method

.method public getAxisID()I
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->axisID:I

    return v0
.end method

.method public getCustomYLabelStrs()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mCustomYLabelStrs:[Ljava/lang/String;

    return-object v0
.end method

.method public getDateFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mDateFormat:Ljava/lang/String;

    return-object v0
.end method

.method public getFillAlpha()F
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillAlpha:F

    return v0
.end method

.method public getFillColorB()F
    .locals 1

    .prologue
    .line 409
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorB:F

    return v0
.end method

.method public getFillColorG()F
    .locals 1

    .prologue
    .line 401
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorG:F

    return v0
.end method

.method public getFillColorR()F
    .locals 1

    .prologue
    .line 393
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorR:F

    return v0
.end method

.method public getFirstDayOfWeekend()I
    .locals 1

    .prologue
    .line 739
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mFirstDayOfWeekend:I

    return v0
.end method

.method public getLabelTitleAlign()I
    .locals 1

    .prologue
    .line 650
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleAlign:I

    return v0
.end method

.method public getLabelTitleOffset()F
    .locals 1

    .prologue
    .line 575
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleOffset:F

    return v0
.end method

.method public getLabelTitleText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleText:Ljava/lang/String;

    return-object v0
.end method

.method public getLabelTitleTextBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getLabelTitleTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 566
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getMainTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mainTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getManualMarkingValue()I
    .locals 1

    .prologue
    .line 385
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->manualMarkingValue:I

    return v0
.end method

.method public getSecondDayOfWeekend()I
    .locals 1

    .prologue
    .line 749
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSecondDayOfWeekend:I

    return v0
.end method

.method public getSeparatorDateFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorDateFormat:Ljava/lang/String;

    return-object v0
.end method

.method public getSeparatorLineThickness()F
    .locals 1

    .prologue
    .line 613
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorLineThickness:F

    return v0
.end method

.method public getSeparatorSpacingLine()I
    .locals 1

    .prologue
    .line 604
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingLine:I

    return v0
.end method

.method public getSeparatorSpacingTop()I
    .locals 1

    .prologue
    .line 594
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingTop:I

    return v0
.end method

.method public getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getSeparatorVisible()Z
    .locals 1

    .prologue
    .line 585
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorVisible:Z

    return v0
.end method

.method public getSpacingAlign()F
    .locals 1

    .prologue
    .line 508
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAlign:F

    return v0
.end method

.method public getSpacingAxisLine()F
    .locals 1

    .prologue
    .line 498
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAxisLine:F

    return v0
.end method

.method public getStrokeWidth()F
    .locals 1

    .prologue
    .line 433
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->strokeWidth:F

    return v0
.end method

.method public getSubTextSpace()F
    .locals 1

    .prologue
    .line 680
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextSpace:F

    return v0
.end method

.method public getSubTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getTextSpace()F
    .locals 1

    .prologue
    .line 641
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textSpace:F

    return v0
.end method

.method public getTextStyleForWeekend1()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend1:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getTextStyleForWeekend2()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend2:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getTitleText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleText:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getTypeDirection()I
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->typeDirection:I

    return v0
.end method

.method public getTypeXY()I
    .locals 1

    .prologue
    .line 321
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->typeXY:I

    return v0
.end method

.method public getUseColorForWeekend()Z
    .locals 1

    .prologue
    .line 729
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mUseColorForWeekend:Z

    return v0
.end method

.method public getUseManualMarking()Z
    .locals 1

    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->useManualMarking:Z

    return v0
.end method

.method public getVisibleAxisLine()Z
    .locals 1

    .prologue
    .line 337
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisLine:Z

    return v0
.end method

.method public getVisibleAxisText()Z
    .locals 1

    .prologue
    .line 345
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisText:Z

    return v0
.end method

.method public getVisibleIndex()[I
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleIndex:[I

    return-object v0
.end method

.method public getVisibleIndexCount()I
    .locals 1

    .prologue
    .line 369
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleIndexCount:I

    return v0
.end method

.method public getVisibleMarking()Z
    .locals 1

    .prologue
    .line 353
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleMarking:Z

    return v0
.end method

.method public getXAxisLabelAlign()I
    .locals 1

    .prologue
    .line 528
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisLabelAlign:I

    return v0
.end method

.method public getXAxisTitleAlign()I
    .locals 1

    .prologue
    .line 478
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisTitleAlign:I

    return v0
.end method

.method public getYAxisLabelAlign()I
    .locals 1

    .prologue
    .line 538
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisLabelAlign:I

    return v0
.end method

.method public getYAxisMaxRoundDigit()I
    .locals 1

    .prologue
    .line 768
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundDigit:I

    return v0
.end method

.method public getYAxisMaxRoundMagnification()F
    .locals 1

    .prologue
    .line 778
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundMagnification:F

    return v0
.end method

.method public getYAxisMaxRounding()I
    .locals 1

    .prologue
    .line 758
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRounding:I

    return v0
.end method

.method public getYAxisMinRoundDigit()I
    .locals 1

    .prologue
    .line 797
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundDigit:I

    return v0
.end method

.method public getYAxisMinRoundMagnification()F
    .locals 1

    .prologue
    .line 807
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundMagnification:F

    return v0
.end method

.method public getYAxisMinRounding()I
    .locals 1

    .prologue
    .line 787
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRounding:I

    return v0
.end method

.method public getYAxisReversed()Z
    .locals 1

    .prologue
    .line 816
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisReversed:Z

    return v0
.end method

.method public getYAxisTitleAlign()I
    .locals 1

    .prologue
    .line 488
    iget v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisTitleAlign:I

    return v0
.end method

.method public getYLabelOffsets()[F
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYLabelOffsets:[F

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v5, 0x4d5

    const/16 v4, 0x4cf

    const/4 v3, 0x0

    .line 827
    const/16 v0, 0x1f

    .line 828
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 829
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDs:[I

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([I)I

    move-result v6

    add-int v1, v2, v6

    .line 830
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDsCount:I

    add-int v1, v2, v6

    .line 831
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->axisID:I

    add-int v1, v2, v6

    .line 832
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 833
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 834
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 835
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 836
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mCustomYLabelStrs:[Ljava/lang/String;

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v6

    add-int v1, v2, v6

    .line 837
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mCustomYLabelStrsCount:I

    add-int v1, v2, v6

    .line 838
    mul-int/lit8 v6, v1, 0x1f

    .line 839
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mDateFormat:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    .line 838
    :goto_0
    add-int v1, v6, v2

    .line 840
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mFirstDayOfWeekend:I

    add-int v1, v2, v6

    .line 841
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleAlign:I

    add-int v1, v2, v6

    .line 842
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleOffset:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 843
    mul-int/lit8 v6, v1, 0x1f

    .line 844
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleText:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    .line 843
    :goto_1
    add-int v1, v6, v2

    .line 845
    mul-int/lit8 v6, v1, 0x1f

    .line 847
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_2

    move v2, v3

    .line 845
    :goto_2
    add-int v1, v6, v2

    .line 849
    mul-int/lit8 v6, v1, 0x1f

    .line 851
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_3

    move v2, v3

    .line 849
    :goto_3
    add-int v1, v6, v2

    .line 853
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSecondDayOfWeekend:I

    add-int v1, v2, v6

    .line 854
    mul-int/lit8 v6, v1, 0x1f

    .line 856
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorDateFormat:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    .line 854
    :goto_4
    add-int v1, v6, v2

    .line 858
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorLineThickness:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 859
    mul-int/lit8 v6, v1, 0x1f

    .line 861
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_5

    move v2, v3

    .line 859
    :goto_5
    add-int v1, v6, v2

    .line 863
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorVisible:Z

    if-eqz v2, :cond_6

    move v2, v4

    :goto_6
    add-int v1, v6, v2

    .line 864
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAlign:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 865
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAxisLine:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 866
    mul-int/lit8 v6, v1, 0x1f

    .line 867
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleText:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    .line 866
    :goto_7
    add-int v1, v6, v2

    .line 868
    mul-int/lit8 v6, v1, 0x1f

    .line 869
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_8

    move v2, v3

    .line 868
    :goto_8
    add-int v1, v6, v2

    .line 870
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mUseColorForWeekend:Z

    if-eqz v2, :cond_9

    move v2, v4

    :goto_9
    add-int v1, v6, v2

    .line 871
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisLabelAlign:I

    add-int v1, v2, v6

    .line 872
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisTitleAlign:I

    add-int v1, v2, v6

    .line 873
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisLabelAlign:I

    add-int v1, v2, v6

    .line 874
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundDigit:I

    add-int v1, v2, v6

    .line 875
    mul-int/lit8 v2, v1, 0x1f

    .line 876
    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundMagnification:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    .line 875
    add-int v1, v2, v6

    .line 877
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRounding:I

    add-int v1, v2, v6

    .line 878
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundDigit:I

    add-int v1, v2, v6

    .line 879
    mul-int/lit8 v2, v1, 0x1f

    .line 880
    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundMagnification:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    .line 879
    add-int v1, v2, v6

    .line 881
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRounding:I

    add-int v1, v2, v6

    .line 882
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisReversed:Z

    if-eqz v2, :cond_a

    move v2, v4

    :goto_a
    add-int v1, v6, v2

    .line 883
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisTitleAlign:I

    add-int v1, v2, v6

    .line 884
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYLabelOffsets:[F

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([F)I

    move-result v6

    add-int v1, v2, v6

    .line 885
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYlabelOffsetsCount:I

    add-int v1, v2, v6

    .line 886
    mul-int/lit8 v6, v1, 0x1f

    .line 887
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mainTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_b

    move v2, v3

    .line 886
    :goto_b
    add-int v1, v6, v2

    .line 888
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->manualMarkingValue:I

    add-int v1, v2, v6

    .line 889
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingLine:I

    add-int v1, v2, v6

    .line 890
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingTop:I

    add-int v1, v2, v6

    .line 891
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->strokeWidth:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 892
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextSpace:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 893
    mul-int/lit8 v6, v1, 0x1f

    .line 894
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_c

    move v2, v3

    .line 893
    :goto_c
    add-int v1, v6, v2

    .line 895
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextUse:Z

    if-eqz v2, :cond_d

    move v2, v4

    :goto_d
    add-int v1, v6, v2

    .line 896
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textSpace:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 897
    mul-int/lit8 v6, v1, 0x1f

    .line 899
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend1:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_e

    move v2, v3

    .line 897
    :goto_e
    add-int v1, v6, v2

    .line 901
    mul-int/lit8 v2, v1, 0x1f

    .line 903
    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend2:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v6, :cond_f

    .line 901
    :goto_f
    add-int v1, v2, v3

    .line 905
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->typeDirection:I

    add-int v1, v2, v3

    .line 906
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->typeXY:I

    add-int v1, v2, v3

    .line 907
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->useManualMarking:Z

    if-eqz v2, :cond_10

    move v2, v4

    :goto_10
    add-int v1, v3, v2

    .line 908
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisLine:Z

    if-eqz v2, :cond_11

    move v2, v4

    :goto_11
    add-int v1, v3, v2

    .line 909
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisText:Z

    if-eqz v2, :cond_12

    move v2, v4

    :goto_12
    add-int v1, v3, v2

    .line 910
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleIndex:[I

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([I)I

    move-result v3

    add-int v1, v2, v3

    .line 911
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleIndexCount:I

    add-int v1, v2, v3

    .line 912
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleMarking:Z

    if-eqz v3, :cond_13

    :goto_13
    add-int v1, v2, v4

    .line 913
    return v1

    .line 839
    :cond_0
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mDateFormat:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 844
    :cond_1
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 847
    :cond_2
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextBitmap:Landroid/graphics/Bitmap;

    .line 848
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 851
    :cond_3
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 852
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 856
    :cond_4
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorDateFormat:Ljava/lang/String;

    .line 857
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 861
    :cond_5
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 862
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto/16 :goto_5

    :cond_6
    move v2, v5

    .line 863
    goto/16 :goto_6

    .line 867
    :cond_7
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 869
    :cond_8
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto/16 :goto_8

    :cond_9
    move v2, v5

    .line 870
    goto/16 :goto_9

    :cond_a
    move v2, v5

    .line 882
    goto/16 :goto_a

    .line 887
    :cond_b
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mainTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto/16 :goto_b

    .line 894
    :cond_c
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto/16 :goto_c

    :cond_d
    move v2, v5

    .line 895
    goto/16 :goto_d

    .line 899
    :cond_e
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend1:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 900
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto/16 :goto_e

    .line 903
    :cond_f
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend2:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 904
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v3

    goto/16 :goto_f

    :cond_10
    move v2, v5

    .line 907
    goto/16 :goto_10

    :cond_11
    move v2, v5

    .line 908
    goto/16 :goto_11

    :cond_12
    move v2, v5

    .line 909
    goto/16 :goto_12

    :cond_13
    move v4, v5

    .line 912
    goto :goto_13
.end method

.method public isSubTextUse()Z
    .locals 1

    .prologue
    .line 690
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextUse:Z

    return v0
.end method

.method public setAssociatedSeriesIDs([I)V
    .locals 0
    .param p1, "associatedSeriesIDs"    # [I

    .prologue
    .line 307
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDs:[I

    .line 308
    return-void
.end method

.method public setAssociatedSeriesIDsCount(I)V
    .locals 0
    .param p1, "associatedSeriesIDsCount"    # I

    .prologue
    .line 317
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->associatedSeriesIDsCount:I

    .line 318
    return-void
.end method

.method public setAxisID(I)V
    .locals 0
    .param p1, "axisID"    # I

    .prologue
    .line 288
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->axisID:I

    .line 289
    return-void
.end method

.method public setCustomYLabelStrs([Ljava/lang/String;)V
    .locals 1
    .param p1, "customYLabelStrs"    # [Ljava/lang/String;

    .prologue
    .line 635
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mCustomYLabelStrs:[Ljava/lang/String;

    .line 636
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mCustomYLabelStrs:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mCustomYLabelStrsCount:I

    .line 637
    return-void
.end method

.method public setDateFormat(Ljava/lang/String;)V
    .locals 0
    .param p1, "dateFormat"    # Ljava/lang/String;

    .prologue
    .line 664
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mDateFormat:Ljava/lang/String;

    .line 666
    return-void
.end method

.method public setFillAlpha(F)V
    .locals 0
    .param p1, "fillAlpha"    # F

    .prologue
    .line 421
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillAlpha:F

    .line 422
    return-void
.end method

.method public setFillColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 426
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorR:F

    .line 427
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorG:F

    .line 428
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorB:F

    .line 429
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillAlpha:F

    .line 430
    return-void
.end method

.method public setFillColorB(F)V
    .locals 0
    .param p1, "fillColorB"    # F

    .prologue
    .line 413
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorB:F

    .line 414
    return-void
.end method

.method public setFillColorG(F)V
    .locals 0
    .param p1, "fillColorG"    # F

    .prologue
    .line 405
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorG:F

    .line 406
    return-void
.end method

.method public setFillColorR(F)V
    .locals 0
    .param p1, "fillColorR"    # F

    .prologue
    .line 397
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->fillColorR:F

    .line 398
    return-void
.end method

.method public setFirstDayOfWeekend(I)V
    .locals 0
    .param p1, "firstDayOfWeekend"    # I

    .prologue
    .line 744
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mFirstDayOfWeekend:I

    .line 745
    return-void
.end method

.method public setLabelTitleAlign(I)V
    .locals 0
    .param p1, "labelTitleAlign"    # I

    .prologue
    .line 659
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleAlign:I

    .line 660
    return-void
.end method

.method public setLabelTitleOffset(F)V
    .locals 0
    .param p1, "labelTitleOffset"    # F

    .prologue
    .line 580
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleOffset:F

    .line 581
    return-void
.end method

.method public setLabelTitleText(Ljava/lang/String;)V
    .locals 0
    .param p1, "labelTitleText"    # Ljava/lang/String;

    .prologue
    .line 561
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleText:Ljava/lang/String;

    .line 562
    return-void
.end method

.method public setLabelTitleTextBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "labelTitleTextBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 704
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextBitmap:Landroid/graphics/Bitmap;

    .line 705
    return-void
.end method

.method public setLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 571
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mLabelTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 572
    return-void
.end method

.method public setMainTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 454
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mainTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 455
    return-void
.end method

.method public setManualMarkingValue(I)V
    .locals 0
    .param p1, "manualMarkingValue"    # I

    .prologue
    .line 389
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->manualMarkingValue:I

    .line 390
    return-void
.end method

.method public setSecondDayOfWeekend(I)V
    .locals 0
    .param p1, "secondDayOfWeekend"    # I

    .prologue
    .line 754
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSecondDayOfWeekend:I

    .line 755
    return-void
.end method

.method public setSeparatorDateFormat(Ljava/lang/String;)V
    .locals 0
    .param p1, "dateFormat"    # Ljava/lang/String;

    .prologue
    .line 675
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorDateFormat:Ljava/lang/String;

    .line 677
    return-void
.end method

.method public setSeparatorLineThickness(F)V
    .locals 0
    .param p1, "separatorLineThickness"    # F

    .prologue
    .line 618
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorLineThickness:F

    .line 619
    return-void
.end method

.method public setSeparatorSpacingLine(I)V
    .locals 0
    .param p1, "separatorSpacingLine"    # I

    .prologue
    .line 609
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingLine:I

    .line 610
    return-void
.end method

.method public setSeparatorSpacingTop(I)V
    .locals 0
    .param p1, "separatorSpacingTop"    # I

    .prologue
    .line 599
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->separatorSpacingTop:I

    .line 600
    return-void
.end method

.method public setSeparatorTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 626
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 627
    return-void
.end method

.method public setSeparatorVisible(Z)V
    .locals 0
    .param p1, "separatorVisible"    # Z

    .prologue
    .line 590
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSeparatorVisible:Z

    .line 591
    return-void
.end method

.method public setSpacingAlign(F)V
    .locals 0
    .param p1, "spacingAlign"    # F

    .prologue
    .line 513
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAlign:F

    .line 514
    return-void
.end method

.method public setSpacingAxisLine(F)V
    .locals 0
    .param p1, "spacingAxisLine"    # F

    .prologue
    .line 503
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mSpacingAxisLine:F

    .line 504
    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 0
    .param p1, "strokeWidth"    # F

    .prologue
    .line 437
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->strokeWidth:F

    .line 438
    return-void
.end method

.method public setSubTextSpace(F)V
    .locals 0
    .param p1, "subTextSpace"    # F

    .prologue
    .line 685
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextSpace:F

    .line 686
    return-void
.end method

.method public setSubTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 464
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 465
    return-void
.end method

.method public setSubTextUse(Z)V
    .locals 0
    .param p1, "subTextUse"    # Z

    .prologue
    .line 695
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->subTextUse:Z

    .line 696
    return-void
.end method

.method public setTextSpace(F)V
    .locals 0
    .param p1, "textSpace"    # F

    .prologue
    .line 646
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textSpace:F

    .line 647
    return-void
.end method

.method public setTextStyleForWeekend1(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "textStyleForWeekend1"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 714
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend1:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 715
    return-void
.end method

.method public setTextStyleForWeekend2(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "textStyleForWeekend2"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 724
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->textStyleForWeekend2:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 725
    return-void
.end method

.method public setTitleText(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 473
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleText:Ljava/lang/String;

    .line 474
    return-void
.end method

.method public setTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 523
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 524
    return-void
.end method

.method public setTypeDirection(I)V
    .locals 0
    .param p1, "typeDirection"    # I

    .prologue
    .line 333
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->typeDirection:I

    .line 334
    return-void
.end method

.method public setTypeXY(I)V
    .locals 0
    .param p1, "typeXY"    # I

    .prologue
    .line 325
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->typeXY:I

    .line 326
    return-void
.end method

.method public setUseColorForWeekend(Z)V
    .locals 0
    .param p1, "useColorForWeekend"    # Z

    .prologue
    .line 734
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mUseColorForWeekend:Z

    .line 735
    return-void
.end method

.method public setUseManualMarking(Z)V
    .locals 0
    .param p1, "useManualMarking"    # Z

    .prologue
    .line 365
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->useManualMarking:Z

    .line 366
    return-void
.end method

.method public setVisibleAxisLine(Z)V
    .locals 0
    .param p1, "visibleAxisLine"    # Z

    .prologue
    .line 341
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisLine:Z

    .line 342
    return-void
.end method

.method public setVisibleAxisText(Z)V
    .locals 0
    .param p1, "visibleAxisText"    # Z

    .prologue
    .line 349
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleAxisText:Z

    .line 350
    return-void
.end method

.method public setVisibleIndex([I)V
    .locals 0
    .param p1, "visibleIndex"    # [I

    .prologue
    .line 381
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleIndex:[I

    .line 382
    return-void
.end method

.method public setVisibleIndexCount(I)V
    .locals 0
    .param p1, "visibleIndexCount"    # I

    .prologue
    .line 373
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleIndexCount:I

    .line 374
    return-void
.end method

.method public setVisibleMarking(Z)V
    .locals 0
    .param p1, "visibleMarking"    # Z

    .prologue
    .line 357
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->visibleMarking:Z

    .line 358
    return-void
.end method

.method public setXAxisLabelAlign(I)V
    .locals 0
    .param p1, "align"    # I

    .prologue
    .line 533
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisLabelAlign:I

    .line 534
    return-void
.end method

.method public setXAxisTitleAlign(I)V
    .locals 0
    .param p1, "xAxisTitleAlign"    # I

    .prologue
    .line 483
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mXAxisTitleAlign:I

    .line 484
    return-void
.end method

.method public setYAxisLabelAlign(I)V
    .locals 0
    .param p1, "align"    # I

    .prologue
    .line 543
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisLabelAlign:I

    .line 544
    return-void
.end method

.method public setYAxisMaxRoundDigit(I)V
    .locals 0
    .param p1, "digit"    # I

    .prologue
    .line 773
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundDigit:I

    .line 774
    return-void
.end method

.method public setYAxisMaxRoundMagnification(F)V
    .locals 0
    .param p1, "magnification"    # F

    .prologue
    .line 783
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRoundMagnification:F

    .line 784
    return-void
.end method

.method public setYAxisMaxRounding(I)V
    .locals 0
    .param p1, "rounding"    # I

    .prologue
    .line 763
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMaxRounding:I

    .line 764
    return-void
.end method

.method public setYAxisMinRoundDigit(I)V
    .locals 0
    .param p1, "digit"    # I

    .prologue
    .line 802
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundDigit:I

    .line 803
    return-void
.end method

.method public setYAxisMinRoundMagnification(F)V
    .locals 0
    .param p1, "magnification"    # F

    .prologue
    .line 812
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRoundMagnification:F

    .line 813
    return-void
.end method

.method public setYAxisMinRounding(I)V
    .locals 0
    .param p1, "rounding"    # I

    .prologue
    .line 792
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisMinRounding:I

    .line 793
    return-void
.end method

.method public setYAxisReversed(Z)V
    .locals 0
    .param p1, "reversed"    # Z

    .prologue
    .line 821
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisReversed:Z

    .line 822
    return-void
.end method

.method public setYAxisTitleAlign(I)V
    .locals 0
    .param p1, "yAxisTitleAlign"    # I

    .prologue
    .line 493
    iput p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYAxisTitleAlign:I

    .line 494
    return-void
.end method

.method public setYLabelOffsets([F)V
    .locals 1
    .param p1, "mYLabelOffsets"    # [F

    .prologue
    .line 551
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYLabelOffsets:[F

    .line 552
    array-length v0, p1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/AxisProperty;->mYlabelOffsetsCount:I

    .line 553
    return-void
.end method
