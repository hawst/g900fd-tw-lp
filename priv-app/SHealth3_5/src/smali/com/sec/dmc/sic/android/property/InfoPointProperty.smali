.class public Lcom/sec/dmc/sic/android/property/InfoPointProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "InfoPointProperty.java"


# static fields
.field public static final INFOPOINT_SHAPE_CIRCLE:I = 0x2

.field public static final INFOPOINT_SHAPE_RECT:I = 0x0

.field public static final INFOPOINT_SHAPE_ROUNDRECT:I = 0x1


# instance fields
.field private fillAlpha:F

.field private fillColorB:F

.field private fillColorG:F

.field private fillColorR:F

.field private height:F

.field private image:Landroid/graphics/Bitmap;

.field private radius:F

.field private shape:I

.field private strokeAlpha:F

.field private strokeColorB:F

.field private strokeColorG:F

.field private strokeColorR:F

.field private strokeWidth:F

.field private textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field private useInfoPoint:Z

.field private visibleText:Z

.field private width:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 76
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->shape:I

    .line 80
    iput v1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorR:F

    .line 81
    iput v1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorG:F

    .line 82
    iput v1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorB:F

    .line 83
    iput v1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillAlpha:F

    .line 85
    iput v2, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorR:F

    .line 86
    iput v2, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorG:F

    .line 87
    iput v2, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorB:F

    .line 88
    iput v1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeAlpha:F

    .line 90
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeWidth:F

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->image:Landroid/graphics/Bitmap;

    .line 93
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->useInfoPoint:Z

    .line 96
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->visibleText:Z

    .line 97
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 98
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 286
    if-ne p0, p1, :cond_1

    .line 342
    :cond_0
    :goto_0
    return v1

    .line 288
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 289
    goto :goto_0

    .line 290
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 291
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 292
    check-cast v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    .line 293
    .local v0, "other":Lcom/sec/dmc/sic/android/property/InfoPointProperty;
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 294
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 293
    if-eq v3, v4, :cond_4

    move v1, v2

    .line 295
    goto :goto_0

    .line 296
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 297
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 296
    if-eq v3, v4, :cond_5

    move v1, v2

    .line 298
    goto :goto_0

    .line 299
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 300
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 299
    if-eq v3, v4, :cond_6

    move v1, v2

    .line 301
    goto :goto_0

    .line 302
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 303
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 302
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 304
    goto :goto_0

    .line 305
    :cond_7
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->height:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->height:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 306
    goto :goto_0

    .line 307
    :cond_8
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->image:Landroid/graphics/Bitmap;

    if-nez v3, :cond_9

    .line 308
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->image:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_a

    move v1, v2

    .line 309
    goto :goto_0

    .line 310
    :cond_9
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->image:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 311
    goto :goto_0

    .line 312
    :cond_a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->radius:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->radius:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 313
    goto/16 :goto_0

    .line 314
    :cond_b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->shape:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->shape:I

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 315
    goto/16 :goto_0

    .line 316
    :cond_c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 317
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 316
    if-eq v3, v4, :cond_d

    move v1, v2

    .line 318
    goto/16 :goto_0

    .line 319
    :cond_d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 320
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 319
    if-eq v3, v4, :cond_e

    move v1, v2

    .line 321
    goto/16 :goto_0

    .line 322
    :cond_e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 323
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 322
    if-eq v3, v4, :cond_f

    move v1, v2

    .line 324
    goto/16 :goto_0

    .line 325
    :cond_f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 326
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 325
    if-eq v3, v4, :cond_10

    move v1, v2

    .line 327
    goto/16 :goto_0

    .line 328
    :cond_10
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 329
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 328
    if-eq v3, v4, :cond_11

    move v1, v2

    .line 330
    goto/16 :goto_0

    .line 331
    :cond_11
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_12

    .line 332
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_13

    move v1, v2

    .line 333
    goto/16 :goto_0

    .line 334
    :cond_12
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    move v1, v2

    .line 335
    goto/16 :goto_0

    .line 336
    :cond_13
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->useInfoPoint:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->useInfoPoint:Z

    if-eq v3, v4, :cond_14

    move v1, v2

    .line 337
    goto/16 :goto_0

    .line 338
    :cond_14
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->visibleText:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->visibleText:Z

    if-eq v3, v4, :cond_15

    move v1, v2

    .line 339
    goto/16 :goto_0

    .line 340
    :cond_15
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->width:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->width:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 341
    goto/16 :goto_0
.end method

.method public getFillAlpha()F
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillAlpha:F

    return v0
.end method

.method public getFillColorB()F
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorB:F

    return v0
.end method

.method public getFillColorG()F
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorG:F

    return v0
.end method

.method public getFillColorR()F
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorR:F

    return v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->height:F

    return v0
.end method

.method public getImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->image:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getRadius()F
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->radius:F

    return v0
.end method

.method public getShape()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->shape:I

    return v0
.end method

.method public getStrokeAlpha()F
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeAlpha:F

    return v0
.end method

.method public getStrokeColorB()F
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorB:F

    return v0
.end method

.method public getStrokeColorG()F
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorG:F

    return v0
.end method

.method public getStrokeColorR()F
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorR:F

    return v0
.end method

.method public getStrokeWidth()F
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeWidth:F

    return v0
.end method

.method public getTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getUseInfoPoint()Z
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->useInfoPoint:Z

    return v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->width:F

    return v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v5, 0x4d5

    const/16 v4, 0x4cf

    const/4 v3, 0x0

    .line 261
    const/16 v0, 0x1f

    .line 262
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 263
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 264
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 265
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 266
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 267
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->height:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 268
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->image:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 269
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->radius:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 270
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->shape:I

    add-int v1, v2, v6

    .line 271
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 272
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 273
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 274
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 275
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeWidth:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 276
    mul-int/lit8 v2, v1, 0x1f

    .line 277
    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v6, :cond_1

    .line 276
    :goto_1
    add-int v1, v2, v3

    .line 278
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->useInfoPoint:Z

    if-eqz v2, :cond_2

    move v2, v4

    :goto_2
    add-int v1, v3, v2

    .line 279
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->visibleText:Z

    if-eqz v3, :cond_3

    :goto_3
    add-int v1, v2, v4

    .line 280
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->width:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 281
    return v1

    .line 268
    :cond_0
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    .line 277
    :cond_1
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v3

    goto :goto_1

    :cond_2
    move v2, v5

    .line 278
    goto :goto_2

    :cond_3
    move v4, v5

    .line 279
    goto :goto_3
.end method

.method public isVisibleText()Z
    .locals 1

    .prologue
    .line 244
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->visibleText:Z

    return v0
.end method

.method public setFillAlpha(F)V
    .locals 0
    .param p1, "fillAlpha"    # F

    .prologue
    .line 152
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillAlpha:F

    .line 153
    return-void
.end method

.method public setFillColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 156
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorR:F

    .line 157
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorG:F

    .line 158
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorB:F

    .line 159
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillAlpha:F

    .line 161
    return-void
.end method

.method public setFillColorB(F)V
    .locals 0
    .param p1, "fillColorB"    # F

    .prologue
    .line 144
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorB:F

    .line 145
    return-void
.end method

.method public setFillColorG(F)V
    .locals 0
    .param p1, "fillColorG"    # F

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorG:F

    .line 137
    return-void
.end method

.method public setFillColorR(F)V
    .locals 0
    .param p1, "fillColorR"    # F

    .prologue
    .line 128
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->fillColorR:F

    .line 129
    return-void
.end method

.method public setHeight(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 232
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->height:F

    .line 233
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->image:Landroid/graphics/Bitmap;

    .line 216
    return-void
.end method

.method public setRadius(F)V
    .locals 0
    .param p1, "radius"    # F

    .prologue
    .line 240
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->radius:F

    .line 241
    return-void
.end method

.method public setShape(I)V
    .locals 0
    .param p1, "shape"    # I

    .prologue
    .line 120
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->shape:I

    .line 121
    return-void
.end method

.method public setStrokeAlpha(F)V
    .locals 0
    .param p1, "strokeAlpha"    # F

    .prologue
    .line 200
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeAlpha:F

    .line 201
    return-void
.end method

.method public setStrokeColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 204
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorR:F

    .line 205
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorR:F

    .line 206
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorR:F

    .line 207
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeAlpha:F

    .line 208
    return-void
.end method

.method public setStrokeColorB(F)V
    .locals 0
    .param p1, "strokeColorB"    # F

    .prologue
    .line 192
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorB:F

    .line 193
    return-void
.end method

.method public setStrokeColorG(F)V
    .locals 0
    .param p1, "strokeColorG"    # F

    .prologue
    .line 184
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorG:F

    .line 185
    return-void
.end method

.method public setStrokeColorR(F)V
    .locals 0
    .param p1, "strokeColorR"    # F

    .prologue
    .line 176
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeColorR:F

    .line 177
    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 0
    .param p1, "strokeWidth"    # F

    .prologue
    .line 168
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->strokeWidth:F

    .line 169
    return-void
.end method

.method public setTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 113
    return-void
.end method

.method public setUseInfoPoint(Z)V
    .locals 0
    .param p1, "useInfoPoint"    # Z

    .prologue
    .line 256
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->useInfoPoint:Z

    .line 257
    return-void
.end method

.method public setVisibleText(Z)V
    .locals 0
    .param p1, "visibleText"    # Z

    .prologue
    .line 248
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->visibleText:Z

    .line 249
    return-void
.end method

.method public setWidth(F)V
    .locals 0
    .param p1, "width"    # F

    .prologue
    .line 224
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->width:F

    .line 225
    return-void
.end method
