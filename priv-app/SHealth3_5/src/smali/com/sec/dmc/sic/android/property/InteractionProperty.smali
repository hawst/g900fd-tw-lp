.class public Lcom/sec/dmc/sic/android/property/InteractionProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "InteractionProperty.java"


# instance fields
.field private mDataZoomEnable:Z

.field private mDataZoomInIntervalStep:I

.field private mDataZoomInLimitInterval:I

.field private mDataZoomInRate:F

.field private mDataZoomOutIntervalStep:I

.field private mDataZoomOutLimitInterval:I

.field private mDataZoomOutRate:F

.field private mInteractionEnable:Z

.field private mMaxCount:I

.field private mMinCount:I

.field private mPartialZoomEnable:Z

.field private mScaleZoomEnable:Z

.field private mScaleZoomInRate:F

.field private mScaleZoomOutRate:F

.field private mScrollEnable:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 28
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 29
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomEnable:Z

    .line 30
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomEnable:Z

    .line 31
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mPartialZoomEnable:Z

    .line 32
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScrollEnable:Z

    .line 33
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mInteractionEnable:Z

    .line 35
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomOutRate:F

    .line 36
    iput v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomInRate:F

    .line 38
    iput v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutRate:F

    .line 39
    iput v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInRate:F

    .line 41
    iput v1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMinCount:I

    .line 42
    iput v1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMaxCount:I

    .line 44
    iput v2, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutIntervalStep:I

    .line 45
    iput v2, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInIntervalStep:I

    .line 47
    iput v1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutLimitInterval:I

    .line 48
    iput v1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInLimitInterval:I

    .line 49
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 187
    if-ne p0, p1, :cond_1

    .line 228
    :cond_0
    :goto_0
    return v1

    .line 189
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 190
    goto :goto_0

    .line 191
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 192
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 193
    check-cast v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;

    .line 194
    .local v0, "other":Lcom/sec/dmc/sic/android/property/InteractionProperty;
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomEnable:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomEnable:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 195
    goto :goto_0

    .line 196
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInIntervalStep:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInIntervalStep:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 197
    goto :goto_0

    .line 198
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInLimitInterval:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInLimitInterval:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 199
    goto :goto_0

    .line 200
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInRate:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 201
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInRate:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 200
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 202
    goto :goto_0

    .line 203
    :cond_7
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutIntervalStep:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutIntervalStep:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 204
    goto :goto_0

    .line 205
    :cond_8
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutLimitInterval:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutLimitInterval:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 206
    goto :goto_0

    .line 207
    :cond_9
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutRate:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 208
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutRate:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 207
    if-eq v3, v4, :cond_a

    move v1, v2

    .line 209
    goto :goto_0

    .line 210
    :cond_a
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mInteractionEnable:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mInteractionEnable:Z

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 211
    goto :goto_0

    .line 212
    :cond_b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMaxCount:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMaxCount:I

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 213
    goto :goto_0

    .line 214
    :cond_c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMinCount:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMinCount:I

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 215
    goto :goto_0

    .line 216
    :cond_d
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mPartialZoomEnable:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mPartialZoomEnable:Z

    if-eq v3, v4, :cond_e

    move v1, v2

    .line 217
    goto :goto_0

    .line 218
    :cond_e
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomEnable:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomEnable:Z

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 219
    goto/16 :goto_0

    .line 220
    :cond_f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomInRate:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 221
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomInRate:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 220
    if-eq v3, v4, :cond_10

    move v1, v2

    .line 222
    goto/16 :goto_0

    .line 223
    :cond_10
    iget v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomOutRate:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 224
    iget v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomOutRate:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 223
    if-eq v3, v4, :cond_11

    move v1, v2

    .line 225
    goto/16 :goto_0

    .line 226
    :cond_11
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScrollEnable:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScrollEnable:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 227
    goto/16 :goto_0
.end method

.method public getDataZoomEnable()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomEnable:Z

    return v0
.end method

.method public getDataZoomInIntervalStep()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInIntervalStep:I

    return v0
.end method

.method public getDataZoomInLimitInterval()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInLimitInterval:I

    return v0
.end method

.method public getDataZoomInRate()F
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInRate:F

    return v0
.end method

.method public getDataZoomOutLimitInterval()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutLimitInterval:I

    return v0
.end method

.method public getDataZoomOutRate()F
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutRate:F

    return v0
.end method

.method public getInteractionEnable()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mInteractionEnable:Z

    return v0
.end method

.method public getMaxCount()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMaxCount:I

    return v0
.end method

.method public getMinCount()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMinCount:I

    return v0
.end method

.method public getPartialZoomEnable()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mPartialZoomEnable:Z

    return v0
.end method

.method public getScaleZoomEnable()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomEnable:Z

    return v0
.end method

.method public getScaleZoomInRate()F
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomInRate:F

    return v0
.end method

.method public getScrollEnable()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScrollEnable:Z

    return v0
.end method

.method public getmDataZoomOutIntervalStep()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutIntervalStep:I

    return v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    .line 165
    const/16 v0, 0x1f

    .line 166
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 167
    .local v1, "result":I
    mul-int/lit8 v5, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomEnable:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v5, v2

    .line 168
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInIntervalStep:I

    add-int v1, v2, v5

    .line 169
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInLimitInterval:I

    add-int v1, v2, v5

    .line 170
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInRate:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 171
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutIntervalStep:I

    add-int v1, v2, v5

    .line 172
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutLimitInterval:I

    add-int v1, v2, v5

    .line 173
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutRate:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 174
    mul-int/lit8 v5, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mInteractionEnable:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v5, v2

    .line 175
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMaxCount:I

    add-int v1, v2, v5

    .line 176
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMinCount:I

    add-int v1, v2, v5

    .line 177
    mul-int/lit8 v5, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mPartialZoomEnable:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v5, v2

    .line 178
    mul-int/lit8 v5, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomEnable:Z

    if-eqz v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v5, v2

    .line 179
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomInRate:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 180
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomOutRate:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 181
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v5, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScrollEnable:Z

    if-eqz v5, :cond_4

    :goto_4
    add-int v1, v2, v3

    .line 182
    return v1

    :cond_0
    move v2, v4

    .line 167
    goto :goto_0

    :cond_1
    move v2, v4

    .line 174
    goto :goto_1

    :cond_2
    move v2, v4

    .line 177
    goto :goto_2

    :cond_3
    move v2, v4

    .line 178
    goto :goto_3

    :cond_4
    move v3, v4

    .line 181
    goto :goto_4
.end method

.method public setDataZoomEnable(Z)V
    .locals 0
    .param p1, "dataZoomEnable"    # Z

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomEnable:Z

    .line 81
    return-void
.end method

.method public setDataZoomInIntervalStep(I)V
    .locals 0
    .param p1, "dataZoomInIntervalStep"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInIntervalStep:I

    .line 145
    return-void
.end method

.method public setDataZoomInLimitInterval(I)V
    .locals 0
    .param p1, "dataZoomInLimitInterval"    # I

    .prologue
    .line 160
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInLimitInterval:I

    .line 161
    return-void
.end method

.method public setDataZoomInRate(F)V
    .locals 0
    .param p1, "dataZoomInRate"    # F

    .prologue
    .line 112
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomInRate:F

    .line 113
    return-void
.end method

.method public setDataZoomOutIntervalStep(I)V
    .locals 0
    .param p1, "dataZoomOutIntervalStep"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutIntervalStep:I

    .line 137
    return-void
.end method

.method public setDataZoomOutLimitInterval(I)V
    .locals 0
    .param p1, "dataZoomOutLimitInterval"    # I

    .prologue
    .line 152
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutLimitInterval:I

    .line 153
    return-void
.end method

.method public setDataZoomOutRate(F)V
    .locals 0
    .param p1, "dataZoomOutRate"    # F

    .prologue
    .line 104
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mDataZoomOutRate:F

    .line 105
    return-void
.end method

.method public setInteractionEnable(Z)V
    .locals 0
    .param p1, "interactionEnable"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mInteractionEnable:Z

    .line 57
    return-void
.end method

.method public setMaxCount(I)V
    .locals 0
    .param p1, "maxCount"    # I

    .prologue
    .line 128
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMaxCount:I

    .line 129
    return-void
.end method

.method public setMinCount(I)V
    .locals 0
    .param p1, "minCount"    # I

    .prologue
    .line 120
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mMinCount:I

    .line 121
    return-void
.end method

.method public setPartialZoomEnable(Z)V
    .locals 0
    .param p1, "partialZoomEnable"    # Z

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mPartialZoomEnable:Z

    .line 89
    return-void
.end method

.method public setScaleZoomEnable(Z)V
    .locals 0
    .param p1, "scaleZoomEnable"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomEnable:Z

    .line 73
    return-void
.end method

.method public setScaleZoomInRate(F)V
    .locals 0
    .param p1, "scaleZoomInRate"    # F

    .prologue
    .line 96
    iput p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScaleZoomInRate:F

    .line 97
    return-void
.end method

.method public setScrollEnable(Z)V
    .locals 0
    .param p1, "scrollEnable"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/InteractionProperty;->mScrollEnable:Z

    .line 65
    return-void
.end method
