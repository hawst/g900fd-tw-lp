.class public Lcom/sec/dmc/sic/android/property/HandlerProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "HandlerProperty.java"


# static fields
.field public static final HANDLER_ITEM_CIRCLE:I = 0x2

.field public static final HANDLER_ITEM_DIRECTION_BOTTOM:I = 0x1

.field public static final HANDLER_ITEM_DIRECTION_TOP:I = 0x0

.field public static final HANDLER_ITEM_RECT:I = 0x0

.field public static final HANDLER_ITEM_ROUNDRECT:I = 0x1

.field public static final HANDLER_LINE_DOTTED:I = 0x1

.field public static final HANDLER_LINE_NORMAL:I


# instance fields
.field animationTime:J

.field animationUse:Z

.field fadeIn:Z

.field fadeOut:Z

.field index:I

.field infoEnable:Z

.field infoFadeIn:Z

.field infoFadeOut:Z

.field infoFillAlpha:F

.field infoFillColorB:F

.field infoFillColorG:F

.field infoFillColorR:F

.field infoImage:Landroid/graphics/Bitmap;

.field infoStrokeAlpha:F

.field infoStrokeColorB:F

.field infoStrokeColorG:F

.field infoStrokeColorR:F

.field infoStrokeWidth:F

.field infoTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field infoVisible:Z

.field infoXYCoordinateUpdatable:Z

.field itemDirection:I

.field itemEnable:Z

.field itemFadeIn:Z

.field itemFadeOut:Z

.field itemFillAlpha:F

.field itemFillColorB:F

.field itemFillColorG:F

.field itemFillColorR:F

.field itemFocusImage:Landroid/graphics/Bitmap;

.field itemHeight:F

.field itemImage:Landroid/graphics/Bitmap;

.field itemOffset:F

.field itemShape:I

.field itemStrokeAlpha:F

.field itemStrokeColorB:F

.field itemStrokeColorG:F

.field itemStrokeColorR:F

.field itemStrokeWidth:F

.field itemTextOffset:F

.field itemTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field itemTextVisible:Z

.field itemVisible:Z

.field itemWidth:F

.field lineAlpha:F

.field lineColorB:F

.field lineColorG:F

.field lineColorR:F

.field lineEnable:Z

.field lineFadeIn:Z

.field lineFadeOut:Z

.field lineFocusImage:Landroid/graphics/Bitmap;

.field lineImage:Landroid/graphics/Bitmap;

.field lineType:I

.field lineVisible:Z

.field lineWidth:F

.field listener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

.field mDateFormat:Ljava/lang/String;

.field timeOutDelay:J


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 127
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 130
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->mDateFormat:Ljava/lang/String;

    .line 131
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemVisible:Z

    .line 132
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemEnable:Z

    .line 134
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineVisible:Z

    .line 135
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineEnable:Z

    .line 137
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoVisible:Z

    .line 138
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoEnable:Z

    .line 139
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoXYCoordinateUpdatable:Z

    .line 141
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextVisible:Z

    .line 143
    iput v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->index:I

    .line 146
    iput v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemShape:I

    .line 147
    iput v4, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemDirection:I

    .line 148
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemWidth:F

    .line 149
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemHeight:F

    .line 150
    iput v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemOffset:F

    .line 151
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextOffset:F

    .line 152
    iput v1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorR:F

    .line 153
    iput v1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorG:F

    .line 154
    iput v1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorB:F

    .line 155
    iput v1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillAlpha:F

    .line 157
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeWidth:F

    .line 159
    iput v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorR:F

    .line 160
    iput v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorG:F

    .line 161
    iput v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorB:F

    .line 162
    iput v1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeAlpha:F

    .line 164
    iput-object v5, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemImage:Landroid/graphics/Bitmap;

    .line 165
    iput-object v5, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFocusImage:Landroid/graphics/Bitmap;

    .line 167
    iput v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineType:I

    .line 169
    iput v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorR:F

    .line 170
    iput v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorG:F

    .line 171
    iput v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorB:F

    .line 172
    iput v1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineAlpha:F

    .line 174
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineWidth:F

    .line 176
    iput-object v5, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineImage:Landroid/graphics/Bitmap;

    .line 177
    iput-object v5, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFocusImage:Landroid/graphics/Bitmap;

    .line 179
    iput v1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorR:F

    .line 180
    iput v1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorG:F

    .line 181
    iput v1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorB:F

    .line 182
    iput v1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillAlpha:F

    .line 184
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeWidth:F

    .line 186
    iput v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorR:F

    .line 187
    iput v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorG:F

    .line 188
    iput v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorB:F

    .line 189
    iput v1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeAlpha:F

    .line 191
    iput-object v5, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoImage:Landroid/graphics/Bitmap;

    .line 193
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 194
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 196
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->timeOutDelay:J

    .line 198
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationUse:Z

    .line 199
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationTime:J

    .line 201
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeIn:Z

    .line 202
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeOut:Z

    .line 203
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeIn:Z

    .line 204
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeOut:Z

    .line 205
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeIn:Z

    .line 206
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeOut:Z

    .line 207
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeIn:Z

    .line 208
    iput-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeOut:Z

    .line 209
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 757
    if-ne p0, p1, :cond_1

    .line 936
    :cond_0
    :goto_0
    return v1

    .line 759
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 760
    goto :goto_0

    .line 761
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 762
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 763
    check-cast v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;

    .line 764
    .local v0, "other":Lcom/sec/dmc/sic/android/property/HandlerProperty;
    iget-wide v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationTime:J

    iget-wide v5, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationTime:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    move v1, v2

    .line 765
    goto :goto_0

    .line 766
    :cond_4
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationUse:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationUse:Z

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 767
    goto :goto_0

    .line 768
    :cond_5
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeIn:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeIn:Z

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 769
    goto :goto_0

    .line 770
    :cond_6
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeOut:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeOut:Z

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 771
    goto :goto_0

    .line 772
    :cond_7
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->index:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->index:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 773
    goto :goto_0

    .line 774
    :cond_8
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoEnable:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoEnable:Z

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 775
    goto :goto_0

    .line 776
    :cond_9
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeIn:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeIn:Z

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 777
    goto :goto_0

    .line 778
    :cond_a
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeOut:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeOut:Z

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 779
    goto :goto_0

    .line 780
    :cond_b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 781
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 780
    if-eq v3, v4, :cond_c

    move v1, v2

    .line 782
    goto :goto_0

    .line 783
    :cond_c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 784
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 783
    if-eq v3, v4, :cond_d

    move v1, v2

    .line 785
    goto :goto_0

    .line 786
    :cond_d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 787
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 786
    if-eq v3, v4, :cond_e

    move v1, v2

    .line 788
    goto/16 :goto_0

    .line 789
    :cond_e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 790
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 789
    if-eq v3, v4, :cond_f

    move v1, v2

    .line 791
    goto/16 :goto_0

    .line 792
    :cond_f
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoImage:Landroid/graphics/Bitmap;

    if-nez v3, :cond_10

    .line 793
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoImage:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_11

    move v1, v2

    .line 794
    goto/16 :goto_0

    .line 795
    :cond_10
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoImage:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    move v1, v2

    .line 796
    goto/16 :goto_0

    .line 797
    :cond_11
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 798
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 797
    if-eq v3, v4, :cond_12

    move v1, v2

    .line 799
    goto/16 :goto_0

    .line 800
    :cond_12
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 801
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 800
    if-eq v3, v4, :cond_13

    move v1, v2

    .line 802
    goto/16 :goto_0

    .line 803
    :cond_13
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 804
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 803
    if-eq v3, v4, :cond_14

    move v1, v2

    .line 805
    goto/16 :goto_0

    .line 806
    :cond_14
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 807
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 806
    if-eq v3, v4, :cond_15

    move v1, v2

    .line 808
    goto/16 :goto_0

    .line 809
    :cond_15
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 810
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 809
    if-eq v3, v4, :cond_16

    move v1, v2

    .line 811
    goto/16 :goto_0

    .line 812
    :cond_16
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_17

    .line 813
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_18

    move v1, v2

    .line 814
    goto/16 :goto_0

    .line 815
    :cond_17
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    move v1, v2

    .line 816
    goto/16 :goto_0

    .line 817
    :cond_18
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoVisible:Z

    if-eq v3, v4, :cond_19

    move v1, v2

    .line 818
    goto/16 :goto_0

    .line 819
    :cond_19
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoXYCoordinateUpdatable:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoXYCoordinateUpdatable:Z

    if-eq v3, v4, :cond_1a

    move v1, v2

    .line 820
    goto/16 :goto_0

    .line 821
    :cond_1a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemDirection:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemDirection:I

    if-eq v3, v4, :cond_1b

    move v1, v2

    .line 822
    goto/16 :goto_0

    .line 823
    :cond_1b
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemEnable:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemEnable:Z

    if-eq v3, v4, :cond_1c

    move v1, v2

    .line 824
    goto/16 :goto_0

    .line 825
    :cond_1c
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeIn:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeIn:Z

    if-eq v3, v4, :cond_1d

    move v1, v2

    .line 826
    goto/16 :goto_0

    .line 827
    :cond_1d
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeOut:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeOut:Z

    if-eq v3, v4, :cond_1e

    move v1, v2

    .line 828
    goto/16 :goto_0

    .line 829
    :cond_1e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 830
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 829
    if-eq v3, v4, :cond_1f

    move v1, v2

    .line 831
    goto/16 :goto_0

    .line 832
    :cond_1f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 833
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 832
    if-eq v3, v4, :cond_20

    move v1, v2

    .line 834
    goto/16 :goto_0

    .line 835
    :cond_20
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 836
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 835
    if-eq v3, v4, :cond_21

    move v1, v2

    .line 837
    goto/16 :goto_0

    .line 838
    :cond_21
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 839
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 838
    if-eq v3, v4, :cond_22

    move v1, v2

    .line 840
    goto/16 :goto_0

    .line 841
    :cond_22
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFocusImage:Landroid/graphics/Bitmap;

    if-nez v3, :cond_23

    .line 842
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFocusImage:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_24

    move v1, v2

    .line 843
    goto/16 :goto_0

    .line 844
    :cond_23
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFocusImage:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFocusImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_24

    move v1, v2

    .line 845
    goto/16 :goto_0

    .line 846
    :cond_24
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemHeight:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 847
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemHeight:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 846
    if-eq v3, v4, :cond_25

    move v1, v2

    .line 848
    goto/16 :goto_0

    .line 849
    :cond_25
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemImage:Landroid/graphics/Bitmap;

    if-nez v3, :cond_26

    .line 850
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemImage:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_27

    move v1, v2

    .line 851
    goto/16 :goto_0

    .line 852
    :cond_26
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemImage:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_27

    move v1, v2

    .line 853
    goto/16 :goto_0

    .line 854
    :cond_27
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemOffset:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 855
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemOffset:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 854
    if-eq v3, v4, :cond_28

    move v1, v2

    .line 856
    goto/16 :goto_0

    .line 857
    :cond_28
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemShape:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemShape:I

    if-eq v3, v4, :cond_29

    move v1, v2

    .line 858
    goto/16 :goto_0

    .line 859
    :cond_29
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 860
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 859
    if-eq v3, v4, :cond_2a

    move v1, v2

    .line 861
    goto/16 :goto_0

    .line 862
    :cond_2a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 863
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 862
    if-eq v3, v4, :cond_2b

    move v1, v2

    .line 864
    goto/16 :goto_0

    .line 865
    :cond_2b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 866
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 865
    if-eq v3, v4, :cond_2c

    move v1, v2

    .line 867
    goto/16 :goto_0

    .line 868
    :cond_2c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 869
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 868
    if-eq v3, v4, :cond_2d

    move v1, v2

    .line 870
    goto/16 :goto_0

    .line 871
    :cond_2d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 872
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 871
    if-eq v3, v4, :cond_2e

    move v1, v2

    .line 873
    goto/16 :goto_0

    .line 874
    :cond_2e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextOffset:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 875
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextOffset:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 874
    if-eq v3, v4, :cond_2f

    move v1, v2

    .line 876
    goto/16 :goto_0

    .line 877
    :cond_2f
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_30

    .line 878
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_31

    move v1, v2

    .line 879
    goto/16 :goto_0

    .line 880
    :cond_30
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_31

    move v1, v2

    .line 881
    goto/16 :goto_0

    .line 882
    :cond_31
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextVisible:Z

    if-eq v3, v4, :cond_32

    move v1, v2

    .line 883
    goto/16 :goto_0

    .line 884
    :cond_32
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemVisible:Z

    if-eq v3, v4, :cond_33

    move v1, v2

    .line 885
    goto/16 :goto_0

    .line 886
    :cond_33
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 887
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 886
    if-eq v3, v4, :cond_34

    move v1, v2

    .line 888
    goto/16 :goto_0

    .line 889
    :cond_34
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 890
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 889
    if-eq v3, v4, :cond_35

    move v1, v2

    .line 891
    goto/16 :goto_0

    .line 892
    :cond_35
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 893
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 892
    if-eq v3, v4, :cond_36

    move v1, v2

    .line 894
    goto/16 :goto_0

    .line 895
    :cond_36
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 896
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 895
    if-eq v3, v4, :cond_37

    move v1, v2

    .line 897
    goto/16 :goto_0

    .line 898
    :cond_37
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 899
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 898
    if-eq v3, v4, :cond_38

    move v1, v2

    .line 900
    goto/16 :goto_0

    .line 901
    :cond_38
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineEnable:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineEnable:Z

    if-eq v3, v4, :cond_39

    move v1, v2

    .line 902
    goto/16 :goto_0

    .line 903
    :cond_39
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeIn:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeIn:Z

    if-eq v3, v4, :cond_3a

    move v1, v2

    .line 904
    goto/16 :goto_0

    .line 905
    :cond_3a
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeOut:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeOut:Z

    if-eq v3, v4, :cond_3b

    move v1, v2

    .line 906
    goto/16 :goto_0

    .line 907
    :cond_3b
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFocusImage:Landroid/graphics/Bitmap;

    if-nez v3, :cond_3c

    .line 908
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFocusImage:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3d

    move v1, v2

    .line 909
    goto/16 :goto_0

    .line 910
    :cond_3c
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFocusImage:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFocusImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3d

    move v1, v2

    .line 911
    goto/16 :goto_0

    .line 912
    :cond_3d
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineImage:Landroid/graphics/Bitmap;

    if-nez v3, :cond_3e

    .line 913
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineImage:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3f

    move v1, v2

    .line 914
    goto/16 :goto_0

    .line 915
    :cond_3e
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineImage:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3f

    move v1, v2

    .line 916
    goto/16 :goto_0

    .line 917
    :cond_3f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineType:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineType:I

    if-eq v3, v4, :cond_40

    move v1, v2

    .line 918
    goto/16 :goto_0

    .line 919
    :cond_40
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineVisible:Z

    if-eq v3, v4, :cond_41

    move v1, v2

    .line 920
    goto/16 :goto_0

    .line 921
    :cond_41
    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 922
    iget v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 921
    if-eq v3, v4, :cond_42

    move v1, v2

    .line 923
    goto/16 :goto_0

    .line 924
    :cond_42
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    if-nez v3, :cond_43

    .line 925
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    if-eqz v3, :cond_44

    move v1, v2

    .line 926
    goto/16 :goto_0

    .line 927
    :cond_43
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_44

    move v1, v2

    .line 928
    goto/16 :goto_0

    .line 929
    :cond_44
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->mDateFormat:Ljava/lang/String;

    if-nez v3, :cond_45

    .line 930
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->mDateFormat:Ljava/lang/String;

    if-eqz v3, :cond_46

    move v1, v2

    .line 931
    goto/16 :goto_0

    .line 932
    :cond_45
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->mDateFormat:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->mDateFormat:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_46

    move v1, v2

    .line 933
    goto/16 :goto_0

    .line 934
    :cond_46
    iget-wide v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->timeOutDelay:J

    iget-wide v5, v0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->timeOutDelay:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    move v1, v2

    .line 935
    goto/16 :goto_0
.end method

.method public getAnimationTime()J
    .locals 2

    .prologue
    .line 662
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationTime:J

    return-wide v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->index:I

    return v0
.end method

.method public getInfoFillAlpha()F
    .locals 1

    .prologue
    .line 474
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillAlpha:F

    return v0
.end method

.method public getInfoFillColorB()F
    .locals 1

    .prologue
    .line 468
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorB:F

    return v0
.end method

.method public getInfoFillColorG()F
    .locals 1

    .prologue
    .line 462
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorG:F

    return v0
.end method

.method public getInfoFillColorR()F
    .locals 1

    .prologue
    .line 456
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorR:F

    return v0
.end method

.method public getInfoImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getInfoStrokeAlpha()F
    .locals 1

    .prologue
    .line 510
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeAlpha:F

    return v0
.end method

.method public getInfoStrokeColorB()F
    .locals 1

    .prologue
    .line 504
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorB:F

    return v0
.end method

.method public getInfoStrokeColorG()F
    .locals 1

    .prologue
    .line 498
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorG:F

    return v0
.end method

.method public getInfoStrokeColorR()F
    .locals 1

    .prologue
    .line 492
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorR:F

    return v0
.end method

.method public getInfoStrokeWidth()F
    .locals 1

    .prologue
    .line 486
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeWidth:F

    return v0
.end method

.method public getInfoTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getItemDateFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->mDateFormat:Ljava/lang/String;

    return-object v0
.end method

.method public getItemDirection()I
    .locals 1

    .prologue
    .line 308
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemDirection:I

    return v0
.end method

.method public getItemFillAlpha()F
    .locals 1

    .prologue
    .line 334
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillAlpha:F

    return v0
.end method

.method public getItemFillColorB()F
    .locals 1

    .prologue
    .line 328
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorB:F

    return v0
.end method

.method public getItemFillColorG()F
    .locals 1

    .prologue
    .line 322
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorG:F

    return v0
.end method

.method public getItemFillColorR()F
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorR:F

    return v0
.end method

.method public getItemFocusImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFocusImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getItemHeight()F
    .locals 1

    .prologue
    .line 614
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemHeight:F

    return v0
.end method

.method public getItemImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getItemOffset()F
    .locals 1

    .prologue
    .line 622
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemOffset:F

    return v0
.end method

.method public getItemShape()I
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemShape:I

    return v0
.end method

.method public getItemStrokeAlpha()F
    .locals 1

    .prologue
    .line 370
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeAlpha:F

    return v0
.end method

.method public getItemStrokeColorB()F
    .locals 1

    .prologue
    .line 364
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorB:F

    return v0
.end method

.method public getItemStrokeColorG()F
    .locals 1

    .prologue
    .line 358
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorG:F

    return v0
.end method

.method public getItemStrokeColorR()F
    .locals 1

    .prologue
    .line 352
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorR:F

    return v0
.end method

.method public getItemStrokeWidth()F
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeWidth:F

    return v0
.end method

.method public getItemTextOffset()F
    .locals 1

    .prologue
    .line 638
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextOffset:F

    return v0
.end method

.method public getItemTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getItemWidth()F
    .locals 1

    .prologue
    .line 606
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemWidth:F

    return v0
.end method

.method public getLineAlpha()F
    .locals 1

    .prologue
    .line 421
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineAlpha:F

    return v0
.end method

.method public getLineColorB()F
    .locals 1

    .prologue
    .line 415
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorB:F

    return v0
.end method

.method public getLineColorG()F
    .locals 1

    .prologue
    .line 409
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorG:F

    return v0
.end method

.method public getLineColorR()F
    .locals 1

    .prologue
    .line 403
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorR:F

    return v0
.end method

.method public getLineFocusImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFocusImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getLineImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getLineType()I
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineType:I

    return v0
.end method

.method public getLineWidth()F
    .locals 1

    .prologue
    .line 433
    iget v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineWidth:F

    return v0
.end method

.method public getListener()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    return-object v0
.end method

.method public getTimeOutDelay()J
    .locals 2

    .prologue
    .line 646
    iget-wide v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->timeOutDelay:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const/16 v10, 0x20

    const/4 v5, 0x0

    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    .line 681
    const/16 v0, 0x1f

    .line 682
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 683
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    .line 684
    iget-wide v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationTime:J

    iget-wide v8, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationTime:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    .line 683
    add-int v1, v2, v6

    .line 685
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationUse:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 686
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeIn:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v6, v2

    .line 687
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeOut:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v6, v2

    .line 688
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->index:I

    add-int v1, v2, v6

    .line 689
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoEnable:Z

    if-eqz v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v6, v2

    .line 690
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeIn:Z

    if-eqz v2, :cond_4

    move v2, v3

    :goto_4
    add-int v1, v6, v2

    .line 691
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeOut:Z

    if-eqz v2, :cond_5

    move v2, v3

    :goto_5
    add-int v1, v6, v2

    .line 692
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 693
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 694
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 695
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 696
    mul-int/lit8 v6, v1, 0x1f

    .line 697
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_6

    move v2, v5

    .line 696
    :goto_6
    add-int v1, v6, v2

    .line 698
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 699
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 700
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 701
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 702
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeWidth:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 703
    mul-int/lit8 v6, v1, 0x1f

    .line 704
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_7

    move v2, v5

    .line 703
    :goto_7
    add-int v1, v6, v2

    .line 705
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoVisible:Z

    if-eqz v2, :cond_8

    move v2, v3

    :goto_8
    add-int v1, v6, v2

    .line 706
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoXYCoordinateUpdatable:Z

    if-eqz v2, :cond_9

    move v2, v3

    :goto_9
    add-int v1, v6, v2

    .line 707
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemDirection:I

    add-int v1, v2, v6

    .line 708
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemEnable:Z

    if-eqz v2, :cond_a

    move v2, v3

    :goto_a
    add-int v1, v6, v2

    .line 709
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeIn:Z

    if-eqz v2, :cond_b

    move v2, v3

    :goto_b
    add-int v1, v6, v2

    .line 710
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeOut:Z

    if-eqz v2, :cond_c

    move v2, v3

    :goto_c
    add-int v1, v6, v2

    .line 711
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 712
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 713
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 714
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 715
    mul-int/lit8 v6, v1, 0x1f

    .line 716
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFocusImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_d

    move v2, v5

    .line 715
    :goto_d
    add-int v1, v6, v2

    .line 717
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemHeight:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 718
    mul-int/lit8 v6, v1, 0x1f

    .line 719
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_e

    move v2, v5

    .line 718
    :goto_e
    add-int v1, v6, v2

    .line 720
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemOffset:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 721
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemShape:I

    add-int v1, v2, v6

    .line 722
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 723
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 724
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 725
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 726
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeWidth:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 727
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextOffset:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 728
    mul-int/lit8 v6, v1, 0x1f

    .line 729
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_f

    move v2, v5

    .line 728
    :goto_f
    add-int v1, v6, v2

    .line 730
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextVisible:Z

    if-eqz v2, :cond_10

    move v2, v3

    :goto_10
    add-int v1, v6, v2

    .line 731
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemVisible:Z

    if-eqz v2, :cond_11

    move v2, v3

    :goto_11
    add-int v1, v6, v2

    .line 732
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemWidth:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 733
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 734
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 735
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 736
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 737
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineEnable:Z

    if-eqz v2, :cond_12

    move v2, v3

    :goto_12
    add-int v1, v6, v2

    .line 738
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeIn:Z

    if-eqz v2, :cond_13

    move v2, v3

    :goto_13
    add-int v1, v6, v2

    .line 739
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeOut:Z

    if-eqz v2, :cond_14

    move v2, v3

    :goto_14
    add-int v1, v6, v2

    .line 740
    mul-int/lit8 v6, v1, 0x1f

    .line 741
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFocusImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_15

    move v2, v5

    .line 740
    :goto_15
    add-int v1, v6, v2

    .line 742
    mul-int/lit8 v6, v1, 0x1f

    .line 743
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_16

    move v2, v5

    .line 742
    :goto_16
    add-int v1, v6, v2

    .line 744
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineType:I

    add-int v1, v2, v6

    .line 745
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineVisible:Z

    if-eqz v6, :cond_17

    :goto_17
    add-int v1, v2, v3

    .line 746
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 747
    mul-int/lit8 v3, v1, 0x1f

    .line 748
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    if-nez v2, :cond_18

    move v2, v5

    .line 747
    :goto_18
    add-int v1, v3, v2

    .line 749
    mul-int/lit8 v2, v1, 0x1f

    .line 750
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->mDateFormat:Ljava/lang/String;

    if-nez v3, :cond_19

    .line 749
    :goto_19
    add-int v1, v2, v5

    .line 751
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->timeOutDelay:J

    iget-wide v5, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->timeOutDelay:J

    ushr-long/2addr v5, v10

    xor-long/2addr v3, v5

    long-to-int v3, v3

    add-int v1, v2, v3

    .line 752
    return v1

    :cond_0
    move v2, v4

    .line 685
    goto/16 :goto_0

    :cond_1
    move v2, v4

    .line 686
    goto/16 :goto_1

    :cond_2
    move v2, v4

    .line 687
    goto/16 :goto_2

    :cond_3
    move v2, v4

    .line 689
    goto/16 :goto_3

    :cond_4
    move v2, v4

    .line 690
    goto/16 :goto_4

    :cond_5
    move v2, v4

    .line 691
    goto/16 :goto_5

    .line 697
    :cond_6
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 704
    :cond_7
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto/16 :goto_7

    :cond_8
    move v2, v4

    .line 705
    goto/16 :goto_8

    :cond_9
    move v2, v4

    .line 706
    goto/16 :goto_9

    :cond_a
    move v2, v4

    .line 708
    goto/16 :goto_a

    :cond_b
    move v2, v4

    .line 709
    goto/16 :goto_b

    :cond_c
    move v2, v4

    .line 710
    goto/16 :goto_c

    .line 716
    :cond_d
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFocusImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_d

    .line 719
    :cond_e
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_e

    .line 729
    :cond_f
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto/16 :goto_f

    :cond_10
    move v2, v4

    .line 730
    goto/16 :goto_10

    :cond_11
    move v2, v4

    .line 731
    goto/16 :goto_11

    :cond_12
    move v2, v4

    .line 737
    goto/16 :goto_12

    :cond_13
    move v2, v4

    .line 738
    goto/16 :goto_13

    :cond_14
    move v2, v4

    .line 739
    goto/16 :goto_14

    .line 741
    :cond_15
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFocusImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_15

    .line 743
    :cond_16
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_16

    :cond_17
    move v3, v4

    .line 745
    goto/16 :goto_17

    .line 748
    :cond_18
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_18

    .line 750
    :cond_19
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->mDateFormat:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    goto/16 :goto_19
.end method

.method public isAnimationUse()Z
    .locals 1

    .prologue
    .line 654
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationUse:Z

    return v0
.end method

.method public isFadeIn()Z
    .locals 1

    .prologue
    .line 278
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeIn:Z

    return v0
.end method

.method public isFadeOut()Z
    .locals 1

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeOut:Z

    return v0
.end method

.method public isInfoEnable()Z
    .locals 1

    .prologue
    .line 582
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoEnable:Z

    return v0
.end method

.method public isInfoFadeIn()Z
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeIn:Z

    return v0
.end method

.method public isInfoFadeOut()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeOut:Z

    return v0
.end method

.method public isInfoVisible()Z
    .locals 1

    .prologue
    .line 574
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoVisible:Z

    return v0
.end method

.method public isInfoXYCoordinateUpdatable()Z
    .locals 1

    .prologue
    .line 590
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoXYCoordinateUpdatable:Z

    return v0
.end method

.method public isItemEnable()Z
    .locals 1

    .prologue
    .line 550
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemEnable:Z

    return v0
.end method

.method public isItemFadeIn()Z
    .locals 1

    .prologue
    .line 230
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeIn:Z

    return v0
.end method

.method public isItemFadeOut()Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeOut:Z

    return v0
.end method

.method public isItemTextVisible()Z
    .locals 1

    .prologue
    .line 630
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextVisible:Z

    return v0
.end method

.method public isItemVisible()Z
    .locals 1

    .prologue
    .line 542
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemVisible:Z

    return v0
.end method

.method public isLineEnable()Z
    .locals 1

    .prologue
    .line 566
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineEnable:Z

    return v0
.end method

.method public isLineFadeIn()Z
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeIn:Z

    return v0
.end method

.method public isLineFadeOut()Z
    .locals 1

    .prologue
    .line 254
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeOut:Z

    return v0
.end method

.method public isLineVisible()Z
    .locals 1

    .prologue
    .line 558
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineVisible:Z

    return v0
.end method

.method public resetHandlerFadeInOut()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 218
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeIn:Z

    .line 219
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeOut:Z

    .line 220
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeIn:Z

    .line 221
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeOut:Z

    .line 222
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeIn:Z

    .line 223
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeOut:Z

    .line 224
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeIn:Z

    .line 225
    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeOut:Z

    .line 226
    return-void
.end method

.method public setAnimationTime(J)V
    .locals 0
    .param p1, "animationTime"    # J

    .prologue
    .line 666
    iput-wide p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationTime:J

    .line 667
    return-void
.end method

.method public setAnimationUse(Z)V
    .locals 0
    .param p1, "animationUse"    # Z

    .prologue
    .line 658
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->animationUse:Z

    .line 659
    return-void
.end method

.method public setFadeIn(Z)V
    .locals 0
    .param p1, "fadeIn"    # Z

    .prologue
    .line 282
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeIn:Z

    .line 283
    return-void
.end method

.method public setFadeOut(Z)V
    .locals 0
    .param p1, "fadeOut"    # Z

    .prologue
    .line 290
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->fadeOut:Z

    .line 291
    return-void
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 297
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->index:I

    .line 298
    return-void
.end method

.method public setInfoEnable(Z)V
    .locals 0
    .param p1, "infoEnable"    # Z

    .prologue
    .line 586
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoEnable:Z

    .line 587
    return-void
.end method

.method public setInfoFadeIn(Z)V
    .locals 0
    .param p1, "infoFadeIn"    # Z

    .prologue
    .line 266
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeIn:Z

    .line 267
    return-void
.end method

.method public setInfoFadeOut(Z)V
    .locals 0
    .param p1, "infoFadeOut"    # Z

    .prologue
    .line 274
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFadeOut:Z

    .line 275
    return-void
.end method

.method public setInfoFillAlpha(F)V
    .locals 0
    .param p1, "infoFillAlpha"    # F

    .prologue
    .line 477
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillAlpha:F

    .line 478
    return-void
.end method

.method public setInfoFillColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 480
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorR:F

    .line 481
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorG:F

    .line 482
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorB:F

    .line 483
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillAlpha:F

    .line 484
    return-void
.end method

.method public setInfoFillColorB(F)V
    .locals 0
    .param p1, "infoFillColorB"    # F

    .prologue
    .line 471
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorB:F

    .line 472
    return-void
.end method

.method public setInfoFillColorG(F)V
    .locals 0
    .param p1, "infoFillColorG"    # F

    .prologue
    .line 465
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorG:F

    .line 466
    return-void
.end method

.method public setInfoFillColorR(F)V
    .locals 0
    .param p1, "infoFillColorR"    # F

    .prologue
    .line 459
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoFillColorR:F

    .line 460
    return-void
.end method

.method public setInfoImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "infoImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 526
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoImage:Landroid/graphics/Bitmap;

    .line 527
    return-void
.end method

.method public setInfoStrokeAlpha(F)V
    .locals 0
    .param p1, "infoStrokeAlpha"    # F

    .prologue
    .line 513
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeAlpha:F

    .line 514
    return-void
.end method

.method public setInfoStrokeColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 516
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorR:F

    .line 517
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorG:F

    .line 518
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorB:F

    .line 519
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeAlpha:F

    .line 520
    return-void
.end method

.method public setInfoStrokeColorB(F)V
    .locals 0
    .param p1, "infoStrokeColorB"    # F

    .prologue
    .line 507
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorB:F

    .line 508
    return-void
.end method

.method public setInfoStrokeColorG(F)V
    .locals 0
    .param p1, "infoStrokeColorG"    # F

    .prologue
    .line 501
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorG:F

    .line 502
    return-void
.end method

.method public setInfoStrokeColorR(F)V
    .locals 0
    .param p1, "infoStrokeColorR"    # F

    .prologue
    .line 495
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeColorR:F

    .line 496
    return-void
.end method

.method public setInfoStrokeWidth(F)V
    .locals 0
    .param p1, "infoStrokeWidth"    # F

    .prologue
    .line 489
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoStrokeWidth:F

    .line 490
    return-void
.end method

.method public setInfoTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "infoTextStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 538
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 539
    return-void
.end method

.method public setInfoVisible(Z)V
    .locals 0
    .param p1, "infoVisible"    # Z

    .prologue
    .line 578
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoVisible:Z

    .line 579
    return-void
.end method

.method public setInfoXYCoordinateUpdatable(Z)V
    .locals 0
    .param p1, "infoXYCoordinateUpdatable"    # Z

    .prologue
    .line 594
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->infoXYCoordinateUpdatable:Z

    .line 595
    return-void
.end method

.method public setItemDateFormat(Ljava/lang/String;)V
    .locals 0
    .param p1, "dateFormat"    # Ljava/lang/String;

    .prologue
    .line 675
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->mDateFormat:Ljava/lang/String;

    .line 677
    return-void
.end method

.method public setItemDirection(I)V
    .locals 0
    .param p1, "itemDirection"    # I

    .prologue
    .line 312
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemDirection:I

    .line 313
    return-void
.end method

.method public setItemEnable(Z)V
    .locals 0
    .param p1, "itemEnable"    # Z

    .prologue
    .line 554
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemEnable:Z

    .line 555
    return-void
.end method

.method public setItemFadeIn(Z)V
    .locals 0
    .param p1, "itemFadeIn"    # Z

    .prologue
    .line 234
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeIn:Z

    .line 235
    return-void
.end method

.method public setItemFadeOut(Z)V
    .locals 0
    .param p1, "itemFadeOut"    # Z

    .prologue
    .line 242
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFadeOut:Z

    .line 243
    return-void
.end method

.method public setItemFillAlpha(F)V
    .locals 0
    .param p1, "itemFillAlpha"    # F

    .prologue
    .line 337
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillAlpha:F

    .line 338
    return-void
.end method

.method public setItemFillColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 340
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorR:F

    .line 341
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorG:F

    .line 342
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorB:F

    .line 343
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillAlpha:F

    .line 344
    return-void
.end method

.method public setItemFillColorB(F)V
    .locals 0
    .param p1, "itemFillColorB"    # F

    .prologue
    .line 331
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorB:F

    .line 332
    return-void
.end method

.method public setItemFillColorG(F)V
    .locals 0
    .param p1, "itemFillColorG"    # F

    .prologue
    .line 325
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorG:F

    .line 326
    return-void
.end method

.method public setItemFillColorR(F)V
    .locals 0
    .param p1, "itemFillColorR"    # F

    .prologue
    .line 319
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFillColorR:F

    .line 320
    return-void
.end method

.method public setItemFocusImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "itemFocusImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 393
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemFocusImage:Landroid/graphics/Bitmap;

    .line 394
    return-void
.end method

.method public setItemHeight(F)V
    .locals 0
    .param p1, "itemHeight"    # F

    .prologue
    .line 618
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemHeight:F

    .line 619
    return-void
.end method

.method public setItemImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "itemImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 385
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemImage:Landroid/graphics/Bitmap;

    .line 386
    return-void
.end method

.method public setItemOffset(F)V
    .locals 0
    .param p1, "itemOffset"    # F

    .prologue
    .line 626
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemOffset:F

    .line 627
    return-void
.end method

.method public setItemShape(I)V
    .locals 0
    .param p1, "shape"    # I

    .prologue
    .line 304
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemShape:I

    .line 305
    return-void
.end method

.method public setItemStrokeAlpha(F)V
    .locals 0
    .param p1, "itemStrokeAlpha"    # F

    .prologue
    .line 373
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeAlpha:F

    .line 374
    return-void
.end method

.method public setItemStrokeColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 376
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorR:F

    .line 377
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorG:F

    .line 378
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorB:F

    .line 379
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeAlpha:F

    .line 380
    return-void
.end method

.method public setItemStrokeColorB(F)V
    .locals 0
    .param p1, "itemStrokeColorB"    # F

    .prologue
    .line 367
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorB:F

    .line 368
    return-void
.end method

.method public setItemStrokeColorG(F)V
    .locals 0
    .param p1, "itemStrokeColorG"    # F

    .prologue
    .line 361
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorG:F

    .line 362
    return-void
.end method

.method public setItemStrokeColorR(F)V
    .locals 0
    .param p1, "itemStrokeColorR"    # F

    .prologue
    .line 355
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeColorR:F

    .line 356
    return-void
.end method

.method public setItemStrokeWidth(F)V
    .locals 0
    .param p1, "itemStrokeWidth"    # F

    .prologue
    .line 349
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemStrokeWidth:F

    .line 350
    return-void
.end method

.method public setItemTextOffset(F)V
    .locals 0
    .param p1, "itemTextOffset"    # F

    .prologue
    .line 642
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextOffset:F

    .line 643
    return-void
.end method

.method public setItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "itemTextStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 532
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 533
    return-void
.end method

.method public setItemTextVisible(Z)V
    .locals 0
    .param p1, "itemTextVisible"    # Z

    .prologue
    .line 634
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemTextVisible:Z

    .line 635
    return-void
.end method

.method public setItemVisible(Z)V
    .locals 0
    .param p1, "itemVisible"    # Z

    .prologue
    .line 546
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemVisible:Z

    .line 547
    return-void
.end method

.method public setItemWidth(F)V
    .locals 0
    .param p1, "itemWidth"    # F

    .prologue
    .line 610
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->itemWidth:F

    .line 611
    return-void
.end method

.method public setLineAlpha(F)V
    .locals 0
    .param p1, "lineAlpha"    # F

    .prologue
    .line 424
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineAlpha:F

    .line 425
    return-void
.end method

.method public setLineColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 427
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorR:F

    .line 428
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorG:F

    .line 429
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorB:F

    .line 430
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineAlpha:F

    .line 431
    return-void
.end method

.method public setLineColorB(F)V
    .locals 0
    .param p1, "lineColorB"    # F

    .prologue
    .line 418
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorB:F

    .line 419
    return-void
.end method

.method public setLineColorG(F)V
    .locals 0
    .param p1, "lineColorG"    # F

    .prologue
    .line 412
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorG:F

    .line 413
    return-void
.end method

.method public setLineColorR(F)V
    .locals 0
    .param p1, "lineColorR"    # F

    .prologue
    .line 406
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineColorR:F

    .line 407
    return-void
.end method

.method public setLineEnable(Z)V
    .locals 0
    .param p1, "lineEnable"    # Z

    .prologue
    .line 570
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineEnable:Z

    .line 571
    return-void
.end method

.method public setLineFadeIn(Z)V
    .locals 0
    .param p1, "lineFadeIn"    # Z

    .prologue
    .line 250
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeIn:Z

    .line 251
    return-void
.end method

.method public setLineFadeOut(Z)V
    .locals 0
    .param p1, "lineFadeOut"    # Z

    .prologue
    .line 258
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFadeOut:Z

    .line 259
    return-void
.end method

.method public setLineFocusImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "lineFocusImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 452
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineFocusImage:Landroid/graphics/Bitmap;

    .line 453
    return-void
.end method

.method public setLineImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "lineImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 444
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineImage:Landroid/graphics/Bitmap;

    .line 445
    return-void
.end method

.method public setLineType(I)V
    .locals 0
    .param p1, "lineType"    # I

    .prologue
    .line 400
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineType:I

    .line 401
    return-void
.end method

.method public setLineVisible(Z)V
    .locals 0
    .param p1, "lineVisible"    # Z

    .prologue
    .line 562
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineVisible:Z

    .line 563
    return-void
.end method

.method public setLineWidth(F)V
    .locals 0
    .param p1, "lineWidth"    # F

    .prologue
    .line 436
    iput p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->lineWidth:F

    .line 437
    return-void
.end method

.method public setListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    .prologue
    .line 602
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    .line 603
    return-void
.end method

.method public setTimeOutDelay(J)V
    .locals 0
    .param p1, "timeOutDelay"    # J

    .prologue
    .line 650
    iput-wide p1, p0, Lcom/sec/dmc/sic/android/property/HandlerProperty;->timeOutDelay:J

    .line 651
    return-void
.end method
