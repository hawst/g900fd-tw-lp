.class public Lcom/sec/dmc/sic/android/property/LegendProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "LegendProperty.java"


# static fields
.field public static final LEGEND_DIRECTION_BOTTOM:I = 0x3

.field public static final LEGEND_DIRECTION_LEFT:I = 0x0

.field public static final LEGEND_DIRECTION_RIGHT:I = 0x1

.field public static final LEGEND_DIRECTION_TOP:I = 0x2


# instance fields
.field direction:I

.field disableAlpha:F

.field disableColorB:F

.field disableColorG:F

.field disableColorR:F

.field itemBimtap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field itemDisableBitmap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field itemHSpace:F

.field itemHeight:F

.field itemInSpace:F

.field itemWSpace:F

.field itemWidth:F

.field textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/high16 v3, 0x41a00000    # 20.0f

    const v2, 0x3f4ccccd    # 0.8f

    .line 89
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 76
    iput-object v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemBimtap:Ljava/util/ArrayList;

    .line 77
    iput-object v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemDisableBitmap:Ljava/util/ArrayList;

    .line 91
    iput-boolean v1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->enable:Z

    .line 92
    iput-boolean v1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->visible:Z

    .line 94
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->direction:I

    .line 96
    const/high16 v1, 0x42c80000    # 100.0f

    iput v1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWidth:F

    .line 97
    iput v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHeight:F

    .line 98
    iput v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHSpace:F

    .line 99
    const/high16 v1, 0x42480000    # 50.0f

    iput v1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWSpace:F

    .line 100
    const/high16 v1, 0x40a00000    # 5.0f

    iput v1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemInSpace:F

    .line 102
    iput v2, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorR:F

    .line 103
    iput v2, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorG:F

    .line 104
    iput v2, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorB:F

    .line 105
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableAlpha:F

    .line 107
    const v0, -0xafafb0

    .line 109
    .local v0, "a":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemBimtap:Ljava/util/ArrayList;

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemDisableBitmap:Ljava/util/ArrayList;

    .line 112
    sget-object v1, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 113
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 259
    if-ne p0, p1, :cond_1

    .line 310
    :cond_0
    :goto_0
    return v1

    .line 261
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 262
    goto :goto_0

    .line 263
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 264
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 265
    check-cast v0, Lcom/sec/dmc/sic/android/property/LegendProperty;

    .line 266
    .local v0, "other":Lcom/sec/dmc/sic/android/property/LegendProperty;
    iget v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->direction:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->direction:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 267
    goto :goto_0

    .line 268
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 269
    iget v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 268
    if-eq v3, v4, :cond_5

    move v1, v2

    .line 270
    goto :goto_0

    .line 271
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 272
    iget v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 271
    if-eq v3, v4, :cond_6

    move v1, v2

    .line 273
    goto :goto_0

    .line 274
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 275
    iget v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 274
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 276
    goto :goto_0

    .line 277
    :cond_7
    iget v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 278
    iget v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 277
    if-eq v3, v4, :cond_8

    move v1, v2

    .line 279
    goto :goto_0

    .line 280
    :cond_8
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemBimtap:Ljava/util/ArrayList;

    if-nez v3, :cond_9

    .line 281
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemBimtap:Ljava/util/ArrayList;

    if-eqz v3, :cond_a

    move v1, v2

    .line 282
    goto :goto_0

    .line 283
    :cond_9
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemBimtap:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemBimtap:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 284
    goto :goto_0

    .line 285
    :cond_a
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemDisableBitmap:Ljava/util/ArrayList;

    if-nez v3, :cond_b

    .line 286
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemDisableBitmap:Ljava/util/ArrayList;

    if-eqz v3, :cond_c

    move v1, v2

    .line 287
    goto :goto_0

    .line 288
    :cond_b
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemDisableBitmap:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemDisableBitmap:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 289
    goto/16 :goto_0

    .line 290
    :cond_c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHSpace:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 291
    iget v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHSpace:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 290
    if-eq v3, v4, :cond_d

    move v1, v2

    .line 292
    goto/16 :goto_0

    .line 293
    :cond_d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHeight:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 294
    iget v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHeight:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 293
    if-eq v3, v4, :cond_e

    move v1, v2

    .line 295
    goto/16 :goto_0

    .line 296
    :cond_e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemInSpace:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 297
    iget v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemInSpace:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 296
    if-eq v3, v4, :cond_f

    move v1, v2

    .line 298
    goto/16 :goto_0

    .line 299
    :cond_f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWSpace:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 300
    iget v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWSpace:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 299
    if-eq v3, v4, :cond_10

    move v1, v2

    .line 301
    goto/16 :goto_0

    .line 302
    :cond_10
    iget v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 303
    iget v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 302
    if-eq v3, v4, :cond_11

    move v1, v2

    .line 304
    goto/16 :goto_0

    .line 305
    :cond_11
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_12

    .line 306
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_0

    move v1, v2

    .line 307
    goto/16 :goto_0

    .line 308
    :cond_12
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/LegendProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 309
    goto/16 :goto_0
.end method

.method public getDirection()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->direction:I

    return v0
.end method

.method public getDisableAlpha()F
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableAlpha:F

    return v0
.end method

.method public getDisableColorB()F
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorB:F

    return v0
.end method

.method public getDisableColorG()F
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorG:F

    return v0
.end method

.method public getDisableColorR()F
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorR:F

    return v0
.end method

.method public getItemBimtap()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemBimtap:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getItemDisableBitmap()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemDisableBitmap:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getItemHSpace()F
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHSpace:F

    return v0
.end method

.method public getItemHeight()F
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHeight:F

    return v0
.end method

.method public getItemInSpace()F
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemInSpace:F

    return v0
.end method

.method public getItemWSpace()F
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWSpace:F

    return v0
.end method

.method public getItemWidth()F
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWidth:F

    return v0
.end method

.method public getTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 234
    const/16 v0, 0x1f

    .line 235
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 236
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->direction:I

    add-int v1, v2, v4

    .line 237
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 238
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 239
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 240
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 241
    mul-int/lit8 v4, v1, 0x1f

    .line 242
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemBimtap:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    move v2, v3

    .line 241
    :goto_0
    add-int v1, v4, v2

    .line 243
    mul-int/lit8 v4, v1, 0x1f

    .line 245
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemDisableBitmap:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    move v2, v3

    .line 243
    :goto_1
    add-int v1, v4, v2

    .line 247
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHSpace:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 248
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHeight:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 249
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemInSpace:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 250
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWSpace:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 251
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 252
    mul-int/lit8 v2, v1, 0x1f

    .line 253
    iget-object v4, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v4, :cond_2

    .line 252
    :goto_2
    add-int v1, v2, v3

    .line 254
    return v1

    .line 242
    :cond_0
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemBimtap:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->hashCode()I

    move-result v2

    goto :goto_0

    .line 245
    :cond_1
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemDisableBitmap:Ljava/util/ArrayList;

    .line 246
    invoke-virtual {v2}, Ljava/util/ArrayList;->hashCode()I

    move-result v2

    goto :goto_1

    .line 253
    :cond_2
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v3

    goto :goto_2
.end method

.method public setDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 126
    iput p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->direction:I

    .line 127
    return-void
.end method

.method public setDisableAlpha(F)V
    .locals 0
    .param p1, "disableAlpha"    # F

    .prologue
    .line 205
    iput p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableAlpha:F

    .line 206
    return-void
.end method

.method public setDisableColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 198
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorR:F

    .line 199
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorG:F

    .line 200
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorB:F

    .line 201
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableAlpha:F

    .line 202
    return-void
.end method

.method public setDisableColorB(F)V
    .locals 0
    .param p1, "disableColorB"    # F

    .prologue
    .line 190
    iput p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorB:F

    .line 191
    return-void
.end method

.method public setDisableColorG(F)V
    .locals 0
    .param p1, "disableColorG"    # F

    .prologue
    .line 182
    iput p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorG:F

    .line 183
    return-void
.end method

.method public setDisableColorR(F)V
    .locals 0
    .param p1, "disableColorR"    # F

    .prologue
    .line 174
    iput p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->disableColorR:F

    .line 175
    return-void
.end method

.method public setItemBimtap(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p1, "itemBimtap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemBimtap:Ljava/util/ArrayList;

    .line 214
    return-void
.end method

.method public setItemDisableBitmap(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 221
    .local p1, "itemDisableBitmap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemDisableBitmap:Ljava/util/ArrayList;

    .line 222
    return-void
.end method

.method public setItemHSpace(F)V
    .locals 0
    .param p1, "itemHSpace"    # F

    .prologue
    .line 158
    iput p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHSpace:F

    .line 159
    return-void
.end method

.method public setItemHeight(F)V
    .locals 0
    .param p1, "itemHeight"    # F

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemHeight:F

    .line 143
    return-void
.end method

.method public setItemInSpace(F)V
    .locals 0
    .param p1, "itemInSpace"    # F

    .prologue
    .line 166
    iput p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemInSpace:F

    .line 167
    return-void
.end method

.method public setItemWSpace(F)V
    .locals 0
    .param p1, "itemWSpace"    # F

    .prologue
    .line 150
    iput p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWSpace:F

    .line 151
    return-void
.end method

.method public setItemWidth(F)V
    .locals 0
    .param p1, "itemWidth"    # F

    .prologue
    .line 134
    iput p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->itemWidth:F

    .line 135
    return-void
.end method

.method public setTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/LegendProperty;->textStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 230
    return-void
.end method
