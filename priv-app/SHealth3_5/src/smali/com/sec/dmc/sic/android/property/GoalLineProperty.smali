.class public Lcom/sec/dmc/sic/android/property/GoalLineProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "GoalLineProperty.java"


# static fields
.field public static final GOAL_LINE_TEXT_ALIGN_CENTER:I = 0x1

.field public static final GOAL_LINE_TEXT_ALIGN_LEFT:I = 0x0

.field public static final GOAL_LINE_TEXT_ALIGN_RIGHT:I = 0x2

.field public static final GOAL_LINE_TEXT_ORIGIN_LEFT:I = 0x0

.field public static final GOAL_LINE_TEXT_ORIGIN_RIGHT:I = 0x1


# instance fields
.field private mGoalEnableUpdateValue:Z

.field private mGoalLineColorA:F

.field private mGoalLineColorB:F

.field private mGoalLineColorG:F

.field private mGoalLineColorR:F

.field private mGoalLineID:I

.field private mGoalLineThickness:F

.field private mGoalOverColorA:F

.field private mGoalOverColorB:F

.field private mGoalOverColorG:F

.field private mGoalOverColorR:F

.field private mGoalPostfixStr:Ljava/lang/String;

.field private mGoalPostfixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field private mGoalPrefixStr:Ljava/lang/String;

.field private mGoalPrefixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field private mGoalTextAlign:I

.field private mGoalTextOffsetX:F

.field private mGoalTextOffsetY:F

.field private mGoalTextOrigin:I

.field private mGoalTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field private mGoalTextVisible:Z

.field private mGoalValue:F


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 46
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->enable:Z

    .line 47
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->visible:Z

    .line 49
    const v0, -0x803662    # -3.4E38f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalValue:F

    .line 50
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 51
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalPrefixStr(Ljava/lang/String;)V

    .line 52
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalPostfixStr(Ljava/lang/String;)V

    .line 53
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 54
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 55
    iput v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOrigin:I

    .line 56
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextAlign:I

    .line 57
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetX:F

    .line 58
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetY:F

    .line 60
    const/high16 v0, 0x40c00000    # 6.0f

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineThickness:F

    .line 61
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorR:F

    .line 62
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorG:F

    .line 63
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorB:F

    .line 64
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorA:F

    .line 65
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorR:F

    .line 66
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorG:F

    .line 67
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorB:F

    .line 68
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorA:F

    .line 69
    iput-boolean v4, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalEnableUpdateValue:Z

    .line 71
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextVisible:Z

    .line 73
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 292
    if-ne p0, p1, :cond_1

    .line 370
    :cond_0
    :goto_0
    return v1

    .line 294
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 295
    goto :goto_0

    .line 296
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 297
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 298
    check-cast v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    .line 299
    .local v0, "other":Lcom/sec/dmc/sic/android/property/GoalLineProperty;
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalEnableUpdateValue:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalEnableUpdateValue:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 300
    goto :goto_0

    .line 301
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorA:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 302
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 301
    if-eq v3, v4, :cond_5

    move v1, v2

    .line 303
    goto :goto_0

    .line 304
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 305
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 304
    if-eq v3, v4, :cond_6

    move v1, v2

    .line 306
    goto :goto_0

    .line 307
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 308
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 307
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 309
    goto :goto_0

    .line 310
    :cond_7
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 311
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 310
    if-eq v3, v4, :cond_8

    move v1, v2

    .line 312
    goto :goto_0

    .line 313
    :cond_8
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineID:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineID:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 314
    goto :goto_0

    .line 315
    :cond_9
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineThickness:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 316
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineThickness:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 315
    if-eq v3, v4, :cond_a

    move v1, v2

    .line 317
    goto :goto_0

    .line 318
    :cond_a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorA:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 319
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorA:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 318
    if-eq v3, v4, :cond_b

    move v1, v2

    .line 320
    goto/16 :goto_0

    .line 321
    :cond_b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 322
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 321
    if-eq v3, v4, :cond_c

    move v1, v2

    .line 323
    goto/16 :goto_0

    .line 324
    :cond_c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 325
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 324
    if-eq v3, v4, :cond_d

    move v1, v2

    .line 326
    goto/16 :goto_0

    .line 327
    :cond_d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 328
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 327
    if-eq v3, v4, :cond_e

    move v1, v2

    .line 329
    goto/16 :goto_0

    .line 330
    :cond_e
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStr:Ljava/lang/String;

    if-nez v3, :cond_f

    .line 331
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStr:Ljava/lang/String;

    if-eqz v3, :cond_10

    move v1, v2

    .line 332
    goto/16 :goto_0

    .line 333
    :cond_f
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStr:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 334
    goto/16 :goto_0

    .line 335
    :cond_10
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_11

    .line 336
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_12

    move v1, v2

    .line 337
    goto/16 :goto_0

    .line 338
    :cond_11
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    move v1, v2

    .line 339
    goto/16 :goto_0

    .line 340
    :cond_12
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStr:Ljava/lang/String;

    if-nez v3, :cond_13

    .line 341
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStr:Ljava/lang/String;

    if-eqz v3, :cond_14

    move v1, v2

    .line 342
    goto/16 :goto_0

    .line 343
    :cond_13
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStr:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    move v1, v2

    .line 344
    goto/16 :goto_0

    .line 345
    :cond_14
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_15

    .line 346
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_16

    move v1, v2

    .line 347
    goto/16 :goto_0

    .line 348
    :cond_15
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    move v1, v2

    .line 349
    goto/16 :goto_0

    .line 350
    :cond_16
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextAlign:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextAlign:I

    if-eq v3, v4, :cond_17

    move v1, v2

    .line 351
    goto/16 :goto_0

    .line 352
    :cond_17
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetX:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 353
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetX:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 352
    if-eq v3, v4, :cond_18

    move v1, v2

    .line 354
    goto/16 :goto_0

    .line 355
    :cond_18
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetY:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 356
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetY:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 355
    if-eq v3, v4, :cond_19

    move v1, v2

    .line 357
    goto/16 :goto_0

    .line 358
    :cond_19
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOrigin:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOrigin:I

    if-eq v3, v4, :cond_1a

    move v1, v2

    .line 359
    goto/16 :goto_0

    .line 360
    :cond_1a
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_1b

    .line 361
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_1c

    move v1, v2

    .line 362
    goto/16 :goto_0

    .line 363
    :cond_1b
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1c

    move v1, v2

    .line 364
    goto/16 :goto_0

    .line 365
    :cond_1c
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextVisible:Z

    if-eq v3, v4, :cond_1d

    move v1, v2

    .line 366
    goto/16 :goto_0

    .line 367
    :cond_1d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalValue:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 368
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalValue:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 367
    if-eq v3, v4, :cond_0

    move v1, v2

    .line 369
    goto/16 :goto_0
.end method

.method public getGoalEnableUpdateValue()Z
    .locals 1

    .prologue
    .line 374
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalEnableUpdateValue:Z

    return v0
.end method

.method public getGoalLineColorAlpha()F
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorA:F

    return v0
.end method

.method public getGoalLineColorB()F
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorB:F

    return v0
.end method

.method public getGoalLineColorG()F
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorG:F

    return v0
.end method

.method public getGoalLineColorR()F
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorR:F

    return v0
.end method

.method public getGoalLineID()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineID:I

    return v0
.end method

.method public getGoalLineThickness()F
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineThickness:F

    return v0
.end method

.method public getGoalOverColorAlpha()F
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorA:F

    return v0
.end method

.method public getGoalOverColorB()F
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorB:F

    return v0
.end method

.method public getGoalOverColorG()F
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorG:F

    return v0
.end method

.method public getGoalOverColorR()F
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorR:F

    return v0
.end method

.method public getGoalPostfixStr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStr:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalPostfixStryle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getGoalPrefixStr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStr:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalPrefixStryle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getGoalTextAlign()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextAlign:I

    return v0
.end method

.method public getGoalTextOffsetX()F
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetX:F

    return v0
.end method

.method public getGoalTextOffsetY()F
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetY:F

    return v0
.end method

.method public getGoalTextOrigin()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOrigin:I

    return v0
.end method

.method public getGoalTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getGoalTextVisible()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextVisible:Z

    return v0
.end method

.method public getGoalValue()F
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalValue:F

    return v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v5, 0x0

    .line 255
    const/16 v0, 0x1f

    .line 256
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 257
    .local v1, "result":I
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalEnableUpdateValue:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 258
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorA:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 259
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 260
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 261
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 262
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineID:I

    add-int v1, v2, v6

    .line 263
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineThickness:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 264
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorA:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 265
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 266
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 267
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 268
    mul-int/lit8 v6, v1, 0x1f

    .line 269
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStr:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v5

    .line 268
    :goto_1
    add-int v1, v6, v2

    .line 270
    mul-int/lit8 v6, v1, 0x1f

    .line 272
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_2

    move v2, v5

    .line 270
    :goto_2
    add-int v1, v6, v2

    .line 274
    mul-int/lit8 v6, v1, 0x1f

    .line 275
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStr:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v5

    .line 274
    :goto_3
    add-int v1, v6, v2

    .line 276
    mul-int/lit8 v6, v1, 0x1f

    .line 278
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v2, :cond_4

    move v2, v5

    .line 276
    :goto_4
    add-int v1, v6, v2

    .line 279
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextAlign:I

    add-int v1, v2, v6

    .line 280
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetX:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 281
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetY:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 282
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOrigin:I

    add-int v1, v2, v6

    .line 283
    mul-int/lit8 v2, v1, 0x1f

    .line 284
    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v6, :cond_5

    .line 283
    :goto_5
    add-int v1, v2, v5

    .line 285
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v5, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextVisible:Z

    if-eqz v5, :cond_6

    :goto_6
    add-int v1, v2, v3

    .line 286
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalValue:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 287
    return v1

    :cond_0
    move v2, v4

    .line 257
    goto/16 :goto_0

    .line 269
    :cond_1
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStr:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 272
    :cond_2
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 273
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto :goto_2

    .line 275
    :cond_3
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStr:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    .line 278
    :cond_4
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v2

    goto :goto_4

    .line 284
    :cond_5
    iget-object v5, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v5

    goto :goto_5

    :cond_6
    move v3, v4

    .line 285
    goto :goto_6
.end method

.method public setGoalEnableUpdateValue(Z)V
    .locals 0
    .param p1, "mGoalEnableUpdateValue"    # Z

    .prologue
    .line 378
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalEnableUpdateValue:Z

    .line 379
    return-void
.end method

.method public setGoalLineColor(FFFF)V
    .locals 0
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 182
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorR:F

    .line 183
    iput p2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorG:F

    .line 184
    iput p3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorB:F

    .line 185
    iput p4, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorA:F

    .line 187
    return-void
.end method

.method public setGoalLineColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 191
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorR:F

    .line 192
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorG:F

    .line 193
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorB:F

    .line 194
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineColorA:F

    .line 196
    return-void
.end method

.method public setGoalLineID(I)V
    .locals 0
    .param p1, "goalLineID"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineID:I

    .line 81
    return-void
.end method

.method public setGoalLineThickness(F)V
    .locals 0
    .param p1, "goalLineThickness"    # F

    .prologue
    .line 177
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalLineThickness:F

    .line 178
    return-void
.end method

.method public setGoalOverColor(FFFF)V
    .locals 0
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 218
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorR:F

    .line 219
    iput p2, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorG:F

    .line 220
    iput p3, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorB:F

    .line 221
    iput p4, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorA:F

    .line 223
    return-void
.end method

.method public setGoalOverColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 227
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorR:F

    .line 228
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorG:F

    .line 229
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorB:F

    .line 230
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalOverColorA:F

    .line 232
    return-void
.end method

.method public setGoalPostfixStr(Ljava/lang/String;)V
    .locals 0
    .param p1, "mGoalPostfixStr"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStr:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setGoalPostfixStryle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "mGoalPostfixStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPostfixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 138
    return-void
.end method

.method public setGoalPrefixStr(Ljava/lang/String;)V
    .locals 0
    .param p1, "mGoalPrefixStr"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStr:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setGoalPrefixStryle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "mGoalPrefixStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalPrefixStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 130
    return-void
.end method

.method public setGoalTextAlign(I)V
    .locals 0
    .param p1, "mGoalTextAlign"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextAlign:I

    .line 154
    return-void
.end method

.method public setGoalTextOffsetX(F)V
    .locals 0
    .param p1, "mGoalTextOffsetX"    # F

    .prologue
    .line 161
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetX:F

    .line 162
    return-void
.end method

.method public setGoalTextOffsetY(F)V
    .locals 0
    .param p1, "mGoalTextOffsetY"    # F

    .prologue
    .line 169
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOffsetY:F

    .line 170
    return-void
.end method

.method public setGoalTextOrigin(I)V
    .locals 0
    .param p1, "mGoalTextOrigin"    # I

    .prologue
    .line 145
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextOrigin:I

    .line 146
    return-void
.end method

.method public setGoalTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "goalTextStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 106
    return-void
.end method

.method public setGoalTextVisible(Z)V
    .locals 0
    .param p1, "goalTextVisible"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalTextVisible:Z

    .line 98
    return-void
.end method

.method public setGoalValue(F)V
    .locals 0
    .param p1, "goalValue"    # F

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->mGoalValue:F

    .line 89
    return-void
.end method
