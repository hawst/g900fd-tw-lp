.class public Lcom/sec/dmc/sic/jni/ChartJNI;
.super Ljava/lang/Object;
.source "ChartJNI.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-string v0, "JNIChartLib"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native AddData(JLjava/nio/ByteBuffer;)V
.end method

.method public static native AddDataWithAni(JLjava/nio/ByteBuffer;I)V
.end method

.method public static native AddNormalRegion(JFF)I
.end method

.method public static native AddStringCandleData(JII[Ljava/lang/Object;)V
.end method

.method public static native AddStringCandleDataFront(JII[Ljava/lang/Object;)V
.end method

.method public static native AddStringData(JII[Ljava/lang/Object;)V
.end method

.method public static native AddStringDataFront(JII[Ljava/lang/Object;)V
.end method

.method public static native AddXAxis(J)I
.end method

.method public static native AddYAxis(J)I
.end method

.method public static ApplyLocaleDateString(Ljava/lang/String;I)Ljava/lang/String;
    .locals 9
    .param p0, "ori"    # Ljava/lang/String;
    .param p1, "type"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 344
    new-instance v2, Ljava/lang/String;

    const-string v3, ""

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 346
    .local v2, "result":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 391
    .end local p0    # "ori":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 349
    .restart local p0    # "ori":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/util/Scanner;

    invoke-direct {v0, p0}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    .line 350
    .local v0, "digitScanner":Ljava/util/Scanner;
    const-string v3, "[^\\d]+"

    invoke-virtual {v0, v3}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    .line 352
    new-instance v1, Ljava/util/Scanner;

    invoke-direct {v1, p0}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    .line 353
    .local v1, "nonDigitscanner":Ljava/util/Scanner;
    const-string v3, "[\\d]+"

    invoke-virtual {v1, v3}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    .line 355
    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 357
    :cond_2
    invoke-virtual {v0}, Ljava/util/Scanner;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 358
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "%02d"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 361
    :cond_3
    invoke-virtual {v1}, Ljava/util/Scanner;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 362
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 365
    :cond_4
    invoke-virtual {v0}, Ljava/util/Scanner;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v1}, Ljava/util/Scanner;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 388
    :goto_1
    invoke-virtual {v0}, Ljava/util/Scanner;->close()V

    .line 389
    invoke-virtual {v1}, Ljava/util/Scanner;->close()V

    move-object p0, v2

    .line 391
    goto :goto_0

    .line 373
    :cond_5
    invoke-virtual {v1}, Ljava/util/Scanner;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 374
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 377
    :cond_6
    invoke-virtual {v0}, Ljava/util/Scanner;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 378
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "%02d"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 381
    :cond_7
    invoke-virtual {v0}, Ljava/util/Scanner;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v1}, Ljava/util/Scanner;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    goto :goto_1
.end method

.method public static native ChangeData(JLjava/nio/ByteBuffer;)V
.end method

.method public static native ChangeStringData(JII[Ljava/lang/Object;)V
.end method

.method public static native ClearData(J)V
.end method

.method public static native CreateSeries(JII)V
.end method

.method public static native CreateSeriesSingle(JI)V
.end method

.method private static CreateText(Ljava/lang/String;IIIIIFF)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I
    .param p5, "fontSize"    # I
    .param p6, "w"    # F
    .param p7, "h"    # F

    .prologue
    .line 304
    move/from16 v0, p6

    float-to-int v7, v0

    move/from16 v0, p7

    float-to-int v8, v0

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 306
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 307
    .local v3, "canvas":Landroid/graphics/Canvas;
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 309
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 310
    .local v4, "textPaint":Landroid/graphics/Paint;
    int-to-float v7, p5

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 311
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 312
    invoke-virtual {v4, p4, p1, p2, p3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 313
    sget-object v7, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 316
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 317
    .local v2, "bounds":Landroid/graphics/Rect;
    const/4 v7, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v4, p0, v7, v8, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 318
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v7, v8

    div-int/lit8 v5, v7, 0x2

    .line 319
    .local v5, "x":I
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v8

    add-int/2addr v7, v8

    div-int/lit8 v6, v7, 0x2

    .line 321
    .local v6, "y":I
    int-to-float v7, v5

    int-to-float v8, v6

    invoke-virtual {v3, p0, v7, v8, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 323
    return-object v1
.end method

.method public static CreateText(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "textStyleName"    # Ljava/lang/String;

    .prologue
    .line 331
    const/4 v0, 0x0

    .line 332
    .local v0, "bp":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public static native DeleteData(JIID)V
.end method

.method public static native DeleteDatas(JIIDD)V
.end method

.method public static native DeleteForHeartRate(JI)J
.end method

.method public static native GetCanZoomPanning(J)Z
.end method

.method public static native GetIsZoom(J)Z
.end method

.method public static native GetLeftEndofTimeChart(J)J
.end method

.method public static native GetLegendItemIndex(J)I
.end method

.method public static native GetLegendItemStatus(J)Z
.end method

.method public static native GetMaxY(J)F
.end method

.method public static native GetMinY(J)F
.end method

.method public static native GetRightEndofTimeChart(J)J
.end method

.method private static GetTextBaselineOffset(IIIILjava/lang/String;ZI)F
    .locals 9
    .param p0, "fsize"    # I
    .param p1, "fstyle"    # I
    .param p2, "fcolor"    # I
    .param p3, "fflags"    # I
    .param p4, "ffamily"    # Ljava/lang/String;
    .param p5, "applyLocale"    # Z
    .param p6, "type"    # I

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 576
    const/4 v0, 0x0

    .line 578
    .local v0, "baselineOffset":F
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    .line 581
    .local v2, "pt":Landroid/text/TextPaint;
    int-to-float v4, p0

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 583
    const-string v4, "0"

    invoke-virtual {p4, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 584
    packed-switch p1, :pswitch_data_0

    .line 598
    invoke-static {p4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 620
    .local v3, "tface":Landroid/graphics/Typeface;
    :goto_0
    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 621
    invoke-virtual {v2, p2}, Landroid/text/TextPaint;->setColor(I)V

    .line 622
    invoke-virtual {v2, p3}, Landroid/text/TextPaint;->setFlags(I)V

    .line 624
    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v1

    .line 625
    .local v1, "fmi":Landroid/graphics/Paint$FontMetricsInt;
    iget v4, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    int-to-float v0, v4

    .line 626
    return v0

    .line 586
    .end local v1    # "fmi":Landroid/graphics/Paint$FontMetricsInt;
    .end local v3    # "tface":Landroid/graphics/Typeface;
    :pswitch_0
    invoke-static {p4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 587
    .restart local v3    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 589
    .end local v3    # "tface":Landroid/graphics/Typeface;
    :pswitch_1
    invoke-static {p4, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 590
    .restart local v3    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 592
    .end local v3    # "tface":Landroid/graphics/Typeface;
    :pswitch_2
    invoke-static {p4, v7}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 593
    .restart local v3    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 595
    .end local v3    # "tface":Landroid/graphics/Typeface;
    :pswitch_3
    invoke-static {p4, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 596
    .restart local v3    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 602
    .end local v3    # "tface":Landroid/graphics/Typeface;
    :cond_0
    packed-switch p1, :pswitch_data_1

    .line 616
    invoke-static {v5}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v3

    .restart local v3    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 604
    .end local v3    # "tface":Landroid/graphics/Typeface;
    :pswitch_4
    invoke-static {v5}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 605
    .restart local v3    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 607
    .end local v3    # "tface":Landroid/graphics/Typeface;
    :pswitch_5
    invoke-static {v6}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 608
    .restart local v3    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 610
    .end local v3    # "tface":Landroid/graphics/Typeface;
    :pswitch_6
    invoke-static {v7}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 611
    .restart local v3    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 613
    .end local v3    # "tface":Landroid/graphics/Typeface;
    :pswitch_7
    invoke-static {v8}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 614
    .restart local v3    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 584
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 602
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static GetTextBoundBox(Ljava/lang/String;IIIILjava/lang/String;ZI)Landroid/graphics/PointF;
    .locals 10
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "fsize"    # I
    .param p2, "fstyle"    # I
    .param p3, "fcolor"    # I
    .param p4, "fflags"    # I
    .param p5, "ffamily"    # Ljava/lang/String;
    .param p6, "applyLocale"    # Z
    .param p7, "type"    # I

    .prologue
    .line 500
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    const-string v9, "ar"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    if-eqz p6, :cond_0

    .line 501
    move/from16 v0, p7

    invoke-static {p0, v0}, Lcom/sec/dmc/sic/jni/ChartJNI;->ApplyLocaleDateString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    .line 505
    :cond_0
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    .line 509
    .local v2, "pt":Landroid/text/TextPaint;
    int-to-float v8, p1

    invoke-virtual {v2, v8}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 511
    const-string v8, "0"

    invoke-virtual {p5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 512
    packed-switch p2, :pswitch_data_0

    .line 526
    const/4 v8, 0x0

    invoke-static {p5, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 548
    .local v6, "tface":Landroid/graphics/Typeface;
    :goto_0
    invoke-virtual {v2, v6}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 549
    invoke-virtual {v2, p3}, Landroid/text/TextPaint;->setColor(I)V

    .line 550
    invoke-virtual {v2, p4}, Landroid/text/TextPaint;->setFlags(I)V

    .line 552
    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v1

    .line 553
    .local v1, "fmi":Landroid/graphics/Paint$FontMetricsInt;
    const/4 v4, 0x0

    .line 554
    .local v4, "strhi":I
    const/4 v5, 0x0

    .line 555
    .local v5, "strwf":F
    const/4 v3, 0x0

    .line 557
    .local v3, "strhf":F
    iget v8, v1, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    iget v9, v1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v8, v9

    iget v9, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    add-int v4, v8, v9

    .line 558
    const-string v8, "\n"

    invoke-virtual {p0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 566
    .local v7, "tmpStr":[Ljava/lang/String;
    invoke-static {p0, v2}, Lcom/sec/dmc/sic/jni/ChartJNI;->GetTextWidth(Ljava/lang/String;Landroid/text/TextPaint;)F

    move-result v5

    .line 567
    array-length v8, v7

    mul-int/2addr v8, v4

    int-to-float v3, v8

    .line 571
    new-instance v8, Landroid/graphics/PointF;

    invoke-direct {v8, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v8

    .line 514
    .end local v1    # "fmi":Landroid/graphics/Paint$FontMetricsInt;
    .end local v3    # "strhf":F
    .end local v4    # "strhi":I
    .end local v5    # "strwf":F
    .end local v6    # "tface":Landroid/graphics/Typeface;
    .end local v7    # "tmpStr":[Ljava/lang/String;
    :pswitch_0
    const/4 v8, 0x0

    invoke-static {p5, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 515
    .restart local v6    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 517
    .end local v6    # "tface":Landroid/graphics/Typeface;
    :pswitch_1
    const/4 v8, 0x1

    invoke-static {p5, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 518
    .restart local v6    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 520
    .end local v6    # "tface":Landroid/graphics/Typeface;
    :pswitch_2
    const/4 v8, 0x2

    invoke-static {p5, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 521
    .restart local v6    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 523
    .end local v6    # "tface":Landroid/graphics/Typeface;
    :pswitch_3
    const/4 v8, 0x3

    invoke-static {p5, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 524
    .restart local v6    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 530
    .end local v6    # "tface":Landroid/graphics/Typeface;
    :cond_1
    packed-switch p2, :pswitch_data_1

    .line 544
    const/4 v8, 0x0

    invoke-static {v8}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v6

    .restart local v6    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 532
    .end local v6    # "tface":Landroid/graphics/Typeface;
    :pswitch_4
    const/4 v8, 0x0

    invoke-static {v8}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 533
    .restart local v6    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 535
    .end local v6    # "tface":Landroid/graphics/Typeface;
    :pswitch_5
    const/4 v8, 0x1

    invoke-static {v8}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 536
    .restart local v6    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 538
    .end local v6    # "tface":Landroid/graphics/Typeface;
    :pswitch_6
    const/4 v8, 0x2

    invoke-static {v8}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 539
    .restart local v6    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 541
    .end local v6    # "tface":Landroid/graphics/Typeface;
    :pswitch_7
    const/4 v8, 0x3

    invoke-static {v8}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 542
    .restart local v6    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 512
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 530
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static GetTextWidth(Ljava/lang/String;Landroid/text/TextPaint;)F
    .locals 3
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "p"    # Landroid/text/TextPaint;

    .prologue
    .line 337
    invoke-static {p0, p1}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v0, v1

    .line 339
    .local v0, "boundedWidth":I
    int-to-float v1, v0

    return v1
.end method

.method public static native GetZoomIndex(JFF)I
.end method

.method public static native InsertData(JIILjava/nio/ByteBuffer;)V
.end method

.method public static native IsGraphRegion(JFF)Z
.end method

.method public static native IsZoomAnimationPlaying(J)Z
.end method

.method public static native OnNotifyFlickEnd(J)V
.end method

.method public static native OnNotifyFlickStart(J)V
.end method

.method public static native OnNotifyScrollEnd(J)V
.end method

.method public static native OnNotifyScrollStart(J)V
.end method

.method public static native OnTouchUp(JFF)V
.end method

.method public static RenderText(Ljava/lang/String;IIIILjava/lang/String;ZI)Landroid/graphics/Bitmap;
    .locals 17
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "fsize"    # I
    .param p2, "fstyle"    # I
    .param p3, "fcolor"    # I
    .param p4, "fflags"    # I
    .param p5, "ffamily"    # Ljava/lang/String;
    .param p6, "applyLocale"    # Z
    .param p7, "type"    # I

    .prologue
    .line 403
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v13

    const-string v14, "ar"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    if-eqz p6, :cond_0

    .line 404
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->ApplyLocaleDateString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    .line 408
    :cond_0
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7}, Landroid/text/TextPaint;-><init>()V

    .line 412
    .local v7, "pt":Landroid/text/TextPaint;
    move/from16 v0, p1

    int-to-float v13, v0

    invoke-virtual {v7, v13}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 414
    const-string v13, "0"

    move-object/from16 v0, p5

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 415
    packed-switch p2, :pswitch_data_0

    .line 429
    const/4 v13, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v13}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v11

    .line 451
    .local v11, "tface":Landroid/graphics/Typeface;
    :goto_0
    invoke-virtual {v7, v11}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 452
    move/from16 v0, p3

    invoke-virtual {v7, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 453
    move/from16 v0, p4

    invoke-virtual {v7, v0}, Landroid/text/TextPaint;->setFlags(I)V

    .line 455
    invoke-virtual {v7}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v5

    .line 456
    .local v5, "fmi":Landroid/graphics/Paint$FontMetricsInt;
    const/4 v9, 0x0

    .line 457
    .local v9, "strhi":I
    const/4 v10, 0x0

    .line 458
    .local v10, "strwf":F
    const/4 v8, 0x0

    .line 460
    .local v8, "strhf":F
    iget v13, v5, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    iget v14, v5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v13, v14

    iget v14, v5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    add-int v9, v13, v14

    .line 461
    const-string v13, "\n"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 470
    .local v12, "tmpStr":[Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lcom/sec/dmc/sic/jni/ChartJNI;->GetTextWidth(Ljava/lang/String;Landroid/text/TextPaint;)F

    move-result v10

    .line 471
    array-length v13, v12

    mul-int/2addr v13, v9

    int-to-float v8, v13

    .line 473
    const/high16 v13, 0x3f800000    # 1.0f

    cmpg-float v13, v10, v13

    if-gtz v13, :cond_1

    .line 474
    const/high16 v10, 0x3f800000    # 1.0f

    .line 477
    :cond_1
    const/high16 v13, 0x3f800000    # 1.0f

    cmpg-float v13, v8, v13

    if-gtz v13, :cond_2

    .line 478
    const/high16 v8, 0x3f800000    # 1.0f

    .line 484
    :cond_2
    float-to-int v13, v10

    float-to-int v14, v8

    sget-object v15, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v13, v14, v15}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 485
    .local v2, "bp":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 487
    .local v4, "cv":Landroid/graphics/Canvas;
    const/4 v3, 0x0

    .line 488
    .local v3, "curY":F
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    array-length v13, v12

    if-lt v6, v13, :cond_4

    .line 495
    return-object v2

    .line 417
    .end local v2    # "bp":Landroid/graphics/Bitmap;
    .end local v3    # "curY":F
    .end local v4    # "cv":Landroid/graphics/Canvas;
    .end local v5    # "fmi":Landroid/graphics/Paint$FontMetricsInt;
    .end local v6    # "i":I
    .end local v8    # "strhf":F
    .end local v9    # "strhi":I
    .end local v10    # "strwf":F
    .end local v11    # "tface":Landroid/graphics/Typeface;
    .end local v12    # "tmpStr":[Ljava/lang/String;
    :pswitch_0
    const/4 v13, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v13}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v11

    .line 418
    .restart local v11    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 420
    .end local v11    # "tface":Landroid/graphics/Typeface;
    :pswitch_1
    const/4 v13, 0x1

    move-object/from16 v0, p5

    invoke-static {v0, v13}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v11

    .line 421
    .restart local v11    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 423
    .end local v11    # "tface":Landroid/graphics/Typeface;
    :pswitch_2
    const/4 v13, 0x2

    move-object/from16 v0, p5

    invoke-static {v0, v13}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v11

    .line 424
    .restart local v11    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 426
    .end local v11    # "tface":Landroid/graphics/Typeface;
    :pswitch_3
    const/4 v13, 0x3

    move-object/from16 v0, p5

    invoke-static {v0, v13}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v11

    .line 427
    .restart local v11    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 433
    .end local v11    # "tface":Landroid/graphics/Typeface;
    :cond_3
    packed-switch p2, :pswitch_data_1

    .line 447
    const/4 v13, 0x0

    invoke-static {v13}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v11

    .restart local v11    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 435
    .end local v11    # "tface":Landroid/graphics/Typeface;
    :pswitch_4
    const/4 v13, 0x0

    invoke-static {v13}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v11

    .line 436
    .restart local v11    # "tface":Landroid/graphics/Typeface;
    goto :goto_0

    .line 438
    .end local v11    # "tface":Landroid/graphics/Typeface;
    :pswitch_5
    const/4 v13, 0x1

    invoke-static {v13}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v11

    .line 439
    .restart local v11    # "tface":Landroid/graphics/Typeface;
    goto/16 :goto_0

    .line 441
    .end local v11    # "tface":Landroid/graphics/Typeface;
    :pswitch_6
    const/4 v13, 0x2

    invoke-static {v13}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v11

    .line 442
    .restart local v11    # "tface":Landroid/graphics/Typeface;
    goto/16 :goto_0

    .line 444
    .end local v11    # "tface":Landroid/graphics/Typeface;
    :pswitch_7
    const/4 v13, 0x3

    invoke-static {v13}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v11

    .line 445
    .restart local v11    # "tface":Landroid/graphics/Typeface;
    goto/16 :goto_0

    .line 489
    .restart local v2    # "bp":Landroid/graphics/Bitmap;
    .restart local v3    # "curY":F
    .restart local v4    # "cv":Landroid/graphics/Canvas;
    .restart local v5    # "fmi":Landroid/graphics/Paint$FontMetricsInt;
    .restart local v6    # "i":I
    .restart local v8    # "strhf":F
    .restart local v9    # "strhi":I
    .restart local v10    # "strwf":F
    .restart local v12    # "tmpStr":[Ljava/lang/String;
    :cond_4
    aget-object v13, v12, v6

    const/4 v14, 0x0

    int-to-float v15, v9

    add-float/2addr v15, v3

    iget v0, v5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v15, v15, v16

    invoke-virtual {v4, v13, v14, v15, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 490
    int-to-float v13, v9

    add-float/2addr v3, v13

    .line 488
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 415
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 433
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static native ResetZoom(J)V
.end method

.method public static native SetAttribute(JJLcom/sec/dmc/sic/android/property/BaseProperty;)V
.end method

.method public static native SetGoalLineTranslate(JIF)V
.end method

.method public static native SetHandlerStartDate(JD)V
.end method

.method public static native SetHandlerStartIndex(JI)V
.end method

.method public static native SetHandlerTranslate(JIF)V
.end method

.method public static native SetHighLight(JZFF)V
.end method

.method public static native SetPrepareZoom(J)V
.end method

.method public static native SetReleaseZoom(J)V
.end method

.method public static native SetScrollRangeDepthLevel(JI)V
.end method

.method public static native SetScrollRangeType(JI)V
.end method

.method public static native SetStartDateVisual(JIDII)V
.end method

.method public static native SetStartVisual(JII)V
.end method

.method public static native SetUserScrollRange(JJJ)V
.end method

.method public static native SetVScroll(JF)V
.end method

.method public static native StartGraphRevealAni(JII)Z
.end method

.method public static native StartRendererZoomOutAni(JI)Z
.end method

.method public static native StartSeriesAppearHide(JII)V
.end method

.method public static native StartSeriesAppearShow(JII)V
.end method

.method public static native changeSize(JII)V
.end method

.method public static native create(II)J
.end method

.method public static native createContext(JII)V
.end method

.method public static native deleteContext(J)V
.end method

.method public static native destroy(J)V
.end method

.method public static native init(JII)V
.end method

.method public static native setChartType(JI)V
.end method

.method public static native setEventCallback(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public static native setGraphType(JII)V
.end method
