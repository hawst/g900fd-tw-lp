.class public Lcom/sec/dmc/sic/android/property/ChartProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "ChartProperty.java"


# static fields
.field public static final MAJOR_LINE_BASE:I = 0x0

.field public static final MINOR_LINE_BASE:I = 0x1


# instance fields
.field private chartBackGroundAlpha:F

.field private chartBackGroundColorB:F

.field private chartBackGroundColorG:F

.field private chartBackGroundColorR:F

.field private graphBackGroundAlpha:F

.field private graphBackGroundColorB:F

.field private graphBackGroundColorG:F

.field private graphBackGroundColorR:F

.field private graphBackGroundColors:[I

.field private graphBackGroundTextStyles:[Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field private graphBackGroundTextValues:[F

.field private graphBackGroundTexts:[Ljava/lang/String;

.field private graphBackGroundValues:[F

.field graphBackgroundImage:Landroid/graphics/Bitmap;

.field private height:F

.field listener:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

.field m_GraphSeparatorVisible:Z

.field m_GraphTitleVisible:Z

.field private m_graphTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field m_maxInclueGoalExceedDataCount:I

.field private m_separatorColor:I

.field private m_separatorWidth:F

.field private markingLineBase:I

.field private paddingBottom:F

.field private paddingLeft:F

.field private paddingRight:F

.field private paddingTop:F

.field private posX:F

.field private posY:F

.field private width:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 97
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 99
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorR:F

    .line 100
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorG:F

    .line 101
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorB:F

    .line 102
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundAlpha:F

    .line 104
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorR:F

    .line 105
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorG:F

    .line 106
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorB:F

    .line 107
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundAlpha:F

    .line 109
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColors:[I

    .line 110
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundValues:[F

    .line 112
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTexts:[Ljava/lang/String;

    .line 113
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextValues:[F

    .line 114
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextStyles:[Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 116
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphTitleVisible:Z

    .line 117
    iput-boolean v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphSeparatorVisible:Z

    .line 118
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_graphTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 119
    const v0, 0x1010101

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorColor:I

    .line 120
    iput v1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorWidth:F

    .line 122
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackgroundImage:Landroid/graphics/Bitmap;

    .line 123
    iput v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->markingLineBase:I

    .line 125
    iput-object v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

    .line 127
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_maxInclueGoalExceedDataCount:I

    .line 128
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 412
    if-ne p0, p1, :cond_1

    .line 503
    :cond_0
    :goto_0
    return v1

    .line 414
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 415
    goto :goto_0

    .line 416
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 417
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 418
    check-cast v0, Lcom/sec/dmc/sic/android/property/ChartProperty;

    .line 419
    .local v0, "other":Lcom/sec/dmc/sic/android/property/ChartProperty;
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 420
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 419
    if-eq v3, v4, :cond_4

    move v1, v2

    .line 421
    goto :goto_0

    .line 422
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 423
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 422
    if-eq v3, v4, :cond_5

    move v1, v2

    .line 424
    goto :goto_0

    .line 425
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 426
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 425
    if-eq v3, v4, :cond_6

    move v1, v2

    .line 427
    goto :goto_0

    .line 428
    :cond_6
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 429
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 428
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 430
    goto :goto_0

    .line 431
    :cond_7
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundAlpha:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 432
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundAlpha:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 431
    if-eq v3, v4, :cond_8

    move v1, v2

    .line 433
    goto :goto_0

    .line 434
    :cond_8
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorB:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 435
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorB:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 434
    if-eq v3, v4, :cond_9

    move v1, v2

    .line 436
    goto :goto_0

    .line 437
    :cond_9
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorG:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 438
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorG:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 437
    if-eq v3, v4, :cond_a

    move v1, v2

    .line 439
    goto/16 :goto_0

    .line 440
    :cond_a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorR:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 441
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorR:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 440
    if-eq v3, v4, :cond_b

    move v1, v2

    .line 442
    goto/16 :goto_0

    .line 443
    :cond_b
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColors:[I

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColors:[I

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 444
    goto/16 :goto_0

    .line 445
    :cond_c
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextStyles:[Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 446
    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextStyles:[Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 445
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    .line 446
    if-nez v3, :cond_d

    move v1, v2

    .line 447
    goto/16 :goto_0

    .line 448
    :cond_d
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextValues:[F

    .line 449
    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextValues:[F

    .line 448
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v3

    .line 449
    if-nez v3, :cond_e

    move v1, v2

    .line 450
    goto/16 :goto_0

    .line 451
    :cond_e
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTexts:[Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTexts:[Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    .line 452
    goto/16 :goto_0

    .line 453
    :cond_f
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundValues:[F

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundValues:[F

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 454
    goto/16 :goto_0

    .line 455
    :cond_10
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackgroundImage:Landroid/graphics/Bitmap;

    if-nez v3, :cond_11

    .line 456
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackgroundImage:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_12

    move v1, v2

    .line 457
    goto/16 :goto_0

    .line 458
    :cond_11
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackgroundImage:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackgroundImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    move v1, v2

    .line 459
    goto/16 :goto_0

    .line 460
    :cond_12
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->height:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->height:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_13

    move v1, v2

    .line 461
    goto/16 :goto_0

    .line 462
    :cond_13
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

    if-nez v3, :cond_14

    .line 463
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

    if-eqz v3, :cond_15

    move v1, v2

    .line 464
    goto/16 :goto_0

    .line 465
    :cond_14
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    move v1, v2

    .line 466
    goto/16 :goto_0

    .line 467
    :cond_15
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphSeparatorVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphSeparatorVisible:Z

    if-eq v3, v4, :cond_16

    move v1, v2

    .line 468
    goto/16 :goto_0

    .line 469
    :cond_16
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphTitleVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphTitleVisible:Z

    if-eq v3, v4, :cond_17

    move v1, v2

    .line 470
    goto/16 :goto_0

    .line 471
    :cond_17
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_graphTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_18

    .line 472
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_graphTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_19

    move v1, v2

    .line 473
    goto/16 :goto_0

    .line 474
    :cond_18
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_graphTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_graphTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_19

    move v1, v2

    .line 475
    goto/16 :goto_0

    .line 476
    :cond_19
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_maxInclueGoalExceedDataCount:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_maxInclueGoalExceedDataCount:I

    if-eq v3, v4, :cond_1a

    move v1, v2

    .line 477
    goto/16 :goto_0

    .line 478
    :cond_1a
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorColor:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorColor:I

    if-eq v3, v4, :cond_1b

    move v1, v2

    .line 479
    goto/16 :goto_0

    .line 480
    :cond_1b
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 481
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorWidth:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 480
    if-eq v3, v4, :cond_1c

    move v1, v2

    .line 482
    goto/16 :goto_0

    .line 483
    :cond_1c
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->markingLineBase:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->markingLineBase:I

    if-eq v3, v4, :cond_1d

    move v1, v2

    .line 484
    goto/16 :goto_0

    .line 485
    :cond_1d
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingBottom:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 486
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingBottom:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 485
    if-eq v3, v4, :cond_1e

    move v1, v2

    .line 487
    goto/16 :goto_0

    .line 488
    :cond_1e
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingLeft:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 489
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingLeft:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 488
    if-eq v3, v4, :cond_1f

    move v1, v2

    .line 490
    goto/16 :goto_0

    .line 491
    :cond_1f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingRight:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 492
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingRight:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 491
    if-eq v3, v4, :cond_20

    move v1, v2

    .line 493
    goto/16 :goto_0

    .line 494
    :cond_20
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingTop:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 495
    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingTop:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 494
    if-eq v3, v4, :cond_21

    move v1, v2

    .line 496
    goto/16 :goto_0

    .line 497
    :cond_21
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->posX:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->posX:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_22

    move v1, v2

    .line 498
    goto/16 :goto_0

    .line 499
    :cond_22
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->posY:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->posY:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_23

    move v1, v2

    .line 500
    goto/16 :goto_0

    .line 501
    :cond_23
    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->width:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lcom/sec/dmc/sic/android/property/ChartProperty;->width:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 502
    goto/16 :goto_0
.end method

.method public getChartBackGroundAlpha()F
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundAlpha:F

    return v0
.end method

.method public getChartBackGroundColorB()F
    .locals 1

    .prologue
    .line 249
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorB:F

    return v0
.end method

.method public getChartBackGroundColorG()F
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorG:F

    return v0
.end method

.method public getChartBackGroundColorR()F
    .locals 1

    .prologue
    .line 233
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorR:F

    return v0
.end method

.method public getGraphBackGroundColors()[I
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColors:[I

    return-object v0
.end method

.method public getGraphBackGroundTextStyles()[Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextStyles:[Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getGraphBackGroundTextValues()[F
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextValues:[F

    return-object v0
.end method

.method public getGraphBackGroundTexts()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTexts:[Ljava/lang/String;

    return-object v0
.end method

.method public getGraphBackGroundValues()[F
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundValues:[F

    return-object v0
.end method

.method public getGraphBackgroundAlpha()F
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundAlpha:F

    return v0
.end method

.method public getGraphBackgroundColorB()F
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorB:F

    return v0
.end method

.method public getGraphBackgroundColorG()F
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorG:F

    return v0
.end method

.method public getGraphBackgroundColorR()F
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorR:F

    return v0
.end method

.method public getGraphBackgroundImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackgroundImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getGraphSeparatorColor()I
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorColor:I

    return v0
.end method

.method public getGraphSeparatorVisible()Z
    .locals 1

    .prologue
    .line 513
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphSeparatorVisible:Z

    return v0
.end method

.method public getGraphSeparatorWidth()F
    .locals 1

    .prologue
    .line 328
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorWidth:F

    return v0
.end method

.method public getGraphTitleTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_graphTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getGraphTitleVisible()Z
    .locals 1

    .prologue
    .line 508
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphTitleVisible:Z

    return v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->height:F

    return v0
.end method

.method public getListener()Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

    return-object v0
.end method

.method public getMarkingLineBase()I
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->markingLineBase:I

    return v0
.end method

.method public getMaxInclueGoalExceedDataCount()I
    .locals 1

    .prologue
    .line 359
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_maxInclueGoalExceedDataCount:I

    return v0
.end method

.method public getPaddingBottom()F
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingBottom:F

    return v0
.end method

.method public getPaddingLeft()F
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingLeft:F

    return v0
.end method

.method public getPaddingRight()F
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingRight:F

    return v0
.end method

.method public getPaddingTop()F
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingTop:F

    return v0
.end method

.method public getPosX()F
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->posX:F

    return v0
.end method

.method public getPosY()F
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->posY:F

    return v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->width:F

    return v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v5, 0x4d5

    const/16 v4, 0x4cf

    const/4 v3, 0x0

    .line 368
    const/16 v0, 0x1f

    .line 369
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 370
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 371
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 372
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 373
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 374
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 375
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorB:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 376
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorG:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 377
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorR:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 378
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColors:[I

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([I)I

    move-result v6

    add-int v1, v2, v6

    .line 379
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextStyles:[Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v6

    add-int v1, v2, v6

    .line 380
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextValues:[F

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([F)I

    move-result v6

    add-int v1, v2, v6

    .line 381
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTexts:[Ljava/lang/String;

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v6

    add-int v1, v2, v6

    .line 382
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundValues:[F

    invoke-static {v6}, Ljava/util/Arrays;->hashCode([F)I

    move-result v6

    add-int v1, v2, v6

    .line 383
    mul-int/lit8 v6, v1, 0x1f

    .line 385
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackgroundImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    move v2, v3

    .line 383
    :goto_0
    add-int v1, v6, v2

    .line 387
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->height:F

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    add-int v1, v2, v6

    .line 388
    mul-int/lit8 v6, v1, 0x1f

    .line 389
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

    if-nez v2, :cond_1

    move v2, v3

    .line 388
    :goto_1
    add-int v1, v6, v2

    .line 390
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphSeparatorVisible:Z

    if-eqz v2, :cond_2

    move v2, v4

    :goto_2
    add-int v1, v6, v2

    .line 391
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphTitleVisible:Z

    if-eqz v6, :cond_3

    :goto_3
    add-int v1, v2, v4

    .line 392
    mul-int/lit8 v2, v1, 0x1f

    .line 394
    iget-object v4, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_graphTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v4, :cond_4

    .line 392
    :goto_4
    add-int v1, v2, v3

    .line 396
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_maxInclueGoalExceedDataCount:I

    add-int v1, v2, v3

    .line 397
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorColor:I

    add-int v1, v2, v3

    .line 398
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorWidth:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 399
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->markingLineBase:I

    add-int v1, v2, v3

    .line 400
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingBottom:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 401
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingLeft:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 402
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingRight:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 403
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingTop:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 404
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->posX:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 405
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->posY:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 406
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->width:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 407
    return v1

    .line 385
    :cond_0
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackgroundImage:Landroid/graphics/Bitmap;

    .line 386
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 389
    :cond_1
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_1

    :cond_2
    move v2, v5

    .line 390
    goto/16 :goto_2

    :cond_3
    move v4, v5

    .line 391
    goto/16 :goto_3

    .line 394
    :cond_4
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_graphTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 395
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v3

    goto/16 :goto_4
.end method

.method public setChartBackGroundAlpha(F)V
    .locals 0
    .param p1, "chartBackGroundAlpha"    # F

    .prologue
    .line 261
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundAlpha:F

    .line 262
    return-void
.end method

.method public setChartBackGroundColorB(F)V
    .locals 0
    .param p1, "chartBackGroundColorB"    # F

    .prologue
    .line 253
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorB:F

    .line 254
    return-void
.end method

.method public setChartBackGroundColorG(F)V
    .locals 0
    .param p1, "chartBackGroundColorG"    # F

    .prologue
    .line 245
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorG:F

    .line 246
    return-void
.end method

.method public setChartBackGroundColorR(F)V
    .locals 0
    .param p1, "chartBackGroundColorR"    # F

    .prologue
    .line 237
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorR:F

    .line 238
    return-void
.end method

.method public setChartBackgroundColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 307
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorR:F

    .line 308
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorG:F

    .line 309
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundColorB:F

    .line 310
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->chartBackGroundAlpha:F

    .line 311
    return-void
.end method

.method public setGraphBackGroundColors([I)V
    .locals 0
    .param p1, "graphBackGroundColors"    # [I

    .prologue
    .line 269
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColors:[I

    .line 270
    return-void
.end method

.method public setGraphBackGroundTextStyles([Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "graphBackGroundTextStyles"    # [Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextStyles:[Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 303
    return-void
.end method

.method public setGraphBackGroundTextValues([F)V
    .locals 0
    .param p1, "graphBackGroundTextValues"    # [F

    .prologue
    .line 293
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTextValues:[F

    .line 294
    return-void
.end method

.method public setGraphBackGroundTexts([Ljava/lang/String;)V
    .locals 0
    .param p1, "graphBackGroundTexts"    # [Ljava/lang/String;

    .prologue
    .line 285
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundTexts:[Ljava/lang/String;

    .line 286
    return-void
.end method

.method public setGraphBackGroundValues([F)V
    .locals 0
    .param p1, "graphBackGroundValues"    # [F

    .prologue
    .line 277
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundValues:[F

    .line 278
    return-void
.end method

.method public setGraphBackgroundAlpha(F)V
    .locals 0
    .param p1, "backGroundAlpha"    # F

    .prologue
    .line 214
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundAlpha:F

    .line 215
    return-void
.end method

.method public setGraphBackgroundColor(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 208
    int-to-float v0, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorR:F

    .line 209
    int-to-float v0, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorG:F

    .line 210
    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorB:F

    .line 211
    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundAlpha:F

    .line 212
    return-void
.end method

.method public setGraphBackgroundColorB(F)V
    .locals 0
    .param p1, "backGroundColorB"    # F

    .prologue
    .line 202
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorB:F

    .line 203
    return-void
.end method

.method public setGraphBackgroundColorG(F)V
    .locals 0
    .param p1, "backGroundColorG"    # F

    .prologue
    .line 196
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorG:F

    .line 197
    return-void
.end method

.method public setGraphBackgroundColorR(F)V
    .locals 0
    .param p1, "backGroundColorR"    # F

    .prologue
    .line 190
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackGroundColorR:F

    .line 191
    return-void
.end method

.method public setGraphBackgroundImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->graphBackgroundImage:Landroid/graphics/Bitmap;

    .line 230
    return-void
.end method

.method public setGraphSeparatorColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 333
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorColor:I

    .line 334
    return-void
.end method

.method public setGraphSeparatorVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 348
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphSeparatorVisible:Z

    .line 349
    return-void
.end method

.method public setGraphSeparatorWidth(F)V
    .locals 0
    .param p1, "width"    # F

    .prologue
    .line 323
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_separatorWidth:F

    .line 324
    return-void
.end method

.method public setGraphTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 314
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_graphTitleTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 315
    return-void
.end method

.method public setGraphTitleVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 343
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_GraphTitleVisible:Z

    .line 344
    return-void
.end method

.method public setHeight(F)V
    .locals 0
    .param p1, "chartHeight"    # F

    .prologue
    .line 148
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->height:F

    .line 149
    return-void
.end method

.method public setListener(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

    .prologue
    .line 355
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->listener:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

    .line 356
    return-void
.end method

.method public setMarkingLineBase(I)V
    .locals 0
    .param p1, "markingLineBase"    # I

    .prologue
    .line 222
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->markingLineBase:I

    .line 223
    return-void
.end method

.method public setMaxInclueGoalExceedDataCount(I)V
    .locals 0
    .param p1, "maxInclueGoalExceedDataCount"    # I

    .prologue
    .line 363
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->m_maxInclueGoalExceedDataCount:I

    .line 364
    return-void
.end method

.method public setPaddingBottom(F)V
    .locals 0
    .param p1, "chartPaddingBottom"    # F

    .prologue
    .line 184
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingBottom:F

    .line 185
    return-void
.end method

.method public setPaddingLeft(F)V
    .locals 0
    .param p1, "chartPaddingLeft"    # F

    .prologue
    .line 166
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingLeft:F

    .line 167
    return-void
.end method

.method public setPaddingRight(F)V
    .locals 0
    .param p1, "chartPaddingRight"    # F

    .prologue
    .line 172
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingRight:F

    .line 173
    return-void
.end method

.method public setPaddingTop(F)V
    .locals 0
    .param p1, "chartPaddingTop"    # F

    .prologue
    .line 178
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->paddingTop:F

    .line 179
    return-void
.end method

.method public setPosX(F)V
    .locals 0
    .param p1, "chartPosX"    # F

    .prologue
    .line 154
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->posX:F

    .line 155
    return-void
.end method

.method public setPosY(F)V
    .locals 0
    .param p1, "chartPosY"    # F

    .prologue
    .line 160
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->posY:F

    .line 161
    return-void
.end method

.method public setWidth(F)V
    .locals 0
    .param p1, "chartWidth"    # F

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/dmc/sic/android/property/ChartProperty;->width:F

    .line 143
    return-void
.end method
