.class public Lcom/sec/dmc/sic/android/property/GraphValueProperty;
.super Lcom/sec/dmc/sic/android/property/BaseProperty;
.source "GraphValueProperty.java"


# instance fields
.field public final GRAPH_VALUE_LAST_ON_SCREEN:I

.field public final GRAPH_VALUE_ONLY_SELECTED:I

.field public final GRAPH_VALUE_SHOW_ALL:I

.field protected mGraphValueID:I

.field protected mOffsetX:F

.field protected mOffsetY:F

.field private mPostfix:Ljava/lang/String;

.field private mPrefix:Ljava/lang/String;

.field protected mSelectedShowItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field protected mVisible:Z

.field private mVisibleOption:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mSelectedShowItemList:Ljava/util/ArrayList;

    .line 23
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->GRAPH_VALUE_SHOW_ALL:I

    .line 24
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->GRAPH_VALUE_LAST_ON_SCREEN:I

    .line 25
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->GRAPH_VALUE_ONLY_SELECTED:I

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisible:Z

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mGraphValueID:I

    .line 31
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 32
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetX:F

    .line 33
    iput v1, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetY:F

    .line 34
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setPrefix(Ljava/lang/String;)V

    .line 35
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setPostfix(Ljava/lang/String;)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mSelectedShowItemList:Ljava/util/ArrayList;

    .line 37
    iput v2, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisibleOption:I

    .line 38
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 138
    if-ne p0, p1, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v1

    .line 140
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 141
    goto :goto_0

    .line 142
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 143
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 144
    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    .line 145
    .local v0, "other":Lcom/sec/dmc/sic/android/property/GraphValueProperty;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 147
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 149
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 151
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mGraphValueID:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mGraphValueID:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 152
    goto :goto_0

    .line 153
    :cond_4
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetX:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 154
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetX:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 153
    if-eq v3, v4, :cond_5

    move v1, v2

    .line 155
    goto :goto_0

    .line 156
    :cond_5
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetY:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 157
    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetY:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 156
    if-eq v3, v4, :cond_6

    move v1, v2

    .line 158
    goto :goto_0

    .line 159
    :cond_6
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPostfix:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 160
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPostfix:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    .line 161
    goto :goto_0

    .line 162
    :cond_7
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPostfix:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPostfix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 163
    goto :goto_0

    .line 164
    :cond_8
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPrefix:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 165
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPrefix:Ljava/lang/String;

    if-eqz v3, :cond_a

    move v1, v2

    .line 166
    goto :goto_0

    .line 167
    :cond_9
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPrefix:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 168
    goto :goto_0

    .line 169
    :cond_a
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mSelectedShowItemList:Ljava/util/ArrayList;

    if-nez v3, :cond_b

    .line 170
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mSelectedShowItemList:Ljava/util/ArrayList;

    if-eqz v3, :cond_c

    move v1, v2

    .line 171
    goto :goto_0

    .line 172
    :cond_b
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mSelectedShowItemList:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mSelectedShowItemList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 173
    goto/16 :goto_0

    .line 174
    :cond_c
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v3, :cond_d

    .line 175
    iget-object v3, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-eqz v3, :cond_e

    move v1, v2

    .line 176
    goto/16 :goto_0

    .line 177
    :cond_d
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    iget-object v4, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 178
    goto/16 :goto_0

    .line 179
    :cond_e
    iget-boolean v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisible:Z

    iget-boolean v4, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisible:Z

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 180
    goto/16 :goto_0

    .line 181
    :cond_f
    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisibleOption:I

    iget v4, v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisibleOption:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 182
    goto/16 :goto_0
.end method

.method public getOffsetX()F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetX:F

    return v0
.end method

.method public getOffsetY()F
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetY:F

    return v0
.end method

.method public getPostfix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPostfix:Ljava/lang/String;

    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPrefix:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedItemList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mSelectedShowItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSeriesID()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mGraphValueID:I

    return v0
.end method

.method public getTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    return-object v0
.end method

.method public getVisible()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisible:Z

    return v0
.end method

.method public getVisibleOption()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisibleOption:I

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 114
    const/16 v0, 0x1f

    .line 115
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/dmc/sic/android/property/BaseProperty;->hashCode()I

    move-result v1

    .line 116
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    add-int/lit8 v1, v2, 0x2

    .line 117
    mul-int/lit8 v2, v1, 0x1f

    add-int/lit8 v1, v2, 0x4

    .line 118
    mul-int/lit8 v2, v1, 0x1f

    add-int/lit8 v1, v2, 0x1

    .line 119
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mGraphValueID:I

    add-int v1, v2, v4

    .line 120
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetX:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 121
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetY:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 122
    mul-int/lit8 v4, v1, 0x1f

    .line 123
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPostfix:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    .line 122
    :goto_0
    add-int v1, v4, v2

    .line 124
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPrefix:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 125
    mul-int/lit8 v4, v1, 0x1f

    .line 127
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mSelectedShowItemList:Ljava/util/ArrayList;

    if-nez v2, :cond_2

    move v2, v3

    .line 125
    :goto_2
    add-int v1, v4, v2

    .line 129
    mul-int/lit8 v2, v1, 0x1f

    .line 130
    iget-object v4, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    if-nez v4, :cond_3

    .line 129
    :goto_3
    add-int v1, v2, v3

    .line 131
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisible:Z

    if-eqz v2, :cond_4

    const/16 v2, 0x4cf

    :goto_4
    add-int v1, v3, v2

    .line 132
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisibleOption:I

    add-int v1, v2, v3

    .line 133
    return v1

    .line 123
    :cond_0
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPostfix:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 124
    :cond_1
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPrefix:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 127
    :cond_2
    iget-object v2, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mSelectedShowItemList:Ljava/util/ArrayList;

    .line 128
    invoke-virtual {v2}, Ljava/util/ArrayList;->hashCode()I

    move-result v2

    goto :goto_2

    .line 130
    :cond_3
    iget-object v3, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->hashCode()I

    move-result v3

    goto :goto_3

    .line 131
    :cond_4
    const/16 v2, 0x4d5

    goto :goto_4
.end method

.method public setOffsetX(F)V
    .locals 0
    .param p1, "offset"    # F

    .prologue
    .line 69
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetX:F

    .line 70
    return-void
.end method

.method public setOffsetY(F)V
    .locals 0
    .param p1, "offset"    # F

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mOffsetY:F

    .line 78
    return-void
.end method

.method public setPostfix(Ljava/lang/String;)V
    .locals 0
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPostfix:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 0
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mPrefix:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setSelectedShowItemList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p1, "selectedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mSelectedShowItemList:Ljava/util/ArrayList;

    .line 102
    return-void
.end method

.method public setSeriesID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mGraphValueID:I

    .line 54
    return-void
.end method

.method public setTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 0
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mTextStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 58
    return-void
.end method

.method public setVisible(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisible:Z

    .line 46
    return-void
.end method

.method public setVisibleOption(I)V
    .locals 0
    .param p1, "visibleOption"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->mVisibleOption:I

    .line 110
    return-void
.end method
