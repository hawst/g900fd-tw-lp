.class public Lcom/sec/dmc/hsl/android/stressanalyzer/StressAnalyzer;
.super Ljava/lang/Object;
.source "StressAnalyzer.java"


# static fields
.field public static final DB_TYPE_DMC:I = 0x0

.field public static final DB_TYPE_HEARTMATH:I = 0x1

.field public static final DEFAULT_MEASUREMENT_DURATION:I = 0x7530

.field public static final MAX_STRESS_SCORE:D = 3.0

.field public static final MIN_STRESS_SCORE:D = -3.0


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-string v0, "_stressanalyzer_v03_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 8
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static native addRRInterval(I)I
.end method

.method public static native calcStressScore(II)D
.end method

.method public static native getCalculatedStressIndex()I
.end method

.method public static native initializeStressAnalyzer(I)V
.end method
