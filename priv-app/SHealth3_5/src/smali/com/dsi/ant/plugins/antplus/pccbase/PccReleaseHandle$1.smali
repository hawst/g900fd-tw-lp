.class Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$1;
.super Ljava/lang/Object;
.source "PccReleaseHandle.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;)V
    .locals 0

    .prologue
    .line 34
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$1;, "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle.1;"
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V
    .locals 3
    .param p2, "resultCode"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;
    .param p3, "initialDeviceState"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$1;, "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle.1;"
    .local p1, "result":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    iget-boolean v0, v0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->isClosed:Z

    if-eqz v0, :cond_1

    .line 43
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->receivedDevice:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->access$000(Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->receivedDevice:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->access$000(Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    move-result-object v0

    const-string/jumbo v2, "received device after death"

    invoke-virtual {v0, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 58
    :cond_0
    :goto_0
    monitor-exit v1

    .line 59
    return-void

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    # setter for: Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->receivedDevice:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    invoke-static {v0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->access$002(Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    .line 55
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->resultSent:Z

    .line 56
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->mResultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->access$100(Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;->onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    goto :goto_0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
