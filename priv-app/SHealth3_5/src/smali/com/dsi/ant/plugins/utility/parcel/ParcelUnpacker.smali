.class public Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;
.super Ljava/lang/Object;
.source "ParcelUnpacker.java"


# instance fields
.field private final mEndIndex:I

.field private final mParcel:Landroid/os/Parcel;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;->mParcel:Landroid/os/Parcel;

    .line 26
    iget-object v0, p0, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;->mEndIndex:I

    .line 27
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;->mParcel:Landroid/os/Parcel;

    iget v1, p0, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;->mEndIndex:I

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 37
    return-void
.end method
