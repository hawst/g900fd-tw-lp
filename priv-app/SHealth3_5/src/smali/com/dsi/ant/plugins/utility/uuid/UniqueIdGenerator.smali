.class public Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;
.super Ljava/lang/Object;
.source "UniqueIdGenerator.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFourByteUniqueId(Landroid/content/Context;)J
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "android_id"

    invoke-static {v7, v8}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "androidId":Ljava/lang/String;
    :try_start_0
    const-string v7, "device_id.xml"

    const/4 v8, 0x4

    invoke-virtual {p0, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 76
    .local v5, "prefs":Landroid/content/SharedPreferences;
    const-string v7, "device_id"

    const/4 v8, 0x0

    invoke-interface {v5, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 77
    .local v4, "id":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 80
    invoke-static {v4}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v6

    .line 92
    .local v6, "uuid":Ljava/util/UUID;
    :goto_0
    const-wide v7, 0xffffffffL

    invoke-virtual {v6}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v9

    and-long v2, v7, v9

    .line 98
    .end local v4    # "id":Ljava/lang/String;
    .end local v5    # "prefs":Landroid/content/SharedPreferences;
    .end local v6    # "uuid":Ljava/util/UUID;
    :goto_1
    return-wide v2

    .line 84
    .restart local v4    # "id":Ljava/lang/String;
    .restart local v5    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    const-string v7, "9774d56d682e549c"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 85
    const-string/jumbo v7, "utf8"

    invoke-virtual {v0, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    invoke-static {v7}, Ljava/util/UUID;->nameUUIDFromBytes([B)Ljava/util/UUID;

    move-result-object v6

    .line 88
    .restart local v6    # "uuid":Ljava/util/UUID;
    :goto_2
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "device_id"

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 95
    .end local v4    # "id":Ljava/lang/String;
    .end local v5    # "prefs":Landroid/content/SharedPreferences;
    .end local v6    # "uuid":Ljava/util/UUID;
    :catch_0
    move-exception v1

    .line 97
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v7, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->TAG:Ljava/lang/String;

    const-string v8, "UnsupportedEncodingException trying to decode Andriod ID as utf8"

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-wide/16 v2, -0x1

    goto :goto_1

    .line 87
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v4    # "id":Ljava/lang/String;
    .restart local v5    # "prefs":Landroid/content/SharedPreferences;
    :cond_1
    :try_start_1
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    .restart local v6    # "uuid":Ljava/util/UUID;
    goto :goto_2
.end method
