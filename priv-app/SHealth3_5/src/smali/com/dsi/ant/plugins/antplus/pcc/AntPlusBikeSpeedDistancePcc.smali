.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;
.source "AntPlusBikeSpeedDistancePcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IMotionAndSpeedDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IRawSpeedAndDistanceDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedAccumulatedDistanceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedSpeedReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mCalculatedAccumulatedDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedAccumulatedDistanceReceiver;

.field mCalculatedSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedSpeedReceiver;

.field mMotionAndSpeedDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IMotionAndSpeedDataReceiver;

.field mRawSpeedAndDistanceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IRawSpeedAndDistanceDataReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 190
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 394
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;-><init>(Z)V

    return-void
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 307
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;>;"
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 8
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "skipPreferredSearch"    # Z
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZI",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;>;"
    new-instance v7, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;

    invoke-direct {v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;-><init>()V

    .line 256
    .local v7, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;
    const/4 v0, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->requestAccessBSC_helper(ZLandroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/content/Context;IIZLcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 8
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "searchProximityThreshold"    # I
    .param p3, "isSpdCadCombinedSensor"    # Z
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IIZ",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 357
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;>;"
    new-instance v7, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;

    invoke-direct {v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;-><init>()V

    .line 359
    .local v7, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;
    const/4 v0, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->requestAccessBSC_helper(ZLandroid/content/Context;IIZLcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController;
    .locals 2
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p2, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 389
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;-><init>()V

    .line 391
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;
    const/4 v1, 0x0

    invoke-static {v1, p0, p1, v0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->requestAccessBSC_Helper_AsyncScanController(ZLandroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$IBikeSpdCadAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$BikeSpdCadAsyncScanController;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 407
    const-string v0, "ANT+ Plugin: Bike Speed Distance"

    return-object v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 399
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 400
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.bikespdcad.CombinedBikeSpdCadService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 401
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 13
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 413
    iget v0, p1, Landroid/os/Message;->arg1:I

    sparse-switch v0, :sswitch_data_0

    .line 468
    :goto_0
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 471
    :cond_0
    :goto_1
    return-void

    .line 417
    :sswitch_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mCalculatedSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedSpeedReceiver;

    if-eqz v0, :cond_0

    .line 420
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    .line 421
    .local v7, "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 422
    .local v1, "estTimestamp":J
    const-string v0, "long_EventFlags"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v11, v12}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    .line 423
    .local v3, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v0, "decimal_calculatedSpeed"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v9

    check-cast v9, Ljava/math/BigDecimal;

    .line 424
    .local v9, "calculatedSpeed":Ljava/math/BigDecimal;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mCalculatedSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedSpeedReceiver;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedSpeedReceiver;->onNewCalculatedSpeedRaw(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V
    invoke-static {v0, v1, v2, v3, v9}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedSpeedReceiver;->access$000(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedSpeedReceiver;JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    goto :goto_1

    .line 430
    .end local v1    # "estTimestamp":J
    .end local v3    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v7    # "b":Landroid/os/Bundle;
    .end local v9    # "calculatedSpeed":Ljava/math/BigDecimal;
    :sswitch_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mCalculatedAccumulatedDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedAccumulatedDistanceReceiver;

    if-eqz v0, :cond_0

    .line 433
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    .line 434
    .restart local v7    # "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 435
    .restart local v1    # "estTimestamp":J
    const-string v0, "long_EventFlags"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v11, v12}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    .line 436
    .restart local v3    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v0, "decimal_calculatedAccumulatedDistance"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v8

    check-cast v8, Ljava/math/BigDecimal;

    .line 437
    .local v8, "calculatedAccumulatedDistance":Ljava/math/BigDecimal;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mCalculatedAccumulatedDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedAccumulatedDistanceReceiver;

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedAccumulatedDistanceReceiver;->onNewCalculatedAccumulatedDistanceRaw(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V
    invoke-static {v0, v1, v2, v3, v8}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedAccumulatedDistanceReceiver;->access$100(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedAccumulatedDistanceReceiver;JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    goto :goto_1

    .line 443
    .end local v1    # "estTimestamp":J
    .end local v3    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v7    # "b":Landroid/os/Bundle;
    .end local v8    # "calculatedAccumulatedDistance":Ljava/math/BigDecimal;
    :sswitch_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mRawSpeedAndDistanceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IRawSpeedAndDistanceDataReceiver;

    if-eqz v0, :cond_0

    .line 446
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    .line 447
    .restart local v7    # "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 448
    .restart local v1    # "estTimestamp":J
    const-string v0, "long_EventFlags"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v11, v12}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    .line 449
    .restart local v3    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v0, "decimal_timestampOfLastEvent"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/math/BigDecimal;

    .line 450
    .local v4, "timestampOfLastEvent":Ljava/math/BigDecimal;
    const-string v0, "long_cumulativeRevolutions"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 451
    .local v5, "cumulativeRevolutions":J
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mRawSpeedAndDistanceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IRawSpeedAndDistanceDataReceiver;

    invoke-interface/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IRawSpeedAndDistanceDataReceiver;->onNewRawSpeedAndDistanceData(JLjava/util/EnumSet;Ljava/math/BigDecimal;J)V

    goto :goto_1

    .line 457
    .end local v1    # "estTimestamp":J
    .end local v3    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v4    # "timestampOfLastEvent":Ljava/math/BigDecimal;
    .end local v5    # "cumulativeRevolutions":J
    .end local v7    # "b":Landroid/os/Bundle;
    :sswitch_3
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mMotionAndSpeedDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IMotionAndSpeedDataReceiver;

    if-eqz v0, :cond_0

    .line 460
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    .line 461
    .restart local v7    # "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 462
    .restart local v1    # "estTimestamp":J
    const-string v0, "long_EventFlags"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v11, v12}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    .line 463
    .restart local v3    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v0, "bool_isStopped"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    .line 464
    .local v10, "isStopped":Z
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mMotionAndSpeedDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IMotionAndSpeedDataReceiver;

    invoke-interface {v0, v1, v2, v3, v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IMotionAndSpeedDataReceiver;->onNewMotionAndSpeedData(JLjava/util/EnumSet;Z)V

    goto/16 :goto_0

    .line 413
    :sswitch_data_0
    .sparse-switch
        0xc9 -> :sswitch_0
        0xca -> :sswitch_1
        0xcb -> :sswitch_2
        0x12f -> :sswitch_3
    .end sparse-switch
.end method

.method public subscribeCalculatedAccumulatedDistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedAccumulatedDistanceReceiver;)V
    .locals 1
    .param p1, "CalculatedAccumulatedDistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedAccumulatedDistanceReceiver;

    .prologue
    const/16 v0, 0xca

    .line 500
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mCalculatedAccumulatedDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedAccumulatedDistanceReceiver;

    .line 501
    if-eqz p1, :cond_0

    .line 503
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->subscribeToEvent(I)Z

    .line 509
    :goto_0
    return-void

    .line 507
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeCalculatedSpeedEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedSpeedReceiver;)V
    .locals 1
    .param p1, "CalculatedSpeedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedSpeedReceiver;

    .prologue
    const/16 v0, 0xc9

    .line 481
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mCalculatedSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$CalculatedSpeedReceiver;

    .line 482
    if-eqz p1, :cond_0

    .line 484
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->subscribeToEvent(I)Z

    .line 490
    :goto_0
    return-void

    .line 488
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeMotionAndSpeedDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IMotionAndSpeedDataReceiver;)Z
    .locals 3
    .param p1, "motionAndSpeedDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IMotionAndSpeedDataReceiver;

    .prologue
    const/16 v2, 0x12f

    .line 540
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->reportedServiceVersion:I

    const/16 v1, 0x4ef0

    if-ge v0, v1, :cond_0

    .line 542
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "subscribeMotionAndSpeedDataEvent requires ANT+ Plugins Service >20208, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->reportedServiceVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const/4 v0, 0x0

    .line 554
    :goto_0
    return v0

    .line 546
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mMotionAndSpeedDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IMotionAndSpeedDataReceiver;

    .line 547
    if-eqz p1, :cond_1

    .line 549
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->subscribeToEvent(I)Z

    move-result v0

    goto :goto_0

    .line 553
    :cond_1
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->unsubscribeFromEvent(I)V

    .line 554
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subscribeRawSpeedAndDistanceDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IRawSpeedAndDistanceDataReceiver;)V
    .locals 1
    .param p1, "RawSpeedAndDistanceDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IRawSpeedAndDistanceDataReceiver;

    .prologue
    const/16 v0, 0xcb

    .line 519
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->mRawSpeedAndDistanceDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc$IRawSpeedAndDistanceDataReceiver;

    .line 520
    if-eqz p1, :cond_0

    .line 522
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->subscribeToEvent(I)Z

    .line 528
    :goto_0
    return-void

    .line 526
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikeSpeedDistancePcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
