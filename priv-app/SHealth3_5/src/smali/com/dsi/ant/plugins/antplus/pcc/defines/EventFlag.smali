.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;
.super Ljava/lang/Enum;
.source "EventFlag.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

.field public static final enum UNRECOGNIZED_FLAG_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

.field public static final enum WAS_BUFFERED:Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;


# instance fields
.field private final longValue:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 13
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    const-string v1, "UNRECOGNIZED_FLAG_PRESENT"

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->UNRECOGNIZED_FLAG_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    .line 18
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    const-string v1, "WAS_BUFFERED"

    const-wide/16 v2, 0x2

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->WAS_BUFFERED:Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    .line 8
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->UNRECOGNIZED_FLAG_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->WAS_BUFFERED:Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 0
    .param p3, "longValue"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput-wide p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->longValue:J

    .line 25
    return-void
.end method

.method public static getEventFlagsFromLong(J)Ljava/util/EnumSet;
    .locals 9
    .param p0, "longValue"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    const-class v7, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    invoke-static {v7}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 45
    .local v1, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->values()[Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v2, v0, v5

    .line 47
    .local v2, "flag":Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;
    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getLongValue()J

    move-result-wide v3

    .line 49
    .local v3, "flagValue":J
    and-long v7, v3, p0

    cmp-long v7, v7, v3

    if-nez v7, :cond_0

    .line 51
    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 52
    sub-long/2addr p0, v3

    .line 45
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 56
    .end local v2    # "flag":Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;
    .end local v3    # "flagValue":J
    :cond_1
    const-wide/16 v7, 0x0

    cmp-long v7, p0, v7

    if-eqz v7, :cond_2

    .line 57
    sget-object v7, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->UNRECOGNIZED_FLAG_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    invoke-virtual {v1, v7}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_2
    return-object v1
.end method

.method public static getLongFromEventFlags(Ljava/util/EnumSet;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-wide/16 v2, 0x0

    .line 71
    .local v2, "longValue":J
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    .line 72
    .local v0, "flag":Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getLongValue()J

    move-result-wide v4

    or-long/2addr v2, v4

    goto :goto_0

    .line 74
    .end local v0    # "flag":Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;
    :cond_0
    return-wide v2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;

    return-object v0
.end method


# virtual methods
.method public getLongValue()J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->longValue:J

    return-wide v0
.end method
