.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;
.super Ljava/lang/Enum;
.source "AntPlusBikePowerPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CrankLengthStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

.field public static final enum DEFAULT_USED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

.field public static final enum INVALID_CRANK_LENGTH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

.field public static final enum SET_AUTOMATICALLY:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

.field public static final enum SET_MANUALLY:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1067
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    const-string v1, "INVALID_CRANK_LENGTH"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->INVALID_CRANK_LENGTH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    .line 1072
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    const-string v1, "DEFAULT_USED"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->DEFAULT_USED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    .line 1077
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    const-string v1, "SET_MANUALLY"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->SET_MANUALLY:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    .line 1082
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    const-string v1, "SET_AUTOMATICALLY"

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->SET_AUTOMATICALLY:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    .line 1062
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->INVALID_CRANK_LENGTH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->DEFAULT_USED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->SET_MANUALLY:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->SET_AUTOMATICALLY:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1062
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;
    .locals 2
    .param p0, "intValue"    # I

    .prologue
    .line 1100
    packed-switch p0, :pswitch_data_0

    .line 1111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Undefined Crank Length Status"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1103
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->INVALID_CRANK_LENGTH:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    .line 1109
    :goto_0
    return-object v0

    .line 1105
    :pswitch_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->DEFAULT_USED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    goto :goto_0

    .line 1107
    :pswitch_2
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->SET_MANUALLY:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    goto :goto_0

    .line 1109
    :pswitch_3
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->SET_AUTOMATICALLY:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    goto :goto_0

    .line 1100
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1062
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;
    .locals 1

    .prologue
    .line 1062
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 1090
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->ordinal()I

    move-result v0

    return v0
.end method
