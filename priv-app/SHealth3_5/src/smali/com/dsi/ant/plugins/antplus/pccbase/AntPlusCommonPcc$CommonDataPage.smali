.class public final enum Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
.super Ljava/lang/Enum;
.source "AntPlusCommonPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CommonDataPage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

.field public static final enum BATTERY_STATUS:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

.field public static final enum COMMAND_STATUS:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

.field public static final enum MANUFACTURER_IDENTIFICATION:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

.field public static final enum PRODUCT_INFORMATION:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

.field public static final enum UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;


# instance fields
.field private intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    const-string v1, "MANUFACTURER_IDENTIFICATION"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v3, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->MANUFACTURER_IDENTIFICATION:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    .line 36
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    const-string v1, "PRODUCT_INFORMATION"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v4, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->PRODUCT_INFORMATION:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    .line 41
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    const-string v1, "BATTERY_STATUS"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v5, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->BATTERY_STATUS:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    .line 46
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    const-string v1, "COMMAND_STATUS"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v6, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->COMMAND_STATUS:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    .line 51
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    const-string v1, "UNRECOGNIZED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    .line 26
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->MANUFACTURER_IDENTIFICATION:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->PRODUCT_INFORMATION:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->BATTERY_STATUS:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->COMMAND_STATUS:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    aput-object v1, v0, v7

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->intValue:I

    .line 58
    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
    .locals 6
    .param p0, "intValue"    # I

    .prologue
    .line 77
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->values()[Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 79
    .local v3, "source":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->getIntValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 85
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
    :goto_1
    return-object v3

    .line 77
    .restart local v3    # "source":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 83
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
    :cond_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    .line 84
    .local v4, "unrecognized":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
    iput p0, v4, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->intValue:I

    move-object v3, v4

    .line 85
    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$CommonDataPage;->intValue:I

    return v0
.end method
