.class Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;
.super Landroid/os/Handler;
.source "AsyncScanController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScanResultHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;",
        ">",
        "Landroid/os/Handler;"
    }
.end annotation


# instance fields
.field controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<TT;>;"
        }
    .end annotation
.end field

.field retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 468
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler<TT;>;"
    .local p1, "retPccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    .local p2, "controller":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 469
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    .line 470
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    .line 471
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 476
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler<TT;>;"
    iget v2, p1, Landroid/os/Message;->what:I

    .line 477
    .local v2, "resultCode":I
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 478
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Async scan controller rcv result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    sparse-switch v2, :sswitch_data_0

    .line 514
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    move-result-object v1

    .line 515
    .local v1, "code":Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    if-ne v1, v4, :cond_1

    .line 516
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RequestAccess failed: Unrecognized return code (need app lib upgrade): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->getIntValue()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "!!!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :goto_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    # invokes: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->reportScanFailure(I)V
    invoke-static {v4, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$800(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;I)V

    .line 520
    .end local v1    # "code":Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;
    :goto_1
    :sswitch_0
    return-void

    .line 482
    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 483
    .local v3, "retInfo":Landroid/os/Bundle;
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    const-string/jumbo v4, "uuid_AccessToken"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/util/UUID;

    iput-object v4, v5, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mAccessToken:Ljava/util/UUID;

    .line 484
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;->retPccObject:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    const-string/jumbo v4, "msgr_PluginComm"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/os/Messenger;

    iput-object v4, v5, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgr:Landroid/os/Messenger;

    .line 485
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->stateLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$100(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 487
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    const/4 v6, 0x1

    # setter for: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isRunning:Z
    invoke-static {v4, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$602(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;Z)Z

    .line 488
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->isClosed:Z
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$700(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 490
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->closeScanController()V

    .line 492
    :cond_0
    monitor-exit v5

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 497
    .end local v3    # "retInfo":Landroid/os/Bundle;
    :sswitch_2
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->sendResultToReceiver(Landroid/os/Bundle;)V

    goto :goto_1

    .line 502
    :sswitch_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 503
    .local v0, "b":Landroid/os/Bundle;
    const-string/jumbo v4, "string_DependencyPackageName"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->lastMissingDependencyPkgName:Ljava/lang/String;

    .line 504
    const-string/jumbo v4, "string_DependencyName"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->lastMissingDependencyName:Ljava/lang/String;

    .line 505
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "requestAccess failed, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->lastMissingDependencyPkgName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " not installed."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$ScanResultHandler;->controller:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    # invokes: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->reportScanFailure(I)V
    invoke-static {v4, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$800(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;I)V

    goto/16 :goto_1

    .line 518
    .end local v0    # "b":Landroid/os/Bundle;
    .restart local v1    # "code":Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;
    :cond_1
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RequestAccess failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 479
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7 -> :sswitch_0
        -0x5 -> :sswitch_3
        0x0 -> :sswitch_1
        0x2 -> :sswitch_2
    .end sparse-switch
.end method
