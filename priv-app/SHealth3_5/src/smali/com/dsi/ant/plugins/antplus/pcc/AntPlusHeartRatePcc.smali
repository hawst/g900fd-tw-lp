.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;
.source "AntPlusHeartRatePcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$ICalculatedRrIntervalReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IPage4AddtDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IHeartRateDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mCalculatedRrIntervalReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$ICalculatedRrIntervalReceiver;

.field mCompat:Lcom/dsi/ant/plugins/internal/compatibility/LegacyHeartRateCompat;

.field mHeartRateDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IHeartRateDataReceiver;

.field mPage4AddtDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IPage4AddtDataReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 525
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;-><init>()V

    return-void
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;>;"
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "skipPreferredSearch"    # Z
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZI",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 390
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;>;"
    new-instance v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;

    invoke-direct {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;-><init>()V

    .local v4, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 392
    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->requestAccess_Helper_SearchActivity(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "searchProximityThreshold"    # I
    .param p4, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 488
    .local p3, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;>;"
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;

    invoke-direct {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;-><init>()V

    .local v3, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 490
    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->requestAccess_Helper_AsyncSearchByDevNumber(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .locals 2
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p2, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 519
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;-><init>()V

    .line 521
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;
    invoke-static {p0, p1, v0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->requestAccess_Helper_AsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 538
    const-string v0, "ANT+ Plugin: Heart Rate"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 530
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 531
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.heartrate.HeartRateService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 532
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 23
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 544
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 613
    :pswitch_0
    invoke-super/range {p0 .. p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusLegacyCommonPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 616
    :cond_0
    :goto_0
    return-void

    .line 548
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mHeartRateDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IHeartRateDataReceiver;

    if-eqz v1, :cond_0

    .line 551
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v22

    .line 552
    .local v22, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 553
    .local v2, "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 554
    .local v4, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_computedHeartRate"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 555
    .local v5, "computedHeartRate":I
    const-string v1, "long_heartBeatCounter"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 557
    .local v6, "heartBeatCounter":J
    const-string v1, "decimal_timestampOfLastEvent"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v8

    check-cast v8, Ljava/math/BigDecimal;

    .line 560
    .local v8, "heartBeatEventTime":Ljava/math/BigDecimal;
    const-string v1, "int_dataState"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 561
    const-string v1, "int_dataState"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    move-result-object v9

    .line 565
    .local v9, "dataState":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mHeartRateDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IHeartRateDataReceiver;

    invoke-interface/range {v1 .. v9}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IHeartRateDataReceiver;->onNewHeartRateData(JLjava/util/EnumSet;IJLjava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;)V

    goto :goto_0

    .line 563
    .end local v9    # "dataState":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    :cond_1
    sget-object v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;->LIVE_DATA:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;

    .restart local v9    # "dataState":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    goto :goto_1

    .line 572
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "computedHeartRate":I
    .end local v6    # "heartBeatCounter":J
    .end local v8    # "heartBeatEventTime":Ljava/math/BigDecimal;
    .end local v9    # "dataState":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$DataState;
    .end local v22    # "b":Landroid/os/Bundle;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mCompat:Lcom/dsi/ant/plugins/internal/compatibility/LegacyHeartRateCompat;

    if-eqz v1, :cond_0

    .line 575
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v22

    .line 576
    .restart local v22    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 577
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 578
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_timestampOfLastEvent"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v8

    check-cast v8, Ljava/math/BigDecimal;

    .line 580
    .restart local v8    # "heartBeatEventTime":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mCompat:Lcom/dsi/ant/plugins/internal/compatibility/LegacyHeartRateCompat;

    invoke-virtual {v1, v2, v3, v4, v8}, Lcom/dsi/ant/plugins/internal/compatibility/LegacyHeartRateCompat;->onNewHeartRateDataTimestamp(JLjava/util/EnumSet;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 586
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v8    # "heartBeatEventTime":Ljava/math/BigDecimal;
    .end local v22    # "b":Landroid/os/Bundle;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mPage4AddtDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IPage4AddtDataReceiver;

    if-eqz v1, :cond_0

    .line 589
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v22

    .line 590
    .restart local v22    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 591
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 592
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_manufacturerSpecificByte"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 593
    .local v14, "manufacturerSpecificByte":I
    const-string v1, "decimal_timestampOfPreviousToLastHeartBeatEvent"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v15

    check-cast v15, Ljava/math/BigDecimal;

    .line 594
    .local v15, "previousHeartBeatEventTime":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mPage4AddtDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IPage4AddtDataReceiver;

    move-wide v11, v2

    move-object v13, v4

    invoke-interface/range {v10 .. v15}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IPage4AddtDataReceiver;->onNewPage4AddtData(JLjava/util/EnumSet;ILjava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 600
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v14    # "manufacturerSpecificByte":I
    .end local v15    # "previousHeartBeatEventTime":Ljava/math/BigDecimal;
    .end local v22    # "b":Landroid/os/Bundle;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mCalculatedRrIntervalReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$ICalculatedRrIntervalReceiver;

    if-eqz v1, :cond_0

    .line 603
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v22

    .line 604
    .restart local v22    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 605
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 606
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_calculatedRrInterval"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v20

    check-cast v20, Ljava/math/BigDecimal;

    .line 607
    .local v20, "calculatedRrInterval":Ljava/math/BigDecimal;
    const-string v1, "int_rrFlag"

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;

    move-result-object v21

    .line 608
    .local v21, "rrFlag":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mCalculatedRrIntervalReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$ICalculatedRrIntervalReceiver;

    move-object/from16 v16, v0

    move-wide/from16 v17, v2

    move-object/from16 v19, v4

    invoke-interface/range {v16 .. v21}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$ICalculatedRrIntervalReceiver;->onNewCalculatedRrInterval(JLjava/util/EnumSet;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$RrFlag;)V

    goto/16 :goto_0

    .line 544
    nop

    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public subscribeCalculatedRrIntervalEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$ICalculatedRrIntervalReceiver;)Z
    .locals 3
    .param p1, "CalculatedRrIntervalReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$ICalculatedRrIntervalReceiver;

    .prologue
    const/16 v2, 0xcf

    .line 681
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->reportedServiceVersion:I

    const/16 v1, 0x4ef0

    if-ge v0, v1, :cond_0

    .line 683
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "subscribeCalculatedRrIntervalEvent requires ANT+ Plugins Service >20208, installed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->reportedServiceVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    const/4 v0, 0x0

    .line 695
    :goto_0
    return v0

    .line 687
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mCalculatedRrIntervalReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$ICalculatedRrIntervalReceiver;

    .line 688
    if-eqz p1, :cond_1

    .line 690
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->subscribeToEvent(I)Z

    move-result v0

    goto :goto_0

    .line 694
    :cond_1
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->unsubscribeFromEvent(I)V

    .line 695
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subscribeHeartRateDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IHeartRateDataReceiver;)V
    .locals 4
    .param p1, "HeartRateDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IHeartRateDataReceiver;

    .prologue
    const/16 v3, 0xca

    const/16 v2, 0xc9

    .line 626
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->reportedServiceVersion:I

    const/16 v1, 0x4ef0

    if-ge v0, v1, :cond_0

    .line 628
    if-eqz p1, :cond_1

    .line 630
    new-instance v0, Lcom/dsi/ant/plugins/internal/compatibility/LegacyHeartRateCompat;

    invoke-direct {v0, p1}, Lcom/dsi/ant/plugins/internal/compatibility/LegacyHeartRateCompat;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IHeartRateDataReceiver;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mCompat:Lcom/dsi/ant/plugins/internal/compatibility/LegacyHeartRateCompat;

    .line 631
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->subscribeToEvent(I)Z

    .line 638
    :goto_0
    iget-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mCompat:Lcom/dsi/ant/plugins/internal/compatibility/LegacyHeartRateCompat;

    .line 641
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mHeartRateDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IHeartRateDataReceiver;

    .line 643
    if-eqz p1, :cond_2

    .line 645
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->subscribeToEvent(I)Z

    .line 651
    :goto_1
    return-void

    .line 635
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mCompat:Lcom/dsi/ant/plugins/internal/compatibility/LegacyHeartRateCompat;

    .line 636
    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->unsubscribeFromEvent(I)V

    goto :goto_0

    .line 649
    :cond_2
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->unsubscribeFromEvent(I)V

    goto :goto_1
.end method

.method public subscribePage4AddtDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IPage4AddtDataReceiver;)V
    .locals 1
    .param p1, "Page4AddtDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IPage4AddtDataReceiver;

    .prologue
    const/16 v0, 0xcb

    .line 660
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->mPage4AddtDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc$IPage4AddtDataReceiver;

    .line 661
    if-eqz p1, :cond_0

    .line 663
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->subscribeToEvent(I)Z

    .line 669
    :goto_0
    return-void

    .line 667
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusHeartRatePcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
