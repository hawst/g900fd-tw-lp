.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
.super Ljava/lang/Enum;
.source "AntPlusBikePowerPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AutoZeroStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

.field public static final enum INVALID:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

.field public static final enum NOT_SUPPORTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

.field public static final enum OFF:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

.field public static final enum ON:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

.field public static final enum UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

.field public static final enum UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;


# instance fields
.field private intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 777
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->OFF:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    .line 782
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    const-string v1, "ON"

    invoke-direct {v0, v1, v5, v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->ON:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    .line 787
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    const-string v1, "NOT_SUPPORTED"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v6, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->NOT_SUPPORTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    .line 792
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    const-string v1, "INVALID"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->INVALID:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    .line 797
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v8, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    .line 802
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    const-string v1, "UNRECOGNIZED"

    const/4 v2, 0x5

    const/4 v3, -0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    .line 772
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->OFF:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->ON:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->NOT_SUPPORTED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->INVALID:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 807
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 808
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->intValue:I

    .line 809
    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
    .locals 6
    .param p0, "intValue"    # I

    .prologue
    .line 827
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 829
    .local v3, "status":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->getIntValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 835
    .end local v3    # "status":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
    :goto_1
    return-object v3

    .line 827
    .restart local v3    # "status":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 833
    .end local v3    # "status":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
    :cond_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    .line 834
    .local v4, "unrecognized":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
    iput p0, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->intValue:I

    move-object v3, v4

    .line 835
    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 772
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
    .locals 1

    .prologue
    .line 772
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 817
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->intValue:I

    return v0
.end method
