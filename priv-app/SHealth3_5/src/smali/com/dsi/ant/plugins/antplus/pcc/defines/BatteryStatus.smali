.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
.super Ljava/lang/Enum;
.source "BatteryStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

.field public static final enum CRITICAL:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

.field public static final enum GOOD:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

.field public static final enum INVALID:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

.field public static final enum LOW:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

.field public static final enum NEW:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

.field public static final enum OK:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

.field public static final enum UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;


# instance fields
.field private intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 11
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    const-string v1, "NEW"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->NEW:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    .line 16
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    const-string v1, "GOOD"

    invoke-direct {v0, v1, v4, v5}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->GOOD:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    .line 21
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    const-string v1, "OK"

    invoke-direct {v0, v1, v5, v6}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->OK:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    .line 26
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v6, v7}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->LOW:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    .line 31
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    const-string v1, "CRITICAL"

    invoke-direct {v0, v1, v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->CRITICAL:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    .line 36
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    const-string v1, "INVALID"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v8, v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->INVALID:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    .line 41
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    const-string v1, "UNRECOGNIZED"

    const/4 v2, 0x6

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    .line 6
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    const/4 v1, 0x0

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->NEW:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    aput-object v2, v0, v1

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->GOOD:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->OK:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->LOW:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->CRITICAL:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->INVALID:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->intValue:I

    .line 48
    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    .locals 6
    .param p0, "intValue"    # I

    .prologue
    .line 66
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->values()[Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 68
    .local v3, "status":Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->getIntValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 75
    .end local v3    # "status":Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    :goto_1
    return-object v3

    .line 66
    .restart local v3    # "status":Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 72
    .end local v3    # "status":Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    :cond_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    .line 73
    .local v4, "unrecognized":Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    iput p0, v4, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->intValue:I

    move-object v3, v4

    .line 75
    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/defines/BatteryStatus;->intValue:I

    return v0
.end method
