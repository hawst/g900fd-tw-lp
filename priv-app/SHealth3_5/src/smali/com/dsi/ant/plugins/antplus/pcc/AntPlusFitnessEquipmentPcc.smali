.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$INordicSkierDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IClimberDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRowerDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IEllipticalDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITreadmillDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mBikeDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;

.field private mBikeMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;

.field mClimberDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IClimberDataReceiver;

.field private mClimberMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;

.field mEllipticalDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IEllipticalDataReceiver;

.field private mEllipticalMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;

.field mFitnessEquipmentStateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;

.field mGeneralFitnessEquipmentDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;

.field mGeneralMetabolicDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;

.field mGeneralSettingsReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;

.field final mIsTrainer:Z

.field mLapOccuredReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;

.field mNordicSkierDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$INordicSkierDataReceiver;

.field private mNordicSkierMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;

.field mRowerDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRowerDataReceiver;

.field private mRowerMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;

.field mTreadmillDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITreadmillDataReceiver;

.field private mTreadmillMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "isFEC"    # Z

    .prologue
    .line 1165
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;-><init>()V

    .line 1361
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTreadmillMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;

    .line 1400
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mEllipticalMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;

    .line 1439
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBikeMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;

    .line 1476
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRowerMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;

    .line 1515
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mClimberMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;

    .line 1554
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mNordicSkierMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;

    .line 1166
    iput-boolean p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mIsTrainer:Z

    .line 1167
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    return-void
.end method

.method public static requestNewOpenAccess(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .locals 2
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p2, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;
    .param p3, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1160
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "This feature requires the members-only plugin lib. See readme and http://www.thisisant.com/business/go-ant/levels-and-benefits/"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static requestNewOpenAccess(Landroid/app/Activity;Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p4, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1004
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;>;"
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestNewOpenAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestNewOpenAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 2
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "skipPreferredSearch"    # Z
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p6, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZI",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1070
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;>;"
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "This feature requires the members-only plugin lib. See readme and http://www.thisisant.com/business/go-ant/levels-and-benefits/"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static requestNewOpenAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 2
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "searchProximityThreshold"    # I
    .param p4, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p5, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1122
    .local p3, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;>;"
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "This feature requires the members-only plugin lib. See readme and http://www.thisisant.com/business/go-ant/levels-and-benefits/"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static requestNewPersonalSessionAccess(Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;I)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p2, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p3, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .param p4, "channelDeviceNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            "I)",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;>;"
    const/4 v5, 0x0

    .line 849
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, v5

    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestNewPersonalSessionAccess(Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;ILcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestNewPersonalSessionAccess(Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;ILcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;[Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 11
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p2, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p3, "feStateReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;
    .param p4, "channelDeviceNumber"    # I
    .param p5, "settings"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;
    .param p6, "selectedFiles"    # [Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;",
            "[",
            "Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 907
    .local p1, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;>;"
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 908
    .local v8, "b":Landroid/os/Bundle;
    const-string v2, "int_RequestAccessMode"

    const/16 v3, 0x12c

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 909
    const-string v2, "int_ChannelDeviceId"

    invoke-virtual {v8, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 911
    move-object/from16 v1, p5

    .line 912
    .local v1, "cachedSettings":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;
    if-nez p5, :cond_0

    .line 913
    new-instance v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;

    .end local v1    # "cachedSettings":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;
    const-string v2, "Invalid"

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings$Gender;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings$Gender;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;-><init>(Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings$Gender;SFF)V

    .line 916
    .restart local v1    # "cachedSettings":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;
    :cond_0
    if-nez p4, :cond_2

    .line 920
    :try_start_0
    const-string v2, "com.dsi.ant.plugins.antplus"

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v10

    .line 921
    .local v10, "pluginContext":Landroid/content/Context;
    invoke-static {v10}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getFourByteUniqueId(Landroid/content/Context;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;->setSerialNumber(J)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 934
    .end local v10    # "pluginContext":Landroid/content/Context;
    :goto_0
    const-string/jumbo v2, "parcelable_settings"

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;->toFitFile()Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 936
    if-eqz p6, :cond_1

    move-object/from16 v0, p6

    array-length v2, v0

    if-eqz v2, :cond_1

    .line 937
    const-string v2, "arrayParcelable_fitFiles"

    move-object/from16 v0, p6

    invoke-virtual {v8, v2, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 941
    :cond_1
    new-instance v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;

    const/4 v2, 0x0

    invoke-direct {v4, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;-><init>(Z)V

    .line 942
    .local v4, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    iput-object p3, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mFitnessEquipmentStateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;

    .line 944
    new-instance v5, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;

    invoke-direct {v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;-><init>()V

    move-object v2, p0

    move-object v3, v8

    move-object v6, p1

    move-object v7, p2

    invoke-static/range {v2 .. v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->requestAccess_Helper_Main(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v2

    return-object v2

    .line 923
    .end local v4    # "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
    :catch_0
    move-exception v9

    .line 925
    .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v9}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 931
    .end local v9    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    int-to-long v2, p4

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$Settings;->setSerialNumber(J)V

    goto :goto_0
.end method


# virtual methods
.method public getBikeMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;
    .locals 1

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBikeMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$BikeMethods;

    return-object v0
.end method

.method public getClimberMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;
    .locals 1

    .prologue
    .line 1524
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mClimberMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ClimberMethods;

    return-object v0
.end method

.method public getEllipticalMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;
    .locals 1

    .prologue
    .line 1409
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mEllipticalMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EllipticalMethods;

    return-object v0
.end method

.method public getNordicSkierMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;
    .locals 1

    .prologue
    .line 1563
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mNordicSkierMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$NordicSkierMethods;

    return-object v0
.end method

.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1180
    const-string v0, "ANT+ Plugin: Fitness Equipment"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mIsTrainer:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4ef1

    .line 42
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x2774

    goto :goto_0
.end method

.method public getRowerMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;
    .locals 1

    .prologue
    .line 1485
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRowerMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$RowerMethods;

    return-object v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1172
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1173
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.fitnessequipment.FitnessEquipmentService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1174
    return-object v0
.end method

.method public getTreadmillMethods()Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;
    .locals 1

    .prologue
    .line 1370
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTreadmillMethods:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$TreadmillMethods;

    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 65
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 1186
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 1355
    invoke-super/range {p0 .. p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 1358
    :cond_0
    :goto_0
    return-void

    .line 1190
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mLapOccuredReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;

    if-eqz v1, :cond_0

    .line 1193
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v63

    .line 1194
    .local v63, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1195
    .local v2, "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1196
    .local v4, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_lapCount"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v64

    .line 1197
    .local v64, "lapCount":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mLapOccuredReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;

    move/from16 v0, v64

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;->onNewLapOccured(JLjava/util/EnumSet;I)V

    goto :goto_0

    .line 1203
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v63    # "b":Landroid/os/Bundle;
    .end local v64    # "lapCount":I
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mFitnessEquipmentStateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;

    if-eqz v1, :cond_0

    .line 1206
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v63

    .line 1207
    .restart local v63    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1208
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1209
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_equipmentTypeCode"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;

    move-result-object v5

    .line 1210
    .local v5, "equipmentTypeCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;
    const-string v1, "int_stateCode"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;

    move-result-object v6

    .line 1211
    .local v6, "stateCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mFitnessEquipmentStateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;

    invoke-interface/range {v1 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IFitnessEquipmentStateReceiver;->onNewFitnessEquipmentState(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;)V

    goto :goto_0

    .line 1217
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "equipmentTypeCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentType;
    .end local v6    # "stateCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$EquipmentState;
    .end local v63    # "b":Landroid/os/Bundle;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralFitnessEquipmentDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;

    if-eqz v1, :cond_0

    .line 1220
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v63

    .line 1221
    .restart local v63    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1222
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1223
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_elapsedTime"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v11

    check-cast v11, Ljava/math/BigDecimal;

    .line 1224
    .local v11, "elapsedTime":Ljava/math/BigDecimal;
    const-string v1, "long_cumulativeDistance"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 1225
    .local v12, "cumulativeDistance":J
    const-string v1, "decimal_instantaneousSpeed"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v14

    check-cast v14, Ljava/math/BigDecimal;

    .line 1227
    .local v14, "instantaneousSpeed":Ljava/math/BigDecimal;
    const-string v1, "bool_virtualInstantaneousSpeed"

    const/4 v7, 0x0

    move-object/from16 v0, v63

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v15

    .line 1228
    .local v15, "virtualSpeed":Z
    const-string v1, "int_instantaneousHeartRate"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v16

    .line 1229
    .local v16, "instantaneousHeartRate":I
    const-string v1, "int_heartRateDataSourceCode"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    move-result-object v17

    .line 1230
    .local v17, "heartRateDataSourceCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralFitnessEquipmentDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;

    move-wide v8, v2

    move-object v10, v4

    invoke-interface/range {v7 .. v17}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;->onNewGeneralFitnessEquipmentData(JLjava/util/EnumSet;Ljava/math/BigDecimal;JLjava/math/BigDecimal;ZILcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;)V

    goto/16 :goto_0

    .line 1236
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v11    # "elapsedTime":Ljava/math/BigDecimal;
    .end local v12    # "cumulativeDistance":J
    .end local v14    # "instantaneousSpeed":Ljava/math/BigDecimal;
    .end local v15    # "virtualSpeed":Z
    .end local v16    # "instantaneousHeartRate":I
    .end local v17    # "heartRateDataSourceCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    .end local v63    # "b":Landroid/os/Bundle;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralSettingsReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;

    if-eqz v1, :cond_0

    .line 1239
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v63

    .line 1240
    .restart local v63    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1241
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1242
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_cycleLength"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v22

    check-cast v22, Ljava/math/BigDecimal;

    .line 1243
    .local v22, "cycleLength":Ljava/math/BigDecimal;
    const-string v1, "decimal_inclinePercentage"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v23

    check-cast v23, Ljava/math/BigDecimal;

    .line 1244
    .local v23, "inclinePercentage":Ljava/math/BigDecimal;
    const-string v1, "int_resistanceLevel"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v24

    .line 1245
    .local v24, "resistanceLevel":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralSettingsReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;

    move-object/from16 v18, v0

    move-wide/from16 v19, v2

    move-object/from16 v21, v4

    invoke-interface/range {v18 .. v24}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;->onNewGeneralSettings(JLjava/util/EnumSet;Ljava/math/BigDecimal;Ljava/math/BigDecimal;I)V

    goto/16 :goto_0

    .line 1251
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v22    # "cycleLength":Ljava/math/BigDecimal;
    .end local v23    # "inclinePercentage":Ljava/math/BigDecimal;
    .end local v24    # "resistanceLevel":I
    .end local v63    # "b":Landroid/os/Bundle;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralMetabolicDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;

    if-eqz v1, :cond_0

    .line 1254
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v63

    .line 1255
    .restart local v63    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1256
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1257
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_instantaneousMetabolicEquivalents"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v29

    check-cast v29, Ljava/math/BigDecimal;

    .line 1258
    .local v29, "instantaneousMetabolicEquivalents":Ljava/math/BigDecimal;
    const-string v1, "decimal_instantaneousCaloricBurn"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v30

    check-cast v30, Ljava/math/BigDecimal;

    .line 1259
    .local v30, "instantaneousCaloricBurn":Ljava/math/BigDecimal;
    const-string v1, "long_cumulativeCalories"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v31

    .line 1260
    .local v31, "cumulativeCalories":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralMetabolicDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;

    move-object/from16 v25, v0

    move-wide/from16 v26, v2

    move-object/from16 v28, v4

    invoke-interface/range {v25 .. v32}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;->onNewGeneralMetabolicData(JLjava/util/EnumSet;Ljava/math/BigDecimal;Ljava/math/BigDecimal;J)V

    goto/16 :goto_0

    .line 1266
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v29    # "instantaneousMetabolicEquivalents":Ljava/math/BigDecimal;
    .end local v30    # "instantaneousCaloricBurn":Ljava/math/BigDecimal;
    .end local v31    # "cumulativeCalories":J
    .end local v63    # "b":Landroid/os/Bundle;
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTreadmillDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITreadmillDataReceiver;

    if-eqz v1, :cond_0

    .line 1269
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v63

    .line 1270
    .restart local v63    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1271
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1272
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 1273
    .local v37, "instantaneousCadence":I
    const-string v1, "decimal_cumulativeNegVertDistance"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v38

    check-cast v38, Ljava/math/BigDecimal;

    .line 1274
    .local v38, "cumulativeNegVertDistance":Ljava/math/BigDecimal;
    const-string v1, "decimal_cumulativePosVertDistance"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v39

    check-cast v39, Ljava/math/BigDecimal;

    .line 1275
    .local v39, "cumulativePosVertDistance":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mTreadmillDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITreadmillDataReceiver;

    move-object/from16 v33, v0

    move-wide/from16 v34, v2

    move-object/from16 v36, v4

    invoke-interface/range {v33 .. v39}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ITreadmillDataReceiver;->onNewTreadmillData(JLjava/util/EnumSet;ILjava/math/BigDecimal;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 1281
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v38    # "cumulativeNegVertDistance":Ljava/math/BigDecimal;
    .end local v39    # "cumulativePosVertDistance":Ljava/math/BigDecimal;
    .end local v63    # "b":Landroid/os/Bundle;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mEllipticalDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IEllipticalDataReceiver;

    if-eqz v1, :cond_0

    .line 1284
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v63

    .line 1285
    .restart local v63    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1286
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1287
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "decimal_cumulativePosVertDistance"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v39

    check-cast v39, Ljava/math/BigDecimal;

    .line 1288
    .restart local v39    # "cumulativePosVertDistance":Ljava/math/BigDecimal;
    const-string v1, "long_cumulativeStrides"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v45

    .line 1289
    .local v45, "cumulativeStrides":J
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 1290
    .restart local v37    # "instantaneousCadence":I
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v48

    .line 1291
    .local v48, "instantaneousPower":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mEllipticalDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IEllipticalDataReceiver;

    move-object/from16 v40, v0

    move-wide/from16 v41, v2

    move-object/from16 v43, v4

    move-object/from16 v44, v39

    move/from16 v47, v37

    invoke-interface/range {v40 .. v48}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IEllipticalDataReceiver;->onNewEllipticalData(JLjava/util/EnumSet;Ljava/math/BigDecimal;JII)V

    goto/16 :goto_0

    .line 1297
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v39    # "cumulativePosVertDistance":Ljava/math/BigDecimal;
    .end local v45    # "cumulativeStrides":J
    .end local v48    # "instantaneousPower":I
    .end local v63    # "b":Landroid/os/Bundle;
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBikeDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;

    if-eqz v1, :cond_0

    .line 1300
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v63

    .line 1301
    .restart local v63    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1302
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1303
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 1304
    .restart local v37    # "instantaneousCadence":I
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v48

    .line 1305
    .restart local v48    # "instantaneousPower":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mBikeDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;

    move-object/from16 v49, v0

    move-wide/from16 v50, v2

    move-object/from16 v52, v4

    move/from16 v53, v37

    move/from16 v54, v48

    invoke-interface/range {v49 .. v54}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IBikeDataReceiver;->onNewBikeData(JLjava/util/EnumSet;II)V

    goto/16 :goto_0

    .line 1311
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v48    # "instantaneousPower":I
    .end local v63    # "b":Landroid/os/Bundle;
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRowerDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRowerDataReceiver;

    if-eqz v1, :cond_0

    .line 1314
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v63

    .line 1315
    .restart local v63    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1316
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1317
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_cumulativeStrokes"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v53

    .line 1318
    .local v53, "cumulativeStrokes":J
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 1319
    .restart local v37    # "instantaneousCadence":I
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v48

    .line 1320
    .restart local v48    # "instantaneousPower":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mRowerDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRowerDataReceiver;

    move-object/from16 v49, v0

    move-wide/from16 v50, v2

    move-object/from16 v52, v4

    move/from16 v55, v37

    move/from16 v56, v48

    invoke-interface/range {v49 .. v56}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IRowerDataReceiver;->onNewRowerData(JLjava/util/EnumSet;JII)V

    goto/16 :goto_0

    .line 1326
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v48    # "instantaneousPower":I
    .end local v53    # "cumulativeStrokes":J
    .end local v63    # "b":Landroid/os/Bundle;
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mClimberDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IClimberDataReceiver;

    if-eqz v1, :cond_0

    .line 1329
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v63

    .line 1330
    .restart local v63    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1331
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1332
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_cumulativeStrideCycles"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v59

    .line 1333
    .local v59, "cumulativeStrideCycles":J
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 1334
    .restart local v37    # "instantaneousCadence":I
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v48

    .line 1335
    .restart local v48    # "instantaneousPower":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mClimberDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IClimberDataReceiver;

    move-object/from16 v55, v0

    move-wide/from16 v56, v2

    move-object/from16 v58, v4

    move/from16 v61, v37

    move/from16 v62, v48

    invoke-interface/range {v55 .. v62}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IClimberDataReceiver;->onNewClimberData(JLjava/util/EnumSet;JII)V

    goto/16 :goto_0

    .line 1341
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v37    # "instantaneousCadence":I
    .end local v48    # "instantaneousPower":I
    .end local v59    # "cumulativeStrideCycles":J
    .end local v63    # "b":Landroid/os/Bundle;
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mNordicSkierDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$INordicSkierDataReceiver;

    if-eqz v1, :cond_0

    .line 1344
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v63

    .line 1345
    .restart local v63    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1346
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1347
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_cumulativeStrides"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v45

    .line 1348
    .restart local v45    # "cumulativeStrides":J
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 1349
    .restart local v37    # "instantaneousCadence":I
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v63

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v48

    .line 1350
    .restart local v48    # "instantaneousPower":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mNordicSkierDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$INordicSkierDataReceiver;

    move-object/from16 v41, v0

    move-wide/from16 v42, v2

    move-object/from16 v44, v4

    move/from16 v47, v37

    invoke-interface/range {v41 .. v48}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$INordicSkierDataReceiver;->onNewNordicSkierData(JLjava/util/EnumSet;JII)V

    goto/16 :goto_0

    .line 1186
    nop

    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public subscribeGeneralFitnessEquipmentDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;)V
    .locals 1
    .param p1, "GeneralFitnessEquipmentDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;

    .prologue
    const/16 v0, 0xcb

    .line 1620
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralFitnessEquipmentDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralFitnessEquipmentDataReceiver;

    .line 1621
    if-eqz p1, :cond_0

    .line 1623
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 1629
    :goto_0
    return-void

    .line 1627
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeGeneralMetabolicDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;)V
    .locals 1
    .param p1, "GeneralMetabolicDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;

    .prologue
    const/16 v0, 0xcd

    .line 1658
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralMetabolicDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralMetabolicDataReceiver;

    .line 1659
    if-eqz p1, :cond_0

    .line 1661
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 1667
    :goto_0
    return-void

    .line 1665
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeGeneralSettingsEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;)V
    .locals 1
    .param p1, "GeneralSettingsReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;

    .prologue
    const/16 v0, 0xcc

    .line 1639
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mGeneralSettingsReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$IGeneralSettingsReceiver;

    .line 1640
    if-eqz p1, :cond_0

    .line 1642
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 1648
    :goto_0
    return-void

    .line 1646
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeLapOccuredEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;)V
    .locals 1
    .param p1, "LapOccuredReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;

    .prologue
    const/16 v0, 0xc9

    .line 1601
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->mLapOccuredReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$ILapOccuredReceiver;

    .line 1602
    if-eqz p1, :cond_0

    .line 1604
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->subscribeToEvent(I)Z

    .line 1610
    :goto_0
    return-void

    .line 1608
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
