.class Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$2;
.super Ljava/lang/Object;
.source "PccReleaseHandle.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private deadStateSent:Z

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;)V
    .locals 1

    .prologue
    .line 64
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$2;, "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle.2;"
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$2;->deadStateSent:Z

    return-void
.end method


# virtual methods
.method public onDeviceStateChange(Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V
    .locals 2
    .param p1, "newDeviceState"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    .prologue
    .line 70
    .local p0, "this":Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$2;, "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle.2;"
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->isActive()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$2;->deadStateSent:Z

    if-nez v0, :cond_1

    .line 76
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->DEAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    invoke-virtual {v0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$2;->deadStateSent:Z

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->mStateReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->access$200(Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;->onDeviceStateChange(Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    .line 81
    :cond_1
    monitor-exit v1

    .line 82
    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
