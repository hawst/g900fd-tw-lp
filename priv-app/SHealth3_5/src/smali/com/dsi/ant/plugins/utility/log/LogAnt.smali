.class public final Lcom/dsi/ant/plugins/utility/log/LogAnt;
.super Ljava/lang/Object;
.source "LogAnt.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;
    }
.end annotation


# static fields
.field public static final DEBUG_LEVEL_DEFAULT:I

.field private static sDebugLevel:I

.field private static sVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->WARNING:Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    sput v0, Lcom/dsi/ant/plugins/utility/log/LogAnt;->DEBUG_LEVEL_DEFAULT:I

    .line 25
    sget v0, Lcom/dsi/ant/plugins/utility/log/LogAnt;->DEBUG_LEVEL_DEFAULT:I

    sput v0, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sDebugLevel:I

    .line 30
    const-string/jumbo v0, "v.NTST: "

    sput-object v0, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sVersion:Ljava/lang/String;

    return-void
.end method

.method public static final d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 119
    sget v0, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sDebugLevel:I

    sget-object v1, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->DEBUG:Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_0
    return-void
.end method

.method public static final e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 89
    sget v0, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sDebugLevel:I

    sget-object v1, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->ERROR:Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    return-void
.end method

.method public static getDebugLevel(Landroid/content/Context;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, -0x1

    .line 62
    const-string v2, "ant_plugins_logging"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 64
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "debug_level"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 65
    .local v0, "debugLevel":I
    if-ne v0, v4, :cond_0

    .line 68
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "debug_level"

    sget v4, Lcom/dsi/ant/plugins/utility/log/LogAnt;->DEBUG_LEVEL_DEFAULT:I

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 74
    :goto_0
    sget v2, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sDebugLevel:I

    return v2

    .line 72
    :cond_0
    sput v0, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sDebugLevel:I

    goto :goto_0
.end method

.method public static final i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 113
    sget v0, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sDebugLevel:I

    sget-object v1, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->INFO:Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_0
    return-void
.end method

.method public static setVersion(Ljava/lang/String;)V
    .locals 2
    .param p0, "version"    # Ljava/lang/String;

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sVersion:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public static final v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 125
    sget v0, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sDebugLevel:I

    sget-object v1, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->VERBOSE:Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :cond_0
    return-void
.end method

.method public static final w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 101
    sget v0, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sDebugLevel:I

    sget-object v1, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->WARNING:Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/dsi/ant/plugins/utility/log/LogAnt;->sVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_0
    return-void
.end method
