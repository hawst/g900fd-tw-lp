.class public abstract Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
.super Ljava/lang/Object;
.source "AntPluginPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler_AsyncSearchByDevNumber;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler_UI;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$StandardReleaseHandle;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;,
        Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;
    }
.end annotation


# static fields
.field public static final PATH_ANTPLUS_PLUGINS_PKG:Ljava/lang/String; = "com.dsi.ant.plugins.antplus"

.field private static final TAG:Ljava/lang/String;

.field static volatile lastMissingDependencyName:Ljava/lang/String;

.field static volatile lastMissingDependencyPkgName:Ljava/lang/String;


# instance fields
.field deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

.field deviceInitializedLatch:Ljava/util/concurrent/CountDownLatch;

.field isInitialized:Z

.field private volatile isReleased:Z

.field mAccessToken:Ljava/util/UUID;

.field mCachedState:Ljava/lang/Integer;

.field private mCurrentCmdThread:Ljava/lang/Thread;

.field private mIsPluginServiceBound:Z

.field mOwnerContext:Landroid/content/Context;

.field private final mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

.field mPluginCommMsgExch:Ljava/util/concurrent/Exchanger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Exchanger",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field mPluginCommProcessingBarrier:Ljava/util/concurrent/CyclicBarrier;

.field volatile mPluginEventHandler:Landroid/os/Handler;

.field mPluginEventHandlerCb:Landroid/os/Handler$Callback;

.field mPluginEventHandlerThread:Landroid/os/HandlerThread;

.field mPluginMsgHandler:Landroid/os/Handler$Callback;

.field mPluginMsgHandlerThread:Landroid/os/HandlerThread;

.field mPluginMsgr:Landroid/os/Messenger;

.field private mPluginServiceBindChange_LOCK:Ljava/lang/Object;

.field volatile mReleaseHandle:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<*>;"
        }
    .end annotation
.end field

.field mStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

.field protected reportedServiceVersion:I

.field serviceBindConn:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    .line 172
    const-string v0, ""

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->lastMissingDependencyPkgName:Ljava/lang/String;

    .line 173
    const-string v0, ""

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->lastMissingDependencyName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 688
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PluginPCCMsgHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgHandlerThread:Landroid/os/HandlerThread;

    .line 689
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgHandler:Landroid/os/Handler$Callback;

    .line 734
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PluginPCCEventHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginEventHandlerThread:Landroid/os/HandlerThread;

    .line 736
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$3;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$3;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginEventHandlerCb:Landroid/os/Handler$Callback;

    .line 807
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 808
    new-instance v0, Ljava/util/concurrent/Exchanger;

    invoke-direct {v0}, Ljava/util/concurrent/Exchanger;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommMsgExch:Ljava/util/concurrent/Exchanger;

    .line 809
    new-instance v0, Ljava/util/concurrent/CyclicBarrier;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/concurrent/CyclicBarrier;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommProcessingBarrier:Ljava/util/concurrent/CyclicBarrier;

    .line 810
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->isInitialized:Z

    .line 811
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->deviceInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 814
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mCachedState:Ljava/lang/Integer;

    .line 1216
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->isReleased:Z

    .line 1265
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mIsPluginServiceBound:Z

    .line 1266
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginServiceBindChange_LOCK:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->notifyBindAndRequestFailed(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method private bindPluginService(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "it"    # Landroid/content/Intent;
    .param p2, "b"    # Landroid/os/Bundle;

    .prologue
    .line 1270
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginServiceBindChange_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1272
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mIsPluginServiceBound:Z

    if-nez v0, :cond_0

    .line 1274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mIsPluginServiceBound:Z

    .line 1276
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mOwnerContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->serviceBindConn:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, p1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1279
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    const-string v2, "Binding to plugin failed"

    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->notifyBindAndRequestFailed(Landroid/os/Bundle;)V

    .line 1283
    :cond_0
    monitor-exit v1

    .line 1284
    return-void

    .line 1283
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstalledPluginsVersionNumber(Landroid/content/Context;)I
    .locals 6
    .param p0, "currentContext"    # Landroid/content/Context;

    .prologue
    .line 114
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 115
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v2

    .line 116
    .local v2, "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInfo;

    .line 118
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v5, "com.dsi.ant.plugins.antplus"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 120
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v4, v4, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v4, :cond_1

    .line 121
    const/4 v4, -0x2

    .line 125
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v4

    .line 122
    .restart local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    iget v4, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    goto :goto_0

    .line 125
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_2
    const/4 v4, -0x1

    goto :goto_0
.end method

.method public static getInstalledPluginsVersionString(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "currentContext"    # Landroid/content/Context;

    .prologue
    .line 135
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 136
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v2

    .line 137
    .local v2, "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInfo;

    .line 139
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v5, "com.dsi.ant.plugins.antplus"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 141
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 144
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-object v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static getMissingDependencyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->lastMissingDependencyName:Ljava/lang/String;

    return-object v0
.end method

.method public static getMissingDependencyPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->lastMissingDependencyPkgName:Ljava/lang/String;

    return-object v0
.end method

.method private getPluginMsgReceiver()Landroid/os/Messenger;
    .locals 4

    .prologue
    .line 679
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginEventHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 680
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginEventHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginEventHandlerCb:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginEventHandler:Landroid/os/Handler;

    .line 681
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 682
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgHandler:Landroid/os/Handler$Callback;

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method

.method private notifyBindAndRequestFailed(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "requestAccessParams"    # Landroid/os/Bundle;

    .prologue
    .line 1124
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->closePluginConnection()V

    .line 1127
    const-string/jumbo v3, "msgr_ReqAccResultReceiver"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/os/Messenger;

    .line 1129
    .local v2, "resultMsgr":Landroid/os/Messenger;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1130
    .local v1, "msgErr":Landroid/os/Message;
    const/4 v3, -0x4

    iput v3, v1, Landroid/os/Message;->what:I

    .line 1134
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1140
    :goto_0
    return-void

    .line 1135
    :catch_0
    move-exception v0

    .line 1137
    .local v0, "e1":Landroid/os/RemoteException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    const-string v4, "Remote exception sending failure msg to client"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected static requestAccess_Helper_AsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .locals 2
    .param p0, "bindingContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p3, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;",
            ">(",
            "Landroid/content/Context;",
            "ITT;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 435
    .local p2, "retPccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    invoke-direct {v0, p3, p2}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)V

    .line 437
    .local v0, "controller":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {p0, p1, v1, p2, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->requestAsyncScan_Helper_SubMain(Landroid/content/Context;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    move-result-object v1

    return-object v1
.end method

.method protected static requestAccess_Helper_AsyncSearchByDevNumber(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "bindingContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;",
            ">(",
            "Landroid/content/Context;",
            "IITT;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<TT;>;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 420
    .local p3, "retPccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<TT;>;"
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 421
    .local v1, "b":Landroid/os/Bundle;
    const-string v0, "int_RequestAccessMode"

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 422
    const-string v0, "int_AntDeviceID"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 423
    const-string v0, "int_ProximityBin"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 425
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler_AsyncSearchByDevNumber;

    invoke-direct {v3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler_AsyncSearchByDevNumber;-><init>()V

    move-object v0, p0

    move-object v2, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->requestAccess_Helper_Main(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method protected static requestAccess_Helper_Main(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 4
    .param p0, "bindingContext"    # Landroid/content/Context;
    .param p1, "reqParams"    # Landroid/os/Bundle;
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;",
            ">(",
            "Landroid/content/Context;",
            "Landroid/os/Bundle;",
            "TT;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler",
            "<TT;>;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<TT;>;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 499
    .local p2, "retPccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    .local p3, "reqAccessResultHandler":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler<TT;>;"
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<TT;>;"
    if-eqz p4, :cond_0

    if-nez p5, :cond_2

    .line 501
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid argument: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p4, :cond_1

    const-string/jumbo v1, "resultReceiver "

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is null "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const-string/jumbo v1, "stateReceiver "

    goto :goto_0

    .line 505
    :cond_2
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$StandardReleaseHandle;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2, p4, p5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$StandardReleaseHandle;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)V

    .line 506
    .local v0, "ret":Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;, "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle<TT;>;"
    iput-object v0, p2, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mReleaseHandle:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    .line 507
    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->stateSink:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    iput-object v1, p2, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    .line 508
    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->resultSink:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;

    invoke-virtual {p3, p2, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;->setReturnInfo(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;)V

    .line 509
    invoke-static {p0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->requestAccess_Helper_SubMain(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Landroid/os/Handler;)V

    .line 510
    return-object v0
.end method

.method protected static requestAccess_Helper_SearchActivity(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "foregroundActivity"    # Landroid/app/Activity;
    .param p1, "bindingContext"    # Landroid/content/Context;
    .param p2, "skipPreferredSearch"    # Z
    .param p3, "searchProximityThreshold"    # I
    .param p6, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;",
            ">(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZITT;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<TT;>;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 385
    .local p4, "retPccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    .local p5, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<TT;>;"
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 386
    .local v1, "b":Landroid/os/Bundle;
    const-string v0, "int_RequestAccessMode"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 387
    const-string v0, "b_ForceManualSelect"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 388
    const-string v0, "int_ProximityBin"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 390
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler_UI;

    invoke-direct {v3, p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler_UI;-><init>(Landroid/app/Activity;)V

    move-object v0, p1

    move-object v2, p4

    move-object v4, p5

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->requestAccess_Helper_Main(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method protected static requestAccess_Helper_SubMain(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Landroid/os/Handler;)V
    .locals 5
    .param p0, "bindingContext"    # Landroid/content/Context;
    .param p1, "reqParams"    # Landroid/os/Bundle;
    .param p3, "resultHandler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;",
            ">(",
            "Landroid/content/Context;",
            "Landroid/os/Bundle;",
            "TT;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 545
    .local p2, "retPccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    if-nez p3, :cond_0

    .line 546
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "resultHandler passed from client was null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 549
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 550
    .local v2, "appNamePkg":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 551
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 553
    .local v1, "appNameLabel":Ljava/lang/String;
    const-string/jumbo v3, "str_ApplicationNamePackage"

    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    const-string/jumbo v3, "str_ApplicationNameTitle"

    invoke-virtual {p1, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    invoke-virtual {p2, p0, p1, p3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->bindAndRequest(Landroid/content/Context;Landroid/os/Bundle;Landroid/os/Handler;)V

    .line 559
    return-void
.end method

.method protected static requestAsyncScan_Helper_SubMain(Landroid/content/Context;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .locals 3
    .param p0, "bindingContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p2, "reqParams"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;",
            ">(",
            "Landroid/content/Context;",
            "I",
            "Landroid/os/Bundle;",
            "TT;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<TT;>;)",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 520
    .local p3, "retPccObject":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;, "TT;"
    .local p4, "controller":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    invoke-static {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->getInstalledPluginsVersionNumber(Landroid/content/Context;)I

    move-result v1

    const/16 v2, 0x2a30

    if-ge v1, v2, :cond_0

    .line 522
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    const-string v2, "Binding to plugin failed, version requirement not met for async scan controller mode"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    invoke-virtual {p3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->getServiceBindIntent()Landroid/content/Intent;

    move-result-object v0

    .line 524
    .local v0, "it":Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->lastMissingDependencyPkgName:Ljava/lang/String;

    .line 525
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->getPluginPrintableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " minimum v.10800"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->lastMissingDependencyName:Ljava/lang/String;

    .line 526
    iget-object v1, p4, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->scanResultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->DEPENDENCY_NOT_INSTALLED:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-interface {v1, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;->onSearchStopped(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    .line 527
    const/4 p4, 0x0

    .line 535
    .end local v0    # "it":Landroid/content/Intent;
    .end local p4    # "controller":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    :goto_0
    return-object p4

    .line 531
    .restart local p4    # "controller":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;, "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController<TT;>;"
    :cond_0
    const-string v1, "int_RequestAccessMode"

    const/4 v2, 0x2

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 532
    const-string v1, "int_ProximityBin"

    invoke-virtual {p2, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 533
    invoke-virtual {p4}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;->getScanResultHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-static {p0, p2, p3, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->requestAccess_Helper_SubMain(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method private sendDependencyNotInstalledMessage(Landroid/os/Messenger;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "resultMsgr"    # Landroid/os/Messenger;
    .param p2, "dpndcyPkgName"    # Ljava/lang/String;
    .param p3, "dpndcyDisplayName"    # Ljava/lang/String;

    .prologue
    .line 662
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 663
    .local v1, "msgErr":Landroid/os/Message;
    const/4 v3, -0x5

    iput v3, v1, Landroid/os/Message;->what:I

    .line 664
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 665
    .local v2, "s":Landroid/os/Bundle;
    const-string/jumbo v3, "string_DependencyPackageName"

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    const-string/jumbo v3, "string_DependencyName"

    invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 670
    :try_start_0
    invoke-virtual {p1, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 675
    :goto_0
    return-void

    .line 671
    :catch_0
    move-exception v0

    .line 673
    .local v0, "e1":Landroid/os/RemoteException;
    const-string v3, "Remote exception sending plugin \'dependency not installed\' msg to client"

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sendPluginCommandInternal(Landroid/os/Message;)Landroid/os/Message;
    .locals 12
    .param p1, "cmdMsg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    .line 919
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    monitor-enter v7

    .line 921
    const/4 v4, 0x0

    .line 922
    .local v4, "success":Z
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mCurrentCmdThread:Ljava/lang/Thread;

    .line 924
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgr:Landroid/os/Messenger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_9

    .line 933
    :try_start_1
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    const-wide/16 v8, 0x1b58

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v8, v9, v10}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 934
    new-instance v6, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v6}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v6
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 935
    :catch_0
    move-exception v1

    .line 937
    .local v1, "e1":Ljava/lang/InterruptedException;
    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InterruptedException obtaining mPluginCommLock in sendPluginCommand on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 939
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V

    .line 940
    monitor-exit v7

    move-object v3, v5

    .line 1053
    .end local v1    # "e1":Ljava/lang/InterruptedException;
    :goto_0
    return-object v3

    .line 941
    :catch_1
    move-exception v1

    .line 943
    .local v1, "e1":Ljava/util/concurrent/TimeoutException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "TimeoutException obtaining mPluginCommLock in sendPluginCommand on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 945
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v3, v5

    goto :goto_0

    .line 950
    .end local v1    # "e1":Ljava/util/concurrent/TimeoutException;
    :cond_0
    :try_start_3
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgr:Landroid/os/Messenger;

    invoke-virtual {v6, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_10
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 965
    :goto_1
    :try_start_4
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommMsgExch:Ljava/util/concurrent/Exchanger;

    const/4 v8, 0x0

    const-wide/16 v9, 0x5

    sget-object v11, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v8, v9, v10, v11}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Message;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_10
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 984
    .local v2, "response":Landroid/os/Message;
    :try_start_5
    iget v6, v2, Landroid/os/Message;->what:I

    iget v8, p1, Landroid/os/Message;->what:I

    if-ne v6, v8, :cond_4

    .line 991
    invoke-static {v2}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_10
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v3

    .line 993
    .local v3, "ret":Landroid/os/Message;
    const/4 v4, 0x1

    .line 1025
    :try_start_6
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1031
    if-eqz v4, :cond_1

    .line 1035
    :try_start_7
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommProcessingBarrier:Ljava/util/concurrent/CyclicBarrier;

    invoke-virtual {v6}, Ljava/util/concurrent/CyclicBarrier;->await()I
    :try_end_7
    .catch Ljava/util/concurrent/BrokenBarrierException; {:try_start_7 .. :try_end_7} :catch_8
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1046
    :cond_1
    :try_start_8
    monitor-exit v7

    goto :goto_0

    .line 1054
    .end local v2    # "response":Landroid/os/Message;
    .end local v3    # "ret":Landroid/os/Message;
    :catchall_0
    move-exception v5

    monitor-exit v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v5

    .line 966
    :catch_2
    move-exception v1

    .line 968
    .local v1, "e1":Ljava/lang/InterruptedException;
    :try_start_9
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InterruptedException in sendPluginCommand (at mPluginCommMsgExch.exchange()) on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 970
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_10
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1025
    :try_start_a
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1031
    if-eqz v4, :cond_2

    .line 1035
    :try_start_b
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommProcessingBarrier:Ljava/util/concurrent/CyclicBarrier;

    invoke-virtual {v6}, Ljava/util/concurrent/CyclicBarrier;->await()I
    :try_end_b
    .catch Ljava/util/concurrent/BrokenBarrierException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 1046
    :cond_2
    :try_start_c
    monitor-exit v7

    move-object v3, v5

    goto :goto_0

    .line 1036
    :catch_3
    move-exception v0

    .line 1038
    .local v0, "e":Ljava/util/concurrent/BrokenBarrierException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BrokenBarrierException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1040
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1041
    .end local v0    # "e":Ljava/util/concurrent/BrokenBarrierException;
    :catch_4
    move-exception v0

    .line 1043
    .local v0, "e":Ljava/lang/InterruptedException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InterruptedException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1045
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V

    .line 1046
    monitor-exit v7
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-object v3, v5

    goto/16 :goto_0

    .line 972
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "e1":Ljava/lang/InterruptedException;
    :catch_5
    move-exception v0

    .line 974
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_d
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "TimeoutException in sendPluginCommand (at mPluginCommMsgExch.exchange()) on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_10
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 1025
    :try_start_e
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 1031
    if-eqz v4, :cond_3

    .line 1035
    :try_start_f
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommProcessingBarrier:Ljava/util/concurrent/CyclicBarrier;

    invoke-virtual {v6}, Ljava/util/concurrent/CyclicBarrier;->await()I
    :try_end_f
    .catch Ljava/util/concurrent/BrokenBarrierException; {:try_start_f .. :try_end_f} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 1046
    :cond_3
    :try_start_10
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1036
    :catch_6
    move-exception v0

    .line 1038
    .local v0, "e":Ljava/util/concurrent/BrokenBarrierException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BrokenBarrierException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1040
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1041
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    :catch_7
    move-exception v0

    .line 1043
    .local v0, "e":Ljava/lang/InterruptedException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InterruptedException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1045
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V

    .line 1046
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1036
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v2    # "response":Landroid/os/Message;
    .restart local v3    # "ret":Landroid/os/Message;
    :catch_8
    move-exception v0

    .line 1038
    .local v0, "e":Ljava/util/concurrent/BrokenBarrierException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BrokenBarrierException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1040
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1041
    .end local v0    # "e":Ljava/util/concurrent/BrokenBarrierException;
    :catch_9
    move-exception v0

    .line 1043
    .local v0, "e":Ljava/lang/InterruptedException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InterruptedException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1045
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V

    .line 1046
    monitor-exit v7
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    move-object v3, v5

    goto/16 :goto_0

    .line 998
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v3    # "ret":Landroid/os/Message;
    :cond_4
    :try_start_11
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleNonCmdPluginMessage(Landroid/os/Message;)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_11} :catch_10
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 1002
    :try_start_12
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommProcessingBarrier:Ljava/util/concurrent/CyclicBarrier;

    invoke-virtual {v6}, Ljava/util/concurrent/CyclicBarrier;->await()I
    :try_end_12
    .catch Ljava/util/concurrent/BrokenBarrierException; {:try_start_12 .. :try_end_12} :catch_a
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_12} :catch_d
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_12} :catch_10
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    goto/16 :goto_1

    .line 1003
    :catch_a
    move-exception v0

    .line 1005
    .local v0, "e":Ljava/util/concurrent/BrokenBarrierException;
    :try_start_13
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BrokenBarrierException in sendPluginCommand (at non-success mPluginCommProcessingBarrier) on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_13} :catch_10
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 1025
    :try_start_14
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 1031
    if-eqz v4, :cond_5

    .line 1035
    :try_start_15
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommProcessingBarrier:Ljava/util/concurrent/CyclicBarrier;

    invoke-virtual {v6}, Ljava/util/concurrent/CyclicBarrier;->await()I
    :try_end_15
    .catch Ljava/util/concurrent/BrokenBarrierException; {:try_start_15 .. :try_end_15} :catch_b
    .catch Ljava/lang/InterruptedException; {:try_start_15 .. :try_end_15} :catch_c
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 1046
    :cond_5
    :try_start_16
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1036
    :catch_b
    move-exception v0

    .line 1038
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BrokenBarrierException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1040
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1041
    :catch_c
    move-exception v0

    .line 1043
    .local v0, "e":Ljava/lang/InterruptedException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InterruptedException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1045
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V

    .line 1046
    monitor-exit v7
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    move-object v3, v5

    goto/16 :goto_0

    .line 1008
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_d
    move-exception v0

    .line 1010
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    :try_start_17
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InterruptedException in sendPluginCommand (at non-success mPluginCommProcessingBarrier) on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1012
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_17} :catch_10
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    .line 1025
    :try_start_18
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    .line 1031
    if-eqz v4, :cond_6

    .line 1035
    :try_start_19
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommProcessingBarrier:Ljava/util/concurrent/CyclicBarrier;

    invoke-virtual {v6}, Ljava/util/concurrent/CyclicBarrier;->await()I
    :try_end_19
    .catch Ljava/util/concurrent/BrokenBarrierException; {:try_start_19 .. :try_end_19} :catch_e
    .catch Ljava/lang/InterruptedException; {:try_start_19 .. :try_end_19} :catch_f
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    .line 1046
    :cond_6
    :try_start_1a
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1036
    :catch_e
    move-exception v0

    .line 1038
    .local v0, "e":Ljava/util/concurrent/BrokenBarrierException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BrokenBarrierException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1040
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1041
    .local v0, "e":Ljava/lang/InterruptedException;
    :catch_f
    move-exception v0

    .line 1043
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InterruptedException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1045
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V

    .line 1046
    monitor-exit v7
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    move-object v3, v5

    goto/16 :goto_0

    .line 1017
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "response":Landroid/os/Message;
    :catch_10
    move-exception v0

    .line 1019
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1b
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "RemoteException sending message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " to plugin"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    .line 1025
    :try_start_1c
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    .line 1031
    if-eqz v4, :cond_7

    .line 1035
    :try_start_1d
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommProcessingBarrier:Ljava/util/concurrent/CyclicBarrier;

    invoke-virtual {v6}, Ljava/util/concurrent/CyclicBarrier;->await()I
    :try_end_1d
    .catch Ljava/util/concurrent/BrokenBarrierException; {:try_start_1d .. :try_end_1d} :catch_11
    .catch Ljava/lang/InterruptedException; {:try_start_1d .. :try_end_1d} :catch_12
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    .line 1046
    :cond_7
    :try_start_1e
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1036
    :catch_11
    move-exception v0

    .line 1038
    .local v0, "e":Ljava/util/concurrent/BrokenBarrierException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BrokenBarrierException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1040
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1041
    .local v0, "e":Landroid/os/RemoteException;
    :catch_12
    move-exception v0

    .line 1043
    .local v0, "e":Ljava/lang/InterruptedException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InterruptedException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1045
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V

    .line 1046
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1025
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v6

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    .line 1031
    if-eqz v4, :cond_8

    .line 1035
    :try_start_1f
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommProcessingBarrier:Ljava/util/concurrent/CyclicBarrier;

    invoke-virtual {v8}, Ljava/util/concurrent/CyclicBarrier;->await()I
    :try_end_1f
    .catch Ljava/util/concurrent/BrokenBarrierException; {:try_start_1f .. :try_end_1f} :catch_13
    .catch Ljava/lang/InterruptedException; {:try_start_1f .. :try_end_1f} :catch_14
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    .line 1046
    :cond_8
    :try_start_20
    throw v6

    .line 1036
    :catch_13
    move-exception v0

    .line 1038
    .local v0, "e":Ljava/util/concurrent/BrokenBarrierException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BrokenBarrierException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1040
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1041
    .end local v0    # "e":Ljava/util/concurrent/BrokenBarrierException;
    :catch_14
    move-exception v0

    .line 1043
    .local v0, "e":Ljava/lang/InterruptedException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InterruptedException in sendPluginCommand finally on message "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 1045
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V

    .line 1046
    monitor-exit v7

    move-object v3, v5

    goto/16 :goto_0

    .line 1053
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_9
    monitor-exit v7
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    move-object v3, v5

    goto/16 :goto_0
.end method

.method private sendReleaseCommand(I)V
    .locals 5
    .param p1, "releaseCode"    # I

    .prologue
    .line 1200
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    monitor-enter v2

    .line 1204
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgr:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 1205
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgr:Landroid/os/Messenger;

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->createCmdMsg(ILandroid/os/Bundle;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1212
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 1213
    return-void

    .line 1207
    :catch_0
    move-exception v0

    .line 1210
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteException, unable to cleanly release (cmd "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1212
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static startPluginManagerActivity(Landroid/app/Activity;)Z
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 159
    invoke-static {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->getInstalledPluginsVersionNumber(Landroid/content/Context;)I

    move-result v1

    if-lez v1, :cond_0

    .line 161
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 162
    .local v0, "it":Landroid/content/Intent;
    const-string v1, "com.dsi.ant.plugins.antplus"

    const-string v2, "com.dsi.ant.plugins.antplus.utility.db.Activity_PluginMgrDashboard"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 164
    const/4 v1, 0x1

    .line 168
    .end local v0    # "it":Landroid/content/Intent;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private unbindPluginService()V
    .locals 5

    .prologue
    .line 1288
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginServiceBindChange_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 1290
    :try_start_0
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mIsPluginServiceBound:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 1294
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mOwnerContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->serviceBindConn:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1301
    :goto_0
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mIsPluginServiceBound:Z

    .line 1303
    :cond_0
    monitor-exit v2

    .line 1304
    return-void

    .line 1296
    :catch_0
    move-exception v0

    .line 1298
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected error unbinding service, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1303
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method


# virtual methods
.method protected bindAndRequest(Landroid/content/Context;Landroid/os/Bundle;Landroid/os/Handler;)V
    .locals 14
    .param p1, "bindingContext"    # Landroid/content/Context;
    .param p2, "b"    # Landroid/os/Bundle;
    .param p3, "resultHandler"    # Landroid/os/Handler;

    .prologue
    .line 567
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mOwnerContext:Landroid/content/Context;

    .line 569
    new-instance v1, Landroid/os/Messenger;

    move-object/from16 v0, p3

    invoke-direct {v1, v0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    .line 571
    .local v1, "commChannel":Landroid/os/Messenger;
    const-string/jumbo v11, "msgr_PluginMsgHandler"

    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->getPluginMsgReceiver()Landroid/os/Messenger;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 572
    const-string/jumbo v11, "msgr_ReqAccResultReceiver"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 574
    const-string v11, "BBD30100"

    invoke-static {v11}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->setVersion(Ljava/lang/String;)V

    .line 578
    :try_start_0
    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mOwnerContext:Landroid/content/Context;

    const-string v12, "com.dsi.ant.plugins.antplus"

    const/4 v13, 0x4

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v7

    .line 579
    .local v7, "pluginContext":Landroid/content/Context;
    invoke-static {v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->getDebugLevel(Landroid/content/Context;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 586
    .end local v7    # "pluginContext":Landroid/content/Context;
    :goto_0
    const-string v11, "int_PluginLibVersion"

    const/16 v12, 0x7594

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 587
    const-string/jumbo v11, "string_PluginLibVersion"

    const-string v12, "3.1.0"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    const-string/jumbo v11, "more"

    const/4 v12, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 590
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->getServiceBindIntent()Landroid/content/Intent;

    move-result-object v4

    .line 593
    .local v4, "it":Landroid/content/Intent;
    const/4 v10, 0x0

    .line 594
    .local v10, "targetService":Landroid/content/pm/PackageInfo;
    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mOwnerContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 595
    .local v8, "pm":Landroid/content/pm/PackageManager;
    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v6

    .line 596
    .local v6, "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/PackageInfo;

    .line 598
    .local v5, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v11, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 600
    move-object v10, v5

    .line 604
    .end local v5    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    if-nez v10, :cond_2

    .line 606
    sget-object v11, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    const-string v12, "Binding to plugin failed, not installed"

    invoke-static {v11, v12}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    const-string/jumbo v11, "msgr_ReqAccResultReceiver"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/os/Messenger;

    .line 608
    .local v9, "resultMsgr":Landroid/os/Messenger;
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "ANT+ Plugins Service"

    invoke-direct {p0, v9, v11, v12}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->sendDependencyNotInstalledMessage(Landroid/os/Messenger;Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    .end local v9    # "resultMsgr":Landroid/os/Messenger;
    :goto_1
    return-void

    .line 581
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "it":Landroid/content/Intent;
    .end local v6    # "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v8    # "pm":Landroid/content/pm/PackageManager;
    .end local v10    # "targetService":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v2

    .line 583
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v11, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unable to configure logging, plugins package not found: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 611
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "it":Landroid/content/Intent;
    .restart local v6    # "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v8    # "pm":Landroid/content/pm/PackageManager;
    .restart local v10    # "targetService":Landroid/content/pm/PackageInfo;
    :cond_2
    iget v11, v10, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->getRequiredServiceVersionForBind()I

    move-result v12

    if-ge v11, v12, :cond_3

    .line 613
    sget-object v11, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    const-string v12, "Binding to plugin failed, version requirement not met"

    invoke-static {v11, v12}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    const-string/jumbo v11, "msgr_ReqAccResultReceiver"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/os/Messenger;

    .line 615
    .restart local v9    # "resultMsgr":Landroid/os/Messenger;
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "ANT+ Plugins Service minimum v."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->getRequiredServiceVersionForBind()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v9, v11, v12}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->sendDependencyNotInstalledMessage(Landroid/os/Messenger;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 619
    .end local v9    # "resultMsgr":Landroid/os/Messenger;
    :cond_3
    new-instance v11, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;

    move-object/from16 v0, p2

    invoke-direct {v11, p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Landroid/os/Bundle;)V

    iput-object v11, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->serviceBindConn:Landroid/content/ServiceConnection;

    .line 657
    move-object/from16 v0, p2

    invoke-direct {p0, v4, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->bindPluginService(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method closePluginConnection()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1224
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->isReleased:Z

    if-eqz v1, :cond_0

    .line 1263
    :goto_0
    return-void

    .line 1225
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->isReleased:Z

    .line 1227
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 1230
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgHandlerThread:Landroid/os/HandlerThread;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Landroid/os/HandlerThread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1237
    :goto_1
    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginEventHandler:Landroid/os/Handler;

    .line 1238
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginEventHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 1241
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginEventHandlerThread:Landroid/os/HandlerThread;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Landroid/os/HandlerThread;->join(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1248
    :goto_2
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->unbindPluginService()V

    .line 1250
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1252
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1259
    :goto_3
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    monitor-enter v2

    .line 1261
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgr:Landroid/os/Messenger;

    .line 1262
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 1231
    :catch_0
    move-exception v0

    .line 1233
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    const-string v2, "Plugin Msg Handler thread failed to shut down cleanly, InterruptedException"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1234
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 1242
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 1244
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    const-string v2, "Plugin Event Handler thread failed to shut down cleanly, InterruptedException"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1245
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_2

    .line 1256
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mCurrentCmdThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_3
.end method

.method connectToAsyncResult(Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)V
    .locals 7
    .param p1, "deviceToConnectTo"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;
    .param p2, "resultMessenger"    # Landroid/os/Messenger;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;",
            ">(",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;",
            "Landroid/os/Messenger;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 445
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    .line 447
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 448
    .local v0, "connectData":Landroid/os/Bundle;
    const-string/jumbo v4, "parcelable_AsyncScanResultDeviceInfo"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 449
    const-string/jumbo v4, "msgr_ReqAccResultReceiver"

    invoke-virtual {v0, v4, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 451
    const/16 v4, 0x2774

    invoke-virtual {p0, v4, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->sendPluginCommand(ILandroid/os/Bundle;)Landroid/os/Message;

    move-result-object v3

    .line 452
    .local v3, "ret":Landroid/os/Message;
    if-nez v3, :cond_0

    .line 454
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    const-string v5, "connectToAsyncResult died in sendPluginCommand()"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 456
    .local v2, "failMsg":Landroid/os/Message;
    const/4 v4, -0x4

    iput v4, v2, Landroid/os/Message;->what:I

    .line 459
    :try_start_0
    invoke-virtual {p2, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 475
    .end local v2    # "failMsg":Landroid/os/Message;
    :goto_0
    return-void

    .line 460
    .restart local v2    # "failMsg":Landroid/os/Message;
    :catch_0
    move-exception v1

    .line 463
    .local v1, "e1":Landroid/os/RemoteException;
    const-string v4, "Remote exception sending async connect failure msg to client"

    invoke-virtual {p0, v4}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    goto :goto_0

    .line 466
    .end local v1    # "e1":Landroid/os/RemoteException;
    .end local v2    # "failMsg":Landroid/os/Message;
    :cond_0
    iget v4, v3, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_1

    .line 469
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Request to connectToAsync Result cmd failed with code "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 473
    :cond_1
    invoke-virtual {v3}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method

.method protected createCmdMsg(ILandroid/os/Bundle;)Landroid/os/Message;
    .locals 3
    .param p1, "cmdCode"    # I
    .param p2, "msgData"    # Landroid/os/Bundle;

    .prologue
    .line 885
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 886
    .local v0, "cmdMsg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 888
    if-nez p2, :cond_0

    .line 889
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "msgData":Landroid/os/Bundle;
    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 891
    .restart local p2    # "msgData":Landroid/os/Bundle;
    :cond_0
    const-string/jumbo v1, "uuid_AccessToken"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mAccessToken:Ljava/util/UUID;

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 892
    invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 894
    return-object v0
.end method

.method public getAntDeviceNumber()I
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v0, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getCurrentDeviceState()Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;
    .locals 1

    .prologue
    .line 880
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mCachedState:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 842
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v0, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract getPluginPrintableName()Ljava/lang/String;
.end method

.method protected abstract getRequiredServiceVersionForBind()I
.end method

.method protected abstract getServiceBindIntent()Landroid/content/Intent;
.end method

.method handleConnectionBroke(Ljava/lang/String;)V
    .locals 3
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 1150
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConnectionDied: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mReleaseHandle:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mReleaseHandle:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    iget-boolean v0, v0, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->isClosed:Z

    if-eqz v0, :cond_1

    .line 1158
    :cond_0
    :goto_0
    return-void

    .line 1154
    :cond_1
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->releaseToken()V

    .line 1157
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;->DEAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;

    invoke-interface {v0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;->onDeviceStateChange(Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    goto :goto_0
.end method

.method protected handleNonCmdPluginMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 794
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginEventHandler:Landroid/os/Handler;

    .line 795
    .local v0, "h":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 797
    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 798
    .local v1, "m":Landroid/os/Message;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 799
    iget-object v2, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    iput-object v2, v1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 800
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 802
    .end local v1    # "m":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method protected abstract handlePluginEvent(Landroid/os/Message;)V
.end method

.method init(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Ljava/util/UUID;Landroid/os/Messenger;II)V
    .locals 1
    .param p1, "devInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "accessToken"    # Ljava/util/UUID;
    .param p3, "pluginMsgr"    # Landroid/os/Messenger;
    .param p4, "initialDeviceState"    # I
    .param p5, "reportedServiceVersion"    # I

    .prologue
    .line 824
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 825
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mAccessToken:Ljava/util/UUID;

    .line 826
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginMsgr:Landroid/os/Messenger;

    .line 827
    iput p5, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->reportedServiceVersion:I

    .line 828
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mCachedState:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 829
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mCachedState:Ljava/lang/Integer;

    .line 831
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->isInitialized:Z

    .line 832
    return-void
.end method

.method public isUserPreferredDeviceForPlugin()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 869
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->isPreferredDevice:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUserRecognizedDevice()Z
    .locals 1

    .prologue
    .line 860
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v0, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->device_dbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseAccess()V
    .locals 1

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mReleaseHandle:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->close()V

    .line 1115
    return-void
.end method

.method releaseToken()V
    .locals 2

    .prologue
    .line 1173
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;

    monitor-enter v1

    .line 1175
    const/16 v0, -0x64

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mCachedState:Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1181
    const/16 v0, 0x2712

    :try_start_1
    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->sendReleaseCommand(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1189
    :try_start_2
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->closePluginConnection()V

    .line 1191
    monitor-exit v1

    .line 1192
    return-void

    .line 1189
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->closePluginConnection()V

    throw v0

    .line 1191
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method protected sendPluginCommand(ILandroid/os/Bundle;)Landroid/os/Message;
    .locals 1
    .param p1, "cmdCode"    # I
    .param p2, "msgData"    # Landroid/os/Bundle;

    .prologue
    .line 899
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->createCmdMsg(ILandroid/os/Bundle;)Landroid/os/Message;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->sendPluginCommandInternal(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    return-object v0
.end method

.method protected sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;
    .locals 3
    .param p1, "cmdMsg"    # Landroid/os/Message;

    .prologue
    .line 905
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 906
    .local v0, "msgData":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 908
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "msgData":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 909
    .restart local v0    # "msgData":Landroid/os/Bundle;
    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 911
    :cond_0
    const-string/jumbo v1, "uuid_AccessToken"

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mAccessToken:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 912
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->sendPluginCommandInternal(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    return-object v1
.end method

.method stopAsyncScan()V
    .locals 1

    .prologue
    .line 1165
    const/16 v0, 0x2775

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->sendReleaseCommand(I)V

    .line 1166
    return-void
.end method

.method protected subscribeToEvent(I)Z
    .locals 6
    .param p1, "eventCode"    # I

    .prologue
    const/4 v2, 0x0

    .line 1059
    const/16 v3, 0x2710

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->createCmdMsg(ILandroid/os/Bundle;)Landroid/os/Message;

    move-result-object v1

    .line 1060
    .local v1, "subCmdMsg":Landroid/os/Message;
    iput p1, v1, Landroid/os/Message;->arg1:I

    .line 1062
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    .line 1063
    .local v0, "ret":Landroid/os/Message;
    if-nez v0, :cond_0

    .line 1065
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "subscribeToEvent died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1078
    :goto_0
    return v2

    .line 1069
    :cond_0
    iget v3, v0, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_1

    .line 1071
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Subscribing to event "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    goto :goto_0

    .line 1077
    :cond_1
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    .line 1078
    const/4 v2, 0x1

    goto :goto_0
.end method

.method protected unsubscribeFromEvent(I)V
    .locals 5
    .param p1, "eventCode"    # I

    .prologue
    .line 1084
    const/16 v2, 0x2711

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->createCmdMsg(ILandroid/os/Bundle;)Landroid/os/Message;

    move-result-object v1

    .line 1085
    .local v1, "unsubCmdMsg":Landroid/os/Message;
    iput p1, v1, Landroid/os/Message;->arg1:I

    .line 1087
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    .line 1088
    .local v0, "ret":Landroid/os/Message;
    if-nez v0, :cond_0

    .line 1090
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "unsubscribeFromEvent died in sendPluginCommand()"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1102
    :goto_0
    return-void

    .line 1093
    :cond_0
    iget v2, v0, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_1

    .line 1096
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsubscribing to event "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed with code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1100
    :cond_1
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method
