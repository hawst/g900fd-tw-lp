.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
.source "AntPlusBloodPressurePcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IResetDataAndSetTimeFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IMeasurementDownloadedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadMeasurementsStatusReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadAllHistoryFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAntFsProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

.field mCommandLock:Ljava/util/concurrent/Semaphore;

.field mDownloadAllHistoryFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadAllHistoryFinishedReceiver;

.field mDownloadMeasurementsStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadMeasurementsStatusReceiver;

.field mFitFileDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;

.field mMeasurementDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IMeasurementDownloadedReceiver;

.field mResetDataAndSetTimeFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IResetDataAndSetTimeFinishedReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 662
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;-><init>()V

    .line 469
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    .line 662
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 580
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;>;"
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "skipPreferredSearch"    # Z
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZI",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 527
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;>;"
    new-instance v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;

    invoke-direct {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;-><init>()V

    .local v4, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 529
    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->requestAccess_Helper_SearchActivity(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "searchProximityThreshold"    # I
    .param p4, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 625
    .local p3, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;>;"
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;

    invoke-direct {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;-><init>()V

    .local v3, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 627
    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->requestAccess_Helper_AsyncSearchByDevNumber(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .locals 2
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p2, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 657
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;-><init>()V

    .line 659
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;
    invoke-static {p0, p1, v0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->requestAccess_Helper_AsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public cancelDownloadMeasurementsMonitor()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 884
    iget v4, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->reportedServiceVersion:I

    const/16 v5, 0x4eea

    if-ge v4, v5, :cond_0

    .line 886
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cancelDownloadMeasurementsMonitor requires ANT+ Plugins Service >20202, installed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->reportedServiceVersion:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    :goto_0
    return v2

    .line 890
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 891
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v4, 0x4e24

    iput v4, v0, Landroid/os/Message;->what:I

    .line 893
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    .line 895
    .local v1, "ret":Landroid/os/Message;
    if-nez v1, :cond_1

    .line 897
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestAntFsMfgId died in sendPluginCommand()"

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 898
    goto :goto_0

    .line 901
    :cond_1
    iget v4, v1, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_2

    .line 903
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd requestAntFsMsgId failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 907
    :cond_2
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    move v2, v3

    .line 908
    goto :goto_0
.end method

.method public getAntFsManufacturerID()I
    .locals 6

    .prologue
    .line 778
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 779
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e22

    iput v3, v0, Landroid/os/Message;->what:I

    .line 781
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 783
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_0

    .line 785
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestAntFsMfgId died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    const/4 v1, -0x1

    .line 799
    :goto_0
    return v1

    .line 789
    :cond_0
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_1

    .line 792
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd requestAntFsMsgId failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "requestAntFsMsgId cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 796
    :cond_1
    iget v1, v2, Landroid/os/Message;->arg2:I

    .line 798
    .local v1, "mfgID":I
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method

.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 675
    const-string v0, "ANT+ Plugin: Blood Pressure"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0x2a30

    return v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 667
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 668
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.bloodpressure.BloodPressureService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 669
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 13
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 681
    iget v0, p1, Landroid/os/Message;->arg1:I

    sparse-switch v0, :sswitch_data_0

    .line 765
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unrecognized event received: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v0, v11}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    :cond_0
    :goto_0
    return-void

    .line 685
    :sswitch_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mAntFsProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    if-eqz v0, :cond_0

    .line 688
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 689
    .local v6, "b":Landroid/os/Bundle;
    const-string v0, "int_stateCode"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;

    move-result-object v1

    .line 690
    .local v1, "stateCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;
    const-string v0, "long_transferredBytes"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 691
    .local v2, "transferredBytes":J
    const-string v0, "long_totalBytes"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 692
    .local v4, "totalBytes":J
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mAntFsProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    invoke-interface/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;->onNewAntFsProgressUpdate(Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;JJ)V

    goto :goto_0

    .line 698
    .end local v1    # "stateCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;
    .end local v2    # "transferredBytes":J
    .end local v4    # "totalBytes":J
    .end local v6    # "b":Landroid/os/Bundle;
    :sswitch_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mFitFileDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;

    if-eqz v0, :cond_0

    .line 701
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 702
    .restart local v6    # "b":Landroid/os/Bundle;
    const-string v0, "arrayByte_rawFileBytes"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v7

    .line 705
    .local v7, "fileBytes":[B
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mFitFileDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;

    new-instance v11, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    invoke-direct {v11, v7}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;-><init>([B)V

    invoke-interface {v0, v11}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;->onNewFitFileDownloaded(Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)V

    goto :goto_0

    .line 711
    .end local v6    # "b":Landroid/os/Bundle;
    .end local v7    # "fileBytes":[B
    :sswitch_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mDownloadAllHistoryFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadAllHistoryFinishedReceiver;

    if-eqz v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 716
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 717
    .restart local v6    # "b":Landroid/os/Bundle;
    const-string v0, "int_statusCode"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;

    move-result-object v10

    .line 718
    .local v10, "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mDownloadAllHistoryFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadAllHistoryFinishedReceiver;

    invoke-interface {v0, v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadAllHistoryFinishedReceiver;->onDownloadAllHistoryFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;)V

    goto :goto_0

    .line 724
    .end local v6    # "b":Landroid/os/Bundle;
    .end local v10    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;
    :sswitch_3
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mDownloadMeasurementsStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadMeasurementsStatusReceiver;

    if-eqz v0, :cond_0

    .line 727
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 728
    .restart local v6    # "b":Landroid/os/Bundle;
    const-string v0, "int_statusCode"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;

    move-result-object v10

    .line 729
    .local v10, "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;
    const-string v0, "int_finishedCode"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;

    move-result-object v8

    .line 731
    .local v8, "finishedCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;->FINISHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;

    if-ne v10, v0, :cond_1

    .line 732
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 734
    :cond_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mDownloadMeasurementsStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadMeasurementsStatusReceiver;

    invoke-interface {v0, v10, v8}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadMeasurementsStatusReceiver;->onDownloadMeasurementsStatus(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;)V

    goto/16 :goto_0

    .line 740
    .end local v6    # "b":Landroid/os/Bundle;
    .end local v8    # "finishedCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;
    .end local v10    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;
    :sswitch_4
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mMeasurementDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IMeasurementDownloadedReceiver;

    if-eqz v0, :cond_0

    .line 743
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 744
    .restart local v6    # "b":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 745
    const-string/jumbo v0, "parcelable_measurement"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;

    .line 746
    .local v9, "measurement":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mMeasurementDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IMeasurementDownloadedReceiver;

    invoke-interface {v0, v9}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IMeasurementDownloadedReceiver;->onMeasurementDownloaded(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;)V

    goto/16 :goto_0

    .line 752
    .end local v6    # "b":Landroid/os/Bundle;
    .end local v9    # "measurement":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$BloodPressureMeasurement;
    :sswitch_5
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mResetDataAndSetTimeFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IResetDataAndSetTimeFinishedReceiver;

    if-eqz v0, :cond_0

    .line 755
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 756
    .restart local v6    # "b":Landroid/os/Bundle;
    const-string v0, "int_statusCode"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;

    move-result-object v10

    .line 758
    .local v10, "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 760
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mResetDataAndSetTimeFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IResetDataAndSetTimeFinishedReceiver;

    invoke-interface {v0, v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IResetDataAndSetTimeFinishedReceiver;->onNewResetDataAndSetTimeFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;)V

    goto/16 :goto_0

    .line 681
    nop

    :sswitch_data_0
    .sparse-switch
        0xbe -> :sswitch_0
        0xbf -> :sswitch_1
        0xcb -> :sswitch_2
        0xcc -> :sswitch_3
        0xcd -> :sswitch_4
        0xce -> :sswitch_5
    .end sparse-switch
.end method

.method public requestDownloadMeasurements(ZZLcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadMeasurementsStatusReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IMeasurementDownloadedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)Z
    .locals 7
    .param p1, "downloadNewOnly"    # Z
    .param p2, "monitorForNewMeasurements"    # Z
    .param p3, "downloadMeasurementStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadMeasurementsStatusReceiver;
    .param p4, "measurementDownloadedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IMeasurementDownloadedReceiver;
    .param p5, "antFsProgressUpdateRecevier"    # Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 830
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->reportedServiceVersion:I

    const/16 v6, 0x4eea

    if-ge v3, v6, :cond_1

    .line 832
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "requestDownloadMeasurements requires ANT+ Plugins Service >20202, installed: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v6, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->reportedServiceVersion:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;->FINISHED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;

    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;->FAIL_PLUGINS_SERVICE_VERSION:Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;

    invoke-interface {p3, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadMeasurementsStatusReceiver;->onDownloadMeasurementsStatus(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$DownloadMeasurementsStatusCode;Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;)V

    .line 871
    :cond_0
    :goto_0
    return v5

    .line 837
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 840
    iput-object p5, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mAntFsProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    .line 841
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mDownloadMeasurementsStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IDownloadMeasurementsStatusReceiver;

    .line 842
    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mMeasurementDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IMeasurementDownloadedReceiver;

    .line 844
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 845
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e23

    iput v3, v0, Landroid/os/Message;->what:I

    .line 846
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 847
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 849
    const-string v3, "bool_downloadNewOnly"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 850
    const-string v3, "bool_monitorForNewMeasurements"

    invoke-virtual {v1, v3, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 851
    const-string v6, "bool_UseAntFsProgressUpdates"

    if-eqz p5, :cond_2

    move v3, v4

    :goto_1
    invoke-virtual {v1, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 853
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 855
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_3

    .line 857
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestDownloadMeasurements died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .end local v2    # "ret":Landroid/os/Message;
    :cond_2
    move v3, v5

    .line 851
    goto :goto_1

    .line 862
    .restart local v2    # "ret":Landroid/os/Message;
    :cond_3
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_4

    .line 865
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd requestDownloadMeasurements failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 867
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "requestAllHistory cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 870
    :cond_4
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    move v5, v4

    .line 871
    goto :goto_0
.end method

.method public requestResetDataAndSetTime(ZLcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IResetDataAndSetTimeFinishedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)Z
    .locals 7
    .param p1, "doSetTime"    # Z
    .param p2, "resetDataAndSetTimeFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IResetDataAndSetTimeFinishedReceiver;
    .param p3, "antFsProgressUpdateRecevier"    # Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 925
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->reportedServiceVersion:I

    const/16 v6, 0x4eed

    if-ge v3, v6, :cond_1

    .line 927
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "requestResetDataAndSetTime requires ANT+ Plugins Service >20205, installed: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v6, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->reportedServiceVersion:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;->FAIL_PLUGINS_SERVICE_VERSION:Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;

    invoke-interface {p2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IResetDataAndSetTimeFinishedReceiver;->onNewResetDataAndSetTimeFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;)V

    .line 964
    :cond_0
    :goto_0
    return v5

    .line 932
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 935
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mResetDataAndSetTimeFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc$IResetDataAndSetTimeFinishedReceiver;

    .line 936
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mAntFsProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    .line 938
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 939
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e25

    iput v3, v0, Landroid/os/Message;->what:I

    .line 940
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 941
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 943
    const-string v3, "bool_doSetTime"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 944
    const-string v6, "bool_UseAntFsProgressUpdates"

    if-eqz p3, :cond_2

    move v3, v4

    :goto_1
    invoke-virtual {v1, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 946
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 948
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_3

    .line 950
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestResetDataAndSetTime died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .end local v2    # "ret":Landroid/os/Message;
    :cond_2
    move v3, v5

    .line 944
    goto :goto_1

    .line 955
    .restart local v2    # "ret":Landroid/os/Message;
    :cond_3
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_4

    .line 958
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd requestDataAndSetTime failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBloodPressurePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 960
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "requestDataAndSetTime cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 963
    :cond_4
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    move v5, v4

    .line 964
    goto :goto_0
.end method
