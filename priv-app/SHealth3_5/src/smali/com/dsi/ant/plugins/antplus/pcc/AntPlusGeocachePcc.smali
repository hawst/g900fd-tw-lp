.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
.source "AntPlusGeocachePcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAuthTokenRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;

.field mAvailableDeviceListReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;

.field mCommandLock:Ljava/util/concurrent/Semaphore;

.field mDataDownloadFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;

.field mProgrammingFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;

.field mSimpleProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 777
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;-><init>()V

    .line 737
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    .line 777
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 790
    const-string v0, "ANT+ Plugin: Geocache"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 782
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 783
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.geocache.GeocacheService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 784
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 16
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 796
    move-object/from16 v0, p1

    iget v13, v0, Landroid/os/Message;->arg1:I

    packed-switch v13, :pswitch_data_0

    .line 883
    sget-object v13, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Unrecognized event received: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    iget v15, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    :cond_0
    :goto_0
    return-void

    .line 800
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mAvailableDeviceListReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;

    if-eqz v13, :cond_0

    .line 803
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 804
    .local v3, "b":Landroid/os/Bundle;
    const-string v13, "arrayInt_deviceIDs"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v7

    .line 805
    .local v7, "deviceIDs":[I
    const-string v13, "arrayString_deviceIdentifierStrings"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 806
    .local v8, "deviceIdentifierStrings":[Ljava/lang/String;
    const-string v13, "int_changeCode"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    invoke-static {v13}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;

    move-result-object v4

    .line 807
    .local v4, "changeCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;
    const-string v13, "int_changingDeviceID"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 808
    .local v5, "changingDeviceID":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mAvailableDeviceListReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;

    invoke-interface {v13, v7, v8, v4, v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAvailableDeviceListReceiver;->onNewAvailableDeviceList([I[Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;I)V

    goto :goto_0

    .line 814
    .end local v3    # "b":Landroid/os/Bundle;
    .end local v4    # "changeCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$DeviceChangingCode;
    .end local v5    # "changingDeviceID":I
    .end local v7    # "deviceIDs":[I
    .end local v8    # "deviceIdentifierStrings":[Ljava/lang/String;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mSimpleProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;

    if-eqz v13, :cond_0

    .line 817
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 818
    .restart local v3    # "b":Landroid/os/Bundle;
    const-string v13, "int_workUnitsFinished"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 819
    .local v12, "workUnitsFinished":I
    const-string v13, "int_totalUnitsWork"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 820
    .local v11, "totalUnitsWork":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mSimpleProgressUpdateReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;

    invoke-interface {v13, v12, v11}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ISimpleProgressUpdateReceiver;->onNewSimpleProgressUpdate(II)V

    goto :goto_0

    .line 826
    .end local v3    # "b":Landroid/os/Bundle;
    .end local v11    # "totalUnitsWork":I
    .end local v12    # "workUnitsFinished":I
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mProgrammingFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;

    if-eqz v13, :cond_0

    .line 829
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v13}, Ljava/util/concurrent/Semaphore;->release()V

    .line 831
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 832
    .restart local v3    # "b":Landroid/os/Bundle;
    const-string v13, "int_statusCode"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    invoke-static {v13}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    move-result-object v10

    .line 833
    .local v10, "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mProgrammingFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;

    invoke-interface {v13, v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IProgrammingFinishedReceiver;->onNewProgrammingFinished(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto :goto_0

    .line 839
    .end local v3    # "b":Landroid/os/Bundle;
    .end local v10    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mDataDownloadFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;

    if-eqz v13, :cond_0

    .line 842
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v13}, Ljava/util/concurrent/Semaphore;->release()V

    .line 844
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 845
    .restart local v3    # "b":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 846
    const-string v13, "int_statusCode"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    invoke-static {v13}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    move-result-object v10

    .line 847
    .restart local v10    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;
    invoke-virtual {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->getIntValue()I

    move-result v13

    if-ltz v13, :cond_2

    .line 849
    const-string v13, "bundle_downloadedData"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v9

    .line 851
    .local v9, "downloadedData":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->reportedServiceVersion:I

    if-nez v13, :cond_1

    .line 853
    invoke-static {v9}, Lcom/dsi/ant/plugins/internal/compatibility/LegacyGeocacheCompat$GeocacheDeviceDataCompat_v1;->readGddFromBundleCompat_v1(Landroid/os/Bundle;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    move-result-object v6

    .line 859
    .local v6, "d":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mDataDownloadFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;

    invoke-interface {v13, v10, v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;->onNewDataDownloadFinished(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;)V

    goto/16 :goto_0

    .line 857
    .end local v6    # "d":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;
    :cond_1
    const-string/jumbo v13, "parcelable_GeocacheDeviceData"

    invoke-virtual {v9, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    .restart local v6    # "d":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;
    goto :goto_1

    .line 863
    .end local v6    # "d":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;
    .end local v9    # "downloadedData":Landroid/os/Bundle;
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mDataDownloadFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;

    const/4 v14, 0x0

    invoke-interface {v13, v10, v14}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IDataDownloadFinishedReceiver;->onNewDataDownloadFinished(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;)V

    goto/16 :goto_0

    .line 870
    .end local v3    # "b":Landroid/os/Bundle;
    .end local v10    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mAuthTokenRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;

    if-eqz v13, :cond_0

    .line 873
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v13}, Ljava/util/concurrent/Semaphore;->release()V

    .line 875
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 876
    .restart local v3    # "b":Landroid/os/Bundle;
    const-string v13, "int_statusCode"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 877
    .local v10, "statusCode":I
    const-string v13, "long_authToken"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 878
    .local v1, "authToken":J
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc;->mAuthTokenRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;

    invoke-static {v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    move-result-object v14

    invoke-interface {v13, v14, v1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$IAuthTokenRequestFinishedReceiver;->onNewAuthTokenRequestFinished(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;J)V

    goto/16 :goto_0

    .line 796
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
