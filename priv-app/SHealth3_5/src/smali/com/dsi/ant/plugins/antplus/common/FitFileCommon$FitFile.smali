.class public Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;
.super Ljava/lang/Object;
.source "FitFileCommon.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/common/FitFileCommon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FitFile"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final ipcVersionNumber:I

.field private mFitFileByteArray:[B

.field private mFitFileType:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 191
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile$1;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "src"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->ipcVersionNumber:I

    .line 172
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 173
    .local v0, "incomingVersion":I
    if-eq v0, v1, :cond_0

    .line 174
    # getter for: Lcom/dsi/ant/plugins/antplus/common/FitFileCommon;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Decoding version "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " FitFile parcel with version 1 parser."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->mFitFileType:S

    .line 177
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->mFitFileByteArray:[B

    .line 178
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->mFitFileByteArray:[B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V

    .line 179
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "fitFileBytes"    # [B

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    const/4 v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->ipcVersionNumber:I

    .line 124
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->mFitFileByteArray:[B

    .line 125
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public getFileType()S
    .locals 1

    .prologue
    .line 151
    iget-short v0, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->mFitFileType:S

    return v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 160
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->mFitFileByteArray:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method

.method public getRawBytes()[B
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->mFitFileByteArray:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public setFileType(S)V
    .locals 0
    .param p1, "fitFileType"    # S

    .prologue
    .line 142
    iput-short p1, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->mFitFileType:S

    .line 143
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 184
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->ipcVersionNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 185
    iget-short v0, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->mFitFileType:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 186
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->mFitFileByteArray:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 187
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;->mFitFileByteArray:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 188
    return-void
.end method
