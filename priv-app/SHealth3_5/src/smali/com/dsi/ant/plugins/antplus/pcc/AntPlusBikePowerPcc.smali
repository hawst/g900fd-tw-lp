.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;
.source "AntPlusBikePowerPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelDistanceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedCrankCadenceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedTorqueReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedPowerReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCtfDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalSmoothnessReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ITorqueEffectivenessReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCrankTorqueDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawWheelTorqueDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IInstantaneousCadenceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalPowerBalanceReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawPowerOnlyDataReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CustomCalibrationStatus;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorAvailabilityStatus;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationId;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankParameters;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationMessage;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

.field mCalculatedCrankCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedCrankCadenceReceiver;

.field mCalculatedPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedPowerReceiver;

.field mCalculatedTorqueReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedTorqueReceiver;

.field mCalculatedWheelDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelDistanceReceiver;

.field mCalculatedWheelSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;

.field mCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

.field mCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

.field mInstantaneousCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IInstantaneousCadenceReceiver;

.field mMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

.field mPedalPowerBalanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalPowerBalanceReceiver;

.field mPedalSmoothnessReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalSmoothnessReceiver;

.field mRawCrankTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCrankTorqueDataReceiver;

.field mRawCtfDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCtfDataReceiver;

.field mRawPowerOnlyDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawPowerOnlyDataReceiver;

.field mRawWheelTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawWheelTorqueDataReceiver;

.field mRequestAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

.field mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

.field mRequestCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

.field mRequestMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

.field mTorqueEffectivenessReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ITorqueEffectivenessReceiver;

.field pccHandler:Landroid/os/Handler;

.field unsubscibeRequestMeasurementOutputDataReceiver:Ljava/lang/Runnable;

.field unsubscribeRequestAutoZeroStatusReceiver:Ljava/lang/Runnable;

.field unsubscribeRequestCalibrationMessageReceiver:Ljava/lang/Runnable;

.field unsubscribeRequestCrankParametersReceiver:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1907
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;-><init>()V

    .line 1677
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->pccHandler:Landroid/os/Handler;

    .line 1679
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$1;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeRequestCalibrationMessageReceiver:Ljava/lang/Runnable;

    .line 1688
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$2;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$2;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeRequestAutoZeroStatusReceiver:Ljava/lang/Runnable;

    .line 1697
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$3;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$3;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscibeRequestMeasurementOutputDataReceiver:Ljava/lang/Runnable;

    .line 1706
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$4;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$4;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeRequestCrankParametersReceiver:Ljava/lang/Runnable;

    .line 1907
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestCalibrationMessageEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestAutoZeroStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;)V

    return-void
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestMeasurementOutputDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;)V

    return-void
.end method

.method static synthetic access$400(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestCrankParametersEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;)V

    return-void
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p3, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1824
    .local p2, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;>;"
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p0, "userActivity"    # Landroid/app/Activity;
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "skipPreferredSearch"    # Z
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "ZI",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1771
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;>;"
    new-instance v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    invoke-direct {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;-><init>()V

    .local v4, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 1773
    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestAccess_Helper_SearchActivity(Landroid/app/Activity;Landroid/content/Context;ZILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAccess(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "searchProximityThreshold"    # I
    .param p4, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1869
    .local p3, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;>;"
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    invoke-direct {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;-><init>()V

    .local v3, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 1871
    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestAccess_Helper_AsyncSearchByDevNumber(Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestAsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;
    .locals 2
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p1, "searchProximityThreshold"    # I
    .param p2, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1901
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;-><init>()V

    .line 1903
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;
    invoke-static {p0, p1, v0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestAccess_Helper_AsyncScanController(Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$IAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController;

    move-result-object v1

    return-object v1
.end method

.method private subscribeRequestAutoZeroStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;)V
    .locals 2
    .param p1, "autoZeroStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    .prologue
    const/16 v1, 0xd2

    .line 2433
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    if-nez v0, :cond_0

    .line 2435
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    if-nez v0, :cond_1

    .line 2436
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2440
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    .line 2441
    return-void

    .line 2437
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    if-eqz v0, :cond_0

    .line 2438
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private subscribeRequestCalibrationMessageEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)V
    .locals 2
    .param p1, "calibrationMessageReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    .prologue
    const/16 v1, 0xd1

    .line 2403
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    if-nez v0, :cond_0

    .line 2405
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    if-nez v0, :cond_1

    .line 2406
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2410
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    .line 2411
    return-void

    .line 2407
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    if-eqz v0, :cond_0

    .line 2408
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private subscribeRequestCrankParametersEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;)V
    .locals 2
    .param p1, "crankParametersReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    .prologue
    const/16 v1, 0xd9

    .line 2588
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    if-nez v0, :cond_0

    .line 2590
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    if-nez v0, :cond_1

    .line 2591
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2595
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    .line 2596
    return-void

    .line 2592
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    if-eqz v0, :cond_0

    .line 2593
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private subscribeRequestMeasurementOutputDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;)V
    .locals 2
    .param p1, "measurementOutputDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    .prologue
    const/16 v1, 0xd8

    .line 2558
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    if-nez v0, :cond_0

    .line 2560
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    if-nez v0, :cond_1

    .line 2561
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2565
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    .line 2566
    return-void

    .line 2562
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    if-eqz v0, :cond_0

    .line 2563
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method


# virtual methods
.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1920
    const-string v0, "ANT+ Plugin: Bike Power"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 32
    const/16 v0, 0x2a30

    return v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1912
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1913
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.bikepower.BikePowerService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1914
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 99
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 1926
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 2226
    invoke-super/range {p0 .. p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 2229
    :cond_0
    :goto_0
    return-void

    .line 1930
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawPowerOnlyDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawPowerOnlyDataReceiver;

    if-eqz v1, :cond_0

    .line 1933
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 1934
    .local v96, "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1935
    .local v2, "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1936
    .local v4, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_powerOnlyUpdateEventCount"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 1937
    .local v5, "powerOnlyUpdateEventCount":J
    const-string v1, "int_instantaneousPower"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 1938
    .local v7, "instantaneousPower":I
    const-string v1, "long_accumulatedPower"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 1939
    .local v8, "accumulatedPower":J
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawPowerOnlyDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawPowerOnlyDataReceiver;

    invoke-interface/range {v1 .. v9}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawPowerOnlyDataReceiver;->onNewRawPowerOnlyData(JLjava/util/EnumSet;JIJ)V

    goto :goto_0

    .line 1945
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "powerOnlyUpdateEventCount":J
    .end local v7    # "instantaneousPower":I
    .end local v8    # "accumulatedPower":J
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mPedalPowerBalanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalPowerBalanceReceiver;

    if-eqz v1, :cond_0

    .line 1948
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 1949
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1950
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1951
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "bool_rightPedalIndicator"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v14

    .line 1952
    .local v14, "rightPedalIndicator":Z
    const-string v1, "int_pedalPowerPercentage"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v15

    .line 1953
    .local v15, "pedalPowerPercentage":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mPedalPowerBalanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalPowerBalanceReceiver;

    move-wide v11, v2

    move-object v13, v4

    invoke-interface/range {v10 .. v15}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalPowerBalanceReceiver;->onNewPedalPowerBalance(JLjava/util/EnumSet;ZI)V

    goto :goto_0

    .line 1959
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v14    # "rightPedalIndicator":Z
    .end local v15    # "pedalPowerPercentage":I
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mInstantaneousCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IInstantaneousCadenceReceiver;

    if-eqz v1, :cond_0

    .line 1962
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 1963
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1964
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1965
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_dataSource"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    move-result-object v20

    .line 1966
    .local v20, "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    const-string v1, "int_instantaneousCadence"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v21

    .line 1967
    .local v21, "instantaneousCadence":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mInstantaneousCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IInstantaneousCadenceReceiver;

    move-object/from16 v16, v0

    move-wide/from16 v17, v2

    move-object/from16 v19, v4

    invoke-interface/range {v16 .. v21}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IInstantaneousCadenceReceiver;->onNewInstantaneousCadence(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;I)V

    goto/16 :goto_0

    .line 1973
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v20    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    .end local v21    # "instantaneousCadence":I
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawWheelTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawWheelTorqueDataReceiver;

    if-eqz v1, :cond_0

    .line 1976
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 1977
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1978
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1979
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_wheelTorqueUpdateEventCount"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v26

    .line 1980
    .local v26, "wheelTorqueUpdateEventCount":J
    const-string v1, "long_accumulatedWheelTicks"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v28

    .line 1981
    .local v28, "accumulatedWheelTicks":J
    const-string v1, "decimal_accumulatedWheelPeriod"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v30

    check-cast v30, Ljava/math/BigDecimal;

    .line 1982
    .local v30, "accumulatedWheelPeriod":Ljava/math/BigDecimal;
    const-string v1, "decimal_accumulatedWheelTorque"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v31

    check-cast v31, Ljava/math/BigDecimal;

    .line 1983
    .local v31, "accumulatedWheelTorque":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawWheelTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawWheelTorqueDataReceiver;

    move-object/from16 v22, v0

    move-wide/from16 v23, v2

    move-object/from16 v25, v4

    invoke-interface/range {v22 .. v31}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawWheelTorqueDataReceiver;->onNewRawWheelTorqueData(JLjava/util/EnumSet;JJLjava/math/BigDecimal;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 1989
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v26    # "wheelTorqueUpdateEventCount":J
    .end local v28    # "accumulatedWheelTicks":J
    .end local v30    # "accumulatedWheelPeriod":Ljava/math/BigDecimal;
    .end local v31    # "accumulatedWheelTorque":Ljava/math/BigDecimal;
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawCrankTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCrankTorqueDataReceiver;

    if-eqz v1, :cond_0

    .line 1992
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 1993
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1994
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 1995
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_crankTorqueUpdateEventCount"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v36

    .line 1996
    .local v36, "crankTorqueUpdateEventCount":J
    const-string v1, "long_accumulatedCrankTicks"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v38

    .line 1997
    .local v38, "accumulatedCrankTicks":J
    const-string v1, "decimal_accumulatedCrankPeriod"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v40

    check-cast v40, Ljava/math/BigDecimal;

    .line 1998
    .local v40, "accumulatedCrankPeriod":Ljava/math/BigDecimal;
    const-string v1, "decimal_accumulatedCrankTorque"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v41

    check-cast v41, Ljava/math/BigDecimal;

    .line 1999
    .local v41, "accumulatedCrankTorque":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawCrankTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCrankTorqueDataReceiver;

    move-object/from16 v32, v0

    move-wide/from16 v33, v2

    move-object/from16 v35, v4

    invoke-interface/range {v32 .. v41}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCrankTorqueDataReceiver;->onNewRawCrankTorqueData(JLjava/util/EnumSet;JJLjava/math/BigDecimal;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 2005
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v36    # "crankTorqueUpdateEventCount":J
    .end local v38    # "accumulatedCrankTicks":J
    .end local v40    # "accumulatedCrankPeriod":Ljava/math/BigDecimal;
    .end local v41    # "accumulatedCrankTorque":Ljava/math/BigDecimal;
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mTorqueEffectivenessReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ITorqueEffectivenessReceiver;

    if-eqz v1, :cond_0

    .line 2008
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2009
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2010
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2011
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_powerOnlyUpdateEventCount"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 2012
    .restart local v5    # "powerOnlyUpdateEventCount":J
    const-string v1, "decimal_leftTorqueEffectiveness"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v48

    check-cast v48, Ljava/math/BigDecimal;

    .line 2013
    .local v48, "leftTorqueEffectiveness":Ljava/math/BigDecimal;
    const-string v1, "decimal_rightTorqueEffectiveness"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v49

    check-cast v49, Ljava/math/BigDecimal;

    .line 2014
    .local v49, "rightTorqueEffectiveness":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mTorqueEffectivenessReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ITorqueEffectivenessReceiver;

    move-object/from16 v42, v0

    move-wide/from16 v43, v2

    move-object/from16 v45, v4

    move-wide/from16 v46, v5

    invoke-interface/range {v42 .. v49}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ITorqueEffectivenessReceiver;->onNewTorqueEffectiveness(JLjava/util/EnumSet;JLjava/math/BigDecimal;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 2020
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "powerOnlyUpdateEventCount":J
    .end local v48    # "leftTorqueEffectiveness":Ljava/math/BigDecimal;
    .end local v49    # "rightTorqueEffectiveness":Ljava/math/BigDecimal;
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mPedalSmoothnessReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalSmoothnessReceiver;

    if-eqz v1, :cond_0

    .line 2023
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2024
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2025
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2026
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_powerOnlyUpdateEventCount"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 2027
    .restart local v5    # "powerOnlyUpdateEventCount":J
    const-string v1, "bool_separatePedalSmoothnessSupport"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v56

    .line 2028
    .local v56, "separatePedalSmoothnessSupport":Z
    const-string v1, "decimal_leftOrCombinedPedalSmoothness"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v57

    check-cast v57, Ljava/math/BigDecimal;

    .line 2029
    .local v57, "leftOrCombinedPedalSmoothness":Ljava/math/BigDecimal;
    const-string v1, "decimal_rightPedalSmoothness"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v58

    check-cast v58, Ljava/math/BigDecimal;

    .line 2030
    .local v58, "rightPedalSmoothness":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mPedalSmoothnessReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalSmoothnessReceiver;

    move-object/from16 v50, v0

    move-wide/from16 v51, v2

    move-object/from16 v53, v4

    move-wide/from16 v54, v5

    invoke-interface/range {v50 .. v58}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalSmoothnessReceiver;->onNewPedalSmoothness(JLjava/util/EnumSet;JZLjava/math/BigDecimal;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 2036
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v5    # "powerOnlyUpdateEventCount":J
    .end local v56    # "separatePedalSmoothnessSupport":Z
    .end local v57    # "leftOrCombinedPedalSmoothness":Ljava/math/BigDecimal;
    .end local v58    # "rightPedalSmoothness":Ljava/math/BigDecimal;
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawCtfDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCtfDataReceiver;

    if-eqz v1, :cond_0

    .line 2039
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2040
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2041
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2042
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "long_ctfUpdateEventCount"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v63

    .line 2043
    .local v63, "ctfUpdateEventCount":J
    const-string v1, "decimal_instantaneousSlope"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v65

    check-cast v65, Ljava/math/BigDecimal;

    .line 2044
    .local v65, "instantaneousSlope":Ljava/math/BigDecimal;
    const-string v1, "decimal_accumulatedTimeStamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v66

    check-cast v66, Ljava/math/BigDecimal;

    .line 2045
    .local v66, "accumulatedTimeStamp":Ljava/math/BigDecimal;
    const-string v1, "long_accumulatedTorqueTicksStamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v67

    .line 2046
    .local v67, "accumulatedTorqueTicksStamp":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawCtfDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCtfDataReceiver;

    move-object/from16 v59, v0

    move-wide/from16 v60, v2

    move-object/from16 v62, v4

    invoke-interface/range {v59 .. v68}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCtfDataReceiver;->onNewRawCtfData(JLjava/util/EnumSet;JLjava/math/BigDecimal;Ljava/math/BigDecimal;J)V

    goto/16 :goto_0

    .line 2052
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v63    # "ctfUpdateEventCount":J
    .end local v65    # "instantaneousSlope":Ljava/math/BigDecimal;
    .end local v66    # "accumulatedTimeStamp":Ljava/math/BigDecimal;
    .end local v67    # "accumulatedTorqueTicksStamp":J
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    if-eqz v1, :cond_0

    .line 2055
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2056
    .restart local v96    # "b":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 2057
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2058
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2060
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    if-eqz v1, :cond_2

    .line 2061
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    const-string/jumbo v1, "parcelable_CalibrationMessage"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationMessage;

    invoke-interface {v10, v2, v3, v4, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;->onNewCalibrationMessage(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationMessage;)V

    .line 2063
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    if-eqz v1, :cond_0

    .line 2065
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    const-string/jumbo v1, "parcelable_CalibrationMessage"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationMessage;

    invoke-interface {v10, v2, v3, v4, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;->onNewCalibrationMessage(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalibrationMessage;)V

    .line 2066
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->pccHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeRequestCalibrationMessageReceiver:Ljava/lang/Runnable;

    invoke-virtual {v1, v10}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2067
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->pccHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeRequestCalibrationMessageReceiver:Ljava/lang/Runnable;

    const-wide/16 v11, 0x1388

    invoke-virtual {v1, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 2074
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    if-nez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    if-eqz v1, :cond_0

    .line 2077
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2078
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2079
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2080
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_autoZeroStatus"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;

    move-result-object v95

    .line 2082
    .local v95, "autoZeroStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    if-eqz v1, :cond_4

    .line 2083
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    move-object/from16 v0, v95

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;->onNewAutoZeroStatus(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;)V

    .line 2085
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    if-eqz v1, :cond_0

    .line 2087
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    move-object/from16 v0, v95

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;->onNewAutoZeroStatus(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;)V

    .line 2088
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->pccHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeRequestAutoZeroStatusReceiver:Ljava/lang/Runnable;

    invoke-virtual {v1, v10}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2089
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->pccHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeRequestAutoZeroStatusReceiver:Ljava/lang/Runnable;

    const-wide/16 v11, 0x1388

    invoke-virtual {v1, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 2096
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v95    # "autoZeroStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$AutoZeroStatus;
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedPowerReceiver;

    if-eqz v1, :cond_0

    .line 2099
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2100
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2101
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2102
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_dataSource"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    move-result-object v20

    .line 2103
    .restart local v20    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    const-string v1, "decimal_calculatedPower"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v47

    check-cast v47, Ljava/math/BigDecimal;

    .line 2104
    .local v47, "calculatedPower":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedPowerReceiver;

    move-object/from16 v42, v0

    move-wide/from16 v43, v2

    move-object/from16 v45, v4

    move-object/from16 v46, v20

    invoke-interface/range {v42 .. v47}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedPowerReceiver;->onNewCalculatedPower(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 2110
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v20    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    .end local v47    # "calculatedPower":Ljava/math/BigDecimal;
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedTorqueReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedTorqueReceiver;

    if-eqz v1, :cond_0

    .line 2113
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2114
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2115
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2116
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_dataSource"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    move-result-object v20

    .line 2117
    .restart local v20    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    const-string v1, "decimal_calculatedTorque"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v55

    check-cast v55, Ljava/math/BigDecimal;

    .line 2118
    .local v55, "calculatedTorque":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedTorqueReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedTorqueReceiver;

    move-object/from16 v50, v0

    move-wide/from16 v51, v2

    move-object/from16 v53, v4

    move-object/from16 v54, v20

    invoke-interface/range {v50 .. v55}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedTorqueReceiver;->onNewCalculatedTorque(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 2124
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v20    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    .end local v55    # "calculatedTorque":Ljava/math/BigDecimal;
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedCrankCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedCrankCadenceReceiver;

    if-eqz v1, :cond_0

    .line 2127
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2128
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2129
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2130
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_dataSource"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    move-result-object v20

    .line 2131
    .restart local v20    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    const-string v1, "decimal_calculatedCrankCadence"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v74

    check-cast v74, Ljava/math/BigDecimal;

    .line 2132
    .local v74, "calculatedCrankCadence":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedCrankCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedCrankCadenceReceiver;

    move-object/from16 v69, v0

    move-wide/from16 v70, v2

    move-object/from16 v72, v4

    move-object/from16 v73, v20

    invoke-interface/range {v69 .. v74}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedCrankCadenceReceiver;->onNewCalculatedCrankCadence(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 2138
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v20    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    .end local v74    # "calculatedCrankCadence":Ljava/math/BigDecimal;
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedWheelSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;

    if-eqz v1, :cond_0

    .line 2141
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2142
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2143
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2144
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_dataSource"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    move-result-object v20

    .line 2145
    .restart local v20    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    const-string v1, "decimal_calculatedWheelSpeed"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v80

    check-cast v80, Ljava/math/BigDecimal;

    .line 2146
    .local v80, "calculatedWheelSpeed":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedWheelSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;

    move-object/from16 v75, v0

    move-wide/from16 v76, v2

    move-object/from16 v78, v4

    move-object/from16 v79, v20

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;->onNewRawCalculatedWheelSpeed(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V
    invoke-static/range {v75 .. v80}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;->access$500(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 2152
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v20    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    .end local v80    # "calculatedWheelSpeed":Ljava/math/BigDecimal;
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedWheelDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelDistanceReceiver;

    if-eqz v1, :cond_0

    .line 2155
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2156
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2157
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2158
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_dataSource"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;

    move-result-object v20

    .line 2159
    .restart local v20    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    const-string v1, "decimal_calculatedWheelDistance"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v86

    check-cast v86, Ljava/math/BigDecimal;

    .line 2160
    .local v86, "calculatedWheelDistance":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedWheelDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelDistanceReceiver;

    move-object/from16 v81, v0

    move-wide/from16 v82, v2

    move-object/from16 v84, v4

    move-object/from16 v85, v20

    # invokes: Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelDistanceReceiver;->onNewRawCalculatedWheelDistance(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V
    invoke-static/range {v81 .. v86}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelDistanceReceiver;->access$600(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelDistanceReceiver;JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;Ljava/math/BigDecimal;)V

    goto/16 :goto_0

    .line 2166
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v20    # "dataSource":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$DataSource;
    .end local v86    # "calculatedWheelDistance":Ljava/math/BigDecimal;
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    if-nez v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    if-eqz v1, :cond_0

    .line 2169
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2170
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2171
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2172
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v1, "int_numOfDataTypes"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v91

    .line 2173
    .local v91, "numOfDataTypes":I
    const-string v1, "int_dataType"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v92

    .line 2174
    .local v92, "dataType":I
    const-string v1, "decimal_timeStamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v93

    check-cast v93, Ljava/math/BigDecimal;

    .line 2175
    .local v93, "timeStamp":Ljava/math/BigDecimal;
    const-string v1, "decimal_measurementValue"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v94

    check-cast v94, Ljava/math/BigDecimal;

    .line 2177
    .local v94, "measurementValue":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    if-eqz v1, :cond_6

    .line 2178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    move-object/from16 v87, v0

    move-wide/from16 v88, v2

    move-object/from16 v90, v4

    invoke-interface/range {v87 .. v94}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;->onNewMeasurementOutputData(JLjava/util/EnumSet;IILjava/math/BigDecimal;Ljava/math/BigDecimal;)V

    .line 2180
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    if-eqz v1, :cond_0

    .line 2182
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    move-object/from16 v87, v0

    move-wide/from16 v88, v2

    move-object/from16 v90, v4

    invoke-interface/range {v87 .. v94}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;->onNewMeasurementOutputData(JLjava/util/EnumSet;IILjava/math/BigDecimal;Ljava/math/BigDecimal;)V

    .line 2183
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->pccHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscibeRequestMeasurementOutputDataReceiver:Ljava/lang/Runnable;

    invoke-virtual {v1, v10}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2184
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->pccHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscibeRequestMeasurementOutputDataReceiver:Ljava/lang/Runnable;

    const-wide/16 v11, 0x1388

    invoke-virtual {v1, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 2191
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v91    # "numOfDataTypes":I
    .end local v92    # "dataType":I
    .end local v93    # "timeStamp":Ljava/math/BigDecimal;
    .end local v94    # "measurementValue":Ljava/math/BigDecimal;
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    if-nez v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    if-eqz v1, :cond_0

    .line 2194
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2195
    .restart local v96    # "b":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 2196
    const-string v1, "long_EstTimestamp"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2197
    .restart local v2    # "estTimestamp":J
    const-string v1, "long_EventFlags"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v4

    .line 2199
    .restart local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    if-eqz v1, :cond_8

    .line 2200
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    const-string/jumbo v1, "parcelable_CrankParameters"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankParameters;

    invoke-interface {v10, v2, v3, v4, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;->onNewCrankParameters(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankParameters;)V

    .line 2202
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    if-eqz v1, :cond_0

    .line 2204
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    const-string/jumbo v1, "parcelable_CrankParameters"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankParameters;

    invoke-interface {v10, v2, v3, v4, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;->onNewCrankParameters(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankParameters;)V

    .line 2205
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->pccHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeRequestCrankParametersReceiver:Ljava/lang/Runnable;

    invoke-virtual {v1, v10}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2206
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->pccHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeRequestCrankParametersReceiver:Ljava/lang/Runnable;

    const-wide/16 v11, 0x1388

    invoke-virtual {v1, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 2213
    .end local v2    # "estTimestamp":J
    .end local v4    # "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    .end local v96    # "b":Landroid/os/Bundle;
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    move-object/from16 v98, v0

    .line 2214
    .local v98, "tempReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .line 2215
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 2217
    if-eqz v98, :cond_0

    .line 2219
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v96

    .line 2220
    .restart local v96    # "b":Landroid/os/Bundle;
    const-string v1, "int_requestStatus"

    move-object/from16 v0, v96

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v97

    .line 2221
    .local v97, "requestStatus":I
    invoke-static/range {v97 .. v97}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;

    move-result-object v1

    move-object/from16 v0, v98

    invoke-interface {v0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;->onNewRequestFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;)V

    goto/16 :goto_0

    .line 1926
    nop

    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public requestCommandBurst(I[BLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 6
    .param p1, "requestedCommandId"    # I
    .param p2, "commandData"    # [B
    .param p3, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 2829
    const-string/jumbo v1, "requestCommandBurst"

    .line 2830
    .local v1, "cmdName":Ljava/lang/String;
    const/16 v2, 0x68

    .line 2831
    .local v2, "whatCmd":I
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2832
    .local v3, "params":Landroid/os/Bundle;
    const-string v0, "int_requestedCommandId"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2833
    const-string v0, "arrayByte_commandData"

    invoke-virtual {v3, v0, p2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 2835
    const/16 v0, 0x4eee

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v0, p0

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method public requestCrankParameters(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 2778
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    invoke-virtual {p0, p1, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestCrankParameters(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestCrankParameters(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;)Z
    .locals 3
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "crankParametersReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    .prologue
    .line 2790
    const-string/jumbo v0, "requestCrankParameters"

    .line 2791
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v1, 0x4e26

    .line 2793
    .local v1, "whatCmd":I
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestCrankParametersEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;)V

    .line 2795
    invoke-virtual {p0, v0, v1, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->sendRequestCommand(Ljava/lang/String;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v2

    return v2
.end method

.method public requestCustomCalibrationParameters([BLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "manufacturerSpecificParameters"    # [B
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 2711
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    invoke-virtual {p0, p1, p2, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestCustomCalibrationParameters([BLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestCustomCalibrationParameters([BLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)Z
    .locals 4
    .param p1, "manufacturerSpecificParameters"    # [B
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p3, "calibrationMessageReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    .prologue
    .line 2724
    const-string/jumbo v0, "requestCustomCalibrationParameters"

    .line 2725
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e24

    .line 2726
    .local v2, "whatCmd":I
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2727
    .local v1, "params":Landroid/os/Bundle;
    const-string v3, "arrayByte_manufacturerSpecificParameters"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 2729
    invoke-direct {p0, p3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestCalibrationMessageEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)V

    .line 2731
    invoke-virtual {p0, v0, v2, v1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v3

    return v3
.end method

.method public requestManualCalibration(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 2
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 2608
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    invoke-virtual {p0, p1, v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestManualCalibration(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestManualCalibration(Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;)Z
    .locals 3
    .param p1, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p2, "calibrationMessageReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;
    .param p3, "measurementOutputDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    .prologue
    .line 2621
    const-string/jumbo v0, "requestManualCalibration"

    .line 2622
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v1, 0x4e21

    .line 2624
    .local v1, "whatCmd":I
    invoke-direct {p0, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestCalibrationMessageEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)V

    .line 2625
    invoke-direct {p0, p3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestMeasurementOutputDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;)V

    .line 2627
    invoke-virtual {p0, v0, v1, p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->sendRequestCommand(Ljava/lang/String;ILcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v2

    return v2
.end method

.method public requestSetAutoZero(ZLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 2
    .param p1, "autoZeroEnable"    # Z
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 2641
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestSetAutoZero(ZLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetAutoZero(ZLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;)Z
    .locals 4
    .param p1, "autoZeroEnable"    # Z
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p3, "calibrationMessageReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;
    .param p4, "autoZeroStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    .prologue
    .line 2655
    const-string/jumbo v0, "requestSetAutoZero"

    .line 2656
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e22

    .line 2657
    .local v2, "whatCmd":I
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2658
    .local v1, "params":Landroid/os/Bundle;
    const-string v3, "bool_autoZeroEnable"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2660
    invoke-direct {p0, p3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestCalibrationMessageEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)V

    .line 2661
    invoke-direct {p0, p4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestAutoZeroStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;)V

    .line 2663
    invoke-virtual {p0, v0, v2, v1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v3

    return v3
.end method

.method public requestSetCrankParameters(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 5
    .param p1, "crankLengthSetting"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;
    .param p2, "fullCrankLength"    # Ljava/math/BigDecimal;
    .param p3, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 2808
    const-string/jumbo v0, "requestSetCrankParameters"

    .line 2809
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e27

    .line 2810
    .local v2, "whatCmd":I
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2811
    .local v1, "params":Landroid/os/Bundle;
    const-string v3, "int_crankLengthSetting"

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthSetting;->getIntValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2812
    const-string v3, "decimal_fullCrankLength"

    invoke-virtual {v1, v3, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2814
    invoke-virtual {p0, v0, v2, v1, p3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v3

    return v3
.end method

.method public requestSetCtfSlope(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "slope"    # Ljava/math/BigDecimal;
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 2677
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    invoke-virtual {p0, p1, p2, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestSetCtfSlope(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetCtfSlope(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)Z
    .locals 4
    .param p1, "slope"    # Ljava/math/BigDecimal;
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p3, "calibrationMessageReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    .prologue
    .line 2690
    const-string/jumbo v0, "requestSetCtfSlope"

    .line 2691
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e23

    .line 2692
    .local v2, "whatCmd":I
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2693
    .local v1, "params":Landroid/os/Bundle;
    const-string v3, "decimal_slope"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2695
    invoke-direct {p0, p3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestCalibrationMessageEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)V

    .line 2697
    invoke-virtual {p0, v0, v2, v1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v3

    return v3
.end method

.method public requestSetCustomCalibrationParameters([BLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z
    .locals 1
    .param p1, "manufacturerSpecificParameters"    # [B
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;

    .prologue
    .line 2745
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    invoke-virtual {p0, p1, p2, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->requestSetCustomCalibrationParameters([BLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)Z

    move-result v0

    return v0
.end method

.method public requestSetCustomCalibrationParameters([BLcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)Z
    .locals 4
    .param p1, "manufacturerSpecificParameters"    # [B
    .param p2, "requestFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;
    .param p3, "calibrationMessageReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    .prologue
    .line 2758
    const-string/jumbo v0, "requestSetCustomCalibrationParameters"

    .line 2759
    .local v0, "cmdName":Ljava/lang/String;
    const/16 v2, 0x4e25

    .line 2760
    .local v2, "whatCmd":I
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2761
    .local v1, "params":Landroid/os/Bundle;
    const-string v3, "arrayByte_manufacturerSpecificParameters"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 2763
    invoke-direct {p0, p3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeRequestCalibrationMessageEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)V

    .line 2765
    invoke-virtual {p0, v0, v2, v1, p2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->sendRequestCommand(Ljava/lang/String;ILandroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusCommonPcc$IRequestFinishedReceiver;)Z

    move-result v3

    return v3
.end method

.method public subscribeAutoZeroStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;)V
    .locals 2
    .param p1, "autoZeroStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    .prologue
    const/16 v1, 0xd2

    .line 2421
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    if-nez v0, :cond_0

    .line 2423
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    if-nez v0, :cond_1

    .line 2424
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2428
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    .line 2429
    return-void

    .line 2425
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mAutoZeroStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IAutoZeroStatusReceiver;

    if-eqz v0, :cond_0

    .line 2426
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeCalculatedCrankCadenceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedCrankCadenceReceiver;)V
    .locals 1
    .param p1, "CalculatedCrankCadenceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedCrankCadenceReceiver;

    .prologue
    const/16 v0, 0xd5

    .line 2489
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedCrankCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedCrankCadenceReceiver;

    .line 2490
    if-eqz p1, :cond_0

    .line 2492
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2498
    :goto_0
    return-void

    .line 2496
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeCalculatedPowerEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedPowerReceiver;)V
    .locals 1
    .param p1, "CalculatedPowerReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedPowerReceiver;

    .prologue
    const/16 v0, 0xd3

    .line 2451
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedPowerReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedPowerReceiver;

    .line 2452
    if-eqz p1, :cond_0

    .line 2454
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2460
    :goto_0
    return-void

    .line 2458
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeCalculatedTorqueEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedTorqueReceiver;)V
    .locals 1
    .param p1, "CalculatedTorqueReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedTorqueReceiver;

    .prologue
    const/16 v0, 0xd4

    .line 2470
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedTorqueReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalculatedTorqueReceiver;

    .line 2471
    if-eqz p1, :cond_0

    .line 2473
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2479
    :goto_0
    return-void

    .line 2477
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeCalculatedWheelDistanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelDistanceReceiver;)V
    .locals 1
    .param p1, "CalculatedWheelDistanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelDistanceReceiver;

    .prologue
    const/16 v0, 0xd7

    .line 2527
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedWheelDistanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelDistanceReceiver;

    .line 2528
    if-eqz p1, :cond_0

    .line 2530
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2536
    :goto_0
    return-void

    .line 2534
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeCalculatedWheelSpeedEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;)V
    .locals 1
    .param p1, "CalculatedWheelSpeedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;

    .prologue
    const/16 v0, 0xd6

    .line 2508
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalculatedWheelSpeedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CalculatedWheelSpeedReceiver;

    .line 2509
    if-eqz p1, :cond_0

    .line 2511
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2517
    :goto_0
    return-void

    .line 2515
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeCalibrationMessageEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;)V
    .locals 2
    .param p1, "calibrationMessageReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    .prologue
    const/16 v1, 0xd1

    .line 2391
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    if-nez v0, :cond_0

    .line 2393
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    if-nez v0, :cond_1

    .line 2394
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2398
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    .line 2399
    return-void

    .line 2395
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCalibrationMessageReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICalibrationMessageReceiver;

    if-eqz v0, :cond_0

    .line 2396
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeCrankParametersEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;)V
    .locals 2
    .param p1, "crankParametersReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    .prologue
    const/16 v1, 0xd9

    .line 2576
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    if-nez v0, :cond_0

    .line 2578
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    if-nez v0, :cond_1

    .line 2579
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2583
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    .line 2584
    return-void

    .line 2580
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mCrankParametersReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ICrankParametersReceiver;

    if-eqz v0, :cond_0

    .line 2581
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeInstantaneousCadenceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IInstantaneousCadenceReceiver;)V
    .locals 1
    .param p1, "InstantaneousCadenceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IInstantaneousCadenceReceiver;

    .prologue
    const/16 v0, 0xcb

    .line 2277
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mInstantaneousCadenceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IInstantaneousCadenceReceiver;

    .line 2278
    if-eqz p1, :cond_0

    .line 2280
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2286
    :goto_0
    return-void

    .line 2284
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeMeasurementOutputDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;)V
    .locals 2
    .param p1, "measurementOutputDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    .prologue
    const/16 v1, 0xd8

    .line 2546
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRequestMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    if-nez v0, :cond_1

    .line 2548
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    if-nez v0, :cond_0

    .line 2549
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2550
    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    if-eqz v0, :cond_1

    .line 2551
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    .line 2553
    :cond_1
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mMeasurementOutputDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IMeasurementOutputDataReceiver;

    .line 2554
    return-void
.end method

.method public subscribePedalPowerBalanceEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalPowerBalanceReceiver;)V
    .locals 1
    .param p1, "PedalPowerBalanceReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalPowerBalanceReceiver;

    .prologue
    const/16 v0, 0xca

    .line 2258
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mPedalPowerBalanceReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalPowerBalanceReceiver;

    .line 2259
    if-eqz p1, :cond_0

    .line 2261
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2267
    :goto_0
    return-void

    .line 2265
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribePedalSmoothnessEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalSmoothnessReceiver;)V
    .locals 1
    .param p1, "PedalSmoothnessReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalSmoothnessReceiver;

    .prologue
    const/16 v0, 0xcf

    .line 2353
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mPedalSmoothnessReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IPedalSmoothnessReceiver;

    .line 2354
    if-eqz p1, :cond_0

    .line 2356
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2362
    :goto_0
    return-void

    .line 2360
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeRawCrankTorqueDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCrankTorqueDataReceiver;)V
    .locals 1
    .param p1, "RawCrankTorqueDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCrankTorqueDataReceiver;

    .prologue
    const/16 v0, 0xcd

    .line 2315
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawCrankTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCrankTorqueDataReceiver;

    .line 2316
    if-eqz p1, :cond_0

    .line 2318
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2324
    :goto_0
    return-void

    .line 2322
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeRawCtfDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCtfDataReceiver;)V
    .locals 1
    .param p1, "RawCtfDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCtfDataReceiver;

    .prologue
    const/16 v0, 0xd0

    .line 2372
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawCtfDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawCtfDataReceiver;

    .line 2373
    if-eqz p1, :cond_0

    .line 2375
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2381
    :goto_0
    return-void

    .line 2379
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeRawPowerOnlyDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawPowerOnlyDataReceiver;)V
    .locals 1
    .param p1, "RawPowerOnlyDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawPowerOnlyDataReceiver;

    .prologue
    const/16 v0, 0xc9

    .line 2239
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawPowerOnlyDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawPowerOnlyDataReceiver;

    .line 2240
    if-eqz p1, :cond_0

    .line 2242
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2248
    :goto_0
    return-void

    .line 2246
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeRawWheelTorqueDataEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawWheelTorqueDataReceiver;)V
    .locals 1
    .param p1, "RawWheelTorqueDataReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawWheelTorqueDataReceiver;

    .prologue
    const/16 v0, 0xcc

    .line 2296
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mRawWheelTorqueDataReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$IRawWheelTorqueDataReceiver;

    .line 2297
    if-eqz p1, :cond_0

    .line 2299
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2305
    :goto_0
    return-void

    .line 2303
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method public subscribeTorqueEffectivenessEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ITorqueEffectivenessReceiver;)V
    .locals 1
    .param p1, "TorqueEffectivenessReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ITorqueEffectivenessReceiver;

    .prologue
    const/16 v0, 0xce

    .line 2334
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->mTorqueEffectivenessReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$ITorqueEffectivenessReceiver;

    .line 2335
    if-eqz p1, :cond_0

    .line 2337
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->subscribeToEvent(I)Z

    .line 2343
    :goto_0
    return-void

    .line 2341
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
