.class public final Lcom/dsi/ant/channel/BurstState;
.super Ljava/lang/Object;
.source "BurstState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;,
        Lcom/dsi/ant/channel/BurstState$BundleData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/BurstState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBundleData:Lcom/dsi/ant/channel/BurstState$BundleData;

.field private mProcessing:Z

.field private mTransmitInProgress:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 253
    new-instance v0, Lcom/dsi/ant/channel/BurstState$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/BurstState$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/BurstState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, v0, v0}, Lcom/dsi/ant/channel/BurstState;-><init>(ZZ)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    new-instance v1, Lcom/dsi/ant/channel/BurstState$BundleData;

    invoke-direct {v1}, Lcom/dsi/ant/channel/BurstState$BundleData;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/channel/BurstState;->mBundleData:Lcom/dsi/ant/channel/BurstState$BundleData;

    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 193
    .local v0, "version":I
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/BurstState;->createFromParcelVersionInitial(Landroid/os/Parcel;)V

    .line 195
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 196
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/BurstState;->createFromParcelVersionWithBundle(Landroid/os/Parcel;)V

    .line 198
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/dsi/ant/channel/BurstState$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/dsi/ant/channel/BurstState$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/BurstState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(ZZ)V
    .locals 1
    .param p1, "processing"    # Z
    .param p2, "transmitInProgress"    # Z

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    new-instance v0, Lcom/dsi/ant/channel/BurstState$BundleData;

    invoke-direct {v0}, Lcom/dsi/ant/channel/BurstState$BundleData;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/channel/BurstState;->mBundleData:Lcom/dsi/ant/channel/BurstState$BundleData;

    .line 37
    iput-boolean p1, p0, Lcom/dsi/ant/channel/BurstState;->mProcessing:Z

    .line 38
    iput-boolean p2, p0, Lcom/dsi/ant/channel/BurstState;->mTransmitInProgress:Z

    .line 39
    return-void
.end method

.method private createFromParcelVersionInitial(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 201
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 203
    .local v1, "detailsArraySize":I
    invoke-static {}, Lcom/dsi/ant/channel/BurstState;->getBurstStateArraySize()I

    move-result v3

    if-le v1, v3, :cond_0

    move v2, v1

    .line 206
    .local v2, "requiredArraySize":I
    :goto_0
    new-array v0, v2, [Z

    .line 207
    .local v0, "detailsArray":[Z
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 209
    sget-object v3, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->PROCESSING:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->ordinal()I

    move-result v3

    aget-boolean v3, v0, v3

    iput-boolean v3, p0, Lcom/dsi/ant/channel/BurstState;->mProcessing:Z

    .line 210
    sget-object v3, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->TRANSMIT_IN_PROGRESS:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->ordinal()I

    move-result v3

    aget-boolean v3, v0, v3

    iput-boolean v3, p0, Lcom/dsi/ant/channel/BurstState;->mTransmitInProgress:Z

    .line 211
    return-void

    .line 203
    .end local v0    # "detailsArray":[Z
    .end local v2    # "requiredArraySize":I
    :cond_0
    invoke-static {}, Lcom/dsi/ant/channel/BurstState;->getBurstStateArraySize()I

    move-result v2

    goto :goto_0
.end method

.method private createFromParcelVersionWithBundle(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 214
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 215
    .local v0, "received":Landroid/os/Bundle;
    const-class v1, Lcom/dsi/ant/channel/BurstState$BundleData;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 216
    const-string v1, "com.dsi.ant.channel.burststate.bundledata"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/channel/BurstState$BundleData;

    iput-object v1, p0, Lcom/dsi/ant/channel/BurstState;->mBundleData:Lcom/dsi/ant/channel/BurstState$BundleData;

    .line 217
    return-void
.end method

.method static getBurstStateArraySize()I
    .locals 1

    .prologue
    .line 267
    sget-object v0, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->ordinal()I

    move-result v0

    return v0
.end method

.method private writeToParcelVersionInitial(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;

    .prologue
    .line 236
    invoke-static {}, Lcom/dsi/ant/channel/BurstState;->getBurstStateArraySize()I

    move-result v1

    new-array v0, v1, [Z

    .line 238
    .local v0, "detailsArray":[Z
    sget-object v1, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->PROCESSING:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/channel/BurstState;->mProcessing:Z

    aput-boolean v2, v0, v1

    .line 239
    sget-object v1, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->TRANSMIT_IN_PROGRESS:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/channel/BurstState;->mTransmitInProgress:Z

    aput-boolean v2, v0, v1

    .line 242
    invoke-static {}, Lcom/dsi/ant/channel/BurstState;->getBurstStateArraySize()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 243
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 244
    return-void
.end method

.method private writeToParcelVersionWithBundle(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;

    .prologue
    .line 247
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 248
    .local v0, "bundleVersion":Landroid/os/Bundle;
    const-string v1, "com.dsi.ant.channel.burststate.bundledata"

    iget-object v2, p0, Lcom/dsi/ant/channel/BurstState;->mBundleData:Lcom/dsi/ant/channel/BurstState$BundleData;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 249
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 250
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    if-ne p0, p1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v1

    .line 92
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 93
    goto :goto_0

    .line 96
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/channel/BurstState;

    if-nez v3, :cond_3

    move v1, v2

    .line 97
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 100
    check-cast v0, Lcom/dsi/ant/channel/BurstState;

    .line 102
    .local v0, "other":Lcom/dsi/ant/channel/BurstState;
    iget-boolean v3, v0, Lcom/dsi/ant/channel/BurstState;->mProcessing:Z

    iget-boolean v4, p0, Lcom/dsi/ant/channel/BurstState;->mProcessing:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, v0, Lcom/dsi/ant/channel/BurstState;->mTransmitInProgress:Z

    iget-boolean v4, p0, Lcom/dsi/ant/channel/BurstState;->mTransmitInProgress:Z

    if-ne v3, v4, :cond_4

    iget-object v3, v0, Lcom/dsi/ant/channel/BurstState;->mBundleData:Lcom/dsi/ant/channel/BurstState$BundleData;

    # getter for: Lcom/dsi/ant/channel/BurstState$BundleData;->mMaxBurstSize:I
    invoke-static {v3}, Lcom/dsi/ant/channel/BurstState$BundleData;->access$000(Lcom/dsi/ant/channel/BurstState$BundleData;)I

    move-result v3

    iget-object v4, p0, Lcom/dsi/ant/channel/BurstState;->mBundleData:Lcom/dsi/ant/channel/BurstState$BundleData;

    # getter for: Lcom/dsi/ant/channel/BurstState$BundleData;->mMaxBurstSize:I
    invoke-static {v4}, Lcom/dsi/ant/channel/BurstState$BundleData;->access$000(Lcom/dsi/ant/channel/BurstState$BundleData;)I

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    .line 105
    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 75
    const/16 v0, 0x1f

    .line 76
    .local v0, "prime":I
    const/4 v1, 0x7

    .line 78
    .local v1, "result":I
    iget-boolean v2, p0, Lcom/dsi/ant/channel/BurstState;->mProcessing:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit16 v1, v2, 0xd9

    .line 79
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v5, p0, Lcom/dsi/ant/channel/BurstState;->mTransmitInProgress:Z

    if-eqz v5, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 80
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lcom/dsi/ant/channel/BurstState;->mBundleData:Lcom/dsi/ant/channel/BurstState$BundleData;

    # getter for: Lcom/dsi/ant/channel/BurstState$BundleData;->mMaxBurstSize:I
    invoke-static {v3}, Lcom/dsi/ant/channel/BurstState$BundleData;->access$000(Lcom/dsi/ant/channel/BurstState$BundleData;)I

    move-result v3

    add-int v1, v2, v3

    .line 82
    return v1

    :cond_0
    move v2, v4

    .line 78
    goto :goto_0

    :cond_1
    move v3, v4

    .line 79
    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Burst State:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 116
    .local v0, "infoStringBuilder":Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Lcom/dsi/ant/channel/BurstState;->mTransmitInProgress:Z

    if-eqz v1, :cond_0

    .line 117
    const-string v1, " -Transmit In Progress"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    :goto_0
    iget-boolean v1, p0, Lcom/dsi/ant/channel/BurstState;->mProcessing:Z

    if-eqz v1, :cond_1

    .line 123
    const-string v1, " -Burst Processing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    :goto_1
    const-string v1, " -Max Burst Size: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/channel/BurstState;->mBundleData:Lcom/dsi/ant/channel/BurstState$BundleData;

    # getter for: Lcom/dsi/ant/channel/BurstState$BundleData;->mMaxBurstSize:I
    invoke-static {v2}, Lcom/dsi/ant/channel/BurstState$BundleData;->access$000(Lcom/dsi/ant/channel/BurstState$BundleData;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 119
    :cond_0
    const-string v1, " -No Transmit In Progress"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 125
    :cond_1
    const-string v1, " -No Burst Processing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 226
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 228
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/BurstState;->writeToParcelVersionInitial(Landroid/os/Parcel;)V

    .line 230
    invoke-static {}, Lcom/dsi/ant/AntService;->requiresBundle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/BurstState;->writeToParcelVersionWithBundle(Landroid/os/Parcel;)V

    .line 233
    :cond_0
    return-void
.end method
