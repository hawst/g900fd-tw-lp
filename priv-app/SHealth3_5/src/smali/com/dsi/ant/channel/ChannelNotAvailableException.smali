.class public final Lcom/dsi/ant/channel/ChannelNotAvailableException;
.super Ljava/lang/Exception;
.source "ChannelNotAvailableException.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/ChannelNotAvailableException;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0x2119566c8224d47eL


# instance fields
.field public final reasonCode:Lcom/dsi/ant/channel/ChannelNotAvailableReason;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/dsi/ant/channel/ChannelNotAvailableException$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/ChannelNotAvailableException$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/ChannelNotAvailableException;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->create(I)Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/dsi/ant/channel/ChannelNotAvailableException;-><init>(Lcom/dsi/ant/channel/ChannelNotAvailableReason;Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/dsi/ant/channel/ChannelNotAvailableException$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/dsi/ant/channel/ChannelNotAvailableException$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/ChannelNotAvailableException;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/channel/ChannelNotAvailableReason;)V
    .locals 2
    .param p1, "reason"    # Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Could not acquire channel. Reason = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 39
    iput-object p1, p0, Lcom/dsi/ant/channel/ChannelNotAvailableException;->reasonCode:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/channel/ChannelNotAvailableReason;Ljava/lang/String;)V
    .locals 0
    .param p1, "reason"    # Lcom/dsi/ant/channel/ChannelNotAvailableReason;
    .param p2, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 50
    iput-object p1, p0, Lcom/dsi/ant/channel/ChannelNotAvailableException;->reasonCode:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    .line 51
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lcom/dsi/ant/channel/ChannelNotAvailableException;->reasonCode:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->getRawValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    invoke-virtual {p0}, Lcom/dsi/ant/channel/ChannelNotAvailableException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    return-void
.end method
