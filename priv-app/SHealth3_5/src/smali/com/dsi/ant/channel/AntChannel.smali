.class public final Lcom/dsi/ant/channel/AntChannel;
.super Ljava/lang/Object;
.source "AntChannel.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/AntChannel;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private volatile mAllowChannelEvents:Z

.field private final mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

.field private final mChannelEventDispatcher:Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/AntChannel;->TAG:Ljava/lang/String;

    .line 1032
    new-instance v0, Lcom/dsi/ant/channel/AntChannel$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/AntChannel$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/AntChannel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;)V
    .locals 2
    .param p1, "communicator"    # Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/channel/AntChannel$1;)V

    iput-object v0, p0, Lcom/dsi/ant/channel/AntChannel;->mChannelEventDispatcher:Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAllowChannelEvents:Z

    .line 89
    if-nez p1, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Channel communicator provided  was null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAllowChannelEvents:Z

    .line 95
    return-void
.end method

.method static synthetic access$100(Lcom/dsi/ant/channel/AntChannel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/AntChannel;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAllowChannelEvents:Z

    return v0
.end method

.method private static checkResult(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Landroid/os/Bundle;)V
    .locals 2
    .param p0, "attemptedMessageType"    # Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .param p1, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 834
    invoke-static {p1}, Lcom/dsi/ant/channel/ipc/ServiceResult;->readFrom(Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/ServiceResult;

    move-result-object v0

    .line 836
    .local v0, "serviceResult":Lcom/dsi/ant/channel/ipc/ServiceResult;
    invoke-virtual {v0}, Lcom/dsi/ant/channel/ipc/ServiceResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 847
    return-void

    .line 838
    :cond_0
    invoke-virtual {v0}, Lcom/dsi/ant/channel/ipc/ServiceResult;->channelExists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 841
    new-instance v1, Landroid/os/DeadObjectException;

    invoke-direct {v1}, Landroid/os/DeadObjectException;-><init>()V

    throw v1

    .line 845
    :cond_1
    new-instance v1, Lcom/dsi/ant/channel/AntCommandFailedException;

    invoke-direct {v1, p0, v0}, Lcom/dsi/ant/channel/AntCommandFailedException;-><init>(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Lcom/dsi/ant/channel/ipc/ServiceResult;)V

    throw v1
.end method

.method private requestMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .locals 4
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 531
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 533
    .local v1, "error":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v3, p1, v1}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->requestMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Landroid/os/Bundle;)Lcom/dsi/ant/message/ipc/AntMessageParcel;

    move-result-object v2

    .line 536
    .local v2, "requestedMessageParcel":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    :try_start_0
    sget-object v3, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->REQUEST_MESSAGE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-static {v3, v1}, Lcom/dsi/ant/channel/AntChannel;->checkResult(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 543
    invoke-static {v2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v3

    return-object v3

    .line 537
    :catch_0
    move-exception v0

    .line 540
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    throw v0
.end method


# virtual methods
.method public assign(Lcom/dsi/ant/message/ChannelType;Lcom/dsi/ant/message/ExtendedAssignment;)V
    .locals 2
    .param p1, "channelType"    # Lcom/dsi/ant/message/ChannelType;
    .param p2, "extendedAssignment"    # Lcom/dsi/ant/message/ExtendedAssignment;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 200
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 202
    .local v0, "error":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v1, p1, p2, v0}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->assign(Lcom/dsi/ant/message/ChannelType;Lcom/dsi/ant/message/ExtendedAssignment;Landroid/os/Bundle;)V

    .line 204
    sget-object v1, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-static {v1, v0}, Lcom/dsi/ant/channel/AntChannel;->checkResult(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Landroid/os/Bundle;)V

    .line 205
    return-void
.end method

.method public clearChannelEventHandler()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 884
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v0}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->clearChannelEventHandler()V

    .line 885
    return-void
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 509
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 511
    .local v0, "error":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v1, v0}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->close(Landroid/os/Bundle;)V

    .line 513
    sget-object v1, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CLOSE_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-static {v1, v0}, Lcom/dsi/ant/channel/AntChannel;->checkResult(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Landroid/os/Bundle;)V

    .line 514
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1016
    const/4 v0, 0x0

    return v0
.end method

.method public disableEventBuffer()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 817
    sget-object v0, Lcom/dsi/ant/channel/EventBufferSettings;->DISABLE_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/AntChannel;->setEventBuffer(Lcom/dsi/ant/channel/EventBufferSettings;)V

    .line 818
    return-void
.end method

.method public getCapabilities()Lcom/dsi/ant/channel/Capabilities;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 933
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v0}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public open()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 484
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 486
    .local v0, "error":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v1, v0}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->open(Landroid/os/Bundle;)V

    .line 488
    sget-object v1, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OPEN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-static {v1, v0}, Lcom/dsi/ant/channel/AntChannel;->checkResult(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Landroid/os/Bundle;)V

    .line 489
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 999
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAllowChannelEvents:Z

    .line 1000
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v0}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->releaseChannel()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1008
    :goto_0
    return-void

    .line 1001
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 558
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_STATUS:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-direct {p0, v0}, Lcom/dsi/ant/channel/AntChannel;->requestMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    return-object v0
.end method

.method public setAdapterWideLibConfig(Lcom/dsi/ant/message/LibConfig;)V
    .locals 3
    .param p1, "libConfig"    # Lcom/dsi/ant/message/LibConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 728
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 730
    .local v0, "error":Landroid/os/Bundle;
    invoke-static {}, Lcom/dsi/ant/AntService;->hasAdapterWideConfigurationSupport()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 731
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v2, p1, v0}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->setLibConfig(Lcom/dsi/ant/message/LibConfig;Landroid/os/Bundle;)V

    .line 739
    :goto_0
    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LIB_CONFIG:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-static {v2, v0}, Lcom/dsi/ant/channel/AntChannel;->checkResult(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Landroid/os/Bundle;)V

    .line 740
    return-void

    .line 735
    :cond_0
    new-instance v1, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v1, v2}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 736
    .local v1, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    invoke-virtual {v1, v0}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public setChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V
    .locals 2
    .param p1, "eventHandler"    # Lcom/dsi/ant/channel/IAntChannelEventHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 863
    if-nez p1, :cond_0

    .line 864
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAllowChannelEvents:Z

    .line 865
    invoke-virtual {p0}, Lcom/dsi/ant/channel/AntChannel;->clearChannelEventHandler()V

    .line 873
    :goto_0
    return-void

    .line 869
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel;->mChannelEventDispatcher:Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;

    # invokes: Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;->setReceivedChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V
    invoke-static {v0, p1}, Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;->access$200(Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;Lcom/dsi/ant/channel/IAntChannelEventHandler;)V

    .line 870
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannel;->mChannelEventDispatcher:Lcom/dsi/ant/channel/AntChannel$ChannelEventDispatcher;

    invoke-interface {v0, v1}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->setChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V

    .line 872
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAllowChannelEvents:Z

    goto :goto_0
.end method

.method public setChannelId(Lcom/dsi/ant/message/ChannelId;)V
    .locals 2
    .param p1, "channelId"    # Lcom/dsi/ant/message/ChannelId;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 244
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 246
    .local v0, "error":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v1, p1, v0}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->setId(Lcom/dsi/ant/message/ChannelId;Landroid/os/Bundle;)V

    .line 248
    sget-object v1, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_ID:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-static {v1, v0}, Lcom/dsi/ant/channel/AntChannel;->checkResult(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Landroid/os/Bundle;)V

    .line 249
    return-void
.end method

.method public setEventBuffer(Lcom/dsi/ant/channel/EventBufferSettings;)V
    .locals 3
    .param p1, "eventBufferSettings"    # Lcom/dsi/ant/channel/EventBufferSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 772
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 774
    .local v0, "error":Landroid/os/Bundle;
    invoke-static {}, Lcom/dsi/ant/AntService;->hasAdapterWideConfigurationSupport()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 775
    iget-object v2, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v2, p1, v0}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->setEventBuffer(Lcom/dsi/ant/channel/EventBufferSettings;Landroid/os/Bundle;)V

    .line 783
    :goto_0
    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_EVENT_BUFFER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-static {v2, v0}, Lcom/dsi/ant/channel/AntChannel;->checkResult(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Landroid/os/Bundle;)V

    .line 784
    return-void

    .line 779
    :cond_0
    new-instance v1, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v1, v2}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    .line 780
    .local v1, "result":Lcom/dsi/ant/channel/ipc/ServiceResult;
    invoke-virtual {v1, v0}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeTo(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public setEventBufferToDefault()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 799
    sget-object v0, Lcom/dsi/ant/channel/EventBufferSettings;->DEFAULT_EVENT_BUFFER_SETTINGS:Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/AntChannel;->setEventBuffer(Lcom/dsi/ant/channel/EventBufferSettings;)V

    .line 800
    return-void
.end method

.method public setRfFrequency(I)V
    .locals 2
    .param p1, "radioFrequencyOffset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 294
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 296
    .local v0, "error":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v1, p1, v0}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->setRfFrequency(ILandroid/os/Bundle;)V

    .line 298
    sget-object v1, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_RF_FREQUENCY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-static {v1, v0}, Lcom/dsi/ant/channel/AntChannel;->checkResult(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Landroid/os/Bundle;)V

    .line 299
    return-void
.end method

.method public unassign()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 222
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 224
    .local v0, "error":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    invoke-interface {v1, v0}, Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;->unassign(Landroid/os/Bundle;)V

    .line 226
    sget-object v1, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->UNASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-static {v1, v0}, Lcom/dsi/ant/channel/AntChannel;->checkResult(Lcom/dsi/ant/message/fromhost/MessageFromHostType;Landroid/os/Bundle;)V

    .line 227
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    instance-of v0, v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    if-eqz v0, :cond_0

    .line 1022
    iget-object v0, p0, Lcom/dsi/ant/channel/AntChannel;->mAntChannelCommunicator:Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;

    check-cast v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    invoke-virtual {v0, p1, p2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1026
    :goto_0
    return-void

    .line 1024
    :cond_0
    sget-object v0, Lcom/dsi/ant/channel/AntChannel;->TAG:Ljava/lang/String;

    const-string v1, "Could not parcel, unknown IPC communicator type"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
