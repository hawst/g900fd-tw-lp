.class final enum Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;
.super Ljava/lang/Enum;
.source "BurstState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/BurstState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "BurstStateArrayIndex"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

.field public static final enum NUMBER_OF_DETAILS:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

.field public static final enum PROCESSING:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

.field public static final enum TRANSMIT_IN_PROGRESS:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 186
    new-instance v0, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    const-string v1, "PROCESSING"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->PROCESSING:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    .line 187
    new-instance v0, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    const-string v1, "TRANSMIT_IN_PROGRESS"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->TRANSMIT_IN_PROGRESS:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    .line 188
    new-instance v0, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    const-string v1, "NUMBER_OF_DETAILS"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    .line 185
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    sget-object v1, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->PROCESSING:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->TRANSMIT_IN_PROGRESS:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->$VALUES:[Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 185
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 185
    const-class v0, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->$VALUES:[Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/BurstState$BurstStateArrayIndex;

    return-object v0
.end method
