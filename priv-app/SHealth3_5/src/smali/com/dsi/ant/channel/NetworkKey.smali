.class public Lcom/dsi/ant/channel/NetworkKey;
.super Ljava/lang/Object;
.source "NetworkKey.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/NetworkKey$BundleData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/NetworkKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBundleData:Lcom/dsi/ant/channel/NetworkKey$BundleData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 177
    new-instance v0, Lcom/dsi/ant/channel/NetworkKey$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/NetworkKey$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/NetworkKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v1, Lcom/dsi/ant/channel/NetworkKey$BundleData;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/dsi/ant/channel/NetworkKey$BundleData;-><init>(Lcom/dsi/ant/channel/NetworkKey$1;)V

    iput-object v1, p0, Lcom/dsi/ant/channel/NetworkKey;->mBundleData:Lcom/dsi/ant/channel/NetworkKey$BundleData;

    .line 158
    const-class v1, Lcom/dsi/ant/channel/NetworkKey$BundleData;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v0

    .line 160
    .local v0, "received":Landroid/os/Bundle;
    const-string v1, "com.dsi.ant.channel.networkkey.bundledata"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/channel/NetworkKey$BundleData;

    iput-object v1, p0, Lcom/dsi/ant/channel/NetworkKey;->mBundleData:Lcom/dsi/ant/channel/NetworkKey$BundleData;

    .line 162
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/dsi/ant/channel/NetworkKey$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/dsi/ant/channel/NetworkKey$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/NetworkKey;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 67
    if-ne p0, p1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v1

    .line 71
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 72
    goto :goto_0

    .line 75
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/channel/NetworkKey;

    if-nez v3, :cond_3

    move v1, v2

    .line 76
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 79
    check-cast v0, Lcom/dsi/ant/channel/NetworkKey;

    .line 81
    .local v0, "other":Lcom/dsi/ant/channel/NetworkKey;
    invoke-virtual {v0}, Lcom/dsi/ant/channel/NetworkKey;->getRawNetworkKey()[B

    move-result-object v3

    invoke-virtual {p0}, Lcom/dsi/ant/channel/NetworkKey;->getRawNetworkKey()[B

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 82
    goto :goto_0
.end method

.method public getRawNetworkKey()[B
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/dsi/ant/channel/NetworkKey;->mBundleData:Lcom/dsi/ant/channel/NetworkKey$BundleData;

    iget-object v0, v0, Lcom/dsi/ant/channel/NetworkKey$BundleData;->networkKey:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dsi/ant/channel/NetworkKey;->mBundleData:Lcom/dsi/ant/channel/NetworkKey$BundleData;

    iget-object v0, v0, Lcom/dsi/ant/channel/NetworkKey$BundleData;->networkKey:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Network Key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/channel/NetworkKey;->mBundleData:Lcom/dsi/ant/channel/NetworkKey$BundleData;

    iget-object v1, v1, Lcom/dsi/ant/channel/NetworkKey$BundleData;->networkKey:[B

    invoke-static {v1}, Lcom/dsi/ant/message/MessageUtils;->getHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 171
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 172
    .local v0, "out":Landroid/os/Bundle;
    const-string v1, "com.dsi.ant.channel.networkkey.bundledata"

    iget-object v2, p0, Lcom/dsi/ant/channel/NetworkKey;->mBundleData:Lcom/dsi/ant/channel/NetworkKey$BundleData;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 173
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 174
    return-void
.end method
