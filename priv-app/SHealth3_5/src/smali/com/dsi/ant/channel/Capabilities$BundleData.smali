.class final Lcom/dsi/ant/channel/Capabilities$BundleData;
.super Ljava/lang/Object;
.source "Capabilities.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/Capabilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BundleData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/Capabilities$BundleData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mEventBuffering:Z

.field private mRfFrequencyMax:I

.field private mRfFrequencyMin:I

.field private mRssi:Z

.field private mWildcardIdList:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 430
    new-instance v0, Lcom/dsi/ant/channel/Capabilities$BundleData$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/Capabilities$BundleData$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/Capabilities$BundleData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386
    iput-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z

    .line 387
    iput-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z

    .line 388
    iput-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z

    .line 389
    const/4 v0, 0x2

    iput v0, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMin:I

    .line 390
    const/16 v0, 0x50

    iput v0, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMax:I

    .line 393
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities$BundleData;

    .prologue
    .line 384
    iget-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z

    return v0
.end method

.method static synthetic access$002(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities$BundleData;
    .param p1, "x1"    # Z

    .prologue
    .line 384
    iput-boolean p1, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z

    return p1
.end method

.method static synthetic access$100(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities$BundleData;

    .prologue
    .line 384
    iget-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z

    return v0
.end method

.method static synthetic access$102(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities$BundleData;
    .param p1, "x1"    # Z

    .prologue
    .line 384
    iput-boolean p1, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z

    return p1
.end method

.method static synthetic access$200(Lcom/dsi/ant/channel/Capabilities$BundleData;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities$BundleData;

    .prologue
    .line 384
    iget-boolean v0, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z

    return v0
.end method

.method static synthetic access$202(Lcom/dsi/ant/channel/Capabilities$BundleData;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities$BundleData;
    .param p1, "x1"    # Z

    .prologue
    .line 384
    iput-boolean p1, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z

    return p1
.end method

.method static synthetic access$300(Lcom/dsi/ant/channel/Capabilities$BundleData;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities$BundleData;

    .prologue
    .line 384
    iget v0, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMin:I

    return v0
.end method

.method static synthetic access$302(Lcom/dsi/ant/channel/Capabilities$BundleData;I)I
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities$BundleData;
    .param p1, "x1"    # I

    .prologue
    .line 384
    iput p1, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMin:I

    return p1
.end method

.method static synthetic access$400(Lcom/dsi/ant/channel/Capabilities$BundleData;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities$BundleData;

    .prologue
    .line 384
    iget v0, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMax:I

    return v0
.end method

.method static synthetic access$402(Lcom/dsi/ant/channel/Capabilities$BundleData;I)I
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/channel/Capabilities$BundleData;
    .param p1, "x1"    # I

    .prologue
    .line 384
    iput p1, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMax:I

    return p1
.end method

.method static getBundleCapabilitiesArraySize()I
    .locals 1

    .prologue
    .line 476
    sget-object v0, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->NUMBER_OF_BUNDLE_CAPABILITIES:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->ordinal()I

    move-result v0

    return v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 397
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 412
    invoke-static {}, Lcom/dsi/ant/channel/Capabilities$BundleData;->getBundleCapabilitiesArraySize()I

    move-result v1

    new-array v0, v1, [Z

    .line 415
    .local v0, "bundleCapabilitiesArray":[Z
    sget-object v1, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->RSSI:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRssi:Z

    aput-boolean v2, v0, v1

    .line 417
    sget-object v1, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->WILDCARD_ID_LIST:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mWildcardIdList:Z

    aput-boolean v2, v0, v1

    .line 419
    sget-object v1, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->EVENT_BUFFERING:Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/Capabilities$BundleData$BundleCapabilitiesArrayIndex;->ordinal()I

    move-result v1

    iget-boolean v2, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mEventBuffering:Z

    aput-boolean v2, v0, v1

    .line 422
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 423
    invoke-static {}, Lcom/dsi/ant/channel/Capabilities$BundleData;->getBundleCapabilitiesArraySize()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 424
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 426
    iget v1, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMin:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 427
    iget v1, p0, Lcom/dsi/ant/channel/Capabilities$BundleData;->mRfFrequencyMax:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 428
    return-void
.end method
