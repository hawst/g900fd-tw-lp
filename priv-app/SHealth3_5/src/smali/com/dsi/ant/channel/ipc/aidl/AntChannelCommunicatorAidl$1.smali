.class final Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$1;
.super Ljava/lang/Object;
.source "AntChannelCommunicatorAidl.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 760
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 762
    .local v0, "version":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 767
    :cond_0
    new-instance v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;-><init>(Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;Z)V

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 757
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$1;->createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 773
    new-array v0, p1, [Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 757
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl$1;->newArray(I)[Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    move-result-object v0

    return-object v0
.end method
