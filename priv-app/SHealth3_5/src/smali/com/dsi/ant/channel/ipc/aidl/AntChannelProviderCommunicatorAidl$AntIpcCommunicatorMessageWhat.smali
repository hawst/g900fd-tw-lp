.class final enum Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;
.super Ljava/lang/Enum;
.source "AntChannelProviderCommunicatorAidl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "AntIpcCommunicatorMessageWhat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field public static final enum ACQUIRE_CHANNEL_PREDEFINED_NETWORK:Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field public static final enum ACQUIRE_CHANNEL_PRIVATE_NETWORK:Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field public static final enum UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

.field private static final sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 194
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 195
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    const-string v1, "ACQUIRE_CHANNEL_PRIVATE_NETWORK"

    invoke-direct {v0, v1, v3, v3}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->ACQUIRE_CHANNEL_PRIVATE_NETWORK:Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 196
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    const-string v1, "ACQUIRE_CHANNEL_PREDEFINED_NETWORK"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->ACQUIRE_CHANNEL_PREDEFINED_NETWORK:Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 193
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->ACQUIRE_CHANNEL_PRIVATE_NETWORK:Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->ACQUIRE_CHANNEL_PREDEFINED_NETWORK:Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->$VALUES:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 201
    invoke-static {}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->values()[Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 203
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->mRawValue:I

    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .prologue
    .line 193
    iget v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->mRawValue:I

    return v0
.end method

.method static createFromRawValue(I)Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 210
    sget-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->UNKNOWN:Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    .line 212
    .local v0, "code":Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 213
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aget-object v2, v2, v1

    invoke-direct {v2, p0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->equals(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 214
    sget-object v2, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->sValues:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    aget-object v0, v2, v1

    .line 219
    :cond_0
    return-object v0

    .line 212
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private equals(I)Z
    .locals 1
    .param p1, "rawValue"    # I

    .prologue
    .line 207
    iget v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->mRawValue:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 193
    const-class v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->$VALUES:[Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    invoke-virtual {v0}, [Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    return-object v0
.end method


# virtual methods
.method getRawValue()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->mRawValue:I

    return v0
.end method
