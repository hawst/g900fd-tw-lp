.class public abstract Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;
.super Landroid/os/Binder;
.source "IAntChannelProviderAidl.java"

# interfaces
.implements Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub$Proxy;
    }
.end annotation


# direct methods
.method public static asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 165
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 42
    :sswitch_0
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v8, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 51
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1

    .line 52
    sget-object v8, Lcom/dsi/ant/channel/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/channel/Capabilities;

    .line 58
    .local v1, "_arg1":Lcom/dsi/ant/channel/Capabilities;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_2

    .line 59
    sget-object v8, Lcom/dsi/ant/channel/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/channel/Capabilities;

    .line 65
    .local v2, "_arg2":Lcom/dsi/ant/channel/Capabilities;
    :goto_2
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 66
    .local v3, "_arg3":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->acquireChannel(ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    move-result-object v4

    .line 67
    .local v4, "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    if-eqz v4, :cond_0

    invoke-interface {v4}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    :cond_0
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 69
    if-eqz v3, :cond_3

    .line 70
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    invoke-virtual {v3, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 55
    .end local v1    # "_arg1":Lcom/dsi/ant/channel/Capabilities;
    .end local v2    # "_arg2":Lcom/dsi/ant/channel/Capabilities;
    .end local v3    # "_arg3":Landroid/os/Bundle;
    .end local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/dsi/ant/channel/Capabilities;
    goto :goto_1

    .line 62
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/dsi/ant/channel/Capabilities;
    goto :goto_2

    .line 74
    .restart local v3    # "_arg3":Landroid/os/Bundle;
    .restart local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    :cond_3
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 80
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/dsi/ant/channel/Capabilities;
    .end local v2    # "_arg2":Lcom/dsi/ant/channel/Capabilities;
    .end local v3    # "_arg3":Landroid/os/Bundle;
    .end local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    :sswitch_2
    const-string v8, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 84
    .local v0, "_arg0":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_5

    .line 85
    sget-object v8, Lcom/dsi/ant/channel/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/channel/Capabilities;

    .line 91
    .restart local v1    # "_arg1":Lcom/dsi/ant/channel/Capabilities;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_6

    .line 92
    sget-object v8, Lcom/dsi/ant/channel/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/channel/Capabilities;

    .line 98
    .restart local v2    # "_arg2":Lcom/dsi/ant/channel/Capabilities;
    :goto_4
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 99
    .restart local v3    # "_arg3":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->acquireChannelKey([BLcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    move-result-object v4

    .line 100
    .restart local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 101
    if-eqz v4, :cond_4

    invoke-interface {v4}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    :cond_4
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 102
    if-eqz v3, :cond_7

    .line 103
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    invoke-virtual {v3, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 88
    .end local v1    # "_arg1":Lcom/dsi/ant/channel/Capabilities;
    .end local v2    # "_arg2":Lcom/dsi/ant/channel/Capabilities;
    .end local v3    # "_arg3":Landroid/os/Bundle;
    .end local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/dsi/ant/channel/Capabilities;
    goto :goto_3

    .line 95
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/dsi/ant/channel/Capabilities;
    goto :goto_4

    .line 107
    .restart local v3    # "_arg3":Landroid/os/Bundle;
    .restart local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    :cond_7
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 113
    .end local v0    # "_arg0":[B
    .end local v1    # "_arg1":Lcom/dsi/ant/channel/Capabilities;
    .end local v2    # "_arg2":Lcom/dsi/ant/channel/Capabilities;
    .end local v3    # "_arg3":Landroid/os/Bundle;
    .end local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    :sswitch_3
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_8

    .line 116
    sget-object v5, Lcom/dsi/ant/channel/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/Capabilities;

    .line 121
    .local v0, "_arg0":Lcom/dsi/ant/channel/Capabilities;
    :goto_5
    invoke-virtual {p0, v0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->getNumChannelsAvailable(Lcom/dsi/ant/channel/Capabilities;)I

    move-result v4

    .line 122
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 123
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 119
    .end local v0    # "_arg0":Lcom/dsi/ant/channel/Capabilities;
    .end local v4    # "_result":I
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/dsi/ant/channel/Capabilities;
    goto :goto_5

    .line 128
    .end local v0    # "_arg0":Lcom/dsi/ant/channel/Capabilities;
    :sswitch_4
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->isLegacyInterfaceInUse()Z

    move-result v4

    .line 130
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 131
    if-eqz v4, :cond_9

    move v5, v6

    :goto_6
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_9
    move v5, v7

    goto :goto_6

    .line 136
    .end local v4    # "_result":Z
    :sswitch_5
    const-string v5, "com.dsi.ant.channel.ipc.aidl.IAntChannelProviderAidl"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_a

    .line 139
    sget-object v5, Landroid/os/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 145
    .local v0, "_arg0":Landroid/os/Message;
    :goto_7
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 146
    .local v1, "_arg1":Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;

    move-result-object v4

    .line 147
    .local v4, "_result":Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 148
    if-eqz v4, :cond_b

    .line 149
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 150
    invoke-virtual {v4, p3, v6}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 155
    :goto_8
    if-eqz v1, :cond_c

    .line 156
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 157
    invoke-virtual {v1, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 142
    .end local v0    # "_arg0":Landroid/os/Message;
    .end local v1    # "_arg1":Landroid/os/Bundle;
    .end local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Message;
    goto :goto_7

    .line 153
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    .restart local v4    # "_result":Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
    :cond_b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_8

    .line 160
    :cond_c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
