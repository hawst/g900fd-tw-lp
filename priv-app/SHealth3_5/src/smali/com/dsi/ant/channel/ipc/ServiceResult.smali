.class public Lcom/dsi/ant/channel/ipc/ServiceResult;
.super Ljava/lang/Object;
.source "ServiceResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/channel/ipc/ServiceResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final INVALID_REQUEST_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

.field public static final NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

.field public static final SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;


# instance fields
.field private mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

.field private mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

.field private mDetailMessage:Ljava/lang/String;

.field private mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

.field private mSuccess:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Z)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->SUCCESS_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    .line 44
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Z)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->NO_CHANNEL_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    .line 48
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->INVALID_REQUEST:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult;-><init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V

    sput-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->INVALID_REQUEST_RESULT:Lcom/dsi/ant/channel/ipc/ServiceResult;

    .line 319
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult$1;

    invoke-direct {v0}, Lcom/dsi/ant/channel/ipc/ServiceResult$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/channel/ipc/ServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    .line 32
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 33
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 34
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 205
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;-><init>(Lcom/dsi/ant/channel/ipc/ServiceResult$1;)V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    .line 131
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/ipc/ServiceResult;->readFromParcel(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/channel/AntCommandFailureReason;)V
    .locals 2
    .param p1, "failureReason"    # Lcom/dsi/ant/channel/AntCommandFailureReason;

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    .line 32
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 33
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 34
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 205
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;-><init>(Lcom/dsi/ant/channel/ipc/ServiceResult$1;)V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    .line 73
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RESPONSE:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v0, p1, :cond_0

    .line 74
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Channel Response failure type only valid when the response is provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    invoke-static {p1}, Lcom/dsi/ant/channel/ipc/ServiceResult;->createDetailMessage(Lcom/dsi/ant/channel/AntCommandFailureReason;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 79
    iput-object p1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 80
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 81
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "channelExists"    # Z

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    .line 32
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 33
    sget-object v0, Lcom/dsi/ant/channel/AntCommandFailureReason;->UNKNOWN:Lcom/dsi/ant/channel/AntCommandFailureReason;

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 34
    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 205
    new-instance v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;-><init>(Lcom/dsi/ant/channel/ipc/ServiceResult$1;)V

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    .line 55
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    iput-boolean p1, v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;->mChannelExists:Z

    .line 57
    if-eqz p1, :cond_0

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    .line 59
    const-string v0, "Success"

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 65
    :goto_0
    return-void

    .line 61
    :cond_0
    const-string v0, "Channel Does Not Exist"

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    goto :goto_0
.end method

.method private static createDetailMessage(Lcom/dsi/ant/channel/AntCommandFailureReason;)Ljava/lang/String;
    .locals 2
    .param p0, "failureReason"    # Lcom/dsi/ant/channel/AntCommandFailureReason;

    .prologue
    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ANT Service responded with failure reason: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 136
    .local v0, "detailMessageBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private createFromParcelVersionInitial(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 303
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/message/MessageUtils;->booleanFromNumber(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    .line 305
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    .line 306
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/channel/AntCommandFailureReason;->create(I)Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    .line 307
    const-class v0, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 308
    return-void
.end method

.method private createFromParcelVersionWithBundle(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 311
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 312
    .local v0, "received":Landroid/os/Bundle;
    const-class v1, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 313
    const-string v1, "com.dsi.ant.channel.ipc.serviceresult.bundledata"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    iput-object v1, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    .line 315
    return-void
.end method

.method public static readFrom(Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/ServiceResult;
    .locals 1
    .param p0, "error"    # Landroid/os/Bundle;

    .prologue
    .line 115
    const-class v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 117
    const-string v0, "com.dsi.ant.serviceerror"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/channel/ipc/ServiceResult;

    return-object v0
.end method

.method private writeToParcelVersionInitial(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 271
    iget-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    invoke-static {v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromBoolean(Z)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 272
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailureReason;->getRawValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 274
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 275
    return-void
.end method

.method private writeToParcelVersionWithBundle(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;

    .prologue
    .line 278
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 279
    .local v0, "out":Landroid/os/Bundle;
    const-string v1, "com.dsi.ant.channel.ipc.serviceresult.bundledata"

    iget-object v2, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 280
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 281
    return-void
.end method


# virtual methods
.method public channelExists()Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mBundleData:Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;

    iget-boolean v0, v0, Lcom/dsi/ant/channel/ipc/ServiceResult$BundleData;->mChannelExists:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    return v0
.end method

.method public getAntMessage()Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mAntMessage:Lcom/dsi/ant/message/ipc/AntMessageParcel;

    return-object v0
.end method

.method public getDetailMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mFailureReason:Lcom/dsi/ant/channel/AntCommandFailureReason;

    return-object v0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mSuccess:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 290
    if-eqz p1, :cond_0

    .line 291
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 293
    .local v0, "version":I
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/ipc/ServiceResult;->createFromParcelVersionInitial(Landroid/os/Parcel;)V

    .line 296
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 297
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/ipc/ServiceResult;->createFromParcelVersionWithBundle(Landroid/os/Parcel;)V

    .line 300
    .end local v0    # "version":I
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/ServiceResult;->mDetailMessage:Ljava/lang/String;

    return-object v0
.end method

.method public writeTo(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "error"    # Landroid/os/Bundle;

    .prologue
    .line 123
    const-string v0, "com.dsi.ant.serviceerror"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 124
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 259
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 262
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeToParcelVersionInitial(Landroid/os/Parcel;I)V

    .line 265
    invoke-static {}, Lcom/dsi/ant/AntService;->requiresBundle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/ipc/ServiceResult;->writeToParcelVersionWithBundle(Landroid/os/Parcel;)V

    .line 268
    :cond_0
    return-void
.end method
