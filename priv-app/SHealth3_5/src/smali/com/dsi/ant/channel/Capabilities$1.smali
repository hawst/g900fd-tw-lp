.class final Lcom/dsi/ant/channel/Capabilities$1;
.super Ljava/lang/Object;
.source "Capabilities.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/channel/Capabilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dsi/ant/channel/Capabilities;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private createFromParcelVersionInitial(Landroid/os/Parcel;)Lcom/dsi/ant/channel/Capabilities;
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 551
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 553
    .local v2, "capabilitiesArraySize":I
    new-array v1, v2, [Z

    .line 554
    .local v1, "capabilitiesArray":[Z
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 557
    new-instance v0, Lcom/dsi/ant/channel/Capabilities;

    invoke-direct {v0}, Lcom/dsi/ant/channel/Capabilities;-><init>()V

    .line 560
    .local v0, "capabilities":Lcom/dsi/ant/channel/Capabilities;
    sget-object v3, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->RX_MESSAGE_TIMESTAMP:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v3

    aget-boolean v3, v1, v3

    # setter for: Lcom/dsi/ant/channel/Capabilities;->mRxMessageTimestamp:Z
    invoke-static {v0, v3}, Lcom/dsi/ant/channel/Capabilities;->access$502(Lcom/dsi/ant/channel/Capabilities;Z)Z

    .line 562
    sget-object v3, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->BACKGROUND_SCANNING:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v3

    aget-boolean v3, v1, v3

    # setter for: Lcom/dsi/ant/channel/Capabilities;->mBackgroundScanning:Z
    invoke-static {v0, v3}, Lcom/dsi/ant/channel/Capabilities;->access$602(Lcom/dsi/ant/channel/Capabilities;Z)Z

    .line 564
    sget-object v3, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->FREQUENCY_AGILITY:Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/Capabilities$CapabilitiesArrayIndex;->ordinal()I

    move-result v3

    aget-boolean v3, v1, v3

    # setter for: Lcom/dsi/ant/channel/Capabilities;->mFrequencyAgility:Z
    invoke-static {v0, v3}, Lcom/dsi/ant/channel/Capabilities;->access$702(Lcom/dsi/ant/channel/Capabilities;Z)Z

    .line 567
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    # setter for: Lcom/dsi/ant/channel/Capabilities;->mMaxOutputPowerLevelSetting:I
    invoke-static {v0, v3}, Lcom/dsi/ant/channel/Capabilities;->access$802(Lcom/dsi/ant/channel/Capabilities;I)I

    .line 569
    return-object v0
.end method

.method private createFromParcelVersionWithBundle(Landroid/os/Parcel;Lcom/dsi/ant/channel/Capabilities;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;
    .param p2, "capabilities"    # Lcom/dsi/ant/channel/Capabilities;

    .prologue
    .line 573
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 574
    .local v0, "received":Landroid/os/Bundle;
    const-class v1, Lcom/dsi/ant/channel/Capabilities$BundleData;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 575
    const-string v1, "com.dsi.ant.channel.capabilities.bundledata"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/channel/Capabilities$BundleData;

    # setter for: Lcom/dsi/ant/channel/Capabilities;->mBundleData:Lcom/dsi/ant/channel/Capabilities$BundleData;
    invoke-static {p2, v1}, Lcom/dsi/ant/channel/Capabilities;->access$902(Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities$BundleData;)Lcom/dsi/ant/channel/Capabilities$BundleData;

    .line 577
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/Capabilities;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 539
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 541
    .local v1, "version":I
    invoke-direct {p0, p1}, Lcom/dsi/ant/channel/Capabilities$1;->createFromParcelVersionInitial(Landroid/os/Parcel;)Lcom/dsi/ant/channel/Capabilities;

    move-result-object v0

    .line 543
    .local v0, "capabilities":Lcom/dsi/ant/channel/Capabilities;
    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 544
    invoke-direct {p0, p1, v0}, Lcom/dsi/ant/channel/Capabilities$1;->createFromParcelVersionWithBundle(Landroid/os/Parcel;Lcom/dsi/ant/channel/Capabilities;)V

    .line 547
    :cond_0
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 536
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/Capabilities$1;->createFromParcel(Landroid/os/Parcel;)Lcom/dsi/ant/channel/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/dsi/ant/channel/Capabilities;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 581
    new-array v0, p1, [Lcom/dsi/ant/channel/Capabilities;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 536
    invoke-virtual {p0, p1}, Lcom/dsi/ant/channel/Capabilities$1;->newArray(I)[Lcom/dsi/ant/channel/Capabilities;

    move-result-object v0

    return-object v0
.end method
