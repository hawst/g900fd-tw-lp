.class public Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl;
.super Ljava/lang/Object;
.source "AntChannelProviderCommunicatorAidl.java"

# interfaces
.implements Lcom/dsi/ant/channel/ipc/IAntChannelProviderCommunicator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;
    }
.end annotation


# instance fields
.field private mIAntChannelProviderAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;


# direct methods
.method public constructor <init>(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-static {p1}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl$Stub;->asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl;->mIAntChannelProviderAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;

    .line 72
    iget-object v0, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl;->mIAntChannelProviderAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The given service does not seem to be an ANT channel pool which communicates over AIDL."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_0
    return-void
.end method


# virtual methods
.method public acquireChannel(Landroid/content/Context;ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "whichNetwork"    # I
    .param p3, "requiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p4, "desiredCapabilities"    # Lcom/dsi/ant/channel/Capabilities;
    .param p5, "error"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 83
    const/4 v3, 0x0

    .line 84
    .local v3, "communicator":Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;
    const/4 v0, 0x0

    .line 89
    .local v0, "acquiredChannelAidl":Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;
    invoke-static {p1}, Lcom/dsi/ant/AntService;->getVersionCode(Landroid/content/Context;)I

    move-result v7

    const v8, 0x9dd0

    if-lt v7, v8, :cond_2

    .line 91
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v5

    .line 92
    .local v5, "message":Landroid/os/Message;
    sget-object v7, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->ACQUIRE_CHANNEL_PREDEFINED_NETWORK:Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;

    # getter for: Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->mRawValue:I
    invoke-static {v7}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;->access$000(Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl$AntIpcCommunicatorMessageWhat;)I

    move-result v7

    iput v7, v5, Landroid/os/Message;->what:I

    .line 95
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 96
    .local v6, "parameters":Landroid/os/Bundle;
    const-string v7, "com.dsi.channel.data.versioncode"

    const v8, 0x9efc

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 97
    const-string v7, "com.dsi.channel.data.predefinednetwork"

    invoke-virtual {v6, v7, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 98
    const-string v7, "com.dsi.channel.data.requiredcapabilities"

    invoke-virtual {v6, v7, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 99
    const-string v7, "com.dsi.channel.data.desiredcapabilities"

    invoke-virtual {v6, v7, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 100
    invoke-virtual {v5, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 102
    iget-object v7, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl;->mIAntChannelProviderAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;

    invoke-interface {v7, v5, p5}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;->handleMessage(Landroid/os/Message;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;

    move-result-object v4

    .line 105
    .local v4, "data":Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
    invoke-virtual {v4}, Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;->getBindersReturned()Ljava/util/ArrayList;

    move-result-object v2

    .line 106
    .local v2, "bindersReturned":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/IBinder;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 107
    .local v1, "binderList":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/IBinder;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 109
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/IBinder;

    invoke-static {v7}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl$Stub;->asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    move-result-object v0

    .line 122
    .end local v1    # "binderList":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/IBinder;>;"
    .end local v2    # "bindersReturned":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/IBinder;>;"
    .end local v4    # "data":Lcom/dsi/ant/channel/ipc/aidl/AntIpcResult;
    .end local v5    # "message":Landroid/os/Message;
    .end local v6    # "parameters":Landroid/os/Bundle;
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 123
    new-instance v3, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;

    .end local v3    # "communicator":Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;
    invoke-direct {v3, v0}, Lcom/dsi/ant/channel/ipc/aidl/AntChannelCommunicatorAidl;-><init>(Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;)V

    .line 126
    .restart local v3    # "communicator":Lcom/dsi/ant/channel/ipc/IAntChannelCommunicator;
    :cond_1
    return-object v3

    .line 112
    :cond_2
    iget-object v7, p0, Lcom/dsi/ant/channel/ipc/aidl/AntChannelProviderCommunicatorAidl;->mIAntChannelProviderAidl:Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;

    invoke-interface {v7, p2, p3, p4, p5}, Lcom/dsi/ant/channel/ipc/aidl/IAntChannelProviderAidl;->acquireChannel(ILcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Bundle;)Lcom/dsi/ant/channel/ipc/aidl/IAntChannelAidl;

    move-result-object v0

    goto :goto_0
.end method
