.class public final Lcom/dsi/ant/message/fromhost/AssignChannelMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "AssignChannelMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mExtendedAssignment:Lcom/dsi/ant/message/ExtendedAssignment;

.field private final mRawChannelType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/ChannelType;Lcom/dsi/ant/message/ExtendedAssignment;)V
    .locals 1
    .param p1, "channelType"    # Lcom/dsi/ant/message/ChannelType;
    .param p2, "extendedAssignment"    # Lcom/dsi/ant/message/ExtendedAssignment;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 63
    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelType;->getRawValue()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mRawChannelType:I

    .line 64
    iput-object p2, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mExtendedAssignment:Lcom/dsi/ant/message/ExtendedAssignment;

    .line 65
    return-void
.end method


# virtual methods
.method public getChannelType()Lcom/dsi/ant/message/ChannelType;
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mRawChannelType:I

    invoke-static {v0}, Lcom/dsi/ant/message/ChannelType;->create(I)Lcom/dsi/ant/message/ChannelType;

    move-result-object v0

    return-object v0
.end method

.method public getMessageContent(II)[B
    .locals 3
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    .line 108
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 111
    .local v0, "content":[B
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 112
    iget v1, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mRawChannelType:I

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 113
    const/4 v1, 0x2

    invoke-static {p2, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 114
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mExtendedAssignment:Lcom/dsi/ant/message/ExtendedAssignment;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ExtendedAssignment;->getFlagsByte()B

    move-result v1

    const/4 v2, 0x3

    invoke-static {v1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 117
    return-object v0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    const-string v1, "Channel Type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->getChannelType()Lcom/dsi/ant/message/ChannelType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/ChannelType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mRawChannelType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;->mExtendedAssignment:Lcom/dsi/ant/message/ExtendedAssignment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
