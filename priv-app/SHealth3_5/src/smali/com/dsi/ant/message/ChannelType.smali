.class public final enum Lcom/dsi/ant/message/ChannelType;
.super Ljava/lang/Enum;
.source "ChannelType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/ChannelType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/ChannelType;

.field public static final enum BIDIRECTIONAL_MASTER:Lcom/dsi/ant/message/ChannelType;

.field public static final enum BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

.field public static final enum SHARED_BIDIRECTIONAL_MASTER:Lcom/dsi/ant/message/ChannelType;

.field public static final enum SHARED_BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

.field public static final SIZE_CHANNEL_TYPE:I = 0x1

.field public static final enum UNKNOWN:Lcom/dsi/ant/message/ChannelType;

.field private static final sValues:[Lcom/dsi/ant/message/ChannelType;


# instance fields
.field private mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/dsi/ant/message/ChannelType;

    const-string v1, "BIDIRECTIONAL_SLAVE"

    invoke-direct {v0, v1, v3, v3}, Lcom/dsi/ant/message/ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    .line 23
    new-instance v0, Lcom/dsi/ant/message/ChannelType;

    const-string v1, "BIDIRECTIONAL_MASTER"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v4, v2}, Lcom/dsi/ant/message/ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_MASTER:Lcom/dsi/ant/message/ChannelType;

    .line 29
    new-instance v0, Lcom/dsi/ant/message/ChannelType;

    const-string v1, "SHARED_BIDIRECTIONAL_SLAVE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v5, v2}, Lcom/dsi/ant/message/ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ChannelType;->SHARED_BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    .line 35
    new-instance v0, Lcom/dsi/ant/message/ChannelType;

    const-string v1, "SHARED_BIDIRECTIONAL_MASTER"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v6, v2}, Lcom/dsi/ant/message/ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ChannelType;->SHARED_BIDIRECTIONAL_MASTER:Lcom/dsi/ant/message/ChannelType;

    .line 41
    new-instance v0, Lcom/dsi/ant/message/ChannelType;

    const-string v1, "UNKNOWN"

    const v2, 0xffff

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/message/ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ChannelType;->UNKNOWN:Lcom/dsi/ant/message/ChannelType;

    .line 16
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dsi/ant/message/ChannelType;

    sget-object v1, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_MASTER:Lcom/dsi/ant/message/ChannelType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/message/ChannelType;->SHARED_BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/message/ChannelType;->SHARED_BIDIRECTIONAL_MASTER:Lcom/dsi/ant/message/ChannelType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/message/ChannelType;->UNKNOWN:Lcom/dsi/ant/message/ChannelType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/dsi/ant/message/ChannelType;->$VALUES:[Lcom/dsi/ant/message/ChannelType;

    .line 50
    invoke-static {}, Lcom/dsi/ant/message/ChannelType;->values()[Lcom/dsi/ant/message/ChannelType;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/message/ChannelType;->sValues:[Lcom/dsi/ant/message/ChannelType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/message/ChannelType;->mRawValue:I

    return-void
.end method

.method public static create(I)Lcom/dsi/ant/message/ChannelType;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 73
    sget-object v0, Lcom/dsi/ant/message/ChannelType;->UNKNOWN:Lcom/dsi/ant/message/ChannelType;

    .line 75
    .local v0, "code":Lcom/dsi/ant/message/ChannelType;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/message/ChannelType;->sValues:[Lcom/dsi/ant/message/ChannelType;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 76
    sget-object v2, Lcom/dsi/ant/message/ChannelType;->sValues:[Lcom/dsi/ant/message/ChannelType;

    aget-object v2, v2, v1

    invoke-direct {v2, p0}, Lcom/dsi/ant/message/ChannelType;->equals(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 77
    sget-object v2, Lcom/dsi/ant/message/ChannelType;->sValues:[Lcom/dsi/ant/message/ChannelType;

    aget-object v0, v2, v1

    .line 84
    :cond_0
    iput p0, v0, Lcom/dsi/ant/message/ChannelType;->mRawValue:I

    .line 86
    return-object v0

    .line 75
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private equals(I)Z
    .locals 1
    .param p1, "rawValue"    # I

    .prologue
    .line 63
    iget v0, p0, Lcom/dsi/ant/message/ChannelType;->mRawValue:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/ChannelType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/dsi/ant/message/ChannelType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/ChannelType;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/ChannelType;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/dsi/ant/message/ChannelType;->$VALUES:[Lcom/dsi/ant/message/ChannelType;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/ChannelType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/ChannelType;

    return-object v0
.end method


# virtual methods
.method public getRawValue()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/dsi/ant/message/ChannelType;->mRawValue:I

    return v0
.end method

.method public isSlave()Z
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    if-eq v0, p0, :cond_0

    sget-object v0, Lcom/dsi/ant/message/ChannelType;->SHARED_BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
