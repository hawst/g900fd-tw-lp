.class public Lcom/dsi/ant/message/fromant/ChannelIdMessage;
.super Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.source "ChannelIdMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;


# instance fields
.field private final mChannelId:Lcom/dsi/ant/message/ChannelId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_ID:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sput-object v0, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-void
.end method

.method protected constructor <init>([B)V
    .locals 2
    .param p1, "messageContent"    # [B

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;-><init>([B)V

    .line 43
    new-instance v0, Lcom/dsi/ant/message/ChannelId;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/dsi/ant/message/ChannelId;-><init>([BI)V

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    .line 44
    return-void
.end method


# virtual methods
.method public getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 62
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    iget-object v1, p0, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelId;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
