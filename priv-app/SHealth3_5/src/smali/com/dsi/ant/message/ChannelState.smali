.class public final enum Lcom/dsi/ant/message/ChannelState;
.super Ljava/lang/Enum;
.source "ChannelState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/ChannelState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/ChannelState;

.field public static final enum ASSIGNED:Lcom/dsi/ant/message/ChannelState;

.field public static final enum INVALID:Lcom/dsi/ant/message/ChannelState;

.field public static final enum SEARCHING:Lcom/dsi/ant/message/ChannelState;

.field public static final enum TRACKING:Lcom/dsi/ant/message/ChannelState;

.field public static final enum UNASSIGNED:Lcom/dsi/ant/message/ChannelState;

.field private static final sValues:[Lcom/dsi/ant/message/ChannelState;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/dsi/ant/message/ChannelState;

    const-string v1, "UNASSIGNED"

    invoke-direct {v0, v1, v3, v3}, Lcom/dsi/ant/message/ChannelState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ChannelState;->UNASSIGNED:Lcom/dsi/ant/message/ChannelState;

    .line 23
    new-instance v0, Lcom/dsi/ant/message/ChannelState;

    const-string v1, "ASSIGNED"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/message/ChannelState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ChannelState;->ASSIGNED:Lcom/dsi/ant/message/ChannelState;

    .line 26
    new-instance v0, Lcom/dsi/ant/message/ChannelState;

    const-string v1, "SEARCHING"

    invoke-direct {v0, v1, v5, v5}, Lcom/dsi/ant/message/ChannelState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    .line 29
    new-instance v0, Lcom/dsi/ant/message/ChannelState;

    const-string v1, "TRACKING"

    invoke-direct {v0, v1, v6, v6}, Lcom/dsi/ant/message/ChannelState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    .line 32
    new-instance v0, Lcom/dsi/ant/message/ChannelState;

    const-string v1, "INVALID"

    const v2, 0xffff

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/message/ChannelState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/ChannelState;->INVALID:Lcom/dsi/ant/message/ChannelState;

    .line 16
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dsi/ant/message/ChannelState;

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->UNASSIGNED:Lcom/dsi/ant/message/ChannelState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->ASSIGNED:Lcom/dsi/ant/message/ChannelState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->INVALID:Lcom/dsi/ant/message/ChannelState;

    aput-object v1, v0, v7

    sput-object v0, Lcom/dsi/ant/message/ChannelState;->$VALUES:[Lcom/dsi/ant/message/ChannelState;

    .line 37
    invoke-static {}, Lcom/dsi/ant/message/ChannelState;->values()[Lcom/dsi/ant/message/ChannelState;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/message/ChannelState;->sValues:[Lcom/dsi/ant/message/ChannelState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/message/ChannelState;->mRawValue:I

    return-void
.end method

.method public static create(I)Lcom/dsi/ant/message/ChannelState;
    .locals 3
    .param p0, "rawValue"    # I

    .prologue
    .line 60
    sget-object v0, Lcom/dsi/ant/message/ChannelState;->INVALID:Lcom/dsi/ant/message/ChannelState;

    .line 62
    .local v0, "code":Lcom/dsi/ant/message/ChannelState;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/dsi/ant/message/ChannelState;->sValues:[Lcom/dsi/ant/message/ChannelState;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 63
    sget-object v2, Lcom/dsi/ant/message/ChannelState;->sValues:[Lcom/dsi/ant/message/ChannelState;

    aget-object v2, v2, v1

    invoke-direct {v2, p0}, Lcom/dsi/ant/message/ChannelState;->equals(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 64
    sget-object v2, Lcom/dsi/ant/message/ChannelState;->sValues:[Lcom/dsi/ant/message/ChannelState;

    aget-object v0, v2, v1

    .line 69
    :cond_0
    return-object v0

    .line 62
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private equals(I)Z
    .locals 1
    .param p1, "rawValue"    # I

    .prologue
    .line 50
    iget v0, p0, Lcom/dsi/ant/message/ChannelState;->mRawValue:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/ChannelState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/dsi/ant/message/ChannelState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/ChannelState;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/ChannelState;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/dsi/ant/message/ChannelState;->$VALUES:[Lcom/dsi/ant/message/ChannelState;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/ChannelState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/ChannelState;

    return-object v0
.end method


# virtual methods
.method public getRawValue()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/dsi/ant/message/ChannelState;->mRawValue:I

    return v0
.end method
