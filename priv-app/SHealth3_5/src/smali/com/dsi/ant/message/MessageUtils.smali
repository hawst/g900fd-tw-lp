.class public Lcom/dsi/ant/message/MessageUtils;
.super Ljava/lang/Object;
.source "MessageUtils.java"


# direct methods
.method public static booleanFromNumber(I)Z
    .locals 1
    .param p0, "data"    # I

    .prologue
    const/4 v0, 0x1

    .line 151
    if-ne p0, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getHexString(I)Ljava/lang/String;
    .locals 5
    .param p0, "number"    # I

    .prologue
    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%02X"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    and-int/lit16 v4, p0, 0xff

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHexString([B)Ljava/lang/String;
    .locals 6
    .param p0, "data"    # [B

    .prologue
    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 267
    .local v0, "hexStringBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 268
    const/16 v2, 0x5b

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 269
    const-string v2, "%02X"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aget-byte v5, p0, v1

    and-int/lit16 v5, v5, 0xff

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const/16 v2, 0x5d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 267
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 273
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static final inRange(III)Z
    .locals 1
    .param p0, "value"    # I
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 238
    const/4 v0, 0x0

    .line 240
    .local v0, "inRange":Z
    if-lt p0, p1, :cond_0

    if-gt p0, p2, :cond_0

    .line 241
    const/4 v0, 0x1

    .line 244
    :cond_0
    return v0
.end method

.method public static isFlagSet(II)Z
    .locals 1
    .param p0, "flagMask"    # I
    .param p1, "flags"    # I

    .prologue
    .line 28
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFlagSet(I[BI)Z
    .locals 1
    .param p0, "flagMask"    # I
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 41
    invoke-static {p1, p2}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    invoke-static {p0, v0}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(II)Z

    move-result v0

    return v0
.end method

.method public static numberFromBits(III)I
    .locals 1
    .param p0, "data"    # I
    .param p1, "mask"    # I
    .param p2, "shift"    # I

    .prologue
    .line 68
    and-int v0, p0, p1

    shr-int/2addr v0, p2

    return v0
.end method

.method public static numberFromBits([BIII)I
    .locals 1
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "mask"    # I
    .param p3, "shift"    # I

    .prologue
    .line 56
    aget-byte v0, p0, p1

    invoke-static {v0, p2, p3}, Lcom/dsi/ant/message/MessageUtils;->numberFromBits(III)I

    move-result v0

    return v0
.end method

.method public static numberFromBoolean(Z)I
    .locals 1
    .param p0, "data"    # Z

    .prologue
    .line 161
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static numberFromByte([BI)I
    .locals 2
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 90
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static numberFromBytes([BII)J
    .locals 5
    .param p0, "data"    # [B
    .param p1, "startOffset"    # I
    .param p2, "numberOfBytes"    # I

    .prologue
    .line 114
    const-wide/16 v1, 0x0

    .line 116
    .local v1, "number":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 117
    add-int v3, p1, v0

    aget-byte v3, p0, v3

    and-int/lit16 v3, v3, 0xff

    mul-int/lit8 v4, v0, 0x8

    shl-int/2addr v3, v4

    int-to-long v3, v3

    add-long/2addr v1, v3

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_0
    return-wide v1
.end method

.method public static placeInArray(I[BI)V
    .locals 3
    .param p0, "number"    # I
    .param p1, "destination"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 195
    const/16 v0, 0xff

    .line 197
    .local v0, "maxValue":I
    if-gt p0, v0, :cond_0

    if-gez p0, :cond_1

    .line 198
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Value outside the bounds of unsigned byte integer"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 201
    :cond_1
    int-to-byte v1, p0

    aput-byte v1, p1, p2

    .line 202
    return-void
.end method

.method public static placeInArray(J[BII)V
    .locals 8
    .param p0, "number"    # J
    .param p2, "destination"    # [B
    .param p3, "numberOfBytes"    # I
    .param p4, "startOffset"    # I

    .prologue
    .line 214
    const-wide/16 v1, 0x0

    .line 216
    .local v1, "maxValue":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 217
    const/16 v3, 0xff

    mul-int/lit8 v4, v0, 0x8

    shl-int/2addr v3, v4

    int-to-long v3, v3

    add-long/2addr v1, v3

    .line 216
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_0
    cmp-long v3, p0, v1

    if-gtz v3, :cond_1

    const-wide/16 v3, 0x0

    cmp-long v3, p0, v3

    if-gez v3, :cond_2

    .line 221
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Value ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") outside the bounds of unsigned "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " byte integer (0-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 224
    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p3, :cond_3

    .line 225
    add-int v3, p4, v0

    mul-int/lit8 v4, v0, 0x8

    shr-long v4, p0, v4

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, p2, v3

    .line 224
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 227
    :cond_3
    return-void
.end method

.method public static signedNumberFromByte([BI)I
    .locals 1
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 101
    aget-byte v0, p0, p1

    return v0
.end method
