.class public Lcom/dsi/ant/message/fromant/AcknowledgedDataMessage;
.super Lcom/dsi/ant/message/fromant/DataMessage;
.source "AcknowledgedDataMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sput-object v0, Lcom/dsi/ant/message/fromant/AcknowledgedDataMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-void
.end method

.method protected constructor <init>([B)V
    .locals 0
    .param p1, "messageContent"    # [B

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/DataMessage;-><init>([B)V

    .line 37
    return-void
.end method


# virtual methods
.method public getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/dsi/ant/message/fromant/AcknowledgedDataMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method
