.class public Lcom/dsi/ant/message/fromant/AntVersionMessage;
.super Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.source "AntVersionMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;
    }
.end annotation


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field private static final TAG:Ljava/lang/String;

.field private static final TAG_OVERRIDE_STATE:Ljava/lang/String;


# instance fields
.field private mBuildNumber:I

.field private mMajorVersion:F

.field private mModule:Ljava/lang/String;

.field private mProductFamily:Ljava/lang/String;

.field private mRevision:C

.field private mVersionMessageFormat:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

.field private final mVersionString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    const-class v0, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->TAG:Ljava/lang/String;

    .line 28
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ANT_VERSION:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sput-object v0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/dsi/ant/message/fromant/AntVersionMessage;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":OverrideState"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->TAG_OVERRIDE_STATE:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>([B)V
    .locals 3
    .param p1, "messageContent"    # [B

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 102
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;-><init>([B)V

    .line 49
    iput-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mProductFamily:Ljava/lang/String;

    .line 52
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mMajorVersion:F

    .line 55
    iput-char v1, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mRevision:C

    .line 58
    iput v1, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mBuildNumber:I

    .line 61
    iput-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mModule:Ljava/lang/String;

    .line 86
    sget-object v0, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->BAD_FORMAT:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionMessageFormat:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    .line 104
    invoke-direct {p0}, Lcom/dsi/ant/message/fromant/AntVersionMessage;->findVersionString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionString:Ljava/lang/String;

    .line 106
    invoke-direct {p0}, Lcom/dsi/ant/message/fromant/AntVersionMessage;->parseVersionString()V

    .line 107
    return-void
.end method

.method private findVersionString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 186
    const/4 v1, 0x0

    .line 187
    .local v1, "length":I
    const-string v3, "?"

    .line 190
    .local v3, "versionString":Ljava/lang/String;
    const/4 v1, 0x0

    :goto_0
    iget-object v5, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mMessageContent:[B

    array-length v5, v5

    if-ge v1, v5, :cond_0

    .line 191
    iget-object v5, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mMessageContent:[B

    aget-byte v5, v5, v1

    if-nez v5, :cond_1

    .line 196
    :cond_0
    new-array v2, v1, [B

    .line 197
    .local v2, "versionBytes":[B
    iget-object v5, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mMessageContent:[B

    invoke-static {v5, v6, v2, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 200
    :try_start_0
    new-instance v4, Ljava/lang/String;

    const-string v5, "UTF-8"

    invoke-direct {v4, v2, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v3    # "versionString":Ljava/lang/String;
    .local v4, "versionString":Ljava/lang/String;
    move-object v3, v4

    .line 208
    .end local v4    # "versionString":Ljava/lang/String;
    .restart local v3    # "versionString":Ljava/lang/String;
    :goto_1
    return-object v3

    .line 190
    .end local v2    # "versionBytes":[B
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 201
    .restart local v2    # "versionBytes":[B
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v5, Lcom/dsi/ant/message/fromant/AntVersionMessage;->TAG:Ljava/lang/String;

    const-string v6, "Could not create version string with UTF-8 encoding"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private parseVersionString()V
    .locals 5

    .prologue
    .line 124
    iget-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionString:Ljava/lang/String;

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 126
    .local v1, "offset":I
    if-gez v1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionString:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\\-$"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mProductFamily:Ljava/lang/String;

    .line 132
    iget-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionString:Ljava/lang/String;

    add-int/lit8 v3, v1, 0x4

    invoke-virtual {v2, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mMajorVersion:F

    .line 136
    add-int/lit8 v1, v1, 0x4

    .line 140
    iget-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionString:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v1

    const/4 v3, 0x3

    if-lt v2, v3, :cond_3

    .line 145
    iget-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionString:Ljava/lang/String;

    add-int/lit8 v3, v1, 0x3

    invoke-virtual {v2, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "moduleOrRevisionString":Ljava/lang/String;
    const-string v2, "[A-Za-z]{3}"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 150
    iget-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionString:Ljava/lang/String;

    add-int/lit8 v3, v1, 0x3

    invoke-virtual {v2, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mModule:Ljava/lang/String;

    .line 152
    sget-object v2, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->VERSION_MODULE:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    iput-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionMessageFormat:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    goto :goto_0

    .line 154
    :cond_2
    const-string v2, "[A-Za-z][0-9]{2}"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 155
    iget-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionString:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    iput-char v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mRevision:C

    .line 156
    add-int/lit8 v1, v1, 0x1

    .line 157
    iget-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionString:Ljava/lang/String;

    add-int/lit8 v3, v1, 0x2

    invoke-virtual {v2, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mBuildNumber:I

    .line 160
    sget-object v2, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->VERSION:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    iput-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionMessageFormat:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    goto :goto_0

    .line 168
    .end local v0    # "moduleOrRevisionString":Ljava/lang/String;
    :cond_3
    sget-object v2, Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;->VERSION:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    iput-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionMessageFormat:Lcom/dsi/ant/message/fromant/AntVersionMessage$FIRMWARE_VERSION_FORMAT;

    goto :goto_0
.end method


# virtual methods
.method public getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 346
    sget-object v0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntVersionMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 338
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    const-string v1, "Version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/AntVersionMessage;->mVersionString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
