.class public Lcom/dsi/ant/message/fromant/ChannelStatusMessage;
.super Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.source "ChannelStatusMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;


# instance fields
.field private final mChannelState:Lcom/dsi/ant/message/ChannelState;

.field private final mChannelType:Lcom/dsi/ant/message/ChannelType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_STATUS:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sput-object v0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 1
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 55
    sget-object v0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-static {v0, p1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->extractCorrectMessageContent(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;-><init>([B)V

    .line 56
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 1
    .param p1, "messageContent"    # [B

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;-><init>([B)V

    .line 61
    invoke-direct {p0}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->findChannelType()I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/message/ChannelType;->create(I)Lcom/dsi/ant/message/ChannelType;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->mChannelType:Lcom/dsi/ant/message/ChannelType;

    .line 62
    invoke-direct {p0}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->findChannelState()I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/message/ChannelState;->create(I)Lcom/dsi/ant/message/ChannelState;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->mChannelState:Lcom/dsi/ant/message/ChannelState;

    .line 63
    return-void
.end method

.method private findChannelState()I
    .locals 4

    .prologue
    .line 71
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->mMessageContent:[B

    const/4 v1, 0x1

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/dsi/ant/message/MessageUtils;->numberFromBits([BIII)I

    move-result v0

    return v0
.end method

.method private findChannelType()I
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->mMessageContent:[B

    const/4 v1, 0x1

    const/16 v2, -0x10

    const/4 v3, 0x4

    invoke-static {v0, v1, v2, v3}, Lcom/dsi/ant/message/MessageUtils;->numberFromBits([BIII)I

    move-result v0

    return v0
.end method


# virtual methods
.method public getChannelState()Lcom/dsi/ant/message/ChannelState;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->mChannelState:Lcom/dsi/ant/message/ChannelState;

    return-object v0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 100
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    const-string v1, "Type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->mChannelType:Lcom/dsi/ant/message/ChannelType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    const-string v1, "State="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->mChannelState:Lcom/dsi/ant/message/ChannelState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 104
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
