.class public abstract Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.super Lcom/dsi/ant/message/AntMessage;
.source "AntMessageFromHost.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/dsi/ant/message/AntMessage;-><init>()V

    .line 266
    return-void
.end method


# virtual methods
.method public getMessageContent()[B
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-virtual {p0, v0, v0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageContent(II)[B

    move-result-object v0

    return-object v0
.end method

.method public abstract getMessageContent(II)[B
.end method

.method public getMessageId()I
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->getMessageId()I

    move-result v0

    return v0
.end method

.method public abstract getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageContentString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected toStringHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageIdString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
