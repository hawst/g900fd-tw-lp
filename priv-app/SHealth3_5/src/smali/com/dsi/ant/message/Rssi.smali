.class public Lcom/dsi/ant/message/Rssi;
.super Ljava/lang/Object;
.source "Rssi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/Rssi$RssiMeasurementType;
    }
.end annotation


# static fields
.field public static final OFFSET_MEASUREMENT_TYPE:I = 0x0

.field public static final OFFSET_RSSI_VALUE:I = 0x1

.field public static final OFFSET_THRESHOLD_CONFIG:I = 0x2

.field public static final SIZE_MEASUREMENT_TYPE:I = 0x1

.field public static final SIZE_RSSI:I = 0x3

.field public static final SIZE_RSSI_VALUE:I = 0x1

.field public static final SIZE_THRESHOLD_CONFIG:I = 0x1

.field public static final THRESHOLD_CONFIG_OFF:I = -0x80


# instance fields
.field private mRawMeasurementType:I

.field private mRssiValue:I

.field private mThresholdConfigDB:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "measurementType"    # I
    .param p2, "rssiValue"    # I
    .param p3, "thresholdConfigDB"    # I

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput p1, p0, Lcom/dsi/ant/message/Rssi;->mRawMeasurementType:I

    .line 122
    iput p2, p0, Lcom/dsi/ant/message/Rssi;->mRssiValue:I

    .line 123
    iput p3, p0, Lcom/dsi/ant/message/Rssi;->mThresholdConfigDB:I

    .line 124
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "messageContent"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/Rssi;->mRawMeasurementType:I

    .line 109
    add-int/lit8 v0, p2, 0x1

    invoke-static {p1, v0}, Lcom/dsi/ant/message/MessageUtils;->signedNumberFromByte([BI)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/Rssi;->mRssiValue:I

    .line 110
    add-int/lit8 v0, p2, 0x2

    invoke-static {p1, v0}, Lcom/dsi/ant/message/MessageUtils;->signedNumberFromByte([BI)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/Rssi;->mThresholdConfigDB:I

    .line 111
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 179
    if-ne p0, p1, :cond_1

    .line 199
    :cond_0
    :goto_0
    return v1

    .line 183
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 184
    goto :goto_0

    .line 187
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/message/Rssi;

    if-nez v3, :cond_3

    move v1, v2

    .line 188
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 191
    check-cast v0, Lcom/dsi/ant/message/Rssi;

    .line 193
    .local v0, "other":Lcom/dsi/ant/message/Rssi;
    iget v3, v0, Lcom/dsi/ant/message/Rssi;->mRawMeasurementType:I

    iget v4, p0, Lcom/dsi/ant/message/Rssi;->mRawMeasurementType:I

    if-ne v3, v4, :cond_4

    iget v3, v0, Lcom/dsi/ant/message/Rssi;->mRssiValue:I

    iget v4, p0, Lcom/dsi/ant/message/Rssi;->mRssiValue:I

    if-ne v3, v4, :cond_4

    iget v3, v0, Lcom/dsi/ant/message/Rssi;->mThresholdConfigDB:I

    iget v4, p0, Lcom/dsi/ant/message/Rssi;->mThresholdConfigDB:I

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    .line 196
    goto :goto_0
.end method

.method public getMeasurementType()Lcom/dsi/ant/message/Rssi$RssiMeasurementType;
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/dsi/ant/message/Rssi;->mRawMeasurementType:I

    invoke-static {v0}, Lcom/dsi/ant/message/Rssi$RssiMeasurementType;->convertToRssiMeasurementType(I)Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    move-result-object v0

    return-object v0
.end method

.method public getRawMeasurementType()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/dsi/ant/message/Rssi;->mRawMeasurementType:I

    return v0
.end method

.method public getRssiValue()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/dsi/ant/message/Rssi;->mRssiValue:I

    return v0
.end method

.method public getThresholdConfigDB()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/dsi/ant/message/Rssi;->mThresholdConfigDB:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 166
    const/16 v0, 0x1f

    .line 167
    .local v0, "prime":I
    const/4 v1, 0x7

    .line 169
    .local v1, "result":I
    iget v2, p0, Lcom/dsi/ant/message/Rssi;->mRawMeasurementType:I

    add-int/lit16 v1, v2, 0xd9

    .line 170
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/dsi/ant/message/Rssi;->mRssiValue:I

    add-int v1, v2, v3

    .line 171
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/dsi/ant/message/Rssi;->mThresholdConfigDB:I

    add-int v1, v2, v3

    .line 173
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RSSI: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 207
    .local v0, "infoStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "Value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    iget v1, p0, Lcom/dsi/ant/message/Rssi;->mRssiValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 209
    invoke-virtual {p0}, Lcom/dsi/ant/message/Rssi;->getMeasurementType()Lcom/dsi/ant/message/Rssi$RssiMeasurementType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 211
    const-string v1, ", Threshold Config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    iget v1, p0, Lcom/dsi/ant/message/Rssi;->mThresholdConfigDB:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 213
    const-string v1, "dB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
