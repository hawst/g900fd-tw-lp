.class public Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;
.super Lcom/dsi/ant/message/fromant/DataMessage;
.source "BurstTransferDataMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;


# instance fields
.field private final mSequenceNumber:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sput-object v0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-void
.end method

.method protected constructor <init>([B)V
    .locals 4
    .param p1, "messageContent"    # [B

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/DataMessage;-><init>([B)V

    .line 68
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->mMessageContent:[B

    const/4 v1, 0x0

    const/16 v2, 0xe0

    const/4 v3, 0x5

    invoke-static {v0, v1, v2, v3}, Lcom/dsi/ant/message/MessageUtils;->numberFromBits([BIII)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->mSequenceNumber:I

    .line 69
    return-void
.end method


# virtual methods
.method public getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method

.method public isFirstMessage()Z
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->mSequenceNumber:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLastMessage()Z
    .locals 2

    .prologue
    .line 96
    const/4 v0, 0x4

    iget v1, p0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->mSequenceNumber:I

    invoke-static {v0, v1}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(II)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lcom/dsi/ant/message/fromant/DataMessage;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 110
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    const-string v1, "Sequence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    iget v1, p0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->mSequenceNumber:I

    invoke-static {v1}, Lcom/dsi/ant/message/MessageUtils;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->isFirstMessage()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, " (FIRST)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;->isLastMessage()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, " (LAST)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
