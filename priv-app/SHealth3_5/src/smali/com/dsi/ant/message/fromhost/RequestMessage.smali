.class public final Lcom/dsi/ant/message/fromhost/RequestMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "RequestMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mRequestedMessageType:Lcom/dsi/ant/message/fromant/MessageFromAntType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->REQUEST_MESSAGE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/RequestMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/fromant/MessageFromAntType;)V
    .locals 0
    .param p1, "requestedMessageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/dsi/ant/message/fromhost/RequestMessage;->mRequestedMessageType:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 69
    return-void
.end method


# virtual methods
.method public getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v4, 0x1

    .line 87
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 90
    .local v0, "content":[B
    int-to-long v1, p1

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v4, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 92
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/RequestMessage;->mRequestedMessageType:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->getMessageId()I

    move-result v1

    invoke-static {v1, v0, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 95
    return-object v0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/dsi/ant/message/fromhost/RequestMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/RequestMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 108
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    const-string v1, "Requested message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/message/fromhost/RequestMessage;->mRequestedMessageType:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 111
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
