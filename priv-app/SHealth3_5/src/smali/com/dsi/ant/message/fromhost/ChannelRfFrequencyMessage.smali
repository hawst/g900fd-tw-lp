.class public final Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "ChannelRfFrequencyMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# instance fields
.field private final mRfFrequencyOffset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_RF_FREQUENCY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "rfFrequency"    # I

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 69
    invoke-static {p1}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->inRange(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RF Frequency out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    iput p1, p0, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->mRfFrequencyOffset:I

    .line 74
    return-void
.end method

.method public static getRealRfFrequency(I)I
    .locals 1
    .param p0, "rfFrequencyOffset"    # I

    .prologue
    .line 91
    add-int/lit16 v0, p0, 0x960

    .line 93
    .local v0, "realRfFrequencyValue":I
    return v0
.end method

.method public static final inRange(I)Z
    .locals 2
    .param p0, "frequency"    # I

    .prologue
    .line 58
    const/4 v0, 0x0

    const/16 v1, 0x7c

    invoke-static {p0, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->inRange(III)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getMessageContent(II)[B
    .locals 4
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v3, 0x1

    .line 117
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 119
    .local v0, "content":[B
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 120
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->mRfFrequencyOffset:I

    int-to-long v1, v1

    invoke-static {v1, v2, v0, v3, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 123
    return-object v0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public getRealRfFrequency()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->mRfFrequencyOffset:I

    invoke-static {v0}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->getRealRfFrequency(I)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 136
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    const-string v1, "RF frequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->getRealRfFrequency()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MHz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->mRfFrequencyOffset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
