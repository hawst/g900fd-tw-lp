.class public Lcom/dsi/ant/AntService;
.super Ljava/lang/Object;
.source "AntService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/AntService$Component;
    }
.end annotation


# static fields
.field public static final ACTION_BIND_ANT_RADIO_SERVICE:Ljava/lang/String; = "com.dsi.ant.bind.AntService"

.field private static final ANT_RADIO_SERVICE_PACKAGE_NAME:Ljava/lang/String; = "com.dsi.ant.service.socket"

.field private static final NON_BUNDLED_SERVICE_VERSION_CODE:I = 0x9c40

.field private static final NO_ADAPTER_WIDE_SERVICE_VERSION_CODE:I = 0x9c40

.field public static final SERVICE_VERSION_CODE_NOT_INSTALLED:I = 0x0

.field private static final SERVICE_VERSION_NAME_NOT_INSTALLED:Ljava/lang/String; = "-"

.field private static final TAG:Ljava/lang/String;

.field private static sServiceVersionCode:I


# instance fields
.field private final mAntRadioService:Lcom/dsi/ant/ipc/aidl/IAntServiceAidl;

.field private mChannelProvider:Lcom/dsi/ant/channel/AntChannelProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const-class v0, Lcom/dsi/ant/AntService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/AntService;->TAG:Ljava/lang/String;

    .line 124
    const/4 v0, 0x0

    sput v0, Lcom/dsi/ant/AntService;->sServiceVersionCode:I

    return-void
.end method

.method public constructor <init>(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    invoke-static {p1}, Lcom/dsi/ant/ipc/aidl/IAntServiceAidl$Stub;->asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/ipc/aidl/IAntServiceAidl;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/AntService;->mAntRadioService:Lcom/dsi/ant/ipc/aidl/IAntServiceAidl;

    .line 215
    iget-object v0, p0, Lcom/dsi/ant/AntService;->mAntRadioService:Lcom/dsi/ant/ipc/aidl/IAntServiceAidl;

    if-nez v0, :cond_0

    .line 216
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The given service binder does not seem to be for the ANT Radio Service"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_0
    return-void
.end method

.method public static bindService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z
    .locals 2
    .param p0, "bindContext"    # Landroid/content/Context;
    .param p1, "conn"    # Landroid/content/ServiceConnection;

    .prologue
    .line 195
    invoke-static {p0}, Lcom/dsi/ant/AntService;->getVersionCode(Landroid/content/Context;)I

    move-result v1

    sput v1, Lcom/dsi/ant/AntService;->sServiceVersionCode:I

    .line 197
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 198
    .local v0, "bindIntent":Landroid/content/Intent;
    const-string v1, "com.dsi.ant.bind.AntService"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    return v1
.end method

.method private getComponentBinder(Lcom/dsi/ant/AntService$Component;)Landroid/os/IBinder;
    .locals 2
    .param p1, "component"    # Lcom/dsi/ant/AntService$Component;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/dsi/ant/AntService;->mAntRadioService:Lcom/dsi/ant/ipc/aidl/IAntServiceAidl;

    invoke-virtual {p1}, Lcom/dsi/ant/AntService$Component;->getRawValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/dsi/ant/ipc/aidl/IAntServiceAidl;->getComponent(I)Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public static getVersionCode(Landroid/content/Context;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 254
    const/4 v1, 0x0

    .line 257
    .local v1, "versionCode":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.dsi.ant.service.socket"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 259
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v1, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    .end local v0    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v1

    .line 260
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static getVersionName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 281
    const-string v1, "-"

    .line 284
    .local v1, "versionName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.dsi.ant.service.socket"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 286
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    .end local v0    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-object v1

    .line 287
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static hasAdapterWideConfigurationSupport()Z
    .locals 2

    .prologue
    .line 163
    sget v0, Lcom/dsi/ant/AntService;->sServiceVersionCode:I

    const v1, 0x9c40

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static requiresBundle()Z
    .locals 2

    .prologue
    .line 150
    sget v0, Lcom/dsi/ant/AntService;->sServiceVersionCode:I

    const v1, 0x9c40

    if-gt v0, v1, :cond_0

    sget v0, Lcom/dsi/ant/AntService;->sServiceVersionCode:I

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getChannelProvider()Lcom/dsi/ant/channel/AntChannelProvider;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 231
    iget-object v0, p0, Lcom/dsi/ant/AntService;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProvider;

    if-nez v0, :cond_0

    .line 232
    new-instance v0, Lcom/dsi/ant/channel/AntChannelProvider;

    sget-object v1, Lcom/dsi/ant/AntService$Component;->CHANNEL_PROVIDER:Lcom/dsi/ant/AntService$Component;

    invoke-direct {p0, v1}, Lcom/dsi/ant/AntService;->getComponentBinder(Lcom/dsi/ant/AntService$Component;)Landroid/os/IBinder;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/channel/AntChannelProvider;-><init>(Landroid/os/IBinder;)V

    iput-object v0, p0, Lcom/dsi/ant/AntService;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProvider;

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/AntService;->mChannelProvider:Lcom/dsi/ant/channel/AntChannelProvider;

    return-object v0
.end method
