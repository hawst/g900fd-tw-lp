.class Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;
.super Ljava/lang/Object;
.source "BackgroundScanTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeviceIdToggleData"
.end annotation


# instance fields
.field private deviceNumber:I

.field private deviceType:I

.field private seenToggleValue:Z

.field private supportsPages:Z

.field private transType:I


# direct methods
.method public constructor <init>(Lcom/dsi/ant/message/ExtendedData;B)V
    .locals 2
    .param p1, "extData"    # Lcom/dsi/ant/message/ExtendedData;
    .param p2, "dataPage"    # B

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-virtual {p1}, Lcom/dsi/ant/message/ExtendedData;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->deviceNumber:I

    .line 46
    invoke-virtual {p1}, Lcom/dsi/ant/message/ExtendedData;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/message/ChannelId;->getTransmissionType()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->transType:I

    .line 47
    invoke-virtual {p1}, Lcom/dsi/ant/message/ExtendedData;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->deviceType:I

    .line 48
    and-int/lit16 v0, p2, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->seenToggleValue:Z

    .line 49
    iput-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->supportsPages:Z

    .line 50
    return-void

    :cond_0
    move v0, v1

    .line 48
    goto :goto_0
.end method

.method static synthetic access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->supportsPages:Z

    return v0
.end method


# virtual methods
.method public getDeviceNumber()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->deviceNumber:I

    return v0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->deviceType:I

    return v0
.end method

.method public getTransType()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->transType:I

    return v0
.end method

.method public isSeenToggleValue()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->seenToggleValue:Z

    return v0
.end method

.method public setSupportsPages(Z)V
    .locals 0
    .param p1, "supportsPages"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->supportsPages:Z

    .line 75
    return-void
.end method
