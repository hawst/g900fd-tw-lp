.class Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;
.super Ljava/lang/Object;
.source "BackgroundScanTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DataPageChecker"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;
    }
.end annotation


# instance fields
.field private seenDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;->seenDevices:Ljava/util/ArrayList;

    .line 86
    return-void
.end method


# virtual methods
.method public isDataPagesSupported(Lcom/dsi/ant/message/ExtendedData;B)Z
    .locals 7
    .param p1, "extData"    # Lcom/dsi/ant/message/ExtendedData;
    .param p2, "dataPage"    # B

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 97
    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;->seenDevices:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 120
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;->seenDevices:Ljava/util/ArrayList;

    new-instance v4, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;

    invoke-direct {v4, p1, p2}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;-><init>(Lcom/dsi/ant/message/ExtendedData;B)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 121
    :cond_1
    :goto_0
    return v2

    .line 97
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;

    .line 99
    .local v0, "i":Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;
    invoke-virtual {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->getDeviceNumber()I

    move-result v5

    invoke-virtual {p1}, Lcom/dsi/ant/message/ExtendedData;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v6

    invoke-virtual {v6}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v6

    if-ne v5, v6, :cond_0

    .line 100
    invoke-virtual {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->getDeviceType()I

    move-result v5

    invoke-virtual {p1}, Lcom/dsi/ant/message/ExtendedData;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v6

    invoke-virtual {v6}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v6

    if-ne v5, v6, :cond_0

    .line 101
    invoke-virtual {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->getTransType()I

    move-result v5

    invoke-virtual {p1}, Lcom/dsi/ant/message/ExtendedData;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v6

    invoke-virtual {v6}, Lcom/dsi/ant/message/ChannelId;->getTransmissionType()I

    move-result v6

    if-ne v5, v6, :cond_0

    .line 103
    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->supportsPages:Z
    invoke-static {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 107
    and-int/lit16 v4, p2, 0x80

    if-eqz v4, :cond_3

    move v1, v2

    .line 108
    .local v1, "newSeenToggleValue":Z
    :goto_1
    invoke-virtual {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->isSeenToggleValue()Z

    move-result v4

    if-eq v4, v1, :cond_4

    .line 110
    invoke-virtual {v0, v2}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker$DeviceIdToggleData;->setSupportsPages(Z)V

    goto :goto_0

    .end local v1    # "newSeenToggleValue":Z
    :cond_3
    move v1, v3

    .line 107
    goto :goto_1

    .restart local v1    # "newSeenToggleValue":Z
    :cond_4
    move v2, v3

    .line 115
    goto :goto_0
.end method
