.class Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1$1;
.super Ljava/lang/Object;
.source "BackgroundScanController.java"

# interfaces
.implements Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$IDeathHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;


# direct methods
.method constructor <init>(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1$1;->this$2:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExecutorDeath()V
    .locals 3

    .prologue
    .line 177
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1$1;->this$2:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;
    invoke-static {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    move-result-object v0

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v0

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->connLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 179
    :try_start_0
    const-string v0, "BackgroundScanController"

    const-string v2, "Executor death reported"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1$1;->this$2:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;
    invoke-static {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    move-result-object v0

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v0

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->OTHER_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {v0, v2}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    .line 181
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1$1;->this$2:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;
    invoke-static {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    move-result-object v0

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopBackgroundScan()V

    .line 177
    monitor-exit v1

    .line 183
    return-void

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
