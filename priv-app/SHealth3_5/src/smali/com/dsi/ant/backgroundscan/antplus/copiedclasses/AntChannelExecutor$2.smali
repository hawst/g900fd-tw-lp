.class Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;
.super Ljava/lang/Object;
.source "AntChannelExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->startWorkerThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;


# direct methods
.method constructor <init>(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 101
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->stopWorkerThread:Z
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$3(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 199
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$2(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 201
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 202
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v2

    const v4, 0x7fffffff

    invoke-virtual {v2, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->handleInterruptRequestInternal(I)Z

    .line 203
    :cond_2
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$6(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V

    .line 199
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 205
    return-void

    .line 104
    :cond_3
    const/4 v1, 0x0

    .line 107
    .local v1, "nextTask":Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    :try_start_1
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$4(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v2

    if-nez v2, :cond_4

    .line 109
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->nextTaskExchange:Ljava/util/concurrent/Exchanger;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$5(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Ljava/util/concurrent/Exchanger;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "nextTask":Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    check-cast v1, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 125
    .restart local v1    # "nextTask":Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    :goto_2
    if-eqz v1, :cond_0

    .line 139
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$2(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 141
    :try_start_2
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    invoke-static {v2, v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$6(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V

    .line 142
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v2

    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->channel:Lcom/dsi/ant/channel/AntChannel;
    invoke-static {v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$7(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->initTaskInternal(Lcom/dsi/ant/channel/AntChannel;)V

    .line 144
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->stopWorkerThread:Z
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$3(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v2

    const v4, 0x7fffffff

    invoke-virtual {v2, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->handleInterruptRequestInternal(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 148
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$6(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V

    .line 149
    monitor-exit v3

    goto :goto_1

    .line 139
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 116
    :cond_4
    :try_start_3
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->nextTaskExchange:Ljava/util/concurrent/Exchanger;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$5(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Ljava/util/concurrent/Exchanger;

    move-result-object v2

    const/4 v3, 0x0

    const-wide/16 v4, 0x64

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3, v4, v5, v6}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "nextTask":Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    check-cast v1, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    .restart local v1    # "nextTask":Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    goto :goto_2

    .line 117
    .end local v1    # "nextTask":Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_4
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$4(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v1

    .restart local v1    # "nextTask":Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    goto :goto_2

    .line 127
    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    .end local v1    # "nextTask":Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    :catch_1
    move-exception v0

    .line 133
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->stopWorkerThread:Z
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$3(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 134
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    const-string v3, "Worker thread interrupted unexpectedly"

    # invokes: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->killExecutor(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$0(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 139
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "nextTask":Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    :cond_5
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 157
    :goto_3
    :try_start_6
    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$8()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Begin doWork() in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskNameInternal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->doWorkInternal()V

    .line 159
    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$8()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "End doWork() in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskNameInternal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$2(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_2

    .line 163
    :try_start_7
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 164
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->cancelling:Z
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$9(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 166
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$10(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Z)V

    .line 167
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$6(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V

    .line 174
    :goto_4
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v2

    if-nez v2, :cond_7

    .line 176
    monitor-exit v3

    goto/16 :goto_0

    .line 161
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v2
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_2

    .line 193
    :catch_2
    move-exception v0

    .line 195
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RemoteException occured executing DoWork() in task: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskNameInternal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->killExecutor(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$0(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 171
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_6
    :try_start_9
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getNextTaskToExecute()Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$6(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V

    goto :goto_4

    .line 180
    :cond_7
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v2

    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->channel:Lcom/dsi/ant/channel/AntChannel;
    invoke-static {v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$7(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->initTaskInternal(Lcom/dsi/ant/channel/AntChannel;)V

    .line 182
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->stopWorkerThread:Z
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$3(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    invoke-static {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v2

    const v4, 0x7fffffff

    invoke-virtual {v2, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->handleInterruptRequestInternal(I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 186
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;->this$0:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->access$6(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V

    .line 187
    monitor-exit v3

    goto/16 :goto_0

    .line 161
    :cond_8
    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_3

    .line 199
    .end local v1    # "nextTask":Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    :catchall_2
    move-exception v2

    :try_start_a
    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    throw v2
.end method
