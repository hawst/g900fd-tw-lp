.class public Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;
.super Ljava/lang/Object;
.source "AntChannelExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$IDeathHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final TASKRANK_CANCELREQUEST:I = 0x7ffffffe

.field public static final TASKRANK_SHUTDOWNORDEATH:I = 0x7fffffff


# instance fields
.field private cancelling:Z

.field private channel:Lcom/dsi/ant/channel/AntChannel;

.field private currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

.field private deathHandler:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$IDeathHandler;

.field private final executorDeadLock:Ljava/lang/Object;

.field private idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

.field private isExecutorDead:Z

.field private final nextTaskExchange:Ljava/util/concurrent/Exchanger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Exchanger",
            "<",
            "Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;",
            ">;"
        }
    .end annotation
.end field

.field private final shutdownLock:Ljava/lang/Object;

.field private stopWorkerThread:Z

.field private final taskLock:Ljava/lang/Object;

.field private taskThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->TAG:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$IDeathHandler;)V
    .locals 2
    .param p1, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "deathHandler"    # Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$IDeathHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->isExecutorDead:Z

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->executorDeadLock:Ljava/lang/Object;

    .line 35
    iput-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->stopWorkerThread:Z

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->shutdownLock:Ljava/lang/Object;

    .line 39
    new-instance v0, Ljava/util/concurrent/Exchanger;

    invoke-direct {v0}, Ljava/util/concurrent/Exchanger;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->nextTaskExchange:Ljava/util/concurrent/Exchanger;

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskLock:Ljava/lang/Object;

    .line 45
    iput-object p2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->deathHandler:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$IDeathHandler;

    .line 46
    invoke-virtual {p0, p1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->initExecutorChannel(Lcom/dsi/ant/channel/AntChannel;)V

    .line 47
    return-void
.end method

.method static synthetic access$0(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 341
    invoke-direct {p0, p1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->killExecutor(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    return-object v0
.end method

.method static synthetic access$10(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Z)V
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->cancelling:Z

    return-void
.end method

.method static synthetic access$2(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->stopWorkerThread:Z

    return v0
.end method

.method static synthetic access$4(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    return-object v0
.end method

.method static synthetic access$5(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Ljava/util/concurrent/Exchanger;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->nextTaskExchange:Ljava/util/concurrent/Exchanger;

    return-object v0
.end method

.method static synthetic access$6(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    return-void
.end method

.method static synthetic access$7(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Lcom/dsi/ant/channel/AntChannel;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->channel:Lcom/dsi/ant/channel/AntChannel;

    return-object v0
.end method

.method static synthetic access$8()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->cancelling:Z

    return v0
.end method

.method private isWorkerStopped()Z
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 454
    :cond_0
    const/4 v0, 0x1

    .line 456
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private killExecutor(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 343
    sget-object v0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Executor Dying: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->executorDeadLock:Ljava/lang/Object;

    monitor-enter v1

    .line 346
    :try_start_0
    iget-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->isExecutorDead:Z

    if-nez v0, :cond_0

    .line 348
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 349
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->deathHandler:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$IDeathHandler;

    invoke-interface {v0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$IDeathHandler;->onExecutorDeath()V

    .line 344
    :cond_0
    monitor-exit v1

    .line 352
    return-void

    .line 344
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private sendInterruptRequest(I)Z
    .locals 4
    .param p1, "interruptingTaskRank"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 428
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskLock:Ljava/lang/Object;

    monitor-enter v2

    .line 430
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-eqz v3, :cond_1

    .line 432
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->cancelling:Z

    .line 433
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v3, p1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->handleInterruptRequestInternal(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 435
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 436
    monitor-exit v2

    .line 446
    :goto_0
    return v0

    .line 440
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->cancelling:Z

    .line 441
    monitor-exit v2

    move v0, v1

    goto :goto_0

    .line 446
    :cond_1
    monitor-exit v2

    goto :goto_0

    .line 428
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private startWorkerThread()V
    .locals 3

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->isExecutorDead:Z

    if-eqz v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to start worker thread on dead executor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->stopWorkerThread:Z

    .line 96
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;

    invoke-direct {v1, p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$2;-><init>(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)V

    .line 206
    const-string v2, "AntChannelExecutorWorker"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 96
    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskThread:Ljava/lang/Thread;

    .line 207
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 208
    return-void
.end method


# virtual methods
.method public cancelCurrentTask(I)Z
    .locals 8
    .param p1, "msToWait"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 218
    invoke-direct {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->isWorkerStopped()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 238
    :goto_0
    return v1

    .line 221
    :cond_0
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskLock:Ljava/lang/Object;

    monitor-enter v3

    .line 223
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-nez v4, :cond_1

    .line 224
    monitor-exit v3

    goto :goto_0

    .line 221
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 226
    :cond_1
    const v4, 0x7ffffffe

    :try_start_1
    invoke-direct {p0, v4}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->sendInterruptRequest(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 227
    monitor-exit v3

    move v1, v2

    goto :goto_0

    .line 221
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233
    :try_start_2
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->nextTaskExchange:Ljava/util/concurrent/Exchanger;

    const/4 v4, 0x0

    int-to-long v5, p1

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v6, v7}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 234
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    move v1, v2

    .line 236
    goto :goto_0
.end method

.method protected initExecutorChannel(Lcom/dsi/ant/channel/AntChannel;)V
    .locals 1
    .param p1, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 51
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->channel:Lcom/dsi/ant/channel/AntChannel;

    .line 53
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$1;-><init>(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)V

    invoke-virtual {p1, v0}, Lcom/dsi/ant/channel/AntChannel;->setChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V

    .line 87
    invoke-direct {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->startWorkerThread()V

    .line 88
    return-void
.end method

.method public setIdleTask(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V
    .locals 4
    .param p1, "task"    # Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .prologue
    .line 253
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskLock:Ljava/lang/Object;

    monitor-enter v2

    .line 256
    :try_start_0
    invoke-direct {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->isWorkerStopped()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 259
    monitor-exit v2

    .line 292
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-ne v1, v3, :cond_3

    .line 265
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 267
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-nez v1, :cond_2

    .line 268
    const v1, 0x7ffffffe

    invoke-direct {p0, v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->sendInterruptRequest(I)Z

    .line 253
    :cond_1
    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 270
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskRank()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->sendInterruptRequest(I)Z

    goto :goto_1

    .line 275
    :cond_3
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 278
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    .line 282
    :try_start_2
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->nextTaskExchange:Ljava/util/concurrent/Exchanger;

    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->idleTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 283
    :catch_0
    move-exception v0

    .line 285
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->TAG:Ljava/lang/String;

    const-string v3, "InterruptedException trying to start idleTask"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public shutdown(Z)Lcom/dsi/ant/channel/AntChannel;
    .locals 5
    .param p1, "releaseChannel"    # Z

    .prologue
    const/4 v1, 0x0

    .line 365
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->shutdownLock:Ljava/lang/Object;

    monitor-enter v2

    .line 367
    :try_start_0
    iget-boolean v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->isExecutorDead:Z

    if-eqz v3, :cond_0

    .line 368
    monitor-exit v2

    .line 422
    :goto_0
    return-object v1

    .line 370
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->isExecutorDead:Z

    .line 371
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->stopWorkerThread:Z

    .line 372
    invoke-direct {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->isWorkerStopped()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-eq v1, v3, :cond_1

    .line 374
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-eqz v1, :cond_2

    .line 378
    const v1, 0x7fffffff

    invoke-direct {p0, v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->sendInterruptRequest(I)Z

    .line 374
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 388
    :try_start_2
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskThread:Ljava/lang/Thread;

    const-wide/16 v3, 0xbb8

    invoke-virtual {v1, v3, v4}, Ljava/lang/Thread;->join(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 398
    :goto_2
    :try_start_3
    invoke-direct {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->isWorkerStopped()Z

    move-result v1

    if-nez v1, :cond_1

    .line 400
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->TAG:Ljava/lang/String;

    const-string v3, "Task not shutting down gracefully, forcing channel release"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    const/4 p1, 0x1

    .line 405
    :cond_1
    if-eqz p1, :cond_3

    .line 407
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 408
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->channel:Lcom/dsi/ant/channel/AntChannel;

    .line 422
    :goto_3
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->channel:Lcom/dsi/ant/channel/AntChannel;

    monitor-exit v2

    goto :goto_0

    .line 365
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 382
    :cond_2
    :try_start_4
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 374
    :catchall_1
    move-exception v1

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v1

    .line 389
    :catch_0
    move-exception v0

    .line 392
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->TAG:Ljava/lang/String;

    const-string v3, "InterruptedException while waiting for task thread to close."

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 414
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_3
    :try_start_6
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->clearChannelEventHandler()V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    .line 415
    :catch_1
    move-exception v0

    .line 417
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_7
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException trying to clear handler in cleanShutdown()"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->channel:Lcom/dsi/ant/channel/AntChannel;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3
.end method

.method public startTask(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;I)Z
    .locals 8
    .param p1, "task"    # Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    .param p2, "msToWaitOnCurrentTask"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 303
    if-nez p1, :cond_0

    .line 304
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Task parameter was null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 306
    :cond_0
    invoke-direct {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->isWorkerStopped()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 307
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Worker thread is not running"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 311
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->nextTaskExchange:Ljava/util/concurrent/Exchanger;

    monitor-enter v3

    .line 314
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->taskLock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    :try_start_1
    iget-object v5, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->currentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-eqz v5, :cond_3

    .line 318
    invoke-virtual {p1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskRankInternal()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->sendInterruptRequest(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 319
    move v1, p2

    .line 314
    .local v1, "waitMs":I
    :goto_0
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 331
    :try_start_2
    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->nextTaskExchange:Ljava/util/concurrent/Exchanger;

    int-to-long v5, v1

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, p1, v5, v6, v7}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 332
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v2, 0x1

    .line 336
    .end local v1    # "waitMs":I
    :goto_1
    return v2

    .line 321
    :cond_2
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    monitor-exit v3

    goto :goto_1

    .line 311
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v2

    .line 325
    :cond_3
    const/16 v1, 0x64

    .restart local v1    # "waitMs":I
    goto :goto_0

    .line 314
    .end local v1    # "waitMs":I
    :catchall_1
    move-exception v2

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v2

    .line 333
    .restart local v1    # "waitMs":I
    :catch_0
    move-exception v0

    .line 335
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    sget-object v4, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Timeout trying to start task, waited "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1
.end method
