.class Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;
.super Ljava/lang/Object;
.source "BackgroundScanController.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;


# direct methods
.method constructor <init>(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    return-object v0
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 152
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;

    invoke-direct {v1, p0, p2}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;-><init>(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;Landroid/os/IBinder;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 208
    .local v0, "setupThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 210
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 215
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->connLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 217
    :try_start_0
    const-string v0, "BackgroundScanController"

    const-string v2, "ARS service disconnected"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->OTHER_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {v0, v2}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    .line 219
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    invoke-virtual {v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopBackgroundScan()V

    .line 215
    monitor-exit v1

    .line 221
    return-void

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
