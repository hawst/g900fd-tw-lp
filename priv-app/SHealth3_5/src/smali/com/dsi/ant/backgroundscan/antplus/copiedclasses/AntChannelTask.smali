.class public abstract Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
.super Ljava/lang/Object;
.source "AntChannelTask.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$dsi$ant$message$fromant$MessageFromAntType:[I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public channel:Lcom/dsi/ant/channel/AntChannel;

.field private doWorkThread:Ljava/lang/Thread;

.field private final isClosingSemaphore:Ljava/util/concurrent/Semaphore;

.field private mIsCloseBugCheck:Z

.field private mIsClosing:Z

.field private mProcessingOn:Z

.field private nextTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

.field private parentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

.field private final processMsgLock:Ljava/lang/Object;

.field private subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

.field private subTaskCancelling:Z


# direct methods
.method static synthetic $SWITCH_TABLE$com$dsi$ant$message$fromant$MessageFromAntType()[I
    .locals 3

    .prologue
    .line 25
    sget-object v0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->$SWITCH_TABLE$com$dsi$ant$message$fromant$MessageFromAntType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->values()[Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_1
    :try_start_1
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ANT_VERSION:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    :goto_2
    :try_start_2
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BROADCAST_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_3
    :try_start_3
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    :goto_4
    :try_start_4
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CAPABILITIES:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_5
    :try_start_5
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_6
    :try_start_6
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_ID:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_7
    :try_start_7
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_8
    :try_start_8
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_STATUS:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_9
    :try_start_9
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->OTHER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_a
    :try_start_a
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->SERIAL_NUMBER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_b
    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->$SWITCH_TABLE$com$dsi$ant$message$fromant$MessageFromAntType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_b

    :catch_1
    move-exception v1

    goto :goto_a

    :catch_2
    move-exception v1

    goto :goto_9

    :catch_3
    move-exception v1

    goto :goto_8

    :catch_4
    move-exception v1

    goto :goto_7

    :catch_5
    move-exception v1

    goto :goto_6

    :catch_6
    move-exception v1

    goto :goto_5

    :catch_7
    move-exception v1

    goto :goto_4

    :catch_8
    move-exception v1

    goto :goto_3

    :catch_9
    move-exception v1

    goto :goto_2

    :catch_a
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsClosing:Z

    .line 30
    iput-boolean v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsCloseBugCheck:Z

    .line 31
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->isClosingSemaphore:Ljava/util/concurrent/Semaphore;

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->processMsgLock:Ljava/lang/Object;

    .line 33
    iput-boolean v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mProcessingOn:Z

    .line 34
    iput-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->nextTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 35
    iput-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->doWorkThread:Ljava/lang/Thread;

    .line 37
    iput-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 38
    iput-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->parentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 39
    iput-boolean v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTaskCancelling:Z

    .line 25
    return-void
.end method

.method private flushAndEnsureCloseInternal()Lcom/dsi/ant/message/ChannelState;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 398
    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->isClosingSemaphore:Ljava/util/concurrent/Semaphore;

    monitor-enter v4

    .line 400
    :try_start_0
    iget-object v5, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->processMsgLock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 403
    :try_start_1
    iget-boolean v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsClosing:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v3, :cond_3

    .line 409
    :try_start_2
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 415
    .local v2, "state":Lcom/dsi/ant/message/ChannelState;
    :try_start_3
    sget-object v3, Lcom/dsi/ant/message/ChannelState;->ASSIGNED:Lcom/dsi/ant/message/ChannelState;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/dsi/ant/message/ChannelState;->UNASSIGNED:Lcom/dsi/ant/message/ChannelState;

    if-ne v2, v3, :cond_1

    .line 416
    :cond_0
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 476
    .end local v2    # "state":Lcom/dsi/ant/message/ChannelState;
    :goto_0
    return-object v2

    .line 410
    :catch_0
    move-exception v1

    .line 412
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :try_start_5
    sget-object v3, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "AntCommandFailedException in requestStatus call: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    new-instance v3, Landroid/os/RemoteException;

    invoke-direct {v3}, Landroid/os/RemoteException;-><init>()V

    throw v3

    .line 400
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v3

    .line 398
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v3

    .line 418
    .restart local v2    # "state":Lcom/dsi/ant/message/ChannelState;
    :cond_1
    const/4 v3, 0x1

    :try_start_7
    iput-boolean v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsClosing:Z

    .line 419
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->isClosingSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_2

    .line 421
    sget-object v3, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    const-string v6, "AntChannelTask could not acquire closing semaphore in locked closing call"

    invoke-static {v3, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    new-instance v3, Landroid/os/RemoteException;

    invoke-direct {v3}, Landroid/os/RemoteException;-><init>()V

    throw v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 429
    :cond_2
    const/4 v0, 0x0

    .line 434
    .local v0, "closeAttempts":I
    :goto_1
    :try_start_8
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->close()V
    :try_end_8
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 400
    .end local v0    # "closeAttempts":I
    .end local v2    # "state":Lcom/dsi/ant/message/ChannelState;
    :cond_3
    :try_start_9
    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 472
    :try_start_a
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->isClosingSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 473
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->isClosingSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 476
    sget-object v2, Lcom/dsi/ant/message/ChannelState;->ASSIGNED:Lcom/dsi/ant/message/ChannelState;

    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_0

    .line 437
    .restart local v0    # "closeAttempts":I
    .restart local v2    # "state":Lcom/dsi/ant/message/ChannelState;
    :catch_1
    move-exception v1

    .line 439
    .restart local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :try_start_b
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v3

    sget-object v6, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v3, v6, :cond_5

    .line 441
    add-int/lit8 v0, v0, 0x1

    .line 442
    const/16 v3, 0x1b

    if-le v0, v3, :cond_4

    .line 444
    sget-object v3, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    const-string v6, "Close() could not break TRANSFER_PROCESSING loop"

    invoke-static {v3, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    new-instance v3, Landroid/os/RemoteException;

    invoke-direct {v3}, Landroid/os/RemoteException;-><init>()V

    throw v3

    .line 449
    :cond_4
    const-wide/16 v6, 0x64

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_1

    .line 452
    :cond_5
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v3

    sget-object v6, Lcom/dsi/ant/channel/AntCommandFailureReason;->CHANNEL_RESPONSE:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v3, v6, :cond_6

    .line 453
    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->getResponseMessage()Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getResponseCode()Lcom/dsi/ant/message/ResponseCode;

    move-result-object v3

    sget-object v6, Lcom/dsi/ant/message/ResponseCode;->CHANNEL_IN_WRONG_STATE:Lcom/dsi/ant/message/ResponseCode;

    if-eq v3, v6, :cond_3

    .line 464
    :cond_6
    sget-object v3, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ACFE in close call: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    new-instance v3, Landroid/os/RemoteException;

    invoke-direct {v3}, Landroid/os/RemoteException;-><init>()V

    throw v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0
.end method

.method private onReceiveMessageInternal(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 1
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 594
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    iget-boolean v0, v0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mProcessingOn:Z

    if-eqz v0, :cond_0

    .line 595
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-direct {v0, p1, p2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->onReceiveMessageInternal(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 598
    :goto_0
    return-void

    .line 597
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    goto :goto_0
.end method


# virtual methods
.method public disableMessageProcessing()V
    .locals 2

    .prologue
    .line 133
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->processMsgLock:Ljava/lang/Object;

    monitor-enter v1

    .line 135
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mProcessingOn:Z

    .line 133
    monitor-exit v1

    .line 137
    return-void

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract doWork()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method doWorkInternal()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 581
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->doWorkThread:Ljava/lang/Thread;

    .line 582
    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->doWork()V

    .line 583
    iput-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->doWorkThread:Ljava/lang/Thread;

    .line 584
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->processMsgLock:Ljava/lang/Object;

    monitor-enter v1

    .line 586
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->disableMessageProcessing()V

    .line 587
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    .line 584
    monitor-exit v1

    .line 589
    return-void

    .line 584
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public enableMessageProcessing()V
    .locals 2

    .prologue
    .line 121
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->processMsgLock:Ljava/lang/Object;

    monitor-enter v1

    .line 123
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mProcessingOn:Z

    .line 121
    monitor-exit v1

    .line 125
    return-void

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public flushAndEnsureClosedChannel()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->parentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->parentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->flushAndEnsureClosedChannel()V

    .line 197
    :goto_0
    return-void

    .line 196
    :cond_0
    invoke-direct {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->flushAndEnsureCloseInternal()Lcom/dsi/ant/message/ChannelState;

    goto :goto_0
.end method

.method public flushAndEnsureUnassignedChannel()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 488
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->parentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-eqz v2, :cond_1

    .line 490
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->parentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->flushAndEnsureUnassignedChannel()V

    .line 525
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    invoke-direct {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->flushAndEnsureCloseInternal()Lcom/dsi/ant/message/ChannelState;

    move-result-object v2

    sget-object v3, Lcom/dsi/ant/message/ChannelState;->ASSIGNED:Lcom/dsi/ant/message/ChannelState;

    if-ne v2, v3, :cond_0

    .line 496
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->processMsgLock:Ljava/lang/Object;

    monitor-enter v3

    .line 500
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->unassign()V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 496
    :try_start_1
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 501
    :catch_0
    move-exception v0

    .line 506
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :try_start_2
    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ACFE in flushAndEnsureUnassignedChannel() unassign(): "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 510
    :try_start_3
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v2

    sget-object v4, Lcom/dsi/ant/message/ChannelState;->UNASSIGNED:Lcom/dsi/ant/message/ChannelState;

    if-ne v2, v4, :cond_2

    .line 512
    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    const-string v4, "flushAndEnsureUnassignedChannel() recovered because state is already unassigned"

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 513
    :try_start_4
    monitor-exit v3

    goto :goto_0

    .line 515
    :catch_1
    move-exception v1

    .line 517
    .local v1, "e1":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ACFE in flushAndEnsureUnassignedChannel() recovery getStatus() call: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    .end local v1    # "e1":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_2
    new-instance v2, Landroid/os/RemoteException;

    invoke-direct {v2}, Landroid/os/RemoteException;-><init>()V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method getNextTaskToExecute()Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->nextTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    return-object v0
.end method

.method public abstract getTaskName()Ljava/lang/String;
.end method

.method getTaskNameInternal()Ljava/lang/String;
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-eqz v0, :cond_0

    .line 574
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "-running subTask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskNameInternal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 576
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTaskRank()I
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method getTaskRankInternal()I
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskRankInternal()I

    move-result v0

    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskRank()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 568
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskRank()I

    move-result v0

    goto :goto_0
.end method

.method public abstract handleExecutorShutdown()V
.end method

.method public handleInterruptRequest(I)Z
    .locals 1
    .param p1, "interruptingTaskRank"    # I

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method handleInterruptRequestInternal(I)Z
    .locals 2
    .param p1, "interruptingTaskRank"    # I

    .prologue
    const/4 v0, 0x1

    .line 539
    const v1, 0x7fffffff

    if-ne p1, v1, :cond_2

    .line 541
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-eqz v1, :cond_0

    .line 543
    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTaskCancelling:Z

    .line 544
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->handleExecutorShutdown()V

    .line 546
    :cond_0
    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->handleExecutorShutdown()V

    .line 559
    :cond_1
    :goto_0
    return v0

    .line 552
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    if-eqz v1, :cond_4

    .line 554
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v1, p1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->handleInterruptRequest(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTaskCancelling:Z

    .line 559
    :goto_1
    iget-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTaskCancelling:Z

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->handleInterruptRequest(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 557
    :cond_4
    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTaskCancelling:Z

    goto :goto_1
.end method

.method initAsSubTask(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V
    .locals 2
    .param p1, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "parentTask"    # Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .prologue
    .line 231
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->processMsgLock:Ljava/lang/Object;

    monitor-enter v1

    .line 233
    :try_start_0
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    .line 234
    iput-object p2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->parentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 235
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mProcessingOn:Z

    .line 237
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 238
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTaskCancelling:Z

    .line 231
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->initTask()V

    .line 242
    return-void

    .line 231
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public abstract initTask()V
.end method

.method initTaskInternal(Lcom/dsi/ant/channel/AntChannel;)V
    .locals 3
    .param p1, "channel"    # Lcom/dsi/ant/channel/AntChannel;

    .prologue
    .line 207
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->processMsgLock:Ljava/lang/Object;

    monitor-enter v1

    .line 209
    :try_start_0
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    .line 211
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsClosing:Z

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsCloseBugCheck:Z

    .line 213
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->isClosingSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    .line 214
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->isClosingSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 215
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mProcessingOn:Z

    .line 217
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 218
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->parentTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 207
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->initTask()V

    .line 222
    return-void

    .line 207
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public abstract onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method processMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 12
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 250
    iget-object v7, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->processMsgLock:Ljava/lang/Object;

    monitor-enter v7

    .line 252
    :try_start_0
    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    if-nez v6, :cond_0

    .line 253
    monitor-exit v7

    .line 385
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsCloseBugCheck:Z

    if-eqz v6, :cond_4

    sget-object v6, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_STATUS:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-ne p1, v6, :cond_4

    .line 258
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsCloseBugCheck:Z

    .line 259
    iget-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsClosing:Z

    if-eqz v6, :cond_3

    .line 261
    new-instance v6, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    invoke-direct {v6, p2}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v6}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v5

    .line 262
    .local v5, "state":Lcom/dsi/ant/message/ChannelState;
    sget-object v6, Lcom/dsi/ant/message/ChannelState;->ASSIGNED:Lcom/dsi/ant/message/ChannelState;

    if-eq v5, v6, :cond_1

    sget-object v6, Lcom/dsi/ant/message/ChannelState;->UNASSIGNED:Lcom/dsi/ant/message/ChannelState;

    if-ne v5, v6, :cond_3

    .line 264
    :cond_1
    sget-object v6, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    const-string v8, "Detected close state before close event. Possible occurence of dropped close event bug."

    invoke-static {v6, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsClosing:Z

    .line 266
    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->isClosingSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v6}, Ljava/util/concurrent/Semaphore;->release()V

    .line 268
    iget-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mProcessingOn:Z

    if-eqz v6, :cond_2

    .line 271
    sget-object v0, Lcom/dsi/ant/message/EventCode;->CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

    .line 273
    .local v0, "closedCode":Lcom/dsi/ant/message/EventCode;
    new-instance v3, Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .line 274
    const/16 v6, 0x40

    .line 276
    const/4 v8, 0x3

    new-array v8, v8, [B

    const/4 v9, 0x0

    .line 277
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v10

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    const/4 v9, 0x1

    .line 278
    const/4 v10, 0x1

    aput-byte v10, v8, v9

    const/4 v9, 0x2

    .line 279
    invoke-virtual {v0}, Lcom/dsi/ant/message/EventCode;->getRawValue()I

    move-result v10

    int-to-byte v10, v10

    aput-byte v10, v8, v9

    .line 273
    invoke-direct {v3, v6, v8}, Lcom/dsi/ant/message/ipc/AntMessageParcel;-><init>(I[B)V

    .line 281
    .local v3, "fakeCloseEvent":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    sget-object v6, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    const-string v8, "--forwarded fake close event."

    invoke-static {v6, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    sget-object v6, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-direct {p0, v6, v3}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->onReceiveMessageInternal(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 285
    .end local v0    # "closedCode":Lcom/dsi/ant/message/EventCode;
    .end local v3    # "fakeCloseEvent":Lcom/dsi/ant/message/ipc/AntMessageParcel;
    :cond_2
    monitor-exit v7

    goto :goto_0

    .line 250
    .end local v5    # "state":Lcom/dsi/ant/message/ChannelState;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 289
    :cond_3
    :try_start_1
    monitor-exit v7

    goto :goto_0

    .line 292
    :cond_4
    iget-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsClosing:Z

    if-eqz v6, :cond_a

    .line 294
    invoke-static {}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->$SWITCH_TABLE$com$dsi$ant$message$fromant$MessageFromAntType()[I

    move-result-object v6

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v8

    aget v6, v6, v8

    packed-switch v6, :pswitch_data_0

    .line 318
    :cond_5
    iget-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsCloseBugCheck:Z

    if-nez v6, :cond_6

    .line 321
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsCloseBugCheck:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 324
    :try_start_2
    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 333
    :cond_6
    :try_start_3
    monitor-exit v7

    goto/16 :goto_0

    .line 305
    :pswitch_0
    new-instance v6, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v6, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v6}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v2

    .line 306
    .local v2, "eventCode":Lcom/dsi/ant/message/EventCode;
    sget-object v6, Lcom/dsi/ant/message/EventCode;->CHANNEL_CLOSED:Lcom/dsi/ant/message/EventCode;

    if-eq v2, v6, :cond_7

    .line 307
    sget-object v6, Lcom/dsi/ant/message/EventCode;->RX_SEARCH_TIMEOUT:Lcom/dsi/ant/message/EventCode;

    if-ne v2, v6, :cond_5

    .line 312
    :cond_7
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsClosing:Z

    .line 313
    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->isClosingSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v6}, Ljava/util/concurrent/Semaphore;->release()V

    .line 378
    .end local v2    # "eventCode":Lcom/dsi/ant/message/EventCode;
    :cond_8
    :goto_1
    :pswitch_1
    iget-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mProcessingOn:Z

    if-eqz v6, :cond_9

    .line 382
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->onReceiveMessageInternal(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 250
    :cond_9
    monitor-exit v7

    goto/16 :goto_0

    .line 325
    :catch_0
    move-exception v1

    .line 327
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v6, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "ACFE requesting status "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 330
    new-instance v6, Landroid/os/RemoteException;

    invoke-direct {v6}, Landroid/os/RemoteException;-><init>()V

    throw v6

    .line 338
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_a
    sget-object v6, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-ne p1, v6, :cond_8

    .line 340
    new-instance v4, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    invoke-direct {v4, p2}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 341
    .local v4, "respMsg":Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getInitiatingMessageId()I

    move-result v6

    const/16 v8, 0x4c

    if-ne v6, v8, :cond_8

    .line 342
    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->getResponseCode()Lcom/dsi/ant/message/ResponseCode;

    move-result-object v6

    sget-object v8, Lcom/dsi/ant/message/ResponseCode;->RESPONSE_NO_ERROR:Lcom/dsi/ant/message/ResponseCode;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-ne v6, v8, :cond_8

    .line 348
    :try_start_4
    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;
    :try_end_4
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v5

    .line 355
    .restart local v5    # "state":Lcom/dsi/ant/message/ChannelState;
    :try_start_5
    sget-object v6, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v5, v6, :cond_b

    sget-object v6, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    if-ne v5, v6, :cond_c

    .line 357
    :cond_b
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->mIsClosing:Z

    .line 359
    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->isClosingSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v6}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v6

    if-nez v6, :cond_8

    .line 360
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v8, "AntChannelTask could not acquire closing semaphore after non-locked close call"

    invoke-direct {v6, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 349
    .end local v5    # "state":Lcom/dsi/ant/message/ChannelState;
    :catch_1
    move-exception v1

    .line 351
    .restart local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v6, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "AntCommandFailedException in close check requestStatus call: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 353
    new-instance v6, Landroid/os/RemoteException;

    invoke-direct {v6}, Landroid/os/RemoteException;-><init>()V

    throw v6

    .line 364
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    .restart local v5    # "state":Lcom/dsi/ant/message/ChannelState;
    :cond_c
    sget-object v6, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    const-string v8, "Ignoring out of order close command response"

    invoke-static {v6, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 294
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public runSubTask(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V
    .locals 4
    .param p1, "task"    # Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 161
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->doWorkThread:Ljava/lang/Thread;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 163
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "runSubTask() called on thread other than doWork() thread"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 167
    :cond_0
    move-object v0, p1

    .line 168
    .local v0, "nextTask":Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    .line 181
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 182
    return-void

    .line 170
    :cond_2
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v0, v1, p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->initAsSubTask(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V

    .line 172
    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 173
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Begin doWork() in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v3}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskNameInternal()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->doWorkInternal()V

    .line 175
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "End doWork() in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v3}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getTaskNameInternal()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const/4 v0, 0x0

    .line 178
    iget-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTaskCancelling:Z

    if-nez v1, :cond_1

    .line 179
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->subTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->getNextTaskToExecute()Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    move-result-object v0

    goto :goto_0
.end method

.method public setNextTask(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;)V
    .locals 0
    .param p1, "nextTask"    # Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;->nextTask:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;

    .line 146
    return-void
.end method
