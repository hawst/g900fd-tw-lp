.class Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;
.super Ljava/lang/Object;
.source "BackgroundScanController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

.field private final synthetic val$service:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    iput-object p2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->val$service:Landroid/os/IBinder;

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 157
    const/4 v2, 0x0

    .line 161
    .local v2, "startedScan":Z
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->connLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 163
    :try_start_1
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->serviceWasStopped:Z
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$1(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 164
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 201
    if-nez v2, :cond_0

    .line 203
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopBackgroundScan()V

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    :try_start_2
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    new-instance v5, Lcom/dsi/ant/AntService;

    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->val$service:Landroid/os/IBinder;

    invoke-direct {v5, v6}, Lcom/dsi/ant/AntService;-><init>(Landroid/os/IBinder;)V

    invoke-static {v3, v5}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$2(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;Lcom/dsi/ant/AntService;)V

    .line 168
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    iget-object v5, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v5}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v5

    # invokes: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->getChannel()Lcom/dsi/ant/channel/AntChannel;
    invoke-static {v5}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$3(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$4(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;Lcom/dsi/ant/channel/AntChannel;)V

    .line 170
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->channel:Lcom/dsi/ant/channel/AntChannel;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$5(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 172
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    new-instance v5, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v6}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v6

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->channel:Lcom/dsi/ant/channel/AntChannel;
    invoke-static {v6}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$5(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v6

    new-instance v7, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1$1;

    invoke-direct {v7, p0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1$1;-><init>(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;)V

    invoke-direct {v5, v6, v7}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor$IDeathHandler;)V

    invoke-static {v3, v5}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$6(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)V

    .line 185
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;

    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->clientResultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$7(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;-><init>(Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;)V

    .line 186
    .local v0, "backgroundScanTask":Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->executor:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->access$8(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    move-result-object v3

    const/16 v5, 0x3e8

    invoke-virtual {v3, v0, v5}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->startTask(Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;I)Z

    .line 187
    const/4 v2, 0x1

    .line 161
    .end local v0    # "backgroundScanTask":Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;
    :cond_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 201
    if-nez v2, :cond_0

    .line 203
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopBackgroundScan()V

    goto :goto_0

    .line 161
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v3
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 191
    :catch_0
    move-exception v1

    .line 193
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_5
    const-string v3, "BackgroundScanController"

    const-string v4, "RemoteException acquiring channel from ARS"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 201
    if-nez v2, :cond_0

    .line 203
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopBackgroundScan()V

    goto/16 :goto_0

    .line 195
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 197
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_6
    const-string v3, "BackgroundScanController"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "InterrupedException: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 201
    if-nez v2, :cond_0

    .line 203
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopBackgroundScan()V

    goto/16 :goto_0

    .line 200
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v3

    .line 201
    if-nez v2, :cond_3

    .line 203
    iget-object v4, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1$1;->this$1:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    # getter for: Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
    invoke-static {v4}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;->access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;)Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopBackgroundScan()V

    .line 205
    :cond_3
    throw v3
.end method
