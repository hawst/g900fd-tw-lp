.class Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$1;
.super Ljava/lang/Object;
.source "AlreadyConnectedDeviceRequestor.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;-><init>(Landroid/os/Looper;Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$IAlreadyConnectedDeviceListReceiver;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;


# direct methods
.method constructor <init>(Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 9
    .param p1, "result"    # Landroid/os/Message;

    .prologue
    .line 54
    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;

    iget-object v7, v6, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->msgRcvLock:Ljava/lang/Object;

    monitor-enter v7

    .line 56
    :try_start_0
    iget v6, p1, Landroid/os/Message;->what:I

    if-nez v6, :cond_0

    .line 57
    iget v6, p1, Landroid/os/Message;->arg1:I

    iget-object v8, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;

    iget v8, v8, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->lastCmdSeqNum:I

    if-ne v6, v8, :cond_0

    .line 59
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    .line 60
    .local v4, "retData":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 61
    const-string v6, "DevType_int"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 62
    .local v1, "devType":I
    const-string v6, "DevDbInfoList_parcelableArray"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v5

    .line 63
    .local v5, "uncastDeviceInfo":[Landroid/os/Parcelable;
    array-length v6, v5

    new-array v3, v6, [Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 64
    .local v3, "listDeviceInfo":[Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v5

    if-lt v2, v6, :cond_1

    .line 67
    array-length v6, v3

    new-array v0, v6, [I

    .line 68
    .local v0, "devNumbers":[I
    const/4 v2, 0x0

    :goto_1
    array-length v6, v3

    if-lt v2, v6, :cond_2

    .line 71
    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;

    iget-object v6, v6, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->listReceiver:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$IAlreadyConnectedDeviceListReceiver;

    invoke-static {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    move-result-object v8

    invoke-interface {v6, v8, v0}, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$IAlreadyConnectedDeviceListReceiver;->onNewListReceived(Lcom/dsi/ant/backgroundscan/antplus/DeviceType;[I)V

    .line 73
    .end local v0    # "devNumbers":[I
    .end local v1    # "devType":I
    .end local v2    # "i":I
    .end local v3    # "listDeviceInfo":[Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .end local v4    # "retData":Landroid/os/Bundle;
    .end local v5    # "uncastDeviceInfo":[Landroid/os/Parcelable;
    :cond_0
    monitor-exit v7

    const/4 v6, 0x1

    return v6

    .line 65
    .restart local v1    # "devType":I
    .restart local v2    # "i":I
    .restart local v3    # "listDeviceInfo":[Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .restart local v4    # "retData":Landroid/os/Bundle;
    .restart local v5    # "uncastDeviceInfo":[Landroid/os/Parcelable;
    :cond_1
    aget-object v6, v5, v2

    check-cast v6, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    aput-object v6, v3, v2

    .line 64
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 69
    .restart local v0    # "devNumbers":[I
    :cond_2
    aget-object v6, v3, v2

    iget-object v6, v6, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aput v6, v0, v2

    .line 68
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 54
    .end local v0    # "devNumbers":[I
    .end local v1    # "devType":I
    .end local v2    # "i":I
    .end local v3    # "listDeviceInfo":[Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .end local v4    # "retData":Landroid/os/Bundle;
    .end local v5    # "uncastDeviceInfo":[Landroid/os/Parcelable;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method
