.class public interface abstract Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;
.super Ljava/lang/Object;
.source "IBackgroundScanResultReceiver.java"


# virtual methods
.method public abstract onAntFsLinkBeacon(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;I)V
.end method

.method public abstract onAntPlusDataMessage(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;)V
.end method

.method public abstract onCommonManufacturerInfo(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;II)V
.end method

.method public abstract onCommonProductInfo(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;JII)V
.end method

.method public abstract onLegacyManufacturerInfo(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;I)V
.end method

.method public abstract onScanStopped(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V
.end method
