.class Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;
.super Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;
.source "BackgroundScanTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$dsi$ant$backgroundscan$antplus$DeviceType:[I

.field private static synthetic $SWITCH_TABLE$com$dsi$ant$message$fromant$MessageFromAntType:[I


# instance fields
.field cancelled:Z

.field private checker:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;

.field private handler:Landroid/os/Handler;

.field private resultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

.field private scanFinished:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method static synthetic $SWITCH_TABLE$com$dsi$ant$backgroundscan$antplus$DeviceType()[I
    .locals 3

    .prologue
    .line 25
    sget-object v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->$SWITCH_TABLE$com$dsi$ant$backgroundscan$antplus$DeviceType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->values()[Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ACTIVITY_MONITOR:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_d

    :goto_1
    :try_start_1
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_CADENCE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_c

    :goto_2
    :try_start_2
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_POWER:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_b

    :goto_3
    :try_start_3
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_SPD_CAD_COMBINED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_a

    :goto_4
    :try_start_4
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_SPEED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_9

    :goto_5
    :try_start_5
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BLOOD_PRESSURE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_8

    :goto_6
    :try_start_6
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->CONTROLS:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_7

    :goto_7
    :try_start_7
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ENVIRONMENT:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_6

    :goto_8
    :try_start_8
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->GEOCACHE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_5

    :goto_9
    :try_start_9
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->HEART_RATE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_4

    :goto_a
    :try_start_a
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->LIGHT_ELECTRIC_VEHICLE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_3

    :goto_b
    :try_start_b
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->STRIDE_BASED_SPEED_DISTANCE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_2

    :goto_c
    :try_start_c
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->UNRECOGNIZED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1

    :goto_d
    :try_start_d
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->WEIGHT:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-virtual {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_0

    :goto_e
    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->$SWITCH_TABLE$com$dsi$ant$backgroundscan$antplus$DeviceType:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_e

    :catch_1
    move-exception v1

    goto :goto_d

    :catch_2
    move-exception v1

    goto :goto_c

    :catch_3
    move-exception v1

    goto :goto_b

    :catch_4
    move-exception v1

    goto :goto_a

    :catch_5
    move-exception v1

    goto :goto_9

    :catch_6
    move-exception v1

    goto :goto_8

    :catch_7
    move-exception v1

    goto :goto_7

    :catch_8
    move-exception v1

    goto :goto_6

    :catch_9
    move-exception v1

    goto :goto_5

    :catch_a
    move-exception v1

    goto :goto_4

    :catch_b
    move-exception v1

    goto/16 :goto_3

    :catch_c
    move-exception v1

    goto/16 :goto_2

    :catch_d
    move-exception v1

    goto/16 :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$dsi$ant$message$fromant$MessageFromAntType()[I
    .locals 3

    .prologue
    .line 25
    sget-object v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->$SWITCH_TABLE$com$dsi$ant$message$fromant$MessageFromAntType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->values()[Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_1
    :try_start_1
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ANT_VERSION:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    :goto_2
    :try_start_2
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BROADCAST_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_3
    :try_start_3
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    :goto_4
    :try_start_4
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CAPABILITIES:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_5
    :try_start_5
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_6
    :try_start_6
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_ID:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_7
    :try_start_7
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_8
    :try_start_8
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_STATUS:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_9
    :try_start_9
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->OTHER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_a
    :try_start_a
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->SERIAL_NUMBER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_b
    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->$SWITCH_TABLE$com$dsi$ant$message$fromant$MessageFromAntType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_b

    :catch_1
    move-exception v1

    goto :goto_a

    :catch_2
    move-exception v1

    goto :goto_9

    :catch_3
    move-exception v1

    goto :goto_8

    :catch_4
    move-exception v1

    goto :goto_7

    :catch_5
    move-exception v1

    goto :goto_6

    :catch_6
    move-exception v1

    goto :goto_5

    :catch_7
    move-exception v1

    goto :goto_4

    :catch_8
    move-exception v1

    goto :goto_3

    :catch_9
    move-exception v1

    goto :goto_2

    :catch_a
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;)V
    .locals 3
    .param p1, "resultReceiver"    # Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelTask;-><init>()V

    .line 142
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->resultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    .line 143
    new-instance v1, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;

    invoke-direct {v1}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->checker:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;

    .line 145
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "BackgroundScanTask Message Handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 146
    .local v0, "newHandlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 147
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->handler:Landroid/os/Handler;

    .line 148
    return-void
.end method

.method private getUniqueId(Lcom/dsi/ant/message/ChannelId;)J
    .locals 5
    .param p1, "id"    # Lcom/dsi/ant/message/ChannelId;

    .prologue
    .line 127
    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x10

    shl-long/2addr v0, v2

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-virtual {p1}, Lcom/dsi/ant/message/ChannelId;->getTransmissionType()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public doWork()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 242
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->getCapabilities()Lcom/dsi/ant/channel/Capabilities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/channel/Capabilities;->hasEventBuffering()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 247
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->disableEventBuffer()V

    .line 250
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->handler:Landroid/os/Handler;

    new-instance v3, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$1;

    invoke-direct {v3, p0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$1;-><init>(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;)V

    .line 273
    const-wide/16 v4, 0x4e20

    .line 250
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    :cond_0
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannel;->open()V

    .line 284
    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->enableMessageProcessing()V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_2

    .line 288
    :try_start_2
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->scanFinished:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 305
    :goto_1
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->handler:Landroid/os/Handler;

    if-eqz v2, :cond_1

    .line 307
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->handler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 308
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->quit()V

    .line 311
    :cond_1
    iget-boolean v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->cancelled:Z

    if-eqz v2, :cond_2

    .line 313
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->resultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->USER_CANCELLED:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-interface {v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;->onScanStopped(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    .line 315
    :cond_2
    return-void

    .line 274
    :catch_0
    move-exception v1

    .line 277
    .local v1, "e1":Lcom/dsi/ant/channel/AntCommandFailedException;
    const-string v2, "BackgroundScanTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EventBuffering disable() failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 290
    .end local v1    # "e1":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/InterruptedException;
    const v2, 0x7fffffff

    :try_start_3
    invoke-virtual {p0, v2}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->handleInterruptRequest(I)Z

    .line 294
    const-string v2, "BackgroundScanTask"

    const-string v3, "Interrupted waiting for close"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 298
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v0

    .line 301
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    const-string v2, "BackgroundScanTask"

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->resultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->CHANNEL_NOT_AVAILABLE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-interface {v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;->onScanStopped(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    goto :goto_1
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    const-string v0, "Background Scan Task"

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 326
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->handleInterruptRequest(I)Z

    .line 327
    return-void
.end method

.method public handleInterruptRequest(I)Z
    .locals 3
    .param p1, "interruptingTaskRank"    # I

    .prologue
    const/4 v2, 0x1

    .line 332
    iput-boolean v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->cancelled:Z

    .line 333
    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->disableMessageProcessing()V

    .line 334
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->scanFinished:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->scanFinished:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 338
    :goto_0
    return v2

    .line 337
    :cond_0
    const-string v0, "BackgroundScanTask"

    const-string v1, "handleInterruptRequest called before initTask()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public initTask()V
    .locals 2

    .prologue
    .line 235
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->cancelled:Z

    .line 236
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->scanFinished:Ljava/util/concurrent/CountDownLatch;

    .line 237
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 27
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 153
    invoke-static {}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->$SWITCH_TABLE$com$dsi$ant$message$fromant$MessageFromAntType()[I

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 230
    :goto_0
    return-void

    .line 156
    :pswitch_0
    new-instance v26, Lcom/dsi/ant/message/ExtendedData;

    invoke-static/range {p2 .. p2}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/message/fromant/DataMessage;

    move-object/from16 v0, v26

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/ExtendedData;-><init>(Lcom/dsi/ant/message/fromant/DataMessage;)V

    .line 157
    .local v26, "extData":Lcom/dsi/ant/message/ExtendedData;
    invoke-virtual/range {v26 .. v26}, Lcom/dsi/ant/message/ExtendedData;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v4

    .line 158
    .local v4, "channelId":Lcom/dsi/ant/message/ChannelId;
    invoke-virtual {v4}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    move-result-object v25

    .line 159
    .local v25, "deviceType":Lcom/dsi/ant/backgroundscan/antplus/DeviceType;
    invoke-virtual/range {v26 .. v26}, Lcom/dsi/ant/message/ExtendedData;->getRssi()Lcom/dsi/ant/message/Rssi;

    move-result-object v5

    .line 161
    .local v5, "rssi":Lcom/dsi/ant/message/Rssi;
    invoke-static {}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->$SWITCH_TABLE$com$dsi$ant$backgroundscan$antplus$DeviceType()[I

    move-result-object v1

    invoke-virtual/range {v25 .. v25}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 174
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    const/16 v2, 0x50

    if-ne v1, v2, :cond_0

    .line 176
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;->getValueFromInt(I)Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;

    move-result-object v6

    .line 177
    .local v6, "manufacturer":Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v1, v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v7

    .line 178
    .local v7, "modelNumber":I
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x4

    aget-byte v1, v1, v2

    invoke-static {v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v8

    .line 179
    .local v8, "hardwareRevision":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->resultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->getUniqueId(Lcom/dsi/ant/message/ChannelId;)J

    move-result-wide v2

    invoke-interface/range {v1 .. v8}, Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;->onCommonManufacturerInfo(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;II)V

    goto :goto_0

    .line 182
    .end local v6    # "manufacturer":Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;
    .end local v7    # "modelNumber":I
    .end local v8    # "hardwareRevision":I
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    const/16 v2, 0x51

    if-ne v1, v2, :cond_1

    .line 184
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    .line 185
    .local v24, "serialNumber":Ljava/lang/Long;
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x4

    aget-byte v1, v1, v2

    invoke-static {v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v16

    .line 186
    .local v16, "mainSoftwareRevision":I
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x3

    aget-byte v1, v1, v2

    invoke-static {v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v17

    .line 187
    .local v17, "supplementalSoftwareRevision":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->resultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->getUniqueId(Lcom/dsi/ant/message/ChannelId;)J

    move-result-wide v10

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    move-object v12, v4

    move-object v13, v5

    invoke-interface/range {v9 .. v17}, Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;->onCommonProductInfo(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;JII)V

    goto/16 :goto_0

    .line 190
    .end local v16    # "mainSoftwareRevision":I
    .end local v17    # "supplementalSoftwareRevision":I
    .end local v24    # "serialNumber":Ljava/lang/Long;
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    const/16 v2, 0x43

    if-ne v1, v2, :cond_2

    .line 191
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x3

    aget-byte v1, v1, v2

    and-int/lit8 v1, v1, 0xf

    if-nez v1, :cond_2

    .line 193
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v1, v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v1

    and-int/lit16 v1, v1, 0x7fff

    invoke-static {v1}, Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;->getValueFromInt(I)Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;

    move-result-object v6

    .line 194
    .restart local v6    # "manufacturer":Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v15

    .line 195
    .local v15, "linkBeaconDeviceType":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->resultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->getUniqueId(Lcom/dsi/ant/message/ChannelId;)J

    move-result-wide v10

    move-object v12, v4

    move-object v13, v5

    move-object v14, v6

    invoke-interface/range {v9 .. v15}, Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;->onAntFsLinkBeacon(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;I)V

    goto/16 :goto_0

    .line 199
    .end local v6    # "manufacturer":Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;
    .end local v15    # "linkBeaconDeviceType":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->resultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->getUniqueId(Lcom/dsi/ant/message/ChannelId;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;->onAntPlusDataMessage(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;)V

    goto/16 :goto_0

    .line 207
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->checker:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;

    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    move-object/from16 v0, v26

    invoke-virtual {v1, v0, v2}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$DataPageChecker;->isDataPagesSupported(Lcom/dsi/ant/message/ExtendedData;B)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 208
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    and-int/lit8 v1, v1, 0x7f

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 210
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x2

    aget-byte v1, v1, v2

    invoke-static {v1}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    invoke-static {v1}, Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;->getValueFromInt(I)Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;

    move-result-object v6

    .line 211
    .restart local v6    # "manufacturer":Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;
    invoke-virtual/range {p2 .. p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v24

    .line 212
    .local v24, "serialNumber":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->resultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->getUniqueId(Lcom/dsi/ant/message/ChannelId;)J

    move-result-wide v19

    move-object/from16 v21, v4

    move-object/from16 v22, v5

    move-object/from16 v23, v6

    invoke-interface/range {v18 .. v24}, Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;->onLegacyManufacturerInfo(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;I)V

    goto/16 :goto_0

    .line 216
    .end local v6    # "manufacturer":Lcom/dsi/ant/backgroundscan/antplus/Manufacturer;
    .end local v24    # "serialNumber":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->resultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->getUniqueId(Lcom/dsi/ant/message/ChannelId;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;->onAntPlusDataMessage(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;)V

    goto/16 :goto_0

    .line 222
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->resultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->getUniqueId(Lcom/dsi/ant/message/ChannelId;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;->onAntPlusDataMessage(JLcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/message/Rssi;)V

    goto/16 :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 161
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
