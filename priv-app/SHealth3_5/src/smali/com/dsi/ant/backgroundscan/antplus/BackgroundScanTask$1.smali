.class Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$1;
.super Ljava/lang/Object;
.source "BackgroundScanTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->doWork()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;


# direct methods
.method constructor <init>(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;

    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 257
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;

    iget-boolean v2, v2, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->cancelled:Z

    if-nez v2, :cond_0

    .line 259
    new-instance v1, Lcom/dsi/ant/channel/EventBufferSettings;

    invoke-direct {v1}, Lcom/dsi/ant/channel/EventBufferSettings;-><init>()V

    .line 260
    .local v1, "es":Lcom/dsi/ant/channel/EventBufferSettings;
    const/16 v2, 0x7d0

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/EventBufferSettings;->setEventBufferTime(I)V

    .line 261
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask$1;->this$0:Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;

    iget-object v2, v2, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanTask;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v2, v1}, Lcom/dsi/ant/channel/AntChannel;->setEventBuffer(Lcom/dsi/ant/channel/EventBufferSettings;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 272
    .end local v1    # "es":Lcom/dsi/ant/channel/EventBufferSettings;
    :cond_0
    :goto_0
    return-void

    .line 264
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "BackgroundScanTask"

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 268
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 270
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    const-string v2, "BackgroundScanTask"

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
