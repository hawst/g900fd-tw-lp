.class public final enum Lcom/dsi/ant/backgroundscan/antplus/DeviceType;
.super Ljava/lang/Enum;
.source "DeviceType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/backgroundscan/antplus/DeviceType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACTIVITY_MONITOR:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum BIKE_CADENCE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum BIKE_POWER:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum BIKE_SPD_CAD_COMBINED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum BIKE_SPEED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum BLOOD_PRESSURE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum CONTROLS:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field private static final synthetic ENUM$VALUES:[Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum ENVIRONMENT:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum GEOCACHE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum HEART_RATE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum LIGHT_ELECTRIC_VEHICLE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum STRIDE_BASED_SPEED_DISTANCE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum UNRECOGNIZED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

.field public static final enum WEIGHT:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;


# instance fields
.field private intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0xb

    .line 5
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "BIKE_POWER"

    invoke-direct {v0, v1, v5, v4}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_POWER:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 7
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "BLOOD_PRESSURE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v6, v2}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BLOOD_PRESSURE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 9
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "CONTROLS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->CONTROLS:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 11
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "GEOCACHE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v8, v2}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->GEOCACHE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 13
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "LIGHT_ELECTRIC_VEHICLE"

    const/4 v2, 0x4

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->LIGHT_ELECTRIC_VEHICLE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 15
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "ACTIVITY_MONITOR"

    const/4 v2, 0x5

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ACTIVITY_MONITOR:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 17
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "ENVIRONMENT"

    const/4 v2, 0x6

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ENVIRONMENT:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 19
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "WEIGHT"

    const/4 v2, 0x7

    const/16 v3, 0x77

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->WEIGHT:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 21
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "HEART_RATE"

    const/16 v2, 0x8

    const/16 v3, 0x78

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->HEART_RATE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 23
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "BIKE_SPD_CAD_COMBINED"

    const/16 v2, 0x9

    const/16 v3, 0x79

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_SPD_CAD_COMBINED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 25
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "BIKE_CADENCE"

    const/16 v2, 0xa

    const/16 v3, 0x7a

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_CADENCE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 27
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "BIKE_SPEED"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v4, v2}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_SPEED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 29
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "STRIDE_BASED_SPEED_DISTANCE"

    const/16 v2, 0xc

    const/16 v3, 0x7c

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->STRIDE_BASED_SPEED_DISTANCE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 31
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    const-string v1, "UNRECOGNIZED"

    const/16 v2, 0xd

    .line 34
    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->UNRECOGNIZED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 3
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_POWER:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BLOOD_PRESSURE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->CONTROLS:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->GEOCACHE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->LIGHT_ELECTRIC_VEHICLE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ACTIVITY_MONITOR:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ENVIRONMENT:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->WEIGHT:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->HEART_RATE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_SPD_CAD_COMBINED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_CADENCE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->BIKE_SPEED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v1, v0, v4

    const/16 v1, 0xc

    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->STRIDE_BASED_SPEED_DISTANCE:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->UNRECOGNIZED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ENUM$VALUES:[Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput p3, p0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->intValue:I

    .line 41
    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/backgroundscan/antplus/DeviceType;
    .locals 6
    .param p0, "intValue"    # I

    .prologue
    .line 50
    and-int/lit16 v2, p0, 0x80

    if-eqz v2, :cond_0

    .line 51
    and-int/lit8 p0, p0, 0x7f

    .line 53
    :cond_0
    invoke-static {}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->values()[Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_2

    .line 59
    sget-object v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->UNRECOGNIZED:Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    .line 60
    .local v1, "unrecognized":Lcom/dsi/ant/backgroundscan/antplus/DeviceType;
    iput p0, v1, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->intValue:I

    move-object v0, v1

    .line 62
    .end local v1    # "unrecognized":Lcom/dsi/ant/backgroundscan/antplus/DeviceType;
    :cond_1
    return-object v0

    .line 53
    :cond_2
    aget-object v0, v3, v2

    .line 55
    .local v0, "type":Lcom/dsi/ant/backgroundscan/antplus/DeviceType;
    invoke-virtual {v0}, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->getIntValue()I

    move-result v5

    if-eq v5, p0, :cond_1

    .line 53
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/backgroundscan/antplus/DeviceType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/backgroundscan/antplus/DeviceType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->ENUM$VALUES:[Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    array-length v1, v0

    new-array v2, v1, [Lcom/dsi/ant/backgroundscan/antplus/DeviceType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/dsi/ant/backgroundscan/antplus/DeviceType;->intValue:I

    return v0
.end method
