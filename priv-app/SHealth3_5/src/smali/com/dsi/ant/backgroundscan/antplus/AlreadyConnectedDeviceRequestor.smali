.class public Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;
.super Ljava/lang/Object;
.source "AlreadyConnectedDeviceRequestor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$IAlreadyConnectedDeviceListReceiver;
    }
.end annotation


# instance fields
.field lastCmdSeqNum:I

.field listReceiver:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$IAlreadyConnectedDeviceListReceiver;

.field msgRcvLock:Ljava/lang/Object;

.field resultReceiverMsgr:Landroid/os/Messenger;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$IAlreadyConnectedDeviceListReceiver;)V
    .locals 3
    .param p1, "resultProcessingLooper"    # Landroid/os/Looper;
    .param p2, "listReceiver"    # Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$IAlreadyConnectedDeviceListReceiver;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->lastCmdSeqNum:I

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->msgRcvLock:Ljava/lang/Object;

    .line 45
    iput-object p2, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->listReceiver:Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$IAlreadyConnectedDeviceListReceiver;

    .line 46
    new-instance v0, Landroid/os/Messenger;

    .line 47
    new-instance v1, Landroid/os/Handler;

    .line 49
    new-instance v2, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$1;

    invoke-direct {v2, p0}, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor$1;-><init>(Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;)V

    .line 47
    invoke-direct {v1, p1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    .line 46
    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->resultReceiverMsgr:Landroid/os/Messenger;

    .line 77
    return-void
.end method


# virtual methods
.method public broadcastAlreadyConnectedDeviceListRequest(Landroid/content/Context;)V
    .locals 5
    .param p1, "contextToSendBroadcastRequest"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 88
    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->msgRcvLock:Ljava/lang/Object;

    monitor-enter v3

    .line 90
    :try_start_0
    iget v2, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->lastCmdSeqNum:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->lastCmdSeqNum:I

    .line 88
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.dsi.ant.plugins.antplus.queryalreadyconnecteddevices"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 94
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 95
    .local v1, "reqData":Landroid/os/Bundle;
    const-string v2, "Version_int"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 96
    const-string v2, "CmdSeqNum_int"

    iget v3, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->lastCmdSeqNum:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 97
    const-string v2, "Mode_int"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 98
    const-string v2, "DevType_int"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 99
    const-string v2, "ResultMsgr_messenger"

    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/AlreadyConnectedDeviceRequestor;->resultReceiverMsgr:Landroid/os/Messenger;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 100
    const-string v2, "com.dsi.ant.plugins.antplus.queryalreadyconnecteddevices.params"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 101
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 102
    return-void

    .line 88
    .end local v0    # "it":Landroid/content/Intent;
    .end local v1    # "reqData":Landroid/os/Bundle;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method
