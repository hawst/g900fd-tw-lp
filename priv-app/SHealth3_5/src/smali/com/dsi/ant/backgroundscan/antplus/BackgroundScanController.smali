.class public Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;
.super Ljava/lang/Object;
.source "BackgroundScanController.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$dsi$ant$channel$ChannelNotAvailableReason:[I


# instance fields
.field private channel:Lcom/dsi/ant/channel/AntChannel;

.field private clientResultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

.field private final connLock:Ljava/lang/Object;

.field private context:Landroid/content/Context;

.field private executor:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

.field private mArsComm:Lcom/dsi/ant/AntService;

.field mArsConnection:Landroid/content/ServiceConnection;

.field private mServiceBound:Z

.field private serviceWasStopped:Z

.field private stopRequested:Z


# direct methods
.method static synthetic $SWITCH_TABLE$com$dsi$ant$channel$ChannelNotAvailableReason()[I
    .locals 3

    .prologue
    .line 27
    sget-object v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->$SWITCH_TABLE$com$dsi$ant$channel$ChannelNotAvailableReason:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->values()[Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ALL_CHANNELS_IN_USE:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_1
    :try_start_1
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ALL_CHANNELS_IN_USE_LEGACY:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_2
    :try_start_2
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ANT_DISABLED_AIRPLANE_MODE_ON:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_3
    :try_start_3
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NETWORK_NOT_AVAILABLE:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_4
    :try_start_4
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NO_ADAPTERS_EXIST:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_5
    :try_start_5
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->NO_CHANNELS_MATCH_CRITERIA:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_6
    :try_start_6
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->RELEASE_PROCESSING:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_7
    :try_start_7
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->SERVICE_INITIALIZING:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_8
    :try_start_8
    sget-object v1, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->UNKNOWN:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_9
    sput-object v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->$SWITCH_TABLE$com$dsi$ant$channel$ChannelNotAvailableReason:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->connLock:Ljava/lang/Object;

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->serviceWasStopped:Z

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mServiceBound:Z

    .line 147
    new-instance v0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController$1;-><init>(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)V

    iput-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mArsConnection:Landroid/content/ServiceConnection;

    .line 45
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->context:Landroid/content/Context;

    .line 46
    return-void
.end method

.method static synthetic access$0(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->connLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->serviceWasStopped:Z

    return v0
.end method

.method static synthetic access$2(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;Lcom/dsi/ant/AntService;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mArsComm:Lcom/dsi/ant/AntService;

    return-void
.end method

.method static synthetic access$3(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Lcom/dsi/ant/channel/AntChannel;
    .locals 1

    .prologue
    .line 230
    invoke-direct {p0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->getChannel()Lcom/dsi/ant/channel/AntChannel;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;Lcom/dsi/ant/channel/AntChannel;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->channel:Lcom/dsi/ant/channel/AntChannel;

    return-void
.end method

.method static synthetic access$5(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Lcom/dsi/ant/channel/AntChannel;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->channel:Lcom/dsi/ant/channel/AntChannel;

    return-object v0
.end method

.method static synthetic access$6(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->executor:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    return-void
.end method

.method static synthetic access$7(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->clientResultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    return-object v0
.end method

.method static synthetic access$8(Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;)Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->executor:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    return-object v0
.end method

.method private acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;
    .locals 10
    .param p1, "network"    # Lcom/dsi/ant/channel/PredefinedNetwork;
    .param p2, "requiredCaps"    # Lcom/dsi/ant/channel/Capabilities;
    .param p3, "optCaps"    # Lcom/dsi/ant/channel/Capabilities;

    .prologue
    const/4 v5, 0x0

    .line 279
    const/4 v0, 0x0

    .line 280
    .local v0, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    .line 285
    .local v3, "startTime_ms":J
    :goto_0
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v3

    const-wide/16 v8, 0x3a98

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    .line 287
    const-string v6, "BackgroundScanController"

    const-string v7, "Acquire channel stuck in acquire loop for 15s, aborting."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->OTHER_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {p0, v6}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    .line 342
    :goto_1
    return-object v5

    .line 292
    :cond_0
    iget-boolean v6, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopRequested:Z

    if-eqz v6, :cond_1

    .line 294
    const-string v6, "BackgroundScanController"

    const-string v7, "BackgroundScan stopping, aborting acquire channel."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->USER_CANCELLED:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {p0, v6}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 337
    :catch_0
    move-exception v1

    .line 340
    .local v1, "e":Landroid/os/RemoteException;
    const-string v6, "BackgroundScanController"

    const-string v7, "RemoteException acquiring channel from ARS"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->OTHER_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {p0, v6}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    goto :goto_1

    .line 303
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mArsComm:Lcom/dsi/ant/AntService;

    invoke-virtual {v6}, Lcom/dsi/ant/AntService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProvider;

    move-result-object v6

    iget-object v7, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->context:Landroid/content/Context;

    invoke-virtual {v6, v7, p1, p2, p3}, Lcom/dsi/ant/channel/AntChannelProvider;->acquireChannel(Landroid/content/Context;Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/dsi/ant/channel/ChannelNotAvailableException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    move-object v5, v0

    .line 312
    goto :goto_1

    .line 305
    :catch_1
    move-exception v1

    .line 307
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_2
    const-string v6, "BackgroundScanController"

    const-string v7, "UnexpectedError, acquireChannel_helper called with null ArsComm"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->OTHER_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {p0, v6}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V
    :try_end_2
    .catch Lcom/dsi/ant/channel/ChannelNotAvailableException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 313
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v1

    .line 315
    .local v1, "e":Lcom/dsi/ant/channel/ChannelNotAvailableException;
    :try_start_3
    invoke-static {}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->$SWITCH_TABLE$com$dsi$ant$channel$ChannelNotAvailableReason()[I

    move-result-object v6

    iget-object v7, v1, Lcom/dsi/ant/channel/ChannelNotAvailableException;->reasonCode:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/ChannelNotAvailableReason;->ordinal()I

    move-result v7

    aget v6, v6, v7

    sparse-switch v6, :sswitch_data_0

    .line 330
    const-string v6, "BackgroundScanController"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Could not acquire channel: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v1, Lcom/dsi/ant/channel/ChannelNotAvailableException;->reasonCode:Lcom/dsi/ant/channel/ChannelNotAvailableReason;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    sget-object v6, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->CHANNEL_NOT_AVAILABLE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {p0, v6}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 322
    :sswitch_0
    const-wide/16 v6, 0xc8

    :try_start_4
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 323
    :catch_3
    move-exception v2

    .line 325
    .local v2, "e2":Ljava/lang/InterruptedException;
    :try_start_5
    const-string v6, "BackgroundScanController"

    const-string v7, "Sleep interupted attempting to acquire not yet ready channel, trying again"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0

    .line 315
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_0
    .end sparse-switch
.end method

.method private connectToArsAndGetChannel()V
    .locals 3

    .prologue
    .line 127
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->connLock:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->serviceWasStopped:Z

    .line 131
    iget-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mServiceBound:Z

    if-nez v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mArsConnection:Landroid/content/ServiceConnection;

    invoke-static {v0, v2}, Lcom/dsi/ant/AntService;->bindService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mServiceBound:Z

    .line 136
    const-string v0, "BackgroundScanController"

    const-string v2, "Bind() returned false"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->OTHER_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    .line 138
    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopBackgroundScan()V

    .line 139
    monitor-exit v1

    .line 144
    :goto_0
    return-void

    .line 141
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mServiceBound:Z

    .line 127
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getChannel()Lcom/dsi/ant/channel/AntChannel;
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 232
    new-instance v5, Lcom/dsi/ant/channel/Capabilities;

    invoke-direct {v5}, Lcom/dsi/ant/channel/Capabilities;-><init>()V

    .line 233
    .local v5, "reqCapabilities":Lcom/dsi/ant/channel/Capabilities;
    new-instance v4, Lcom/dsi/ant/channel/Capabilities;

    invoke-direct {v4}, Lcom/dsi/ant/channel/Capabilities;-><init>()V

    .line 234
    .local v4, "optCapabilities":Lcom/dsi/ant/channel/Capabilities;
    invoke-virtual {v5, v8}, Lcom/dsi/ant/channel/Capabilities;->supportBackgroundScanning(Z)V

    .line 235
    invoke-virtual {v4, v8}, Lcom/dsi/ant/channel/Capabilities;->supportRssi(Z)V

    .line 236
    sget-object v8, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-direct {p0, v8, v5, v4}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Lcom/dsi/ant/channel/Capabilities;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v6

    .line 238
    .local v6, "setupChannel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v6, :cond_0

    move-object v6, v7

    .line 273
    .end local v6    # "setupChannel":Lcom/dsi/ant/channel/AntChannel;
    :goto_0
    return-object v6

    .line 244
    .restart local v6    # "setupChannel":Lcom/dsi/ant/channel/AntChannel;
    :cond_0
    :try_start_0
    new-instance v0, Lcom/dsi/ant/message/LibConfig;

    invoke-direct {v0}, Lcom/dsi/ant/message/LibConfig;-><init>()V

    .line 245
    .local v0, "adapterConfig":Lcom/dsi/ant/message/LibConfig;
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Lcom/dsi/ant/message/LibConfig;->setEnableChannelIdOutput(Z)V

    .line 246
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Lcom/dsi/ant/message/LibConfig;->setEnableRssiOutput(Z)V

    .line 247
    invoke-virtual {v6, v0}, Lcom/dsi/ant/channel/AntChannel;->setAdapterWideLibConfig(Lcom/dsi/ant/message/LibConfig;)V

    .line 249
    new-instance v3, Lcom/dsi/ant/message/ExtendedAssignment;

    invoke-direct {v3}, Lcom/dsi/ant/message/ExtendedAssignment;-><init>()V

    .line 250
    .local v3, "extendedAssign":Lcom/dsi/ant/message/ExtendedAssignment;
    invoke-virtual {v3}, Lcom/dsi/ant/message/ExtendedAssignment;->enableBackgroundScanning()V

    .line 251
    sget-object v8, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    invoke-virtual {v6, v8, v3}, Lcom/dsi/ant/channel/AntChannel;->assign(Lcom/dsi/ant/message/ChannelType;Lcom/dsi/ant/message/ExtendedAssignment;)V

    .line 254
    new-instance v1, Lcom/dsi/ant/message/ChannelId;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct {v1, v8, v9, v10}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    .line 255
    .local v1, "channelId":Lcom/dsi/ant/message/ChannelId;
    invoke-virtual {v6, v1}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 256
    const/16 v8, 0x39

    invoke-virtual {v6, v8}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 260
    .end local v0    # "adapterConfig":Lcom/dsi/ant/message/LibConfig;
    .end local v1    # "channelId":Lcom/dsi/ant/message/ChannelId;
    .end local v3    # "extendedAssign":Lcom/dsi/ant/message/ExtendedAssignment;
    :catch_0
    move-exception v2

    .line 263
    .local v2, "e":Landroid/os/RemoteException;
    const-string v8, "BackgroundScanController"

    const-string v9, "RemoteException acquiring channel from ARS"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    sget-object v8, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->OTHER_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {p0, v8}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    .line 265
    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->release()V

    move-object v6, v7

    .line 266
    goto :goto_0

    .line 268
    .end local v2    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v2

    .line 270
    .local v2, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    const-string v8, "BackgroundScanController"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AntCommandFailedException: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    sget-object v8, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->OTHER_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {p0, v8}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    .line 272
    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntChannel;->release()V

    move-object v6, v7

    .line 273
    goto :goto_0
.end method


# virtual methods
.method public reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V
    .locals 4
    .param p1, "reason"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    .prologue
    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->clientResultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    invoke-interface {v1, p1}, Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;->onScanStopped(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 87
    .local v0, "ex":Ljava/lang/NullPointerException;
    const-string v1, "BackgroundScanController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Reporting stopped on null client receiver, reason: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startBackgroundScan(Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;)V
    .locals 4
    .param p1, "resultReceiver"    # Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    .prologue
    .line 55
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->connLock:Ljava/lang/Object;

    monitor-enter v2

    .line 57
    if-nez p1, :cond_0

    .line 58
    :try_start_0
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v3, "startBackgroundScan() was passed a null resultReceiver"

    invoke-direct {v1, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 55
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 61
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/dsi/ant/AntService;->getVersionCode(Landroid/content/Context;)I

    move-result v0

    .line 62
    .local v0, "pluginVersionCode":I
    const v1, 0x9c4f

    if-ge v0, v1, :cond_1

    .line 64
    const-string v1, "BackgroundScanController"

    const-string v3, "Ant Radio Service not installed, or below version 4.0.15"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->DEPENDENCY_NOT_INSTALLED:Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->reportStoppedReason(Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;)V

    .line 66
    monitor-exit v2

    .line 78
    :goto_0
    return-void

    .line 69
    :cond_1
    iget-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->serviceWasStopped:Z

    if-nez v1, :cond_2

    .line 71
    invoke-virtual {p0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopBackgroundScan()V

    .line 74
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopRequested:Z

    .line 75
    iput-object p1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->clientResultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    .line 76
    invoke-direct {p0}, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->connectToArsAndGetChannel()V

    .line 55
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public stopBackgroundScan()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 96
    iput-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->stopRequested:Z

    .line 97
    iget-object v2, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->connLock:Ljava/lang/Object;

    monitor-enter v2

    .line 99
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->executor:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->executor:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 101
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->executor:Lcom/dsi/ant/backgroundscan/antplus/copiedclasses/AntChannelExecutor;

    .line 102
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->channel:Lcom/dsi/ant/channel/AntChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :try_start_1
    iget-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mServiceBound:Z

    if-eqz v1, :cond_1

    .line 107
    iget-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mArsConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    :cond_1
    :goto_0
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mServiceBound:Z

    .line 115
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->mArsComm:Lcom/dsi/ant/AntService;

    .line 117
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->serviceWasStopped:Z

    .line 118
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/backgroundscan/antplus/BackgroundScanController;->clientResultReceiver:Lcom/dsi/ant/backgroundscan/antplus/IBackgroundScanResultReceiver;

    .line 97
    monitor-exit v2

    .line 120
    return-void

    .line 109
    :catch_0
    move-exception v0

    .line 111
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v1, "BackgroundScanController"

    const-string v3, "Attempted to unbind from already unbound service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 97
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
