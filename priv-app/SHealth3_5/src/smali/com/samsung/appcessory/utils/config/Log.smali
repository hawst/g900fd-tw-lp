.class public Lcom/samsung/appcessory/utils/config/Log;
.super Ljava/lang/Object;
.source "Log.java"


# static fields
.field public static DBG:Z

.field public static ERR:Z

.field public static INF:Z

.field public static LOG_LEVEL:I

.field public static VRB:Z

.field public static WAR:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 12
    sput v4, Lcom/samsung/appcessory/utils/config/Log;->LOG_LEVEL:I

    .line 14
    sget v0, Lcom/samsung/appcessory/utils/config/Log;->LOG_LEVEL:I

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/samsung/appcessory/utils/config/Log;->ERR:Z

    .line 15
    sget v0, Lcom/samsung/appcessory/utils/config/Log;->LOG_LEVEL:I

    if-le v0, v1, :cond_1

    move v0, v1

    :goto_1
    sput-boolean v0, Lcom/samsung/appcessory/utils/config/Log;->WAR:Z

    .line 16
    sget v0, Lcom/samsung/appcessory/utils/config/Log;->LOG_LEVEL:I

    const/4 v3, 0x2

    if-le v0, v3, :cond_2

    move v0, v1

    :goto_2
    sput-boolean v0, Lcom/samsung/appcessory/utils/config/Log;->INF:Z

    .line 17
    sget v0, Lcom/samsung/appcessory/utils/config/Log;->LOG_LEVEL:I

    if-le v0, v4, :cond_3

    move v0, v1

    :goto_3
    sput-boolean v0, Lcom/samsung/appcessory/utils/config/Log;->DBG:Z

    .line 18
    sget v0, Lcom/samsung/appcessory/utils/config/Log;->LOG_LEVEL:I

    const/4 v3, 0x4

    if-le v0, v3, :cond_4

    :goto_4
    sput-boolean v1, Lcom/samsung/appcessory/utils/config/Log;->VRB:Z

    return-void

    :cond_0
    move v0, v2

    .line 14
    goto :goto_0

    :cond_1
    move v0, v2

    .line 15
    goto :goto_1

    :cond_2
    move v0, v2

    .line 16
    goto :goto_2

    :cond_3
    move v0, v2

    .line 17
    goto :goto_3

    :cond_4
    move v1, v2

    .line 18
    goto :goto_4
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "TAG"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 35
    sget-boolean v0, Lcom/samsung/appcessory/utils/config/Log;->DBG:Z

    if-eqz v0, :cond_0

    .line 36
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 38
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "TAG"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 28
    sget-boolean v0, Lcom/samsung/appcessory/utils/config/Log;->ERR:Z

    if-eqz v0, :cond_0

    .line 29
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 31
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "TAG"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 21
    sget-boolean v0, Lcom/samsung/appcessory/utils/config/Log;->INF:Z

    if-eqz v0, :cond_0

    .line 22
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 24
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "TAG"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 42
    sget-boolean v0, Lcom/samsung/appcessory/utils/config/Log;->VRB:Z

    if-eqz v0, :cond_0

    .line 43
    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 45
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "TAG"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 49
    sget-boolean v0, Lcom/samsung/appcessory/utils/config/Log;->WAR:Z

    if-eqz v0, :cond_0

    .line 50
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 52
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
