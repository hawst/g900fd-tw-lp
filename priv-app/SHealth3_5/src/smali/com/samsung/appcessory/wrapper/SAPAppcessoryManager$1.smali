.class Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$1;
.super Ljava/lang/Object;
.source "SAPAppcessoryManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 721
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onServiceConnected"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 723
    check-cast v0, Lcom/samsung/appcessory/server/SAPService$SAPBinder;

    .line 724
    .local v0, "binder":Lcom/samsung/appcessory/server/SAPService$SAPBinder;
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    .line 725
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    .line 726
    invoke-virtual {v0}, Lcom/samsung/appcessory/server/SAPService$SAPBinder;->getService()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$1(Lcom/samsung/appcessory/server/SAPService;)V

    .line 727
    new-instance v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;

    invoke-direct {v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;-><init>()V

    invoke-static {v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$2(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;)V

    .line 728
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v1

    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapEventListener:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$4()Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/appcessory/server/SAPService;->sapInit(Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;)Z

    .line 729
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 716
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$1(Lcom/samsung/appcessory/server/SAPService;)V

    .line 718
    return-void
.end method
