.class Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AccessoryListener;
.super Ljava/lang/Object;
.source "SAPAppcessoryManager.java"

# interfaces
.implements Lcom/samsung/appcessory/base/IAccessoryEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AccessoryListener"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccessoryAttached(JI)V
    .locals 0
    .param p1, "accId"    # J
    .param p3, "accType"    # I

    .prologue
    .line 280
    return-void
.end method

.method public onAccessoryAttached([JI)V
    .locals 0
    .param p1, "arg0"    # [J
    .param p2, "type"    # I

    .prologue
    .line 223
    return-void
.end method

.method public onAccessoryDetached(J)V
    .locals 0
    .param p1, "arg0"    # J

    .prologue
    .line 220
    return-void
.end method

.method public onAccessoryDetached(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
    .locals 0
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 217
    return-void
.end method

.method public onAccessoryDisconnected(J)V
    .locals 0
    .param p1, "accId"    # J

    .prologue
    .line 238
    return-void
.end method

.method public onAccessoryMessageReceived(Lcom/samsung/appcessory/base/SAPMessageItem;)V
    .locals 11
    .param p1, "item"    # Lcom/samsung/appcessory/base/SAPMessageItem;

    .prologue
    .line 189
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "onAccessoryMessageReceived "

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    if-nez p1, :cond_0

    .line 191
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "onAccessoryMessageReceived Message Item is null"

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    :goto_0
    return-void

    .line 194
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/appcessory/base/SAPMessageItem;->getAccessoryId()J

    move-result-wide v2

    .line 195
    .local v2, "accId":J
    invoke-virtual {p1}, Lcom/samsung/appcessory/base/SAPMessageItem;->getMessage()Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v7

    .line 196
    .local v7, "m":Lcom/samsung/appcessory/base/SAPMessage;
    if-nez v7, :cond_1

    .line 197
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "onAccessoryMessageReceived Message is null"

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 200
    :cond_1
    invoke-virtual {v7}, Lcom/samsung/appcessory/base/SAPMessage;->getSessionId()J

    move-result-wide v4

    .line 201
    .local v4, "sessionId":J
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "onAccessoryMessageReceived on acc "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "session "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "mesg is "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v10

    invoke-static {v10}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    invoke-virtual {v7}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v8

    array-length v6, v8

    .line 205
    .local v6, "length":I
    invoke-virtual {v7}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v1

    .line 206
    .local v1, "buffer":[B
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "read buffer length :: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    sget-object v8, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;

    .line 209
    .local v0, "appListener":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;
    if-eqz v0, :cond_2

    .line 210
    invoke-interface/range {v0 .. v5}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;->onReceive([BJJ)I

    goto/16 :goto_0

    .line 212
    :cond_2
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Application callback is null"

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onAcessoryAttached(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
    .locals 2
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 185
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onAcessoryAttached"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    return-void
.end method

.method public onConnectStateChange(JI)V
    .locals 0
    .param p1, "id"    # J
    .param p3, "status"    # I

    .prologue
    .line 228
    return-void
.end method

.method public onError(JJI)V
    .locals 7
    .param p1, "accId"    # J
    .param p3, "mSessionID"    # J
    .param p5, "errCode"    # I

    .prologue
    .line 255
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onError "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " errCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;

    .line 257
    .local v0, "appListener":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;
    if-eqz v0, :cond_0

    .line 258
    const-string v1, "WRONG_DATA_FORMAT"

    # invokes: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->_translateErrorCode(I)I
    invoke-static {p5}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$5(I)I

    move-result v2

    move-wide v3, p1

    move-wide v5, p3

    invoke-interface/range {v0 .. v6}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;->onError(Ljava/lang/String;IJJ)I

    .line 262
    :goto_0
    return-void

    .line 260
    :cond_0
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Application callback is null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V
    .locals 0
    .param p1, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    .param p2, "accID"    # J
    .param p4, "errCode"    # I

    .prologue
    .line 267
    return-void
.end method

.method public onNewConnection(JLcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V
    .locals 0
    .param p1, "accId"    # J
    .param p3, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 243
    return-void
.end method

.method public onPairedStatus(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JZ)V
    .locals 0
    .param p1, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    .param p2, "accID"    # J
    .param p4, "isPaired"    # Z

    .prologue
    .line 274
    return-void
.end method

.method public onSessionClose(JJ)V
    .locals 6
    .param p1, "accId"    # J
    .param p3, "sId"    # J

    .prologue
    .line 247
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onSessionClose accId= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v0

    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/appcessory/base/IAccessoryEventListener;

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/appcessory/server/SAPService;->removeAccessoryListener(JJLcom/samsung/appcessory/base/IAccessoryEventListener;)V

    .line 249
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    return-void
.end method

.method public onStatusChange(ILcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V
    .locals 0
    .param p1, "status"    # I
    .param p2, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 232
    return-void
.end method
