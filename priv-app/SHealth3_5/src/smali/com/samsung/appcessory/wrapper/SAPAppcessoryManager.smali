.class public Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;
.super Ljava/lang/Object;
.source "SAPAppcessoryManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AccessoryListener;,
        Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;,
        Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;,
        Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;,
        Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;
    }
.end annotation


# static fields
.field public static final CLOSE_SESSION_TIME_OUT_ERROR:I = 0x6c

.field public static final CONNECTION_ERROR:I = 0x66

.field public static final CONNECTION_NO_RESPONSE_ERROR:I = 0x6e

.field public static final OPEN_SESSION_IN_PROGRESS_ERROR:I = 0x6a

.field public static final OPEN_SESSION_TIME_OUT_ERROR:I = 0x6b

.field public static PAYLOAD_BINARY:B = 0x0t

.field public static PAYLOAD_JSON:B = 0x0t

.field public static final SENSOR_NOT_PAIRED_ERROR:I = 0x68

.field public static final SENSOR_SEARCH_FAILED_ERROR:I = 0x69

.field private static TAG:Ljava/lang/String; = null

.field public static final TRANSPORT_BLE:I = 0x4

.field public static final TRANSPORT_BT:I = 0x2

.field public static final TRANSPORT_DISABLED:I = 0x0

.field public static final TRANSPORT_ENABLED:I = 0x1

.field public static final TRANSPORT_NFC:I = 0x8

.field public static final TRANSPORT_USB:I = 0x10

.field public static final TRANSPORT_WIFI:I = 0x1

.field public static final UNKNOWN_ERROR:I = 0x64

.field public static final WRITE_MESSAGE_ERROR:I = 0x6d

.field public static final WRONG_DATA_FORMAT:I = 0x67

.field protected static mAppDataListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;",
            ">;"
        }
    .end annotation
.end field

.field protected static mAppEventListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final mLock:Ljava/lang/Object;

.field protected static mSapDataListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AccessoryListener;",
            ">;"
        }
    .end annotation
.end field

.field private static mSapEventListener:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;

.field private static mService:Lcom/samsung/appcessory/server/SAPService;

.field private static final mServiceConnection:Landroid/content/ServiceConnection;

.field static m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;


# instance fields
.field m_Context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    const-string v0, "SAP/SAPAppcessoryManager/29Dec2014"

    sput-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    .line 41
    sput-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    .line 46
    sput-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;

    .line 57
    const/4 v0, 0x1

    sput-byte v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->PAYLOAD_BINARY:B

    .line 61
    const/4 v0, 0x2

    sput-byte v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->PAYLOAD_JSON:B

    .line 713
    new-instance v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$1;

    invoke-direct {v0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$1;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 730
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static _translateErrorCode(I)I
    .locals 2
    .param p0, "err"    # I

    .prologue
    .line 1450
    const/16 v0, 0x64

    .line 1451
    .local v0, "error":I
    const/16 v1, 0x66

    if-ne p0, v1, :cond_1

    .line 1452
    const/16 v0, 0x66

    .line 1472
    :cond_0
    :goto_0
    return v0

    .line 1453
    :cond_1
    const/16 v1, 0x68

    if-ne p0, v1, :cond_2

    .line 1454
    const/16 v0, 0x68

    .line 1455
    goto :goto_0

    :cond_2
    const/16 v1, 0x69

    if-ne p0, v1, :cond_3

    .line 1456
    const/16 v0, 0x69

    .line 1457
    goto :goto_0

    :cond_3
    const/16 v1, 0x64

    if-ne p0, v1, :cond_4

    .line 1458
    const/16 v0, 0x64

    .line 1459
    goto :goto_0

    :cond_4
    const/16 v1, 0x67

    if-ne p0, v1, :cond_5

    .line 1460
    const/16 v0, 0x67

    .line 1461
    goto :goto_0

    :cond_5
    const/16 v1, 0x6a

    if-ne p0, v1, :cond_6

    .line 1462
    const/16 v0, 0x6a

    .line 1463
    goto :goto_0

    :cond_6
    const/16 v1, 0x6b

    if-ne p0, v1, :cond_7

    .line 1464
    const/16 v0, 0x6b

    .line 1465
    goto :goto_0

    :cond_7
    const/16 v1, 0x6c

    if-ne p0, v1, :cond_8

    .line 1466
    const/16 v0, 0x6c

    .line 1467
    goto :goto_0

    :cond_8
    const/16 v1, 0x6d

    if-ne p0, v1, :cond_9

    .line 1468
    const/16 v0, 0x6d

    .line 1469
    goto :goto_0

    :cond_9
    const/16 v1, 0x6e

    if-ne p0, v1, :cond_0

    .line 1470
    const/16 v0, 0x6e

    goto :goto_0
.end method

.method private _translateTransportType(I)Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    .locals 2
    .param p1, "transport"    # I

    .prologue
    .line 1436
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_WIFI:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 1437
    .local v0, "type":Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    .line 1438
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 1446
    :cond_0
    :goto_0
    return-object v0

    .line 1439
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 1440
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_WIFI:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 1441
    goto :goto_0

    :cond_2
    const/16 v1, 0x10

    if-ne p1, v1, :cond_3

    .line 1442
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_USB:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 1443
    goto :goto_0

    :cond_3
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 1444
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    goto :goto_0
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/appcessory/server/SAPService;)V
    .locals 0

    .prologue
    .line 39
    sput-object p0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;)V
    .locals 0

    .prologue
    .line 42
    sput-object p0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapEventListener:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;

    return-void
.end method

.method static synthetic access$3()Lcom/samsung/appcessory/server/SAPService;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    return-object v0
.end method

.method static synthetic access$4()Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapEventListener:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;

    return-object v0
.end method

.method static synthetic access$5(I)I
    .locals 1

    .prologue
    .line 1449
    invoke-static {p0}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->_translateErrorCode(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$6()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method public static declared-synchronized getDefaultManager(Landroid/content/Context;J)Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;
    .locals 6
    .param p0, "uContext"    # Landroid/content/Context;
    .param p1, "transportMask"    # J

    .prologue
    .line 756
    const-class v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    monitor-enter v2

    :try_start_0
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 757
    :try_start_1
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    if-nez v1, :cond_0

    .line 758
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v4, "getDefaultAdapter()"

    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    new-instance v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    invoke-direct {v1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;-><init>()V

    sput-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    .line 761
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iput-object p0, v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_Context:Landroid/content/Context;

    .line 765
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/appcessory/server/SAPService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 766
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "TransortMask"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 767
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v1, v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_Context:Landroid/content/Context;

    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 768
    const/4 v5, 0x1

    .line 767
    invoke-virtual {v1, v0, v4, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 770
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 772
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    monitor-exit v2

    return-object v1

    :cond_0
    :try_start_2
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    monitor-exit v3

    goto :goto_0

    .line 756
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public closeConnection(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z
    .locals 7
    .param p1, "uDevice"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    .prologue
    .line 1235
    const/4 v1, 0x0

    .line 1236
    .local v1, "ret":Z
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v4, "closeConnection()"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1238
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v3, :cond_0

    .line 1239
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v4, "Service not created"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1254
    .end local v1    # "ret":Z
    .local v2, "ret":I
    :goto_0
    return v2

    .line 1242
    .end local v2    # "ret":I
    .restart local v1    # "ret":Z
    :cond_0
    if-nez p1, :cond_1

    .line 1243
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v4, "Device is null"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1244
    .restart local v2    # "ret":I
    goto :goto_0

    .line 1247
    .end local v2    # "ret":I
    :cond_1
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Lcom/samsung/appcessory/server/SAPService;->getActiveAccessoryObjectById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v0

    .line 1248
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v0, :cond_2

    .line 1249
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "closing connection acc=>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1250
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    invoke-virtual {v3, v0}, Lcom/samsung/appcessory/server/SAPService;->removeAccessoryObject(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z

    move-result v1

    :goto_1
    move v2, v1

    .line 1254
    .restart local v2    # "ret":I
    goto :goto_0

    .line 1252
    .end local v2    # "ret":I
    :cond_2
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v4, "closing connection acc= null"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public closeSession(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J)Z
    .locals 7
    .param p1, "uDevice"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    .param p2, "sessionId"    # J

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1195
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v2, "closeSession()"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v1, :cond_0

    .line 1198
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v2, "Service not created"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1221
    :goto_0
    return v0

    .line 1201
    :cond_0
    if-nez p1, :cond_1

    .line 1202
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v2, "Device is null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1205
    :cond_1
    const-wide/16 v1, 0xff

    cmp-long v1, p2, v1

    if-nez v1, :cond_2

    .line 1206
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v2, "Reserved Session ID , No need to call closesession"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1209
    :cond_2
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_3

    .line 1210
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v1, "closeSession session is invalid, disconnect the device"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1211
    invoke-virtual {p0, p1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->closeConnection(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z

    move v0, v6

    .line 1212
    goto :goto_0

    .line 1214
    :cond_3
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    iget-wide v1, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/appcessory/server/SAPService;->closeSession(Ljava/lang/Long;Ljava/lang/Long;)Z

    .line 1215
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Removing listener for session"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "appcessory id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1216
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    iget-wide v1, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    .line 1217
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/appcessory/base/IAccessoryEventListener;

    move-wide v3, p2

    .line 1216
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/appcessory/server/SAPService;->removeAccessoryListener(JJLcom/samsung/appcessory/base/IAccessoryEventListener;)V

    .line 1218
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v6

    .line 1221
    goto/16 :goto_0
.end method

.method public deInit()V
    .locals 5

    .prologue
    .line 1413
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v1, "deinit Called"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1415
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 1416
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1418
    :cond_0
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 1419
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1421
    :cond_1
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-eqz v0, :cond_2

    .line 1422
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    invoke-virtual {v0}, Lcom/samsung/appcessory/server/SAPService;->deinit()V

    .line 1424
    :cond_2
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1425
    :try_start_0
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    if-eqz v0, :cond_3

    .line 1426
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v0, v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_Context:Landroid/content/Context;

    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1427
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v0, v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_Context:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    .line 1428
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    iget-object v3, v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_Context:Landroid/content/Context;

    const-class v4, Lcom/samsung/appcessory/server/SAPService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1427
    invoke-virtual {v0, v2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 1429
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->m_OnlyInstance:Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;

    .line 1424
    :cond_3
    monitor-exit v1

    .line 1433
    return-void

    .line 1424
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized deRegisterEventListener(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;I)Z
    .locals 8
    .param p1, "uListener"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    .param p2, "transportId"    # I

    .prologue
    const/4 v7, 0x1

    .line 1382
    monitor-enter p0

    :try_start_0
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v4, "deRegisterEventListener Entry"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1384
    if-eqz p1, :cond_0

    .line 1386
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v3, 0x8

    if-lt v0, v3, :cond_1

    .line 1403
    .end local v0    # "i":I
    :cond_0
    monitor-exit p0

    return v7

    .line 1387
    .restart local v0    # "i":I
    :cond_1
    shl-int v2, v7, v0

    .line 1388
    .local v2, "transportMask":I
    and-int v3, p2, v2

    if-eqz v3, :cond_2

    .line 1389
    :try_start_1
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Un register event for transport "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1390
    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1391
    :try_start_2
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1392
    .local v1, "mListnList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-eqz v1, :cond_3

    .line 1393
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Listener Removed   Key ="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "listener="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1394
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1395
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1390
    :goto_1
    monitor-exit v4

    .line 1386
    .end local v1    # "mListnList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1397
    .restart local v1    # "mListnList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_3
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Listener Already Removed "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1390
    .end local v1    # "mListnList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1382
    .end local v0    # "i":I
    .end local v2    # "transportMask":I
    :catchall_1
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public getBLEVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1480
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v0, :cond_0

    .line 1481
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v1, "Service not created"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1482
    const/4 v0, 0x0

    .line 1484
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    invoke-virtual {v0}, Lcom/samsung/appcessory/server/SAPService;->getBLEVersion()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getListofAvailableDevices(I)Ljava/util/List;
    .locals 8
    .param p1, "transport"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 984
    sget-object v6, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v6, :cond_1

    .line 985
    sget-object v6, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v7, "Service not created"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v5

    .line 1020
    :cond_0
    :goto_0
    return-object v3

    .line 989
    :cond_1
    sget-object v6, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    .line 990
    invoke-direct {p0, p1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->_translateTransportType(I)Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/appcessory/server/SAPService;->getActiveAccessoryObjectByType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Ljava/util/List;

    move-result-object v1

    .line 991
    .local v1, "accList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    if-nez v1, :cond_2

    .line 992
    sget-object v6, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v7, "Error in getting accessory"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v5

    .line 993
    goto :goto_0

    .line 995
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 996
    .local v3, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;>;"
    const/4 v6, 0x4

    if-ne p1, v6, :cond_3

    .line 997
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 998
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 999
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    new-instance v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    invoke-direct {v2}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;-><init>()V

    .line 1000
    .local v2, "device":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBleAccessory;->getDeviceAddress()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    .line 1001
    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBleAccessory;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    .line 1002
    iput p1, v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->transportId:I

    .line 1003
    iget-wide v5, v0, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    iput-wide v5, v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    .line 1004
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 997
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1006
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .end local v2    # "device":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    .end local v4    # "i":I
    :cond_3
    const/4 v6, 0x2

    if-ne p1, v6, :cond_4

    .line 1007
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 1008
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .line 1009
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    new-instance v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    invoke-direct {v2}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;-><init>()V

    .line 1010
    .restart local v2    # "device":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    iget-object v5, v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    .line 1011
    iget-object v5, v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    .line 1012
    iput p1, v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->transportId:I

    .line 1013
    iget-wide v5, v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->_id:J

    iput-wide v5, v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    .line 1014
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1017
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    .end local v2    # "device":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    .end local v4    # "i":I
    :cond_4
    sget-object v6, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v7, "Not supported transport"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v5

    .line 1018
    goto/16 :goto_0
.end method

.method public getListofPairedDevices(I)Ljava/util/List;
    .locals 9
    .param p1, "transport"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v4, 0x0

    .line 1031
    sget-object v6, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v6, :cond_1

    .line 1032
    sget-object v6, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v7, "Service not created"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1068
    :cond_0
    :goto_0
    return-object v4

    .line 1036
    :cond_1
    if-eq p1, v8, :cond_2

    const/4 v6, 0x2

    if-eq p1, v6, :cond_2

    .line 1037
    sget-object v6, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v7, "Not supported transport"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1041
    :cond_2
    sget-object v6, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    invoke-virtual {v6, p1}, Lcom/samsung/appcessory/server/SAPService;->getPairedDevices(I)Ljava/util/List;

    move-result-object v0

    .line 1043
    .local v0, "accList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    if-nez v0, :cond_3

    .line 1044
    sget-object v6, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v7, "Error in getting paired devices"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1048
    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1050
    .local v4, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 1051
    new-instance v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    invoke-direct {v3}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;-><init>()V

    .line 1053
    .local v3, "device":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    if-ne p1, v8, :cond_4

    .line 1054
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 1055
    .local v1, "bleAcc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    iget-object v6, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    .line 1056
    iget-object v6, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    .line 1057
    iget-wide v6, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    iput-wide v6, v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    .line 1065
    .end local v1    # "bleAcc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    :goto_2
    iput p1, v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->transportId:I

    .line 1066
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1050
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1060
    :cond_4
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .line 1061
    .local v2, "btAcc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    iget-object v6, v2, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    .line 1062
    iget-object v6, v2, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    .line 1063
    iget-wide v6, v2, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->_id:J

    iput-wide v6, v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    goto :goto_2
.end method

.method public isConnected(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z
    .locals 6
    .param p1, "uDevice"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 885
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v3, :cond_0

    .line 886
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "Service not created"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    :goto_0
    return v1

    .line 889
    :cond_0
    if-nez p1, :cond_1

    .line 890
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "Device is null"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 893
    :cond_1
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    .line 894
    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Lcom/samsung/appcessory/server/SAPService;->getActiveAccessoryObjectById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v0

    .line 895
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v0, :cond_6

    .line 896
    iget-object v3, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    sget-object v4, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v3, v4, :cond_3

    .line 897
    check-cast v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBleAccessory;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 898
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BLE Device is connected "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 899
    goto :goto_0

    .line 901
    :cond_2
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BLE Device is not connected "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 904
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_3
    iget-object v3, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    sget-object v4, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v3, v4, :cond_5

    .line 905
    check-cast v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 906
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BT Device is connected "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 907
    goto :goto_0

    .line 909
    :cond_4
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BT Device is not connected "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 914
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_5
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isConnected is not supported for USB/WIFI "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 918
    :cond_6
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "accessory not found"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public isPaired(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z
    .locals 6
    .param p1, "uDevice"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 932
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v3, :cond_0

    .line 933
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "Service not created"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 966
    :goto_0
    return v1

    .line 936
    :cond_0
    if-nez p1, :cond_1

    .line 937
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "Device is null"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 940
    :cond_1
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    .line 941
    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Lcom/samsung/appcessory/server/SAPService;->getActiveAccessoryObjectById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v0

    .line 942
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v0, :cond_6

    .line 943
    iget-object v3, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    sget-object v4, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v3, v4, :cond_3

    .line 944
    check-cast v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBleAccessory;->isPaired()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 945
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BLE Device is paired "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 946
    goto :goto_0

    .line 948
    :cond_2
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BLE Device is unpaired "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 951
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_3
    iget-object v3, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    sget-object v4, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v3, v4, :cond_5

    .line 952
    check-cast v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->isPaired()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 953
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BT Device is paired "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 954
    goto :goto_0

    .line 956
    :cond_4
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BT Device is unpaired "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 961
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_5
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pairing is not valid for USB/WIFI "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 965
    :cond_6
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "accessory not found"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public openConnection(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z
    .locals 5
    .param p1, "uDevice"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    .prologue
    .line 1148
    const/4 v0, 0x0

    .line 1149
    .local v0, "ret":Z
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v2, :cond_0

    .line 1150
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "Service not created"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 1158
    .end local v0    # "ret":Z
    .local v1, "ret":I
    :goto_0
    return v1

    .line 1153
    .end local v1    # "ret":I
    .restart local v0    # "ret":Z
    :cond_0
    if-nez p1, :cond_1

    .line 1154
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "Device is null"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 1155
    .restart local v1    # "ret":I
    goto :goto_0

    .line 1157
    .end local v1    # "ret":I
    :cond_1
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    iget-wide v3, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/appcessory/server/SAPService;->openConnection(Ljava/lang/Long;Z)Z

    move-result v0

    move v1, v0

    .line 1158
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public openConnection(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;Z)Z
    .locals 5
    .param p1, "uDevice"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    .param p2, "isAutoConnect"    # Z

    .prologue
    .line 1126
    const/4 v0, 0x0

    .line 1127
    .local v0, "ret":Z
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v2, :cond_0

    .line 1128
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "Service not created"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 1136
    .end local v0    # "ret":Z
    .local v1, "ret":I
    :goto_0
    return v1

    .line 1131
    .end local v1    # "ret":I
    .restart local v0    # "ret":Z
    :cond_0
    if-nez p1, :cond_1

    .line 1132
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "Device is null"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 1133
    .restart local v1    # "ret":I
    goto :goto_0

    .line 1135
    .end local v1    # "ret":I
    :cond_1
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    iget-wide v3, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/samsung/appcessory/server/SAPService;->openConnection(Ljava/lang/Long;Z)Z

    move-result v0

    move v1, v0

    .line 1136
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public openSession(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;B)Z
    .locals 3
    .param p1, "uDevice"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    .param p2, "payloadType"    # B

    .prologue
    const/4 v0, 0x0

    .line 1173
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v1, :cond_0

    .line 1174
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v2, "Service not created"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1181
    :goto_0
    return v0

    .line 1177
    :cond_0
    if-nez p1, :cond_1

    .line 1178
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v2, "Device is null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1181
    :cond_1
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    iget-wide v1, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/samsung/appcessory/server/SAPService;->openSession(BLjava/lang/Long;)Z

    move-result v0

    goto :goto_0
.end method

.method public declared-synchronized registerDataListener(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J)Z
    .locals 6
    .param p1, "uListener"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IDataListener;
    .param p2, "uDevice"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    .param p3, "sessionId"    # J

    .prologue
    const/4 v0, 0x0

    .line 1300
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v1, :cond_0

    .line 1301
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v2, "Service not created"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1326
    :goto_0
    monitor-exit p0

    return v0

    .line 1304
    :cond_0
    if-nez p2, :cond_1

    .line 1305
    :try_start_1
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v2, "Device is null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1300
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1308
    :cond_1
    const-wide/16 v1, -0x1

    cmp-long v1, p3, v1

    if-nez v1, :cond_2

    .line 1309
    :try_start_2
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v2, "Invalid session -1"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1312
    :cond_2
    new-instance v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AccessoryListener;

    invoke-direct {v5}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AccessoryListener;-><init>()V

    .line 1314
    .local v5, "accListener":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AccessoryListener;
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    if-nez v0, :cond_3

    .line 1315
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    .line 1317
    :cond_3
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    if-nez v0, :cond_4

    .line 1318
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    .line 1320
    :cond_4
    if-eqz p1, :cond_5

    .line 1321
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1322
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1323
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    iget-wide v1, p2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/appcessory/server/SAPService;->addAccessoryListener(JJLcom/samsung/appcessory/base/IAccessoryEventListener;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1326
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public registerEventListener(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;I)Z
    .locals 7
    .param p1, "uListener"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    .param p2, "transportId"    # I

    .prologue
    const/4 v6, 0x1

    .line 1347
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "registerEventListener Entry transportId = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1348
    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 1349
    :try_start_0
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    if-nez v3, :cond_0

    .line 1350
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    .line 1348
    :cond_0
    monitor-exit v4

    .line 1354
    if-eqz p1, :cond_1

    .line 1356
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v3, 0x8

    if-lt v0, v3, :cond_2

    .line 1372
    .end local v0    # "i":I
    :cond_1
    return v6

    .line 1348
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1357
    .restart local v0    # "i":I
    :cond_2
    shl-int v2, v6, v0

    .line 1358
    .local v2, "transportMask":I
    and-int v3, p2, v2

    if-eqz v3, :cond_4

    .line 1359
    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 1360
    :try_start_1
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1362
    .local v1, "mListnList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-nez v1, :cond_3

    .line 1363
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "mListnList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1365
    .restart local v1    # "mListnList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_3
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1366
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1359
    monitor-exit v4

    .line 1356
    .end local v1    # "mListnList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1359
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v3
.end method

.method public send(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J[B)I
    .locals 5
    .param p1, "uDevice"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    .param p2, "sessionId"    # J
    .param p4, "buffer"    # [B

    .prologue
    const/4 v1, -0x1

    .line 1267
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v2, :cond_1

    .line 1268
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "Service not created"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1285
    :cond_0
    :goto_0
    return v1

    .line 1271
    :cond_1
    if-nez p1, :cond_2

    .line 1272
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "Device is null"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1275
    :cond_2
    const-wide/16 v2, -0x1

    cmp-long v2, p2, v2

    if-nez v2, :cond_3

    .line 1276
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v3, "Invalid session -1"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1279
    :cond_3
    new-instance v0, Lcom/samsung/appcessory/base/SAPMessage;

    invoke-direct {v0, p2, p3}, Lcom/samsung/appcessory/base/SAPMessage;-><init>(J)V

    .line 1280
    .local v0, "m":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual {v0, p4}, Lcom/samsung/appcessory/base/SAPMessage;->setMessageBytes([B)V

    .line 1281
    sget-object v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    iget-wide v3, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, Lcom/samsung/appcessory/server/SAPService;->sendMessage(Ljava/lang/Long;Ljava/lang/Long;Lcom/samsung/appcessory/base/SAPMessage;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1285
    array-length v1, p4

    goto :goto_0
.end method

.method public startDiscovery(I)Z
    .locals 2
    .param p1, "transportId"    # I

    .prologue
    .line 785
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startDiscovery()"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v0, :cond_0

    .line 792
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v1, "Service not created"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 793
    const/4 v0, 0x0

    .line 796
    :goto_0
    return v0

    .line 795
    :cond_0
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    invoke-direct {p0, p1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->_translateTransportType(I)Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/server/SAPService;->startDiscovery(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z

    .line 796
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stopDiscovery(I)Z
    .locals 2
    .param p1, "transportId"    # I

    .prologue
    .line 807
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopDiscovery()"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v0, :cond_0

    .line 810
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v1, "Service not created"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    const/4 v0, 0x0

    .line 817
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    invoke-direct {p0, p1}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->_translateTransportType(I)Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/server/SAPService;->stopDiscovery(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z

    move-result v0

    goto :goto_0
.end method

.method public unpairDevice(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)Z
    .locals 9
    .param p1, "uDevice"    # Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    .prologue
    .line 848
    const/4 v2, 0x0

    .line 849
    .local v2, "ret":Z
    sget-object v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "unpairDevice()"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    sget-object v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    if-nez v5, :cond_0

    .line 852
    sget-object v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v6, "Service not created"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 873
    .end local v2    # "ret":Z
    .local v3, "ret":I
    :goto_0
    return v3

    .line 855
    .end local v3    # "ret":I
    .restart local v2    # "ret":Z
    :cond_0
    if-nez p1, :cond_1

    .line 856
    sget-object v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string v6, "Device is null"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 857
    .restart local v3    # "ret":I
    goto :goto_0

    .line 859
    .end local v3    # "ret":I
    :cond_1
    sget-object v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    .line 860
    iget-wide v6, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-virtual {v5, v6, v7}, Lcom/samsung/appcessory/server/SAPService;->getActiveAccessoryObjectById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v0

    .line 861
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v0, :cond_4

    .line 862
    iget-wide v5, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    invoke-static {v5, v6}, Lcom/samsung/appcessory/session/SAPSessionManager;->getAllAccessorySessions(J)Ljava/util/List;

    move-result-object v4

    .line 863
    .local v4, "sessionList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-lt v5, v6, :cond_2

    .line 864
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lt v1, v5, :cond_3

    .line 868
    .end local v1    # "i":I
    :cond_2
    sget-object v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    invoke-virtual {v5, v0}, Lcom/samsung/appcessory/server/SAPService;->unpairAccessoryObject(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z

    move-result v2

    .end local v4    # "sessionList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    :goto_2
    move v3, v2

    .line 873
    .restart local v3    # "ret":I
    goto :goto_0

    .line 865
    .end local v3    # "ret":I
    .restart local v1    # "i":I
    .restart local v4    # "sessionList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    :cond_3
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/appcessory/session/SAPSession;

    iget-wide v5, v5, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual {p0, p1, v5, v6}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->closeSession(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;J)Z

    .line 864
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 870
    .end local v1    # "i":I
    .end local v4    # "sessionList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    :cond_4
    sget-object v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "unpair unknown accessory "

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    sget-object v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;

    iget-object v6, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    iget-object v7, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    iget v8, p1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->transportId:I

    invoke-direct {p0, v8}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->_translateTransportType(I)Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    move-result-object v8

    invoke-virtual {v5, v6, v7, v8}, Lcom/samsung/appcessory/server/SAPService;->unpairUnknownAcc(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z

    move-result v2

    goto :goto_2
.end method
