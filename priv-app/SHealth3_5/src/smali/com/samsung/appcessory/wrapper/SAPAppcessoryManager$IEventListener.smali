.class public interface abstract Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
.super Ljava/lang/Object;
.source "SAPAppcessoryManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IEventListener"
.end annotation


# virtual methods
.method public abstract onAccessoryAttached(J)V
.end method

.method public abstract onAccessoryDetached(J)V
.end method

.method public abstract onCloseSessionRequest(JJ)V
.end method

.method public abstract onDeviceFound(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)V
.end method

.method public abstract onError(III)V
.end method

.method public abstract onOpenSessionAccepted(JJ)V
.end method

.method public abstract onOpenSessionRequest(JJ)V
.end method

.method public abstract onPairedStatus(IJZ)V
.end method

.method public abstract onSessionClosed(JJ)V
.end method

.method public abstract onTransportStatusChanged(II)V
.end method
