.class Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$SapEventListener;
.super Ljava/lang/Object;
.source "SAPAppcessoryManager.java"

# interfaces
.implements Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SapEventListener"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccessoryAttached(J)V
    .locals 8
    .param p1, "accessoryId"    # J

    .prologue
    .line 520
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/appcessory/server/SAPService;->getAccessoryType(Ljava/lang/Long;)I

    move-result v3

    .line 521
    .local v3, "transportId":I
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "onAccessoryAttached transport "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$6()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 523
    :try_start_0
    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    if-eqz v4, :cond_0

    .line 524
    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 525
    .local v0, "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 526
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 522
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    :cond_0
    :goto_1
    monitor-exit v5

    .line 539
    return-void

    .line 527
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;

    .line 528
    .local v2, "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    if-eqz v2, :cond_2

    .line 529
    invoke-interface {v2, p1, p2}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;->onAccessoryAttached(J)V

    .line 526
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 531
    :cond_2
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "listener not available for transport "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 522
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    .end local v2    # "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 535
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_3
    :try_start_1
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "App callback is null for transport "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public onAccessoryDetached(J)V
    .locals 11
    .param p1, "accessoryId"    # J

    .prologue
    .line 289
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onAccessoryDetached accessoryId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 292
    invoke-static {p1, p2}, Lcom/samsung/appcessory/session/SAPSessionManager;->getAllAccessorySessions(J)Ljava/util/List;

    move-result-object v9

    .line 293
    .local v9, "sessionList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    if-eqz v9, :cond_0

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 294
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-lt v7, v0, :cond_3

    .line 307
    .end local v7    # "i":I
    :cond_0
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/server/SAPService;->getAccessoryType(Ljava/lang/Long;)I

    move-result v10

    .line 308
    .local v10, "transportId":I
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onAccessoryDetached transport "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$6()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 310
    :try_start_0
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 311
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 312
    .local v6, "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 313
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v7, v0, :cond_6

    .line 309
    .end local v6    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v7    # "i":I
    :cond_1
    :goto_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    .end local v9    # "sessionList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    .end local v10    # "transportId":I
    :cond_2
    return-void

    .line 295
    .restart local v7    # "i":I
    .restart local v9    # "sessionList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    :cond_3
    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/session/SAPSession;

    iget-wide v3, v0, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    .line 296
    .local v3, "sessionId":J
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Removing listener for session"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "appcessory id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v0

    .line 298
    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/appcessory/base/IAccessoryEventListener;

    move-wide v1, p1

    .line 297
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/appcessory/server/SAPService;->removeAccessoryListener(JJLcom/samsung/appcessory/base/IAccessoryEventListener;)V

    .line 299
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    if-eqz v0, :cond_4

    .line 300
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    :cond_4
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    if-eqz v0, :cond_5

    .line 303
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 314
    .end local v3    # "sessionId":J
    .restart local v6    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .restart local v10    # "transportId":I
    :cond_6
    :try_start_1
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;

    .line 315
    .local v8, "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    if-eqz v8, :cond_7

    .line 316
    invoke-interface {v8, p1, p2}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;->onAccessoryDetached(J)V

    .line 313
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 318
    :cond_7
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "listener not available for transport "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 309
    .end local v6    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v7    # "i":I
    .end local v8    # "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 322
    .restart local v6    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_8
    :try_start_2
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "App callback is null for transport "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2
.end method

.method public onCloseSessionRequest(JJ)V
    .locals 11
    .param p1, "accessoryId"    # J
    .param p3, "sId"    # J

    .prologue
    .line 422
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/server/SAPService;->getAccessoryType(Ljava/lang/Long;)I

    move-result v9

    .line 423
    .local v9, "transportId":I
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onCloseSessionRequest transport "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$6()Ljava/lang/Object;

    move-result-object v10

    monitor-enter v10

    .line 425
    :try_start_0
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 426
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 427
    .local v6, "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 428
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v7, v0, :cond_1

    .line 424
    .end local v6    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v7    # "i":I
    :cond_0
    :goto_1
    monitor-exit v10

    .line 444
    return-void

    .line 429
    .restart local v6    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .restart local v7    # "i":I
    :cond_1
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;

    .line 430
    .local v8, "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    if-eqz v8, :cond_2

    .line 431
    invoke-interface {v8, p1, p2, p3, p4}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;->onCloseSessionRequest(JJ)V

    .line 432
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v0

    sget-object v1, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/appcessory/base/IAccessoryEventListener;

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/appcessory/server/SAPService;->removeAccessoryListener(JJLcom/samsung/appcessory/base/IAccessoryEventListener;)V

    .line 433
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mSapDataListeners:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    sget-object v0, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppDataListeners:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 436
    :cond_2
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "listener not available for transport "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 424
    .end local v6    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v7    # "i":I
    .end local v8    # "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 440
    .restart local v6    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_3
    :try_start_1
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "App callback is null for transport "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public onDeviceFound(JI)V
    .locals 12
    .param p1, "id"    # J
    .param p3, "type"    # I

    .prologue
    .line 471
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "onDeviceFound "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "type "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$6()Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 473
    :try_start_0
    sget-object v8, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    if-eqz v8, :cond_1

    .line 474
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Lcom/samsung/appcessory/server/SAPService;->getActiveAccessoryObjectById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v1

    .line 475
    .local v1, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-nez v1, :cond_0

    .line 476
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "onDeviceFound acc is null id "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    monitor-exit v9

    .line 516
    .end local v1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :goto_0
    return-void

    .line 479
    .restart local v1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_0
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "mAppEventListeners size "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v11, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-virtual {v11}, Ljava/util/HashMap;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    sget-object v8, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    .line 481
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 482
    .local v2, "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_6

    .line 483
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v6, v8, :cond_2

    .line 472
    .end local v1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v2    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v6    # "i":I
    :cond_1
    :goto_2
    monitor-exit v9

    goto :goto_0

    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    .line 484
    .restart local v1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .restart local v2    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .restart local v6    # "i":I
    :cond_2
    :try_start_1
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;

    .line 485
    .local v7, "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    if-eqz v7, :cond_5

    .line 486
    new-instance v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;

    invoke-direct {v5}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;-><init>()V

    .line 487
    .local v5, "device":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    const/4 v8, 0x4

    if-ne p3, v8, :cond_3

    instance-of v8, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;

    if-eqz v8, :cond_3

    .line 488
    move-object v0, v1

    check-cast v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-object v3, v0

    .line 489
    .local v3, "bleAcc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    iget-object v8, v3, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 490
    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    .line 489
    iput-object v8, v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    .line 491
    iget-object v8, v3, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    .line 492
    iget-wide v10, v3, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    iput-wide v10, v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    .line 502
    .end local v3    # "bleAcc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    :goto_3
    iput p3, v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->transportId:I

    .line 503
    invoke-interface {v7, v5}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;->onDeviceFound(Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;)V

    .line 483
    .end local v5    # "device":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 493
    .restart local v5    # "device":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    :cond_3
    const/4 v8, 0x2

    if-ne p3, v8, :cond_4

    instance-of v8, v1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    if-eqz v8, :cond_4

    .line 494
    move-object v0, v1

    check-cast v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    move-object v4, v0

    .line 495
    .local v4, "btAcc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    iget-object v8, v4, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->address:Ljava/lang/String;

    .line 496
    iget-object v8, v4, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->name:Ljava/lang/String;

    .line 497
    iget-wide v10, v4, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->_id:J

    iput-wide v10, v5, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;->appcessoryId:J

    goto :goto_3

    .line 499
    .end local v4    # "btAcc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    :cond_4
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    const-string v10, "No matching. Ignore this accessory.."

    invoke-static {v8, v10}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 505
    .end local v5    # "device":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$AppcessoryDevice;
    :cond_5
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    .line 506
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "listener not available for transport "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 507
    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 506
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 505
    invoke-static {v8, v10}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 511
    .end local v6    # "i":I
    .end local v7    # "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    :cond_6
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v8

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "App callback is null for transport "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 512
    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 511
    invoke-static {v8, v10}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2
.end method

.method public onError(III)V
    .locals 7
    .param p1, "transportId"    # I
    .param p2, "accID"    # I
    .param p3, "errCode"    # I

    .prologue
    .line 543
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "onError tID="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " accID="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " errCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$6()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 545
    :try_start_0
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    if-eqz v3, :cond_0

    .line 546
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 547
    .local v0, "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 548
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 544
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    :cond_0
    :goto_1
    monitor-exit v4

    .line 561
    return-void

    .line 549
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;

    .line 550
    .local v2, "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    if-eqz v2, :cond_2

    .line 551
    # invokes: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->_translateErrorCode(I)I
    invoke-static {p3}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$5(I)I

    move-result v3

    invoke-interface {v2, p1, p2, v3}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;->onError(III)V

    .line 548
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 553
    :cond_2
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "listener not available for transport "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 544
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    .end local v2    # "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 557
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_3
    :try_start_1
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "App callback is null for transport "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public onOpenSessionAccepted(JJ)V
    .locals 8
    .param p1, "accessoryId"    # J
    .param p3, "sessionId"    # J

    .prologue
    .line 399
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/appcessory/server/SAPService;->getAccessoryType(Ljava/lang/Long;)I

    move-result v3

    .line 400
    .local v3, "transportId":I
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "onOpenSessionAccepted transport "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$6()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 402
    :try_start_0
    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    if-eqz v4, :cond_0

    .line 403
    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 404
    .local v0, "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 405
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 401
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    :cond_0
    :goto_1
    monitor-exit v5

    .line 418
    return-void

    .line 406
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;

    .line 407
    .local v2, "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    if-eqz v2, :cond_2

    .line 408
    invoke-interface {v2, p1, p2, p3, p4}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;->onOpenSessionAccepted(JJ)V

    .line 405
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 410
    :cond_2
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "listener not available for transport "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 401
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    .end local v2    # "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 414
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_3
    :try_start_1
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "App callback is null for transport "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public onOpenSessionRequest(JJ)V
    .locals 8
    .param p1, "accessoryId"    # J
    .param p3, "sessionId"    # J

    .prologue
    .line 376
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/appcessory/server/SAPService;->getAccessoryType(Ljava/lang/Long;)I

    move-result v3

    .line 377
    .local v3, "transportId":I
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "onOpenSessionRequest transport "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$6()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 379
    :try_start_0
    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    if-eqz v4, :cond_0

    .line 380
    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 381
    .local v0, "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 382
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 378
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    :cond_0
    :goto_1
    monitor-exit v5

    .line 395
    return-void

    .line 383
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;

    .line 384
    .local v2, "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    if-eqz v2, :cond_2

    .line 385
    invoke-interface {v2, p1, p2, p3, p4}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;->onOpenSessionRequest(JJ)V

    .line 382
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 387
    :cond_2
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "listener not available for transport "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 378
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    .end local v2    # "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 391
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_3
    :try_start_1
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "App callback is null for transport "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public onPairedStatus(IJZ)V
    .locals 7
    .param p1, "transportId"    # I
    .param p2, "accID"    # J
    .param p4, "isPaired"    # Z

    .prologue
    .line 354
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "onPairedStatus transport "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$6()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 356
    :try_start_0
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    if-eqz v3, :cond_0

    .line 357
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 358
    .local v0, "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 359
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 355
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    :cond_0
    :goto_1
    monitor-exit v4

    .line 372
    return-void

    .line 360
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;

    .line 361
    .local v2, "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    if-eqz v2, :cond_2

    .line 362
    invoke-interface {v2, p1, p2, p3, p4}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;->onPairedStatus(IJZ)V

    .line 359
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 364
    :cond_2
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "listener not available for transport "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 355
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    .end local v2    # "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 368
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_3
    :try_start_1
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "App callback is null for transport "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public onSessionClosed(JJ)V
    .locals 8
    .param p1, "accessoryId"    # J
    .param p3, "sId"    # J

    .prologue
    .line 448
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mService:Lcom/samsung/appcessory/server/SAPService;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$3()Lcom/samsung/appcessory/server/SAPService;

    move-result-object v4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/appcessory/server/SAPService;->getAccessoryType(Ljava/lang/Long;)I

    move-result v3

    .line 449
    .local v3, "transportId":I
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "onSessionClosed transport "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$6()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 451
    :try_start_0
    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    if-eqz v4, :cond_0

    .line 452
    sget-object v4, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 453
    .local v0, "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 454
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 450
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    :cond_0
    :goto_1
    monitor-exit v5

    .line 467
    return-void

    .line 455
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;

    .line 456
    .local v2, "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    if-eqz v2, :cond_2

    .line 457
    invoke-interface {v2, p1, p2, p3, p4}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;->onSessionClosed(JJ)V

    .line 454
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 459
    :cond_2
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "listener not available for transport "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 450
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    .end local v2    # "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 463
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_3
    :try_start_1
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "App callback is null for transport "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public onTransportStatusChanged(II)V
    .locals 7
    .param p1, "status"    # I
    .param p2, "transportId"    # I

    .prologue
    .line 332
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "onTransportStatusChanged transport "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$6()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 334
    :try_start_0
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    if-eqz v3, :cond_0

    .line 335
    sget-object v3, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->mAppEventListeners:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 336
    .local v0, "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 337
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 333
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    :cond_0
    :goto_1
    monitor-exit v4

    .line 350
    return-void

    .line 338
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;

    .line 339
    .local v2, "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    if-eqz v2, :cond_2

    .line 340
    invoke-interface {v2, p1, p2}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;->onTransportStatusChanged(II)V

    .line 337
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 342
    :cond_2
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "listener not available for transport "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 333
    .end local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    .end local v1    # "i":I
    .end local v2    # "list":Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 346
    .restart local v0    # "appCallBack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager$IEventListener;>;"
    :cond_3
    :try_start_1
    # getter for: Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/appcessory/wrapper/SAPAppcessoryManager;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "App callback is null for transport "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
