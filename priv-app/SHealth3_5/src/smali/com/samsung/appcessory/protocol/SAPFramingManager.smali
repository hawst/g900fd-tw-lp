.class public Lcom/samsung/appcessory/protocol/SAPFramingManager;
.super Ljava/lang/Object;
.source "SAPFramingManager.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BINARY_PAYLOAD:B = 0x1t

.field private static final CHARSET:Ljava/nio/charset/Charset;

.field public static final DELIMITER:C = ','

.field private static final ENCODING_FORMAT:Ljava/lang/String; = "UTF-8"

.field private static final JSON_PAYLOAD:B = 0x2t

.field private static final MAXIMUM_PAYLOAD_SIZE_IN_BYTES:I = 0x10000

.field private static final TAG:Ljava/lang/String; = "SAP/SAPFramingManager/29Dec2014"

.field private static sHolder:Ljava/nio/ByteBuffer;

.field static sHolderLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/samsung/appcessory/protocol/SAPFramingManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/appcessory/protocol/SAPFramingManager;->$assertionsDisabled:Z

    .line 44
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/samsung/appcessory/protocol/SAPFramingManager;->CHARSET:Ljava/nio/charset/Charset;

    .line 871
    const/high16 v0, 0x20000

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 870
    sput-object v0, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    .line 873
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolderLock:Ljava/lang/Object;

    .line 875
    return-void

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addEscapeSequenceToFrame([B)[B
    .locals 13
    .param p0, "buffer"    # [B

    .prologue
    const/16 v12, 0x7e

    const/16 v11, 0x7d

    .line 724
    sget-object v9, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolderLock:Ljava/lang/Object;

    monitor-enter v9

    .line 725
    :try_start_0
    array-length v3, p0

    .line 726
    .local v3, "length":I
    const/4 v2, 0x0

    .line 727
    .local v2, "index":I
    const/4 v5, 0x0

    .line 728
    .local v5, "offset":I
    const/4 v4, 0x0

    .line 730
    .local v4, "nCharsEscaped":I
    sget-object v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 731
    :goto_0
    if-lt v2, v3, :cond_1

    .line 763
    if-eq v2, v5, :cond_0

    .line 767
    sget-object v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    sub-int v10, v2, v5

    invoke-virtual {v8, p0, v5, v10}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 772
    :cond_0
    array-length v8, p0

    add-int v3, v8, v4

    .line 774
    new-array v6, v3, [B

    .line 775
    .local v6, "result":[B
    sget-object v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    .line 776
    .local v7, "temp":[B
    sget-object v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->arrayOffset()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 779
    .local v0, "arrayOffset":I
    const/4 v8, 0x0

    :try_start_1
    invoke-static {v7, v0, v6, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 785
    :try_start_2
    monitor-exit v9

    .end local v6    # "result":[B
    :goto_1
    return-object v6

    .line 733
    .end local v0    # "arrayOffset":I
    .end local v7    # "temp":[B
    :cond_1
    aget-byte v8, p0, v2

    if-eq v8, v12, :cond_2

    .line 734
    aget-byte v8, p0, v2

    if-ne v8, v11, :cond_3

    .line 742
    :cond_2
    sget-object v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    sub-int v10, v2, v5

    invoke-virtual {v8, p0, v5, v10}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 744
    sget-object v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    const/16 v10, 0x7d

    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 746
    aget-byte v8, p0, v2

    if-ne v8, v12, :cond_4

    .line 747
    sget-object v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    const/16 v10, 0x5e

    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 757
    :goto_2
    add-int/lit8 v5, v2, 0x1

    .line 758
    add-int/lit8 v4, v4, 0x1

    .line 731
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 750
    :cond_4
    sget-object v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    const/16 v10, 0x5d

    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_2

    .line 724
    .end local v2    # "index":I
    .end local v3    # "length":I
    .end local v4    # "nCharsEscaped":I
    .end local v5    # "offset":I
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v8

    .line 780
    .restart local v0    # "arrayOffset":I
    .restart local v2    # "index":I
    .restart local v3    # "length":I
    .restart local v4    # "nCharsEscaped":I
    .restart local v5    # "offset":I
    .restart local v6    # "result":[B
    .restart local v7    # "temp":[B
    :catch_0
    move-exception v1

    .line 781
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :try_start_3
    const-string v8, "SAP/SAPFramingManager/29Dec2014"

    const-string v10, "Array index out of bound"

    invoke-static {v8, v10}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 782
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v6, 0x0

    goto :goto_1
.end method

.method private static composeCapexReqMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B
    .locals 15
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    const v14, 0xff00

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 595
    const/4 v8, 0x0

    .line 596
    .local v8, "svcCount":I
    const/4 v0, 0x0

    .line 597
    .local v0, "count":I
    iget-char v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numSvc:C

    if-lez v9, :cond_0

    .line 598
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-char v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numSvc:C

    if-lt v2, v9, :cond_1

    .line 604
    iget-char v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numSvc:C

    add-int/lit8 v9, v9, -0x1

    add-int/2addr v8, v9

    .line 606
    .end local v2    # "i":I
    :cond_0
    iget-char v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numPayloadType:C

    add-int/lit8 v9, v9, 0xa

    add-int v7, v9, v8

    .line 607
    .local v7, "size":I
    new-array v4, v7, [B

    .line 608
    .local v4, "payload":[B
    const-string v9, "SAP/SAPFramingManager/29Dec2014"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "composeCapexReqMessage size = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 609
    const-string/jumbo v11, "params._numSvc = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-char v11, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numSvc:C

    int-to-short v11, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " svcCount = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 610
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 608
    invoke-static {v9, v10}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    const/16 v9, 0x60

    aput-byte v9, v4, v0

    .line 612
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    iget v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    int-to-byte v9, v9

    aput-byte v9, v4, v1

    .line 613
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    iget-object v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-byte v9, v9, v12

    aput-byte v9, v4, v0

    .line 614
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    iget-object v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-byte v9, v9, v13

    aput-byte v9, v4, v1

    .line 615
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    iget-object v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    aget-byte v9, v9, v12

    aput-byte v9, v4, v0

    .line 616
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    iget-object v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    aget-byte v9, v9, v13

    aput-byte v9, v4, v1

    .line 617
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    iget-char v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numPayloadType:C

    and-int/2addr v9, v14

    shr-int/lit8 v9, v9, 0x8

    int-to-byte v9, v9

    aput-byte v9, v4, v0

    .line 618
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    iget-char v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numPayloadType:C

    and-int/lit16 v9, v9, 0xff

    int-to-byte v9, v9

    aput-byte v9, v4, v1

    .line 620
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-char v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numPayloadType:C

    if-lt v2, v9, :cond_2

    .line 623
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    iget-char v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numSvc:C

    and-int/2addr v9, v14

    shr-int/lit8 v9, v9, 0x8

    int-to-byte v9, v9

    aput-byte v9, v4, v0

    .line 624
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    iget-char v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numSvc:C

    and-int/lit16 v9, v9, 0xff

    int-to-byte v9, v9

    aput-byte v9, v4, v1

    .line 626
    const/4 v2, 0x0

    :goto_2
    iget-char v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numSvc:C

    if-lt v2, v9, :cond_3

    .line 638
    return-object v4

    .line 599
    .end local v4    # "payload":[B
    .end local v7    # "size":I
    :cond_1
    iget-object v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_services:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    sget-object v10, Lcom/samsung/appcessory/protocol/SAPFramingManager;->CHARSET:Ljava/nio/charset/Charset;

    invoke-virtual {v9, v10}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v9

    array-length v9, v9

    add-int/2addr v8, v9

    .line 598
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 621
    .restart local v4    # "payload":[B
    .restart local v7    # "size":I
    :cond_2
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    iget-object v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_supportedPayloadType:[B

    aget-byte v9, v9, v2

    aput-byte v9, v4, v0

    .line 620
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_1

    .line 627
    :cond_3
    iget-object v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_services:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 628
    .local v5, "service":Ljava/lang/String;
    sget-object v9, Lcom/samsung/appcessory/protocol/SAPFramingManager;->CHARSET:Ljava/nio/charset/Charset;

    invoke-virtual {v5, v9}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v6

    .line 629
    .local v6, "serviceByte":[B
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_3
    array-length v9, v6

    if-lt v3, v9, :cond_5

    .line 632
    iget-char v9, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numSvc:C

    add-int/lit8 v9, v9, -0x1

    if-ge v2, v9, :cond_4

    .line 633
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    const/16 v9, 0x2c

    aput-byte v9, v4, v0

    move v0, v1

    .line 626
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 630
    :cond_5
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    aget-byte v9, v6, v3

    aput-byte v9, v4, v0

    .line 629
    add-int/lit8 v3, v3, 0x1

    move v0, v1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_3
.end method

.method private static composeCapexRspMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B
    .locals 7
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 644
    const/4 v0, 0x0

    .line 645
    .local v0, "count":I
    const/4 v3, 0x7

    .line 646
    .local v3, "size":I
    new-array v2, v3, [B

    .line 648
    .local v2, "payload":[B
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    const/16 v4, 0x60

    aput-byte v4, v2, v0

    .line 649
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    iget v4, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    int-to-byte v4, v4

    aput-byte v4, v2, v1

    .line 650
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    iget-byte v4, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_capexStatus:B

    aput-byte v4, v2, v0

    .line 651
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    iget-object v4, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-byte v4, v4, v5

    aput-byte v4, v2, v1

    .line 652
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    iget-object v4, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-byte v4, v4, v6

    aput-byte v4, v2, v0

    .line 653
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    iget-object v4, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    aget-byte v4, v4, v5

    aput-byte v4, v2, v1

    .line 654
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    iget-object v4, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    aget-byte v4, v4, v6

    aput-byte v4, v2, v0

    .line 656
    return-object v2
.end method

.method private static composeCloseSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B
    .locals 4
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    .line 539
    const/4 v1, 0x3

    new-array v0, v1, [B

    .line 541
    .local v0, "payload":[B
    const/4 v1, 0x0

    const/16 v2, 0x60

    aput-byte v2, v0, v1

    .line 542
    const/4 v1, 0x1

    iget v2, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 543
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 545
    return-object v0
.end method

.method private static composeOpenSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B
    .locals 8
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x3

    .line 504
    const/4 v0, 0x0

    .line 506
    .local v0, "payload":[B
    iget v1, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    if-ne v1, v3, :cond_1

    .line 509
    const/4 v1, 0x5

    new-array v0, v1, [B

    .line 510
    const/16 v1, 0x60

    aput-byte v1, v0, v4

    .line 511
    iget v1, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    int-to-byte v1, v1

    aput-byte v1, v0, v5

    .line 512
    iget-wide v1, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, v0, v6

    .line 513
    iget-byte v1, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionType:B

    aput-byte v1, v0, v3

    .line 514
    iget-byte v1, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_payloadType:B

    aput-byte v1, v0, v7

    .line 515
    const-string v1, "SAP/SAPFramingManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "composeOpenSessionMessage : _payloadType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 516
    iget-byte v3, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_payloadType:B

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 515
    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    :cond_0
    :goto_0
    return-object v0

    .line 525
    :cond_1
    iget v1, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    const/16 v2, 0x83

    if-ne v1, v2, :cond_0

    .line 526
    new-array v0, v7, [B

    .line 527
    const/16 v1, 0x60

    aput-byte v1, v0, v4

    .line 528
    iget v1, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    int-to-byte v1, v1

    aput-byte v1, v0, v5

    .line 529
    iget-wide v1, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, v0, v6

    .line 530
    iget-byte v1, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionType:B

    aput-byte v1, v0, v3

    goto :goto_0
.end method

.method public static composeProtocolFrame(Lcom/samsung/appcessory/base/SAPMessage;)[B
    .locals 9
    .param p0, "m"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    const/4 v4, 0x0

    .line 122
    const-string v5, "DEBUG"

    const-string v6, "DEBUG"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 123
    sget-boolean v5, Lcom/samsung/appcessory/protocol/SAPFramingManager;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-nez p0, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 126
    :cond_0
    const/4 v2, 0x0

    .line 127
    .local v2, "payload":[B
    const-wide/16 v5, 0xff

    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getSessionId()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-nez v5, :cond_2

    .line 128
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v2

    .line 137
    :goto_0
    array-length v5, v2

    .line 138
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getSessionId()J

    move-result-wide v6

    .line 137
    invoke-static {v5, v6, v7}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->formProtocolHeader(IJ)[B

    move-result-object v3

    .line 139
    .local v3, "rawProtocolFrame":[B
    if-nez v3, :cond_4

    .line 140
    const-string v5, "SAP/SAPFramingManager/29Dec2014"

    const-string/jumbo v6, "rawProtocolFrame is NULL"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    .line 174
    :cond_1
    :goto_1
    return-object v1

    .line 130
    .end local v3    # "rawProtocolFrame":[B
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getpayloadType()B

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 131
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v2

    .line 132
    goto :goto_0

    .line 133
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/appcessory/protocol/SAPFramingManager;->CHARSET:Ljava/nio/charset/Charset;

    invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    goto :goto_0

    .line 148
    .restart local v3    # "rawProtocolFrame":[B
    :cond_4
    const/4 v5, 0x0

    .line 149
    const/4 v6, 0x4

    :try_start_0
    array-length v7, v2

    .line 148
    invoke-static {v2, v5, v3, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :goto_2
    invoke-static {v3}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->addEscapeSequenceToFrame([B)[B

    move-result-object v1

    .line 170
    .local v1, "escapedProtocolFrame":[B
    if-nez v1, :cond_1

    .line 173
    const-string v5, "SAP/SAPFramingManager/29Dec2014"

    const-string v6, "escapedProtocolFrame is NULL"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    .line 174
    goto :goto_1

    .line 150
    .end local v1    # "escapedProtocolFrame":[B
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v5, "SAP/SAPFramingManager/29Dec2014"

    const-string v6, "Array index out of bound"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private static composeServiceAssociationMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B
    .locals 11
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    const/4 v4, 0x6

    const/4 v10, 0x2

    const/4 v5, 0x1

    const/4 v9, 0x0

    const-wide/16 v7, 0xff

    .line 566
    const/4 v0, 0x0

    .line 568
    .local v0, "payload":[B
    iget v3, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    if-ne v3, v4, :cond_1

    .line 569
    new-array v0, v4, [B

    .line 570
    const/16 v3, 0x60

    aput-byte v3, v0, v9

    .line 571
    iget v3, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    int-to-byte v3, v3

    aput-byte v3, v0, v5

    .line 574
    iget-wide v3, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_profileId:J

    const-wide/32 v5, 0xffffff

    and-long v1, v3, v5

    .line 578
    .local v1, "profileId":J
    const-wide/32 v3, 0xff0000

    and-long/2addr v3, v1

    const/16 v5, 0x10

    shr-long/2addr v3, v5

    and-long/2addr v3, v7

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v0, v10

    .line 579
    const/4 v3, 0x3

    const-wide/32 v4, 0xff00

    and-long/2addr v4, v1

    const/16 v6, 0x8

    shr-long/2addr v4, v6

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 580
    const/4 v3, 0x4

    and-long v4, v1, v7

    shr-long/2addr v4, v9

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 582
    const/4 v3, 0x5

    iget-wide v4, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 589
    .end local v1    # "profileId":J
    :cond_0
    :goto_0
    return-object v0

    .line 583
    :cond_1
    iget v3, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    const/16 v4, 0x86

    if-ne v3, v4, :cond_0

    .line 584
    new-array v0, v10, [B

    .line 585
    const/16 v3, 0x60

    aput-byte v3, v0, v9

    .line 586
    iget v3, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    int-to-byte v3, v3

    aput-byte v3, v0, v5

    goto :goto_0
.end method

.method private static composeTransportMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B
    .locals 6
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    .line 551
    iget-object v5, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_transportPayload:[B

    array-length v5, v5

    add-int/lit8 v4, v5, 0x2

    .line 552
    .local v4, "size":I
    const/4 v0, 0x0

    .line 553
    .local v0, "count":I
    new-array v3, v4, [B

    .line 555
    .local v3, "payload":[B
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    const/16 v5, 0x60

    aput-byte v5, v3, v0

    .line 556
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    iget v5, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    int-to-byte v5, v5

    aput-byte v5, v3, v1

    .line 557
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_transportPayload:[B

    array-length v5, v5

    if-lt v2, v5, :cond_0

    .line 561
    return-object v3

    .line 558
    :cond_0
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    iget-object v5, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_transportPayload:[B

    aget-byte v5, v5, v2

    aput-byte v5, v3, v0

    .line 557
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_0
.end method

.method private static formProtocolHeader(IJ)[B
    .locals 9
    .param p0, "payloadLength"    # I
    .param p1, "sessionId"    # J

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 660
    if-lez p0, :cond_0

    const-wide/16 v5, -0x1

    cmp-long v5, p1, v5

    if-nez v5, :cond_1

    .line 661
    :cond_0
    const-string v5, "SAP/SAPFramingManager/29Dec2014"

    const-string v6, "Invalid message parameters!"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    const/4 v3, 0x0

    .line 701
    :goto_0
    return-object v3

    .line 665
    :cond_1
    add-int/lit8 v5, p0, 0x4

    new-array v3, v5, [B

    .line 674
    .local v3, "protocolFrame":[B
    const/16 v5, 0x20

    aput-byte v5, v3, v8

    .line 680
    const/4 v5, 0x1

    long-to-int v6, p1

    int-to-byte v6, v6

    aput-byte v6, v3, v5

    .line 682
    new-array v2, v7, [B

    .line 683
    .local v2, "payloadLengthField":[B
    const/4 v1, 0x1

    .line 684
    .local v1, "fieldLength":I
    const/4 v4, 0x0

    .line 685
    .local v4, "rotate":I
    :goto_1
    if-gez v1, :cond_2

    .line 695
    const/4 v5, 0x0

    .line 696
    const/4 v6, 0x2

    .line 697
    const/4 v7, 0x2

    .line 695
    :try_start_0
    invoke-static {v2, v5, v3, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 698
    :catch_0
    move-exception v0

    .line 699
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v5, "SAP/SAPFramingManager/29Dec2014"

    const-string v6, "Array index out of bound"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 686
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_2
    shr-int v5, p0, v4

    int-to-byte v5, v5

    aput-byte v5, v2, v1

    .line 687
    add-int/lit8 v1, v1, -0x1

    .line 688
    add-int/lit8 v4, v4, 0x8

    goto :goto_1
.end method

.method private static getPayloadLength([B)S
    .locals 5
    .param p0, "buffer"    # [B

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 706
    const/4 v1, -0x1

    .line 707
    .local v1, "payloadLength":S
    new-array v2, v3, [B

    .line 709
    .local v2, "temp":[B
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 710
    .local v0, "b":Ljava/nio/ByteBuffer;
    invoke-static {p0, v3, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 713
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 714
    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 716
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    .line 717
    return v1
.end method

.method public static getSessionID([B)J
    .locals 4
    .param p0, "protocolFrame"    # [B

    .prologue
    const/4 v3, 0x2

    .line 859
    const-wide/16 v0, -0x1

    .line 860
    .local v0, "mSessionId":J
    array-length v2, p0

    if-le v2, v3, :cond_0

    .line 861
    aget-byte v2, p0, v3

    and-int/lit16 v2, v2, 0xff

    int-to-long v0, v2

    .line 863
    :cond_0
    return-wide v0
.end method

.method private static handleCapexService(Lcom/samsung/appcessory/protocol/SAPServiceParams;I[B)Z
    .locals 12
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .param p1, "serviceId"    # I
    .param p2, "payload"    # [B

    .prologue
    .line 325
    const/4 v8, 0x2

    if-ne p1, v8, :cond_3

    .line 326
    const/4 v0, 0x0

    .line 327
    .local v0, "count":I
    const/4 v8, 0x2

    new-array v8, v8, [B

    iput-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    .line 328
    iget-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    const/4 v9, 0x0

    const/4 v10, 0x2

    aget-byte v10, p2, v10

    aput-byte v10, v8, v9

    .line 329
    iget-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    const/4 v9, 0x1

    const/4 v10, 0x3

    aget-byte v10, p2, v10

    aput-byte v10, v8, v9

    .line 330
    const/4 v8, 0x2

    new-array v8, v8, [B

    iput-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    .line 331
    iget-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    const/4 v9, 0x0

    const/4 v10, 0x4

    aget-byte v10, p2, v10

    aput-byte v10, v8, v9

    .line 332
    iget-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    const/4 v9, 0x1

    const/4 v10, 0x5

    aget-byte v10, p2, v10

    aput-byte v10, v8, v9

    .line 333
    const/4 v8, 0x6

    aget-byte v8, p2, v8

    int-to-char v8, v8

    shl-int/lit8 v8, v8, 0x8

    const/4 v9, 0x7

    aget-byte v9, p2, v9

    and-int/lit16 v9, v9, 0xff

    add-int/2addr v8, v9

    int-to-char v8, v8

    iput-char v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numPayloadType:C

    .line 334
    const/16 v0, 0x8

    .line 335
    const-string v8, "SAP/SAPFramingManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "software version "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 336
    iget-object v10, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    const/4 v11, 0x1

    aget-byte v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 335
    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    const-string v8, "SAP/SAPFramingManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "proto version "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 338
    iget-object v10, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    const/4 v11, 0x1

    aget-byte v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 337
    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    const-string v8, "SAP/SAPFramingManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Number of payload types "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 341
    iget-char v10, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numPayloadType:C

    int-to-short v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 340
    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    iget-char v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numPayloadType:C

    new-array v8, v8, [B

    iput-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_supportedPayloadType:[B

    .line 343
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-char v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numPayloadType:C

    if-lt v2, v8, :cond_1

    .line 347
    aget-byte v8, p2, v0

    int-to-char v8, v8

    shl-int/lit8 v8, v8, 0x8

    add-int/lit8 v9, v0, 0x1

    aget-byte v9, p2, v9

    and-int/lit16 v9, v9, 0xff

    add-int/2addr v8, v9

    int-to-char v8, v8

    iput-char v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numSvc:C

    .line 348
    const-string v8, "SAP/SAPFramingManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Number of service types "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-char v10, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numSvc:C

    int-to-short v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    add-int/lit8 v0, v0, 0x2

    .line 350
    const/4 v3, 0x0

    .line 351
    .local v3, "j":I
    array-length v8, p2

    sub-int/2addr v8, v0

    new-array v7, v8, [B

    .line 352
    .local v7, "svcList":[B
    const-string v8, "SAP/SAPFramingManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Payload length "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v10, p2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " count "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    move v2, v0

    :goto_1
    array-length v8, p2

    if-lt v2, v8, :cond_2

    .line 356
    new-instance v5, Ljava/lang/String;

    sget-object v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->CHARSET:Ljava/nio/charset/Charset;

    invoke-direct {v5, v7, v8}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 357
    .local v5, "serviceList":Ljava/lang/String;
    const-string v8, "SAP/SAPFramingManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Services list"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 360
    .local v6, "services":[Ljava/lang/String;
    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_services:Ljava/util/List;

    .line 377
    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v5    # "serviceList":Ljava/lang/String;
    .end local v6    # "services":[Ljava/lang/String;
    .end local v7    # "svcList":[B
    :cond_0
    :goto_2
    const/4 v8, 0x1

    return v8

    .line 344
    .restart local v0    # "count":I
    .restart local v2    # "i":I
    :cond_1
    iget-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_supportedPayloadType:[B

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    aget-byte v9, p2, v0

    aput-byte v9, v8, v2

    .line 343
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto/16 :goto_0

    .line 354
    .restart local v3    # "j":I
    .restart local v7    # "svcList":[B
    :cond_2
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "j":I
    .local v4, "j":I
    aget-byte v8, p2, v2

    aput-byte v8, v7, v3

    .line 353
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_1

    .line 361
    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v7    # "svcList":[B
    :cond_3
    const/16 v8, 0x82

    if-ne p1, v8, :cond_0

    .line 362
    const/4 v8, 0x2

    aget-byte v8, p2, v8

    iput-byte v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_capexStatus:B

    .line 363
    const/4 v8, 0x2

    new-array v8, v8, [B

    iput-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    .line 364
    iget-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    const/4 v9, 0x0

    const/4 v10, 0x3

    aget-byte v10, p2, v10

    aput-byte v10, v8, v9

    .line 365
    iget-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    const/4 v9, 0x1

    const/4 v10, 0x4

    aget-byte v10, p2, v10

    aput-byte v10, v8, v9

    .line 366
    const/4 v8, 0x2

    new-array v8, v8, [B

    iput-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    .line 367
    iget-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    const/4 v9, 0x0

    const/4 v10, 0x5

    aget-byte v10, p2, v10

    aput-byte v10, v8, v9

    .line 368
    iget-object v8, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    const/4 v9, 0x1

    const/4 v10, 0x6

    aget-byte v10, p2, v10

    aput-byte v10, v8, v9

    .line 370
    const-string v8, "SAP/SAPFramingManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Resp software version "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 371
    iget-object v10, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    const/4 v11, 0x1

    aget-byte v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 370
    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    const-string v8, "SAP/SAPFramingManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Resp proto version "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 373
    iget-object v10, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    const/4 v11, 0x1

    aget-byte v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 372
    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    const-string v8, "SAP/SAPFramingManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Capex status "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v10, p0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_capexStatus:B

    int-to-short v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public static obtainCapexReqMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;
    .locals 4
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    .line 213
    const-string v2, "DEBUG"

    const-string v3, "DEBUG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    sget-boolean v2, Lcom/samsung/appcessory/protocol/SAPFramingManager;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 217
    :cond_0
    invoke-static {p0}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->composeCapexReqMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B

    move-result-object v1

    .line 219
    .local v1, "payload":[B
    new-instance v0, Lcom/samsung/appcessory/base/SAPMessage;

    const-wide/16 v2, 0xff

    invoke-direct {v0, v2, v3}, Lcom/samsung/appcessory/base/SAPMessage;-><init>(J)V

    .line 220
    .local v0, "m":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/base/SAPMessage;->setMessageBytes([B)V

    .line 222
    return-object v0
.end method

.method public static obtainCapexRspMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;
    .locals 4
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    .line 234
    const-string v2, "DEBUG"

    const-string v3, "DEBUG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 235
    sget-boolean v2, Lcom/samsung/appcessory/protocol/SAPFramingManager;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 238
    :cond_0
    invoke-static {p0}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->composeCapexRspMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B

    move-result-object v1

    .line 240
    .local v1, "payload":[B
    new-instance v0, Lcom/samsung/appcessory/base/SAPMessage;

    const-wide/16 v2, 0xff

    invoke-direct {v0, v2, v3}, Lcom/samsung/appcessory/base/SAPMessage;-><init>(J)V

    .line 241
    .local v0, "m":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/base/SAPMessage;->setMessageBytes([B)V

    .line 243
    return-object v0
.end method

.method public static obtainCloseSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;
    .locals 4
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    .line 258
    const-string v2, "DEBUG"

    const-string v3, "DEBUG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 259
    sget-boolean v2, Lcom/samsung/appcessory/protocol/SAPFramingManager;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 262
    :cond_0
    invoke-static {p0}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->composeCloseSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B

    move-result-object v1

    .line 264
    .local v1, "payload":[B
    new-instance v0, Lcom/samsung/appcessory/base/SAPMessage;

    const-wide/16 v2, 0xff

    invoke-direct {v0, v2, v3}, Lcom/samsung/appcessory/base/SAPMessage;-><init>(J)V

    .line 265
    .local v0, "m":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/base/SAPMessage;->setMessageBytes([B)V

    .line 267
    return-object v0
.end method

.method public static obtainOpenSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;
    .locals 4
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    .line 190
    const-string v2, "DEBUG"

    const-string v3, "DEBUG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 191
    sget-boolean v2, Lcom/samsung/appcessory/protocol/SAPFramingManager;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 194
    :cond_0
    invoke-static {p0}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->composeOpenSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B

    move-result-object v1

    .line 196
    .local v1, "payload":[B
    new-instance v0, Lcom/samsung/appcessory/base/SAPMessage;

    const-wide/16 v2, 0xff

    invoke-direct {v0, v2, v3}, Lcom/samsung/appcessory/base/SAPMessage;-><init>(J)V

    .line 197
    .local v0, "m":Lcom/samsung/appcessory/base/SAPMessage;
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 198
    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/base/SAPMessage;->setMessageBytes([B)V

    .line 201
    :cond_1
    return-object v0
.end method

.method public static obtainServiceAssociationMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;
    .locals 4
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    .line 285
    const-string v2, "DEBUG"

    const-string v3, "DEBUG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 286
    sget-boolean v2, Lcom/samsung/appcessory/protocol/SAPFramingManager;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 289
    :cond_0
    invoke-static {p0}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->composeServiceAssociationMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B

    move-result-object v1

    .line 291
    .local v1, "payload":[B
    new-instance v0, Lcom/samsung/appcessory/base/SAPMessage;

    const-wide/16 v2, 0xff

    invoke-direct {v0, v2, v3}, Lcom/samsung/appcessory/base/SAPMessage;-><init>(J)V

    .line 292
    .local v0, "m":Lcom/samsung/appcessory/base/SAPMessage;
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 293
    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/base/SAPMessage;->setMessageBytes([B)V

    .line 296
    :cond_1
    return-object v0
.end method

.method public static obtainTransportMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;J)Lcom/samsung/appcessory/base/SAPMessage;
    .locals 4
    .param p0, "params"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .param p1, "sessionId"    # J

    .prologue
    .line 311
    const-string v2, "DEBUG"

    const-string v3, "DEBUG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 312
    sget-boolean v2, Lcom/samsung/appcessory/protocol/SAPFramingManager;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 315
    :cond_0
    invoke-static {p0}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->composeTransportMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)[B

    move-result-object v1

    .line 317
    .local v1, "payload":[B
    new-instance v0, Lcom/samsung/appcessory/base/SAPMessage;

    invoke-direct {v0, p1, p2}, Lcom/samsung/appcessory/base/SAPMessage;-><init>(J)V

    .line 318
    .local v0, "m":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/base/SAPMessage;->setMessageBytes([B)V

    .line 320
    return-object v0
.end method

.method public static parseProtocolFrame([B)Lcom/samsung/appcessory/base/SAPMessage;
    .locals 10
    .param p0, "frame"    # [B

    .prologue
    .line 54
    const-string v8, "DEBUG"

    const-string v9, "DEBUG"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 55
    sget-boolean v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    if-nez p0, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 71
    :cond_0
    invoke-static {p0}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->stripEscapeSequenceFromFrame([B)[B

    move-result-object v4

    .line 82
    .local v4, "protocolFrame":[B
    const/4 v8, 0x1

    aget-byte v8, v4, v8

    and-int/lit16 v8, v8, 0xff

    int-to-long v6, v8

    .line 87
    .local v6, "sessionId":J
    invoke-static {v4}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->getPayloadLength([B)S

    move-result v3

    .line 92
    .local v3, "payloadLength":S
    new-array v2, v3, [B

    .line 95
    .local v2, "payload":[B
    const/4 v8, 0x4

    const/4 v9, 0x0

    .line 94
    :try_start_0
    invoke-static {v4, v8, v2, v9, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    new-instance v1, Lcom/samsung/appcessory/base/SAPMessage;

    invoke-direct {v1, v6, v7}, Lcom/samsung/appcessory/base/SAPMessage;-><init>(J)V

    .line 101
    .local v1, "m":Lcom/samsung/appcessory/base/SAPMessage;
    const-wide/16 v8, 0xff

    cmp-long v8, v8, v6

    if-nez v8, :cond_1

    .line 102
    invoke-virtual {v1, v2}, Lcom/samsung/appcessory/base/SAPMessage;->setMessageBytes([B)V

    .line 111
    :goto_1
    return-object v1

    .line 97
    .end local v1    # "m":Lcom/samsung/appcessory/base/SAPMessage;
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v8, "SAP/SAPFramingManager/29Dec2014"

    const-string v9, "Array index out of bound"

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 104
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v1    # "m":Lcom/samsung/appcessory/base/SAPMessage;
    :cond_1
    new-instance v5, Ljava/lang/String;

    sget-object v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->CHARSET:Ljava/nio/charset/Charset;

    invoke-direct {v5, v2, v8}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 107
    .local v5, "s":Ljava/lang/String;
    invoke-virtual {v1, v5}, Lcom/samsung/appcessory/base/SAPMessage;->setMessageBody(Ljava/lang/String;)V

    .line 108
    invoke-virtual {v1, v2}, Lcom/samsung/appcessory/base/SAPMessage;->setMessageBytes([B)V

    goto :goto_1
.end method

.method public static processAccessoryMessage(Lcom/samsung/appcessory/base/SAPMessage;)Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .locals 14
    .param p0, "m"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    .line 393
    const-string v10, "DEBUG"

    const-string v11, "DEBUG"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 394
    sget-boolean v10, Lcom/samsung/appcessory/protocol/SAPFramingManager;->$assertionsDisabled:Z

    if-nez v10, :cond_0

    if-nez p0, :cond_0

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 397
    :cond_0
    const/4 v1, 0x0

    .line 398
    .local v1, "payload":[B
    const-wide/16 v10, 0xff

    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getSessionId()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-nez v10, :cond_2

    .line 399
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v1

    .line 404
    :goto_0
    const/4 v10, 0x0

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xf0

    int-to-byte v3, v10

    .line 405
    .local v3, "pmm":B
    const/16 v10, 0x60

    if-ne v3, v10, :cond_9

    .line 406
    const/4 v6, 0x0

    .line 407
    .local v6, "serviceId":I
    const-wide/16 v4, 0x0

    .line 408
    .local v4, "profileId":J
    const-wide/16 v7, 0x0

    .line 409
    .local v7, "sessionId":J
    const/4 v9, 0x0

    .line 410
    .local v9, "sessionType":B
    const/4 v2, 0x0

    .line 411
    .local v2, "payloadType":B
    const-string v10, "SAP/SAPFramingManager/29Dec2014"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "EXEC cmd "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    new-instance v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;

    invoke-direct {v0}, Lcom/samsung/appcessory/protocol/SAPServiceParams;-><init>()V

    .line 415
    .local v0, "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    const/4 v10, 0x1

    aget-byte v10, v1, v10

    and-int/lit16 v6, v10, 0xff

    .line 416
    const-string v10, "SAP/SAPFramingManager/29Dec2014"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Service id "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    const/4 v10, 0x3

    if-ne v6, v10, :cond_3

    .line 418
    const/4 v10, 0x2

    aget-byte v9, v1, v10

    .line 419
    const/4 v10, 0x3

    aget-byte v2, v1, v10

    .line 471
    :cond_1
    :goto_1
    iput v6, v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    .line 472
    iput-wide v4, v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_profileId:J

    .line 473
    iput-wide v7, v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    .line 474
    iput-byte v9, v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionType:B

    .line 475
    iput-byte v2, v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_payloadType:B

    .line 483
    .end local v0    # "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .end local v2    # "payloadType":B
    .end local v4    # "profileId":J
    .end local v6    # "serviceId":I
    .end local v7    # "sessionId":J
    .end local v9    # "sessionType":B
    :goto_2
    return-object v0

    .line 401
    .end local v3    # "pmm":B
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/samsung/appcessory/protocol/SAPFramingManager;->CHARSET:Ljava/nio/charset/Charset;

    invoke-virtual {v10, v11}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    goto :goto_0

    .line 420
    .restart local v0    # "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .restart local v2    # "payloadType":B
    .restart local v3    # "pmm":B
    .restart local v4    # "profileId":J
    .restart local v6    # "serviceId":I
    .restart local v7    # "sessionId":J
    .restart local v9    # "sessionType":B
    :cond_3
    const/16 v10, 0x83

    if-ne v6, v10, :cond_4

    .line 421
    const/4 v10, 0x2

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    int-to-long v7, v10

    .line 422
    const/4 v10, 0x3

    aget-byte v9, v1, v10

    .line 423
    goto :goto_1

    :cond_4
    const/4 v10, 0x4

    if-eq v6, v10, :cond_5

    .line 424
    const/16 v10, 0x84

    if-ne v6, v10, :cond_6

    .line 425
    :cond_5
    const/4 v10, 0x2

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    int-to-long v7, v10

    .line 426
    goto :goto_1

    :cond_6
    const/4 v10, 0x2

    if-eq v6, v10, :cond_7

    .line 427
    const/16 v10, 0x82

    if-ne v6, v10, :cond_8

    .line 428
    :cond_7
    invoke-static {v0, v6, v1}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->handleCapexService(Lcom/samsung/appcessory/protocol/SAPServiceParams;I[B)Z

    goto :goto_1

    .line 429
    :cond_8
    const/4 v10, 0x6

    if-ne v6, v10, :cond_1

    .line 433
    const/4 v10, 0x2

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x10

    int-to-long v10, v10

    .line 434
    const/4 v12, 0x3

    aget-byte v12, v1, v12

    and-int/lit16 v12, v12, 0xff

    shl-int/lit8 v12, v12, 0x8

    int-to-long v12, v12

    .line 433
    add-long/2addr v10, v12

    .line 434
    const/4 v12, 0x4

    aget-byte v12, v1, v12

    and-int/lit16 v12, v12, 0xff

    shl-int/lit8 v12, v12, 0x0

    int-to-long v12, v12

    .line 433
    add-long v4, v10, v12

    .line 439
    const/4 v10, 0x5

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    int-to-long v7, v10

    goto :goto_1

    .line 479
    .end local v0    # "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .end local v2    # "payloadType":B
    .end local v4    # "profileId":J
    .end local v6    # "serviceId":I
    .end local v7    # "sessionId":J
    .end local v9    # "sessionType":B
    :cond_9
    const-string v10, "SAP/SAPFramingManager/29Dec2014"

    const-string v11, "Unsupported PMM received!"

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static stripEscapeSequenceFromFrame([B)[B
    .locals 11
    .param p0, "buffer"    # [B

    .prologue
    const/16 v10, 0x7d

    .line 792
    sget-object v8, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolderLock:Ljava/lang/Object;

    monitor-enter v8

    .line 793
    :try_start_0
    array-length v2, p0

    .line 794
    .local v2, "length":I
    const/4 v1, 0x0

    .line 795
    .local v1, "index":I
    const/4 v4, 0x0

    .line 796
    .local v4, "offset":I
    const/4 v3, 0x0

    .line 798
    .local v3, "nCharsEscaped":I
    sget-object v7, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 799
    :goto_0
    if-lt v1, v2, :cond_1

    .line 836
    if-eq v1, v4, :cond_0

    .line 840
    sget-object v7, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    sub-int v9, v1, v4

    invoke-virtual {v7, p0, v4, v9}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 845
    :cond_0
    array-length v7, p0

    sub-int v2, v7, v3

    .line 847
    new-array v5, v2, [B

    .line 848
    .local v5, "result":[B
    sget-object v7, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    .line 849
    .local v6, "temp":[B
    sget-object v7, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    .line 851
    .local v0, "arrayOffset":I
    const/4 v7, 0x0

    invoke-static {v6, v0, v5, v7, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 854
    monitor-exit v8

    return-object v5

    .line 800
    .end local v0    # "arrayOffset":I
    .end local v5    # "result":[B
    .end local v6    # "temp":[B
    :cond_1
    aget-byte v7, p0, v1

    if-ne v7, v10, :cond_2

    .line 803
    add-int/lit8 v7, v1, 0x1

    if-lt v7, v2, :cond_3

    .line 799
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 813
    :cond_3
    sget-object v7, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    sub-int v9, v1, v4

    invoke-virtual {v7, p0, v4, v9}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 814
    add-int/lit8 v7, v1, 0x1

    aget-byte v7, p0, v7

    const/16 v9, 0x5e

    if-ne v7, v9, :cond_4

    .line 817
    sget-object v7, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    const/16 v9, 0x7e

    invoke-virtual {v7, v9}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 829
    :goto_2
    add-int/lit8 v1, v1, 0x1

    .line 830
    add-int/lit8 v4, v1, 0x1

    .line 831
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 818
    :cond_4
    add-int/lit8 v7, v1, 0x1

    aget-byte v7, p0, v7

    const/16 v9, 0x5d

    if-ne v7, v9, :cond_5

    .line 821
    sget-object v7, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    const/16 v9, 0x7d

    invoke-virtual {v7, v9}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_2

    .line 792
    .end local v1    # "index":I
    .end local v2    # "length":I
    .end local v3    # "nCharsEscaped":I
    .end local v4    # "offset":I
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 824
    .restart local v1    # "index":I
    .restart local v2    # "length":I
    .restart local v3    # "nCharsEscaped":I
    .restart local v4    # "offset":I
    :cond_5
    :try_start_1
    sget-object v7, Lcom/samsung/appcessory/protocol/SAPFramingManager;->sHolder:Ljava/nio/ByteBuffer;

    const/16 v9, 0x7d

    invoke-virtual {v7, v9}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 825
    add-int/lit8 v4, v1, 0x1

    .line 826
    goto :goto_1
.end method
