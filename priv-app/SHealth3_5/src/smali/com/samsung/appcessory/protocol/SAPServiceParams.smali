.class public Lcom/samsung/appcessory/protocol/SAPServiceParams;
.super Ljava/lang/Object;
.source "SAPServiceParams.java"


# static fields
.field public static final SAP_SERVICE_ASSOCIATION_REQ:B = 0x6t

.field public static final SAP_SERVICE_ASSOCIATION_RES:I = 0x86

.field public static final SAP_SERVICE_CAPEX_REQ:I = 0x2

.field public static final SAP_SERVICE_CAPEX_RSP:I = 0x82

.field public static final SAP_SERVICE_ID_CLOSE_SESSION:B = 0x4t

.field public static final SAP_SERVICE_ID_OPEN_SESSION:B = 0x3t

.field public static final SAP_SERVICE_ID_SESSION_ACCEPTED:I = 0x83

.field public static final SAP_SERVICE_ID_SESSION_TERMINATED:I = 0x84

.field public static final SAP_SERVICE_ID_TRANSPORT:B = 0x1t


# instance fields
.field public _binaryPayload:[B

.field public _capexStatus:B

.field public _numPayloadType:C

.field public _numSvc:C

.field public _payloadType:B

.field public _profileId:J

.field public _protoVersion:[B

.field public _serviceId:I

.field public _services:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public _sessionId:J

.field public _sessionType:B

.field public _supportedPayloadType:[B

.field public _swVersion:[B

.field public _transportPayload:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
