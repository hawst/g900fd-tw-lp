.class public Lcom/samsung/appcessory/protocol/SAPFrameConstants;
.super Ljava/lang/Object;
.source "SAPFrameConstants.java"


# static fields
.field public static final SAP_ESCAPE_CHAR_REPLACEMENT:B = 0x5dt

.field public static final SAP_FRAME_MARKER_REPLACEMENT:B = 0x5et

.field public static final SAP_PAYLOAD_LENGTH_FIELD_WIDTH:B = 0x2t

.field public static final SAP_PROTOCOL_HEADER_LENGTH:B = 0x4t

.field public static final SAP_PROTOCOL_VERSION:B = 0x1t

.field public static final SAP_PROTOCOL_VERSION_BIT_OFFSET:B = 0x5t

.field public static final SAP_PROTOCOL_VERSION_MASK:I = 0xe0

.field public static final SAP_TCP_ESCAPE_CHARACTER:B = 0x7dt

.field public static final SAP_TCP_FRAME_MARKER:B = 0x7et

.field public static final SAP_TCP_PAYLOAD_LENGTH_OFFSET:I = 0x2

.field public static final SAP_TCP_PAYLOAD_OFFSET:I = 0x4

.field public static final SAP_TCP_PROTOCOL_VERSION_OFFSET:I = 0x0

.field public static final SAP_TCP_SESSION_ID_OFFSET:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
