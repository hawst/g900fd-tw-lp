.class Lcom/samsung/appcessory/transport/SAPConnectivityManager$1;
.super Ljava/lang/Object;
.source "SAPConnectivityManager.java"

# interfaces
.implements Lcom/samsung/appcessory/transport/IConnectionEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/appcessory/transport/SAPConnectivityManager;->openConnection(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$accessory:Lcom/samsung/appcessory/base/SAPBaseAccessory;


# direct methods
.method constructor <init>(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/appcessory/transport/SAPConnectivityManager$1;->val$accessory:Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(JI)V
    .locals 5
    .param p1, "mSessionID"    # J
    .param p3, "errCode"    # I

    .prologue
    .line 99
    const-string v2, "SAP/SAPConnectivityManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onError mSessionID="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "err="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 101
    .local v0, "m":Landroid/os/Message;
    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPConnectivityManager$1;->val$accessory:Lcom/samsung/appcessory/base/SAPBaseAccessory;

    iget-wide v2, v2, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    long-to-int v2, v2

    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 102
    long-to-int v2, p1

    iput v2, v0, Landroid/os/Message;->arg2:I

    .line 103
    sget v2, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->SAP_ERROR:I

    iput v2, v0, Landroid/os/Message;->what:I

    .line 104
    int-to-long v2, p3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 107
    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPConnectivityManager$1;->val$accessory:Lcom/samsung/appcessory/base/SAPBaseAccessory;

    iget-wide v2, v2, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v2, v3, p1, p2}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v1

    .line 109
    .local v1, "session":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v1, :cond_0

    .line 110
    invoke-virtual {v1}, Lcom/samsung/appcessory/session/SAPSession;->getHandler()Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->sendMessage(Landroid/os/Message;)Z

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_0
    const-string v2, "SAP/SAPConnectivityManager/29Dec2014"

    const-string v3, "OnError Invalid session ID!"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onMessageReceived(Lcom/samsung/appcessory/base/SAPMessage;)V
    .locals 6
    .param p1, "message"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    .line 76
    const-string v2, "SAP/SAPConnectivityManager/29Dec2014"

    .line 77
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">>> onMessageReceived: accessory ID: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 78
    iget-object v4, p0, Lcom/samsung/appcessory/transport/SAPConnectivityManager$1;->val$accessory:Lcom/samsung/appcessory/base/SAPBaseAccessory;

    iget-wide v4, v4, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 79
    const-string v4, "; session ID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 80
    invoke-virtual {p1}, Lcom/samsung/appcessory/base/SAPMessage;->getSessionId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 77
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 76
    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 82
    .local v0, "m":Landroid/os/Message;
    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPConnectivityManager$1;->val$accessory:Lcom/samsung/appcessory/base/SAPBaseAccessory;

    iget-wide v2, v2, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    long-to-int v2, v2

    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 83
    invoke-virtual {p1}, Lcom/samsung/appcessory/base/SAPMessage;->getSessionId()J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, v0, Landroid/os/Message;->arg2:I

    .line 84
    sget v2, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->SAP_MESSAGE:I

    iput v2, v0, Landroid/os/Message;->what:I

    .line 85
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 89
    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPConnectivityManager$1;->val$accessory:Lcom/samsung/appcessory/base/SAPBaseAccessory;

    iget-wide v2, v2, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    .line 90
    invoke-virtual {p1}, Lcom/samsung/appcessory/base/SAPMessage;->getSessionId()J

    move-result-wide v4

    .line 89
    invoke-static {v2, v3, v4, v5}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v1

    .line 91
    .local v1, "session":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v1, :cond_0

    .line 92
    invoke-virtual {v1}, Lcom/samsung/appcessory/session/SAPSession;->getHandler()Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->sendMessage(Landroid/os/Message;)Z

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_0
    const-string v2, "SAP/SAPConnectivityManager/29Dec2014"

    const-string v3, "Invalid session ID!"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
