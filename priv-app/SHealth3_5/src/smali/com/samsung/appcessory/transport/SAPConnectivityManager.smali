.class public Lcom/samsung/appcessory/transport/SAPConnectivityManager;
.super Ljava/lang/Object;
.source "SAPConnectivityManager.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static SAP_ERROR:I = 0x0

.field public static SAP_MESSAGE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SAP/SAPConnectivityManager/29Dec2014"

.field private static sConnectionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/appcessory/base/SAPConnection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    const-class v0, Lcom/samsung/appcessory/transport/SAPConnectivityManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->$assertionsDisabled:Z

    .line 40
    sput v2, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->SAP_MESSAGE:I

    .line 41
    sput v1, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->SAP_ERROR:I

    .line 157
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->sConnectionMap:Ljava/util/Map;

    .line 160
    return-void

    :cond_0
    move v0, v2

    .line 38
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static closeConnection(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
    .locals 4
    .param p0, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 134
    sget-object v1, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->sConnectionMap:Ljava/util/Map;

    iget-wide v2, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    sget-object v1, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->sConnectionMap:Ljava/util/Map;

    iget-wide v2, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPConnection;

    .line 136
    .local v0, "conn":Lcom/samsung/appcessory/base/SAPConnection;
    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPConnection;->close()V

    .line 141
    :cond_0
    sget-object v1, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->sConnectionMap:Ljava/util/Map;

    iget-wide v2, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    .end local v0    # "conn":Lcom/samsung/appcessory/base/SAPConnection;
    :cond_1
    return-void
.end method

.method public static getConnectionById(J)Lcom/samsung/appcessory/base/SAPConnection;
    .locals 3
    .param p0, "accessoryId"    # J

    .prologue
    .line 149
    sget-object v1, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->sConnectionMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    sget-object v1, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->sConnectionMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPConnection;

    .line 154
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static openConnection(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z
    .locals 7
    .param p0, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 66
    const/4 v2, 0x0

    .line 67
    .local v2, "ret":Z
    invoke-static {p0}, Lcom/samsung/appcessory/transport/SAPConnectionFactory;->getConnection(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Lcom/samsung/appcessory/base/SAPConnection;

    move-result-object v0

    .line 68
    .local v0, "conn":Lcom/samsung/appcessory/base/SAPConnection;
    const-string v3, "SAP/SAPConnectivityManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Now openingConnection ConectivityMgr=>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    if-eqz v0, :cond_0

    .line 72
    new-instance v1, Lcom/samsung/appcessory/transport/SAPConnectivityManager$1;

    invoke-direct {v1, p0}, Lcom/samsung/appcessory/transport/SAPConnectivityManager$1;-><init>(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V

    .line 117
    .local v1, "listener":Lcom/samsung/appcessory/transport/IConnectionEventListener;
    invoke-virtual {v0, p0, v1}, Lcom/samsung/appcessory/base/SAPConnection;->init(Lcom/samsung/appcessory/base/SAPBaseAccessory;Lcom/samsung/appcessory/transport/IConnectionEventListener;)V

    .line 118
    sget-object v3, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iget-object v4, v0, Lcom/samsung/appcessory/base/SAPConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    if-ne v3, v4, :cond_1

    .line 120
    sget-object v3, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->sConnectionMap:Ljava/util/Map;

    iget-wide v4, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    const/4 v2, 0x1

    .line 129
    .end local v1    # "listener":Lcom/samsung/appcessory/transport/IConnectionEventListener;
    :cond_0
    :goto_0
    return v2

    .line 124
    .restart local v1    # "listener":Lcom/samsung/appcessory/transport/IConnectionEventListener;
    :cond_1
    const-string v3, "SAP/SAPConnectivityManager/29Dec2014"

    const-string v4, "Invalid connection status! Returning ..."

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public sendToAccessory(JLcom/samsung/appcessory/base/SAPMessage;)V
    .locals 6
    .param p1, "accessoryId"    # J
    .param p3, "msg"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    .line 45
    sget-object v3, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->sConnectionMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 46
    invoke-virtual {p3}, Lcom/samsung/appcessory/base/SAPMessage;->getSessionId()J

    move-result-wide v1

    .line 47
    .local v1, "sessionId":J
    const-string v3, "SAP/SAPConnectivityManager/29Dec2014"

    .line 48
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ">>> sendToAccessory: accessory ID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 49
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; session ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 50
    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 47
    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    sget-object v3, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->sConnectionMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPConnection;

    .line 54
    .local v0, "conn":Lcom/samsung/appcessory/base/SAPConnection;
    const-string v3, "DEBUG"

    const-string v4, "DEBUG"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 55
    sget-boolean v3, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez v0, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 58
    :cond_0
    invoke-virtual {v0, p3}, Lcom/samsung/appcessory/base/SAPConnection;->write(Lcom/samsung/appcessory/base/SAPMessage;)V

    .line 60
    .end local v0    # "conn":Lcom/samsung/appcessory/base/SAPConnection;
    .end local v1    # "sessionId":J
    :cond_1
    return-void
.end method
