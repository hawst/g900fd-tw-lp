.class Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;
.super Ljava/lang/Object;
.source "SAPBtRfConnection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/transport/SAPBtRfConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SAPListenTask"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPListenTask-BTRF/29Dec2014"


# instance fields
.field private mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

.field final synthetic this$0:Lcom/samsung/appcessory/transport/SAPBtRfConnection;


# direct methods
.method constructor <init>(Lcom/samsung/appcessory/transport/SAPBtRfConnection;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;->this$0:Lcom/samsung/appcessory/transport/SAPBtRfConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 266
    const-string v1, "SAP/SAPListenTask-BTRF/29Dec2014"

    const-string v2, "check for read"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;->this$0:Lcom/samsung/appcessory/transport/SAPBtRfConnection;

    invoke-virtual {v1}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->read()Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v0

    .line 270
    .local v0, "msg":Lcom/samsung/appcessory/base/SAPMessage;
    if-eqz v0, :cond_1

    # getter for: Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapError:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    invoke-static {}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->access$0()Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    move-result-object v1

    sget-object v2, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    if-ne v1, v2, :cond_1

    .line 271
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;->mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

    invoke-interface {v1, v0}, Lcom/samsung/appcessory/transport/IConnectionEventListener;->onMessageReceived(Lcom/samsung/appcessory/base/SAPMessage;)V

    .line 282
    :cond_0
    :goto_0
    const-string v1, "SAP/SAPListenTask-BTRF/29Dec2014"

    const-string v2, "check for read exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    return-void

    .line 273
    :cond_1
    # getter for: Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapError:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    invoke-static {}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->access$0()Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    move-result-object v1

    sget-object v2, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_END_MARKER_NOT_FOUND:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    if-eq v1, v2, :cond_2

    .line 274
    # getter for: Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapError:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    invoke-static {}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->access$0()Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    move-result-object v1

    sget-object v2, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    if-ne v1, v2, :cond_0

    .line 276
    :cond_2
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;->this$0:Lcom/samsung/appcessory/transport/SAPBtRfConnection;

    # getter for: Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSessionId:J
    invoke-static {v1}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->access$1(Lcom/samsung/appcessory/transport/SAPBtRfConnection;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 278
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;->mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;->this$0:Lcom/samsung/appcessory/transport/SAPBtRfConnection;

    # getter for: Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSessionId:J
    invoke-static {v2}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->access$1(Lcom/samsung/appcessory/transport/SAPBtRfConnection;)J

    move-result-wide v2

    const/16 v4, 0x67

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/appcessory/transport/IConnectionEventListener;->onError(JI)V

    goto :goto_0
.end method

.method public setConnectionListener(Lcom/samsung/appcessory/transport/IConnectionEventListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/appcessory/transport/IConnectionEventListener;

    .prologue
    .line 260
    const-string v0, "SAP/SAPListenTask-BTRF/29Dec2014"

    const-string/jumbo v1, "setConnectionListener enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iput-object p1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;->mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

    .line 262
    const-string v0, "SAP/SAPListenTask-BTRF/29Dec2014"

    const-string/jumbo v1, "setConnectionListener exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    return-void
.end method
