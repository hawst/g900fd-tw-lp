.class public Lcom/samsung/appcessory/transport/SAPBtRfConnection;
.super Lcom/samsung/appcessory/base/SAPConnection;
.source "SAPBtRfConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;
    }
.end annotation


# static fields
.field public static final END_MARKER:B = 0x7et

.field public static final HEADER_LEN:I = 0x6

.field private static final LENGTH_INDEX:I = 0x4

.field private static final MAXIMUM_PAYLOAD_SIZE_IN_BYTES:I = 0x4000

.field private static final SAP_CONNECTION_LISTEN_POLL_INTERVAL:J = 0x64L

.field private static final SAP_LISTENER_TERMINATION_WAIT_TIME:J = 0xc8L

.field public static final SAP_UUID:Ljava/lang/String; = "a49eb41e-cb06-495c-9f4f-aa80a90cdf4a"

.field public static final START_MARKER:B = 0x7et

.field private static final TAG:Ljava/lang/String; = "SAP/SAPBtRfConnection/29Dec2014"

.field private static mSapError:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

.field private static mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;


# instance fields
.field private isStreamingData:Z

.field private listener:Ljava/util/concurrent/ScheduledExecutorService;

.field private mBtSocket:Landroid/bluetooth/BluetoothSocket;

.field private mInStream:Ljava/io/InputStream;

.field private mOutStream:Ljava/io/OutputStream;

.field private mSessionId:J

.field private mStrmDataLen:I

.field private packLen:I

.field sFragment:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 70
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPConnection;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    .line 66
    iput v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    .line 67
    iput v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->packLen:I

    .line 68
    iput-boolean v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->isStreamingData:Z

    .line 73
    const-string v0, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v1, "SAPBtRfConnection enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const-string v0, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v1, "SAPBtRfConnection exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    return-void
.end method

.method static synthetic access$0()Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapError:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/appcessory/transport/SAPBtRfConnection;)J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSessionId:J

    return-wide v0
.end method

.method private appendToFragment([B[B)[B
    .locals 5
    .param p1, "buffer"    # [B
    .param p2, "sFragment"    # [B

    .prologue
    const/4 v4, 0x0

    .line 408
    array-length v2, p2

    array-length v3, p1

    add-int/2addr v2, v3

    new-array v0, v2, [B

    .line 411
    .local v0, "holder":[B
    array-length v2, p2

    invoke-static {p2, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 414
    array-length v2, p2

    array-length v3, p1

    invoke-static {p1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 419
    invoke-static {v0}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->stripEscapeSequenceFromFrame([B)[B

    move-result-object v1

    .line 420
    .local v1, "newfragment":[B
    return-object v1
.end method

.method private printByteArray([BILjava/lang/String;)V
    .locals 7
    .param p1, "inData"    # [B
    .param p2, "len"    # I
    .param p3, "info"    # Ljava/lang/String;

    .prologue
    .line 424
    const-string v1, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "printByteArray enter"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 428
    const-string v1, "SAP/SAPBtRfConnection/29Dec2014"

    const-string/jumbo v2, "printByteArray exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    return-void

    .line 426
    :cond_0
    const-string v1, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "array["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%02X "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aget-byte v6, p1, v0

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 147
    const-string v1, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v2, "close enter"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 153
    :try_start_0
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0xc8

    .line 154
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 153
    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    .line 154
    if-nez v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 156
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :cond_0
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mInStream:Ljava/io/InputStream;

    if-eqz v1, :cond_1

    .line 169
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mInStream:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 170
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mInStream:Ljava/io/InputStream;

    .line 173
    :cond_1
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mOutStream:Ljava/io/OutputStream;

    if-eqz v1, :cond_2

    .line 175
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mOutStream:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 176
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mOutStream:Ljava/io/OutputStream;

    .line 178
    :cond_2
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mBtSocket:Landroid/bluetooth/BluetoothSocket;

    if-eqz v1, :cond_3

    .line 180
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mBtSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V

    .line 181
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mBtSocket:Landroid/bluetooth/BluetoothSocket;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 189
    :cond_3
    :goto_1
    sget-object v1, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_CLOSED:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 190
    const-string v1, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v2, "close exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "close InterruptedException"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    sget-object v1, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_INVALID:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 161
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 162
    iput-object v5, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    .line 163
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 183
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 184
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "close exception"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    sget-object v1, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_INVALID:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v1, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 186
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public handleReceivedMsg([BI)Lcom/samsung/appcessory/base/SAPMessage;
    .locals 10
    .param p1, "value2"    # [B
    .param p2, "length"    # I

    .prologue
    .line 291
    const/4 v0, 0x0

    .line 292
    .local v0, "endPackLen":I
    invoke-static {p1}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->stripEscapeSequenceFromFrame([B)[B

    move-result-object v5

    .line 293
    .local v5, "value1":[B
    new-array v4, p2, [B

    .line 294
    .local v4, "value":[B
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v4, v7, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 296
    sget-object v6, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapError:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    .line 298
    const/4 v6, 0x0

    aget-byte v6, v4, v6

    const/16 v7, 0x7e

    if-ne v6, v7, :cond_4

    iget-boolean v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->isStreamingData:Z

    if-nez v6, :cond_4

    .line 299
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Start Marker Found value.length="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v8, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Length="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 300
    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 299
    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    const/4 v6, 0x0

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 302
    invoke-static {v4}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->getSessionID([B)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSessionId:J

    .line 303
    array-length v6, v4

    const/4 v7, 0x4

    if-gt v6, v7, :cond_0

    .line 304
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->isStreamingData:Z

    .line 305
    const/4 v6, 0x0

    new-array v6, v6, [B

    iput-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    .line 306
    iget-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    invoke-direct {p0, v4, v6}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->appendToFragment([B[B)[B

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    .line 307
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    .line 308
    const/4 v6, 0x0

    .line 404
    :goto_0
    return-object v6

    .line 311
    :cond_0
    const/4 v6, 0x3

    aget-byte v6, v4, v6

    and-int/lit16 v1, v6, 0xff

    .line 312
    .local v1, "packLen0":I
    const/4 v6, 0x4

    aget-byte v6, v4, v6

    and-int/lit16 v2, v6, 0xff

    .line 314
    .local v2, "packLen1":I
    shl-int/lit8 v6, v1, 0x8

    or-int/2addr v6, v2

    iput v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->packLen:I

    .line 315
    iget v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->packLen:I

    add-int/lit8 v0, v6, 0x6

    .line 317
    array-length v6, v4

    if-le v0, v6, :cond_1

    .line 318
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Streaming Data"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->isStreamingData:Z

    .line 320
    const/4 v6, 0x0

    new-array v6, v6, [B

    iput-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    .line 321
    iget-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    invoke-direct {p0, v4, v6}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->appendToFragment([B[B)[B

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    .line 322
    iget v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->packLen:I

    iput v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    .line 323
    const/4 v6, 0x0

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 404
    .end local v1    # "packLen0":I
    .end local v2    # "packLen1":I
    :goto_1
    sget-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    goto :goto_0

    .line 325
    .restart local v1    # "packLen0":I
    .restart local v2    # "packLen1":I
    :cond_1
    add-int/lit8 v6, v0, -0x1

    array-length v7, v4

    if-lt v6, v7, :cond_2

    .line 326
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Array Index Crossing ..."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 327
    array-length v8, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 326
    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    const/4 v6, 0x0

    new-array v6, v6, [B

    iput-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    .line 329
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    .line 330
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->isStreamingData:Z

    .line 331
    const/4 v6, 0x0

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 332
    sget-object v6, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapError:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    .line 333
    const/4 v6, 0x0

    goto :goto_0

    .line 336
    :cond_2
    const/4 v3, 0x0

    .line 337
    .local v3, "stripedArray":[B
    add-int/lit8 v6, v0, -0x1

    aget-byte v6, v4, v6

    const/16 v7, 0x7e

    if-ne v6, v7, :cond_3

    .line 338
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "End Packet Marker found "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v8, v0, -0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    add-int/lit8 v6, v0, -0x2

    new-array v3, v6, [B

    .line 341
    const/4 v6, 0x1

    const/4 v7, 0x0

    add-int/lit8 v8, v0, -0x2

    invoke-static {v4, v6, v3, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 342
    invoke-static {v3}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->parseProtocolFrame([B)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v6

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 353
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->isStreamingData:Z

    goto :goto_1

    .line 344
    :cond_3
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v7, "Packet END Marker not found ERROR "

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    const/4 v6, 0x0

    new-array v6, v6, [B

    iput-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    .line 346
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    .line 347
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->isStreamingData:Z

    .line 348
    const/4 v6, 0x0

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 349
    sget-object v6, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_END_MARKER_NOT_FOUND:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapError:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    .line 350
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 356
    .end local v1    # "packLen0":I
    .end local v2    # "packLen1":I
    .end local v3    # "stripedArray":[B
    :cond_4
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v7, "Streaming Data Received"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    iget-boolean v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->isStreamingData:Z

    if-eqz v6, :cond_8

    .line 359
    iget-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    invoke-direct {p0, v4, v6}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->appendToFragment([B[B)[B

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    .line 360
    iget v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    if-nez v6, :cond_5

    .line 361
    iget-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    array-length v6, v6

    const/4 v7, 0x4

    if-le v6, v7, :cond_5

    .line 362
    iget-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    const/4 v7, 0x4

    aget-byte v6, v6, v7

    iput v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    .line 366
    :cond_5
    iget-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    array-length v6, v6

    iget v7, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    add-int/lit8 v7, v7, 0x6

    if-ge v6, v7, :cond_6

    .line 367
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v7, "Still Streaming Data not Completed"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    const/4 v6, 0x0

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 369
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 372
    :cond_6
    iget-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    iget v7, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    add-int/lit8 v7, v7, 0x6

    add-int/lit8 v7, v7, -0x1

    aget-byte v6, v6, v7

    const/16 v7, 0x7e

    if-ne v6, v7, :cond_7

    .line 373
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v7, "Streaming Data Receiving OVER"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    iget v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    add-int/lit8 v6, v6, 0x6

    add-int/lit8 v6, v6, -0x2

    new-array v3, v6, [B

    .line 376
    .restart local v3    # "stripedArray":[B
    iget-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget v9, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    add-int/lit8 v9, v9, 0x6

    add-int/lit8 v9, v9, -0x2

    invoke-static {v6, v7, v3, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 377
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "sFragment Data=> "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    array-length v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "stripedArray Data=> "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const/4 v6, 0x0

    new-array v6, v6, [B

    iput-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    .line 381
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    .line 382
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->isStreamingData:Z

    .line 385
    invoke-static {v3}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->parseProtocolFrame([B)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v6

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    goto/16 :goto_1

    .line 388
    .end local v3    # "stripedArray":[B
    :cond_7
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "All Data is received but No End Marker error "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "sFragment Data=> "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    array-length v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    const/4 v6, 0x0

    new-array v6, v6, [B

    iput-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->sFragment:[B

    .line 393
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mStrmDataLen:I

    .line 394
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->isStreamingData:Z

    .line 395
    sget-object v6, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_END_MARKER_NOT_FOUND:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapError:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    .line 396
    const/4 v6, 0x0

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    goto/16 :goto_1

    .line 399
    :cond_8
    const/4 v6, 0x0

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 400
    sget-object v6, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    sput-object v6, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mSapError:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    .line 401
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v7, "Invalid Data Received"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public init(Lcom/samsung/appcessory/base/SAPBaseAccessory;Lcom/samsung/appcessory/transport/IConnectionEventListener;)V
    .locals 12
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .param p2, "ll"    # Lcom/samsung/appcessory/transport/IConnectionEventListener;

    .prologue
    const-wide/16 v4, 0x64

    const-wide/16 v2, 0x0

    const/4 v11, 0x1

    .line 81
    const-string v0, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v6, "init enter"

    invoke-static {v0, v6}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v7

    .line 84
    .local v7, "btAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-eqz v7, :cond_0

    .line 85
    invoke-virtual {v7}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 88
    :cond_0
    const/4 v8, 0x0

    .line 90
    .local v8, "btRfAccessory":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    sget-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_UNKNOWN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 91
    instance-of v0, p1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    if-eqz v0, :cond_1

    move-object v8, p1

    .line 92
    check-cast v8, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .line 98
    iget-object v0, v8, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mBtSocket:Landroid/bluetooth/BluetoothSocket;

    if-eqz v0, :cond_2

    .line 99
    const-string v0, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v6, "SAPBtRfConnection already streams are found"

    invoke-static {v0, v6}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v0, v8, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mBtSocket:Landroid/bluetooth/BluetoothSocket;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mBtSocket:Landroid/bluetooth/BluetoothSocket;

    .line 101
    iget-object v0, v8, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mAccIntStream:Ljava/io/InputStream;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mInStream:Ljava/io/InputStream;

    .line 102
    iget-object v0, v8, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mAccOutStream:Ljava/io/OutputStream;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mOutStream:Ljava/io/OutputStream;

    .line 103
    sget-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 104
    new-instance v1, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;

    invoke-direct {v1, p0}, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;-><init>(Lcom/samsung/appcessory/transport/SAPBtRfConnection;)V

    .line 105
    .local v1, "task":Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;
    invoke-virtual {v1, p2}, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;->setConnectionListener(Lcom/samsung/appcessory/transport/IConnectionEventListener;)V

    .line 106
    invoke-static {v11}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    .line 107
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    .line 108
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 107
    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 142
    .end local v1    # "task":Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;
    :goto_0
    const-string v0, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v2, "init exit"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_1
    return-void

    .line 112
    :cond_2
    :try_start_0
    iget-object v0, v8, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 113
    const-string v2, "a49eb41e-cb06-495c-9f4f-aa80a90cdf4a"

    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v0

    .line 112
    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mBtSocket:Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 120
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mBtSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->connect()V

    .line 121
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mBtSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mInStream:Ljava/io/InputStream;

    .line 122
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mBtSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mOutStream:Ljava/io/OutputStream;

    .line 123
    sget-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 124
    new-instance v1, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;

    invoke-direct {v1, p0}, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;-><init>(Lcom/samsung/appcessory/transport/SAPBtRfConnection;)V

    .line 125
    .restart local v1    # "task":Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;
    invoke-virtual {v1, p2}, Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;->setConnectionListener(Lcom/samsung/appcessory/transport/IConnectionEventListener;)V

    .line 126
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    .line 127
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x64

    .line 128
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 127
    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 129
    .end local v1    # "task":Lcom/samsung/appcessory/transport/SAPBtRfConnection$SAPListenTask;
    :catch_0
    move-exception v9

    .line 130
    .local v9, "e":Ljava/io/IOException;
    const-string v0, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "connect exception "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :try_start_2
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mBtSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 138
    :goto_2
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 114
    .end local v9    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v9

    .line 115
    .restart local v9    # "e":Ljava/io/IOException;
    sget-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_INVALID:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 116
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 117
    const-string v0, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SAPBtRfConnection error e="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 133
    :catch_2
    move-exception v10

    .line 134
    .local v10, "e1":Ljava/io/IOException;
    sget-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_INVALID:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 135
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    .line 136
    const-string v0, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SAPBtRfConnection error"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public read()Lcom/samsung/appcessory/base/SAPMessage;
    .locals 9

    .prologue
    const/16 v8, 0x4000

    .line 230
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    const-string/jumbo v7, "read enter"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    new-array v0, v8, [B

    .line 232
    .local v0, "buf":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v8, :cond_0

    .line 235
    const/4 v5, 0x0

    .line 238
    .local v5, "readCount":I
    :try_start_0
    iget-object v6, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mInStream:Ljava/io/InputStream;

    invoke-virtual {v6, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    .line 245
    :goto_1
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "read "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " bytes"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    if-lez v5, :cond_1

    .line 247
    const-string v6, "**RECEIVED**"

    invoke-direct {p0, v0, v5, v6}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->printByteArray([BILjava/lang/String;)V

    .line 248
    const/4 v4, 0x0

    .line 249
    .local v4, "m":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual {p0, v0, v5}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->handleReceivedMsg([BI)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v4

    .line 250
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    const-string/jumbo v7, "read exit"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    .end local v4    # "m":Lcom/samsung/appcessory/base/SAPMessage;
    :goto_2
    return-object v4

    .line 233
    .end local v5    # "readCount":I
    :cond_0
    const/4 v6, 0x0

    aput-byte v6, v0, v2

    .line 232
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 239
    .restart local v5    # "readCount":I
    :catch_0
    move-exception v3

    .line 240
    .local v3, "ioe":Ljava/io/IOException;
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "read io exception"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->close()V

    goto :goto_1

    .line 242
    .end local v3    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 243
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "read exception"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 253
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v6, "SAP/SAPBtRfConnection/29Dec2014"

    const-string/jumbo v7, "read exit"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public write(Lcom/samsung/appcessory/base/SAPMessage;)V
    .locals 9
    .param p1, "message"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    const/16 v8, 0x7e

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 196
    const-string v3, "SAP/SAPBtRfConnection/29Dec2014"

    const-string v4, " write enter"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-virtual {p1, v7}, Lcom/samsung/appcessory/base/SAPMessage;->setpayloadType(B)V

    .line 199
    invoke-virtual {p1}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v4

    array-length v4, v4

    const-string v5, "**WRITING**"

    invoke-direct {p0, v3, v4, v5}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->printByteArray([BILjava/lang/String;)V

    .line 201
    invoke-static {p1}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->composeProtocolFrame(Lcom/samsung/appcessory/base/SAPMessage;)[B

    move-result-object v1

    .line 202
    .local v1, "protocolFrame":[B
    if-nez v1, :cond_0

    .line 203
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "protocolFrame is NULL"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 227
    :goto_0
    return-void

    .line 208
    :cond_0
    array-length v3, v1

    add-int/lit8 v3, v3, 0x2

    new-array v2, v3, [B

    .line 209
    .local v2, "tcpFrame":[B
    aput-byte v8, v2, v6

    .line 211
    array-length v3, v1

    invoke-static {v1, v6, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 212
    array-length v3, v1

    add-int/lit8 v3, v3, 0x1

    aput-byte v8, v2, v3

    .line 215
    :try_start_0
    array-length v3, v2

    const-string v4, "**WRITING**"

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->printByteArray([BILjava/lang/String;)V

    .line 216
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->mOutStream:Ljava/io/OutputStream;

    invoke-virtual {v3, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 226
    :goto_1
    const-string v3, "SAP/SAPBtRfConnection/29Dec2014"

    const-string/jumbo v4, "write exit"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;->close()V

    .line 219
    const-string v3, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "write io exception"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 221
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 222
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "SAP/SAPBtRfConnection/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "write exception"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
