.class Lcom/samsung/appcessory/transport/SAPUsbConnection$SAPListenTask;
.super Ljava/lang/Object;
.source "SAPUsbConnection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/transport/SAPUsbConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SAPListenTask"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPListenTask-USB/29Dec2014"


# instance fields
.field private mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

.field final synthetic this$0:Lcom/samsung/appcessory/transport/SAPUsbConnection;


# direct methods
.method constructor <init>(Lcom/samsung/appcessory/transport/SAPUsbConnection;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection$SAPListenTask;->this$0:Lcom/samsung/appcessory/transport/SAPUsbConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 185
    const-string v1, "SAP/SAPListenTask-USB/29Dec2014"

    const-string v2, "check for read"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection$SAPListenTask;->this$0:Lcom/samsung/appcessory/transport/SAPUsbConnection;

    invoke-virtual {v1}, Lcom/samsung/appcessory/transport/SAPUsbConnection;->read()Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v0

    .line 187
    .local v0, "msg":Lcom/samsung/appcessory/base/SAPMessage;
    if-eqz v0, :cond_0

    .line 188
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection$SAPListenTask;->mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

    invoke-interface {v1, v0}, Lcom/samsung/appcessory/transport/IConnectionEventListener;->onMessageReceived(Lcom/samsung/appcessory/base/SAPMessage;)V

    .line 192
    :goto_0
    const-string v1, "SAP/SAPListenTask-USB/29Dec2014"

    const-string v2, "check for read exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    return-void

    .line 190
    :cond_0
    const-string v1, "SAP/SAPListenTask-USB/29Dec2014"

    const-string v2, "check for read msg is null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setConnectionListener(Lcom/samsung/appcessory/transport/IConnectionEventListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/appcessory/transport/IConnectionEventListener;

    .prologue
    .line 179
    const-string v0, "SAP/SAPListenTask-USB/29Dec2014"

    const-string/jumbo v1, "setConnectionListener enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iput-object p1, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection$SAPListenTask;->mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

    .line 181
    const-string v0, "SAP/SAPListenTask-USB/29Dec2014"

    const-string/jumbo v1, "setConnectionListener exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    return-void
.end method
