.class Lcom/samsung/appcessory/transport/SAPBleconnection$1;
.super Ljava/lang/Object;
.source "SAPBleconnection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/appcessory/transport/SAPBleconnection;->startBleConnThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$appcessory$base$SAPAccessoryManager$ErrorCodes:[I


# instance fields
.field mSessionID:J

.field final synthetic this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$appcessory$base$SAPAccessoryManager$ErrorCodes()[I
    .locals 3

    .prologue
    .line 222
    sget-object v0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->$SWITCH_TABLE$com$samsung$appcessory$base$SAPAccessoryManager$ErrorCodes:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->values()[Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_END_MARKER_NOT_FOUND:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->$SWITCH_TABLE$com$samsung$appcessory$base$SAPAccessoryManager$ErrorCodes:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/samsung/appcessory/transport/SAPBleconnection;)V
    .locals 2

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    .line 222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 223
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->mSessionID:J

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 226
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    # getter for: Lcom/samsung/appcessory/transport/SAPBleconnection;->msgWaitLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/samsung/appcessory/transport/SAPBleconnection;->access$0(Lcom/samsung/appcessory/transport/SAPBleconnection;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 227
    :cond_0
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    # getter for: Lcom/samsung/appcessory/transport/SAPBleconnection;->mBleConnHnldMsg:Z
    invoke-static {v3}, Lcom/samsung/appcessory/transport/SAPBleconnection;->access$1(Lcom/samsung/appcessory/transport/SAPBleconnection;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 226
    monitor-exit v4

    .line 274
    return-void

    .line 228
    :cond_1
    const-string v3, "SAP/SAPBleconnection/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mBleConnHnldMsg "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    # getter for: Lcom/samsung/appcessory/transport/SAPBleconnection;->mBleConnHnldMsg:Z
    invoke-static {v6}, Lcom/samsung/appcessory/transport/SAPBleconnection;->access$1(Lcom/samsung/appcessory/transport/SAPBleconnection;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    :try_start_1
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    # getter for: Lcom/samsung/appcessory/transport/SAPBleconnection;->msgWaitLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/samsung/appcessory/transport/SAPBleconnection;->access$0(Lcom/samsung/appcessory/transport/SAPBleconnection;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    :goto_1
    :try_start_2
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    iget-object v3, v3, Lcom/samsung/appcessory/transport/SAPBleconnection;->gRawMessage:[B

    if-eqz v3, :cond_0

    .line 237
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    iget-object v5, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    iget-object v5, v5, Lcom/samsung/appcessory/transport/SAPBleconnection;->gRawMessage:[B

    # invokes: Lcom/samsung/appcessory/transport/SAPBleconnection;->handleReceivedData([B)Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    invoke-static {v3, v5}, Lcom/samsung/appcessory/transport/SAPBleconnection;->access$2(Lcom/samsung/appcessory/transport/SAPBleconnection;[B)Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    move-result-object v2

    .line 238
    .local v2, "err":Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    sget-object v3, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    if-eq v2, v3, :cond_2

    .line 240
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    iget-object v3, v3, Lcom/samsung/appcessory/transport/SAPBleconnection;->gRawMessage:[B

    invoke-static {v3}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->getSessionID([B)J

    move-result-wide v5

    .line 239
    iput-wide v5, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->mSessionID:J

    .line 243
    :cond_2
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    const/4 v5, 0x0

    iput-object v5, v3, Lcom/samsung/appcessory/transport/SAPBleconnection;->gRawMessage:[B

    .line 245
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    # getter for: Lcom/samsung/appcessory/transport/SAPBleconnection;->handleData:Z
    invoke-static {v3}, Lcom/samsung/appcessory/transport/SAPBleconnection;->access$3(Lcom/samsung/appcessory/transport/SAPBleconnection;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 246
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    iget-object v3, v3, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    if-eqz v3, :cond_3

    .line 247
    sget-object v3, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    if-ne v2, v3, :cond_3

    .line 249
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    iget-object v3, v3, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    invoke-virtual {v3}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v0

    .line 250
    .local v0, "blemsg":[B
    const-string v3, "SAP/SAPBleconnection/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "---------blemsg----------"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 251
    array-length v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 250
    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    invoke-virtual {v3, v0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->printByteArray([B)V

    .line 254
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    # getter for: Lcom/samsung/appcessory/transport/SAPBleconnection;->mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;
    invoke-static {v3}, Lcom/samsung/appcessory/transport/SAPBleconnection;->access$4(Lcom/samsung/appcessory/transport/SAPBleconnection;)Lcom/samsung/appcessory/transport/IConnectionEventListener;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    iget-object v5, v5, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    invoke-interface {v3, v5}, Lcom/samsung/appcessory/transport/IConnectionEventListener;->onMessageReceived(Lcom/samsung/appcessory/base/SAPMessage;)V

    .line 255
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    const/4 v5, 0x0

    iput-object v5, v3, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    goto/16 :goto_0

    .line 226
    .end local v0    # "blemsg":[B
    .end local v2    # "err":Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 232
    :catch_0
    move-exception v1

    .line 233
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 234
    const-string v3, "SAP/SAPBleconnection/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "wait exception "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 256
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v2    # "err":Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    :cond_3
    iget-wide v5, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->mSessionID:J

    const-wide/16 v7, -0x1

    cmp-long v3, v5, v7

    if-eqz v3, :cond_4

    .line 257
    invoke-static {}, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->$SWITCH_TABLE$com$samsung$appcessory$base$SAPAccessoryManager$ErrorCodes()[I

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 264
    const-string v3, "SAP/SAPBleconnection/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " err =>"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 260
    :pswitch_0
    const-string v3, "SAP/SAPBleconnection/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "err =>"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v3, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->this$0:Lcom/samsung/appcessory/transport/SAPBleconnection;

    # getter for: Lcom/samsung/appcessory/transport/SAPBleconnection;->mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;
    invoke-static {v3}, Lcom/samsung/appcessory/transport/SAPBleconnection;->access$4(Lcom/samsung/appcessory/transport/SAPBleconnection;)Lcom/samsung/appcessory/transport/IConnectionEventListener;

    move-result-object v3

    iget-wide v5, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->mSessionID:J

    const/16 v7, 0x67

    invoke-interface {v3, v5, v6, v7}, Lcom/samsung/appcessory/transport/IConnectionEventListener;->onError(JI)V

    goto/16 :goto_0

    .line 268
    :cond_4
    const-string v3, "SAP/SAPBleconnection/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error not sent to App mSessionID="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 269
    iget-wide v6, p0, Lcom/samsung/appcessory/transport/SAPBleconnection$1;->mSessionID:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 268
    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 257
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
