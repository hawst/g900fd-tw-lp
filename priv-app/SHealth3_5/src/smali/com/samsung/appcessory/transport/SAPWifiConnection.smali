.class public Lcom/samsung/appcessory/transport/SAPWifiConnection;
.super Lcom/samsung/appcessory/base/SAPConnection;
.source "SAPWifiConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/transport/SAPWifiConnection$SAPListenTask;
    }
.end annotation


# static fields
.field private static final MAXIMUM_PAYLOAD_SIZE_IN_BYTES:I = 0x10000

.field private static final SAP_CONNECTION_LISTEN_POLL_INTERVAL:J = 0x32L

.field private static final SAP_LISTENER_TERMINATION_WAIT_TIME:J = 0xc8L

.field private static final TAG:Ljava/lang/String; = "SAP/SAPWifiConnection/29Dec2014"

.field static sFragment:[B

.field static sHolder:[B

.field static sProtocolBuffer:Ljava/nio/ByteBuffer;


# instance fields
.field private client:Ljava/nio/channels/SocketChannel;

.field private connection:Ljava/nio/channels/SocketChannel;

.field private destinationHost:Ljava/lang/String;

.field private destinationPort:I

.field private input:Ljava/nio/channels/DatagramChannel;

.field listener:Ljava/util/concurrent/ScheduledExecutorService;

.field private mDescriptor:Ljava/net/DatagramSocket;

.field private output:Ljava/nio/channels/DatagramChannel;

.field private server:Ljava/nio/channels/ServerSocketChannel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x10000

    .line 486
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 485
    sput-object v0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sProtocolBuffer:Ljava/nio/ByteBuffer;

    .line 488
    new-array v0, v1, [B

    sput-object v0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sHolder:[B

    .line 490
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPConnection;-><init>()V

    .line 49
    return-void
.end method

.method private appendToFragment([B)V
    .locals 4
    .param p1, "buffer"    # [B

    .prologue
    const/4 v3, 0x0

    .line 410
    sget-object v1, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    array-length v1, v1

    array-length v2, p1

    add-int/2addr v1, v2

    new-array v0, v1, [B

    .line 413
    .local v0, "holder":[B
    sget-object v1, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    sget-object v2, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 416
    sget-object v1, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    array-length v1, v1

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 418
    array-length v1, v0

    new-array v1, v1, [B

    sput-object v1, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    .line 419
    sget-object v1, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 420
    return-void
.end method

.method private getProtocolFrame()[B
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/16 v10, 0x7e

    const/4 v9, 0x0

    .line 301
    sget-object v6, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iget-object v7, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    if-eq v6, v7, :cond_1

    .line 302
    const-string v6, "SAP/SAPWifiConnection/29Dec2014"

    const-string v7, "Invalid connection state"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :cond_0
    :goto_0
    return-object v4

    .line 306
    :cond_1
    const/4 v0, 0x0

    .line 308
    .local v0, "bFrameStartFound":Z
    const/4 v3, 0x0

    .local v3, "packetLengthInBytes":I
    :goto_1
    sget-object v6, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sHolder:[B

    array-length v6, v6

    if-lt v3, v6, :cond_2

    .line 361
    :goto_2
    add-int/lit8 v6, v3, -0x1

    new-array v4, v6, [B

    .line 362
    .local v4, "protocolFrame":[B
    sget-object v6, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sHolder:[B

    array-length v7, v4

    invoke-static {v6, v9, v4, v9, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 315
    .end local v4    # "protocolFrame":[B
    :cond_2
    sget-object v6, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    array-length v6, v6

    if-ne v3, v6, :cond_3

    .line 316
    const/4 v1, 0x0

    .line 319
    .local v1, "bytesRead":I
    :try_start_0
    iget-object v6, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->client:Ljava/nio/channels/SocketChannel;

    sget-object v7, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sProtocolBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v7}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 328
    :goto_3
    if-eqz v1, :cond_0

    .line 335
    sget-object v6, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sProtocolBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 336
    sget-object v6, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sProtocolBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    new-array v5, v6, [B

    .line 337
    .local v5, "received":[B
    sget-object v6, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sProtocolBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 340
    sget-object v6, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sProtocolBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 341
    invoke-direct {p0, v5}, Lcom/samsung/appcessory/transport/SAPWifiConnection;->appendToFragment([B)V

    .line 344
    .end local v1    # "bytesRead":I
    .end local v5    # "received":[B
    :cond_3
    sget-object v6, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    aget-byte v6, v6, v3

    if-ne v6, v10, :cond_4

    .line 345
    if-nez v0, :cond_4

    .line 346
    const/4 v0, 0x1

    .line 308
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 323
    .restart local v1    # "bytesRead":I
    :catch_0
    move-exception v2

    .line 324
    .local v2, "e":Ljava/io/IOException;
    sget-object v6, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_INVALID:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v6, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    goto :goto_3

    .line 348
    .end local v1    # "bytesRead":I
    .end local v2    # "e":Ljava/io/IOException;
    :cond_4
    sget-object v6, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    aget-byte v6, v6, v3

    if-ne v6, v10, :cond_5

    .line 349
    invoke-direct {p0, v3}, Lcom/samsung/appcessory/transport/SAPWifiConnection;->trimPartialFrame(I)V

    .line 350
    const/4 v0, 0x0

    .line 351
    goto :goto_2

    .line 356
    :cond_5
    sget-object v6, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sHolder:[B

    add-int/lit8 v7, v3, -0x1

    sget-object v8, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    aget-byte v8, v8, v3

    aput-byte v8, v6, v7

    goto :goto_4
.end method

.method private trimPartialFrame(I)V
    .locals 5
    .param p1, "idx"    # I

    .prologue
    const/4 v4, 0x0

    .line 423
    add-int/lit8 v1, p1, 0x1

    sget-object v2, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 426
    sget-object v1, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    array-length v1, v1

    add-int/lit8 v2, p1, 0x1

    sub-int/2addr v1, v2

    new-array v0, v1, [B

    .line 429
    .local v0, "holder":[B
    sget-object v1, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    add-int/lit8 v2, p1, 0x1

    array-length v3, v0

    invoke-static {v1, v2, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 432
    array-length v1, v0

    new-array v1, v1, [B

    sput-object v1, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    .line 435
    sget-object v1, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    array-length v2, v0

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 441
    .end local v0    # "holder":[B
    :goto_0
    return-void

    .line 438
    :cond_0
    new-array v1, v4, [B

    sput-object v1, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    goto :goto_0
.end method

.method private writeProtocolFrame([B)V
    .locals 8
    .param p1, "frame"    # [B

    .prologue
    const/16 v7, 0x7e

    const/4 v6, 0x0

    .line 369
    sget-object v4, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iget-object v5, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    if-eq v4, v5, :cond_1

    .line 370
    const-string v4, "SAP/SAPWifiConnection/29Dec2014"

    const-string v5, "Invalid connection state"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :cond_0
    return-void

    .line 374
    :cond_1
    array-length v4, p1

    add-int/lit8 v4, v4, 0x2

    new-array v3, v4, [B

    .line 378
    .local v3, "tcpFrame":[B
    aput-byte v7, v3, v6

    .line 380
    const/4 v4, 0x1

    array-length v5, p1

    invoke-static {p1, v6, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 381
    array-length v4, p1

    add-int/lit8 v4, v4, 0x1

    aput-byte v7, v3, v4

    .line 383
    array-length v4, v3

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 384
    .local v0, "buf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 385
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 386
    const/4 v1, 0x0

    .line 387
    .local v1, "bytesWritten":I
    :goto_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 390
    :try_start_0
    iget-object v4, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->client:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v4, v0}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    add-int/2addr v1, v4

    goto :goto_0

    .line 394
    :catch_0
    move-exception v2

    .line 395
    .local v2, "ioe":Ljava/io/IOException;
    sget-object v4, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_INVALID:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v4, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 6

    .prologue
    .line 164
    const-string v2, "SAP/SAPWifiConnection/29Dec2014"

    const-string v3, ">>> close()"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v2}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 169
    :try_start_0
    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v3, 0xc8

    .line 170
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 169
    invoke-interface {v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v2

    .line 170
    if-nez v2, :cond_0

    .line 171
    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v2}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :cond_0
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->client:Ljava/nio/channels/SocketChannel;

    if-eqz v2, :cond_1

    .line 195
    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->client:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->close()V

    .line 196
    sget-object v2, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_CLOSED:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v2, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 215
    :cond_1
    :goto_1
    return-void

    .line 173
    :catch_0
    move-exception v0

    .line 175
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v2}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 178
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 209
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 210
    .local v1, "ioe":Ljava/io/IOException;
    sget-object v2, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_INVALID:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v2, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 211
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public init(Lcom/samsung/appcessory/base/SAPBaseAccessory;Lcom/samsung/appcessory/transport/IConnectionEventListener;)V
    .locals 10
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .param p2, "ll"    # Lcom/samsung/appcessory/transport/IConnectionEventListener;

    .prologue
    const-wide/16 v4, 0x32

    .line 53
    const-string v0, "SAP/SAPWifiConnection/29Dec2014"

    const-string v2, ">>> init()"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const/4 v9, 0x0

    .line 56
    .local v9, "sapWifiAccessory":Lcom/samsung/appcessory/base/SAPWifiAccessory;
    instance-of v0, p1, Lcom/samsung/appcessory/base/SAPWifiAccessory;

    if-eqz v0, :cond_0

    move-object v9, p1

    .line 57
    check-cast v9, Lcom/samsung/appcessory/base/SAPWifiAccessory;

    .line 94
    :try_start_0
    const-string v0, "SAP/SAPWifiConnection/29Dec2014"

    const-string v2, "Trying to esthablish"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    sget-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_UNKNOWN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 100
    invoke-virtual {v9}, Lcom/samsung/appcessory/base/SAPWifiAccessory;->getOutputPort()I

    move-result v0

    .line 99
    iput v0, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->destinationPort:I

    .line 101
    invoke-virtual {v9}, Lcom/samsung/appcessory/base/SAPWifiAccessory;->getHost()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->destinationHost:Ljava/lang/String;

    .line 105
    new-instance v8, Ljava/net/InetSocketAddress;

    .line 106
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->destinationHost:Ljava/lang/String;

    iget v2, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->destinationPort:I

    .line 105
    invoke-direct {v8, v0, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 107
    .local v8, "remoteAddr":Ljava/net/SocketAddress;
    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->client:Ljava/nio/channels/SocketChannel;

    .line 108
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->client:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v8}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    .line 111
    sget-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 112
    const-string v0, "SAP/SAPWifiConnection/29Dec2014"

    const-string v2, "Connection established"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    .end local v8    # "remoteAddr":Ljava/net/SocketAddress;
    :goto_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->sFragment:[B

    .line 149
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    .line 151
    new-instance v1, Lcom/samsung/appcessory/transport/SAPWifiConnection$SAPListenTask;

    invoke-direct {v1, p0}, Lcom/samsung/appcessory/transport/SAPWifiConnection$SAPListenTask;-><init>(Lcom/samsung/appcessory/transport/SAPWifiConnection;)V

    .line 152
    .local v1, "task":Lcom/samsung/appcessory/transport/SAPWifiConnection$SAPListenTask;
    invoke-virtual {v1, p2}, Lcom/samsung/appcessory/transport/SAPWifiConnection$SAPListenTask;->setConnectionListener(Lcom/samsung/appcessory/transport/IConnectionEventListener;)V

    .line 154
    const-string v0, "SAP/SAPWifiConnection/29Dec2014"

    .line 155
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting the listen poll interval to: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 156
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 157
    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 155
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 154
    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x0

    .line 159
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 158
    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 160
    .end local v1    # "task":Lcom/samsung/appcessory/transport/SAPWifiConnection$SAPListenTask;
    :cond_0
    return-void

    .line 137
    :catch_0
    move-exception v7

    .line 138
    .local v7, "ioe":Ljava/io/IOException;
    sget-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_INVALID:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 139
    const-string v0, "SAP/SAPWifiConnection/29Dec2014"

    const-string v2, "Connect failed!"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const-string v0, "SAP/SAPWifiConnection/29Dec2014"

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public read()Lcom/samsung/appcessory/base/SAPMessage;
    .locals 2

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/samsung/appcessory/transport/SAPWifiConnection;->getProtocolFrame()[B

    move-result-object v1

    .line 252
    .local v1, "protocolFrame":[B
    if-eqz v1, :cond_0

    .line 256
    invoke-static {v1}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->parseProtocolFrame([B)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v0

    .line 261
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public write(Lcom/samsung/appcessory/base/SAPMessage;)V
    .locals 3
    .param p1, "message"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    .line 288
    invoke-static {p1}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->composeProtocolFrame(Lcom/samsung/appcessory/base/SAPMessage;)[B

    move-result-object v0

    .line 291
    .local v0, "protocolFrame":[B
    if-nez v0, :cond_0

    .line 292
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v2, "protocolFrame is NULL"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 297
    :goto_0
    return-void

    .line 295
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/appcessory/transport/SAPWifiConnection;->writeProtocolFrame([B)V

    goto :goto_0
.end method
