.class Lcom/samsung/appcessory/transport/SAPWifiConnection$SAPListenTask;
.super Ljava/lang/Object;
.source "SAPWifiConnection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/transport/SAPWifiConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SAPListenTask"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPListenTask/29Dec2014"


# instance fields
.field private mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

.field final synthetic this$0:Lcom/samsung/appcessory/transport/SAPWifiConnection;


# direct methods
.method constructor <init>(Lcom/samsung/appcessory/transport/SAPWifiConnection;)V
    .locals 0

    .prologue
    .line 443
    iput-object p1, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection$SAPListenTask;->this$0:Lcom/samsung/appcessory/transport/SAPWifiConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 451
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection$SAPListenTask;->this$0:Lcom/samsung/appcessory/transport/SAPWifiConnection;

    invoke-virtual {v1}, Lcom/samsung/appcessory/transport/SAPWifiConnection;->read()Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v0

    .line 452
    .local v0, "msg":Lcom/samsung/appcessory/base/SAPMessage;
    if-eqz v0, :cond_0

    .line 453
    const-string v1, "SAP/SAPListenTask/29Dec2014"

    const-string v2, "You\'ve got mail!"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection$SAPListenTask;->mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

    invoke-interface {v1, v0}, Lcom/samsung/appcessory/transport/IConnectionEventListener;->onMessageReceived(Lcom/samsung/appcessory/base/SAPMessage;)V

    .line 456
    :cond_0
    return-void
.end method

.method public setConnectionListener(Lcom/samsung/appcessory/transport/IConnectionEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/appcessory/transport/IConnectionEventListener;

    .prologue
    .line 446
    iput-object p1, p0, Lcom/samsung/appcessory/transport/SAPWifiConnection$SAPListenTask;->mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

    .line 447
    return-void
.end method
