.class public Lcom/samsung/appcessory/transport/SAPBleconnection;
.super Lcom/samsung/appcessory/base/SAPConnection;
.source "SAPBleconnection.java"


# static fields
.field private static final CHARSET:Ljava/nio/charset/Charset;

.field private static final CMD_INDEX:I = 0x5

.field private static final ENCODING_FORMAT:Ljava/lang/String; = "UTF-8"

.field public static final END_MARKER:B = 0x7et

.field public static final HEADER_LEN:I = 0x6

.field private static final LENGTH_INDEX:I = 0x4

.field private static final MAXIMUM_PAYLOAD_SIZE_IN_BYTES:I = 0x10000

.field public static final START_MARKER:B = 0x7et

.field private static final TAG:Ljava/lang/String; = "SAP/SAPBleconnection/29Dec2014"

.field private static final mLock:Ljava/lang/Object;

.field private static mSelfRef:Lcom/samsung/appcessory/transport/SAPBleconnection;

.field static sFragment:[B


# instance fields
.field gRawMessage:[B

.field gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

.field private handleData:Z

.field private isStreamingData:Z

.field private mBleConnHnldMsg:Z

.field private mBleConnThread:Ljava/lang/Thread;

.field private mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

.field private mSapBleAccManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

.field private mStrmDataLen:I

.field private final msgWaitLock:Ljava/lang/Object;

.field private packLen:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mLock:Ljava/lang/Object;

    .line 47
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    .line 50
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/samsung/appcessory/transport/SAPBleconnection;->CHARSET:Ljava/nio/charset/Charset;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPConnection;-><init>()V

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->msgWaitLock:Ljava/lang/Object;

    .line 32
    iput-boolean v1, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->handleData:Z

    .line 35
    iput-object v2, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 36
    iput-object v2, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gRawMessage:[B

    .line 44
    iput v1, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->packLen:I

    .line 45
    iput-boolean v1, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->isStreamingData:Z

    .line 48
    iput v1, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    .line 53
    const-string v0, "SAP/SAPBleconnection/29Dec2014"

    const-string v1, "SAPBleconnection enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    sget-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 55
    const-string v0, "SAP/SAPBleconnection/29Dec2014"

    const-string v1, "SAPBleconnection exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/appcessory/transport/SAPBleconnection;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->msgWaitLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/appcessory/transport/SAPBleconnection;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mBleConnHnldMsg:Z

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/appcessory/transport/SAPBleconnection;[B)Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    .locals 1

    .prologue
    .line 366
    invoke-direct {p0, p1}, Lcom/samsung/appcessory/transport/SAPBleconnection;->handleReceivedData([B)Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/appcessory/transport/SAPBleconnection;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->handleData:Z

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/appcessory/transport/SAPBleconnection;)Lcom/samsung/appcessory/transport/IConnectionEventListener;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

    return-object v0
.end method

.method private appendToFragment([B)V
    .locals 4
    .param p1, "buffer"    # [B

    .prologue
    const/4 v3, 0x0

    .line 110
    sget-object v1, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    array-length v1, v1

    array-length v2, p1

    add-int/2addr v1, v2

    new-array v0, v1, [B

    .line 113
    .local v0, "holder":[B
    sget-object v1, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    sget-object v2, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 116
    sget-object v1, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    array-length v1, v1

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 120
    invoke-static {v0}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->stripEscapeSequenceFromFrame([B)[B

    move-result-object v1

    sput-object v1, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    .line 121
    return-void
.end method

.method public static getInstance()Lcom/samsung/appcessory/transport/SAPBleconnection;
    .locals 3

    .prologue
    .line 60
    const-string v0, "SAP/SAPBleconnection/29Dec2014"

    const-string v1, "getInstance enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    sget-object v1, Lcom/samsung/appcessory/transport/SAPBleconnection;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 62
    :try_start_0
    sget-object v0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mSelfRef:Lcom/samsung/appcessory/transport/SAPBleconnection;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lcom/samsung/appcessory/transport/SAPBleconnection;

    invoke-direct {v0}, Lcom/samsung/appcessory/transport/SAPBleconnection;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mSelfRef:Lcom/samsung/appcessory/transport/SAPBleconnection;

    .line 65
    :cond_0
    const-string v0, "SAP/SAPBleconnection/29Dec2014"

    const-string v2, "getInstance exit"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    sget-object v0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mSelfRef:Lcom/samsung/appcessory/transport/SAPBleconnection;

    monitor-exit v1

    return-object v0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private handleReceivedData([B)Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    .locals 14
    .param p1, "value2"    # [B

    .prologue
    const/16 v13, 0x7e

    const/4 v12, 0x1

    const/4 v9, 0x4

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 367
    const/4 v1, 0x0

    .line 368
    .local v1, "endPackLen":I
    invoke-static {p1}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->stripEscapeSequenceFromFrame([B)[B

    move-result-object v6

    .line 370
    .local v6, "value1":[B
    array-length v7, v6

    new-array v5, v7, [B

    .line 371
    .local v5, "value":[B
    array-length v7, v6

    invoke-static {v6, v11, v5, v11, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 372
    iput-object v10, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 374
    aget-byte v7, v5, v11

    if-ne v7, v13, :cond_5

    iget-boolean v7, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->isStreamingData:Z

    if-nez v7, :cond_5

    .line 375
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    const-string v8, "Start Marker Found "

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    array-length v7, v5

    if-gt v7, v9, :cond_0

    .line 379
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->resetReadValues()V

    .line 380
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    .line 514
    :goto_0
    return-object v7

    .line 383
    :cond_0
    const/4 v7, 0x3

    aget-byte v7, v5, v7

    and-int/lit16 v2, v7, 0xff

    .line 384
    .local v2, "packLen0":I
    aget-byte v7, v5, v9

    and-int/lit16 v3, v7, 0xff

    .line 388
    .local v3, "packLen1":I
    shl-int/lit8 v7, v2, 0x8

    or-int/2addr v7, v3

    iput v7, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->packLen:I

    .line 389
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Total Data Len=>"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->packLen:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    iget v7, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->packLen:I

    add-int/lit8 v1, v7, 0x6

    .line 392
    array-length v7, v5

    if-le v1, v7, :cond_2

    .line 393
    array-length v7, v5

    const/16 v8, 0x14

    if-ge v7, v8, :cond_1

    .line 394
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    const-string v8, "Error: Not Compelete packet return"

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->resetReadValues()V

    .line 396
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    goto :goto_0

    .line 398
    :cond_1
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Streaming Data"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iput-boolean v12, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->isStreamingData:Z

    .line 400
    new-array v7, v11, [B

    sput-object v7, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    .line 401
    invoke-direct {p0, v5}, Lcom/samsung/appcessory/transport/SAPBleconnection;->appendToFragment([B)V

    .line 402
    iget v7, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->packLen:I

    iput v7, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    .line 403
    iput-object v10, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 404
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    goto :goto_0

    .line 407
    :cond_2
    add-int/lit8 v7, v1, -0x1

    array-length v8, v5

    if-lt v7, v8, :cond_3

    .line 408
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Array Index Crossing plz Check...loosing Data "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 409
    array-length v9, v5

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 408
    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->resetReadValues()V

    .line 412
    iput-object v10, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 413
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    goto/16 :goto_0

    .line 416
    :cond_3
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Normal Packet endPackLen= "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " value.length="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 417
    array-length v9, v5

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 416
    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    const/4 v4, 0x0

    .line 419
    .local v4, "stripedArray":[B
    add-int/lit8 v7, v1, -0x1

    aget-byte v7, v5, v7

    if-ne v7, v13, :cond_4

    .line 420
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "End Packet Marker found "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v9, v1, -0x1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    add-int/lit8 v7, v1, -0x2

    new-array v4, v7, [B

    .line 424
    const/4 v7, 0x1

    const/4 v8, 0x0

    add-int/lit8 v9, v1, -0x2

    :try_start_0
    invoke-static {v5, v7, v4, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 436
    :goto_1
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "---------stripedArray----------"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v9, v4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    invoke-virtual {p0, v4}, Lcom/samsung/appcessory/transport/SAPBleconnection;->printByteArray([B)V

    .line 440
    invoke-static {v4}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->parseProtocolFrame([B)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 442
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->resetReadValues()V

    .line 443
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    goto/16 :goto_0

    .line 425
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    const-string v8, "Array index out of bound"

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 429
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_4
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    const-string v8, "Packet END Marker not found ERROR "

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->resetReadValues()V

    .line 433
    iput-object v10, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 434
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    goto/16 :goto_0

    .line 446
    .end local v2    # "packLen0":I
    .end local v3    # "packLen1":I
    .end local v4    # "stripedArray":[B
    :cond_5
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    const-string v8, "Streaming Data Received"

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    iget-boolean v7, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->isStreamingData:Z

    if-eqz v7, :cond_a

    .line 449
    invoke-direct {p0, v5}, Lcom/samsung/appcessory/transport/SAPBleconnection;->appendToFragment([B)V

    .line 450
    iget v7, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    if-nez v7, :cond_6

    .line 451
    sget-object v7, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    array-length v7, v7

    if-le v7, v9, :cond_6

    .line 452
    sget-object v7, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    aget-byte v7, v7, v9

    iput v7, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    .line 457
    :cond_6
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "mStrmDataLen+HEADER_LEN =>"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 458
    iget v9, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    add-int/lit8 v9, v9, 0x6

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 457
    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "sFragment.length =>"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v9, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    array-length v9, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    sget-object v7, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    array-length v7, v7

    iget v8, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    add-int/lit8 v8, v8, 0x6

    if-ge v7, v8, :cond_8

    .line 463
    array-length v7, v5

    const/16 v8, 0x14

    if-ge v7, v8, :cond_7

    .line 464
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    const-string v8, "Error:1  Not Compelete packet return"

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->resetReadValues()V

    .line 466
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    goto/16 :goto_0

    .line 468
    :cond_7
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    const-string v8, "Still Streaming Data not Completed"

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iput-object v10, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 470
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    goto/16 :goto_0

    .line 475
    :cond_8
    sget-object v7, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    iget v8, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    add-int/lit8 v8, v8, 0x6

    add-int/lit8 v8, v8, -0x1

    aget-byte v7, v7, v8

    if-ne v7, v13, :cond_9

    .line 476
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    const-string v8, "Streaming Data Receiving OVER"

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "sFragment len="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v9, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    array-length v9, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mStrmDataLen=>"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 479
    iget v9, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 478
    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "---------FragmentData----------"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v9, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    array-length v9, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    iget v7, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    add-int/lit8 v7, v7, 0x6

    add-int/lit8 v7, v7, -0x2

    new-array v4, v7, [B

    .line 483
    .restart local v4    # "stripedArray":[B
    :try_start_1
    sget-object v7, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget v10, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    add-int/lit8 v10, v10, 0x6

    .line 484
    add-int/lit8 v10, v10, -0x2

    .line 483
    invoke-static {v7, v8, v4, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    .line 489
    :goto_2
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "sFragment Data=> "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v9, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    array-length v9, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    sget-object v7, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    invoke-virtual {p0, v7}, Lcom/samsung/appcessory/transport/SAPBleconnection;->printByteArray([B)V

    .line 492
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "stripedArray Data=> "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v9, v4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    invoke-virtual {p0, v4}, Lcom/samsung/appcessory/transport/SAPBleconnection;->printByteArray([B)V

    .line 495
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->resetReadValues()V

    .line 496
    invoke-static {v4}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->parseProtocolFrame([B)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 497
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    goto/16 :goto_0

    .line 485
    :catch_1
    move-exception v0

    .line 486
    .restart local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    const-string v8, "Array index out of bound"

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 500
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .end local v4    # "stripedArray":[B
    :cond_9
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "All Data is received but No End Marker error "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 501
    iget v9, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 500
    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "sFragment Data=> "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v9, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    array-length v9, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    sget-object v7, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    invoke-virtual {p0, v7}, Lcom/samsung/appcessory/transport/SAPBleconnection;->printByteArray([B)V

    .line 506
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->resetReadValues()V

    .line 507
    iput-object v10, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 508
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_END_MARKER_NOT_FOUND:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    goto/16 :goto_0

    .line 511
    :cond_a
    iput-object v10, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 512
    const-string v7, "SAP/SAPBleconnection/29Dec2014"

    const-string v8, "Invalid Data Received"

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->resetReadValues()V

    .line 514
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    goto/16 :goto_0
.end method

.method public static processAccessoryMessage(Lcom/samsung/appcessory/base/SAPMessage;)Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .locals 14
    .param p0, "m"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    .line 282
    const-string v10, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v11, "processAccessoryMessage Enter"

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    const/4 v1, 0x0

    .line 285
    .local v1, "payload":[B
    const-wide/16 v10, 0xff

    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getSessionId()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-nez v10, :cond_1

    .line 286
    const-string v10, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v11, "processAccessoryMessage Enter:1"

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v1

    .line 298
    :goto_0
    const-string v10, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v11, "processAccessoryMessage Enter:4"

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    const/4 v10, 0x0

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xf0

    int-to-byte v3, v10

    .line 300
    .local v3, "pmm":B
    const/16 v10, 0x60

    if-ne v3, v10, :cond_7

    .line 301
    const/4 v6, 0x0

    .line 302
    .local v6, "serviceId":I
    const-wide/16 v4, 0x0

    .line 303
    .local v4, "profileId":J
    const-wide/16 v7, 0x0

    .line 304
    .local v7, "sessionId":J
    const/4 v9, 0x0

    .line 305
    .local v9, "sessionType":B
    const/4 v2, 0x0

    .line 306
    .local v2, "payloadType":B
    const-string v10, "SAP/SAPBleconnection/29Dec2014"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "EXEC cmd "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    new-instance v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;

    invoke-direct {v0}, Lcom/samsung/appcessory/protocol/SAPServiceParams;-><init>()V

    .line 309
    .local v0, "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    const/4 v10, 0x1

    aget-byte v10, v1, v10

    and-int/lit16 v6, v10, 0xff

    .line 310
    const-string v10, "SAP/SAPBleconnection/29Dec2014"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Service id "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    const/4 v10, 0x3

    if-ne v6, v10, :cond_3

    .line 312
    const/4 v10, 0x2

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    int-to-long v7, v10

    .line 313
    const/4 v10, 0x3

    aget-byte v9, v1, v10

    .line 314
    const/4 v10, 0x4

    aget-byte v2, v1, v10

    .line 338
    :cond_0
    :goto_1
    iput v6, v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    .line 339
    iput-wide v4, v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_profileId:J

    .line 340
    iput-wide v7, v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    .line 341
    iput-byte v9, v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionType:B

    .line 342
    iput-byte v2, v0, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_payloadType:B

    .line 344
    const-string v10, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v11, "processAccessoryMessage Exit"

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    .end local v0    # "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .end local v2    # "payloadType":B
    .end local v4    # "profileId":J
    .end local v6    # "serviceId":I
    .end local v7    # "sessionId":J
    .end local v9    # "sessionType":B
    :goto_2
    return-object v0

    .line 289
    .end local v3    # "pmm":B
    :cond_1
    const-string v10, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v11, "processAccessoryMessage Enter:1"

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getpayloadType()B

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_2

    .line 291
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v1

    .line 296
    :goto_3
    const-string v10, "SAP/SAPBleconnection/29Dec2014"

    const-string v11, "Paylod received"

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 293
    :cond_2
    const-string v10, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v11, "processAccessoryMessage Enter:2"

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/samsung/appcessory/transport/SAPBleconnection;->CHARSET:Ljava/nio/charset/Charset;

    invoke-virtual {v10, v11}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    goto :goto_3

    .line 315
    .restart local v0    # "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .restart local v2    # "payloadType":B
    .restart local v3    # "pmm":B
    .restart local v4    # "profileId":J
    .restart local v6    # "serviceId":I
    .restart local v7    # "sessionId":J
    .restart local v9    # "sessionType":B
    :cond_3
    const/16 v10, 0x83

    if-ne v6, v10, :cond_4

    .line 316
    const/4 v10, 0x2

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    int-to-long v7, v10

    .line 317
    const/4 v10, 0x3

    aget-byte v9, v1, v10

    .line 318
    goto :goto_1

    :cond_4
    const/4 v10, 0x4

    if-eq v6, v10, :cond_5

    .line 319
    const/16 v10, 0x84

    if-ne v6, v10, :cond_6

    .line 320
    :cond_5
    const/4 v10, 0x2

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    int-to-long v7, v10

    .line 321
    goto :goto_1

    :cond_6
    const/4 v10, 0x2

    if-eq v6, v10, :cond_0

    .line 322
    const/16 v10, 0x82

    if-eq v6, v10, :cond_0

    .line 324
    const/4 v10, 0x6

    if-ne v6, v10, :cond_0

    .line 328
    const/4 v10, 0x2

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x10

    int-to-long v10, v10

    .line 329
    const/4 v12, 0x3

    aget-byte v12, v1, v12

    and-int/lit16 v12, v12, 0xff

    shl-int/lit8 v12, v12, 0x8

    int-to-long v12, v12

    .line 328
    add-long/2addr v10, v12

    .line 329
    const/4 v12, 0x4

    aget-byte v12, v1, v12

    and-int/lit16 v12, v12, 0xff

    shl-int/lit8 v12, v12, 0x0

    int-to-long v12, v12

    .line 328
    add-long v4, v10, v12

    .line 334
    const/4 v10, 0x5

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    int-to-long v7, v10

    goto/16 :goto_1

    .line 347
    .end local v0    # "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .end local v2    # "payloadType":B
    .end local v4    # "profileId":J
    .end local v6    # "serviceId":I
    .end local v7    # "sessionId":J
    .end local v9    # "sessionType":B
    :cond_7
    const-string v10, "SAP/SAPBleconnection/29Dec2014"

    const-string v11, "Unsupported PMM received!"

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    const-string v10, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v11, "processAccessoryMessage Exit"

    invoke-static {v10, v11}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    const/4 v0, 0x0

    goto/16 :goto_2
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    iput-object v2, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gSapMessage:Lcom/samsung/appcessory/base/SAPMessage;

    .line 126
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->msgWaitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 127
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mBleConnHnldMsg:Z

    .line 128
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->msgWaitLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 126
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    iput-object v2, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mBleConnThread:Ljava/lang/Thread;

    .line 132
    return-void

    .line 126
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public init(Lcom/samsung/appcessory/base/SAPBaseAccessory;Lcom/samsung/appcessory/transport/IConnectionEventListener;)V
    .locals 2
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .param p2, "ll"    # Lcom/samsung/appcessory/transport/IConnectionEventListener;

    .prologue
    .line 72
    const-string v0, "SAP/SAPBleconnection/29Dec2014"

    const-string v1, "init enter() Called"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->msgWaitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 74
    :try_start_0
    iput-object p2, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mListener:Lcom/samsung/appcessory/transport/IConnectionEventListener;

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mBleConnHnldMsg:Z

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->isStreamingData:Z

    .line 73
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    invoke-virtual {p0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->startBleConnThread()V

    .line 80
    const-string v0, "SAP/SAPBleconnection/29Dec2014"

    const-string v1, " init exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    return-void

    .line 73
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized printByteArray([B)V
    .locals 5
    .param p1, "bArray"    # [B

    .prologue
    .line 357
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 358
    .local v1, "p":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-lt v0, v2, :cond_0

    .line 362
    const-string v2, "SAP/SAPBleconnection/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    monitor-exit p0

    return-void

    .line 359
    :cond_0
    :try_start_1
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 360
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 358
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 357
    .end local v0    # "i":I
    .end local v1    # "p":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public read()Lcom/samsung/appcessory/base/SAPMessage;
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method public read([B)V
    .locals 2
    .param p1, "value"    # [B

    .prologue
    .line 96
    const-string v0, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v1, "read enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-boolean v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->handleData:Z

    if-eqz v0, :cond_0

    .line 99
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->msgWaitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 101
    :try_start_0
    iput-object p1, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->gRawMessage:[B

    .line 102
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->msgWaitLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 99
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :cond_0
    const-string v0, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v1, "read exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    return-void

    .line 99
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public resetReadValues()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 520
    const-string v0, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v1, "resetReadValues"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    new-array v0, v2, [B

    sput-object v0, Lcom/samsung/appcessory/transport/SAPBleconnection;->sFragment:[B

    .line 522
    iput v2, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mStrmDataLen:I

    .line 523
    iput-boolean v2, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->isStreamingData:Z

    .line 524
    return-void
.end method

.method public setManager(Lcom/samsung/appcessory/base/SAPBleAccManager;)V
    .locals 0
    .param p1, "sapBleAccManager"    # Lcom/samsung/appcessory/base/SAPBleAccManager;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mSapBleAccManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    .line 86
    return-void
.end method

.method public startBleConnThread()V
    .locals 2

    .prologue
    .line 222
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/appcessory/transport/SAPBleconnection$1;

    invoke-direct {v1, p0}, Lcom/samsung/appcessory/transport/SAPBleconnection$1;-><init>(Lcom/samsung/appcessory/transport/SAPBleconnection;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mBleConnThread:Ljava/lang/Thread;

    .line 276
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mBleConnThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 278
    return-void
.end method

.method public write(Lcom/samsung/appcessory/base/SAPMessage;)V
    .locals 5
    .param p1, "message"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    const/4 v4, 0x1

    .line 136
    const-string v2, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v3, "write  enter"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-virtual {p1, v4}, Lcom/samsung/appcessory/base/SAPMessage;->setpayloadType(B)V

    .line 177
    invoke-virtual {p1}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v0

    .line 178
    .local v0, "data":[B
    const-string v2, "SAP/SAPBleconnection/29Dec2014"

    const-string v3, "-------Data byte Received from Framework------"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-virtual {p0, v0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->printByteArray([B)V

    .line 183
    invoke-static {p1}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->composeProtocolFrame(Lcom/samsung/appcessory/base/SAPMessage;)[B

    move-result-object v1

    .line 185
    .local v1, "protocolFrame":[B
    if-nez v1, :cond_0

    .line 186
    const-string v2, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v3, "protocolFrame is NULL"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :goto_0
    return-void

    .line 189
    :cond_0
    iput-boolean v4, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->handleData:Z

    .line 194
    invoke-virtual {p0, v1}, Lcom/samsung/appcessory/transport/SAPBleconnection;->writeFrameWithTCP([B)V

    .line 198
    const-string v2, "SAP/SAPBleconnection/29Dec2014"

    const-string/jumbo v3, "write  exit"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public writeFrameWithTCP([B)V
    .locals 5
    .param p1, "frame"    # [B

    .prologue
    const/16 v4, 0x7e

    const/4 v3, 0x0

    .line 203
    sget-object v1, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    if-eq v1, v2, :cond_0

    .line 204
    const-string v1, "SAP/SAPBleconnection/29Dec2014"

    const-string v2, "Invalid connection state"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :goto_0
    return-void

    .line 208
    :cond_0
    array-length v1, p1

    add-int/lit8 v1, v1, 0x2

    new-array v0, v1, [B

    .line 210
    .local v0, "tcpFrame":[B
    aput-byte v4, v0, v3

    .line 212
    const/4 v1, 0x1

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 213
    array-length v1, p1

    add-int/lit8 v1, v1, 0x1

    aput-byte v4, v0, v1

    .line 215
    const-string v1, "SAP/SAPBleconnection/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "---------tcpFrame--------->  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    invoke-virtual {p0, v0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->printByteArray([B)V

    .line 218
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPBleconnection;->mSapBleAccManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/base/SAPBleAccManager;->writeCharacteristic([B)Z

    goto :goto_0
.end method
