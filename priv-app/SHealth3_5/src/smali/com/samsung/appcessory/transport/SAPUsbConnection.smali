.class public Lcom/samsung/appcessory/transport/SAPUsbConnection;
.super Lcom/samsung/appcessory/base/SAPConnection;
.source "SAPUsbConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/transport/SAPUsbConnection$SAPListenTask;
    }
.end annotation


# static fields
.field private static final MAXIMUM_PAYLOAD_SIZE_IN_BYTES:I = 0x4000

.field private static final SAP_CONNECTION_LISTEN_POLL_INTERVAL:J = 0x64L

.field private static final SAP_LISTENER_TERMINATION_WAIT_TIME:J = 0xc8L

.field private static final TAG:Ljava/lang/String; = "SAP/SAPUsbConnection/29Dec2014"


# instance fields
.field listener:Ljava/util/concurrent/ScheduledExecutorService;

.field private mAccessory:Lcom/samsung/appcessory/base/SAPBaseAccessory;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPConnection;-><init>()V

    .line 62
    const-string v0, "SAP/SAPUsbConnection/29Dec2014"

    const-string v1, "SAPUsbConnection enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    const-string v0, "SAP/SAPUsbConnection/29Dec2014"

    const-string v1, "SAPUsbConnection exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    return-void
.end method


# virtual methods
.method public close()V
    .locals 5

    .prologue
    .line 87
    const-string v1, "SAP/SAPUsbConnection/29Dec2014"

    const-string v2, "close enter"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 92
    :try_start_0
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0xc8

    .line 93
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 92
    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    .line 93
    if-nez v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_0
    :goto_0
    const-string v1, "SAP/SAPUsbConnection/29Dec2014"

    const-string v2, "close exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v1, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 98
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public init(Lcom/samsung/appcessory/base/SAPBaseAccessory;Lcom/samsung/appcessory/transport/IConnectionEventListener;)V
    .locals 7
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .param p2, "ll"    # Lcom/samsung/appcessory/transport/IConnectionEventListener;

    .prologue
    .line 71
    const-string v0, "SAP/SAPUsbConnection/29Dec2014"

    const-string v2, "init enter"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iput-object p1, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection;->mAccessory:Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 74
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    .line 75
    sget-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    iput-object v0, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection;->_status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 77
    new-instance v1, Lcom/samsung/appcessory/transport/SAPUsbConnection$SAPListenTask;

    invoke-direct {v1, p0}, Lcom/samsung/appcessory/transport/SAPUsbConnection$SAPListenTask;-><init>(Lcom/samsung/appcessory/transport/SAPUsbConnection;)V

    .line 78
    .local v1, "task":Lcom/samsung/appcessory/transport/SAPUsbConnection$SAPListenTask;
    invoke-virtual {v1, p2}, Lcom/samsung/appcessory/transport/SAPUsbConnection$SAPListenTask;->setConnectionListener(Lcom/samsung/appcessory/transport/IConnectionEventListener;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection;->listener:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x0

    .line 81
    const-wide/16 v4, 0x64

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 80
    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 82
    const-string v0, "SAP/SAPUsbConnection/29Dec2014"

    const-string v2, "init exit"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    return-void
.end method

.method public read()Lcom/samsung/appcessory/base/SAPMessage;
    .locals 7

    .prologue
    const/16 v6, 0x4000

    .line 137
    const-string v4, "SAP/SAPUsbConnection/29Dec2014"

    const-string/jumbo v5, "read enter"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    new-array v0, v6, [B

    .line 139
    .local v0, "buf":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v6, :cond_0

    .line 142
    const/4 v3, 0x0

    .line 145
    .local v3, "readCount":I
    :try_start_0
    iget-object v4, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection;->mAccessory:Lcom/samsung/appcessory/base/SAPBaseAccessory;

    check-cast v4, Lcom/samsung/appcessory/base/SAPUsbAccessory;

    iget-object v4, v4, Lcom/samsung/appcessory/base/SAPUsbAccessory;->mAccFileIntStream:Ljava/io/FileInputStream;

    .line 146
    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 153
    :goto_1
    if-lez v3, :cond_1

    .line 169
    invoke-static {v0}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->parseProtocolFrame([B)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v2

    .line 173
    :goto_2
    return-object v2

    .line 140
    .end local v3    # "readCount":I
    :cond_0
    const/4 v4, 0x0

    aput-byte v4, v0, v1

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 172
    .restart local v3    # "readCount":I
    :cond_1
    const-string v4, "SAP/SAPUsbConnection/29Dec2014"

    const-string/jumbo v5, "read exit"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    const/4 v2, 0x0

    goto :goto_2

    .line 149
    :catch_0
    move-exception v4

    goto :goto_1

    .line 147
    :catch_1
    move-exception v4

    goto :goto_1
.end method

.method public write(Lcom/samsung/appcessory/base/SAPMessage;)V
    .locals 5
    .param p1, "message"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    .line 107
    const-string v2, "SAP/SAPUsbConnection/29Dec2014"

    const-string v3, " write enter"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-static {p1}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->composeProtocolFrame(Lcom/samsung/appcessory/base/SAPMessage;)[B

    move-result-object v1

    .line 117
    .local v1, "protocolFrame":[B
    if-nez v1, :cond_0

    .line 118
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v3, "protocolFrame is NULL"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 134
    :goto_0
    return-void

    .line 123
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/appcessory/transport/SAPUsbConnection;->mAccessory:Lcom/samsung/appcessory/base/SAPBaseAccessory;

    check-cast v2, Lcom/samsung/appcessory/base/SAPUsbAccessory;

    iget-object v2, v2, Lcom/samsung/appcessory/base/SAPUsbAccessory;->mAccFileOutStream:Ljava/io/FileOutputStream;

    .line 124
    invoke-virtual {v2, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 133
    :goto_1
    const-string v2, "SAP/SAPUsbConnection/29Dec2014"

    const-string/jumbo v3, "write exit"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "SAP/SAPUsbConnection/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "write exception"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 128
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 129
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SAP/SAPUsbConnection/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "write exception"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
