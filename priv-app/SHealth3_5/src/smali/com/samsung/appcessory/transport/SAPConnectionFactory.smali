.class public Lcom/samsung/appcessory/transport/SAPConnectionFactory;
.super Ljava/lang/Object;
.source "SAPConnectionFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPConnectionFactory"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getConnection(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Lcom/samsung/appcessory/base/SAPConnection;
    .locals 4
    .param p0, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 31
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 33
    .local v1, "type":Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_WIFI:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v1, v2, :cond_0

    .line 34
    new-instance v0, Lcom/samsung/appcessory/transport/SAPWifiConnection;

    invoke-direct {v0}, Lcom/samsung/appcessory/transport/SAPWifiConnection;-><init>()V

    .line 51
    :goto_0
    return-object v0

    .line 36
    :cond_0
    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v1, v2, :cond_2

    .line 37
    const/4 v0, 0x0

    .line 38
    .local v0, "bleConnection":Lcom/samsung/appcessory/transport/SAPBleconnection;
    sget v2, Lcom/samsung/appcessory/base/SAPAccessoryManager;->sCurrentApiVersion:I

    const/16 v3, 0x12

    if-ge v2, v3, :cond_1

    .line 39
    const-string v2, "SAP/SAPConnectionFactory"

    const-string v3, "BLE not supported"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 41
    :cond_1
    invoke-static {}, Lcom/samsung/appcessory/transport/SAPBleconnection;->getInstance()Lcom/samsung/appcessory/transport/SAPBleconnection;

    move-result-object v0

    goto :goto_0

    .line 45
    .end local v0    # "bleConnection":Lcom/samsung/appcessory/transport/SAPBleconnection;
    :cond_2
    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_USB:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v1, v2, :cond_3

    .line 46
    new-instance v0, Lcom/samsung/appcessory/transport/SAPUsbConnection;

    invoke-direct {v0}, Lcom/samsung/appcessory/transport/SAPUsbConnection;-><init>()V

    goto :goto_0

    .line 48
    :cond_3
    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v1, v2, :cond_4

    .line 49
    new-instance v0, Lcom/samsung/appcessory/transport/SAPBtRfConnection;

    invoke-direct {v0}, Lcom/samsung/appcessory/transport/SAPBtRfConnection;-><init>()V

    goto :goto_0

    .line 51
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
