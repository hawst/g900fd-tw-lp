.class public interface abstract Lcom/samsung/appcessory/base/IAccessoryEventListener;
.super Ljava/lang/Object;
.source "IAccessoryEventListener.java"


# virtual methods
.method public abstract onAccessoryAttached(JI)V
.end method

.method public abstract onAccessoryDetached(J)V
.end method

.method public abstract onAccessoryDetached(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
.end method

.method public abstract onAccessoryDisconnected(J)V
.end method

.method public abstract onAccessoryMessageReceived(Lcom/samsung/appcessory/base/SAPMessageItem;)V
.end method

.method public abstract onAcessoryAttached(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
.end method

.method public abstract onConnectStateChange(JI)V
.end method

.method public abstract onError(JJI)V
.end method

.method public abstract onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V
.end method

.method public abstract onNewConnection(JLcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V
.end method

.method public abstract onPairedStatus(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JZ)V
.end method

.method public abstract onSessionClose(JJ)V
.end method

.method public abstract onStatusChange(ILcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V
.end method
