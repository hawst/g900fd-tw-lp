.class Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;
.super Landroid/os/Handler;
.source "SAPAccessoryManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPAccessoryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SAPAccMngrHndl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/base/SAPAccessoryManager;


# direct methods
.method public constructor <init>(Lcom/samsung/appcessory/base/SAPAccessoryManager;Landroid/os/Looper;)V
    .locals 2
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    .line 231
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 232
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v1, "SAPAccMngrHndl enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 236
    const-string v3, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SAPAccMngrHndl enter msg = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    # getter for: Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->access$0()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 238
    :try_start_0
    iget v3, p1, Landroid/os/Message;->what:I

    if-nez v3, :cond_2

    .line 239
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 240
    .local v2, "b":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    # getter for: Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->access$1(Lcom/samsung/appcessory/base/SAPAccessoryManager;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 241
    .local v1, "accNum":I
    if-nez v1, :cond_1

    .line 242
    const-string v3, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v5, "SAPAccMngrHndl accNum == 0"

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    .end local v1    # "accNum":I
    .end local v2    # "b":Landroid/os/Bundle;
    :cond_0
    :goto_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    const-string v3, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v4, "SAPAccMngrHndl exit"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    return-void

    .line 244
    .restart local v1    # "accNum":I
    .restart local v2    # "b":Landroid/os/Bundle;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    # getter for: Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->access$2(Lcom/samsung/appcessory/base/SAPAccessoryManager;)Lcom/samsung/appcessory/base/IAccessoryEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 245
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    # getter for: Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->access$2(Lcom/samsung/appcessory/base/SAPAccessoryManager;)Lcom/samsung/appcessory/base/IAccessoryEventListener;

    move-result-object v3

    .line 246
    const-string v5, "accid"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 247
    const-string v7, "DEVICE TYPE"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 245
    invoke-interface {v3, v5, v6, v7}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onAccessoryAttached(JI)V

    goto :goto_0

    .line 237
    .end local v1    # "accNum":I
    .end local v2    # "b":Landroid/os/Bundle;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 250
    :cond_2
    :try_start_2
    iget v3, p1, Landroid/os/Message;->what:I

    const/4 v5, 0x1

    if-ne v3, v5, :cond_0

    .line 251
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 252
    .restart local v2    # "b":Landroid/os/Bundle;
    const-string v3, "accid"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 253
    .local v0, "accId":Ljava/lang/Long;
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    # invokes: Lcom/samsung/appcessory/base/SAPAccessoryManager;->removeAcc(Ljava/lang/Long;)V
    invoke-static {v3, v0}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->access$3(Lcom/samsung/appcessory/base/SAPAccessoryManager;Ljava/lang/Long;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
