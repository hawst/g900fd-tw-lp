.class Lcom/samsung/appcessory/base/SAPFileOutputStream;
.super Ljava/io/FileOutputStream;
.source "SAPUsbAccManager.java"


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/FileDescriptor;)V
    .locals 1
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 318
    invoke-direct {p0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 315
    const-string v0, "SAP/SAPFileOutputStream/29Dec2014"

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPFileOutputStream;->TAG:Ljava/lang/String;

    .line 320
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPFileOutputStream;->TAG:Ljava/lang/String;

    const-string v1, "closing file"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 325
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPFileOutputStream;->TAG:Ljava/lang/String;

    const-string v1, "finalize of SAPFileOutputStream**********"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    invoke-super {p0}, Ljava/io/FileOutputStream;->finalize()V

    .line 327
    return-void
.end method
