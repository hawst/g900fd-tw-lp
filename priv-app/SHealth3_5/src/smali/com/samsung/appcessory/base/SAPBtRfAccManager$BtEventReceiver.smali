.class Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SAPBtRfAccManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPBtRfAccManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BtEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;


# direct methods
.method private constructor <init>(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;)V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0, p1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;-><init>(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 253
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 254
    .local v4, "action":Ljava/lang/String;
    const-string v14, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Received Action : "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const-string v14, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 256
    const-string v14, "android.bluetooth.adapter.extra.STATE"

    .line 257
    const/4 v15, 0x0

    .line 256
    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 258
    .local v13, "state":I
    const-string v14, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "ACTION_STATE_CHANGED new state "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    invoke-static {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->obtainMessage()Landroid/os/Message;

    move-result-object v12

    .line 260
    .local v12, "msg":Landroid/os/Message;
    const/16 v14, 0xa

    if-ne v13, v14, :cond_1

    .line 261
    const/4 v14, 0x0

    iput v14, v12, Landroid/os/Message;->what:I

    .line 262
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    invoke-static {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    move-result-object v14

    invoke-virtual {v14, v12}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->sendMessage(Landroid/os/Message;)Z

    .line 360
    .end local v12    # "msg":Landroid/os/Message;
    .end local v13    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 263
    .restart local v12    # "msg":Landroid/os/Message;
    .restart local v13    # "state":I
    :cond_1
    const/16 v14, 0xc

    if-ne v13, v14, :cond_0

    .line 264
    const/4 v14, 0x1

    iput v14, v12, Landroid/os/Message;->what:I

    .line 265
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    invoke-static {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    move-result-object v14

    invoke-virtual {v14, v12}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 267
    .end local v12    # "msg":Landroid/os/Message;
    .end local v13    # "state":I
    :cond_2
    const-string v14, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 268
    const-string v14, "android.bluetooth.device.extra.DEVICE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/bluetooth/BluetoothDevice;

    .line 269
    .local v9, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v9, :cond_3

    .line 270
    const-string v14, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "ACTION_FOUND device found "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    invoke-static {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->obtainMessage()Landroid/os/Message;

    move-result-object v12

    .line 272
    .restart local v12    # "msg":Landroid/os/Message;
    const/4 v14, 0x3

    iput v14, v12, Landroid/os/Message;->what:I

    .line 273
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 274
    .local v5, "b":Landroid/os/Bundle;
    const-string v14, "device"

    invoke-virtual {v5, v14, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 275
    invoke-virtual {v12, v5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 276
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    invoke-static {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    move-result-object v14

    invoke-virtual {v14, v12}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 278
    .end local v5    # "b":Landroid/os/Bundle;
    .end local v12    # "msg":Landroid/os/Message;
    :cond_3
    const-string v14, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v15, "Device in NULL "

    invoke-static {v14, v15}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 280
    .end local v9    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_4
    const-string v14, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 281
    const-string v14, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v15, "ACTION_DISCOVERY_FINISHED Received"

    invoke-static {v14, v15}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    invoke-static {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    move-result-object v14

    const/4 v15, 0x4

    invoke-virtual {v14, v15}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 284
    :cond_5
    const-string v14, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 285
    const-string v14, "android.bluetooth.device.extra.DEVICE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/bluetooth/BluetoothDevice;

    .line 286
    .restart local v9    # "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v9, :cond_0

    .line 287
    const-string v14, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "bond state = "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v14

    const/16 v15, 0xc

    if-ne v14, v15, :cond_7

    .line 290
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v15

    # invokes: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->isHealthDevice(Ljava/lang/String;)Z
    invoke-static {v14, v15}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 291
    const-string v14, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v15, "found health device. listen rfcomm"

    invoke-static {v14, v15}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->startListeningOnBtRf()Z

    goto/16 :goto_0

    .line 293
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v15

    # invokes: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->isGearDevice(Ljava/lang/String;)Z
    invoke-static {v14, v15}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$6(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 294
    const-string v14, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v15, "eSAP will handle the connection from scale"

    invoke-static {v14, v15}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->stopListeningOnBtRf()Z

    goto/16 :goto_0

    .line 298
    :cond_7
    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v14

    const/16 v15, 0xa

    if-ne v14, v15, :cond_0

    .line 299
    const/4 v6, 0x1

    .line 300
    .local v6, "bStopListen":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$1(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v14

    invoke-virtual {v14}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v7

    .line 301
    .local v7, "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v7, :cond_9

    .line 302
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 303
    .local v10, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_a

    .line 313
    .end local v10    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_9
    :goto_1
    if-eqz v6, :cond_0

    .line 314
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->stopListeningOnBtRf()Z

    goto/16 :goto_0

    .line 304
    .restart local v10    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_a
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/bluetooth/BluetoothDevice;

    .line 305
    .local v8, "dev":Landroid/bluetooth/BluetoothDevice;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v15

    # invokes: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->isHealthDevice(Ljava/lang/String;)Z
    invoke-static {v14, v15}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 306
    const-string v14, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v15, "found health device. continue to listen rfcomm"

    invoke-static {v14, v15}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    const/4 v6, 0x0

    .line 308
    goto :goto_1

    .line 318
    .end local v6    # "bStopListen":Z
    .end local v7    # "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v8    # "dev":Landroid/bluetooth/BluetoothDevice;
    .end local v9    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v10    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_b
    const-string v14, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_10

    .line 319
    const-string v14, "android.bluetooth.device.extra.DEVICE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/bluetooth/BluetoothDevice;

    .line 320
    .restart local v9    # "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v9, :cond_0

    .line 321
    const-string v14, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "bond state = "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v15

    # invokes: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->getAccByMacAddress(Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    invoke-static {v14, v15}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$7(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    move-result-object v3

    .line 323
    .local v3, "acc1":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    if-eqz v3, :cond_c

    .line 324
    const/4 v14, 0x1

    invoke-virtual {v3, v14}, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->setConnectedState(Z)V

    .line 326
    :cond_c
    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v14

    const/16 v15, 0xc

    if-ne v14, v15, :cond_0

    .line 327
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;
    invoke-static {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$8(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Ljava/lang/Object;

    move-result-object v15

    monitor-enter v15

    .line 328
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;
    invoke-static {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$9(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 330
    .local v11, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    :cond_d
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_f

    .line 327
    :cond_e
    :goto_2
    monitor-exit v15

    goto/16 :goto_0

    .end local v11    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    :catchall_0
    move-exception v14

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v14

    .line 331
    .restart local v11    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    :cond_f
    :try_start_1
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .line 332
    .local v2, "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    iget-object v14, v2, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v14}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_d

    .line 333
    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v14

    const/16 v16, 0xc

    move/from16 v0, v16

    if-ne v14, v0, :cond_e

    .line 334
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    invoke-static {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->obtainMessage()Landroid/os/Message;

    move-result-object v12

    .line 335
    .restart local v12    # "msg":Landroid/os/Message;
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 336
    .restart local v5    # "b":Landroid/os/Bundle;
    const-string v14, "id"

    iget-wide v0, v2, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->_id:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v5, v14, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 337
    const/4 v14, 0x6

    iput v14, v12, Landroid/os/Message;->what:I

    .line 338
    invoke-virtual {v12, v5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 339
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    invoke-static {v14}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    move-result-object v14

    invoke-virtual {v14, v12}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 347
    .end local v2    # "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    .end local v3    # "acc1":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    .end local v5    # "b":Landroid/os/Bundle;
    .end local v9    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v11    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    .end local v12    # "msg":Landroid/os/Message;
    :cond_10
    const-string v14, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 348
    const-string v14, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v15, "ACTION_ACL_DISCONNECTED Received"

    invoke-static {v14, v15}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    const-string v14, "android.bluetooth.device.extra.DEVICE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/bluetooth/BluetoothDevice;

    .line 351
    .restart local v8    # "dev":Landroid/bluetooth/BluetoothDevice;
    if-eqz v8, :cond_0

    .line 352
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v15

    # invokes: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->getAccByMacAddress(Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    invoke-static {v14, v15}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$7(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    move-result-object v2

    .line 354
    .restart local v2    # "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    if-eqz v2, :cond_0

    .line 355
    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->setConnectedState(Z)V

    .line 356
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    const/4 v15, 0x1

    invoke-virtual {v14, v2, v15}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->removeAccessory(Lcom/samsung/appcessory/base/SAPBtRfAccessory;Z)V

    goto/16 :goto_0
.end method
