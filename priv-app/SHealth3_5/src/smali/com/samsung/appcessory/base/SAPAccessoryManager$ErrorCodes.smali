.class public final enum Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
.super Ljava/lang/Enum;
.source "SAPAccessoryManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPAccessoryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorCodes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

.field public static final enum ERROR_FRAME_END_MARKER_NOT_FOUND:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

.field public static final enum ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

.field public static final enum MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 71
    new-instance v0, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    const-string v1, "MESSAGE_SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    new-instance v0, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    const-string v1, "ERROR_FRAME_INVALID"

    invoke-direct {v0, v1, v3}, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    new-instance v0, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    const-string v1, "ERROR_FRAME_END_MARKER_NOT_FOUND"

    invoke-direct {v0, v1, v4}, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_END_MARKER_NOT_FOUND:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    .line 70
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    sget-object v1, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->MESSAGE_SUCCESS:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_INVALID:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ERROR_FRAME_END_MARKER_NOT_FOUND:Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ENUM$VALUES:[Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    return-object v0
.end method

.method public static values()[Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;->ENUM$VALUES:[Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
