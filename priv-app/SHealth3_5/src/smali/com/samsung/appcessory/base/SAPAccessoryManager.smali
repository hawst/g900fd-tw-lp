.class public final Lcom/samsung/appcessory/base/SAPAccessoryManager;
.super Ljava/lang/Object;
.source "SAPAccessoryManager.java"

# interfaces
.implements Lcom/samsung/appcessory/base/IAccessoryListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/base/SAPAccessoryManager$ErrorCodes;,
        Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;
    }
.end annotation


# static fields
.field private static final ACC_EVT_ACC_FOUND:I = 0x0

.field private static final ACC_EVT_ACC_LOST:I = 0x1

.field private static final ACC_EVT_DATA_ACCID:Ljava/lang/String; = "accid"

.field private static final ACC_EVT_FOUND_TYPE:Ljava/lang/String; = "DEVICE TYPE"

.field public static final CLOSE_SESSION_TIME_OUT_ERROR:I = 0x6c

.field public static final CONNECTION_ERROR:I = 0x66

.field public static final CONNECTION_NO_RESPONSE_ERROR:I = 0x6e

.field public static final OPEN_SESSION_IN_PROGRESS_ERROR:I = 0x6a

.field public static final OPEN_SESSION_TIME_OUT_ERROR:I = 0x6b

.field public static final SENSOR_NOT_PAIRED_ERROR:I = 0x68

.field public static final SENSOR_SEARCH_FAILED_ERROR:I = 0x69

.field private static final TAG:Ljava/lang/String; = "SAP/SAPAccessoryManager/29Dec2014"

.field private static final TRANSPORT_BLE:I = 0x4

.field private static final TRANSPORT_BT:I = 0x2

.field private static final TRANSPORT_NFC:I = 0x8

.field private static final TRANSPORT_USB:I = 0x10

.field private static final TRANSPORT_WIFI:I = 0x1

.field public static final UNKNOWN_ERROR:I = 0x64

.field public static final WRITE_MESSAGE_ERROR:I = 0x6d

.field public static final WRONG_DATA_FORMAT:I = 0x67

.field private static final msAccListLock:Ljava/lang/Object;

.field public static sCurrentApiVersion:I


# instance fields
.field private mActiveAccList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation
.end field

.field private mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

.field private mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

.field public mContext:Landroid/content/Context;

.field private mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

.field mloop:Landroid/os/Looper;

.field private mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, -0x1

    sput v0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->sCurrentApiVersion:I

    .line 75
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loop"    # Landroid/os/Looper;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    .line 78
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v1, "SAPAccessoryManager enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mContext:Landroid/content/Context;

    .line 80
    sget-object v1, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 81
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    .line 80
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    new-instance v0, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    invoke-direct {v0, p0, p2}, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;-><init>(Lcom/samsung/appcessory/base/SAPAccessoryManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    .line 84
    iput-object p2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mloop:Landroid/os/Looper;

    .line 85
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v1, "SAPAccessoryManager exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    return-void

    .line 80
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic access$0()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/appcessory/base/SAPAccessoryManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/appcessory/base/SAPAccessoryManager;)Lcom/samsung/appcessory/base/IAccessoryEventListener;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/appcessory/base/SAPAccessoryManager;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0, p1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->removeAcc(Ljava/lang/Long;)V

    return-void
.end method

.method static generateUniqueId()J
    .locals 7

    .prologue
    .line 187
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 188
    .local v0, "rng":Ljava/util/Random;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Random;->setSeed(J)V

    .line 189
    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v3

    const-wide/32 v5, 0xffff

    and-long v1, v3, v5

    .line 190
    .local v1, "uid":J
    return-wide v1
.end method

.method private removeAcc(Ljava/lang/Long;)V
    .locals 8
    .param p1, "accId"    # Ljava/lang/Long;

    .prologue
    .line 263
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "removeAcc enter accId = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    sget-object v3, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v3

    .line 265
    :try_start_0
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 266
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v4, "mActiveAccList == null plz check"

    invoke-static {v2, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    monitor-exit v3

    .line 285
    :goto_0
    return-void

    .line 269
    :cond_0
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 270
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v4, "mActiveAccList.size()=0"

    invoke-static {v2, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    monitor-exit v3

    goto :goto_0

    .line 264
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 273
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 283
    :goto_2
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v4, "removeAcc exit"

    invoke-static {v2, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    monitor-exit v3

    goto :goto_0

    .line 274
    :cond_2
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 275
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v0, :cond_3

    iget-wide v4, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-nez v2, :cond_3

    .line 276
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 277
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "latest acc count after removing acc with id "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 278
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 277
    invoke-static {v2, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onAccessoryDetached(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 273
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private translateTransportType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I
    .locals 2
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 288
    const/4 v0, 0x0

    .line 289
    .local v0, "transport":I
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_WIFI:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_1

    .line 290
    const/4 v0, 0x1

    .line 298
    :cond_0
    :goto_0
    return v0

    .line 291
    :cond_1
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_2

    .line 292
    const/4 v0, 0x4

    .line 293
    goto :goto_0

    :cond_2
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_3

    .line 294
    const/4 v0, 0x2

    .line 295
    goto :goto_0

    :cond_3
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_USB:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_0

    .line 296
    const/16 v0, 0x10

    goto :goto_0
.end method


# virtual methods
.method public connectAccessory(JZ)Z
    .locals 5
    .param p1, "id"    # J
    .param p3, "isAutoConnect"    # Z

    .prologue
    .line 456
    sget-object v2, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v2

    .line 457
    :try_start_0
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "connectAccessory enter id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    invoke-virtual {p0, p1, p2}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getAccessoryById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v0

    .line 461
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v0, :cond_2

    .line 462
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "acc._type =>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 463
    const-string v4, "SAPAccessoryType.ACC_TYPE_BLE="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 464
    sget-object v4, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 462
    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    iget-object v1, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    if-eqz v1, :cond_0

    .line 466
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-virtual {v1, p1, p2, p3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->startBleConnect(JZ)Z

    move-result v1

    monitor-exit v2

    .line 475
    :goto_0
    return v1

    .line 467
    :cond_0
    iget-object v1, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    if-eqz v1, :cond_1

    .line 468
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->startBtRfConnect(J)Z

    move-result v1

    monitor-exit v2

    goto :goto_0

    .line 456
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 470
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_1
    :try_start_1
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "connectAccessory unsupported transport "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 471
    iget-object v4, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 470
    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deinit()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 194
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v3, "deinit "

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    sget-object v3, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v3

    .line 196
    :try_start_0
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 197
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 198
    .local v1, "mtempActiveAccList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    if-eqz v1, :cond_0

    .line 200
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_4

    .line 203
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 205
    .end local v0    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 206
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    .line 195
    .end local v1    # "mtempActiveAccList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    if-eqz v2, :cond_2

    .line 217
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-virtual {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager;->deinit()V

    .line 219
    :cond_2
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    if-eqz v2, :cond_3

    .line 220
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v2}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->deinit()V

    .line 223
    :cond_3
    iput-object v4, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .line 224
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v3, "deinit Exit"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    return-void

    .line 201
    .restart local v0    # "i":I
    .restart local v1    # "mtempActiveAccList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    :cond_4
    :try_start_1
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    invoke-virtual {p0, v2}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->removeAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    .end local v0    # "i":I
    .end local v1    # "mtempActiveAccList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public getAccessoryById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 433
    sget-object v4, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v4

    .line 434
    :try_start_0
    const-string v3, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getAccessoryById enter id = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const/4 v0, 0x0

    .line 438
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    .line 440
    monitor-exit v4

    move-object v1, v0

    .line 451
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .local v1, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :goto_0
    return-object v1

    .line 443
    .end local v1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_0
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 450
    :goto_1
    const-string v3, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v5, "getAccessoryById exit"

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    monitor-exit v4

    move-object v1, v0

    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .restart local v1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    goto :goto_0

    .line 443
    .end local v1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 444
    .local v2, "it":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    iget-wide v5, v2, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    cmp-long v5, v5, p1

    if-nez v5, :cond_1

    .line 445
    move-object v0, v2

    .line 446
    const-string v3, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v5, "getAccessoryById acc found"

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 433
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v2    # "it":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getAccessoryByName(Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 505
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAccessoryByType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Ljava/util/List;
    .locals 7
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 480
    sget-object v4, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v4

    .line 481
    :try_start_0
    const-string v3, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getAccessoryByType enter type = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 483
    .local v1, "accList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 499
    const-string v3, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getAccessoryByType exit. No of accessories found = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    monitor-exit v4

    return-object v1

    .line 483
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 484
    .local v2, "it":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    iget-object v3, v2, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v3, p1, :cond_0

    .line 485
    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v3, :cond_2

    .line 487
    move-object v0, v2

    check-cast v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/samsung/appcessory/base/SAPBleAccessory;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 489
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 480
    .end local v1    # "accList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    .end local v2    # "it":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 494
    .restart local v1    # "accList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    .restart local v2    # "it":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_2
    :try_start_1
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public getAccessoryState(J)Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 519
    sget-object v2, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v2

    .line 520
    :try_start_0
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getAccessoryState enter id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 527
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v3, "getAccessoryState exit no acc found"

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    monitor-exit v2

    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 521
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 522
    .local v0, "it":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    iget-wide v3, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    cmp-long v3, v3, p1

    if-nez v3, :cond_0

    .line 523
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getAccessoryState exit state = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    iget-object v1, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    monitor-exit v2

    goto :goto_0

    .line 519
    .end local v0    # "it":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getBLEVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 657
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    if-eqz v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBleAccManager;->getBLEVersion()Ljava/lang/String;

    move-result-object v0

    .line 661
    :goto_0
    return-object v0

    .line 660
    :cond_0
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v1, "getBLEVesrsion() Failed"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getConnectedAccessory()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 426
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v1, "getConnectedAccessory enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    sget-object v1, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 428
    :try_start_0
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    monitor-exit v1

    return-object v0

    .line 427
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getPairedAccessoryList(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Ljava/util/List;
    .locals 3
    .param p1, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 643
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getPairedAccessoryList. AccessoryType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    if-eqz v0, :cond_1

    .line 646
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBleAccManager;->getPairedAccessoryList()Ljava/util/List;

    move-result-object v0

    .line 653
    :goto_0
    return-object v0

    .line 648
    :cond_0
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v0, :cond_1

    .line 649
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    if-eqz v0, :cond_1

    .line 650
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->getPairedAccessoryList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 653
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Lcom/samsung/appcessory/base/IAccessoryEventListener;J)V
    .locals 5
    .param p1, "serviceListener"    # Lcom/samsung/appcessory/base/IAccessoryEventListener;
    .param p2, "mTrasnportMask"    # J

    .prologue
    const-wide/16 v3, 0x0

    .line 140
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "init enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->sCurrentApiVersion:I

    .line 144
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .line 153
    const-wide/16 v0, 0x2

    and-long/2addr v0, p2

    cmp-long v0, v0, v3

    if-eqz v0, :cond_0

    .line 154
    new-instance v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mloop:Landroid/os/Looper;

    invoke-direct {v0, v1, v2}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    .line 155
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v0, p0}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->init(Lcom/samsung/appcessory/base/IAccessoryListener;)V

    .line 158
    :cond_0
    const-wide/16 v0, 0x4

    and-long/2addr v0, p2

    cmp-long v0, v0, v3

    if-eqz v0, :cond_1

    .line 159
    sget v0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->sCurrentApiVersion:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_2

    .line 160
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v1, "Platform does not support Android 4.3 Gatt APIs"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :cond_1
    :goto_0
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v1, "init exit SAPAccessoryManager"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    return-void

    .line 162
    :cond_2
    new-instance v0, Lcom/samsung/appcessory/base/SAPBleAccManager;

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mloop:Landroid/os/Looper;

    invoke-direct {v0, v1, v2}, Lcom/samsung/appcessory/base/SAPBleAccManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    .line 163
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-virtual {v0, p0}, Lcom/samsung/appcessory/base/SAPBleAccManager;->init(Lcom/samsung/appcessory/base/IAccessoryListener;)V

    goto :goto_0
.end method

.method public onAccessoryDisconnected(Ljava/lang/Long;)V
    .locals 3
    .param p1, "accId"    # Ljava/lang/Long;

    .prologue
    .line 420
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onAccessoryDisconnected enter accId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onAccessoryDisconnected(J)V

    .line 423
    :cond_0
    return-void
.end method

.method public onAccessoryLost(Ljava/lang/Long;)V
    .locals 6
    .param p1, "accId"    # Ljava/lang/Long;

    .prologue
    .line 407
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onAccessoryLost enter accId = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    monitor-enter v3

    .line 409
    :try_start_0
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    invoke-virtual {v2}, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 410
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 411
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 412
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "accid"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 413
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 414
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    invoke-virtual {v2, v1}, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->sendMessage(Landroid/os/Message;)Z

    .line 408
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v3, "onAccessoryLost exit"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    return-void

    .line 408
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public onBleAccessoryFound(Lcom/samsung/appcessory/base/SAPBleAccessory;Z)V
    .locals 10
    .param p1, "acc"    # Lcom/samsung/appcessory/base/SAPBleAccessory;
    .param p2, "informApp"    # Z

    .prologue
    .line 343
    sget-object v5, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v5

    .line 344
    :try_start_0
    const-string v4, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v6, "onBleAccessoryFound enter"

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    if-eqz p1, :cond_2

    .line 346
    const/4 v1, 0x0

    .line 348
    .local v1, "duplicateFound":Z
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 356
    :goto_0
    const-string v4, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "duplicateFound = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    if-nez v1, :cond_1

    .line 358
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    :cond_1
    if-eqz p2, :cond_2

    .line 363
    iget-object v6, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 364
    :try_start_1
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    invoke-virtual {v4}, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 365
    .local v2, "msg":Landroid/os/Message;
    const/4 v4, 0x0

    iput v4, v2, Landroid/os/Message;->what:I

    .line 366
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 367
    .local v0, "b":Landroid/os/Bundle;
    const-string v4, "DEVICE TYPE"

    sget-object v7, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-direct {p0, v7}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->translateTransportType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I

    move-result v7

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 368
    const-string v4, "accid"

    iget-wide v7, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-virtual {v0, v4, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 369
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 370
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    invoke-virtual {v4, v2}, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->sendMessage(Landroid/os/Message;)Z

    .line 363
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "duplicateFound":Z
    .end local v2    # "msg":Landroid/os/Message;
    :cond_2
    :try_start_2
    const-string v4, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v6, "onBleAccessoryFound exit"

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    monitor-exit v5

    .line 376
    return-void

    .line 348
    .restart local v1    # "duplicateFound":Z
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 349
    .local v3, "tempAcc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    iget-object v6, v3, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    sget-object v7, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v6, v7, :cond_0

    .line 350
    iget-wide v6, v3, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    iget-wide v8, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    .line 351
    const/4 v1, 0x1

    .line 352
    goto :goto_0

    .line 363
    .end local v3    # "tempAcc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v4

    .line 343
    .end local v1    # "duplicateFound":Z
    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v4
.end method

.method public onBtRfAccessoryFound(Lcom/samsung/appcessory/base/SAPBtRfAccessory;)V
    .locals 7
    .param p1, "acc"    # Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .prologue
    .line 380
    sget-object v3, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v3

    .line 381
    :try_start_0
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v4, "onBtRfAccessoryFound enter"

    invoke-static {v2, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    if-eqz p1, :cond_0

    .line 384
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "final size of mactList=>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 390
    :try_start_1
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    invoke-virtual {v2}, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 391
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    .line 392
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 394
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "DEVICE TYPE"

    .line 395
    sget-object v5, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-direct {p0, v5}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->translateTransportType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I

    move-result v5

    .line 393
    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 396
    const-string v2, "accid"

    iget-wide v5, p1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->_id:J

    invoke-virtual {v0, v2, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 397
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 398
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    invoke-virtual {v2, v1}, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->sendMessage(Landroid/os/Message;)Z

    .line 388
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    :try_start_2
    const-string v2, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v4, "onBtRfAccessoryFound exit"

    invoke-static {v2, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 404
    return-void

    .line 388
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2

    .line 380
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2
.end method

.method public onConnectStateChange(Ljava/lang/Long;I)V
    .locals 3
    .param p1, "accId"    # Ljava/lang/Long;
    .param p2, "status"    # I

    .prologue
    .line 590
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v1, "onConnectStateChange"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2, p2}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onConnectStateChange(JI)V

    .line 593
    :cond_0
    return-void
.end method

.method public onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V
    .locals 3
    .param p1, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    .param p2, "accID"    # J
    .param p4, "errCode"    # I

    .prologue
    .line 626
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onError "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Error="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V

    .line 640
    :cond_0
    return-void
.end method

.method public onNewConnection(Lcom/samsung/appcessory/base/SAPBaseAccessory;Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V
    .locals 3
    .param p1, "acc"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .param p2, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 615
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v1, "onNewConnection"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    if-eqz v0, :cond_0

    .line 617
    sget-object v1, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 618
    :try_start_0
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 617
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 620
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    iget-wide v1, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-interface {v0, v1, v2, p2}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onNewConnection(JLcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V

    .line 623
    :cond_0
    return-void

    .line 617
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onPairedStatus(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JZ)V
    .locals 3
    .param p1, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    .param p2, "accID"    # J
    .param p4, "isPaired"    # Z

    .prologue
    .line 606
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onPairedStatus "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mserviceListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 607
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 606
    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onPairedStatus(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JZ)V

    .line 611
    :cond_0
    return-void
.end method

.method public onStatusChange(ILcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V
    .locals 3
    .param p1, "status"    # I
    .param p2, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 597
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onStatusChange "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mserviceListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 598
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 597
    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mserviceListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onStatusChange(ILcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V

    .line 602
    :cond_0
    return-void
.end method

.method public onUsbAccessoryFound(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/appcessory/base/SAPUsbAccessory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 303
    .local p1, "mUsbAccList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/base/SAPUsbAccessory;>;"
    sget-object v2, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v2

    .line 304
    :try_start_0
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v3, "onUsbAccessoryFound enter"

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 306
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onAccessoryFound acc count = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 308
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 309
    :try_start_1
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 310
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 311
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_USB:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-direct {p0, v1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->translateTransportType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 312
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->sendMessage(Landroid/os/Message;)Z

    .line 308
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    :try_start_2
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v3, "onUsbAccessoryFound exit"

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 318
    return-void

    .line 308
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1

    .line 303
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method public onWifiAccessoryFound(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/appcessory/base/SAPWifiAccessory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 322
    .local p1, "mWifiAccList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/base/SAPWifiAccessory;>;"
    sget-object v2, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v2

    .line 323
    :try_start_0
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v3, "onWifiAccessoryFound enter"

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 325
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    .line 326
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onAccessoryFound acc count = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 325
    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mActiveAccList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 329
    :cond_0
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 330
    :try_start_1
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 331
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 332
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_WIFI:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-direct {p0, v1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->translateTransportType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 333
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mSAPAccMngrHndl:Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/base/SAPAccessoryManager$SAPAccMngrHndl;->sendMessage(Landroid/os/Message;)Z

    .line 329
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 337
    :try_start_2
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    const-string/jumbo v3, "onWifiAccessoryFound exit"

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 339
    return-void

    .line 329
    .end local v0    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1

    .line 322
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method public removeAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
    .locals 5
    .param p1, "acc"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 534
    sget-object v2, Lcom/samsung/appcessory/base/SAPAccessoryManager;->msAccListLock:Ljava/lang/Object;

    monitor-enter v2

    .line 535
    :try_start_0
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "removeAccessory AccessoryType : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iget-object v1, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    if-eqz v1, :cond_1

    .line 537
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v3, "Removing BLE Accessory"

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    instance-of v1, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;

    if-eqz v1, :cond_0

    .line 539
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object v0, p1

    check-cast v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-object v1, v0

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->removeAccessory(Lcom/samsung/appcessory/base/SAPBleAccessory;Z)V

    .line 540
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    check-cast p1, Lcom/samsung/appcessory/base/SAPBleAccessory;

    .end local p1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    invoke-virtual {v1, p1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->cancelConnection(Lcom/samsung/appcessory/base/SAPBleAccessory;)V

    .line 534
    :cond_0
    :goto_0
    monitor-exit v2

    .line 556
    return-void

    .line 542
    .restart local p1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_1
    iget-object v1, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    if-eqz v1, :cond_2

    .line 543
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v3, "Removing BTRF Accessory"

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    instance-of v1, p1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    if-eqz v1, :cond_0

    .line 545
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    check-cast p1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .end local p1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    const/4 v3, 0x0

    invoke-virtual {v1, p1, v3}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->removeAccessory(Lcom/samsung/appcessory/base/SAPBtRfAccessory;Z)V

    goto :goto_0

    .line 534
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 549
    .restart local p1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_2
    :try_start_1
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v3, "Type not supported"

    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public startDiscovery(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z
    .locals 4
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 96
    .local v0, "ret":Z
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "startDiscovery. AccessoryType : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    if-eqz v1, :cond_1

    .line 98
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->startBleScan()Z

    move-result v0

    .line 103
    :cond_0
    :goto_0
    return v0

    .line 99
    :cond_1
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->startDiscovery()Z

    move-result v0

    goto :goto_0
.end method

.method public startListening(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z
    .locals 3
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 121
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "startListening enter type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->startListeningOnBtRf()Z

    move-result v0

    .line 126
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopDiscovery(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z
    .locals 4
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 107
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "stopDiscovery enter SAPAccessoryType = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const/4 v0, 0x0

    .line 110
    .local v0, "ret":Z
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    if-eqz v1, :cond_1

    .line 111
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->stopBleScan()Z

    move-result v0

    .line 117
    :cond_0
    :goto_0
    return v0

    .line 112
    :cond_1
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->stopDiscovery()Z

    move-result v0

    goto :goto_0
.end method

.method public stopListening(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z
    .locals 3
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 130
    const-string v0, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "startListening enter type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->stopListeningOnBtRf()Z

    move-result v0

    .line 135
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unpairAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z
    .locals 4
    .param p1, "acc"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 559
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "unpairAccessory. AccessoryType : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    const/4 v0, 0x0

    .line 561
    .local v0, "ret":Z
    iget-object v1, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    if-eqz v1, :cond_1

    .line 562
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v2, "Unpairing BLE Accessory"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    instance-of v1, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;

    if-eqz v1, :cond_0

    .line 564
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    check-cast p1, Lcom/samsung/appcessory/base/SAPBleAccessory;

    .end local p1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    invoke-virtual {v1, p1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->unpairAccessory(Lcom/samsung/appcessory/base/SAPBleAccessory;)Z

    move-result v0

    .line 573
    :cond_0
    :goto_0
    return v0

    .line 566
    .restart local p1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_1
    iget-object v1, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    if-eqz v1, :cond_0

    .line 567
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    const-string v2, "Unpairing BTRF Accessory"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    instance-of v1, p1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    if-eqz v1, :cond_0

    .line 569
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    check-cast p1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .end local p1    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    invoke-virtual {v1, p1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->unpairAccessory(Lcom/samsung/appcessory/base/SAPBtRfAccessory;)Z

    move-result v0

    goto :goto_0
.end method

.method public unpairAccessory(Ljava/lang/String;Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z
    .locals 4
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 577
    const/4 v0, 0x0

    .line 578
    .local v0, "ret":Z
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p2, v1, :cond_1

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    if-eqz v1, :cond_1

    .line 579
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBleManager:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-virtual {v1, p1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->unpairAccessory(Ljava/lang/String;)Z

    move-result v0

    .line 584
    :cond_0
    :goto_0
    const-string v1, "SAP/SAPAccessoryManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "unpairAccessory. AccessoryType : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    return v0

    .line 580
    :cond_1
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p2, v1, :cond_0

    .line 581
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    if-eqz v1, :cond_0

    .line 582
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPAccessoryManager;->mBtRfManager:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v1, p1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->unpairAccessory(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
