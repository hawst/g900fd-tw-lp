.class Lcom/samsung/appcessory/base/SAPBleAccManager$2$1;
.super Ljava/lang/Object;
.source "SAPBleAccManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/appcessory/base/SAPBleAccManager$2;->onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/appcessory/base/SAPBleAccManager$2;

.field private final synthetic val$device:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Lcom/samsung/appcessory/base/SAPBleAccManager$2;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2$1;->this$1:Lcom/samsung/appcessory/base/SAPBleAccManager$2;

    iput-object p2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    .line 547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 550
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "In Thread Starte"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 561
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2$1;->this$1:Lcom/samsung/appcessory/base/SAPBleAccManager$2;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;
    invoke-static {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->access$0(Lcom/samsung/appcessory/base/SAPBleAccManager$2;)Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-result-object v2

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
    invoke-static {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 562
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x3

    iput v2, v1, Landroid/os/Message;->what:I

    .line 563
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 564
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2$1;->this$1:Lcom/samsung/appcessory/base/SAPBleAccManager$2;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;
    invoke-static {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->access$0(Lcom/samsung/appcessory/base/SAPBleAccManager$2;)Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-result-object v2

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
    invoke-static {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->sendMessage(Landroid/os/Message;)Z

    .line 565
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2$1;->this$1:Lcom/samsung/appcessory/base/SAPBleAccManager$2;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;
    invoke-static {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->access$0(Lcom/samsung/appcessory/base/SAPBleAccManager$2;)Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$19(Lcom/samsung/appcessory/base/SAPBleAccManager;Ljava/lang/Thread;)V

    .line 566
    .end local v1    # "msg":Landroid/os/Message;
    :goto_0
    return-void

    .line 554
    :catch_0
    move-exception v0

    .line 556
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "EVT Service thread intrupted return"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 558
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
