.class Lcom/samsung/appcessory/base/SAPBleAccManager$1;
.super Ljava/lang/Object;
.source "SAPBleAccManager.java"

# interfaces
.implements Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPBleAccManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;


# direct methods
.method constructor <init>(Lcom/samsung/appcessory/base/SAPBleAccManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$1;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    .line 299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 8
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "rssi"    # I
    .param p3, "scanRecord"    # [B

    .prologue
    .line 304
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$1;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;
    invoke-static {v5}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$0(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 305
    :try_start_0
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$1;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$1(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 306
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 315
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    .line 317
    .local v4, "name":Ljava/lang/String;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_3

    .line 319
    :cond_1
    const-string v5, "SAP/SAPBleAccManager/29Dec2014"

    const-string v7, "device name is null or length is 0 .Ignore device"

    invoke-static {v5, v7}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    monitor-exit v6

    .line 337
    .end local v4    # "name":Ljava/lang/String;
    :goto_0
    return-void

    .line 307
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 308
    .local v2, "listDev":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 311
    monitor-exit v6

    goto :goto_0

    .line 304
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v2    # "listDev":Landroid/bluetooth/BluetoothDevice;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 323
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    .restart local v4    # "name":Ljava/lang/String;
    :cond_3
    :try_start_1
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$1;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$1(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 325
    const-string v5, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "mGattCallbacks.onLeScan enter deviceName ="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 326
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " address = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 327
    const-string v7, "\nBondState="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 325
    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$1;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
    invoke-static {v5}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    .line 331
    .local v3, "msg":Landroid/os/Message;
    const/4 v5, 0x5

    iput v5, v3, Landroid/os/Message;->what:I

    .line 332
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 333
    .local v0, "b":Landroid/os/Bundle;
    const-string v5, "device"

    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 334
    const-string v5, "informApp"

    const/4 v6, 0x1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 335
    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 336
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$1;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
    invoke-static {v5}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method
