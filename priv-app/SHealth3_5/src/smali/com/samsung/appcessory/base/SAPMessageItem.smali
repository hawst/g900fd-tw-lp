.class public Lcom/samsung/appcessory/base/SAPMessageItem;
.super Ljava/lang/Object;
.source "SAPMessageItem.java"


# instance fields
.field private _accessoryId:J

.field private _message:Lcom/samsung/appcessory/base/SAPMessage;

.field private _sessionId:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 0
    .param p1, "accessoryId"    # J
    .param p3, "sessionId"    # J

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-wide p1, p0, Lcom/samsung/appcessory/base/SAPMessageItem;->_accessoryId:J

    .line 29
    iput-wide p3, p0, Lcom/samsung/appcessory/base/SAPMessageItem;->_sessionId:J

    .line 30
    return-void
.end method


# virtual methods
.method public getAccessoryId()J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/appcessory/base/SAPMessageItem;->_accessoryId:J

    return-wide v0
.end method

.method public getMessage()Lcom/samsung/appcessory/base/SAPMessage;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPMessageItem;->_message:Lcom/samsung/appcessory/base/SAPMessage;

    return-object v0
.end method

.method public getSessionId()J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/samsung/appcessory/base/SAPMessageItem;->_sessionId:J

    return-wide v0
.end method

.method public setMessage(Lcom/samsung/appcessory/base/SAPMessage;)V
    .locals 0
    .param p1, "msg"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPMessageItem;->_message:Lcom/samsung/appcessory/base/SAPMessage;

    .line 34
    return-void
.end method
