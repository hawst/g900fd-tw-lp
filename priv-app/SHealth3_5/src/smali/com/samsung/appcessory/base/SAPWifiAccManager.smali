.class public Lcom/samsung/appcessory/base/SAPWifiAccManager;
.super Ljava/lang/Object;
.source "SAPWifiAccManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPWifiAccManager/29Dec2014"

.field private static final WIFIMNGR_ACC_REMOVED:I = 0x2

.field private static final WIFIMNGR_SEARCHFOR_ACC:I = 0x1

.field private static mAccId:J

.field private static mContext:Landroid/content/Context;

.field private static mWifiMngrHndl:Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;


# instance fields
.field private mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

.field mWifiAccList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/appcessory/base/SAPWifiAccessory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-wide/16 v0, 0x14

    sput-wide v0, Lcom/samsung/appcessory/base/SAPWifiAccManager;->mAccId:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loop"    # Landroid/os/Looper;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, "SAP/SAPWifiAccManager/29Dec2014"

    const-string v1, "SAPWifiAccManager enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPWifiAccManager;->mWifiAccList:Ljava/util/ArrayList;

    .line 38
    new-instance v0, Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;

    invoke-direct {v0, p0, p2}, Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;-><init>(Lcom/samsung/appcessory/base/SAPWifiAccManager;Landroid/os/Looper;)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPWifiAccManager;->mWifiMngrHndl:Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;

    .line 39
    sput-object p1, Lcom/samsung/appcessory/base/SAPWifiAccManager;->mContext:Landroid/content/Context;

    .line 40
    const-string v0, "SAP/SAPWifiAccManager/29Dec2014"

    const-string v1, "SAPWifiAccManager exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/appcessory/base/SAPWifiAccManager;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPWifiAccManager;->search()V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/appcessory/base/SAPWifiAccManager;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPWifiAccManager;->removeAccessory()V

    return-void
.end method

.method private removeAccessory()V
    .locals 2

    .prologue
    .line 102
    const-string v0, "SAP/SAPWifiAccManager/29Dec2014"

    const-string/jumbo v1, "removeAccessory enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const-string v0, "SAP/SAPWifiAccManager/29Dec2014"

    const-string/jumbo v1, "removeAccessory exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    return-void
.end method

.method private search()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPWifiAccManager;->searchAccessory()Z

    .line 70
    return-void
.end method

.method private searchAccessory()Z
    .locals 5

    .prologue
    .line 77
    const-string v3, "SAP/SAPWifiAccManager/29Dec2014"

    const-string/jumbo v4, "searchAccessory enter"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const/4 v0, 0x0

    .line 79
    .local v0, "accFound":Z
    sget-object v3, Lcom/samsung/appcessory/base/SAPWifiAccManager;->mContext:Landroid/content/Context;

    .line 80
    const-string/jumbo v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 79
    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 81
    .local v2, "wifiMngr":Landroid/net/wifi/WifiManager;
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    .line 82
    .local v1, "bWifistate":Z
    if-eqz v1, :cond_0

    .line 87
    const/4 v0, 0x1

    .line 97
    :cond_0
    const-string v3, "SAP/SAPWifiAccManager/29Dec2014"

    const-string/jumbo v4, "searchAccessory exit"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    return v0
.end method


# virtual methods
.method public addDevice(Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPWifiAccessory;
    .locals 6
    .param p1, "ipAddr"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x2026

    .line 55
    const-string v1, "SAP/SAPWifiAccManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "addDevice Wifi "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    new-instance v0, Lcom/samsung/appcessory/base/SAPWifiAccessory;

    .line 57
    invoke-static {}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->generateUniqueId()J

    move-result-wide v1

    move v4, v3

    move-object v5, p1

    .line 56
    invoke-direct/range {v0 .. v5}, Lcom/samsung/appcessory/base/SAPWifiAccessory;-><init>(JIILjava/lang/String;)V

    .line 58
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPWifiAccessory;
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPWifiAccManager;->mWifiAccList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPWifiAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPWifiAccManager;->mWifiAccList:Ljava/util/ArrayList;

    invoke-interface {v1, v2}, Lcom/samsung/appcessory/base/IAccessoryListener;->onWifiAccessoryFound(Ljava/util/ArrayList;)V

    .line 60
    return-object v0
.end method

.method public deinit()V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public init(Lcom/samsung/appcessory/base/IAccessoryListener;)V
    .locals 4
    .param p1, "accMngrLstnr"    # Lcom/samsung/appcessory/base/IAccessoryListener;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPWifiAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    .line 48
    sget-object v1, Lcom/samsung/appcessory/base/SAPWifiAccManager;->mWifiMngrHndl:Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 49
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 50
    sget-object v1, Lcom/samsung/appcessory/base/SAPWifiAccManager;->mWifiMngrHndl:Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 52
    return-void
.end method
