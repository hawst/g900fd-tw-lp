.class public Lcom/samsung/appcessory/base/SAPBleAccessory;
.super Lcom/samsung/appcessory/base/SAPBaseAccessory;
.source "SAPBleAccessory.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPBLEAccessory/29Dec2014"


# instance fields
.field private isConnected:Z

.field private isVisible:Z

.field public mDevice:Landroid/bluetooth/BluetoothDevice;

.field public mReadCharact:Landroid/bluetooth/BluetoothGattCharacteristic;

.field public mWriteCharact:Landroid/bluetooth/BluetoothGattCharacteristic;


# direct methods
.method public constructor <init>(JLandroid/bluetooth/BluetoothDevice;)V
    .locals 2
    .param p1, "accessoryId"    # J
    .param p3, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/samsung/appcessory/base/SAPBaseAccessory;-><init>(J)V

    .line 16
    iput-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->isConnected:Z

    .line 17
    iput-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->isVisible:Z

    .line 21
    const-string v0, "SAP/SAPBLEAccessory/29Dec2014"

    const-string v1, "SAPBleAccessory enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    iput-object p3, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 24
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 25
    const-string v0, "SAP/SAPBLEAccessory/29Dec2014"

    const-string v1, "SAPBleAccessory exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    return-void
.end method


# virtual methods
.method public getDeviceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReadCharac()Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->mReadCharact:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method public getWriteCharac()Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->mWriteCharact:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->isConnected:Z

    return v0
.end method

.method public isPaired()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 53
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-nez v1, :cond_1

    .line 54
    const-string v1, "SAP/SAPBLEAccessory/29Dec2014"

    const-string v2, "isPaired Failed mDevice = null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :cond_0
    :goto_0
    return v0

    .line 57
    :cond_1
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    .line 58
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->isVisible:Z

    return v0
.end method

.method public setConnectedState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->isConnected:Z

    .line 73
    return-void
.end method

.method public setReadCharac(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 0
    .param p1, "charc"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->mReadCharact:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 30
    return-void
.end method

.method public setVisibility(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->isVisible:Z

    .line 81
    return-void
.end method

.method public setWriteCharac(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 0
    .param p1, "charc"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccessory;->mWriteCharact:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 34
    return-void
.end method
