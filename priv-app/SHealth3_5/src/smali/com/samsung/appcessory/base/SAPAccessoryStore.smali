.class public Lcom/samsung/appcessory/base/SAPAccessoryStore;
.super Ljava/lang/Object;
.source "SAPAccessoryStore.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPAccessoryStore/29Dec2014"

.field private static final sAccessoryMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/base/SAPAccessoryStore;->sAccessoryMap:Ljava/util/Map;

    .line 128
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z
    .locals 10
    .param p0, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 37
    iget-wide v2, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    .line 38
    .local v2, "key":J
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryStore;->sAccessoryMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 39
    const-string v7, "SAP/SAPAccessoryStore/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Adding a new accessory with ID: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryStore;->sAccessoryMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v7, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    if-eq v7, v8, :cond_0

    sget-object v7, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DISCONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    if-ne v7, v8, :cond_7

    .line 47
    :cond_0
    invoke-static {p0}, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->openConnection(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z

    move-result v1

    .line 48
    .local v1, "connected":Z
    if-eqz v1, :cond_7

    .line 49
    const-string v6, "SAP/SAPAccessoryStore/29Dec2014"

    const-string v7, "Accessory state connected: "

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    sget-object v6, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_CONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v6, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 94
    .end local v1    # "connected":Z
    :cond_1
    :goto_0
    return v5

    .line 55
    :cond_2
    const-string v7, "SAP/SAPAccessoryStore/29Dec2014"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Attempt to add duplicate accessory (accessory ID: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 56
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 55
    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const-string v7, "SAP/SAPAccessoryStore/29Dec2014"

    .line 59
    const-string v8, "Skip updating the accessory mapping. Retrieving existing accessory ..."

    .line 58
    invoke-static {v7, v8}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    sget-object v7, Lcom/samsung/appcessory/base/SAPAccessoryStore;->sAccessoryMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 62
    .local v0, "cachedAccessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v0, :cond_1

    .line 64
    iget-object v4, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 65
    .local v4, "state":Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;
    sget-object v7, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    if-eq v7, v4, :cond_3

    .line 66
    sget-object v7, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DISCONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    if-ne v7, v4, :cond_5

    .line 69
    :cond_3
    invoke-static {v0}, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->openConnection(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z

    move-result v1

    .line 70
    .restart local v1    # "connected":Z
    if-eqz v1, :cond_4

    .line 71
    sget-object v6, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_CONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v6, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    goto :goto_0

    :cond_4
    move v5, v6

    .line 75
    goto :goto_0

    .line 76
    .end local v1    # "connected":Z
    :cond_5
    sget-object v6, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    if-ne v6, v4, :cond_6

    .line 78
    const-string v6, "SAP/SAPAccessoryStore/29Dec2014"

    .line 79
    const-string v7, "No reserved session found with the retrieved accessory"

    .line 78
    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 80
    :cond_6
    sget-object v6, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_INITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    if-ne v6, v4, :cond_1

    .line 83
    const-string v6, "SAP/SAPAccessoryStore/29Dec2014"

    .line 84
    const-string v7, "Reserved session already established with the accessory. Nothing to do here ..."

    .line 83
    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v0    # "cachedAccessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v4    # "state":Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;
    :cond_7
    move v5, v6

    .line 94
    goto :goto_0
.end method

.method public static getAccessoryById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .locals 3
    .param p0, "id"    # J

    .prologue
    .line 118
    const-string v0, "SAP/SAPAccessoryStore/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getAccessoryById"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    sget-object v0, Lcom/samsung/appcessory/base/SAPAccessoryStore;->sAccessoryMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    const-string v0, "SAP/SAPAccessoryStore/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getAccessoryById found "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    sget-object v0, Lcom/samsung/appcessory/base/SAPAccessoryStore;->sAccessoryMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 123
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static removeAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
    .locals 5
    .param p0, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    .line 99
    .local v0, "key":J
    sget-object v2, Lcom/samsung/appcessory/base/SAPAccessoryStore;->sAccessoryMap:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 104
    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    if-ne v2, v3, :cond_0

    .line 105
    invoke-static {p0}, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->closeConnection(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V

    .line 106
    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DISCONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v2, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 107
    sget-object v2, Lcom/samsung/appcessory/base/SAPAccessoryStore;->sAccessoryMap:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    :goto_0
    return-void

    .line 109
    :cond_0
    const-string v2, "SAP/SAPAccessoryStore/29Dec2014"

    const-string/jumbo v3, "removeAccessory failed"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 112
    :cond_1
    const-string v2, "SAP/SAPAccessoryStore/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Attempt to remove unknown accessory (accessory ID: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 113
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 112
    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
