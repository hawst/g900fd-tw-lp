.class public Lcom/samsung/appcessory/base/SAPMessage;
.super Ljava/lang/Object;
.source "SAPMessage.java"


# static fields
.field public static final BINARY_PAYLOAD:B = 0x1t

.field public static final JSON_PAYLOAD:B = 0x2t


# instance fields
.field private _payload:Ljava/lang/String;

.field private _payloadType:B

.field private _sessionId:J

.field private mBinaryPayload:[B

.field private mStringPayload:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/appcessory/base/SAPMessage;->_sessionId:J

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPMessage;->_payload:Ljava/lang/String;

    .line 26
    const/4 v0, 0x2

    iput-byte v0, p0, Lcom/samsung/appcessory/base/SAPMessage;->_payloadType:B

    .line 27
    return-void
.end method

.method public constructor <init>(J)V
    .locals 0
    .param p1, "sessionId"    # J

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-wide p1, p0, Lcom/samsung/appcessory/base/SAPMessage;->_sessionId:J

    .line 31
    return-void
.end method


# virtual methods
.method public getMessageBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPMessage;->mStringPayload:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageBytes()[B
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPMessage;->mBinaryPayload:[B

    return-object v0
.end method

.method public getSessionId()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/samsung/appcessory/base/SAPMessage;->_sessionId:J

    return-wide v0
.end method

.method public getpayloadType()B
    .locals 1

    .prologue
    .line 55
    iget-byte v0, p0, Lcom/samsung/appcessory/base/SAPMessage;->_payloadType:B

    return v0
.end method

.method public setMessageBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "payload"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPMessage;->mStringPayload:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setMessageBytes([B)V
    .locals 3
    .param p1, "payload"    # [B

    .prologue
    const/4 v2, 0x0

    .line 46
    array-length v0, p1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPMessage;->mBinaryPayload:[B

    .line 47
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPMessage;->mBinaryPayload:[B

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 48
    return-void
.end method

.method public setpayloadType(B)V
    .locals 0
    .param p1, "_payloadType"    # B

    .prologue
    .line 59
    iput-byte p1, p0, Lcom/samsung/appcessory/base/SAPMessage;->_payloadType:B

    .line 60
    return-void
.end method
