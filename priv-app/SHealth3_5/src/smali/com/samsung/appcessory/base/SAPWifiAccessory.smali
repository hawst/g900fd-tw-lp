.class public Lcom/samsung/appcessory/base/SAPWifiAccessory;
.super Lcom/samsung/appcessory/base/SAPBaseAccessory;
.source "SAPWifiAccessory.java"


# static fields
.field private static final DEFAULT_IN_PORT:I = 0x2026

.field private static final DEFAULT_OUT_PORT:I = 0x2026


# instance fields
.field private host:Ljava/lang/String;

.field private inPort:I

.field private outPort:I


# direct methods
.method public constructor <init>(J)V
    .locals 2
    .param p1, "accessoryId"    # J

    .prologue
    const/16 v1, 0x2026

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/samsung/appcessory/base/SAPBaseAccessory;-><init>(J)V

    .line 26
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_WIFI:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 28
    iput v1, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->inPort:I

    .line 29
    iput v1, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->outPort:I

    .line 30
    return-void
.end method

.method public constructor <init>(JIILjava/lang/String;)V
    .locals 1
    .param p1, "accessoryId"    # J
    .param p3, "_in"    # I
    .param p4, "_out"    # I
    .param p5, "_host"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/samsung/appcessory/base/SAPBaseAccessory;-><init>(J)V

    .line 43
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_WIFI:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 45
    iput p3, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->inPort:I

    .line 46
    iput p4, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->outPort:I

    .line 47
    iput-object p5, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->host:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 2
    .param p1, "accessoryId"    # J
    .param p3, "_host"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0x2026

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/samsung/appcessory/base/SAPBaseAccessory;-><init>(J)V

    .line 34
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_WIFI:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 36
    iput v1, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->inPort:I

    .line 37
    iput v1, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->outPort:I

    .line 38
    iput-object p3, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->host:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->host:Ljava/lang/String;

    return-object v0
.end method

.method public getInputPort()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->inPort:I

    return v0
.end method

.method public getOutputPort()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->outPort:I

    return v0
.end method

.method public setHost(Ljava/lang/String;)V
    .locals 0
    .param p1, "_host"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPWifiAccessory;->host:Ljava/lang/String;

    .line 64
    return-void
.end method
