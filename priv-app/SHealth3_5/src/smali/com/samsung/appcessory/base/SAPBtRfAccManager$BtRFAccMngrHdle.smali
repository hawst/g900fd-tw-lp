.class Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
.super Landroid/os/Handler;
.source "SAPBtRfAccManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPBtRfAccManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BtRFAccMngrHdle"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;


# direct methods
.method public constructor <init>(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    .line 95
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 96
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v11, 0x1

    .line 100
    const-string v8, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "BtRFAccMngrHdle.handleMessage enter what = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    iget v7, p1, Landroid/os/Message;->what:I

    .line 103
    .local v7, "what":I
    packed-switch v7, :pswitch_data_0

    .line 167
    :cond_0
    :goto_0
    :pswitch_0
    const-string v8, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v9, "BtRFAccMngrHdle.handleMessage exit"

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    return-void

    .line 105
    :pswitch_1
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v8}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->stopListeningOnBtRf()Z

    .line 106
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$0(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v8

    const/4 v9, 0x0

    sget-object v10, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-interface {v8, v9, v10}, Lcom/samsung/appcessory/base/IAccessoryListener;->onStatusChange(ILcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V

    goto :goto_0

    .line 110
    :pswitch_2
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$0(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v8

    .line 111
    sget-object v9, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 110
    invoke-interface {v8, v11, v9}, Lcom/samsung/appcessory/base/IAccessoryListener;->onStatusChange(ILcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V

    .line 113
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$1(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v8

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v2

    .line 114
    .local v2, "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v2, :cond_0

    .line 115
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 116
    .local v5, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 117
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    .line 118
    .local v3, "dev":Landroid/bluetooth/BluetoothDevice;
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v9

    # invokes: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->isHealthDevice(Ljava/lang/String;)Z
    invoke-static {v8, v9}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 119
    const-string v8, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v9, "found health device. listen rfcomm"

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v8}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->startListeningOnBtRf()Z

    goto :goto_0

    .line 128
    .end local v2    # "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v3    # "dev":Landroid/bluetooth/BluetoothDevice;
    .end local v5    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :pswitch_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 129
    .local v1, "b":Landroid/os/Bundle;
    const-string v8, "device"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 130
    .local v4, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v4, :cond_2

    .line 131
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # invokes: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->handleNewAcc(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v8, v4}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$3(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    .line 133
    :cond_2
    const-string v8, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v9, "Device is null"

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 138
    .end local v1    # "b":Landroid/os/Bundle;
    .end local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    :pswitch_4
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 139
    .restart local v1    # "b":Landroid/os/Bundle;
    const-string v8, "id"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 140
    .local v0, "accId":Ljava/lang/Long;
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$0(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v8

    invoke-interface {v8, v0, v11}, Lcom/samsung/appcessory/base/IAccessoryListener;->onConnectStateChange(Ljava/lang/Long;I)V

    goto/16 :goto_0

    .line 144
    .end local v0    # "accId":Ljava/lang/Long;
    .end local v1    # "b":Landroid/os/Bundle;
    :pswitch_5
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$1(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v8

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v2

    .line 146
    .restart local v2    # "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v2, :cond_0

    .line 147
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 148
    .restart local v5    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 149
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    .line 150
    .restart local v3    # "dev":Landroid/bluetooth/BluetoothDevice;
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # invokes: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->handleNewAcc(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v8, v3}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$3(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_1

    .line 158
    .end local v2    # "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v3    # "dev":Landroid/bluetooth/BluetoothDevice;
    .end local v5    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :pswitch_6
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Landroid/bluetooth/BluetoothSocket;

    .line 159
    .local v6, "socket":Landroid/bluetooth/BluetoothSocket;
    invoke-virtual {v6}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    .line 160
    .restart local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # invokes: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->handleNewAcc(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothSocket;)V
    invoke-static {v8, v4, v6}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$4(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothSocket;)V

    goto/16 :goto_0

    .line 103
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_4
    .end packed-switch
.end method
