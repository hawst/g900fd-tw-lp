.class public Lcom/samsung/appcessory/base/SAPBtRfAccessory;
.super Lcom/samsung/appcessory/base/SAPBaseAccessory;
.source "SAPBtRfAccessory.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPBtRfAccessory/29Dec2014"


# instance fields
.field private isConnected:Z

.field public mAccIntStream:Ljava/io/InputStream;

.field public mAccOutStream:Ljava/io/OutputStream;

.field public mBtSocket:Landroid/bluetooth/BluetoothSocket;

.field public mDevice:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method public constructor <init>(Landroid/content/Context;JLandroid/bluetooth/BluetoothDevice;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "accessoryId"    # J
    .param p4, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 24
    invoke-direct {p0, p2, p3}, Lcom/samsung/appcessory/base/SAPBaseAccessory;-><init>(J)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->isConnected:Z

    .line 25
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 26
    iput-object p4, p0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 27
    return-void
.end method


# virtual methods
.method public isConnected()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->isConnected:Z

    return v0
.end method

.method public isPaired()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 30
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-nez v1, :cond_1

    .line 31
    const-string v1, "SAP/SAPBtRfAccessory/29Dec2014"

    const-string v2, "isPaired Failed mDevice = null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    :cond_0
    :goto_0
    return v0

    .line 34
    :cond_1
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    .line 35
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setConnectedState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->isConnected:Z

    .line 50
    return-void
.end method
