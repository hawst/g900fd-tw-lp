.class Lcom/samsung/appcessory/base/SAPBleAccManager$3;
.super Landroid/content/BroadcastReceiver;
.source "SAPBleAccManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPBleAccManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;


# direct methods
.method constructor <init>(Lcom/samsung/appcessory/base/SAPBleAccManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    .line 939
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 942
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 943
    .local v2, "action":Ljava/lang/String;
    move-object v5, p2

    .line 944
    .local v5, "mIntent":Landroid/content/Intent;
    const-string v8, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "onReceive. Intent.getAction "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 945
    const-string v8, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 948
    const-string v8, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v5, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    .line 949
    .local v3, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v3, :cond_0

    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 950
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # invokes: Lcom/samsung/appcessory/base/SAPBleAccManager;->isBLEDevice(Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v8, v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$20(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 951
    const-string v8, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "BluetoothDevice.ACTION_BOND_STATE_CHANGED  :: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 952
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 951
    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 953
    const/4 v1, 0x0

    .line 954
    .local v1, "accDev":Lcom/samsung/appcessory/base/SAPBleAccessory;
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$0(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 955
    :try_start_0
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$17(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 956
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 954
    :goto_1
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 969
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v8

    const/16 v9, 0xc

    if-ne v8, v9, :cond_3

    .line 982
    if-eqz v1, :cond_0

    .line 983
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v8

    sget-object v9, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iget-wide v10, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    const/4 v12, 0x1

    invoke-interface {v8, v9, v10, v11, v12}, Lcom/samsung/appcessory/base/IAccessoryListener;->onPairedStatus(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JZ)V

    .line 1022
    .end local v1    # "accDev":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .end local v3    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :cond_0
    :goto_2
    return-void

    .line 957
    .restart local v1    # "accDev":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .restart local v3    # "device":Landroid/bluetooth/BluetoothDevice;
    .restart local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :cond_1
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-object v1, v0

    .line 958
    iget-object v8, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    .line 959
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v10

    .line 958
    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 959
    if-eqz v8, :cond_2

    .line 960
    const-string v8, "SAP/SAPBleAccManager/29Dec2014"

    .line 961
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "ACTION_BOND_STATE_CHANGED"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 962
    iget-wide v11, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Device Name="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 963
    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPBleAccessory;->getDeviceName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 961
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 960
    invoke-static {v8, v10}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 954
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 966
    .restart local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 986
    :cond_3
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v8

    const/16 v9, 0xa

    if-ne v8, v9, :cond_0

    .line 987
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->connectedDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$22(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 988
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->connectedDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$22(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v8

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 989
    const-string v8, "SAP/SAPBleAccManager/29Dec2014"

    const-string v9, "My Connected Device Bond State = BOND_NONE"

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    :cond_4
    :goto_3
    if-eqz v1, :cond_0

    .line 997
    const-string v8, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "BluetoothDevice.BOND_NONE"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v10, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 998
    const-string v10, " Device Name="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPBleAccessory;->getDeviceName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 997
    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v8

    sget-object v9, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iget-wide v10, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    const/4 v12, 0x0

    invoke-interface {v8, v9, v10, v11, v12}, Lcom/samsung/appcessory/base/IAccessoryListener;->onPairedStatus(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JZ)V

    goto/16 :goto_2

    .line 991
    :cond_5
    const-string v8, "SAP/SAPBleAccManager/29Dec2014"

    const-string v9, "Not My Accessory"

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 1003
    .end local v1    # "accDev":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .end local v3    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :cond_6
    const-string v8, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1004
    const-string v8, "android.bluetooth.adapter.extra.STATE"

    .line 1005
    const/4 v9, 0x0

    .line 1004
    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 1006
    .local v7, "state":I
    const-string v8, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ACTION_STATE_CHANGED new state "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->obtainMessage()Landroid/os/Message;

    move-result-object v6

    .line 1008
    .local v6, "msg":Landroid/os/Message;
    const/16 v8, 0xa

    if-ne v7, v8, :cond_7

    .line 1009
    const/4 v8, 0x0

    iput v8, v6, Landroid/os/Message;->what:I

    .line 1010
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_2

    .line 1011
    :cond_7
    const/16 v8, 0xc

    if-ne v7, v8, :cond_0

    .line 1012
    const/4 v8, 0x1

    iput v8, v6, Landroid/os/Message;->what:I

    .line 1013
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$3;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
    invoke-static {v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_2
.end method
