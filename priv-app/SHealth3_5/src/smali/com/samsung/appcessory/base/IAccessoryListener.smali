.class public interface abstract Lcom/samsung/appcessory/base/IAccessoryListener;
.super Ljava/lang/Object;
.source "IAccessoryListener.java"


# virtual methods
.method public abstract onAccessoryDisconnected(Ljava/lang/Long;)V
.end method

.method public abstract onAccessoryLost(Ljava/lang/Long;)V
.end method

.method public abstract onBleAccessoryFound(Lcom/samsung/appcessory/base/SAPBleAccessory;Z)V
.end method

.method public abstract onBtRfAccessoryFound(Lcom/samsung/appcessory/base/SAPBtRfAccessory;)V
.end method

.method public abstract onConnectStateChange(Ljava/lang/Long;I)V
.end method

.method public abstract onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V
.end method

.method public abstract onNewConnection(Lcom/samsung/appcessory/base/SAPBaseAccessory;Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V
.end method

.method public abstract onPairedStatus(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JZ)V
.end method

.method public abstract onStatusChange(ILcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V
.end method

.method public abstract onUsbAccessoryFound(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/appcessory/base/SAPUsbAccessory;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onWifiAccessoryFound(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/appcessory/base/SAPWifiAccessory;",
            ">;)V"
        }
    .end annotation
.end method
