.class public Lcom/samsung/appcessory/base/SAPBaseAccessory;
.super Ljava/lang/Object;
.source "SAPBaseAccessory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;,
        Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    }
.end annotation


# instance fields
.field public _id:J

.field public _state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

.field public _type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    .line 51
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 52
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "acessoryId"    # J

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-wide p1, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    .line 46
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 47
    return-void
.end method


# virtual methods
.method public getAccessoryID()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    return-wide v0
.end method
