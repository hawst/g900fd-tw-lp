.class Lcom/samsung/appcessory/base/SAPBleAccManager$2;
.super Landroid/bluetooth/BluetoothGattCallback;
.source "SAPBleAccManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPBleAccManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;


# direct methods
.method constructor <init>(Lcom/samsung/appcessory/base/SAPBleAccManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    .line 340
    invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/appcessory/base/SAPBleAccManager$2;)Lcom/samsung/appcessory/base/SAPBleAccManager;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    return-object v0
.end method


# virtual methods
.method public onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 2
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 344
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v1, "onCharacteristicChanged enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$3(Lcom/samsung/appcessory/base/SAPBleAccManager;Z)V

    .line 346
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mSAPBleconnection:Lcom/samsung/appcessory/transport/SAPBleconnection;
    invoke-static {v0}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$4(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/transport/SAPBleconnection;

    move-result-object v0

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/transport/SAPBleconnection;->read([B)V

    .line 347
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v1, "onCharacteristicChanged exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    return-void
.end method

.method public onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 2
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "status"    # I

    .prologue
    .line 352
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v1, "onCharacteristicRead: "

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    return-void
.end method

.method public onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 2
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "status"    # I

    .prologue
    .line 357
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v1, "onCharacteristicWrite"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    return-void
.end method

.method public onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 9
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "status"    # I
    .param p3, "newState"    # I

    .prologue
    const/4 v7, 0x0

    .line 452
    const-string v4, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "mGattCallbacks.onConnectionStateChange Enter status ="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 453
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " newState="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 452
    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    if-eqz p2, :cond_1

    const/16 v4, 0x8

    if-eq p2, v4, :cond_1

    .line 457
    const-string v4, "SAP/SAPBleAccManager/29Dec2014"

    const-string v5, "error in connecting, close the BluetoothGatt instance"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 460
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v4

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 461
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-static {v4, v7}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$16(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothGatt;)V

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 467
    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    .line 469
    .local v2, "device":Landroid/bluetooth/BluetoothDevice;
    const/4 v4, 0x2

    if-ne p3, v4, :cond_5

    .line 470
    const-string v4, "SAP/SAPBleAccManager/29Dec2014"

    .line 471
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "mGattCallbacks.onConnectionStateChange discoverServices() Called BondState="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 472
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 471
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 470
    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$0(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 475
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$17(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_3

    .line 474
    :goto_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v4

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothGatt;->discoverServices()Z

    .line 511
    .end local v3    # "i":I
    :cond_2
    :goto_3
    const-string v4, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "mGattCallbacks.onConnectionStateChange exit "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 476
    .restart local v3    # "i":I
    :cond_3
    :try_start_1
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$17(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 477
    .local v1, "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    iget-object v4, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 478
    const-string v4, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Accessory Connected "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v7, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/samsung/appcessory/base/SAPBleAccessory;->setConnectedState(Z)V

    goto :goto_2

    .line 474
    .end local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 475
    .restart local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 486
    .end local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .end local v3    # "i":I
    :cond_5
    if-nez p3, :cond_2

    .line 487
    const-string v4, "SAP/SAPBleAccManager/29Dec2014"

    const-string v5, "BluetoothGatt.STATE_DISCONNECTED"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->evtServiceThread:Ljava/lang/Thread;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$18(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Thread;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 490
    const-string v4, "SAP/SAPBleAccManager/29Dec2014"

    const-string v5, "Ongoing discoverServices thread interrupted"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->evtServiceThread:Ljava/lang/Thread;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$18(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    .line 492
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-static {v4, v7}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$19(Lcom/samsung/appcessory/base/SAPBleAccManager;Ljava/lang/Thread;)V

    .line 494
    :cond_6
    const/4 v1, 0x0

    .line 495
    .restart local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$0(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 496
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_4
    :try_start_2
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$17(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_7

    .line 495
    :goto_5
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 505
    if-eqz v1, :cond_2

    .line 507
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5}, Lcom/samsung/appcessory/base/SAPBleAccManager;->removeAccessory(Lcom/samsung/appcessory/base/SAPBleAccessory;Z)V

    goto/16 :goto_3

    .line 497
    :cond_7
    :try_start_3
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$17(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-object v1, v0

    .line 498
    iget-object v4, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 499
    const-string v4, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Accessory Removed =>"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v7, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 495
    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v4

    .line 502
    :cond_8
    const/4 v1, 0x0

    .line 496
    add-int/lit8 v3, v3, 0x1

    goto :goto_4
.end method

.method public onDescriptorRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 6
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "descriptor"    # Landroid/bluetooth/BluetoothGattDescriptor;
    .param p3, "status"    # I

    .prologue
    const/4 v5, 0x1

    .line 362
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v4, "onDescriptorRead "

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    if-eqz p3, :cond_0

    .line 367
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 368
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;
    invoke-static {v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$6(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    # invokes: Lcom/samsung/appcessory/base/SAPBleAccManager;->handleAutoConnectRequest(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v3, v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$7(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 369
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "error in desc read ,status = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :goto_0
    return-void

    .line 372
    :cond_0
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattDescriptor;->getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v0

    .line 374
    .local v0, "chart":Landroid/bluetooth/BluetoothGattCharacteristic;
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$8(Lcom/samsung/appcessory/base/SAPBleAccManager;I)V

    .line 376
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mCharAttr:I
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$9(Lcom/samsung/appcessory/base/SAPBleAccManager;)I

    move-result v3

    and-int/lit8 v3, v3, 0x10

    if-eqz v3, :cond_2

    .line 377
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string v4, "Set isNoti true "

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-static {v3, v5}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$10(Lcom/samsung/appcessory/base/SAPBleAccManager;Z)V

    .line 384
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v3

    invoke-virtual {v3, v0, v5}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    move-result v2

    .line 386
    .local v2, "ret":Z
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "onDescriptorRead UUID="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ret="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 387
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 386
    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    if-nez v2, :cond_3

    .line 389
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v4, "setCharacteristicNotification failed"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 379
    .end local v2    # "ret":Z
    :cond_2
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mCharAttr:I
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$9(Lcom/samsung/appcessory/base/SAPBleAccManager;)I

    move-result v3

    and-int/lit8 v3, v3, 0x20

    if-eqz v3, :cond_1

    .line 380
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string v4, "Set isIndicate true "

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    invoke-static {v3, v5}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$11(Lcom/samsung/appcessory/base/SAPBleAccManager;Z)V

    goto :goto_1

    .line 394
    .restart local v2    # "ret":Z
    :cond_3
    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_DESC1:Ljava/util/UUID;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$12()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v1

    .line 395
    .local v1, "clientConfig":Landroid/bluetooth/BluetoothGattDescriptor;
    if-nez v1, :cond_4

    .line 396
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v4, "onDescriptorRead  clientConfig == null"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 399
    :cond_4
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->isIndicate:Z
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$13(Lcom/samsung/appcessory/base/SAPBleAccManager;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 401
    sget-object v3, Landroid/bluetooth/BluetoothGattDescriptor;->ENABLE_INDICATION_VALUE:[B

    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    move-result v2

    .line 402
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ENABLE_INDICATION_VALUE  ret="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/bluetooth/BluetoothGatt;->writeDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Z

    move-result v2

    .line 404
    if-nez v2, :cond_5

    .line 405
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string v4, " write desc failed"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :cond_5
    :goto_2
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v4, "onDescriptorRead  mBTGatt.writeDescriptor Called"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "onDescriptorRead exit ret="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 407
    :cond_6
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->isNoti:Z
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$14(Lcom/samsung/appcessory/base/SAPBleAccManager;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 409
    sget-object v3, Landroid/bluetooth/BluetoothGattDescriptor;->ENABLE_NOTIFICATION_VALUE:[B

    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    move-result v2

    .line 410
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ENABLE_NOTIFICATION_VALUE  ret="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/bluetooth/BluetoothGatt;->writeDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Z

    move-result v2

    .line 412
    if-nez v2, :cond_5

    .line 413
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string v4, " write desc failed"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 416
    :cond_7
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string v4, "Indicate or Noti is not set. So just return"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 4
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "descriptor"    # Landroid/bluetooth/BluetoothGattDescriptor;
    .param p3, "status"    # I

    .prologue
    .line 429
    if-eqz p3, :cond_0

    .line 432
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 433
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;
    invoke-static {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$6(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    # invokes: Lcom/samsung/appcessory/base/SAPBleAccManager;->handleAutoConnectRequest(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v1, v2}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$7(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 434
    const-string v1, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error in desc write ,status= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    :goto_0
    return-void

    .line 438
    :cond_0
    const-string v1, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v2, "onDescriptorWrite"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->isWriteEnabled:Z
    invoke-static {v1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$15(Lcom/samsung/appcessory/base/SAPBleAccManager;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 441
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;
    invoke-static {v1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$6(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-result-object v1

    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_CONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v2, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 443
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
    invoke-static {v1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 444
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->what:I

    .line 445
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
    invoke-static {v1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$2(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->sendMessage(Landroid/os/Message;)Z

    .line 447
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    const-string v1, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v2, "onDescriptorWrite exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onReadRemoteRssi(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 0
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "rssi"    # I
    .param p3, "status"    # I

    .prologue
    .line 516
    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onReadRemoteRssi(Landroid/bluetooth/BluetoothGatt;II)V

    .line 517
    return-void
.end method

.method public onReliableWriteCompleted(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 0
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "status"    # I

    .prologue
    .line 521
    invoke-super {p0, p1, p2}, Landroid/bluetooth/BluetoothGattCallback;->onReliableWriteCompleted(Landroid/bluetooth/BluetoothGatt;I)V

    .line 522
    return-void
.end method

.method public onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 5
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "status"    # I

    .prologue
    .line 526
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 527
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "mGattCallbacks.onServicesDiscovered enter status = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 528
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " device="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 527
    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothGatt;->getServices()Ljava/util/List;

    move-result-object v1

    .line 531
    .local v1, "services":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothGattService;>;"
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "services discovered .. service count = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 532
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 531
    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p2, :cond_1

    .line 536
    :cond_0
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "error in finding services, hence disconnecting"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 538
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # invokes: Lcom/samsung/appcessory/base/SAPBleAccManager;->handleAutoConnectRequest(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v2, v0}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$7(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 571
    :goto_0
    return-void

    .line 541
    :cond_1
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BondState="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->evtServiceThread:Ljava/lang/Thread;
    invoke-static {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$18(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Thread;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 544
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "Previous thread interrupted"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->evtServiceThread:Ljava/lang/Thread;
    invoke-static {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$18(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 547
    :cond_2
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/samsung/appcessory/base/SAPBleAccManager$2$1;

    invoke-direct {v4, p0, v0}, Lcom/samsung/appcessory/base/SAPBleAccManager$2$1;-><init>(Lcom/samsung/appcessory/base/SAPBleAccManager$2;Landroid/bluetooth/BluetoothDevice;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-static {v2, v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$19(Lcom/samsung/appcessory/base/SAPBleAccManager;Ljava/lang/Thread;)V

    .line 568
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$2;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->evtServiceThread:Ljava/lang/Thread;
    invoke-static {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$18(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 570
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v3, "mGattCallbacks.onServicesDiscovered exit"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
