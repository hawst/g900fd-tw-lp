.class public Lcom/samsung/appcessory/base/SAPUsbAccessory;
.super Lcom/samsung/appcessory/base/SAPBaseAccessory;
.source "SAPUsbAccessory.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPUsbAccessory/29Dec2014"


# instance fields
.field public mAccBufferdInStream:Ljava/io/BufferedInputStream;

.field public mAccBufferdOutStream:Ljava/io/BufferedOutputStream;

.field public mAccFileIntStream:Ljava/io/FileInputStream;

.field public mAccFileOutStream:Ljava/io/FileOutputStream;

.field public mManuName:Ljava/lang/String;

.field public mModel:Ljava/lang/String;

.field public mSerial:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "accessoryId"    # J
    .param p4, "manuName"    # Ljava/lang/String;
    .param p5, "model"    # Ljava/lang/String;
    .param p6, "serial"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p2, p3}, Lcom/samsung/appcessory/base/SAPBaseAccessory;-><init>(J)V

    .line 47
    const-string v0, "SAP/SAPUsbAccessory/29Dec2014"

    const-string v1, "SAPUsbAccessory.SAPUsbAccessory enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const-string v0, "Samsung"

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPUsbAccessory;->mManuName:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Lcom/samsung/appcessory/base/SAPUsbAccessory;->mModel:Ljava/lang/String;

    .line 50
    iput-object p6, p0, Lcom/samsung/appcessory/base/SAPUsbAccessory;->mSerial:Ljava/lang/String;

    .line 51
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_USB:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPUsbAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 53
    const-string v0, "SAP/SAPUsbAccessory/29Dec2014"

    const-string v1, "SAPUsbAccessory.SAPUsbAccessory exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    return-void
.end method
