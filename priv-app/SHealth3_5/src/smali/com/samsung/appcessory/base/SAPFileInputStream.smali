.class Lcom/samsung/appcessory/base/SAPFileInputStream;
.super Ljava/io/FileInputStream;
.source "SAPUsbAccManager.java"


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/FileDescriptor;)V
    .locals 1
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 296
    invoke-direct {p0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 293
    const-string v0, "SAP/SAPFileInputStream/29Dec2014"

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPFileInputStream;->TAG:Ljava/lang/String;

    .line 298
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPFileInputStream;->TAG:Ljava/lang/String;

    const-string v1, "closing SAPFileInputStream"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 308
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPFileInputStream;->TAG:Ljava/lang/String;

    const-string v1, "finalize of SAPFileInputStream**********"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    invoke-super {p0}, Ljava/io/FileInputStream;->finalize()V

    .line 310
    return-void
.end method
