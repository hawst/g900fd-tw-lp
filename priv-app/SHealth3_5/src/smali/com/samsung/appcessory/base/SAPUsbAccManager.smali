.class public Lcom/samsung/appcessory/base/SAPUsbAccManager;
.super Ljava/lang/Object;
.source "SAPUsbAccManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/base/SAPUsbAccManager$SearchTask;,
        Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;
    }
.end annotation


# static fields
.field public static final ACTION_USB_ACC_FOUND:Ljava/lang/String; = "com.samsung.sap.action.usbAccFound"

.field public static final ACTION_USB_ACC_PERM_DENIED:Ljava/lang/String; = "com.samsung.sap.action.permdenied"

.field public static final ACTION_USB_ACC_REMOVED:Ljava/lang/String; = "com.samsung.sap.action.usbAccRemoved"

.field private static final ACTION_USB_PERMISSION:Ljava/lang/String; = "com.android.acessorychat.action.USB_PERMISSION"

.field private static final TAG:Ljava/lang/String; = "SAP/SAPUsbAccManager/29Dec2014"

.field private static final USBMNGR_ACC_REMOVED:I = 0x3

.field private static final USBMNGR_PERMISSION_DENIED:I = 0x2

.field private static final USBMNGR_SEARCHFOR_ACC:I = 0x1

.field private static mAccId:J

.field private static mContext:Landroid/content/Context;

.field private static mManuName:Ljava/lang/String;

.field private static mPermissionIntent:Landroid/app/PendingIntent;

.field private static mUsbAccList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/appcessory/base/SAPUsbAccessory;",
            ">;"
        }
    .end annotation
.end field

.field private static mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;


# instance fields
.field private mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

.field private mUsbReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const-string v0, "Samsung"

    sput-object v0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mManuName:Ljava/lang/String;

    .line 47
    const-wide/16 v0, 0x16

    sput-wide v0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mAccId:J

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loop"    # Landroid/os/Looper;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 250
    new-instance v0, Lcom/samsung/appcessory/base/SAPUsbAccManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/appcessory/base/SAPUsbAccManager$1;-><init>(Lcom/samsung/appcessory/base/SAPUsbAccManager;)V

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbReceiver:Landroid/content/BroadcastReceiver;

    .line 58
    const-string v0, "SAP/SAPUsbAccManager/29Dec2014"

    const-string v1, "SAPUsbAccManager enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbAccList:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    invoke-direct {v0, p0, p2}, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;-><init>(Lcom/samsung/appcessory/base/SAPUsbAccManager;Landroid/os/Looper;)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    .line 61
    sput-object p1, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mContext:Landroid/content/Context;

    .line 62
    const-string v0, "SAP/SAPUsbAccManager/29Dec2014"

    const-string v1, "SAPUsbAccManager exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    return-void
.end method

.method static synthetic access$0()Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/appcessory/base/SAPUsbAccManager;)V
    .locals 0

    .prologue
    .line 213
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->search()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/appcessory/base/SAPUsbAccManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0, p1, p2}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->removeAccessory(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/appcessory/base/SAPUsbAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/appcessory/base/SAPUsbAccManager;)Z
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->searchAccessory()Z

    move-result v0

    return v0
.end method

.method private addAccessory(Landroid/hardware/usb/UsbAccessory;)Z
    .locals 1
    .param p1, "acc"    # Landroid/hardware/usb/UsbAccessory;

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method private removeAccessory(Lcom/samsung/appcessory/base/SAPUsbAccessory;)V
    .locals 3
    .param p1, "acc"    # Lcom/samsung/appcessory/base/SAPUsbAccessory;

    .prologue
    .line 205
    const-string v0, "SAP/SAPUsbAccManager/29Dec2014"

    const-string/jumbo v1, "removeAccessory enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    sget-object v0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbAccList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 208
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    iget-wide v1, p1, Lcom/samsung/appcessory/base/SAPUsbAccessory;->_id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/appcessory/base/IAccessoryListener;->onAccessoryLost(Ljava/lang/Long;)V

    .line 210
    :cond_0
    const-string v0, "SAP/SAPUsbAccManager/29Dec2014"

    const-string/jumbo v1, "removeAccessory exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    return-void
.end method

.method private removeAccessory(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "manu"    # Ljava/lang/String;
    .param p2, "serial"    # Ljava/lang/String;

    .prologue
    .line 183
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    const-string/jumbo v3, "removeAccessory enter"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    sget-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbAccList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbAccList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 185
    :cond_0
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    const-string/jumbo v3, "removeAccessory size is 0,exit"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :goto_0
    return-void

    .line 189
    :cond_1
    sget-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbAccList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 190
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPUsbAccessory;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 201
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    const-string/jumbo v3, "removeAccessory exit"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 191
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPUsbAccessory;

    .line 192
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPUsbAccessory;
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "manu"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "serial"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "acc.mManuName"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/samsung/appcessory/base/SAPUsbAccessory;->mManuName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "acc.mSerial"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/samsung/appcessory/base/SAPUsbAccessory;->mSerial:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v2, v0, Lcom/samsung/appcessory/base/SAPUsbAccessory;->mManuName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/samsung/appcessory/base/SAPUsbAccessory;->mSerial:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 198
    invoke-direct {p0, v0}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->removeAccessory(Lcom/samsung/appcessory/base/SAPUsbAccessory;)V

    goto :goto_1
.end method

.method private search()V
    .locals 2

    .prologue
    .line 214
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/appcessory/base/SAPUsbAccManager$SearchTask;

    invoke-direct {v1, p0}, Lcom/samsung/appcessory/base/SAPUsbAccManager$SearchTask;-><init>(Lcom/samsung/appcessory/base/SAPUsbAccManager;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 215
    .local v0, "searchThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 217
    return-void
.end method

.method private searchAccessory()Z
    .locals 17

    .prologue
    .line 97
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    const-string/jumbo v3, "searchAccessory enter"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const/4 v8, 0x0

    .line 99
    .local v8, "accFound":Z
    sget-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mContext:Landroid/content/Context;

    .line 100
    const-string/jumbo v3, "usb"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    .line 99
    check-cast v14, Landroid/hardware/usb/UsbManager;

    .line 101
    .local v14, "usbManager":Landroid/hardware/usb/UsbManager;
    invoke-virtual {v14}, Landroid/hardware/usb/UsbManager;->getAccessoryList()[Landroid/hardware/usb/UsbAccessory;

    move-result-object v9

    .line 103
    .local v9, "accList":[Landroid/hardware/usb/UsbAccessory;
    if-eqz v9, :cond_5

    .line 104
    array-length v0, v9

    move/from16 v16, v0

    const/4 v2, 0x0

    move v15, v2

    :goto_0
    move/from16 v0, v16

    if-lt v15, v0, :cond_1

    .line 159
    :goto_1
    if-eqz v8, :cond_0

    .line 160
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    sget-object v3, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbAccList:Ljava/util/ArrayList;

    invoke-interface {v2, v3}, Lcom/samsung/appcessory/base/IAccessoryListener;->onUsbAccessoryFound(Ljava/util/ArrayList;)V

    .line 166
    :cond_0
    :goto_2
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    const-string/jumbo v3, "searchAccessory exit"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    return v8

    .line 104
    :cond_1
    aget-object v13, v9, v15

    .line 105
    .local v13, "usbAccessory":Landroid/hardware/usb/UsbAccessory;
    invoke-virtual {v13}, Landroid/hardware/usb/UsbAccessory;->getManufacturer()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mManuName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 107
    invoke-virtual {v14, v13}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbAccessory;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 108
    const/4 v1, 0x0

    .line 109
    .local v1, "usbAcc":Lcom/samsung/appcessory/base/SAPUsbAccessory;
    new-instance v1, Lcom/samsung/appcessory/base/SAPUsbAccessory;

    .end local v1    # "usbAcc":Lcom/samsung/appcessory/base/SAPUsbAccessory;
    sget-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mContext:Landroid/content/Context;

    .line 110
    invoke-static {}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->generateUniqueId()J

    move-result-wide v3

    .line 111
    invoke-virtual {v13}, Landroid/hardware/usb/UsbAccessory;->getManufacturer()Ljava/lang/String;

    move-result-object v5

    .line 112
    invoke-virtual {v13}, Landroid/hardware/usb/UsbAccessory;->getModel()Ljava/lang/String;

    move-result-object v6

    .line 113
    invoke-virtual {v13}, Landroid/hardware/usb/UsbAccessory;->getSerial()Ljava/lang/String;

    move-result-object v7

    .line 109
    invoke-direct/range {v1 .. v7}, Lcom/samsung/appcessory/base/SAPUsbAccessory;-><init>(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    .restart local v1    # "usbAcc":Lcom/samsung/appcessory/base/SAPUsbAccessory;
    invoke-virtual {v14, v13}, Landroid/hardware/usb/UsbManager;->openAccessory(Landroid/hardware/usb/UsbAccessory;)Landroid/os/ParcelFileDescriptor;

    move-result-object v12

    .line 117
    .local v12, "fd_acc":Landroid/os/ParcelFileDescriptor;
    if-nez v12, :cond_3

    .line 104
    .end local v1    # "usbAcc":Lcom/samsung/appcessory/base/SAPUsbAccessory;
    .end local v12    # "fd_acc":Landroid/os/ParcelFileDescriptor;
    :cond_2
    :goto_3
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto :goto_0

    .line 119
    .restart local v1    # "usbAcc":Lcom/samsung/appcessory/base/SAPUsbAccessory;
    .restart local v12    # "fd_acc":Landroid/os/ParcelFileDescriptor;
    :cond_3
    invoke-virtual {v12}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v11

    .line 120
    .local v11, "fd":Ljava/io/FileDescriptor;
    if-eqz v11, :cond_2

    .line 125
    :try_start_0
    new-instance v2, Lcom/samsung/appcessory/base/SAPFileInputStream;

    .line 126
    invoke-direct {v2, v11}, Lcom/samsung/appcessory/base/SAPFileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 125
    iput-object v2, v1, Lcom/samsung/appcessory/base/SAPUsbAccessory;->mAccFileIntStream:Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :try_start_1
    new-instance v2, Lcom/samsung/appcessory/base/SAPFileOutputStream;

    .line 133
    invoke-direct {v2, v11}, Lcom/samsung/appcessory/base/SAPFileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 132
    iput-object v2, v1, Lcom/samsung/appcessory/base/SAPUsbAccessory;->mAccFileOutStream:Ljava/io/FileOutputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 139
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    .line 140
    const-string/jumbo v3, "searchAccessory Found valid accessory to communicate"

    .line 139
    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    const/4 v8, 0x1

    .line 143
    sget-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbAccList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    const-string/jumbo v3, "searchAccessory Acc found and added"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 127
    :catch_0
    move-exception v10

    .line 128
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 134
    .end local v10    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v10

    .line 136
    .restart local v10    # "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 147
    .end local v1    # "usbAcc":Lcom/samsung/appcessory/base/SAPUsbAccessory;
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v11    # "fd":Ljava/io/FileDescriptor;
    .end local v12    # "fd_acc":Landroid/os/ParcelFileDescriptor;
    :cond_4
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    .line 148
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "searchAccessory No permission for accessory.requesting perm for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 149
    invoke-virtual {v13}, Landroid/hardware/usb/UsbAccessory;->getManufacturer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 148
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 147
    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    sget-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.acessorychat.action.USB_PERMISSION"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 152
    const/4 v5, 0x0

    .line 150
    invoke-static {v2, v3, v4, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    sput-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mPermissionIntent:Landroid/app/PendingIntent;

    .line 154
    sget-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mPermissionIntent:Landroid/app/PendingIntent;

    .line 153
    invoke-virtual {v14, v13, v2}, Landroid/hardware/usb/UsbManager;->requestPermission(Landroid/hardware/usb/UsbAccessory;Landroid/app/PendingIntent;)V

    goto :goto_3

    .line 163
    .end local v13    # "usbAccessory":Landroid/hardware/usb/UsbAccessory;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/samsung/appcessory/base/IAccessoryListener;->onUsbAccessoryFound(Ljava/util/ArrayList;)V

    .line 164
    const-string v2, "SAP/SAPUsbAccManager/29Dec2014"

    const-string/jumbo v3, "searchAccessory acc list is null"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method


# virtual methods
.method public deinit()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 84
    sget-object v0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 85
    iput-object v2, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    .line 86
    sget-object v0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbAccList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 87
    sput-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbAccList:Ljava/util/ArrayList;

    .line 88
    sput-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    .line 89
    return-void
.end method

.method public getUsbAccessory(J)Lcom/samsung/appcessory/base/SAPUsbAccessory;
    .locals 1
    .param p1, "accId"    # J

    .prologue
    .line 171
    const/4 v0, 0x0

    return-object v0
.end method

.method public init(Lcom/samsung/appcessory/base/IAccessoryListener;)V
    .locals 5
    .param p1, "accMngrLstnr"    # Lcom/samsung/appcessory/base/IAccessoryListener;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    .line 70
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 71
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "com.android.acessorychat.action.USB_PERMISSION"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 72
    const-string v2, "android.hardware.usb.action.USB_ACCESSORY_DETACHED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 73
    const-string v2, "android.hardware.usb.action.USB_ACCESSORY_ATTACHED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 74
    sget-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 77
    sget-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    invoke-virtual {v2}, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 78
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 79
    sget-object v2, Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v2, v1, v3, v4}, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 81
    return-void
.end method
