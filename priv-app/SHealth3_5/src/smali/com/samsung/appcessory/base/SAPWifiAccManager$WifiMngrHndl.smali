.class public Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;
.super Landroid/os/Handler;
.source "SAPWifiAccManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPWifiAccManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WifiMngrHndl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/base/SAPWifiAccManager;


# direct methods
.method public constructor <init>(Lcom/samsung/appcessory/base/SAPWifiAccManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPWifiAccManager;

    .line 115
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 116
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 120
    const-string v0, "SAP/SAPWifiAccManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleMessage enter msg.what = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 122
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPWifiAccManager;

    # invokes: Lcom/samsung/appcessory/base/SAPWifiAccManager;->search()V
    invoke-static {v0}, Lcom/samsung/appcessory/base/SAPWifiAccManager;->access$0(Lcom/samsung/appcessory/base/SAPWifiAccManager;)V

    .line 126
    :cond_0
    :goto_0
    const-string v0, "SAP/SAPWifiAccManager/29Dec2014"

    const-string v1, "handleMessage exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    return-void

    .line 123
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 124
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPWifiAccManager$WifiMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPWifiAccManager;

    # invokes: Lcom/samsung/appcessory/base/SAPWifiAccManager;->removeAccessory()V
    invoke-static {v0}, Lcom/samsung/appcessory/base/SAPWifiAccManager;->access$1(Lcom/samsung/appcessory/base/SAPWifiAccManager;)V

    goto :goto_0
.end method
