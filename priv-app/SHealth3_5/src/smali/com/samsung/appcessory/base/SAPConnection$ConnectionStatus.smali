.class public final enum Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;
.super Ljava/lang/Enum;
.source "SAPConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectionStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CONNECTION_STATUS_CLOSED:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

.field public static final enum CONNECTION_STATUS_INVALID:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

.field public static final enum CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

.field public static final enum CONNECTION_STATUS_UNKNOWN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    new-instance v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    const-string v1, "CONNECTION_STATUS_UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_UNKNOWN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    new-instance v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    const-string v1, "CONNECTION_STATUS_OPEN"

    invoke-direct {v0, v1, v3}, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    new-instance v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    const-string v1, "CONNECTION_STATUS_CLOSED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_CLOSED:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    new-instance v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    const-string v1, "CONNECTION_STATUS_INVALID"

    invoke-direct {v0, v1, v5}, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_INVALID:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    .line 47
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    sget-object v1, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_UNKNOWN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_OPEN:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_CLOSED:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->CONNECTION_STATUS_INVALID:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->ENUM$VALUES:[Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    return-object v0
.end method

.method public static values()[Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;->ENUM$VALUES:[Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
