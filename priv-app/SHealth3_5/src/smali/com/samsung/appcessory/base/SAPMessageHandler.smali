.class public Lcom/samsung/appcessory/base/SAPMessageHandler;
.super Ljava/lang/Object;
.source "SAPMessageHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPMessageHandler/29Dec2014"


# instance fields
.field servicelistener:Lcom/samsung/appcessory/server/IServiceListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/samsung/appcessory/server/IServiceListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/appcessory/server/IServiceListener;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPMessageHandler;->servicelistener:Lcom/samsung/appcessory/server/IServiceListener;

    .line 49
    return-void
.end method


# virtual methods
.method public sendCapability(Ljava/lang/Long;Lcom/samsung/appcessory/session/SAPCapexMsg;)Z
    .locals 11
    .param p1, "accessoryId"    # Ljava/lang/Long;
    .param p2, "capexMsg"    # Lcom/samsung/appcessory/session/SAPCapexMsg;

    .prologue
    const/4 v10, 0x2

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 52
    new-instance v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;

    invoke-direct {v2}, Lcom/samsung/appcessory/protocol/SAPServiceParams;-><init>()V

    .line 53
    .local v2, "request":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    iput v10, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    .line 55
    invoke-virtual {p2}, Lcom/samsung/appcessory/session/SAPCapexMsg;->GetSwVersion()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\\."

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 56
    .local v4, "swVer":[Ljava/lang/String;
    const-string v5, "SAP/SAPMessageHandler/29Dec2014"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "swVer!"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v4, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v4, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    new-array v5, v10, [B

    iput-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    .line 58
    iget-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-object v6, v4, v8

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v5, v8

    .line 59
    iget-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-object v6, v4, v9

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v5, v9

    .line 60
    invoke-virtual {p2}, Lcom/samsung/appcessory/session/SAPCapexMsg;->GetProtoVersion()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\\."

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "protoVer":[Ljava/lang/String;
    new-array v5, v10, [B

    iput-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    .line 62
    iget-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    aget-object v6, v1, v8

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v5, v8

    .line 63
    iget-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    aget-object v6, v1, v9

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v5, v9

    .line 64
    invoke-virtual {p2}, Lcom/samsung/appcessory/session/SAPCapexMsg;->getNumPayloadType()C

    move-result v5

    iput-char v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numPayloadType:C

    .line 65
    invoke-virtual {p2}, Lcom/samsung/appcessory/session/SAPCapexMsg;->getPayloadType()[B

    move-result-object v5

    iput-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_supportedPayloadType:[B

    .line 66
    invoke-virtual {p2}, Lcom/samsung/appcessory/session/SAPCapexMsg;->getNumProfileList()C

    move-result v5

    iput-char v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_numSvc:C

    .line 67
    invoke-virtual {p2}, Lcom/samsung/appcessory/session/SAPCapexMsg;->GetSupportedProfileList()Ljava/util/List;

    move-result-object v5

    iput-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_services:Ljava/util/List;

    .line 68
    invoke-static {v2}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->obtainCapexReqMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v0

    .line 72
    .local v0, "msg":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 73
    const-wide/16 v7, 0xff

    .line 72
    invoke-static {v5, v6, v7, v8}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v3

    .line 74
    .local v3, "s":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v3, :cond_0

    .line 75
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6, v0}, Lcom/samsung/appcessory/session/SAPSession;->sendMessage(JLcom/samsung/appcessory/base/SAPMessage;)V

    .line 79
    :goto_0
    return v9

    .line 77
    :cond_0
    const-string v5, "SAP/SAPMessageHandler/29Dec2014"

    const-string v6, "Reserved session not found!"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendCapabilityRsp(Ljava/lang/Long;Lcom/samsung/appcessory/session/SAPCapexMsg;)Z
    .locals 10
    .param p1, "accessoryId"    # Ljava/lang/Long;
    .param p2, "capexMsg"    # Lcom/samsung/appcessory/session/SAPCapexMsg;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v9, 0x1

    .line 85
    new-instance v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;

    invoke-direct {v2}, Lcom/samsung/appcessory/protocol/SAPServiceParams;-><init>()V

    .line 86
    .local v2, "response":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    const/16 v5, 0x82

    iput v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    .line 88
    invoke-virtual {p2}, Lcom/samsung/appcessory/session/SAPCapexMsg;->GetSwVersion()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\\."

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 89
    .local v4, "swVer":[Ljava/lang/String;
    new-array v5, v8, [B

    iput-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    .line 90
    iget-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-object v6, v4, v7

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v5, v7

    .line 91
    iget-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-object v6, v4, v9

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v5, v9

    .line 92
    invoke-virtual {p2}, Lcom/samsung/appcessory/session/SAPCapexMsg;->GetProtoVersion()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\\."

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 93
    .local v1, "protoVer":[Ljava/lang/String;
    new-array v5, v8, [B

    iput-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    .line 94
    iget-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    aget-object v6, v1, v7

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v5, v7

    .line 95
    iget-object v5, v2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    aget-object v6, v1, v9

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v5, v9

    .line 96
    invoke-static {v2}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->obtainCapexRspMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v0

    .line 100
    .local v0, "msg":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 101
    const-wide/16 v7, 0xff

    .line 100
    invoke-static {v5, v6, v7, v8}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v3

    .line 102
    .local v3, "s":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v3, :cond_0

    .line 103
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6, v0}, Lcom/samsung/appcessory/session/SAPSession;->sendMessage(JLcom/samsung/appcessory/base/SAPMessage;)V

    .line 107
    :goto_0
    return v9

    .line 105
    :cond_0
    const-string v5, "SAP/SAPMessageHandler/29Dec2014"

    const-string v6, "Reserved session not found!"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public serviceCallback(Ljava/lang/String;)Z
    .locals 1
    .param p1, "service"    # Ljava/lang/String;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPMessageHandler;->servicelistener:Lcom/samsung/appcessory/server/IServiceListener;

    invoke-interface {v0, p1}, Lcom/samsung/appcessory/server/IServiceListener;->onListenerCallback(Ljava/lang/String;)V

    .line 113
    const/4 v0, 0x1

    return v0
.end method
