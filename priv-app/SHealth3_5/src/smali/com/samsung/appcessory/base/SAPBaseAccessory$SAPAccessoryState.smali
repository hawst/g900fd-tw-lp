.class public final enum Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;
.super Ljava/lang/Enum;
.source "SAPBaseAccessory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPBaseAccessory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SAPAccessoryState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACC_STATE_CONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

.field public static final enum ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

.field public static final enum ACC_STATE_DISCONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

.field public static final enum ACC_STATE_INITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

.field public static final enum ACC_STATE_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    const-string v1, "ACC_STATE_UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    new-instance v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    const-string v1, "ACC_STATE_CONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_CONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    new-instance v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    const-string v1, "ACC_STATE_DISCONNECTED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DISCONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    new-instance v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    const-string v1, "ACC_STATE_INITIALIZED"

    invoke-direct {v0, v1, v5}, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_INITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    new-instance v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    const-string v1, "ACC_STATE_DEINITIALIZED"

    invoke-direct {v0, v1, v6}, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 54
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_CONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DISCONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_INITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ENUM$VALUES:[Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ENUM$VALUES:[Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
