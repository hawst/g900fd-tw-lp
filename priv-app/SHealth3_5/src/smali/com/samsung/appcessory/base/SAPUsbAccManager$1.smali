.class Lcom/samsung/appcessory/base/SAPUsbAccManager$1;
.super Landroid/content/BroadcastReceiver;
.source "SAPUsbAccManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPUsbAccManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/base/SAPUsbAccManager;


# direct methods
.method constructor <init>(Lcom/samsung/appcessory/base/SAPUsbAccManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager$1;->this$0:Lcom/samsung/appcessory/base/SAPUsbAccManager;

    .line 250
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 253
    const-string v3, "SAP/SAPUsbAccManager/29Dec2014"

    .line 254
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mUsbPermReceiver onReceive enter action == "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 255
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 254
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 253
    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    const-string v3, "com.android.acessorychat.action.USB_PERMISSION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 257
    # getter for: Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->access$0()Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 258
    .local v2, "msg":Landroid/os/Message;
    iput v6, v2, Landroid/os/Message;->what:I

    .line 259
    # getter for: Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->access$0()Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->sendMessage(Landroid/os/Message;)Z

    .line 286
    :goto_0
    return-void

    .line 260
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    const-string v3, "android.hardware.usb.action.USB_ACCESSORY_ATTACHED"

    .line 261
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 260
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 261
    if-eqz v3, :cond_1

    .line 262
    # getter for: Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->access$0()Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 263
    .restart local v2    # "msg":Landroid/os/Message;
    iput v6, v2, Landroid/os/Message;->what:I

    goto :goto_0

    .line 264
    .end local v2    # "msg":Landroid/os/Message;
    :cond_1
    const-string v3, "android.hardware.usb.action.USB_ACCESSORY_DETACHED"

    .line 265
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 264
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 265
    if-eqz v3, :cond_3

    .line 266
    # getter for: Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->access$0()Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 268
    .restart local v2    # "msg":Landroid/os/Message;
    const-string v3, "accessory"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbAccessory;

    .line 269
    .local v0, "acc":Landroid/hardware/usb/UsbAccessory;
    if-eqz v0, :cond_2

    .line 270
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 271
    .local v1, "b":Landroid/os/Bundle;
    const-string/jumbo v3, "manu"

    invoke-virtual {v0}, Landroid/hardware/usb/UsbAccessory;->getManufacturer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string/jumbo v3, "model"

    invoke-virtual {v0}, Landroid/hardware/usb/UsbAccessory;->getModel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const-string/jumbo v3, "serial"

    invoke-virtual {v0}, Landroid/hardware/usb/UsbAccessory;->getSerial()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-virtual {v2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 275
    const/4 v3, 0x3

    iput v3, v2, Landroid/os/Message;->what:I

    .line 276
    # getter for: Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->access$0()Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 278
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_2
    const-string v3, "SAP/SAPUsbAccManager/29Dec2014"

    const-string v4, "Accessory is NULL "

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 281
    .end local v0    # "acc":Landroid/hardware/usb/UsbAccessory;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_3
    # getter for: Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->access$0()Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 282
    .restart local v2    # "msg":Landroid/os/Message;
    const/4 v3, 0x2

    iput v3, v2, Landroid/os/Message;->what:I

    .line 283
    # getter for: Lcom/samsung/appcessory/base/SAPUsbAccManager;->mUsbMngrHndl:Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->access$0()Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->sendMessage(Landroid/os/Message;)Z

    .line 284
    const-string v3, "SAP/SAPUsbAccManager/29Dec2014"

    const-string/jumbo v4, "mUsbPermReceiver permission denied for accessory "

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
