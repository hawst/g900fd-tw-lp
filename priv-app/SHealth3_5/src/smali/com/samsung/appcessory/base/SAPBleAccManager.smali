.class public Lcom/samsung/appcessory/base/SAPBleAccManager;
.super Lcom/samsung/appcessory/base/SAPBaseAccessory;
.source "SAPBleAccManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
    }
.end annotation


# static fields
.field private static final BLE_CONNECTED:I = 0x2

.field private static final CHAR_INDICATE:I = 0x20

.field private static final CHAR_NOTIFY:I = 0x10

.field private static DPS_DESC1:Ljava/util/UUID; = null

.field private static DPS_READ_CHAR:Ljava/util/UUID; = null

.field private static DPS_SERVICE:Ljava/util/UUID; = null

.field private static DPS_WRITE_CHAR:Ljava/util/UUID; = null

.field private static final EVT_FAILED:I = 0x0

.field private static final EVT_HANDLE_AUTOCONNECT:I = 0x9

.field private static final EVT_HANDLE_BTOFF:I = 0x0

.field private static final EVT_HANDLE_BTON:I = 0x1

.field private static final EVT_HANDLE_ERROR:I = 0x8

.field private static final EVT_HANDLE_NEW_DEVICE:I = 0x5

.field private static final EVT_READY_FOR_DATA_WRITE:I = 0x6

.field private static final EVT_SERVICES_FOUND:I = 0x3

.field private static final EVT_SUCCESS:I = 0x1

.field private static final HNDL_ACCID:Ljava/lang/String; = "accId"

.field private static final HNDL_ERR_CODE:Ljava/lang/String; = "errcode"

.field private static final TAG:Ljava/lang/String; = "SAP/SAPBleAccManager/29Dec2014"


# instance fields
.field private connectedDevice:Landroid/bluetooth/BluetoothDevice;

.field private evtServiceThread:Ljava/lang/Thread;

.field private isAutoConnectEnabled:Z

.field private isConnect:Z

.field private isIndicate:Z

.field private isNoti:Z

.field private isWriteEnabled:Z

.field private mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

.field private mAttachedEventSent:Z

.field private final mBLEReceiver:Landroid/content/BroadcastReceiver;

.field private mBleAccList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/appcessory/base/SAPBleAccessory;",
            ">;"
        }
    .end annotation
.end field

.field private mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

.field private mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

.field private mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mCharAttr:I

.field private mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

.field private mContext:Landroid/content/Context;

.field private mDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mGattCallbacks:Landroid/bluetooth/BluetoothGattCallback;

.field private mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

.field private final mListLock:Ljava/lang/Object;

.field private mSAPBleconnection:Lcom/samsung/appcessory/transport/SAPBleconnection;

.field private final mServiceLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loop"    # Landroid/os/Looper;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 118
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPBaseAccessory;-><init>()V

    .line 56
    iput-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mContext:Landroid/content/Context;

    .line 58
    iput v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mCharAttr:I

    .line 60
    iput-boolean v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isNoti:Z

    .line 61
    iput-boolean v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isIndicate:Z

    .line 62
    iput-boolean v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isConnect:Z

    .line 63
    iput-boolean v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isWriteEnabled:Z

    .line 64
    iput-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 69
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;

    .line 70
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mServiceLock:Ljava/lang/Object;

    .line 71
    iput-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->connectedDevice:Landroid/bluetooth/BluetoothDevice;

    .line 72
    iput-boolean v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAttachedEventSent:Z

    .line 73
    iput-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->evtServiceThread:Ljava/lang/Thread;

    .line 75
    iput-boolean v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isAutoConnectEnabled:Z

    .line 299
    new-instance v1, Lcom/samsung/appcessory/base/SAPBleAccManager$1;

    invoke-direct {v1, p0}, Lcom/samsung/appcessory/base/SAPBleAccManager$1;-><init>(Lcom/samsung/appcessory/base/SAPBleAccManager;)V

    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 340
    new-instance v1, Lcom/samsung/appcessory/base/SAPBleAccManager$2;

    invoke-direct {v1, p0}, Lcom/samsung/appcessory/base/SAPBleAccManager$2;-><init>(Lcom/samsung/appcessory/base/SAPBleAccManager;)V

    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mGattCallbacks:Landroid/bluetooth/BluetoothGattCallback;

    .line 939
    new-instance v1, Lcom/samsung/appcessory/base/SAPBleAccManager$3;

    invoke-direct {v1, p0}, Lcom/samsung/appcessory/base/SAPBleAccManager$3;-><init>(Lcom/samsung/appcessory/base/SAPBleAccManager;)V

    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBLEReceiver:Landroid/content/BroadcastReceiver;

    .line 120
    const-string v1, "SAP/SAPBleAccManager/29Dec2014"

    const-string v2, "SAPBleAccManager enter"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    .line 123
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;

    .line 124
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mContext:Landroid/content/Context;

    .line 125
    new-instance v1, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    invoke-direct {v1, p0, p2}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;-><init>(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    .line 126
    invoke-static {}, Lcom/samsung/appcessory/transport/SAPBleconnection;->getInstance()Lcom/samsung/appcessory/transport/SAPBleconnection;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mSAPBleconnection:Lcom/samsung/appcessory/transport/SAPBleconnection;

    .line 127
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mSAPBleconnection:Lcom/samsung/appcessory/transport/SAPBleconnection;

    invoke-virtual {v1, p0}, Lcom/samsung/appcessory/transport/SAPBleconnection;->setManager(Lcom/samsung/appcessory/base/SAPBleAccManager;)V

    .line 130
    :try_start_0
    const-string v1, "00002902-0000-1000-8000-00805f9b34fb"

    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v1

    sput-object v1, Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_DESC1:Ljava/util/UUID;

    .line 131
    const-string v1, "fc1cff01-71f1-887f-c5fa-a19b3dca3230"

    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v1

    sput-object v1, Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_SERVICE:Ljava/util/UUID;

    .line 132
    const-string v1, "fc1cff08-71f1-887f-c5fa-a19b3dca3230"

    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v1

    sput-object v1, Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_READ_CHAR:Ljava/util/UUID;

    .line 133
    const-string v1, "fc1cff07-71f1-887f-c5fa-a19b3dca3230"

    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v1

    sput-object v1, Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_WRITE_CHAR:Ljava/util/UUID;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_0
    const-string v1, "SAP/SAPBleAccManager/29Dec2014"

    const-string v2, "SAPBleAccManager exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "SAP/SAPBleAccManager/29Dec2014"

    const-string v2, "UUID generation failed"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/appcessory/base/SAPBleAccManager;Z)V
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isNoti:Z

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/appcessory/base/SAPBleAccManager;Z)V
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isIndicate:Z

    return-void
.end method

.method static synthetic access$12()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_DESC1:Ljava/util/UUID;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/appcessory/base/SAPBleAccManager;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isIndicate:Z

    return v0
.end method

.method static synthetic access$14(Lcom/samsung/appcessory/base/SAPBleAccManager;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isNoti:Z

    return v0
.end method

.method static synthetic access$15(Lcom/samsung/appcessory/base/SAPBleAccManager;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isWriteEnabled:Z

    return v0
.end method

.method static synthetic access$16(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothGatt;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$18(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->evtServiceThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$19(Lcom/samsung/appcessory/base/SAPBleAccManager;Ljava/lang/Thread;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->evtServiceThread:Ljava/lang/Thread;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1

    .prologue
    .line 1025
    invoke-direct {p0, p1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->isBLEDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    return-object v0
.end method

.method static synthetic access$22(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->connectedDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$23()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_SERVICE:Ljava/util/UUID;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mServiceLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$25()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_READ_CHAR:Ljava/util/UUID;

    return-object v0
.end method

.method static synthetic access$26()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_WRITE_CHAR:Ljava/util/UUID;

    return-object v0
.end method

.method static synthetic access$27(Lcom/samsung/appcessory/base/SAPBleAccManager;Lcom/samsung/appcessory/base/SAPBleAccessory;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    return-void
.end method

.method static synthetic access$28(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothDevice;Z)Lcom/samsung/appcessory/base/SAPBleAccessory;
    .locals 1

    .prologue
    .line 232
    invoke-direct {p0, p1, p2}, Lcom/samsung/appcessory/base/SAPBleAccManager;->handleNewBleAcc(Landroid/bluetooth/BluetoothDevice;Z)Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$29(Lcom/samsung/appcessory/base/SAPBleAccManager;Z)V
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAttachedEventSent:Z

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/appcessory/base/SAPBleAccManager;Z)V
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isWriteEnabled:Z

    return-void
.end method

.method static synthetic access$30(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$31(Lcom/samsung/appcessory/base/SAPBleAccManager;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isAutoConnectEnabled:Z

    return v0
.end method

.method static synthetic access$32(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGattCallback;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mGattCallbacks:Landroid/bluetooth/BluetoothGattCallback;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/transport/SAPBleconnection;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mSAPBleconnection:Lcom/samsung/appcessory/transport/SAPBleconnection;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccessory;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 921
    invoke-direct {p0, p1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->handleAutoConnectRequest(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/appcessory/base/SAPBleAccManager;I)V
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mCharAttr:I

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/appcessory/base/SAPBleAccManager;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mCharAttr:I

    return v0
.end method

.method private handleAutoConnectRequest(Landroid/bluetooth/BluetoothDevice;)V
    .locals 5
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 922
    if-nez p1, :cond_0

    .line 923
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "handleAutoConnectRequest device = null"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 937
    :goto_0
    return-void

    .line 926
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isAutoConnectEnabled:Z

    if-nez v2, :cond_1

    .line 927
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "isAutoConnectEnabled is not set,so no connect retry"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 931
    :cond_1
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    invoke-virtual {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 932
    .local v1, "msg":Landroid/os/Message;
    const/16 v2, 0x9

    iput v2, v1, Landroid/os/Message;->what:I

    .line 933
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 934
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "device"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 935
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 936
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v2, v1, v3, v4}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method private handleNewBleAcc(Landroid/bluetooth/BluetoothDevice;Z)Lcom/samsung/appcessory/base/SAPBleAccessory;
    .locals 10
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "informApp"    # Z

    .prologue
    .line 234
    const/4 v0, 0x0

    .line 235
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    iget-object v6, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v6

    .line 236
    :try_start_0
    const-string v5, "SAP/SAPBleAccManager/29Dec2014"

    .line 237
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "handleNewBleAcc enter mDeviceList size="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 238
    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 237
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 236
    invoke-static {v5, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    const/4 v2, 0x0

    .line 240
    .local v2, "deviceFound":Z
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 241
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_3

    move-object v1, v0

    .line 252
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .local v1, "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    :goto_0
    if-nez v2, :cond_4

    .line 254
    :try_start_1
    new-instance v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 255
    invoke-static {}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->generateUniqueId()J

    move-result-wide v7

    .line 254
    invoke-direct {v0, v7, v8, p1}, Lcom/samsung/appcessory/base/SAPBleAccessory;-><init>(JLandroid/bluetooth/BluetoothDevice;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 256
    .end local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    const/4 v5, 0x1

    :try_start_2
    invoke-virtual {v0, v5}, Lcom/samsung/appcessory/base/SAPBleAccessory;->setVisibility(Z)V

    .line 257
    const-string v5, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Adding new Accssory Id ="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, v0, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " device="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 258
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 257
    invoke-static {v5, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v5

    const/16 v7, 0xc

    if-ne v5, v7, :cond_1

    .line 261
    const-string v5, "SAP/SAPBleAccManager/29Dec2014"

    .line 262
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Device is already Bonded...."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 261
    invoke-static {v5, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_1
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    :goto_1
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 268
    if-eqz v0, :cond_2

    .line 269
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    invoke-interface {v5, v0, p2}, Lcom/samsung/appcessory/base/IAccessoryListener;->onBleAccessoryFound(Lcom/samsung/appcessory/base/SAPBleAccessory;Z)V

    .line 271
    :cond_2
    return-object v0

    .line 242
    :cond_3
    :try_start_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 243
    .local v4, "listAcc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    iget-object v5, v4, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 245
    const/4 v2, 0x1

    .line 246
    move-object v0, v4

    .line 247
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lcom/samsung/appcessory/base/SAPBleAccessory;->setVisibility(Z)V

    move-object v1, v0

    .line 248
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .restart local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    goto :goto_0

    .line 235
    .end local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .end local v2    # "deviceFound":Z
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    .end local v4    # "listAcc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    :catchall_0
    move-exception v5

    :goto_2
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v5

    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .restart local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .restart local v2    # "deviceFound":Z
    .restart local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    goto :goto_2

    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .restart local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    :cond_4
    move-object v0, v1

    .end local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    goto :goto_1
.end method

.method private isBLEDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1026
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v0

    .line 1027
    .local v0, "type":I
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1028
    :cond_0
    const/4 v1, 0x1

    .line 1030
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private sendError(IJ)V
    .locals 4
    .param p1, "errorCode"    # I
    .param p2, "accId"    # J

    .prologue
    .line 1266
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    if-eqz v2, :cond_0

    .line 1267
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    invoke-virtual {v2}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1268
    .local v1, "msg":Landroid/os/Message;
    const/16 v2, 0x8

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1269
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1270
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "errcode"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1271
    const-string v2, "accId"

    invoke-virtual {v0, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1272
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1273
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccMngrHdle:Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;

    invoke-virtual {v2, v1}, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->sendMessage(Landroid/os/Message;)Z

    .line 1277
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :goto_0
    return-void

    .line 1275
    :cond_0
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v3, "sendError mBleAccMngrHdle is null"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public cancelConnection(Lcom/samsung/appcessory/base/SAPBleAccessory;)V
    .locals 3
    .param p1, "acc"    # Lcom/samsung/appcessory/base/SAPBleAccessory;

    .prologue
    .line 1106
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1107
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " cancelConnection disconnect "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1108
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 1112
    :goto_0
    return-void

    .line 1110
    :cond_0
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string v1, "CancelConnection Failed"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public deinit()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 189
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string v1, "deinit enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBLEReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-eqz v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 211
    iput-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 215
    :cond_2
    iput-boolean v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isWriteEnabled:Z

    .line 216
    iput-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 217
    iput-boolean v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAttachedEventSent:Z

    .line 219
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 220
    :try_start_0
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 221
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 223
    :cond_3
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 224
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 219
    :cond_4
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string v1, "deinit exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    return-void

    .line 219
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public disableNotification(Lcom/samsung/appcessory/base/SAPBleAccessory;)V
    .locals 6
    .param p1, "mAppcessory"    # Lcom/samsung/appcessory/base/SAPBleAccessory;

    .prologue
    .line 1176
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string v4, "disableNotification Enter"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1177
    if-nez p1, :cond_0

    .line 1178
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string v4, "connectedAppcessory == null"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1198
    :goto_0
    return-void

    .line 1181
    :cond_0
    const/4 v1, 0x0

    .line 1182
    .local v1, "readChart":Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-virtual {p1}, Lcom/samsung/appcessory/base/SAPBleAccessory;->getReadCharac()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    .line 1184
    if-nez v1, :cond_1

    .line 1185
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string v4, "disableNotification chart is null"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1190
    :cond_1
    sget-object v3, Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_DESC1:Ljava/util/UUID;

    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v0

    .line 1191
    .local v0, "clientConfig":Landroid/bluetooth/BluetoothGattDescriptor;
    if-nez v0, :cond_2

    .line 1192
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    const-string v4, " disableNotification desc is null"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1195
    :cond_2
    sget-object v3, Landroid/bluetooth/BluetoothGattDescriptor;->DISABLE_NOTIFICATION_VALUE:[B

    invoke-virtual {v0, v3}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    move-result v2

    .line 1196
    .local v2, "retVal":Z
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "disableNotification success retVal = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getBLEVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1281
    const-string v0, ""

    .line 1282
    .local v0, "version":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "samsung_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getPairedAccessoryList()Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1202
    const-string v12, "SAP/SAPBleAccManager/29Dec2014"

    const-string v13, "getPairedAccessoryList enter"

    invoke-static {v12, v13}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1203
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v12, :cond_8

    .line 1204
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v12}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v11

    .line 1206
    .local v11, "pList":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v11, :cond_7

    .line 1207
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1209
    .local v5, "bleAccList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_1

    .line 1257
    const-string v12, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "getPairedAccessoryList return "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    .end local v5    # "bleAccList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    .end local v11    # "pList":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_1
    return-object v5

    .line 1209
    .restart local v5    # "bleAccList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    .restart local v11    # "pList":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_1
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/bluetooth/BluetoothDevice;

    .line 1210
    .local v7, "dev":Landroid/bluetooth/BluetoothDevice;
    const/4 v6, 0x0

    .line 1211
    .local v6, "currentAccDev":Lcom/samsung/appcessory/base/SAPBleAccessory;
    const-wide/16 v2, -0x1

    .line 1212
    .local v2, "accId":J
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/appcessory/base/SAPBleAccManager;->isBLEDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1213
    const-string v12, "SAP/SAPBleAccManager/29Dec2014"

    .line 1214
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Paired="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "BondState ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 1215
    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\nAddr="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 1216
    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 1214
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 1213
    invoke-static {v12, v14}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v9

    .line 1220
    .local v9, "name":Ljava/lang/String;
    if-eqz v9, :cond_2

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v12

    if-gtz v12, :cond_3

    .line 1222
    :cond_2
    const-string v12, "SAP/SAPBleAccManager/29Dec2014"

    const-string v14, "device name is null or length is 0 .Ignore device"

    invoke-static {v12, v14}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1226
    :cond_3
    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 1232
    .local v4, "add":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v14

    .line 1233
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    .line 1234
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 1235
    .local v8, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :cond_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_5

    .line 1243
    :goto_2
    const-wide/16 v15, -0x1

    cmp-long v12, v2, v15

    if-nez v12, :cond_6

    .line 1245
    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v12}, Lcom/samsung/appcessory/base/SAPBleAccManager;->handleNewBleAcc(Landroid/bluetooth/BluetoothDevice;Z)Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-result-object v10

    .line 1246
    .local v10, "newAcc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1232
    .end local v10    # "newAcc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    :goto_3
    monitor-exit v14

    goto/16 :goto_0

    .end local v8    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :catchall_0
    move-exception v12

    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v12

    .line 1236
    .restart local v8    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :cond_5
    :try_start_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-object v6, v0

    .line 1237
    iget-object v12, v6, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v12}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1238
    iget-wide v2, v6, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    .line 1239
    goto :goto_2

    .line 1250
    :cond_6
    move-object v1, v6

    .line 1251
    .local v1, "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    iput-object v7, v1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 1252
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 1260
    .end local v1    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .end local v2    # "accId":J
    .end local v4    # "add":Ljava/lang/String;
    .end local v5    # "bleAccList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    .end local v6    # "currentAccDev":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .end local v7    # "dev":Landroid/bluetooth/BluetoothDevice;
    .end local v8    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    .end local v9    # "name":Ljava/lang/String;
    :cond_7
    const-string v12, "SAP/SAPBleAccManager/29Dec2014"

    const-string v13, "getPairedAccessoryList error"

    invoke-static {v12, v13}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    .end local v11    # "pList":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_8
    const/4 v5, 0x0

    goto/16 :goto_1
.end method

.method public init(Lcom/samsung/appcessory/base/IAccessoryListener;)V
    .locals 6
    .param p1, "accMngrLstnr"    # Lcom/samsung/appcessory/base/IAccessoryListener;

    .prologue
    const/4 v5, 0x0

    .line 143
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mContext:Landroid/content/Context;

    const-string v3, "bluetooth"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    .line 144
    .local v0, "bluetoothManager":Landroid/bluetooth/BluetoothManager;
    if-nez v0, :cond_0

    .line 145
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "Failed to initialize BluetoothManager"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :goto_0
    return-void

    .line 148
    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 149
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v2, :cond_1

    .line 150
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "Init Failed mBtAdapter == null"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 153
    :cond_1
    iput-boolean v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isNoti:Z

    .line 154
    iput-boolean v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isIndicate:Z

    .line 156
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    .line 158
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "init enter "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 161
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 162
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 163
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBLEReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 165
    iput-boolean v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isWriteEnabled:Z

    .line 166
    iput-boolean v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAttachedEventSent:Z

    .line 168
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v3

    .line 169
    :try_start_0
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 170
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 172
    :cond_2
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    .line 173
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 168
    :cond_3
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 180
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "BT Is ON"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :goto_1
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "init exit"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 168
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 182
    :cond_4
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-interface {v2, v5, v3}, Lcom/samsung/appcessory/base/IAccessoryListener;->onStatusChange(ILcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V

    .line 183
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "BT Is OFF"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public removeAccessory(Lcom/samsung/appcessory/base/SAPBleAccessory;Z)V
    .locals 6
    .param p1, "acc"    # Lcom/samsung/appcessory/base/SAPBleAccessory;
    .param p2, "isCleanUpDevice"    # Z

    .prologue
    const/4 v5, 0x0

    .line 1034
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "removeAccessory enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    if-eqz v0, :cond_6

    .line 1037
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1038
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string v1, "accessory present"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1043
    :goto_0
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    invoke-virtual {p0, v0}, Lcom/samsung/appcessory/base/SAPBleAccManager;->disableNotification(Lcom/samsung/appcessory/base/SAPBleAccessory;)V

    .line 1044
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 1045
    iput-boolean v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isConnect:Z

    .line 1047
    if-eqz p2, :cond_0

    .line 1049
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1050
    :try_start_0
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1051
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1049
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1060
    :cond_0
    invoke-virtual {p1, v5}, Lcom/samsung/appcessory/base/SAPBleAccessory;->setConnectedState(Z)V

    .line 1061
    iput-boolean v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isIndicate:Z

    .line 1062
    iput-boolean v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isNoti:Z

    .line 1063
    iput-boolean v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isWriteEnabled:Z

    .line 1066
    iget-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAttachedEventSent:Z

    if-nez v0, :cond_4

    .line 1067
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string v1, "Error before Device attached "

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1068
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    if-eqz v0, :cond_1

    .line 1069
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string v1, "Connection in Progress But Failed"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iget-wide v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    const/16 v4, 0x66

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/samsung/appcessory/base/IAccessoryListener;->onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V

    .line 1080
    :cond_1
    :goto_1
    iput-boolean v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAttachedEventSent:Z

    .line 1102
    :cond_2
    :goto_2
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v1, "removeAccessory exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1103
    return-void

    .line 1040
    :cond_3
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string v1, "ERROR different accessory"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1049
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1072
    :cond_4
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    if-eqz v0, :cond_1

    .line 1073
    if-eqz p2, :cond_5

    .line 1074
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    iget-wide v1, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/appcessory/base/IAccessoryListener;->onAccessoryLost(Ljava/lang/Long;)V

    goto :goto_1

    .line 1077
    :cond_5
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    iget-wide v1, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/appcessory/base/IAccessoryListener;->onAccessoryDisconnected(Ljava/lang/Long;)V

    goto :goto_1

    .line 1082
    :cond_6
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v1, "removeAccessory connectedAppcessory == null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1083
    invoke-virtual {p1}, Lcom/samsung/appcessory/base/SAPBleAccessory;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-eqz v0, :cond_7

    .line 1085
    invoke-virtual {p1, v5}, Lcom/samsung/appcessory/base/SAPBleAccessory;->setConnectedState(Z)V

    .line 1088
    :cond_7
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isConnect:Z

    if-eqz v0, :cond_8

    .line 1089
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string v1, "Connection in Progress But Failed"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1090
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 1091
    iget-wide v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    const/16 v4, 0x6e

    .line 1090
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/samsung/appcessory/base/IAccessoryListener;->onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V

    .line 1093
    :cond_8
    iput-boolean v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isConnect:Z

    .line 1094
    if-eqz p2, :cond_2

    .line 1095
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1096
    :try_start_2
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1097
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1095
    monitor-exit v1

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public startBleConnect(JZ)Z
    .locals 10
    .param p1, "id"    # J
    .param p3, "isAutoConnect"    # Z

    .prologue
    const/4 v8, 0x0

    .line 622
    const-string v5, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "startBleConnect isAutoConnect="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 623
    const/4 v3, 0x0

    .line 625
    .local v3, "retVal":Z
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v5, :cond_0

    move v4, v3

    .line 666
    .end local v3    # "retVal":Z
    .local v4, "retVal":I
    :goto_0
    return v4

    .line 629
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":Z
    :cond_0
    iput-boolean v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isWriteEnabled:Z

    .line 630
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 631
    iput-boolean v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAttachedEventSent:Z

    .line 633
    iget-object v6, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v6

    .line 634
    :try_start_0
    const-string v5, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Before Connect mBleAccList.size()= "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_1

    .line 633
    :goto_2
    monitor-exit v6

    move v4, v3

    .line 666
    .restart local v4    # "retVal":I
    goto :goto_0

    .line 636
    .end local v4    # "retVal":I
    :cond_1
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 637
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    iget-wide v7, v0, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    cmp-long v5, v7, p1

    if-nez v5, :cond_4

    .line 638
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v7, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v5, v7}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 639
    iget-object v1, v0, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 640
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v5, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Found accessory =>"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, v0, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "device.getName() "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 641
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "Addr =>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 640
    invoke-static {v5, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    iput-boolean p3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isAutoConnectEnabled:Z

    .line 646
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-eqz v5, :cond_2

    .line 647
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 650
    :cond_2
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mGattCallbacks:Landroid/bluetooth/BluetoothGattCallback;

    invoke-virtual {v1, v5, p3, v7}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 651
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isConnect:Z

    .line 652
    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->connectedDevice:Landroid/bluetooth/BluetoothDevice;

    .line 653
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-nez v5, :cond_3

    .line 654
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/samsung/appcessory/base/SAPBleAccessory;->setConnectedState(Z)V

    .line 655
    const-string v5, "SAP/SAPBleAccManager/29Dec2014"

    const-string v7, "mBluetoothGatt.connect failed"

    invoke-static {v5, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 633
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "i":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 658
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .restart local v2    # "i":I
    :cond_3
    const/4 v3, 0x1

    .line 660
    goto :goto_2

    .line 662
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_4
    :try_start_1
    const-string v5, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v7, "startBleConnect Failed acc._id not matching"

    invoke-static {v5, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 635
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1
.end method

.method public startBleScan()Z
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 575
    const/4 v0, 0x0

    .line 577
    .local v0, "retVal":Z
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v4, :cond_0

    move v1, v0

    .line 605
    .end local v0    # "retVal":Z
    .local v1, "retVal":I
    :goto_0
    return v1

    .line 581
    .end local v1    # "retVal":I
    .restart local v0    # "retVal":Z
    :cond_0
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 582
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v5

    .line 583
    :try_start_0
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    .line 584
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_5

    .line 588
    :cond_1
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    .line 589
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 582
    :cond_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 593
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v4, :cond_3

    .line 594
    const/4 v4, 0x1

    new-array v3, v4, [Ljava/util/UUID;

    sget-object v4, Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_SERVICE:Ljava/util/UUID;

    aput-object v4, v3, v7

    .line 595
    .local v3, "uuids":[Ljava/util/UUID;
    const-string v4, "SAP/SAPBleAccManager/29Dec2014"

    const-string v5, "Ble Scan Started "

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    move-result v0

    .line 598
    const-string v4, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "startScan status = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    .end local v3    # "uuids":[Ljava/util/UUID;
    :cond_3
    if-nez v0, :cond_4

    .line 601
    const-string v4, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v5, "startBleScan Failed..."

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    sget-object v5, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    const-wide/16 v6, -0x1

    const/16 v8, 0x69

    invoke-interface {v4, v5, v6, v7, v8}, Lcom/samsung/appcessory/base/IAccessoryListener;->onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V

    :cond_4
    move v1, v0

    .line 605
    .restart local v1    # "retVal":I
    goto :goto_0

    .line 584
    .end local v1    # "retVal":I
    :cond_5
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 585
    .local v2, "tempAcc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Lcom/samsung/appcessory/base/SAPBleAccessory;->setVisibility(Z)V

    goto :goto_1

    .line 582
    .end local v2    # "tempAcc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public stopBleScan()Z
    .locals 2

    .prologue
    .line 609
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string v1, "Ble Scan Stop "

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 611
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 618
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public unpairAccessory(Lcom/samsung/appcessory/base/SAPBleAccessory;)Z
    .locals 6
    .param p1, "acc"    # Lcom/samsung/appcessory/base/SAPBleAccessory;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1115
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v1, "unpairAccessory enter "

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    if-eqz v0, :cond_0

    .line 1118
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "check=>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    iget-wide v2, v2, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1119
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    iget-wide v0, v0, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    iget-wide v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1121
    iput-boolean v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isConnect:Z

    .line 1126
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-eqz v0, :cond_1

    .line 1127
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "unpairAccessory enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1128
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1129
    :try_start_0
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1130
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mDeviceList:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1128
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1133
    invoke-virtual {p1, v4}, Lcom/samsung/appcessory/base/SAPBleAccessory;->setConnectedState(Z)V

    .line 1135
    invoke-virtual {p0, p1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->disableNotification(Lcom/samsung/appcessory/base/SAPBleAccessory;)V

    .line 1137
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 1138
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "REMOVING BOND for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1139
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 1140
    iput-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 1142
    iput-boolean v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isNoti:Z

    .line 1143
    iput-boolean v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isIndicate:Z

    .line 1145
    iput-boolean v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->isWriteEnabled:Z

    .line 1146
    iput-object v5, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 1148
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    if-eqz v0, :cond_1

    .line 1149
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iget-wide v2, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/samsung/appcessory/base/IAccessoryListener;->onPairedStatus(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JZ)V

    .line 1150
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    iget-wide v1, p1, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/appcessory/base/IAccessoryListener;->onAccessoryLost(Ljava/lang/Long;)V

    .line 1153
    :cond_1
    const-string v0, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v1, "removeAccessory exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1154
    const/4 v0, 0x1

    return v0

    .line 1128
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public unpairAccessory(Ljava/lang/String;)Z
    .locals 6
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 1158
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "unpairAccessory enter :address "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1159
    const/4 v2, 0x0

    .line 1160
    .local v2, "ret":Z
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    .line 1161
    .local v0, "bondedList":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v0, :cond_1

    .line 1162
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1171
    :cond_1
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "unpairAccessory exit ret = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1172
    .end local v2    # "ret":Z
    :goto_0
    return v2

    .line 1162
    .restart local v2    # "ret":Z
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 1163
    .local v1, "dev":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v1}, Lcom/samsung/appcessory/base/SAPBleAccManager;->isBLEDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1164
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 1165
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 1166
    const-string v3, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "unpairAccessory unpaired device: address "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1167
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public writeCharacteristic([B)Z
    .locals 7
    .param p1, "b"    # [B

    .prologue
    const/16 v6, 0x6d

    .line 276
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string/jumbo v3, "writeCharacteristic enter"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    const/4 v1, 0x0

    .line 278
    .local v1, "status":Z
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    if-eqz v2, :cond_1

    .line 279
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    .line 280
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBleAccessory;->getWriteCharac()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z

    .line 281
    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBleAccessory;->getWriteCharac()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v2

    .line 282
    const/4 v3, 0x1

    .line 281
    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V

    .line 283
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBleAccessory;->getWriteCharac()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothGatt;->writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v1

    .line 284
    if-nez v1, :cond_0

    .line 285
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 286
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;

    iget-wide v4, v4, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    .line 285
    invoke-interface {v2, v3, v4, v5, v6}, Lcom/samsung/appcessory/base/IAccessoryListener;->onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V

    .line 295
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBleAccessory;
    :cond_0
    :goto_0
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "writeCharacteristic exit status = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    return v1

    .line 290
    :cond_1
    const-string v2, "SAP/SAPBleAccManager/29Dec2014"

    const-string v3, "connectedAppcessory is null"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    const-wide/16 v4, -0x1

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/samsung/appcessory/base/IAccessoryListener;->onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V

    goto :goto_0
.end method
