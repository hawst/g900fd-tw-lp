.class public Lcom/samsung/appcessory/base/SAPBtRfAccManager;
.super Lcom/samsung/appcessory/base/SAPBaseAccessory;
.source "SAPBtRfAccManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;,
        Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;,
        Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    }
.end annotation


# static fields
.field private static final EVT_DEVICE_BONDED:I = 0x2

.field private static final EVT_DEVICE_CONNECTED:I = 0x6

.field private static final EVT_HANDLE_BTOFF:I = 0x0

.field private static final EVT_HANDLE_BTON:I = 0x1

.field private static final EVT_HANDLE_NEW_DEVICE:I = 0x3

.field private static final EVT_HANDLE_PAIRED_DEVICES:I = 0x4

.field private static final EVT_NEW_INCOMMING_CONN:I = 0x5

.field private static final HEALTH_DEVICE_LIST:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "SAP/SAPBtRfAccManager/29Dec2014"

.field private static mAcceptThreadRun:Z


# instance fields
.field private mAcceptThread:Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;

.field private mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

.field private mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

.field private mBtRfAccList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/appcessory/base/SAPBtRfAccessory;",
            ">;"
        }
    .end annotation
.end field

.field public mContext:Landroid/content/Context;

.field private final mListLock:Ljava/lang/Object;

.field private mLoop:Landroid/os/Looper;

.field private mReceiver:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "EI-HS10"

    aput-object v1, v0, v2

    sput-object v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->HEALTH_DEVICE_LIST:[Ljava/lang/String;

    .line 53
    sput-boolean v2, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThreadRun:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loop"    # Landroid/os/Looper;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPBaseAccessory;-><init>()V

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mContext:Landroid/content/Context;

    .line 57
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v1, "SAPBtRfAccManager enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mContext:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mLoop:Landroid/os/Looper;

    .line 60
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$10()Z
    .locals 1

    .prologue
    .line 53
    sget-boolean v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThreadRun:Z

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 541
    invoke-direct {p0, p1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->isHealthDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 377
    invoke-direct {p0, p1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->handleNewAcc(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothSocket;)V
    .locals 0

    .prologue
    .line 413
    invoke-direct {p0, p1, p2}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->handleNewAcc(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothSocket;)V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 556
    invoke-direct {p0, p1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->isGearDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    .locals 1

    .prologue
    .line 364
    invoke-direct {p0, p1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->getAccByMacAddress(Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private deRegisterBtEvents()V
    .locals 2

    .prologue
    .line 242
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v1, "deRegisterBtEvents enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mReceiver:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mReceiver:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 246
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mReceiver:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;

    .line 248
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v1, "deRegisterBtEvents exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    return-void
.end method

.method private getAccByMacAddress(Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    .locals 4
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 365
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v3

    .line 366
    :try_start_0
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 367
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 365
    monitor-exit v3

    .line 374
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 368
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .line 369
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    iget-object v2, v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 370
    monitor-exit v3

    goto :goto_0

    .line 365
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private handleNewAcc(Landroid/bluetooth/BluetoothDevice;)V
    .locals 7
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 379
    const-string v3, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v4, "handleNewAcc enter "

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const/4 v1, 0x0

    .line 381
    .local v1, "deviceFound":Z
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v4

    .line 382
    :try_start_0
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 383
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 381
    :goto_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 392
    if-nez v1, :cond_2

    .line 394
    new-instance v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mContext:Landroid/content/Context;

    .line 395
    invoke-static {}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->generateUniqueId()J

    move-result-wide v4

    .line 394
    invoke-direct {v0, v3, v4, v5, p1}, Lcom/samsung/appcessory/base/SAPBtRfAccessory;-><init>(Landroid/content/Context;JLandroid/bluetooth/BluetoothDevice;)V

    .line 397
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    const-string v3, "SAP/SAPBtRfAccManager/29Dec2014"

    .line 398
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Adding Accssory Id ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->_id:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " device="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 399
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 398
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 397
    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v3

    const/16 v4, 0xc

    if-ne v3, v4, :cond_1

    .line 401
    const-string v3, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Device is already Bonded...."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    :cond_1
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v4

    .line 405
    :try_start_1
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 404
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 407
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    invoke-interface {v3, v0}, Lcom/samsung/appcessory/base/IAccessoryListener;->onBtRfAccessoryFound(Lcom/samsung/appcessory/base/SAPBtRfAccessory;)V

    .line 409
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    :cond_2
    const-string v3, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v4, "handleNewAcc exit "

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    return-void

    .line 384
    :cond_3
    :try_start_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .line 385
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    iget-object v3, v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 386
    const-string v3, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v5, "handleNewAcc device already added"

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    const/4 v1, 0x1

    .line 388
    goto/16 :goto_0

    .line 381
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 404
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    .restart local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    :catchall_1
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3
.end method

.method private handleNewAcc(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothSocket;)V
    .locals 9
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "socket"    # Landroid/bluetooth/BluetoothSocket;

    .prologue
    .line 414
    const-string v5, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v6, "handleNewAcc enter "

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    const/4 v1, 0x0

    .line 417
    .local v1, "btRfAccessory":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    iget-object v6, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v6

    .line 418
    :try_start_0
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 419
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 417
    :goto_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 429
    if-nez v1, :cond_1

    .line 430
    new-instance v1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .end local v1    # "btRfAccessory":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mContext:Landroid/content/Context;

    .line 431
    invoke-static {}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->generateUniqueId()J

    move-result-wide v6

    .line 430
    invoke-direct {v1, v5, v6, v7, p1}, Lcom/samsung/appcessory/base/SAPBtRfAccessory;-><init>(Landroid/content/Context;JLandroid/bluetooth/BluetoothDevice;)V

    .line 433
    .restart local v1    # "btRfAccessory":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    iget-object v6, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v6

    .line 434
    :try_start_1
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 437
    const-string v5, "SAP/SAPBtRfAccManager/29Dec2014"

    .line 438
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Adding Accssory Id ="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v7, v1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->_id:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " device="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 439
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 438
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 437
    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v5

    const/16 v6, 0xc

    if-ne v5, v6, :cond_1

    .line 441
    const-string v5, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Device is already Bonded...."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    :cond_1
    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    .line 447
    const-string v5, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v6, "found a device with sockets"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    :try_start_2
    iput-object p2, v1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mBtSocket:Landroid/bluetooth/BluetoothSocket;

    .line 450
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mAccIntStream:Ljava/io/InputStream;

    .line 451
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mAccOutStream:Ljava/io/OutputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 463
    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    sget-object v6, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-interface {v5, v1, v6}, Lcom/samsung/appcessory/base/IAccessoryListener;->onNewConnection(Lcom/samsung/appcessory/base/SAPBaseAccessory;Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V

    .line 466
    :cond_2
    const-string v5, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v6, "handleNewAcc exit "

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    :goto_1
    return-void

    .line 420
    :cond_3
    :try_start_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .line 421
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    iget-object v5, v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 422
    const-string v5, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v7, "handleNewAcc device already added"

    invoke-static {v5, v7}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    move-object v1, v0

    .line 424
    goto/16 :goto_0

    .line 417
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v5

    .line 433
    .restart local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBtRfAccessory;>;"
    :catchall_1
    move-exception v5

    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v5

    .line 452
    :catch_0
    move-exception v2

    .line 453
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Exception - finding streams.Close socket"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    :try_start_5
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 459
    :goto_2
    const-string v5, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v6, "handleNewAcc exit error"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 456
    :catch_1
    move-exception v3

    .line 457
    .local v3, "e1":Ljava/io/IOException;
    const-string v5, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Exception-closing socket"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private isGearDevice(Ljava/lang/String;)Z
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 557
    const-string v1, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isGear2Device, name : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    if-nez p1, :cond_1

    .line 559
    const-string v1, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v2, "device name is null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    :cond_0
    :goto_0
    return v0

    .line 563
    :cond_1
    const-string v1, "Gear"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "GEAR"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "gear"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 564
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isHealthDevice(Ljava/lang/String;)Z
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 543
    const-string v2, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isFilteredDevice, name : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    if-nez p1, :cond_1

    .line 545
    const-string v2, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v3, "device name is null"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    :cond_0
    :goto_0
    return v1

    .line 548
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v2, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->HEALTH_DEVICE_LIST:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 549
    sget-object v2, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->HEALTH_DEVICE_LIST:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 550
    const/4 v1, 0x1

    goto :goto_0

    .line 548
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private isSAPInstalled(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 571
    if-nez p1, :cond_0

    .line 572
    const-string v3, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v4, "context is null"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    :goto_0
    return v2

    .line 576
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 579
    .local v1, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.samsung.accessory"

    const/16 v4, 0x80

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 585
    const/4 v2, 0x1

    goto :goto_0

    .line 580
    :catch_0
    move-exception v0

    .line 581
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v4, "SAP doesn\'t exist"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerBtEvents()V
    .locals 3

    .prologue
    .line 223
    const-string v1, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v2, "registerBtEvents enter"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 227
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.adapter.action.SCAN_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 228
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 229
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 230
    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 231
    const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 232
    const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 234
    new-instance v1, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;-><init>(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;)V

    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mReceiver:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;

    .line 236
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mReceiver:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtEventReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 238
    const-string v1, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v2, "registerBtEvents exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    return-void
.end method


# virtual methods
.method public deinit()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 474
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v1, "deinit enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    iput-object v2, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    .line 478
    invoke-virtual {p0}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->stopListeningOnBtRf()Z

    .line 479
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 482
    :cond_0
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->deRegisterBtEvents()V

    .line 484
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 485
    :try_start_0
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 484
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 487
    iput-object v2, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThread:Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;

    .line 488
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v1, "deinit exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    return-void

    .line 484
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getPairedAccessoryList()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 652
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v4, :cond_1

    .line 653
    iget-object v4, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v3

    .line 654
    .local v3, "pList":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v3, :cond_1

    .line 655
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 656
    .local v2, "pBtRfAccList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 667
    .end local v2    # "pBtRfAccList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    .end local v3    # "pList":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_1
    return-object v2

    .line 656
    .restart local v2    # "pBtRfAccList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    .restart local v3    # "pList":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 657
    .local v1, "dev":Landroid/bluetooth/BluetoothDevice;
    const-string v5, "SAP/SAPBtRfAccManager/29Dec2014"

    .line 658
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Paired="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "BondState ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 659
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\nAddr="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 660
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 658
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 657
    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    new-instance v0, Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    iget-object v5, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mContext:Landroid/content/Context;

    const-wide/16 v6, -0x1

    invoke-direct {v0, v5, v6, v7, v1}, Lcom/samsung/appcessory/base/SAPBtRfAccessory;-><init>(Landroid/content/Context;JLandroid/bluetooth/BluetoothDevice;)V

    .line 662
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 667
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    .end local v1    # "dev":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "pBtRfAccList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    .end local v3    # "pList":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public init(Lcom/samsung/appcessory/base/IAccessoryListener;)V
    .locals 2
    .param p1, "accMngrLstnr"    # Lcom/samsung/appcessory/base/IAccessoryListener;

    .prologue
    .line 63
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v1, "init enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 67
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    .line 68
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v1, "Init Failed mBtAdapter == null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 73
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    .line 72
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    .line 76
    new-instance v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mLoop:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;-><init>(Lcom/samsung/appcessory/base/SAPBtRfAccManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    .line 78
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v1, "BT Is ON"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->sendEmptyMessage(I)Z

    .line 86
    :goto_1
    invoke-direct {p0}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->registerBtEvents()V

    .line 89
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v1, "init exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 72
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->sendEmptyMessage(I)Z

    .line 83
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v1, "BT Is OFF"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public removeAccessory(Lcom/samsung/appcessory/base/SAPBtRfAccessory;Z)V
    .locals 3
    .param p1, "acc"    # Lcom/samsung/appcessory/base/SAPBtRfAccessory;
    .param p2, "isCleanupDevice"    # Z

    .prologue
    .line 493
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v1, "removeAccessory enter "

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    if-eqz p1, :cond_0

    .line 496
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "device name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 498
    :try_start_0
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 497
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 501
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    if-eqz v0, :cond_0

    .line 502
    if-eqz p2, :cond_1

    .line 503
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    iget-wide v1, p1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->_id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/appcessory/base/IAccessoryListener;->onAccessoryLost(Ljava/lang/Long;)V

    .line 509
    :cond_0
    :goto_0
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v1, "removeAccessory exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    return-void

    .line 497
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 505
    :cond_1
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    iget-wide v1, p1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->_id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/appcessory/base/IAccessoryListener;->onAccessoryDisconnected(Ljava/lang/Long;)V

    goto :goto_0
.end method

.method public startBtRfConnect(J)Z
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 470
    const/4 v0, 0x1

    return v0
.end method

.method public startDiscovery()Z
    .locals 3

    .prologue
    .line 201
    const/4 v0, 0x0

    .line 202
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v2

    .line 203
    :try_start_0
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 204
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 202
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_1

    .line 208
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    move-result v0

    .line 210
    :cond_1
    return v0

    .line 202
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public startListeningOnBtRf()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 173
    const-string v1, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v2, "startListeningOnBtRf enter"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->isSAPInstalled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v1, "eSAP will handle the connection from scale"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    const/4 v0, 0x0

    .line 186
    :goto_0
    return v0

    .line 180
    :cond_0
    sput-boolean v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThreadRun:Z

    .line 181
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThread:Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;

    if-nez v1, :cond_1

    .line 182
    new-instance v1, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;

    invoke-direct {v1, p0}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;-><init>(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)V

    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThread:Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;

    .line 183
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThread:Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;

    invoke-virtual {v1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;->start()V

    .line 185
    :cond_1
    const-string v1, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v2, "startListeningOnBtRf exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopDiscovery()Z
    .locals 3

    .prologue
    .line 214
    const-string v1, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v2, "stopDiscovery enter"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    const/4 v0, 0x0

    .line 216
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    move-result v0

    .line 218
    :cond_0
    const-string v1, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v2, "stopDiscovery exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    return v0
.end method

.method public declared-synchronized stopListeningOnBtRf()Z
    .locals 2

    .prologue
    .line 190
    monitor-enter p0

    :try_start_0
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v1, "stopListeningOnBtRf enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThreadRun:Z

    .line 192
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThread:Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThread:Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;

    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;->cancel()V

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThread:Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;

    .line 196
    :cond_0
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v1, "stopListeningOnBtRf exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public unpairAccessory(Lcom/samsung/appcessory/base/SAPBtRfAccessory;)Z
    .locals 3
    .param p1, "acc"    # Lcom/samsung/appcessory/base/SAPBtRfAccessory;

    .prologue
    .line 513
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v1, "unpairAccessory enter "

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    if-eqz p1, :cond_0

    .line 516
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "device name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 518
    :try_start_0
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRfAccList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 517
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 521
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    if-eqz v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;

    iget-wide v1, p1, Lcom/samsung/appcessory/base/SAPBtRfAccessory;->_id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/appcessory/base/IAccessoryListener;->onAccessoryLost(Ljava/lang/Long;)V

    .line 525
    :cond_0
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v1, "removeAccessory exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    const/4 v0, 0x1

    return v0

    .line 517
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public unpairAccessory(Ljava/lang/String;)Z
    .locals 2
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 536
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v1, "unpairAccessory enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    const-string v0, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v1, "unpairAccessory exit"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    const/4 v0, 0x0

    return v0
.end method
