.class public Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;
.super Landroid/os/Handler;
.source "SAPUsbAccManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPUsbAccManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UsbMngrHndl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/base/SAPUsbAccManager;


# direct methods
.method public constructor <init>(Lcom/samsung/appcessory/base/SAPUsbAccManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPUsbAccManager;

    .line 223
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 224
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 228
    const-string v1, "SAP/SAPUsbAccManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleMessage enter msg.what = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 231
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPUsbAccManager;

    # invokes: Lcom/samsung/appcessory/base/SAPUsbAccManager;->search()V
    invoke-static {v1}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->access$1(Lcom/samsung/appcessory/base/SAPUsbAccManager;)V

    .line 239
    :cond_0
    :goto_0
    const-string v1, "SAP/SAPUsbAccManager/29Dec2014"

    const-string v2, "handleMessage exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    return-void

    .line 232
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 233
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 234
    .local v0, "b":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPUsbAccManager;

    const-string/jumbo v2, "manu"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "serial"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/appcessory/base/SAPUsbAccManager;->removeAccessory(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->access$2(Lcom/samsung/appcessory/base/SAPUsbAccManager;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 235
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 237
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPUsbAccManager$UsbMngrHndl;->this$0:Lcom/samsung/appcessory/base/SAPUsbAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPUsbAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static {v1}, Lcom/samsung/appcessory/base/SAPUsbAccManager;->access$3(Lcom/samsung/appcessory/base/SAPUsbAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/samsung/appcessory/base/IAccessoryListener;->onUsbAccessoryFound(Ljava/util/ArrayList;)V

    goto :goto_0
.end method
