.class public abstract Lcom/samsung/appcessory/base/SAPConnection;
.super Ljava/lang/Object;
.source "SAPConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;
    }
.end annotation


# instance fields
.field public _status:Lcom/samsung/appcessory/base/SAPConnection$ConnectionStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract close()V
.end method

.method public abstract init(Lcom/samsung/appcessory/base/SAPBaseAccessory;Lcom/samsung/appcessory/transport/IConnectionEventListener;)V
.end method

.method public abstract read()Lcom/samsung/appcessory/base/SAPMessage;
.end method

.method public abstract write(Lcom/samsung/appcessory/base/SAPMessage;)V
.end method
