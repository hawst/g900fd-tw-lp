.class Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;
.super Landroid/os/Handler;
.source "SAPBleAccManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPBleAccManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BleAccMngrHdle"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;


# direct methods
.method public constructor <init>(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 671
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    .line 672
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 673
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 24
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 677
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "BleAccMngrHdle.handleMessage enter what = "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    .line 680
    .local v17, "what":I
    packed-switch v17, :pswitch_data_0

    .line 915
    :cond_0
    :goto_0
    :pswitch_0
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "BleAccMngrHdle.handleMessage exit"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    :cond_1
    :goto_1
    return-void

    .line 706
    :pswitch_1
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "EVT_SERVICES_FOUND "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_SERVICE:Ljava/util/UUID;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$23()Ljava/util/UUID;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$6(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-result-object v19

    if-eqz v19, :cond_2

    .line 709
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$6(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    move-object/from16 v19, v0

    sget-object v20, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_CONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 710
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "EVT_SERVICES_FOUND event have been already handled"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 720
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->isWriteEnabled:Z
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$15(Lcom/samsung/appcessory/base/SAPBleAccManager;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 721
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "********* isWriteEnabled == true return"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 725
    :cond_3
    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Landroid/bluetooth/BluetoothDevice;

    .line 726
    .local v8, "device":Landroid/bluetooth/BluetoothDevice;
    const/4 v14, 0x0

    .line 727
    .local v14, "readChart":Landroid/bluetooth/BluetoothGattCharacteristic;
    const/16 v18, 0x0

    .line 728
    .local v18, "writeChart":Landroid/bluetooth/BluetoothGattCharacteristic;
    const-wide/16 v12, -0x1

    .line 729
    .local v12, "mAccID":J
    const/4 v3, 0x0

    .line 731
    .local v3, "accDev":Lcom/samsung/appcessory/base/SAPBleAccessory;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mListLock:Ljava/lang/Object;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$0(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Object;

    move-result-object v20

    monitor-enter v20

    .line 732
    :try_start_0
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "device="

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " Addr="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " mBleAccList.size()="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;
    invoke-static/range {v22 .. v22}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$17(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/util/ArrayList;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBleAccList:Ljava/util/ArrayList;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$17(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/util/ArrayList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 734
    .local v11, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_4

    .line 731
    :goto_3
    monitor-exit v20
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 745
    if-nez v3, :cond_6

    .line 746
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "No Accessory found ERROR "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v19

    if-eqz v19, :cond_1

    .line 749
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "Connection in Progress But Failed"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v19

    sget-object v20, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    const/16 v21, 0x66

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-interface {v0, v1, v12, v13, v2}, Lcom/samsung/appcessory/base/IAccessoryListener;->onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V

    goto/16 :goto_1

    .line 735
    :cond_4
    :try_start_1
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v0, v19

    check-cast v0, Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-object v3, v0

    .line 736
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "accDev addr ="

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    iget-object v0, v3, Lcom/samsung/appcessory/base/SAPBleAccessory;->mDevice:Landroid/bluetooth/BluetoothDevice;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v19

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 738
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "accID= "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v0, v3, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    move-wide/from16 v22, v0

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    .line 739
    const-string v22, " Device Name="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v3}, Lcom/samsung/appcessory/base/SAPBleAccessory;->getDeviceName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 738
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 731
    .end local v11    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :catchall_0
    move-exception v19

    monitor-exit v20
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v19

    .line 742
    .restart local v11    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 755
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mServiceLock:Ljava/lang/Object;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$24(Lcom/samsung/appcessory/base/SAPBleAccManager;)Ljava/lang/Object;

    move-result-object v20

    monitor-enter v20

    .line 756
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$6(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-result-object v19

    if-eqz v19, :cond_7

    .line 757
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$6(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    move-object/from16 v19, v0

    sget-object v21, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_CONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_7

    .line 758
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v21, "EVT_SERVICES_FOUND event have been already handled :"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    monitor-exit v20

    goto/16 :goto_1

    .line 755
    :catchall_1
    move-exception v19

    monitor-exit v20
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v19

    .line 762
    :cond_7
    :try_start_3
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "acc state = "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/samsung/appcessory/base/SAPBleAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    iget-wide v12, v3, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    .line 765
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v19

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_SERVICE:Ljava/util/UUID;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$23()Ljava/util/UUID;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;

    move-result-object v15

    .line 766
    .local v15, "srvc":Landroid/bluetooth/BluetoothGattService;
    if-nez v15, :cond_9

    .line 767
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "handleMessage srvc is null "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_SERVICE:Ljava/util/UUID;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$23()Ljava/util/UUID;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 770
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    # invokes: Lcom/samsung/appcessory/base/SAPBleAccManager;->handleAutoConnectRequest(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v0, v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$7(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 771
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v19

    if-eqz v19, :cond_8

    .line 772
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    .line 773
    const-string v21, "Connection in Progress But Failed :not able to retreive gatt services"

    .line 772
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    :cond_8
    monitor-exit v20

    goto/16 :goto_1

    .line 779
    :cond_9
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "DPS_READ_CHAR  "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_READ_CHAR:Ljava/util/UUID;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$25()Ljava/util/UUID;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 781
    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_READ_CHAR:Ljava/util/UUID;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$25()Ljava/util/UUID;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v14

    .line 782
    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_WRITE_CHAR:Ljava/util/UUID;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$26()Ljava/util/UUID;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v18

    .line 784
    if-eqz v14, :cond_a

    if-nez v18, :cond_c

    .line 785
    :cond_a
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "handleMessage chart is null"

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 788
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    # invokes: Lcom/samsung/appcessory/base/SAPBleAccManager;->handleAutoConnectRequest(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v0, v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$7(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 789
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v19

    if-eqz v19, :cond_b

    .line 790
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v21, "Connection in Progress But Failed :no characteristics"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 793
    :cond_b
    monitor-exit v20

    goto/16 :goto_1

    .line 796
    :cond_c
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "DPS_READ_DESC DPS_DESC1 "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_DESC1:Ljava/util/UUID;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$12()Ljava/util/UUID;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->DPS_DESC1:Ljava/util/UUID;
    invoke-static {}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$12()Ljava/util/UUID;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v7

    .line 798
    .local v7, "desc":Landroid/bluetooth/BluetoothGattDescriptor;
    if-nez v7, :cond_e

    .line 799
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v21, "handleMessage desc is null"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 802
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    # invokes: Lcom/samsung/appcessory/base/SAPBleAccManager;->handleAutoConnectRequest(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v0, v8}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$7(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 803
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v19

    if-eqz v19, :cond_d

    .line 804
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    .line 805
    const-string v21, "Connection in Progress But Failed :not able to retreive descriptor"

    .line 804
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    :cond_d
    monitor-exit v20

    goto/16 :goto_1

    .line 810
    :cond_e
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v21, "handleMessage reading desc"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/bluetooth/BluetoothGatt;->readDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Z

    move-result v16

    .line 814
    .local v16, "status":Z
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string/jumbo v22, "read desc result ="

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Set Char=>ReadChar="

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Set Char=>WriteChar="

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    invoke-virtual {v3, v14}, Lcom/samsung/appcessory/base/SAPBleAccessory;->setReadCharac(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    .line 819
    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/samsung/appcessory/base/SAPBleAccessory;->setWriteCharac(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    .line 820
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v3}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$27(Lcom/samsung/appcessory/base/SAPBleAccManager;Lcom/samsung/appcessory/base/SAPBleAccessory;)V

    .line 755
    monitor-exit v20
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_0

    .line 837
    .end local v3    # "accDev":Lcom/samsung/appcessory/base/SAPBleAccessory;
    .end local v7    # "desc":Landroid/bluetooth/BluetoothGattDescriptor;
    .end local v8    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v11    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBleAccessory;>;"
    .end local v12    # "mAccID":J
    .end local v14    # "readChart":Landroid/bluetooth/BluetoothGattCharacteristic;
    .end local v15    # "srvc":Landroid/bluetooth/BluetoothGattService;
    .end local v16    # "status":Z
    .end local v18    # "writeChart":Landroid/bluetooth/BluetoothGattCharacteristic;
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 838
    .local v6, "b":Landroid/os/Bundle;
    if-nez v6, :cond_f

    .line 839
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "EVT_HANDLE_NEW_DEVICE get data is null"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 842
    :cond_f
    const-string v19, "device"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/bluetooth/BluetoothDevice;

    .line 843
    .restart local v8    # "device":Landroid/bluetooth/BluetoothDevice;
    if-nez v8, :cond_10

    .line 844
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "EVT_HANDLE_NEW_DEVICE device is null"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 847
    :cond_10
    const-string v19, "informApp"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    .line 848
    .local v10, "informApp":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    # invokes: Lcom/samsung/appcessory/base/SAPBleAccManager;->handleNewBleAcc(Landroid/bluetooth/BluetoothDevice;Z)Lcom/samsung/appcessory/base/SAPBleAccessory;
    invoke-static {v0, v8, v10}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$28(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothDevice;Z)Lcom/samsung/appcessory/base/SAPBleAccessory;

    goto/16 :goto_0

    .line 853
    .end local v6    # "b":Landroid/os/Bundle;
    .end local v8    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v10    # "informApp":Z
    :pswitch_3
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "EVT_READY_FOR_DATA_WRITE Event"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 854
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v19

    if-eqz v19, :cond_11

    .line 855
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v19

    .line 856
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mConnectedAppcessory:Lcom/samsung/appcessory/base/SAPBleAccessory;
    invoke-static/range {v20 .. v20}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$6(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/SAPBleAccessory;

    move-result-object v20

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/samsung/appcessory/base/SAPBleAccessory;->_id:J

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    const/16 v21, 0x2

    .line 855
    invoke-interface/range {v19 .. v21}, Lcom/samsung/appcessory/base/IAccessoryListener;->onConnectStateChange(Ljava/lang/Long;I)V

    .line 857
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$29(Lcom/samsung/appcessory/base/SAPBleAccManager;Z)V

    goto/16 :goto_0

    .line 860
    :cond_11
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "mAccessoryListener == null"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 865
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v19

    if-eqz v19, :cond_0

    .line 866
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 867
    .restart local v6    # "b":Landroid/os/Bundle;
    const-string v19, "errcode"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 868
    .local v9, "errcode":I
    const-string v19, "accId"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 869
    .local v4, "accId":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v19

    sget-object v20, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v4, v5, v9}, Lcom/samsung/appcessory/base/IAccessoryListener;->onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V

    goto/16 :goto_0

    .line 875
    .end local v4    # "accId":J
    .end local v6    # "b":Landroid/os/Bundle;
    .end local v9    # "errcode":I
    :pswitch_5
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "EVT_HANDLE_AUTOCONNECT try to connect"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 877
    .restart local v6    # "b":Landroid/os/Bundle;
    if-nez v6, :cond_12

    .line 878
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "EVT_HANDLE_AUTOCONNECT get data is null"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 881
    :cond_12
    const-string v19, "device"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/bluetooth/BluetoothDevice;

    .line 882
    .restart local v8    # "device":Landroid/bluetooth/BluetoothDevice;
    if-nez v8, :cond_13

    .line 883
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "EVT_HANDLE_AUTOCONNECT device is null"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 887
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v19

    if-eqz v19, :cond_14

    .line 888
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 891
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mContext:Landroid/content/Context;
    invoke-static/range {v20 .. v20}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$30(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/content/Context;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->isAutoConnectEnabled:Z
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$31(Lcom/samsung/appcessory/base/SAPBleAccManager;)Z

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mGattCallbacks:Landroid/bluetooth/BluetoothGattCallback;
    invoke-static/range {v22 .. v22}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$32(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGattCallback;

    move-result-object v22

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v8, v0, v1, v2}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$16(Lcom/samsung/appcessory/base/SAPBleAccManager;Landroid/bluetooth/BluetoothGatt;)V

    .line 892
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "device.connectGatt called"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBleAccManager;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v19

    if-nez v19, :cond_0

    .line 894
    const-string v19, "SAP/SAPBleAccManager/29Dec2014"

    const-string v20, "mBluetoothGatt = null"

    invoke-static/range {v19 .. v20}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 901
    .end local v6    # "b":Landroid/os/Bundle;
    .end local v8    # "device":Landroid/bluetooth/BluetoothDevice;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v19

    const/16 v20, 0x0

    sget-object v21, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-interface/range {v19 .. v21}, Lcom/samsung/appcessory/base/IAccessoryListener;->onStatusChange(ILcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V

    goto/16 :goto_0

    .line 905
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/base/SAPBleAccManager$BleAccMngrHdle;->this$0:Lcom/samsung/appcessory/base/SAPBleAccManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/appcessory/base/SAPBleAccManager;->mAccessoryListener:Lcom/samsung/appcessory/base/IAccessoryListener;
    invoke-static/range {v19 .. v19}, Lcom/samsung/appcessory/base/SAPBleAccManager;->access$21(Lcom/samsung/appcessory/base/SAPBleAccManager;)Lcom/samsung/appcessory/base/IAccessoryListener;

    move-result-object v19

    const/16 v20, 0x1

    sget-object v21, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-interface/range {v19 .. v21}, Lcom/samsung/appcessory/base/IAccessoryListener;->onStatusChange(ILcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V

    goto/16 :goto_0

    .line 680
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
