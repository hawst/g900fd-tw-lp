.class Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;
.super Ljava/lang/Thread;
.source "SAPBtRfAccManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/base/SAPBtRfAccManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AcceptThread"
.end annotation


# instance fields
.field private mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

.field final synthetic this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;


# direct methods
.method public constructor <init>(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)V
    .locals 5

    .prologue
    .line 592
    iput-object p1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 593
    const-string v2, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v3, "AcceptThread enter "

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    const/4 v1, 0x0

    .line 596
    .local v1, "tmp":Landroid/bluetooth/BluetoothServerSocket;
    :try_start_0
    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {p1}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$1(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    const-string v3, "SAP"

    .line 597
    const-string v4, "a49eb41e-cb06-495c-9f4f-aa80a90cdf4a"

    invoke-static {v4}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v4

    .line 596
    invoke-virtual {v2, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->listenUsingRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;

    move-result-object v1

    .line 598
    iput-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 603
    :goto_0
    return-void

    .line 599
    :catch_0
    move-exception v0

    .line 600
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "exception"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 4

    .prologue
    .line 639
    const-string v1, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v2, "cancel enter"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    :try_start_0
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    if-eqz v1, :cond_0

    .line 642
    iget-object v1, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 647
    :cond_0
    :goto_0
    const-string v1, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v2, "cancel exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    return-void

    .line 643
    :catch_0
    move-exception v0

    .line 644
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "close exception "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 607
    :goto_0
    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mAcceptThreadRun:Z
    invoke-static {}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$10()Z

    move-result v3

    if-nez v3, :cond_0

    .line 636
    :goto_1
    return-void

    .line 608
    :cond_0
    const/4 v1, 0x0

    .line 610
    .local v1, "mSocket":Landroid/bluetooth/BluetoothSocket;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    if-eqz v3, :cond_1

    .line 611
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothServerSocket;->accept()Landroid/bluetooth/BluetoothSocket;

    move-result-object v1

    .line 612
    :cond_1
    const-string v3, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string/jumbo v4, "new connection accepted"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 618
    if-eqz v1, :cond_2

    .line 619
    const-string v3, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v4, "handling client socket"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 621
    .local v2, "msg":Landroid/os/Message;
    const/4 v3, 0x5

    iput v3, v2, Landroid/os/Message;->what:I

    .line 622
    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 623
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    # getter for: Lcom/samsung/appcessory/base/SAPBtRfAccManager;->mBtRFAccMngrHdle:Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;
    invoke-static {v3}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->access$5(Lcom/samsung/appcessory/base/SAPBtRfAccManager;)Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/appcessory/base/SAPBtRfAccManager$BtRFAccMngrHdle;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 613
    .end local v2    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 614
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "SAP/SAPBtRfAccManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "exception1 "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    iget-object v3, p0, Lcom/samsung/appcessory/base/SAPBtRfAccManager$AcceptThread;->this$0:Lcom/samsung/appcessory/base/SAPBtRfAccManager;

    invoke-virtual {v3}, Lcom/samsung/appcessory/base/SAPBtRfAccManager;->stopListeningOnBtRf()Z

    goto :goto_1

    .line 625
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    const-string v3, "SAP/SAPBtRfAccManager/29Dec2014"

    const-string v4, "client socket is null"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
