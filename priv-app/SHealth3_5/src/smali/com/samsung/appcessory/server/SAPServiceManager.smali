.class public Lcom/samsung/appcessory/server/SAPServiceManager;
.super Ljava/lang/Object;
.source "SAPServiceManager.java"

# interfaces
.implements Lcom/samsung/appcessory/server/IServiceListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPServiceManager/29Dec2014"

.field private static listenerList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/appcessory/server/IServiceListener;",
            ">;"
        }
    .end annotation
.end field

.field private static final msListenerListLock:Ljava/lang/Object;


# instance fields
.field private capexServer:Lcom/samsung/appcessory/session/SAPCapabilityServer;

.field private mListener:Lcom/samsung/appcessory/server/IServiceListener;

.field private msgHndlr:Lcom/samsung/appcessory/base/SAPMessageHandler;

.field private serviceSessionList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/server/SAPServiceManager;->msListenerListLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "appContext"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget-object v1, Lcom/samsung/appcessory/server/SAPServiceManager;->msListenerListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 47
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/server/SAPServiceManager;->listenerList:Ljava/util/HashMap;

    .line 46
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/appcessory/server/SAPServiceManager;->setServiceSessionList(Ljava/util/HashMap;)V

    .line 50
    iput-object p0, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->mListener:Lcom/samsung/appcessory/server/IServiceListener;

    .line 51
    new-instance v0, Lcom/samsung/appcessory/base/SAPMessageHandler;

    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->mListener:Lcom/samsung/appcessory/server/IServiceListener;

    invoke-direct {v0, v1}, Lcom/samsung/appcessory/base/SAPMessageHandler;-><init>(Lcom/samsung/appcessory/server/IServiceListener;)V

    invoke-virtual {p0, v0}, Lcom/samsung/appcessory/server/SAPServiceManager;->setMsgHndlr(Lcom/samsung/appcessory/base/SAPMessageHandler;)V

    .line 52
    return-void

    .line 46
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getCapexServer()Lcom/samsung/appcessory/session/SAPCapabilityServer;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->capexServer:Lcom/samsung/appcessory/session/SAPCapabilityServer;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lcom/samsung/appcessory/session/SAPCapabilityServer;

    invoke-direct {v0}, Lcom/samsung/appcessory/session/SAPCapabilityServer;-><init>()V

    iput-object v0, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->capexServer:Lcom/samsung/appcessory/session/SAPCapabilityServer;

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->capexServer:Lcom/samsung/appcessory/session/SAPCapabilityServer;

    return-object v0
.end method

.method public getMsgHndlr()Lcom/samsung/appcessory/base/SAPMessageHandler;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->msgHndlr:Lcom/samsung/appcessory/base/SAPMessageHandler;

    return-object v0
.end method

.method public getServiceSessionList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->serviceSessionList:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->serviceSessionList:Ljava/util/HashMap;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->serviceSessionList:Ljava/util/HashMap;

    return-object v0
.end method

.method public onListenerCallback(Ljava/lang/String;)V
    .locals 3
    .param p1, "service"    # Ljava/lang/String;

    .prologue
    .line 88
    sget-object v2, Lcom/samsung/appcessory/server/SAPServiceManager;->msListenerListLock:Ljava/lang/Object;

    monitor-enter v2

    .line 90
    :try_start_0
    sget-object v1, Lcom/samsung/appcessory/server/SAPServiceManager;->listenerList:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/server/IServiceListener;

    .line 91
    .local v0, "mListener":Lcom/samsung/appcessory/server/IServiceListener;
    invoke-interface {v0, p1}, Lcom/samsung/appcessory/server/IServiceListener;->onListenerCallback(Ljava/lang/String;)V

    .line 88
    monitor-exit v2

    .line 93
    return-void

    .line 88
    .end local v0    # "mListener":Lcom/samsung/appcessory/server/IServiceListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onReceiveServiceData(Ljava/lang/String;)V
    .locals 1
    .param p1, "service"    # Ljava/lang/String;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->mListener:Lcom/samsung/appcessory/server/IServiceListener;

    invoke-interface {v0, p1}, Lcom/samsung/appcessory/server/IServiceListener;->onReceiveServiceData(Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public registerService(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;Ljava/lang/String;Ljava/lang/String;)J
    .locals 3
    .param p1, "TransportId"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    .param p2, "service_desc"    # Ljava/lang/String;
    .param p3, "uuid"    # Ljava/lang/String;

    .prologue
    .line 78
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/appcessory/server/SAPServiceManager;->getCapexServer()Lcom/samsung/appcessory/session/SAPCapabilityServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->getSvcDir()Lcom/samsung/appcessory/server/SAPServiceDirectory;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/samsung/appcessory/server/SAPServiceDirectory;->addService(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    invoke-static {p3}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->convertToServiceProfileIdentifier(Ljava/lang/String;)I

    move-result v1

    int-to-long v1, v1

    return-wide v1

    .line 79
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerService(JLcom/samsung/appcessory/server/IServiceListener;)Z
    .locals 3
    .param p1, "serviceId"    # J
    .param p3, "listener"    # Lcom/samsung/appcessory/server/IServiceListener;

    .prologue
    .line 55
    sget-object v2, Lcom/samsung/appcessory/server/SAPServiceManager;->msListenerListLock:Ljava/lang/Object;

    monitor-enter v2

    .line 64
    long-to-int v1, p1

    :try_start_0
    invoke-static {v1}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->convertToServiceProfileIdentifier(I)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "service":Ljava/lang/String;
    sget-object v1, Lcom/samsung/appcessory/server/SAPServiceManager;->listenerList:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    monitor-exit v2

    const/4 v1, 0x1

    return v1

    .line 55
    .end local v0    # "service":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setCapexServer(Lcom/samsung/appcessory/session/SAPCapabilityServer;)V
    .locals 0
    .param p1, "capexServer"    # Lcom/samsung/appcessory/session/SAPCapabilityServer;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->capexServer:Lcom/samsung/appcessory/session/SAPCapabilityServer;

    .line 129
    return-void
.end method

.method public setMsgHndlr(Lcom/samsung/appcessory/base/SAPMessageHandler;)V
    .locals 0
    .param p1, "msgHndlr"    # Lcom/samsung/appcessory/base/SAPMessageHandler;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->msgHndlr:Lcom/samsung/appcessory/base/SAPMessageHandler;

    .line 101
    return-void
.end method

.method public setServiceSessionList(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "serviceSessionList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/samsung/appcessory/server/SAPServiceManager;->serviceSessionList:Ljava/util/HashMap;

    .line 118
    return-void
.end method
