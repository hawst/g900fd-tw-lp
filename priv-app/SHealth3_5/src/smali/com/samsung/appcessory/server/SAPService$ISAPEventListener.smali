.class public interface abstract Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
.super Ljava/lang/Object;
.source "SAPService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/server/SAPService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ISAPEventListener"
.end annotation


# virtual methods
.method public abstract onAccessoryAttached(J)V
.end method

.method public abstract onAccessoryDetached(J)V
.end method

.method public abstract onCloseSessionRequest(JJ)V
.end method

.method public abstract onDeviceFound(JI)V
.end method

.method public abstract onError(III)V
.end method

.method public abstract onOpenSessionAccepted(JJ)V
.end method

.method public abstract onOpenSessionRequest(JJ)V
.end method

.method public abstract onPairedStatus(IJZ)V
.end method

.method public abstract onSessionClosed(JJ)V
.end method

.method public abstract onTransportStatusChanged(II)V
.end method
