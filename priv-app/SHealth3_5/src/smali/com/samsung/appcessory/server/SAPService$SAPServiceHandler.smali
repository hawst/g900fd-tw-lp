.class final Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;
.super Landroid/os/Handler;
.source "SAPService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/server/SAPService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "SAPServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/server/SAPService;


# direct methods
.method public constructor <init>(Lcom/samsung/appcessory/server/SAPService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 1302
    iput-object p1, p0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    .line 1303
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1304
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 24
    .param p1, "m"    # Landroid/os/Message;

    .prologue
    .line 1308
    const-string v21, "SAP/SAPService/29Dec2014"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "handle message"

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1309
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_2

    .line 1310
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-long v8, v0

    .line 1311
    .local v8, "accessoryId":J
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v16, v0

    check-cast v16, Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .line 1313
    .local v16, "listener":Lcom/samsung/appcessory/base/IAccessoryEventListener;
    const-string v21, "SAP/SAPService/29Dec2014"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Open a reserved session with the accessory ID: "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1314
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " listener"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 1313
    invoke-static/range {v21 .. v22}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v8, v9, v1}, Lcom/samsung/appcessory/session/SAPSessionManager;->openReservedSession(JLcom/samsung/appcessory/base/IAccessoryEventListener;)Z

    move-result v17

    .line 1317
    .local v17, "opened":Z
    if-nez v17, :cond_0

    .line 1318
    const-string v21, "SAP/SAPService/29Dec2014"

    const-string v22, "Error opening reserved session!"

    invoke-static/range {v21 .. v22}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1331
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$0(Lcom/samsung/appcessory/server/SAPService;)Ljava/util/HashMap;

    move-result-object v21

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 1332
    .local v7, "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v7, :cond_1

    .line 1333
    sget-object v21, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_INITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 1440
    .end local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v8    # "accessoryId":J
    .end local v16    # "listener":Lcom/samsung/appcessory/base/IAccessoryEventListener;
    .end local v17    # "opened":Z
    :cond_1
    :goto_0
    return-void

    .line 1359
    :cond_2
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_4

    .line 1361
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-long v8, v0

    .line 1362
    .restart local v8    # "accessoryId":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/appcessory/server/SAPService;->getAccessoryById(Ljava/lang/Long;)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v7

    .line 1363
    .restart local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    const-string v21, "SAP/SAPService/29Dec2014"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Close the reserved session with the accessory ID:"

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1364
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 1363
    invoke-static/range {v21 .. v22}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1365
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8, v9}, Lcom/samsung/appcessory/session/SAPSessionManager;->closeReservedSession(J)V

    .line 1366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v21

    if-eqz v21, :cond_3

    .line 1367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v0, v8, v9}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onAccessoryDetached(J)V

    .line 1370
    :cond_3
    if-eqz v7, :cond_1

    .line 1371
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$0(Lcom/samsung/appcessory/server/SAPService;)Ljava/util/HashMap;

    move-result-object v21

    iget-wide v0, v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 1372
    sget-object v21, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 1373
    const-string v21, "SAP/SAPService/29Dec2014"

    const-string v22, "accessory removed from mAccessoryMap"

    invoke-static/range {v21 .. v22}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$0(Lcom/samsung/appcessory/server/SAPService;)Ljava/util/HashMap;

    move-result-object v21

    iget-wide v0, v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1376
    invoke-static {v7}, Lcom/samsung/appcessory/base/SAPAccessoryStore;->removeAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V

    goto/16 :goto_0

    .line 1380
    .end local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v8    # "accessoryId":J
    :cond_4
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_5

    .line 1381
    move-object/from16 v0, p1

    iget-object v15, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v15, Lcom/samsung/appcessory/base/SAPMessageItem;

    .line 1384
    .local v15, "item":Lcom/samsung/appcessory/base/SAPMessageItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Lcom/samsung/appcessory/server/SAPService;->processAccessoryMessage(Lcom/samsung/appcessory/base/SAPMessageItem;)V

    goto/16 :goto_0

    .line 1385
    .end local v15    # "item":Lcom/samsung/appcessory/base/SAPMessageItem;
    :cond_5
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    const/16 v22, 0x4

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_6

    .line 1386
    const-string v21, "SAP/SAPService/29Dec2014"

    const-string v22, "enterSAP_SERVICE_ACCESSORY_FOUND "

    invoke-static/range {v21 .. v22}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1387
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v10

    .line 1388
    .local v10, "b":Landroid/os/Bundle;
    const-string v21, "DEVICE TYPE"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v20

    .line 1389
    .local v20, "type":I
    const-string v21, "DEVICE ID"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v13

    .line 1390
    .local v13, "id":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$2(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/base/SAPAccessoryManager;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13, v14}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getAccessoryById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v3

    .line 1391
    .local v3, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v21

    if-eqz v21, :cond_1

    if-eqz v3, :cond_1

    .line 1392
    const-string v21, "SAP/SAPService/29Dec2014"

    const-string v22, "calling onDeviceFound"

    invoke-static/range {v21 .. v22}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-interface {v0, v13, v14, v1}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onDeviceFound(JI)V

    goto/16 :goto_0

    .line 1398
    .end local v3    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v10    # "b":Landroid/os/Bundle;
    .end local v13    # "id":J
    .end local v20    # "type":I
    :cond_6
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    const/16 v22, 0x5

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_7

    .line 1400
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v18, v0

    .line 1401
    .local v18, "status":I
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 1403
    .local v20, "type":Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    # invokes: Lcom/samsung/appcessory/server/SAPService;->translateTransportType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I
    invoke-static {v0, v1}, Lcom/samsung/appcessory/server/SAPService;->access$3(Lcom/samsung/appcessory/server/SAPService;Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I

    move-result v19

    .line 1404
    .local v19, "transport":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v21

    if-eqz v21, :cond_1

    .line 1405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-interface {v0, v1, v2}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onTransportStatusChanged(II)V

    goto/16 :goto_0

    .line 1409
    .end local v18    # "status":I
    .end local v19    # "transport":I
    .end local v20    # "type":Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    :cond_7
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    const/16 v22, 0x7

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    .line 1411
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 1412
    .restart local v20    # "type":Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    # invokes: Lcom/samsung/appcessory/server/SAPService;->translateTransportType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I
    invoke-static {v0, v1}, Lcom/samsung/appcessory/server/SAPService;->access$3(Lcom/samsung/appcessory/server/SAPService;Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I

    move-result v19

    .line 1413
    .restart local v19    # "transport":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg2:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-long v5, v0

    .line 1414
    .local v5, "accId":J
    const/4 v11, 0x0

    .line 1415
    .local v11, "bond_status":Z
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_9

    .line 1416
    const/4 v11, 0x1

    .line 1420
    :cond_8
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v21

    if-eqz v21, :cond_1

    .line 1421
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-interface {v0, v1, v5, v6, v11}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onPairedStatus(IJZ)V

    goto/16 :goto_0

    .line 1417
    :cond_9
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    if-nez v21, :cond_8

    .line 1418
    const/4 v11, 0x0

    goto :goto_1

    .line 1425
    .end local v5    # "accId":J
    .end local v11    # "bond_status":Z
    .end local v19    # "transport":I
    .end local v20    # "type":Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    :cond_a
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_b

    .line 1427
    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/Message;->arg1:I

    .line 1428
    .local v12, "errCode":I
    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    .line 1429
    .local v4, "accID":I
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 1430
    .restart local v20    # "type":Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    # invokes: Lcom/samsung/appcessory/server/SAPService;->translateTransportType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I
    invoke-static {v0, v1}, Lcom/samsung/appcessory/server/SAPService;->access$3(Lcom/samsung/appcessory/server/SAPService;Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I

    move-result v19

    .line 1431
    .restart local v19    # "transport":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v21

    if-eqz v21, :cond_1

    .line 1432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->this$0:Lcom/samsung/appcessory/server/SAPService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static/range {v21 .. v21}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-interface {v0, v1, v4, v12}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onError(III)V

    goto/16 :goto_0

    .line 1438
    .end local v4    # "accID":I
    .end local v12    # "errCode":I
    .end local v19    # "transport":I
    .end local v20    # "type":Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    :cond_b
    const-string v21, "SAP/SAPService/29Dec2014"

    const-string v22, "Unsupported message received"

    invoke-static/range {v21 .. v22}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
