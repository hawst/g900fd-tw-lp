.class public Lcom/samsung/appcessory/server/SAPServiceDirectory;
.super Ljava/lang/Object;
.source "SAPServiceDirectory.java"


# static fields
.field private static final CHARSET:Ljava/nio/charset/Charset;

.field private static final ENCODING_FORMAT:Ljava/lang/String; = "UTF-8"

.field private static final SERVICE_DIR:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "SAP/SAPServiceDirectory/29Dec2014"


# instance fields
.field public dirFileInStream:Ljava/io/FileInputStream;

.field public dirFileOutStream:Ljava/io/FileOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "services.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/appcessory/server/SAPServiceDirectory;->SERVICE_DIR:Ljava/lang/String;

    .line 54
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/samsung/appcessory/server/SAPServiceDirectory;->CHARSET:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/io/FileOutputStream;

    sget-object v1, Lcom/samsung/appcessory/server/SAPServiceDirectory;->SERVICE_DIR:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/appcessory/server/SAPServiceDirectory;->dirFileOutStream:Ljava/io/FileOutputStream;

    .line 59
    new-instance v0, Ljava/io/FileInputStream;

    sget-object v1, Lcom/samsung/appcessory/server/SAPServiceDirectory;->SERVICE_DIR:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/appcessory/server/SAPServiceDirectory;->dirFileInStream:Ljava/io/FileInputStream;

    .line 68
    return-void
.end method


# virtual methods
.method public StartService(Ljava/lang/String;)V
    .locals 0
    .param p1, "Service"    # Ljava/lang/String;

    .prologue
    .line 146
    return-void
.end method

.method public addService(Ljava/lang/String;)Z
    .locals 4
    .param p1, "service"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    new-instance v1, Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/samsung/appcessory/server/SAPServiceDirectory;->dirFileOutStream:Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 88
    .local v1, "out":Ljava/io/DataOutputStream;
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    sget-object v3, Lcom/samsung/appcessory/server/SAPServiceDirectory;->CHARSET:Ljava/nio/charset/Charset;

    invoke-direct {v2, v1, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 89
    .local v0, "bw":Ljava/io/BufferedWriter;
    invoke-virtual {v0, p1}, Ljava/io/BufferedWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 90
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->newLine()V

    .line 91
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    .line 93
    const/4 v2, 0x1

    return v2
.end method

.method public getServiceList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    const-string v4, "SAP/SAPServiceDirectory/29Dec2014"

    const-string v5, "getServiceList enter"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v0, "Services":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/io/DataInputStream;

    iget-object v4, p0, Lcom/samsung/appcessory/server/SAPServiceDirectory;->dirFileInStream:Ljava/io/FileInputStream;

    invoke-direct {v2, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 74
    .local v2, "in":Ljava/io/DataInputStream;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    sget-object v5, Lcom/samsung/appcessory/server/SAPServiceDirectory;->CHARSET:Ljava/nio/charset/Charset;

    invoke-direct {v4, v2, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 77
    .local v1, "br":Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .local v3, "strLine":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 82
    const-string v4, "SAP/SAPServiceDirectory/29Dec2014"

    const-string v5, "getServiceList exit"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    return-object v0

    .line 78
    :cond_0
    const-string v4, "SAP/SAPServiceDirectory/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Read Line "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public removeService(Ljava/lang/String;)Z
    .locals 14
    .param p1, "service"    # Ljava/lang/String;

    .prologue
    .line 97
    new-instance v8, Ljava/io/File;

    const-string/jumbo v11, "myTempFile.txt"

    invoke-direct {v8, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 98
    .local v8, "tempFile":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    sget-object v11, Lcom/samsung/appcessory/server/SAPServiceDirectory;->SERVICE_DIR:Ljava/lang/String;

    invoke-direct {v4, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 99
    .local v4, "inputFile":Ljava/io/File;
    const/4 v5, 0x0

    .line 101
    .local v5, "outStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    .end local v5    # "outStream":Ljava/io/FileOutputStream;
    .local v6, "outStream":Ljava/io/FileOutputStream;
    new-instance v10, Ljava/io/BufferedWriter;

    new-instance v11, Ljava/io/OutputStreamWriter;

    .line 107
    sget-object v12, Lcom/samsung/appcessory/server/SAPServiceDirectory;->CHARSET:Ljava/nio/charset/Charset;

    invoke-direct {v11, v6, v12}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    .line 106
    invoke-direct {v10, v11}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 108
    .local v10, "writer":Ljava/io/BufferedWriter;
    new-instance v3, Ljava/io/DataInputStream;

    iget-object v11, p0, Lcom/samsung/appcessory/server/SAPServiceDirectory;->dirFileInStream:Ljava/io/FileInputStream;

    invoke-direct {v3, v11}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 109
    .local v3, "in":Ljava/io/DataInputStream;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/InputStreamReader;

    .line 110
    sget-object v12, Lcom/samsung/appcessory/server/SAPServiceDirectory;->CHARSET:Ljava/nio/charset/Charset;

    invoke-direct {v11, v3, v12}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 109
    invoke-direct {v0, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 114
    .local v0, "br":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .local v1, "currentLine":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 126
    :try_start_2
    invoke-virtual {v10}, Ljava/io/BufferedWriter;->close()V

    .line 127
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 128
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 129
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 135
    .end local v1    # "currentLine":Ljava/lang/String;
    :goto_1
    invoke-virtual {v8, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v7

    .local v7, "successful":Z
    move-object v5, v6

    .line 136
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "in":Ljava/io/DataInputStream;
    .end local v6    # "outStream":Ljava/io/FileOutputStream;
    .end local v7    # "successful":Z
    .end local v10    # "writer":Ljava/io/BufferedWriter;
    .restart local v5    # "outStream":Ljava/io/FileOutputStream;
    :goto_2
    return v7

    .line 102
    :catch_0
    move-exception v2

    .line 103
    .local v2, "e":Ljava/io/IOException;
    const-string v11, "SAP/SAPServiceDirectory/29Dec2014"

    const-string v12, " IOException on opening myTempFile.txt"

    invoke-static {v11, v12}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const/4 v7, 0x0

    goto :goto_2

    .line 115
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "outStream":Ljava/io/FileOutputStream;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "currentLine":Ljava/lang/String;
    .restart local v3    # "in":Ljava/io/DataInputStream;
    .restart local v6    # "outStream":Ljava/io/FileOutputStream;
    .restart local v10    # "writer":Ljava/io/BufferedWriter;
    :cond_1
    :try_start_3
    const-string v11, "SAP/SAPServiceDirectory/29Dec2014"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Read Line "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 118
    .local v9, "trimmedLine":Ljava/lang/String;
    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 120
    invoke-virtual {v10, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 122
    .end local v1    # "currentLine":Ljava/lang/String;
    .end local v9    # "trimmedLine":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 123
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 126
    :try_start_5
    invoke-virtual {v10}, Ljava/io/BufferedWriter;->close()V

    .line 127
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 128
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 129
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 130
    :catch_2
    move-exception v2

    .line 131
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 124
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    .line 126
    :try_start_6
    invoke-virtual {v10}, Ljava/io/BufferedWriter;->close()V

    .line 127
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 128
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 129
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 133
    :goto_3
    throw v11

    .line 130
    :catch_3
    move-exception v2

    .line 131
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 130
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "currentLine":Ljava/lang/String;
    :catch_4
    move-exception v2

    .line 131
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public startAllServices()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    return-void
.end method
