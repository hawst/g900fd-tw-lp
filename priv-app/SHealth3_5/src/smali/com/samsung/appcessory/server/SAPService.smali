.class public Lcom/samsung/appcessory/server/SAPService;
.super Landroid/app/Service;
.source "SAPService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;,
        Lcom/samsung/appcessory/server/SAPService$SAPBinder;,
        Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EVT_BLE_CONNECT_SUCCESS:I = 0x2

.field public static final EVT_BLE_OPEN_SESSION:I = 0x3

.field public static final EVT_FAILED:I = 0x0

.field public static final EVT_SUCCESS:I = 0x1

.field private static final SAP_SERVICE_ACCESSORY_FOUND:I = 0x4

.field private static final SAP_SERVICE_ACCESSORY_FOUND_ID:Ljava/lang/String; = "DEVICE ID"

.field private static final SAP_SERVICE_ACCESSORY_FOUND_TYPE:Ljava/lang/String; = "DEVICE TYPE"

.field private static final SAP_SERVICE_BOND_STATE:I = 0x7

.field private static final SAP_SERVICE_BT_STATUS:I = 0x5

.field private static final SAP_SERVICE_MESSAGE_ADD_ACCESSORY:I = 0x1

.field private static final SAP_SERVICE_MESSAGE_ON_ERROR:I = 0x6

.field private static final SAP_SERVICE_MESSAGE_PROCESS_RECEIVED_MESSAGE:I = 0x3

.field private static final SAP_SERVICE_MESSAGE_REM_ACCESSORY:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SAP/SAPService/29Dec2014"


# instance fields
.field private mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

.field private mAccessoryMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation
.end field

.field private final mBinder:Landroid/os/IBinder;

.field private mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

.field private mListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

.field private mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

.field private mSessionManager:Lcom/samsung/appcessory/session/SAPSessionManager;

.field private mTrasnportMask:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/samsung/appcessory/server/SAPService;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/appcessory/server/SAPService;->$assertionsDisabled:Z

    .line 1501
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 1486
    new-instance v0, Lcom/samsung/appcessory/server/SAPService$SAPBinder;

    invoke-direct {v0, p0}, Lcom/samsung/appcessory/server/SAPService$SAPBinder;-><init>(Lcom/samsung/appcessory/server/SAPService;)V

    iput-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mBinder:Landroid/os/IBinder;

    .line 61
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/appcessory/server/SAPService;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 1498
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    .locals 1

    .prologue
    .line 1499
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/base/SAPAccessoryManager;
    .locals 1

    .prologue
    .line 1491
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/appcessory/server/SAPService;Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I
    .locals 1

    .prologue
    .line 1248
    invoke-direct {p0, p1}, Lcom/samsung/appcessory/server/SAPService;->translateTransportType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;
    .locals 1

    .prologue
    .line 1488
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/appcessory/server/SAPService;J)V
    .locals 0

    .prologue
    .line 970
    invoke-direct {p0, p1, p2}, Lcom/samsung/appcessory/server/SAPService;->handleLostAcc(J)V

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/appcessory/server/SAPService;JZ)Z
    .locals 1

    .prologue
    .line 936
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/appcessory/server/SAPService;->handleNewAcc(JZ)Z

    move-result v0

    return v0
.end method

.method private handleLostAcc(J)V
    .locals 4
    .param p1, "accId"    # J

    .prologue
    .line 971
    const-string v1, "SAP/SAPService/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleLostAcc enter id ="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 973
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v0, :cond_0

    .line 974
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v1, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 975
    invoke-static {v0}, Lcom/samsung/appcessory/base/SAPAccessoryStore;->removeAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V

    .line 976
    invoke-virtual {p0, v0}, Lcom/samsung/appcessory/server/SAPService;->removeAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V

    .line 981
    :goto_0
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string v2, "handleLostAcc exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 982
    return-void

    .line 978
    :cond_0
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string v2, "handleLostAcc , acc not found"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleNewAcc(JZ)Z
    .locals 8
    .param p1, "accId"    # J
    .param p3, "isAutoCOnnect"    # Z

    .prologue
    .line 937
    const-string v4, "SAP/SAPService/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "handleNewAcc enter "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 939
    const/4 v3, 0x0

    .line 940
    .local v3, "ret":Z
    iget-object v4, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    .line 941
    invoke-virtual {v4}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getConnectedAccessory()Ljava/util/List;

    move-result-object v1

    .line 940
    check-cast v1, Ljava/util/ArrayList;

    .line 942
    .local v1, "activeAccList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 943
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 945
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 966
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    :cond_1
    :goto_0
    const-string v4, "SAP/SAPService/29Dec2014"

    const-string v5, "handleNewAcc exit"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    return v3

    .line 946
    .restart local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 947
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    iget-wide v4, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    .line 948
    const-string v4, "SAP/SAPService/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " handleNewAcc handling acc with id ="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " State=>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "type="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    if-eqz p3, :cond_3

    .line 950
    const-string v4, "SAP/SAPService/29Dec2014"

    const-string v5, "AutoConnect Adding accessory internally"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    sget-object v4, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DISCONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v4, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 954
    :cond_3
    invoke-static {v0}, Lcom/samsung/appcessory/base/SAPAccessoryStore;->addAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z

    move-result v3

    .line 955
    if-eqz v3, :cond_1

    .line 957
    const-string v4, "SAP/SAPService/29Dec2014"

    const-string v5, "AddAccessory Called"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 958
    invoke-virtual {p0, v0}, Lcom/samsung/appcessory/server/SAPService;->addAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V

    goto :goto_0

    .line 964
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    :cond_4
    const-string v4, "SAP/SAPService/29Dec2014"

    const-string v5, "handleNewAcc empty acc list"

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private translateTransportType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I
    .locals 2
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 1249
    const/4 v0, 0x0

    .line 1250
    .local v0, "transport":I
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_WIFI:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_1

    .line 1251
    const/4 v0, 0x1

    .line 1259
    :cond_0
    :goto_0
    return v0

    .line 1252
    :cond_1
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_2

    .line 1253
    const/4 v0, 0x4

    .line 1254
    goto :goto_0

    :cond_2
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_3

    .line 1255
    const/4 v0, 0x2

    .line 1256
    goto :goto_0

    :cond_3
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_USB:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v1, :cond_0

    .line 1257
    const/16 v0, 0x10

    goto :goto_0
.end method


# virtual methods
.method protected addAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
    .locals 8
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    const/4 v7, 0x1

    .line 499
    if-eqz p1, :cond_0

    .line 500
    iget-object v3, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    iget-wide v4, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 503
    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_CONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iget-object v4, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    if-ne v3, v4, :cond_1

    .line 504
    const-string v3, "SAP/SAPService/29Dec2014"

    .line 505
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Adding accessory with ID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 506
    iget-wide v5, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 505
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 504
    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    iget-object v3, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    iget-wide v4, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 523
    .local v1, "msg":Landroid/os/Message;
    iput v7, v1, Landroid/os/Message;->what:I

    .line 524
    iget-wide v3, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    long-to-int v3, v3

    iput v3, v1, Landroid/os/Message;->arg1:I

    .line 525
    iget-object v3, p0, Lcom/samsung/appcessory/server/SAPService;->mListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    iput-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 526
    iget-object v3, p0, Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    invoke-virtual {v3, v1}, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 569
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 528
    :cond_1
    const-string v3, "SAP/SAPService/29Dec2014"

    const-string v4, "No connection to the accessory! Returning ..."

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 531
    :cond_2
    const-string v3, "SAP/SAPService/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Attempt to add duplicate accessory (accessory ID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 532
    iget-wide v5, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 531
    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    const-string v3, "SAP/SAPService/29Dec2014"

    .line 535
    const-string v4, "Skip updating the accessory mapping. Retrieving existing accessory ..."

    .line 534
    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iget-object v3, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    .line 537
    iget-wide v4, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 539
    .local v0, "cachedAccessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v0, :cond_0

    .line 541
    iget-object v2, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 542
    .local v2, "state":Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;
    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    if-eq v3, v2, :cond_3

    .line 543
    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DISCONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    if-ne v3, v2, :cond_4

    .line 544
    :cond_3
    const-string v3, "SAP/SAPService/29Dec2014"

    .line 545
    const-string v4, "No connection to the accessory! Returning ..."

    .line 544
    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 546
    :cond_4
    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    if-ne v3, v2, :cond_5

    .line 550
    const-string v3, "SAP/SAPService/29Dec2014"

    .line 551
    const-string v4, "Establish reserved session with the existing accessory ..."

    .line 550
    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 553
    .restart local v1    # "msg":Landroid/os/Message;
    iput v7, v1, Landroid/os/Message;->what:I

    .line 554
    iget-wide v3, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    long-to-int v3, v3

    iput v3, v1, Landroid/os/Message;->arg1:I

    .line 555
    iget-object v3, p0, Lcom/samsung/appcessory/server/SAPService;->mListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    iput-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 556
    iget-object v3, p0, Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    invoke-virtual {v3, v1}, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 557
    .end local v1    # "msg":Landroid/os/Message;
    :cond_5
    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_INITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    goto :goto_0
.end method

.method public addAccessoryListener(JJLcom/samsung/appcessory/base/IAccessoryEventListener;)V
    .locals 6
    .param p1, "accessoryId"    # J
    .param p3, "sessionId"    # J
    .param p5, "listener"    # Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .prologue
    .line 341
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mSessionManager:Lcom/samsung/appcessory/session/SAPSessionManager;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mSessionManager:Lcom/samsung/appcessory/session/SAPSessionManager;

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/appcessory/session/SAPSessionManager;->addAccessoryEventListener(JJLcom/samsung/appcessory/base/IAccessoryEventListener;)V

    .line 347
    :goto_0
    return-void

    .line 345
    :cond_0
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string v1, "addAccessoryListener() Failed mSessionManager = null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public closeSession(Ljava/lang/Long;Ljava/lang/Long;)Z
    .locals 5
    .param p1, "accessoryId"    # Ljava/lang/Long;
    .param p2, "sessionId"    # Ljava/lang/Long;

    .prologue
    .line 458
    invoke-virtual {p0}, Lcom/samsung/appcessory/server/SAPService;->getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/appcessory/session/SAPSessionManager;->closeSession(JJ)Z

    move-result v0

    return v0
.end method

.method public deinit()V
    .locals 7

    .prologue
    .line 1268
    const-string v2, "SAP/SAPService/29Dec2014"

    const-string v3, "deinit enter"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1269
    iget-object v2, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v2, :cond_0

    .line 1271
    iget-object v2, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v2}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getConnectedAccessory()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1272
    .local v1, "activeAccList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1279
    .end local v1    # "activeAccList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    :cond_0
    const-string v2, "SAP/SAPService/29Dec2014"

    const-string v3, "deinit exit"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1280
    return-void

    .line 1272
    .restart local v1    # "activeAccList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/appcessory/base/SAPBaseAccessory;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 1274
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    const-string v3, "SAP/SAPService/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "force deinit for acc "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1275
    sget-object v3, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v3, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 1276
    invoke-static {v0}, Lcom/samsung/appcessory/base/SAPAccessoryStore;->removeAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V

    goto :goto_0
.end method

.method public getAccessoryById(Ljava/lang/Long;)Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .locals 1
    .param p1, "id"    # Ljava/lang/Long;

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/samsung/appcessory/server/SAPService;->getAccessoryMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    return-object v0
.end method

.method public getAccessoryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getAccessoryMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 595
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public getAccessoryType(Ljava/lang/Long;)I
    .locals 5
    .param p1, "id"    # Ljava/lang/Long;

    .prologue
    .line 389
    invoke-virtual {p0}, Lcom/samsung/appcessory/server/SAPService;->getAccessoryMap()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 390
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v0, :cond_0

    .line 392
    iget-object v2, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-direct {p0, v2}, Lcom/samsung/appcessory/server/SAPService;->translateTransportType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)I

    move-result v1

    .line 396
    :goto_0
    return v1

    .line 395
    :cond_0
    const-string v2, "SAP/SAPService/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ERROR getAccessoryType "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getActiveAccessoryObjectById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .locals 2
    .param p1, "accId"    # J

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getAccessoryById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v0

    .line 266
    :goto_0
    return-object v0

    .line 265
    :cond_0
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string v1, "getActiveAccessoryObjectById() Failed mAccessoryManager = null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActiveAccessoryObjectByName(Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 299
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v0, p1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getAccessoryByName(Ljava/lang/String;)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v0

    .line 303
    :goto_0
    return-object v0

    .line 302
    :cond_0
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string v1, "getActiveAccessoryObjectByName() Failed mAccessoryManager = null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActiveAccessoryObjectByType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Ljava/util/List;
    .locals 2
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v0, p1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getAccessoryByType(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Ljava/util/List;

    move-result-object v0

    .line 285
    :goto_0
    return-object v0

    .line 284
    :cond_0
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string v1, "getActiveAccessoryObjectByType() Failed mAccessoryManager = null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActiveAccessoryObjectState(J)Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;
    .locals 2
    .param p1, "accId"    # J

    .prologue
    .line 317
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getAccessoryState(J)Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    move-result-object v0

    .line 321
    :goto_0
    return-object v0

    .line 320
    :cond_0
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string v1, "getActiveAccessoryObjectState() Failed mAccessoryManager = null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActiveAccessoryObjects()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getConnectedAccessory()Ljava/util/List;

    move-result-object v0

    .line 248
    :goto_0
    return-object v0

    .line 247
    :cond_0
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string v1, "getActiveAccessoryObjects() Failed mAccessoryManager = null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBLEVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_0

    .line 1449
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getBLEVersion()Ljava/lang/String;

    move-result-object v0

    .line 1452
    :goto_0
    return-object v0

    .line 1451
    :cond_0
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string v1, "getBLEVesrsion() Failed"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1452
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPairedDevices(I)Ljava/util/List;
    .locals 2
    .param p1, "transport"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/SAPBaseAccessory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1283
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_1

    .line 1284
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 1285
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getPairedAccessoryList(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Ljava/util/List;

    move-result-object v0

    .line 1291
    :goto_0
    return-object v0

    .line 1287
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 1288
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getPairedAccessoryList(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1291
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mSessionManager:Lcom/samsung/appcessory/session/SAPSessionManager;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1005
    const-string v0, "TransortMask"

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/appcessory/server/SAPService;->mTrasnportMask:J

    .line 1006
    const-string v0, "SAP/SAPService/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onBind mTrasnportMask = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/appcessory/server/SAPService;->mTrasnportMask:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 986
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string v1, "SAP onCreate() Called"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 991
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string v2, "SAP onDestroy()"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 992
    invoke-virtual {p0}, Lcom/samsung/appcessory/server/SAPService;->sapDeinit()Z

    .line 993
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    if-eqz v1, :cond_0

    .line 994
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    invoke-virtual {v1}, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 995
    .local v0, "looper":Landroid/os/Looper;
    if-eqz v0, :cond_0

    .line 996
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 999
    .end local v0    # "looper":Landroid/os/Looper;
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .line 1000
    return-void
.end method

.method public openConnection(Ljava/lang/Long;Z)Z
    .locals 4
    .param p1, "accessoryId"    # Ljava/lang/Long;
    .param p2, "isAutoConnect"    # Z

    .prologue
    .line 414
    const/4 v0, 0x0

    .line 415
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v1, :cond_1

    .line 416
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, p2}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->connectAccessory(JZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/appcessory/server/SAPService;->handleNewAcc(JZ)Z

    move-result v0

    .line 425
    :goto_0
    return v0

    .line 420
    :cond_0
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string/jumbo v2, "openConnection() Failed return false"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 423
    :cond_1
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string/jumbo v2, "openConnection() Failed mAccessoryManager = null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public openSession(BLjava/lang/Long;)Z
    .locals 3
    .param p1, "payloadType"    # B
    .param p2, "accessoryId"    # Ljava/lang/Long;

    .prologue
    .line 443
    invoke-virtual {p0}, Lcom/samsung/appcessory/server/SAPService;->getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/samsung/appcessory/session/SAPSessionManager;->initSession(BJ)Z

    move-result v0

    return v0
.end method

.method protected processAccessoryMessage(Lcom/samsung/appcessory/base/SAPMessageItem;)V
    .locals 18
    .param p1, "item"    # Lcom/samsung/appcessory/base/SAPMessageItem;

    .prologue
    .line 608
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/appcessory/base/SAPMessageItem;->getAccessoryId()J

    move-result-wide v2

    .line 609
    .local v2, "accessoryId":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/appcessory/base/SAPMessageItem;->getSessionId()J

    move-result-wide v14

    .line 610
    .local v14, "sessionId":J
    const-string v1, "SAP/SAPService/29Dec2014"

    .line 611
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ">>> onAccessoryMessageReceived: accessory ID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 612
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; session ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 613
    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 611
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 610
    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    const-wide/16 v4, 0xff

    cmp-long v1, v14, v4

    if-nez v1, :cond_12

    .line 616
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/appcessory/base/SAPMessageItem;->getMessage()Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v8

    .line 617
    .local v8, "m":Lcom/samsung/appcessory/base/SAPMessage;
    const-string v1, "DEBUG"

    const-string v4, "DEBUG"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 618
    sget-boolean v1, Lcom/samsung/appcessory/server/SAPService;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez v8, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 622
    :cond_0
    invoke-static {v8}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->processAccessoryMessage(Lcom/samsung/appcessory/base/SAPMessage;)Lcom/samsung/appcessory/protocol/SAPServiceParams;

    move-result-object v9

    .line 623
    .local v9, "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    const-string v1, "DEBUG"

    const-string v4, "DEBUG"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 624
    sget-boolean v1, Lcom/samsung/appcessory/server/SAPService;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-nez v9, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 626
    :cond_1
    if-nez v9, :cond_3

    .line 627
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string/jumbo v4, "params is null after processAccessoryMessage"

    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    .end local v8    # "m":Lcom/samsung/appcessory/base/SAPMessage;
    .end local v9    # "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    :cond_2
    :goto_0
    return-void

    .line 630
    .restart local v8    # "m":Lcom/samsung/appcessory/base/SAPMessage;
    .restart local v9    # "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    :cond_3
    iget-wide v4, v9, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    const-wide/16 v16, 0xff

    and-long v12, v4, v16

    .line 632
    .local v12, "sId":J
    const-string v1, "SAP/SAPService/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_serviceId = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v9, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " sId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    iget v1, v9, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    const/4 v4, 0x3

    if-ne v1, v4, :cond_5

    .line 639
    const-wide/16 v4, 0xff

    cmp-long v1, v12, v4

    if-nez v1, :cond_4

    .line 646
    new-instance v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;

    invoke-direct {v10}, Lcom/samsung/appcessory/protocol/SAPServiceParams;-><init>()V

    .line 647
    .local v10, "response":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    const/16 v1, 0x83

    iput v1, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    .line 648
    const-wide/16 v4, 0xff

    iput-wide v4, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    .line 649
    const/4 v1, 0x1

    iput-byte v1, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionType:B

    .line 652
    invoke-static {v10}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->obtainOpenSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v6

    .line 653
    .local v6, "message":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/appcessory/server/SAPService;->getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;

    move-result-object v1

    .line 654
    const-wide/16 v4, 0xff

    .line 653
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/appcessory/session/SAPSessionManager;->dispatchMessage(JJLcom/samsung/appcessory/base/SAPMessage;)Z

    .line 657
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 658
    .local v7, "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v7, :cond_2

    .line 659
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_INITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v1, v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    goto :goto_0

    .line 685
    .end local v6    # "message":Lcom/samsung/appcessory/base/SAPMessage;
    .end local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v10    # "response":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    :cond_4
    invoke-static {}, Lcom/samsung/appcessory/session/SAPSession;->obtain()Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v11

    .line 698
    .local v11, "s":Lcom/samsung/appcessory/session/SAPSession;
    const/4 v1, 0x1

    iput-boolean v1, v11, Lcom/samsung/appcessory/session/SAPSession;->_opened:Z

    .line 699
    invoke-static {v2, v3, v11}, Lcom/samsung/appcessory/session/SAPSessionManager;->addSessionToMap(JLcom/samsung/appcessory/session/SAPSession;)V

    .line 704
    invoke-virtual {v11}, Lcom/samsung/appcessory/session/SAPSession;->init()V

    .line 707
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string v4, "ACK the application session"

    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    new-instance v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;

    invoke-direct {v10}, Lcom/samsung/appcessory/protocol/SAPServiceParams;-><init>()V

    .line 710
    .restart local v10    # "response":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    const/16 v1, 0x83

    iput v1, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    .line 711
    iget-wide v4, v11, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    iput-wide v4, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    .line 714
    const/4 v1, 0x1

    iput-byte v1, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionType:B

    .line 717
    invoke-static {v10}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->obtainOpenSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v6

    .line 718
    .restart local v6    # "message":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/appcessory/server/SAPService;->getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;

    move-result-object v1

    .line 719
    const-wide/16 v4, 0xff

    .line 718
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/appcessory/session/SAPSessionManager;->dispatchMessage(JJLcom/samsung/appcessory/base/SAPMessage;)Z

    .line 735
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    if-eqz v1, :cond_2

    .line 736
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    iget-wide v4, v11, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onOpenSessionRequest(JJ)V

    goto/16 :goto_0

    .line 739
    .end local v6    # "message":Lcom/samsung/appcessory/base/SAPMessage;
    .end local v10    # "response":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .end local v11    # "s":Lcom/samsung/appcessory/session/SAPSession;
    :cond_5
    iget v1, v9, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    const/16 v4, 0x83

    if-ne v1, v4, :cond_a

    .line 740
    const-wide/16 v4, 0xff

    cmp-long v1, v12, v4

    if-nez v1, :cond_6

    .line 747
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string v4, "ACK received for RESERVED session ..."

    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 751
    .restart local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v7, :cond_2

    .line 752
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_INITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v1, v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    goto/16 :goto_0

    .line 779
    .end local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_6
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string v4, "ACK received for APPLICATION session ..."

    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    invoke-static {v2, v3, v12, v13}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v11

    .line 793
    .restart local v11    # "s":Lcom/samsung/appcessory/session/SAPSession;
    const-string v1, "SAP/SAPService/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Application Session "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 794
    const-string/jumbo v5, "sid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "s="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 793
    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    if-eqz v11, :cond_7

    .line 798
    sget-object v1, Lcom/samsung/appcessory/session/SAPSession$SessionState;->SESSION_OPENED:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    iput-object v1, v11, Lcom/samsung/appcessory/session/SAPSession;->_state:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    .line 799
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string v4, "Now Stop Timer"

    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    invoke-virtual {v11}, Lcom/samsung/appcessory/session/SAPSession;->stopTimer()V

    .line 804
    :cond_7
    const-string v1, "DEBUG"

    const-string v4, "DEBUG"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 805
    sget-boolean v1, Lcom/samsung/appcessory/server/SAPService;->$assertionsDisabled:Z

    if-nez v1, :cond_8

    if-nez v11, :cond_8

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 808
    :cond_8
    if-eqz v11, :cond_9

    .line 809
    const/4 v1, 0x1

    iput-boolean v1, v11, Lcom/samsung/appcessory/session/SAPSession;->_opened:Z

    .line 812
    invoke-virtual {v11}, Lcom/samsung/appcessory/session/SAPSession;->init()V

    .line 830
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    if-eqz v1, :cond_2

    .line 831
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    invoke-interface {v1, v2, v3, v12, v13}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onOpenSessionAccepted(JJ)V

    goto/16 :goto_0

    .line 834
    :cond_9
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string v4, "Fetching session from map returned NULL!"

    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 837
    .end local v11    # "s":Lcom/samsung/appcessory/session/SAPSession;
    :cond_a
    iget v1, v9, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    const/4 v4, 0x4

    if-ne v1, v4, :cond_d

    .line 838
    const-wide/16 v4, 0xff

    cmp-long v1, v12, v4

    if-nez v1, :cond_b

    .line 848
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string v4, "ACK the closure of reserved session"

    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    new-instance v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;

    invoke-direct {v10}, Lcom/samsung/appcessory/protocol/SAPServiceParams;-><init>()V

    .line 850
    .restart local v10    # "response":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    const/16 v1, 0x84

    iput v1, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    .line 851
    const-wide/16 v4, 0xff

    iput-wide v4, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    .line 852
    const/4 v1, 0x1

    iput-byte v1, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionType:B

    .line 855
    invoke-static {v10}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->obtainCloseSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v6

    .line 856
    .restart local v6    # "message":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/appcessory/server/SAPService;->getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;

    move-result-object v1

    .line 857
    const-wide/16 v4, 0xff

    .line 856
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/appcessory/session/SAPSessionManager;->dispatchMessage(JJLcom/samsung/appcessory/base/SAPMessage;)Z

    .line 860
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 861
    .restart local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v7, :cond_2

    .line 862
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v1, v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    goto/16 :goto_0

    .line 869
    .end local v6    # "message":Lcom/samsung/appcessory/base/SAPMessage;
    .end local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v10    # "response":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    :cond_b
    invoke-static {v2, v3, v12, v13}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v11

    .line 871
    .restart local v11    # "s":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v11, :cond_c

    .line 872
    invoke-virtual {v11}, Lcom/samsung/appcessory/session/SAPSession;->recycle()V

    .line 873
    invoke-static {v2, v3, v11}, Lcom/samsung/appcessory/session/SAPSessionManager;->removeSessionFromMap(JLcom/samsung/appcessory/session/SAPSession;)V

    .line 875
    :cond_c
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string v4, "ACK the closure of application session"

    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    new-instance v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;

    invoke-direct {v10}, Lcom/samsung/appcessory/protocol/SAPServiceParams;-><init>()V

    .line 877
    .restart local v10    # "response":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    const/16 v1, 0x84

    iput v1, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    .line 878
    iput-wide v12, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    .line 879
    const/4 v1, 0x1

    iput-byte v1, v10, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionType:B

    .line 882
    invoke-static {v10}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->obtainCloseSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v6

    .line 883
    .restart local v6    # "message":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/appcessory/server/SAPService;->getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;

    move-result-object v1

    .line 884
    const-wide/16 v4, 0xff

    .line 883
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/appcessory/session/SAPSessionManager;->dispatchMessage(JJLcom/samsung/appcessory/base/SAPMessage;)Z

    .line 886
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    if-eqz v1, :cond_2

    .line 887
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    invoke-interface {v1, v2, v3, v12, v13}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onCloseSessionRequest(JJ)V

    goto/16 :goto_0

    .line 890
    .end local v6    # "message":Lcom/samsung/appcessory/base/SAPMessage;
    .end local v10    # "response":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .end local v11    # "s":Lcom/samsung/appcessory/session/SAPSession;
    :cond_d
    iget v1, v9, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    const/16 v4, 0x84

    if-ne v1, v4, :cond_2

    .line 891
    const-wide/16 v4, 0xff

    cmp-long v1, v12, v4

    if-nez v1, :cond_f

    .line 900
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 901
    .restart local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v7, :cond_e

    .line 902
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v1, v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 904
    :cond_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    if-eqz v1, :cond_2

    .line 905
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    invoke-interface {v1, v2, v3, v12, v13}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onSessionClosed(JJ)V

    goto/16 :goto_0

    .line 908
    .end local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_f
    invoke-static {v2, v3, v12, v13}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v11

    .line 910
    .restart local v11    # "s":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v11, :cond_10

    .line 912
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string v4, "Stoping Closer Timer"

    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    sget-object v1, Lcom/samsung/appcessory/session/SAPSession$SessionState;->SESSION_CLOSED:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    iput-object v1, v11, Lcom/samsung/appcessory/session/SAPSession;->_state:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    .line 914
    invoke-virtual {v11}, Lcom/samsung/appcessory/session/SAPSession;->stopTimer()V

    .line 916
    invoke-virtual {v11}, Lcom/samsung/appcessory/session/SAPSession;->recycle()V

    .line 917
    invoke-static {v2, v3, v11}, Lcom/samsung/appcessory/session/SAPSessionManager;->removeSessionFromMap(JLcom/samsung/appcessory/session/SAPSession;)V

    .line 919
    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .line 920
    .restart local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v7, :cond_11

    .line 921
    sget-object v1, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v1, v7, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 923
    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    if-eqz v1, :cond_2

    .line 924
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    invoke-interface {v1, v2, v3, v12, v13}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onSessionClosed(JJ)V

    goto/16 :goto_0

    .line 929
    .end local v7    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v8    # "m":Lcom/samsung/appcessory/base/SAPMessage;
    .end local v9    # "params":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .end local v11    # "s":Lcom/samsung/appcessory/session/SAPSession;
    .end local v12    # "sId":J
    :cond_12
    const-string v1, "SAP/SAPService/29Dec2014"

    .line 930
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Received a message with incorrect session ID ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 931
    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 932
    const-string v5, ") on the reserved session"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 930
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 929
    invoke-static {v1, v4}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public readMessage(Ljava/lang/Long;Ljava/lang/Long;)Lcom/samsung/appcessory/base/SAPMessage;
    .locals 1
    .param p1, "accessoryId"    # Ljava/lang/Long;
    .param p2, "sessionId"    # Ljava/lang/Long;

    .prologue
    .line 492
    const/4 v0, 0x0

    return-object v0
.end method

.method protected removeAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
    .locals 5
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 572
    if-eqz p1, :cond_0

    .line 573
    const-string v1, "SAP/SAPService/29Dec2014"

    .line 574
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Removing accessory with ID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 575
    iget-wide v3, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 574
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 573
    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 578
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 579
    iget-wide v1, p1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    long-to-int v1, v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 581
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 588
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public removeAccessoryListener(JJLcom/samsung/appcessory/base/IAccessoryEventListener;)V
    .locals 6
    .param p1, "accessoryId"    # J
    .param p3, "sessionId"    # J
    .param p5, "listener"    # Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .prologue
    .line 364
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mSessionManager:Lcom/samsung/appcessory/session/SAPSessionManager;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mSessionManager:Lcom/samsung/appcessory/session/SAPSessionManager;

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/appcessory/session/SAPSessionManager;->removeAccessoryEventListener(JJLcom/samsung/appcessory/base/IAccessoryEventListener;)V

    .line 370
    :goto_0
    return-void

    .line 368
    :cond_0
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string/jumbo v1, "removeAccessoryListener() Failed mSessionManager = null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeAccessoryObject(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z
    .locals 2
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v0, p1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->removeAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V

    .line 208
    const/4 v0, 0x1

    .line 211
    :goto_0
    return v0

    .line 210
    :cond_0
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string/jumbo v1, "removeAccessoryObject() Failed mAccessoryManager = null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sapDeinit()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1216
    iput-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    .line 1217
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_0

    .line 1219
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v0}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->deinit()V

    .line 1221
    :cond_0
    iput-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    .line 1222
    iput-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .line 1223
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 1225
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1228
    :cond_1
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mSessionManager:Lcom/samsung/appcessory/session/SAPSessionManager;

    if-eqz v0, :cond_2

    .line 1230
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mSessionManager:Lcom/samsung/appcessory/session/SAPSessionManager;

    invoke-virtual {v0}, Lcom/samsung/appcessory/session/SAPSessionManager;->deRegisterEventListener()V

    .line 1232
    :cond_2
    iput-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mSessionManager:Lcom/samsung/appcessory/session/SAPSessionManager;

    .line 1233
    const/4 v0, 0x1

    return v0
.end method

.method public sapInit(Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;)Z
    .locals 6
    .param p1, "sapEventListener"    # Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    .prologue
    .line 1012
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "SAPServiceHandler"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 1013
    .local v1, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 1014
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 1015
    .local v0, "looper":Landroid/os/Looper;
    if-eqz v0, :cond_0

    .line 1016
    new-instance v2, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    invoke-direct {v2, p0, v0}, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;-><init>(Lcom/samsung/appcessory/server/SAPService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    .line 1019
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;

    .line 1020
    new-instance v2, Lcom/samsung/appcessory/session/SAPSessionManager;

    invoke-direct {v2}, Lcom/samsung/appcessory/session/SAPSessionManager;-><init>()V

    iput-object v2, p0, Lcom/samsung/appcessory/server/SAPService;->mSessionManager:Lcom/samsung/appcessory/session/SAPSessionManager;

    .line 1021
    iget-object v2, p0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    if-nez v2, :cond_1

    .line 1022
    iput-object p1, p0, Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    .line 1025
    :cond_1
    new-instance v2, Lcom/samsung/appcessory/server/SAPService$1;

    invoke-direct {v2, p0}, Lcom/samsung/appcessory/server/SAPService$1;-><init>(Lcom/samsung/appcessory/server/SAPService;)V

    iput-object v2, p0, Lcom/samsung/appcessory/server/SAPService;->mListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .line 1206
    if-eqz v0, :cond_2

    .line 1207
    new-instance v2, Lcom/samsung/appcessory/base/SAPAccessoryManager;

    .line 1208
    invoke-virtual {p0}, Lcom/samsung/appcessory/server/SAPService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/samsung/appcessory/base/SAPAccessoryManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    .line 1207
    iput-object v2, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    .line 1209
    iget-object v2, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    iget-object v3, p0, Lcom/samsung/appcessory/server/SAPService;->mListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    iget-wide v4, p0, Lcom/samsung/appcessory/server/SAPService;->mTrasnportMask:J

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->init(Lcom/samsung/appcessory/base/IAccessoryEventListener;J)V

    .line 1210
    invoke-virtual {p0}, Lcom/samsung/appcessory/server/SAPService;->getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/appcessory/server/SAPService;->mListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    invoke-virtual {v2, v3}, Lcom/samsung/appcessory/session/SAPSessionManager;->registerEventListener(Lcom/samsung/appcessory/base/IAccessoryEventListener;)V

    .line 1212
    :cond_2
    const/4 v2, 0x1

    return v2
.end method

.method public sendMessage(Ljava/lang/Long;Ljava/lang/Long;Lcom/samsung/appcessory/base/SAPMessage;)Z
    .locals 8
    .param p1, "accessoryId"    # Ljava/lang/Long;
    .param p2, "sessionId"    # Ljava/lang/Long;
    .param p3, "msg"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 478
    invoke-virtual {p3}, Lcom/samsung/appcessory/base/SAPMessage;->getMessageBytes()[B

    move-result-object v6

    .line 479
    .local v6, "payload":[B
    array-length v0, v6

    add-int/lit8 v0, v0, 0x2

    new-array v7, v0, [B

    .line 480
    .local v7, "transport":[B
    const/16 v0, 0x60

    aput-byte v0, v7, v3

    .line 481
    aput-byte v1, v7, v1

    .line 482
    const-string v0, "SAP/SAPService/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "sendMessage "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    const/4 v0, 0x2

    array-length v1, v6

    invoke-static {v6, v3, v7, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 484
    invoke-virtual {p3, v7}, Lcom/samsung/appcessory/base/SAPMessage;->setMessageBytes([B)V

    .line 486
    invoke-virtual {p0}, Lcom/samsung/appcessory/server/SAPService;->getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/appcessory/session/SAPSessionManager;->dispatchMessage(JJLcom/samsung/appcessory/base/SAPMessage;)Z

    move-result v0

    return v0
.end method

.method public startDiscovery()Z
    .locals 3

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 87
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-virtual {v1, v2}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->startDiscovery(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z

    move-result v0

    .line 93
    :goto_0
    return v0

    .line 90
    :cond_0
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string/jumbo v2, "startDiscovery() Failed mAccessoryManager = null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startDiscovery(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z
    .locals 3
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 108
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v1, p1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->startDiscovery(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z

    move-result v0

    .line 113
    :goto_0
    return v0

    .line 111
    :cond_0
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string/jumbo v2, "startDiscovery() Failed mAccessoryManager = null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startListening(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z
    .locals 2
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v0, p1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->startListening(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z

    move-result v0

    .line 171
    :goto_0
    return v0

    .line 170
    :cond_0
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string/jumbo v1, "startListening() Failed mAccessoryManager = null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopDiscovery()Z
    .locals 3

    .prologue
    .line 125
    const/4 v0, 0x0

    .line 126
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v1, :cond_0

    .line 127
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BLE:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    invoke-virtual {v1, v2}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->stopDiscovery(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z

    move-result v0

    .line 133
    :goto_0
    return v0

    .line 130
    :cond_0
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string/jumbo v2, "stopDiscovery() Failed mAccessoryManager = null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopDiscovery(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z
    .locals 3
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 146
    const/4 v0, 0x0

    .line 147
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v1, p1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->stopDiscovery(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z

    move-result v0

    .line 153
    :goto_0
    return v0

    .line 150
    :cond_0
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string/jumbo v2, "stopDiscovery() Failed mAccessoryManager = null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopListening(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z
    .locals 2
    .param p1, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v0, p1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->stopListening(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z

    move-result v0

    .line 190
    :goto_0
    return v0

    .line 189
    :cond_0
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string/jumbo v1, "stopListening() Failed mAccessoryManager = null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unpairAccessoryObject(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z
    .locals 3
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 225
    const/4 v0, 0x0

    .line 226
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v1, p1}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->unpairAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)Z

    move-result v0

    .line 232
    :goto_0
    return v0

    .line 229
    :cond_0
    const-string v1, "SAP/SAPService/29Dec2014"

    const-string/jumbo v2, "unpairAccessoryObject() Failed mAccessoryManager = null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unpairUnknownAcc(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z
    .locals 1
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 1295
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    if-eqz v0, :cond_0

    .line 1296
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;

    invoke-virtual {v0, p1, p3}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->unpairAccessory(Ljava/lang/String;Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)Z

    move-result v0

    .line 1298
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
