.class Lcom/samsung/appcessory/server/SAPService$1;
.super Ljava/lang/Object;
.source "SAPService.java"

# interfaces
.implements Lcom/samsung/appcessory/base/IAccessoryEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/appcessory/server/SAPService;->sapInit(Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/server/SAPService;


# direct methods
.method constructor <init>(Lcom/samsung/appcessory/server/SAPService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    .line 1025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/appcessory/server/SAPService$1;)Lcom/samsung/appcessory/server/SAPService;
    .locals 1

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    return-object v0
.end method


# virtual methods
.method public onAccessoryAttached(JI)V
    .locals 4
    .param p1, "accId"    # J
    .param p3, "type"    # I

    .prologue
    .line 1055
    const-string v2, "SAP/SAPService/29Dec2014"

    const-string v3, "Accessory attached"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1057
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x4

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1058
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1059
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "DEVICE TYPE"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1060
    const-string v2, "DEVICE ID"

    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1061
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1062
    iget-object v2, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;
    invoke-static {v2}, Lcom/samsung/appcessory/server/SAPService;->access$4(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1063
    return-void
.end method

.method public onAccessoryDetached(J)V
    .locals 1
    .param p1, "accId"    # J

    .prologue
    .line 1066
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # invokes: Lcom/samsung/appcessory/server/SAPService;->handleLostAcc(J)V
    invoke-static {v0, p1, p2}, Lcom/samsung/appcessory/server/SAPService;->access$5(Lcom/samsung/appcessory/server/SAPService;J)V

    .line 1067
    return-void
.end method

.method public onAccessoryDetached(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
    .locals 0
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 1051
    return-void
.end method

.method public onAccessoryDisconnected(J)V
    .locals 4
    .param p1, "accId"    # J

    .prologue
    .line 1136
    const-string v1, "SAP/SAPService/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onAccessoryDisconnected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1137
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1138
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1139
    long-to-int v1, p1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 1140
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;
    invoke-static {v1}, Lcom/samsung/appcessory/server/SAPService;->access$4(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1141
    return-void
.end method

.method public onAccessoryMessageReceived(Lcom/samsung/appcessory/base/SAPMessageItem;)V
    .locals 3
    .param p1, "item"    # Lcom/samsung/appcessory/base/SAPMessageItem;

    .prologue
    .line 1037
    const-string v1, "DEBUG"

    const-string v2, "DEBUG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1038
    sget-boolean v1, Lcom/samsung/appcessory/server/SAPService;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1042
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1043
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1044
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1045
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;
    invoke-static {v1}, Lcom/samsung/appcessory/server/SAPService;->access$4(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1046
    return-void
.end method

.method public onAcessoryAttached(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V
    .locals 0
    .param p1, "accessory"    # Lcom/samsung/appcessory/base/SAPBaseAccessory;

    .prologue
    .line 1029
    return-void
.end method

.method public onConnectStateChange(JI)V
    .locals 4
    .param p1, "id"    # J
    .param p3, "status"    # I

    .prologue
    const/4 v3, 0x1

    .line 1072
    const-string v0, "SAP/SAPService/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onConnectStateChange status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1073
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1072
    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1075
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static {v0}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1076
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string v1, "listener call back is null"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1107
    :cond_0
    :goto_0
    return-void

    .line 1079
    :cond_1
    if-ne p3, v3, :cond_2

    .line 1080
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static {v0}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onAccessoryAttached(J)V

    goto :goto_0

    .line 1081
    :cond_2
    if-nez p3, :cond_3

    .line 1082
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static {v0}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onAccessoryDetached(J)V

    goto :goto_0

    .line 1083
    :cond_3
    const/4 v0, 0x2

    if-ne p3, v0, :cond_5

    .line 1084
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/appcessory/server/SAPService;->getAccessoryType(Ljava/lang/Long;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    .line 1086
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string v1, "AutoConnect Device Attached"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1087
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # invokes: Lcom/samsung/appcessory/server/SAPService;->handleNewAcc(JZ)Z
    invoke-static {v0, p1, p2, v3}, Lcom/samsung/appcessory/server/SAPService;->access$6(Lcom/samsung/appcessory/server/SAPService;JZ)Z

    .line 1088
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/appcessory/server/SAPService$1$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/appcessory/server/SAPService$1$1;-><init>(Lcom/samsung/appcessory/server/SAPService$1;J)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1100
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 1102
    :cond_4
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mSAPEventListener:Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;
    invoke-static {v0}, Lcom/samsung/appcessory/server/SAPService;->access$1(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/appcessory/server/SAPService$ISAPEventListener;->onAccessoryAttached(J)V

    goto :goto_0

    .line 1104
    :cond_5
    const/4 v0, 0x3

    if-ne p3, v0, :cond_0

    .line 1105
    const-string v0, "SAP/SAPService/29Dec2014"

    const-string v1, "ble open session event callback received"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onError(JJI)V
    .locals 0
    .param p1, "accId"    # J
    .param p3, "mSessionID"    # J
    .param p5, "errCode"    # I

    .prologue
    .line 1158
    return-void
.end method

.method public onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V
    .locals 8
    .param p1, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    .param p2, "accID"    # J
    .param p4, "errCode"    # I

    .prologue
    .line 1162
    const-string v5, "SAP/SAPService/29Dec2014"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "onError: accType= "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " accID="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " errCode="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1163
    sget-object v5, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p1, v5, :cond_3

    .line 1164
    iget-object v5, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;
    invoke-static {v5}, Lcom/samsung/appcessory/server/SAPService;->access$2(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/base/SAPAccessoryManager;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getAccessoryById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v0

    .line 1165
    .local v0, "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-nez v0, :cond_0

    .line 1166
    const-string v5, "SAP/SAPService/29Dec2014"

    const-string v6, "Error acc = null"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1203
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :goto_0
    return-void

    .line 1169
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_0
    const/16 v5, 0x6b

    if-eq p4, v5, :cond_1

    .line 1170
    const/16 v5, 0x6c

    if-ne p4, v5, :cond_2

    .line 1171
    :cond_1
    sget-object v5, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v5, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 1174
    :cond_2
    iget-object p1, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_type:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 1177
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    :cond_3
    const/16 v5, 0x66

    if-ne p4, v5, :cond_5

    .line 1179
    move-wide v2, p2

    .line 1180
    .local v2, "accessoryId":J
    iget-object v5, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/appcessory/server/SAPService;->getAccessoryById(Ljava/lang/Long;)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v1

    .line 1181
    .local v1, "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    iget-object v5, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mAccessoryManager:Lcom/samsung/appcessory/base/SAPAccessoryManager;
    invoke-static {v5}, Lcom/samsung/appcessory/server/SAPService;->access$2(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/base/SAPAccessoryManager;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Lcom/samsung/appcessory/base/SAPAccessoryManager;->getAccessoryById(J)Lcom/samsung/appcessory/base/SAPBaseAccessory;

    move-result-object v0

    .line 1182
    .restart local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    if-eqz v0, :cond_4

    .line 1184
    sget-object v5, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DISCONNECTED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v5, v0, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 1186
    :cond_4
    iget-object v5, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    invoke-virtual {v5}, Lcom/samsung/appcessory/server/SAPService;->getSessionManager()Lcom/samsung/appcessory/session/SAPSessionManager;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Lcom/samsung/appcessory/session/SAPSessionManager;->closeReservedSession(J)V

    .line 1187
    if-eqz v1, :cond_5

    .line 1188
    iget-object v5, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/samsung/appcessory/server/SAPService;->access$0(Lcom/samsung/appcessory/server/SAPService;)Ljava/util/HashMap;

    move-result-object v5

    iget-wide v6, v1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1189
    sget-object v5, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;->ACC_STATE_DEINITIALIZED:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    iput-object v5, v1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_state:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryState;

    .line 1190
    iget-object v5, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mAccessoryMap:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/samsung/appcessory/server/SAPService;->access$0(Lcom/samsung/appcessory/server/SAPService;)Ljava/util/HashMap;

    move-result-object v5

    iget-wide v6, v1, Lcom/samsung/appcessory/base/SAPBaseAccessory;->_id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1192
    invoke-static {v1}, Lcom/samsung/appcessory/base/SAPAccessoryStore;->removeAccessory(Lcom/samsung/appcessory/base/SAPBaseAccessory;)V

    .line 1197
    .end local v0    # "acc":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v1    # "accessory":Lcom/samsung/appcessory/base/SAPBaseAccessory;
    .end local v2    # "accessoryId":J
    :cond_5
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v4

    .line 1198
    .local v4, "msg":Landroid/os/Message;
    const/4 v5, 0x6

    iput v5, v4, Landroid/os/Message;->what:I

    .line 1199
    iput p4, v4, Landroid/os/Message;->arg1:I

    .line 1200
    long-to-int v5, p2

    iput v5, v4, Landroid/os/Message;->arg2:I

    .line 1201
    iput-object p1, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1202
    iget-object v5, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;
    invoke-static {v5}, Lcom/samsung/appcessory/server/SAPService;->access$4(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onNewConnection(JLcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V
    .locals 2
    .param p1, "accId"    # J
    .param p3, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 1145
    sget-object v0, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_TYPE_BT_RF:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    if-ne p3, v0, :cond_0

    .line 1146
    iget-object v0, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/appcessory/server/SAPService;->handleNewAcc(JZ)Z
    invoke-static {v0, p1, p2, v1}, Lcom/samsung/appcessory/server/SAPService;->access$6(Lcom/samsung/appcessory/server/SAPService;JZ)Z

    .line 1148
    :cond_0
    return-void
.end method

.method public onPairedStatus(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JZ)V
    .locals 4
    .param p1, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;
    .param p2, "accID"    # J
    .param p4, "isPaired"    # Z

    .prologue
    .line 1121
    const-string v1, "SAP/SAPService/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onPairedStatus "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1122
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1123
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x7

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1124
    if-eqz p4, :cond_0

    .line 1125
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 1129
    :goto_0
    long-to-int v1, p2

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 1130
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1131
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;
    invoke-static {v1}, Lcom/samsung/appcessory/server/SAPService;->access$4(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1132
    return-void

    .line 1127
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->arg1:I

    goto :goto_0
.end method

.method public onSessionClose(JJ)V
    .locals 0
    .param p1, "accId"    # J
    .param p3, "sId"    # J

    .prologue
    .line 1154
    return-void
.end method

.method public onStatusChange(ILcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;)V
    .locals 4
    .param p1, "status"    # I
    .param p2, "accType"    # Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .prologue
    .line 1111
    const-string v1, "SAP/SAPService/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onStatusChange "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1112
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1113
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1114
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1115
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1116
    iget-object v1, p0, Lcom/samsung/appcessory/server/SAPService$1;->this$0:Lcom/samsung/appcessory/server/SAPService;

    # getter for: Lcom/samsung/appcessory/server/SAPService;->mHandler:Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;
    invoke-static {v1}, Lcom/samsung/appcessory/server/SAPService;->access$4(Lcom/samsung/appcessory/server/SAPService;)Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/appcessory/server/SAPService$SAPServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1117
    return-void
.end method
