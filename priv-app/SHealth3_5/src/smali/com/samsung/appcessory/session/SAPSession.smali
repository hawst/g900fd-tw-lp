.class public Lcom/samsung/appcessory/session/SAPSession;
.super Ljava/lang/Object;
.source "SAPSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/session/SAPSession$SAPDispatchTask;,
        Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;,
        Lcom/samsung/appcessory/session/SAPSession$SessionState;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final MAX_POOL_SIZE:I = 0xa

.field private static final SAP_ERROR:I = 0x1

.field private static final SAP_MESSAGE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SAP/SAPSession/29Dec2014"

.field private static volatile sPool:Lcom/samsung/appcessory/session/SAPSession;

.field private static sPoolSize:I


# instance fields
.field public _id:J

.field public _opened:Z

.field public _payloadType:B

.field public _state:Lcom/samsung/appcessory/session/SAPSession$SessionState;

.field dispatcher:Ljava/util/concurrent/ExecutorService;

.field handler:Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;

.field listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/IAccessoryEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private mSessionTimer:Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

.field next:Lcom/samsung/appcessory/session/SAPSession;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    const-class v0, Lcom/samsung/appcessory/session/SAPSession;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/appcessory/session/SAPSession;->$assertionsDisabled:Z

    .line 340
    sput v1, Lcom/samsung/appcessory/session/SAPSession;->sPoolSize:I

    .line 348
    return-void

    :cond_0
    move v0, v1

    .line 47
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    .line 337
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPSession;->mSessionTimer:Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    .line 69
    invoke-static {}, Lcom/samsung/appcessory/session/SAPSession;->generateUniqueId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/appcessory/session/SAPSession;->_opened:Z

    .line 71
    return-void
.end method

.method static generateUniqueId()J
    .locals 7

    .prologue
    const-wide/16 v5, 0xff

    .line 269
    const-wide/16 v1, 0x7e

    .line 275
    .local v1, "uid":J
    :goto_0
    const-wide/16 v3, 0x7e

    cmp-long v3, v1, v3

    if-eqz v3, :cond_0

    const-wide/16 v3, 0x7d

    cmp-long v3, v1, v3

    if-eqz v3, :cond_0

    const-wide/16 v3, 0x5d

    cmp-long v3, v1, v3

    if-eqz v3, :cond_0

    const-wide/16 v3, 0x5e

    cmp-long v3, v1, v3

    if-eqz v3, :cond_0

    .line 276
    cmp-long v3, v1, v5

    if-eqz v3, :cond_0

    .line 282
    return-wide v1

    .line 277
    :cond_0
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 278
    .local v0, "rng":Ljava/util/Random;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Random;->setSeed(J)V

    .line 279
    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v3

    and-long v1, v3, v5

    goto :goto_0
.end method

.method public static obtain()Lcom/samsung/appcessory/session/SAPSession;
    .locals 3

    .prologue
    .line 80
    sget-object v1, Lcom/samsung/appcessory/session/SAPSession;->sPool:Lcom/samsung/appcessory/session/SAPSession;

    if-eqz v1, :cond_0

    .line 81
    sget-object v0, Lcom/samsung/appcessory/session/SAPSession;->sPool:Lcom/samsung/appcessory/session/SAPSession;

    .line 86
    .local v0, "s":Lcom/samsung/appcessory/session/SAPSession;
    invoke-static {}, Lcom/samsung/appcessory/session/SAPSession;->generateUniqueId()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    .line 87
    iget-object v1, v0, Lcom/samsung/appcessory/session/SAPSession;->next:Lcom/samsung/appcessory/session/SAPSession;

    sput-object v1, Lcom/samsung/appcessory/session/SAPSession;->sPool:Lcom/samsung/appcessory/session/SAPSession;

    .line 88
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/appcessory/session/SAPSession;->next:Lcom/samsung/appcessory/session/SAPSession;

    .line 89
    sget v1, Lcom/samsung/appcessory/session/SAPSession;->sPoolSize:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/samsung/appcessory/session/SAPSession;->sPoolSize:I

    .line 90
    sget-object v1, Lcom/samsung/appcessory/session/SAPSession$SessionState;->INIT_STATE:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    iput-object v1, v0, Lcom/samsung/appcessory/session/SAPSession;->_state:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    .line 93
    .end local v0    # "s":Lcom/samsung/appcessory/session/SAPSession;
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/appcessory/session/SAPSession;

    invoke-direct {v0}, Lcom/samsung/appcessory/session/SAPSession;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/samsung/appcessory/base/IAccessoryEventListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .prologue
    .line 115
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 116
    if-eqz p1, :cond_0

    .line 117
    :try_start_0
    const-string v0, "SAP/SAPSession/29Dec2014"

    const-string v2, "addListener Success"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    :goto_0
    monitor-exit v1

    .line 123
    return-void

    .line 120
    :cond_0
    const-string v0, "SAP/SAPSession/29Dec2014"

    const-string v2, "addListener failed listener = null"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearAllListeners()V
    .locals 3

    .prologue
    .line 153
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 154
    :try_start_0
    const-string v0, "SAP/SAPSession/29Dec2014"

    const-string v2, "Clear listeners"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 153
    monitor-exit v1

    .line 157
    return-void

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method clearForRecycle()V
    .locals 7

    .prologue
    .line 174
    const-string v3, "SAP/SAPSession/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Recycling the session ..."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, p0, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSession;->mSessionTimer:Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    if-eqz v3, :cond_0

    .line 180
    const-string v3, "SAP/SAPSession/29Dec2014"

    const-string v4, "Stop the timer"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSession;->mSessionTimer:Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    invoke-virtual {v3}, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->cancel()Z

    move-result v2

    .line 182
    .local v2, "ret":Z
    const-string v3, "SAP/SAPSession/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Timer Cancel Status ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/appcessory/session/SAPSession;->mSessionTimer:Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    .line 186
    .end local v2    # "ret":Z
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/appcessory/session/SAPSession;->clearAllListeners()V

    .line 187
    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSession;->handler:Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;

    if-eqz v3, :cond_1

    .line 188
    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSession;->handler:Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;

    invoke-virtual {v3}, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    .line 189
    .local v1, "looper":Landroid/os/Looper;
    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    .line 192
    .end local v1    # "looper":Landroid/os/Looper;
    :cond_1
    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSession;->dispatcher:Ljava/util/concurrent/ExecutorService;

    if-eqz v3, :cond_2

    .line 193
    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSession;->dispatcher:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 196
    :try_start_0
    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSession;->dispatcher:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v4, 0xc8

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v5, v6}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 197
    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSession;->dispatcher:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :cond_2
    :goto_0
    return-void

    .line 200
    :catch_0
    move-exception v0

    .line 202
    .local v0, "ie":Ljava/lang/InterruptedException;
    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSession;->dispatcher:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 204
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public getHandler()Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPSession;->handler:Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;

    return-object v0
.end method

.method public getListeners()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/IAccessoryEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 134
    :try_start_0
    const-string v0, "SAP/SAPSession/29Dec2014"

    const-string v2, "getListeners"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    monitor-exit v1

    return-object v0

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getTimer()Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPSession;->mSessionTimer:Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    return-object v0
.end method

.method public init()V
    .locals 4

    .prologue
    .line 98
    const-string v2, "SAP/SAPSession/29Dec2014"

    const-string v3, "init called"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "SessionHandler"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 100
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 101
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    .line 102
    .local v1, "tmpLoop":Landroid/os/Looper;
    if-eqz v1, :cond_0

    .line 103
    const-string v2, "SAP/SAPSession/29Dec2014"

    const-string v3, "handler Created"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    new-instance v2, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;

    invoke-direct {v2, p0, v1}, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;-><init>(Lcom/samsung/appcessory/session/SAPSession;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/appcessory/session/SAPSession;->handler:Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;

    .line 111
    :goto_0
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/appcessory/session/SAPSession;->dispatcher:Ljava/util/concurrent/ExecutorService;

    .line 112
    return-void

    .line 107
    :cond_0
    const-string v2, "SAP/SAPSession/29Dec2014"

    const-string v3, "Handler not created"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public recycle()V
    .locals 3

    .prologue
    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/appcessory/session/SAPSession;->_opened:Z

    .line 162
    invoke-virtual {p0}, Lcom/samsung/appcessory/session/SAPSession;->clearForRecycle()V

    .line 164
    sget v0, Lcom/samsung/appcessory/session/SAPSession;->sPoolSize:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 165
    sget-object v0, Lcom/samsung/appcessory/session/SAPSession;->sPool:Lcom/samsung/appcessory/session/SAPSession;

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPSession;->next:Lcom/samsung/appcessory/session/SAPSession;

    .line 166
    sput-object p0, Lcom/samsung/appcessory/session/SAPSession;->sPool:Lcom/samsung/appcessory/session/SAPSession;

    .line 167
    sget v0, Lcom/samsung/appcessory/session/SAPSession;->sPoolSize:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/appcessory/session/SAPSession;->sPoolSize:I

    .line 170
    :cond_0
    const-string v0, "SAP/SAPSession/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Pool-size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/samsung/appcessory/session/SAPSession;->sPoolSize:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    return-void
.end method

.method public removeListener(Lcom/samsung/appcessory/base/IAccessoryEventListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .prologue
    .line 126
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 127
    :try_start_0
    const-string v0, "SAP/SAPSession/29Dec2014"

    const-string v2, "Remove listener"

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 126
    monitor-exit v1

    .line 130
    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public sendMessage(JLcom/samsung/appcessory/base/SAPMessage;)V
    .locals 3
    .param p1, "accessoryId"    # J
    .param p3, "msg"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    .line 140
    const-string v1, "DEBUG"

    const-string v2, "DEBUG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    sget-boolean v1, Lcom/samsung/appcessory/session/SAPSession;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/appcessory/session/SAPSession;->_opened:Z

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 142
    :cond_0
    sget-boolean v1, Lcom/samsung/appcessory/session/SAPSession;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession;->dispatcher:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 145
    :cond_1
    const-string v1, "SAP/SAPSession/29Dec2014"

    const-string v2, ">>> sendMessage"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    new-instance v0, Lcom/samsung/appcessory/session/SAPSession$SAPDispatchTask;

    invoke-direct {v0}, Lcom/samsung/appcessory/session/SAPSession$SAPDispatchTask;-><init>()V

    .line 147
    .local v0, "dispatch":Lcom/samsung/appcessory/session/SAPSession$SAPDispatchTask;
    iput-wide p1, v0, Lcom/samsung/appcessory/session/SAPSession$SAPDispatchTask;->_accessoryId:J

    .line 148
    iput-object p3, v0, Lcom/samsung/appcessory/session/SAPSession$SAPDispatchTask;->_msg:Lcom/samsung/appcessory/base/SAPMessage;

    .line 149
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession;->dispatcher:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 150
    return-void
.end method

.method public setTimer(Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;)V
    .locals 4
    .param p1, "timer"    # Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    .prologue
    .line 286
    const-string v0, "SAP/SAPSession/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setTimer sID="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPSession;->mSessionTimer:Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    .line 288
    return-void
.end method

.method public stopTimer()V
    .locals 5

    .prologue
    .line 291
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession;->mSessionTimer:Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession;->mSessionTimer:Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    invoke-virtual {v1}, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->cancel()Z

    move-result v0

    .line 293
    .local v0, "ret":Z
    const-string v1, "SAP/SAPSession/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Timer Stopped sID="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, p0, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " status="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/appcessory/session/SAPSession;->mSessionTimer:Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    .line 298
    .end local v0    # "ret":Z
    :goto_0
    return-void

    .line 296
    :cond_0
    const-string v1, "SAP/SAPSession/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "sessionTimer = null"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, p0, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
