.class public final Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;
.super Landroid/os/Handler;
.source "SAPSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/session/SAPSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "SAPSessionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/appcessory/session/SAPSession;


# direct methods
.method public constructor <init>(Lcom/samsung/appcessory/session/SAPSession;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->this$0:Lcom/samsung/appcessory/session/SAPSession;

    .line 212
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 213
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v3, 0xff

    .line 217
    if-nez p1, :cond_0

    .line 218
    const-string v1, "SAP/SAPSession/29Dec2014"

    const-string/jumbo v2, "msg is null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :goto_0
    return-void

    .line 221
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 262
    const-string v1, "SAP/SAPSession/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error in handleMessage:default "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 223
    :pswitch_0
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v9, Lcom/samsung/appcessory/base/SAPMessage;

    .line 226
    .local v9, "m":Lcom/samsung/appcessory/base/SAPMessage;
    if-nez v9, :cond_1

    .line 227
    const-string v1, "SAP/SAPSession/29Dec2014"

    const-string v2, "Message is null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 230
    :cond_1
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->this$0:Lcom/samsung/appcessory/session/SAPSession;

    iget-boolean v1, v1, Lcom/samsung/appcessory/session/SAPSession;->_opened:Z

    if-nez v1, :cond_2

    .line 231
    invoke-virtual {v9}, Lcom/samsung/appcessory/base/SAPMessage;->getSessionId()J

    move-result-wide v1

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    .line 232
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->this$0:Lcom/samsung/appcessory/session/SAPSession;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/samsung/appcessory/session/SAPSession;->_opened:Z

    .line 234
    :cond_2
    invoke-virtual {v9}, Lcom/samsung/appcessory/base/SAPMessage;->getSessionId()J

    move-result-wide v1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_3

    .line 235
    const-string v1, "SAP/SAPSession/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Set payload type ..."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->this$0:Lcom/samsung/appcessory/session/SAPSession;

    iget-byte v3, v3, Lcom/samsung/appcessory/session/SAPSession;->_payloadType:B

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->this$0:Lcom/samsung/appcessory/session/SAPSession;

    iget-byte v1, v1, Lcom/samsung/appcessory/session/SAPSession;->_payloadType:B

    invoke-virtual {v9, v1}, Lcom/samsung/appcessory/base/SAPMessage;->setpayloadType(B)V

    .line 239
    :cond_3
    new-instance v8, Lcom/samsung/appcessory/base/SAPMessageItem;

    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-long v1, v1

    .line 240
    iget v3, p1, Landroid/os/Message;->arg2:I

    int-to-long v3, v3

    .line 239
    invoke-direct {v8, v1, v2, v3, v4}, Lcom/samsung/appcessory/base/SAPMessageItem;-><init>(JJ)V

    .line 241
    .local v8, "item":Lcom/samsung/appcessory/base/SAPMessageItem;
    invoke-virtual {v8, v9}, Lcom/samsung/appcessory/base/SAPMessageItem;->setMessage(Lcom/samsung/appcessory/base/SAPMessage;)V

    .line 243
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->this$0:Lcom/samsung/appcessory/session/SAPSession;

    iget-object v2, v1, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    monitor-enter v2

    .line 244
    :try_start_0
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->this$0:Lcom/samsung/appcessory/session/SAPSession;

    iget-object v1, v1, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 243
    monitor-exit v2

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 244
    :cond_5
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .line 245
    .local v0, "listener":Lcom/samsung/appcessory/base/IAccessoryEventListener;
    if-eqz v0, :cond_4

    .line 246
    invoke-interface {v0, v8}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onAccessoryMessageReceived(Lcom/samsung/appcessory/base/SAPMessageItem;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 252
    .end local v0    # "listener":Lcom/samsung/appcessory/base/IAccessoryEventListener;
    .end local v8    # "item":Lcom/samsung/appcessory/base/SAPMessageItem;
    .end local v9    # "m":Lcom/samsung/appcessory/base/SAPMessage;
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->this$0:Lcom/samsung/appcessory/session/SAPSession;

    iget-object v10, v1, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    monitor-enter v10

    .line 253
    :try_start_2
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 254
    .local v6, "errCode":J
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSession$SAPSessionHandler;->this$0:Lcom/samsung/appcessory/session/SAPSession;

    iget-object v1, v1, Lcom/samsung/appcessory/session/SAPSession;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_6
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_7

    .line 252
    monitor-exit v10

    goto/16 :goto_0

    .end local v6    # "errCode":J
    :catchall_1
    move-exception v1

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    .line 254
    .restart local v6    # "errCode":J
    :cond_7
    :try_start_3
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .line 255
    .restart local v0    # "listener":Lcom/samsung/appcessory/base/IAccessoryEventListener;
    if-eqz v0, :cond_6

    .line 256
    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-long v1, v1

    iget v3, p1, Landroid/os/Message;->arg2:I

    int-to-long v3, v3

    long-to-int v5, v6

    invoke-interface/range {v0 .. v5}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onError(JJI)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 221
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
