.class public Lcom/samsung/appcessory/session/SAPCapexMsg;
.super Ljava/lang/Object;
.source "SAPCapexMsg.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SAP/SAPCapexMsg/29Dec2014"


# instance fields
.field private PayloadType:[B

.field private protoVersion:Ljava/lang/String;

.field private rspStatus:B

.field private supportedProfileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private swVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;B)V
    .locals 0
    .param p1, "_swVersion"    # Ljava/lang/String;
    .param p2, "_protoVersion"    # Ljava/lang/String;
    .param p3, "_rspStatus"    # B

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->swVersion:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->protoVersion:Ljava/lang/String;

    .line 42
    iput-byte p3, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->rspStatus:B

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .param p1, "_swVersion"    # Ljava/lang/String;
    .param p2, "_protoVersion"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "_supportedProfileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->swVersion:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->protoVersion:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->supportedProfileList:Ljava/util/List;

    .line 50
    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->PayloadType:[B

    .line 51
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->PayloadType:[B

    aput-byte v1, v0, v1

    .line 52
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->PayloadType:[B

    aput-byte v2, v0, v2

    .line 53
    return-void
.end method


# virtual methods
.method public GetProtoVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 65
    const-string v0, "SAP/SAPCapexMsg/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Proto version"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->protoVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->protoVersion:Ljava/lang/String;

    return-object v0
.end method

.method public GetSupportedProfileList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->supportedProfileList:Ljava/util/List;

    return-object v0
.end method

.method public GetSwVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 56
    const-string v0, "SAP/SAPCapexMsg/29Dec2014"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SW version"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->swVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->swVersion:Ljava/lang/String;

    return-object v0
.end method

.method public SetProtoVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "_protoVersion"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->protoVersion:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public SetSupportedProfileList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "_supportedProfileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->supportedProfileList:Ljava/util/List;

    .line 79
    return-void
.end method

.method public SetSwVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "_swVersion"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->swVersion:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public getNumPayloadType()C
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->PayloadType:[B

    array-length v0, v0

    int-to-char v0, v0

    return v0
.end method

.method public getNumProfileList()C
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->supportedProfileList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method public getPayloadType()[B
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->PayloadType:[B

    return-object v0
.end method

.method public getRspStatus()B
    .locals 1

    .prologue
    .line 98
    iget-byte v0, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->rspStatus:B

    return v0
.end method

.method public setPayloadType([B)V
    .locals 0
    .param p1, "payloadType"    # [B

    .prologue
    .line 94
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->PayloadType:[B

    .line 95
    return-void
.end method

.method public setRspStatus(B)V
    .locals 0
    .param p1, "rspStatus"    # B

    .prologue
    .line 102
    iput-byte p1, p0, Lcom/samsung/appcessory/session/SAPCapexMsg;->rspStatus:B

    .line 103
    return-void
.end method
