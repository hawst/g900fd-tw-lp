.class public Lcom/samsung/appcessory/session/SAPServAssocMsg;
.super Ljava/lang/Object;
.source "SAPServAssocMsg.java"


# instance fields
.field private servProfileIdentifier:I

.field private sessionIdentifier:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;I)V
    .locals 0
    .param p1, "_sessionIdentifier"    # Ljava/lang/Long;
    .param p2, "_servProfileIdentifier"    # I

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPServAssocMsg;->sessionIdentifier:Ljava/lang/Long;

    .line 31
    iput p2, p0, Lcom/samsung/appcessory/session/SAPServAssocMsg;->servProfileIdentifier:I

    .line 32
    return-void
.end method


# virtual methods
.method public getServProfileIdentifier()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/appcessory/session/SAPServAssocMsg;->servProfileIdentifier:I

    return v0
.end method

.method public getSessionIdentifier()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPServAssocMsg;->sessionIdentifier:Ljava/lang/Long;

    return-object v0
.end method

.method public setServProfileIdentifier(I)V
    .locals 0
    .param p1, "_servProfileIdentifier"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/appcessory/session/SAPServAssocMsg;->servProfileIdentifier:I

    .line 51
    return-void
.end method

.method public setSessionIdentifier(Ljava/lang/Long;)V
    .locals 0
    .param p1, "_sessionIdentifier"    # Ljava/lang/Long;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPServAssocMsg;->sessionIdentifier:Ljava/lang/Long;

    .line 43
    return-void
.end method
