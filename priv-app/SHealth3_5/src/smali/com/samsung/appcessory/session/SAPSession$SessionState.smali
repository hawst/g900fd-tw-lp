.class public final enum Lcom/samsung/appcessory/session/SAPSession$SessionState;
.super Ljava/lang/Enum;
.source "SAPSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/session/SAPSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SessionState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/appcessory/session/SAPSession$SessionState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CLOSING_SESSION:Lcom/samsung/appcessory/session/SAPSession$SessionState;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/appcessory/session/SAPSession$SessionState;

.field public static final enum INIT_STATE:Lcom/samsung/appcessory/session/SAPSession$SessionState;

.field public static final enum OPENING_SESSION:Lcom/samsung/appcessory/session/SAPSession$SessionState;

.field public static final enum SESSION_CLOSED:Lcom/samsung/appcessory/session/SAPSession$SessionState;

.field public static final enum SESSION_OPENED:Lcom/samsung/appcessory/session/SAPSession$SessionState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;

    const-string v1, "INIT_STATE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/appcessory/session/SAPSession$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;->INIT_STATE:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    .line 56
    new-instance v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;

    const-string v1, "OPENING_SESSION"

    invoke-direct {v0, v1, v3}, Lcom/samsung/appcessory/session/SAPSession$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;->OPENING_SESSION:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    .line 57
    new-instance v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;

    const-string v1, "CLOSING_SESSION"

    invoke-direct {v0, v1, v4}, Lcom/samsung/appcessory/session/SAPSession$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;->CLOSING_SESSION:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    .line 58
    new-instance v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;

    const-string v1, "SESSION_OPENED"

    invoke-direct {v0, v1, v5}, Lcom/samsung/appcessory/session/SAPSession$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;->SESSION_OPENED:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    .line 59
    new-instance v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;

    const-string v1, "SESSION_CLOSED"

    invoke-direct {v0, v1, v6}, Lcom/samsung/appcessory/session/SAPSession$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;->SESSION_CLOSED:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    .line 54
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/appcessory/session/SAPSession$SessionState;

    sget-object v1, Lcom/samsung/appcessory/session/SAPSession$SessionState;->INIT_STATE:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/appcessory/session/SAPSession$SessionState;->OPENING_SESSION:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/appcessory/session/SAPSession$SessionState;->CLOSING_SESSION:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/appcessory/session/SAPSession$SessionState;->SESSION_OPENED:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/appcessory/session/SAPSession$SessionState;->SESSION_CLOSED:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;->ENUM$VALUES:[Lcom/samsung/appcessory/session/SAPSession$SessionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/appcessory/session/SAPSession$SessionState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/appcessory/session/SAPSession$SessionState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/appcessory/session/SAPSession$SessionState;->ENUM$VALUES:[Lcom/samsung/appcessory/session/SAPSession$SessionState;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/appcessory/session/SAPSession$SessionState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
