.class public Lcom/samsung/appcessory/session/SAPSessionManager;
.super Ljava/lang/Object;
.source "SAPSessionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;
    }
.end annotation


# static fields
.field private static final ACCESSORY_FRAMEWORK_INIT_WAIT_DELAY:J = 0x3e8L

.field public static final CLOSE_SESSION_TIMEOUT:I = 0xa

.field public static final OPEN_SESSION_TIMEOUT:I = 0xa

.field public static final RESERVED_SESSION_ID:J = 0xffL

.field private static final SESSION_OPEN_RTT:J = 0x1f4L

.field private static final TAG:Ljava/lang/String; = "SAP/SAPSessionManager/29Dec2014"

.field private static msConnectionManager:Lcom/samsung/appcessory/transport/SAPConnectivityManager;

.field private static msSessionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/session/SAPSession;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final msSessionMapLock:Ljava/lang/Object;


# instance fields
.field genericEventListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

.field public pmmTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 459
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMapLock:Ljava/lang/Object;

    .line 461
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMap:Ljava/util/Map;

    .line 462
    new-instance v0, Lcom/samsung/appcessory/transport/SAPConnectivityManager;

    invoke-direct {v0}, Lcom/samsung/appcessory/transport/SAPConnectivityManager;-><init>()V

    sput-object v0, Lcom/samsung/appcessory/session/SAPSessionManager;->msConnectionManager:Lcom/samsung/appcessory/transport/SAPConnectivityManager;

    .line 481
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPSessionManager;->genericEventListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .line 478
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPSessionManager;->pmmTimer:Ljava/util/Timer;

    .line 42
    return-void
.end method

.method public static addSessionToMap(JLcom/samsung/appcessory/session/SAPSession;)V
    .locals 4
    .param p0, "accessoryId"    # J
    .param p2, "s"    # Lcom/samsung/appcessory/session/SAPSession;

    .prologue
    .line 361
    sget-object v1, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMapLock:Ljava/lang/Object;

    monitor-enter v1

    .line 362
    :try_start_0
    sget-object v0, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    sget-object v0, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 364
    const-string v0, "SAP/SAPSessionManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addSessionToMap accessoryId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    :cond_0
    monitor-exit v1

    .line 367
    return-void

    .line 361
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getAllAccessorySessions(J)Ljava/util/List;
    .locals 5
    .param p0, "accessoryId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/session/SAPSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344
    sget-object v2, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMapLock:Ljava/lang/Object;

    monitor-enter v2

    .line 345
    :try_start_0
    sget-object v1, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 346
    sget-object v1, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 347
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    .line 348
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getAllAccessorySessions No of session "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 349
    const-string v4, " for accessory "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 348
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 347
    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    monitor-exit v2

    .line 356
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    :goto_0
    return-object v0

    .line 352
    :cond_0
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    .line 353
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getAllAccessorySessions No session found for accessory "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 354
    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 353
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 352
    invoke-static {v1, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    monitor-exit v2

    const/4 v0, 0x0

    goto :goto_0

    .line 344
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getConnectionManager()Lcom/samsung/appcessory/transport/SAPConnectivityManager;
    .locals 1

    .prologue
    .line 319
    sget-object v0, Lcom/samsung/appcessory/session/SAPSessionManager;->msConnectionManager:Lcom/samsung/appcessory/transport/SAPConnectivityManager;

    return-object v0
.end method

.method public static getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;
    .locals 8
    .param p0, "accessoryId"    # J
    .param p2, "sessionId"    # J

    .prologue
    .line 323
    sget-object v4, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMapLock:Ljava/lang/Object;

    monitor-enter v4

    .line 324
    :try_start_0
    sget-object v3, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 325
    sget-object v3, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 326
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    const-string v3, "SAP/SAPSessionManager/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getSessionFromMap session size "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "search session "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 329
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/session/SAPSession;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 337
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/session/SAPSession;>;"
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    :cond_1
    const-string v3, "SAP/SAPSessionManager/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getSessionFromMap Failed no SAPSession obj for=>"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 338
    invoke-virtual {v5, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sessinID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 337
    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    monitor-exit v4

    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 330
    .restart local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/session/SAPSession;>;"
    .restart local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/appcessory/session/SAPSession;

    .line 331
    .local v2, "s":Lcom/samsung/appcessory/session/SAPSession;
    const-string v3, "SAP/SAPSessionManager/29Dec2014"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getSessionFromMap session id ="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v2, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    iget-wide v5, v2, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    cmp-long v3, v5, p2

    if-nez v3, :cond_0

    .line 333
    monitor-exit v4

    goto :goto_0

    .line 323
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/session/SAPSession;>;"
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    .end local v2    # "s":Lcom/samsung/appcessory/session/SAPSession;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public static removeSessionFromMap(JLcom/samsung/appcessory/session/SAPSession;)V
    .locals 3
    .param p0, "accessoryId"    # J
    .param p2, "s"    # Lcom/samsung/appcessory/session/SAPSession;

    .prologue
    .line 370
    sget-object v1, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMapLock:Ljava/lang/Object;

    monitor-enter v1

    .line 371
    :try_start_0
    sget-object v0, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    sget-object v0, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMap:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 370
    :cond_0
    monitor-exit v1

    .line 375
    return-void

    .line 370
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public addAccessoryEventListener(JJLcom/samsung/appcessory/base/IAccessoryEventListener;)V
    .locals 1
    .param p1, "accessoryId"    # J
    .param p3, "sessionId"    # J
    .param p5, "listener"    # Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .prologue
    .line 48
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v0

    .line 49
    .local v0, "s":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {v0, p5}, Lcom/samsung/appcessory/session/SAPSession;->addListener(Lcom/samsung/appcessory/base/IAccessoryEventListener;)V

    .line 52
    :cond_0
    return-void
.end method

.method public closeReservedSession(J)V
    .locals 11
    .param p1, "accessoryId"    # J

    .prologue
    .line 117
    const-string v5, "SAP/SAPSessionManager/29Dec2014"

    const-string v6, "Closing the reserved session"

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const-string v5, "SAP/SAPSessionManager/29Dec2014"

    const-string v6, "Any open application sessions will be closed as well ..."

    invoke-static {v5, v6}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const/4 v1, 0x0

    .line 120
    .local v1, "cachedSessions":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    sget-object v6, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMapLock:Ljava/lang/Object;

    monitor-enter v6

    .line 121
    :try_start_0
    sget-object v5, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/util/List;

    move-object v1, v0

    .line 122
    if-eqz v1, :cond_0

    .line 123
    const/4 v3, 0x0

    .line 124
    .local v3, "open":Z
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 125
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/session/SAPSession;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 133
    const/4 v1, 0x0

    .line 120
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/session/SAPSession;>;"
    .end local v3    # "open":Z
    :cond_0
    monitor-exit v6

    .line 136
    return-void

    .line 126
    .restart local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/session/SAPSession;>;"
    .restart local v3    # "open":Z
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/appcessory/session/SAPSession;

    .line 127
    .local v4, "s":Lcom/samsung/appcessory/session/SAPSession;
    if-nez v3, :cond_2

    const-wide/16 v7, 0xff

    iget-wide v9, v4, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    cmp-long v5, v7, v9

    if-eqz v5, :cond_2

    .line 128
    iget-boolean v5, v4, Lcom/samsung/appcessory/session/SAPSession;->_opened:Z

    or-int/2addr v3, v5

    .line 129
    const-string v5, "SAP/SAPSessionManager/29Dec2014"

    const-string v7, "Found open application session(s) ..."

    invoke-static {v5, v7}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_2
    invoke-virtual {v4}, Lcom/samsung/appcessory/session/SAPSession;->recycle()V

    goto :goto_0

    .line 120
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/appcessory/session/SAPSession;>;"
    .end local v3    # "open":Z
    .end local v4    # "s":Lcom/samsung/appcessory/session/SAPSession;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public closeSession(JJ)Z
    .locals 9
    .param p1, "accessoryId"    # J
    .param p3, "sessionId"    # J

    .prologue
    .line 243
    const-string v6, "SAP/SAPSessionManager/29Dec2014"

    const-string v7, "Negotiate session closure with the remote end ..."

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const/4 v2, 0x0

    .line 246
    .local v2, "ret":Z
    new-instance v1, Lcom/samsung/appcessory/protocol/SAPServiceParams;

    invoke-direct {v1}, Lcom/samsung/appcessory/protocol/SAPServiceParams;-><init>()V

    .line 247
    .local v1, "request":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    const/4 v6, 0x4

    iput v6, v1, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    .line 248
    iput-wide p3, v1, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    .line 249
    const/4 v6, 0x1

    iput-byte v6, v1, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionType:B

    .line 250
    invoke-static {v1}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->obtainCloseSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v0

    .line 254
    .local v0, "msg":Lcom/samsung/appcessory/base/SAPMessage;
    const-wide/16 v6, 0xff

    invoke-static {p1, p2, v6, v7}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v3

    .line 255
    .local v3, "s":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v3, :cond_1

    .line 257
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v4

    .line 258
    .local v4, "s1":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v4, :cond_0

    .line 259
    const-string v6, "SAP/SAPSessionManager/29Dec2014"

    const-string v7, "Close session Timer started"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    sget-object v6, Lcom/samsung/appcessory/session/SAPSession$SessionState;->CLOSING_SESSION:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    iput-object v6, v4, Lcom/samsung/appcessory/session/SAPSession;->_state:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    .line 261
    new-instance v5, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    invoke-direct {v5, p0, v4, p1, p2}, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;-><init>(Lcom/samsung/appcessory/session/SAPSessionManager;Lcom/samsung/appcessory/session/SAPSession;J)V

    .line 262
    .local v5, "timer":Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;
    iget-object v6, p0, Lcom/samsung/appcessory/session/SAPSessionManager;->pmmTimer:Ljava/util/Timer;

    const-wide/16 v7, 0x2710

    invoke-virtual {v6, v5, v7, v8}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 263
    invoke-virtual {v4, v5}, Lcom/samsung/appcessory/session/SAPSession;->setTimer(Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;)V

    .line 264
    invoke-virtual {v3, p1, p2, v0}, Lcom/samsung/appcessory/session/SAPSession;->sendMessage(JLcom/samsung/appcessory/base/SAPMessage;)V

    .line 265
    const/4 v2, 0x1

    .line 275
    .end local v4    # "s1":Lcom/samsung/appcessory/session/SAPSession;
    .end local v5    # "timer":Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;
    :goto_0
    return v2

    .line 269
    .restart local v4    # "s1":Lcom/samsung/appcessory/session/SAPSession;
    :cond_0
    const-string v6, "SAP/SAPSessionManager/29Dec2014"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Session with id "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " does not exist "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    const/4 v2, 0x0

    .line 272
    goto :goto_0

    .line 273
    .end local v4    # "s1":Lcom/samsung/appcessory/session/SAPSession;
    :cond_1
    const-string v6, "SAP/SAPSessionManager/29Dec2014"

    const-string v7, "Reserved session not found!"

    invoke-static {v6, v7}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public deRegisterEventListener()V
    .locals 2

    .prologue
    .line 383
    const-string v0, "SAP/SAPSessionManager/29Dec2014"

    const-string v1, "deRegisterEventListener enter"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPSessionManager;->genericEventListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .line 385
    return-void
.end method

.method public dispatchMessage(JJLcom/samsung/appcessory/base/SAPMessage;)Z
    .locals 5
    .param p1, "accessoryId"    # J
    .param p3, "sessionId"    # J
    .param p5, "message"    # Lcom/samsung/appcessory/base/SAPMessage;

    .prologue
    .line 280
    const-string v2, "SAP/SAPSessionManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dispatchMessage! on session "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    const/4 v0, 0x0

    .line 283
    .local v0, "ret":Z
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v1

    .line 284
    .local v1, "s":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v1, :cond_1

    .line 285
    const-wide/16 v2, 0xff

    cmp-long v2, p3, v2

    if-eqz v2, :cond_0

    .line 286
    const-string v2, "SAP/SAPSessionManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting payload type! "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v4, v1, Lcom/samsung/appcessory/session/SAPSession;->_payloadType:B

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-byte v2, v1, Lcom/samsung/appcessory/session/SAPSession;->_payloadType:B

    invoke-virtual {p5, v2}, Lcom/samsung/appcessory/base/SAPMessage;->setpayloadType(B)V

    .line 289
    :cond_0
    invoke-virtual {v1, p1, p2, p5}, Lcom/samsung/appcessory/session/SAPSession;->sendMessage(JLcom/samsung/appcessory/base/SAPMessage;)V

    .line 290
    const/4 v0, 0x1

    .line 297
    :goto_0
    return v0

    .line 292
    :cond_1
    const-string v2, "SAP/SAPSessionManager/29Dec2014"

    .line 293
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Obtained a NULL session when retrieving session ID: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 294
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for accessory ID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 295
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 293
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 292
    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAccessoryEventListener(JJ)Ljava/util/List;
    .locals 2
    .param p1, "accessoryId"    # J
    .param p3, "sessionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/appcessory/base/IAccessoryEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v0

    .line 66
    .local v0, "s":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {v0}, Lcom/samsung/appcessory/session/SAPSession;->getListeners()Ljava/util/List;

    move-result-object v1

    .line 69
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public initSession(BJ)Z
    .locals 17
    .param p1, "payloadType"    # B
    .param p2, "accessoryId"    # J

    .prologue
    .line 146
    const-string v13, "SAP/SAPSessionManager/29Dec2014"

    const-string v14, "Enter initSession"

    invoke-static {v13, v14}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    const/4 v6, 0x0

    .line 148
    .local v6, "ret":Z
    const-wide/16 v9, 0x7f

    .line 152
    .local v9, "sessionId":J
    invoke-static/range {p2 .. p3}, Lcom/samsung/appcessory/session/SAPSessionManager;->getAllAccessorySessions(J)Ljava/util/List;

    move-result-object v11

    .line 153
    .local v11, "sessionList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    if-nez v11, :cond_1

    .line 154
    const-string v13, "SAP/SAPSessionManager/29Dec2014"

    const-string v14, "Ist session opening in progress"

    invoke-static {v13, v14}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_0
    invoke-static {}, Lcom/samsung/appcessory/session/SAPSession;->obtain()Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v8

    .line 177
    .local v8, "session":Lcom/samsung/appcessory/session/SAPSession;
    sget-object v13, Lcom/samsung/appcessory/session/SAPSession$SessionState;->OPENING_SESSION:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    iput-object v13, v8, Lcom/samsung/appcessory/session/SAPSession;->_state:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    .line 178
    new-instance v12, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v12, v0, v8, v1, v2}, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;-><init>(Lcom/samsung/appcessory/session/SAPSessionManager;Lcom/samsung/appcessory/session/SAPSession;J)V

    .line 179
    .local v12, "timer":Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/appcessory/session/SAPSessionManager;->pmmTimer:Ljava/util/Timer;

    const-wide/16 v14, 0x2710

    invoke-virtual {v13, v12, v14, v15}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 180
    invoke-virtual {v8, v12}, Lcom/samsung/appcessory/session/SAPSession;->setTimer(Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;)V

    .line 182
    const-string v13, "SAP/SAPSessionManager/29Dec2014"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Setting payload type "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "sId ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-wide v15, v8, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    move/from16 v0, p1

    iput-byte v0, v8, Lcom/samsung/appcessory/session/SAPSession;->_payloadType:B

    .line 184
    move-wide/from16 v0, p2

    invoke-static {v0, v1, v8}, Lcom/samsung/appcessory/session/SAPSessionManager;->addSessionToMap(JLcom/samsung/appcessory/session/SAPSession;)V

    .line 186
    iget-wide v9, v8, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    .line 187
    const-string v13, "SAP/SAPSessionManager/29Dec2014"

    .line 188
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Obtained session with ID: "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v15, v8, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 187
    invoke-static {v13, v14}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const-string v13, "SAP/SAPSessionManager/29Dec2014"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Negotiate session opening with the remote end accessoryId="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    new-instance v5, Lcom/samsung/appcessory/protocol/SAPServiceParams;

    invoke-direct {v5}, Lcom/samsung/appcessory/protocol/SAPServiceParams;-><init>()V

    .line 200
    .local v5, "request":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    const/4 v13, 0x3

    iput v13, v5, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    .line 202
    iput-wide v9, v5, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionId:J

    .line 203
    const/4 v13, 0x1

    iput-byte v13, v5, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_sessionType:B

    .line 204
    move/from16 v0, p1

    iput-byte v0, v5, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_payloadType:B

    .line 205
    invoke-static {v5}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->obtainOpenSessionMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v4

    .line 209
    .local v4, "msg":Lcom/samsung/appcessory/base/SAPMessage;
    const-wide/16 v13, 0xff

    move-wide/from16 v0, p2

    invoke-static {v0, v1, v13, v14}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v7

    .line 210
    .local v7, "s":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v7, :cond_5

    .line 211
    move-wide/from16 v0, p2

    invoke-virtual {v7, v0, v1, v4}, Lcom/samsung/appcessory/session/SAPSession;->sendMessage(JLcom/samsung/appcessory/base/SAPMessage;)V

    .line 212
    const/4 v6, 0x1

    .line 218
    :goto_0
    const-string v13, "SAP/SAPSessionManager/29Dec2014"

    const-string v14, ">>> Session initialize complete"

    invoke-static {v13, v14}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v13, v6

    .line 219
    .end local v4    # "msg":Lcom/samsung/appcessory/base/SAPMessage;
    .end local v5    # "request":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .end local v8    # "session":Lcom/samsung/appcessory/session/SAPSession;
    .end local v12    # "timer":Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;
    :goto_1
    return v13

    .line 156
    .end local v7    # "s":Lcom/samsung/appcessory/session/SAPSession;
    :cond_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v13

    if-ge v3, v13, :cond_0

    .line 157
    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/appcessory/session/SAPSession;

    .line 158
    .restart local v7    # "s":Lcom/samsung/appcessory/session/SAPSession;
    const-string v13, "SAP/SAPSessionManager/29Dec2014"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string/jumbo v15, "s_ID="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v15, v7, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " accessoryId="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-wide/from16 v0, p2

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v13, v7, Lcom/samsung/appcessory/session/SAPSession;->_state:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    sget-object v14, Lcom/samsung/appcessory/session/SAPSession$SessionState;->OPENING_SESSION:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    if-eq v13, v14, :cond_2

    .line 160
    iget-object v13, v7, Lcom/samsung/appcessory/session/SAPSession;->_state:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    sget-object v14, Lcom/samsung/appcessory/session/SAPSession$SessionState;->SESSION_OPENED:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    if-ne v13, v14, :cond_4

    .line 161
    :cond_2
    const-string v13, "SAP/SAPSessionManager/29Dec2014"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Opening session already in progress  || OPENED for ID="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 162
    const-string v15, "SessionState ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v7, Lcom/samsung/appcessory/session/SAPSession;->_state:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 161
    invoke-static {v13, v14}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/appcessory/session/SAPSessionManager;->genericEventListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    if-eqz v13, :cond_3

    .line 164
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/appcessory/session/SAPSessionManager;->genericEventListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    sget-object v14, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    .line 165
    const/16 v15, 0x6a

    .line 164
    move-wide/from16 v0, p2

    invoke-interface {v13, v14, v0, v1, v15}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V

    .line 169
    :goto_3
    const/4 v13, 0x0

    goto :goto_1

    .line 167
    :cond_3
    const-string v13, "SAP/SAPSessionManager/29Dec2014"

    const-string v14, "initSession genericEventListener = null"

    invoke-static {v13, v14}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 156
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 214
    .end local v3    # "i":I
    .restart local v4    # "msg":Lcom/samsung/appcessory/base/SAPMessage;
    .restart local v5    # "request":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    .restart local v8    # "session":Lcom/samsung/appcessory/session/SAPSession;
    .restart local v12    # "timer":Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;
    :cond_5
    const-string v13, "SAP/SAPSessionManager/29Dec2014"

    const-string v14, "Reserved session not found!"

    invoke-static {v13, v14}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public openReservedSession(JLcom/samsung/appcessory/base/IAccessoryEventListener;)Z
    .locals 7
    .param p1, "accessoryId"    # J
    .param p3, "listener"    # Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .prologue
    const-wide/16 v2, 0xff

    const/4 v6, 0x1

    .line 90
    invoke-static {p1, p2, v2, v3}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v0

    .line 91
    .local v0, "s":Lcom/samsung/appcessory/session/SAPSession;
    if-nez v0, :cond_0

    .line 92
    invoke-static {}, Lcom/samsung/appcessory/session/SAPSession;->obtain()Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v0

    .line 93
    iput-wide v2, v0, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    .line 95
    iput-boolean v6, v0, Lcom/samsung/appcessory/session/SAPSession;->_opened:Z

    .line 98
    invoke-virtual {v0, p3}, Lcom/samsung/appcessory/session/SAPSession;->addListener(Lcom/samsung/appcessory/base/IAccessoryEventListener;)V

    .line 101
    invoke-virtual {v0}, Lcom/samsung/appcessory/session/SAPSession;->init()V

    .line 103
    const-string v2, "SAP/SAPSessionManager/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Reserved session created ...accessoryId="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " s._id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v0, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v1, "sessions":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    sget-object v3, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMapLock:Ljava/lang/Object;

    monitor-enter v3

    .line 107
    :try_start_0
    sget-object v2, Lcom/samsung/appcessory/session/SAPSessionManager;->msSessionMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    monitor-exit v3

    .line 110
    .end local v1    # "sessions":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    :cond_0
    return v6

    .line 106
    .restart local v1    # "sessions":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/appcessory/session/SAPSession;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public registerEventListener(Lcom/samsung/appcessory/base/IAccessoryEventListener;)V
    .locals 2
    .param p1, "list"    # Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .prologue
    .line 378
    const-string v0, "SAP/SAPSessionManager/29Dec2014"

    const-string/jumbo v1, "registerEventListener done"

    invoke-static {v0, v1}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPSessionManager;->genericEventListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .line 380
    return-void
.end method

.method public removeAccessoryEventListener(JJLcom/samsung/appcessory/base/IAccessoryEventListener;)V
    .locals 1
    .param p1, "accessoryId"    # J
    .param p3, "sessionId"    # J
    .param p5, "listener"    # Lcom/samsung/appcessory/base/IAccessoryEventListener;

    .prologue
    .line 57
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v0

    .line 58
    .local v0, "s":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {v0, p5}, Lcom/samsung/appcessory/session/SAPSession;->removeListener(Lcom/samsung/appcessory/base/IAccessoryEventListener;)V

    .line 61
    :cond_0
    return-void
.end method

.method public sendTransportData(JJ[B)V
    .locals 6
    .param p1, "accessoryId"    # J
    .param p3, "sessionId"    # J
    .param p5, "payload"    # [B

    .prologue
    .line 302
    const-string v3, "SAP/SAPSessionManager/29Dec2014"

    const-string v4, "Sending binary data using TRANSPORT ..."

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    new-instance v1, Lcom/samsung/appcessory/protocol/SAPServiceParams;

    invoke-direct {v1}, Lcom/samsung/appcessory/protocol/SAPServiceParams;-><init>()V

    .line 305
    .local v1, "request":Lcom/samsung/appcessory/protocol/SAPServiceParams;
    const/4 v3, 0x1

    iput v3, v1, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_serviceId:I

    .line 306
    iput-object p5, v1, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_transportPayload:[B

    .line 307
    invoke-static {v1, p3, p4}, Lcom/samsung/appcessory/protocol/SAPFramingManager;->obtainTransportMessage(Lcom/samsung/appcessory/protocol/SAPServiceParams;J)Lcom/samsung/appcessory/base/SAPMessage;

    move-result-object v0

    .line 310
    .local v0, "msg":Lcom/samsung/appcessory/base/SAPMessage;
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v2

    .line 311
    .local v2, "s":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v2, :cond_0

    .line 312
    invoke-virtual {v2, p1, p2, v0}, Lcom/samsung/appcessory/session/SAPSession;->sendMessage(JLcom/samsung/appcessory/base/SAPMessage;)V

    .line 316
    :goto_0
    return-void

    .line 314
    :cond_0
    const-string v3, "SAP/SAPSessionManager/29Dec2014"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Session not found! "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
