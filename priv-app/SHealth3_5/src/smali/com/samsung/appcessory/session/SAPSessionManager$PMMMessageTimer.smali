.class public Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;
.super Ljava/util/TimerTask;
.source "SAPSessionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/session/SAPSessionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PMMMessageTimer"
.end annotation


# instance fields
.field private accId:J

.field private s:Lcom/samsung/appcessory/session/SAPSession;

.field final synthetic this$0:Lcom/samsung/appcessory/session/SAPSessionManager;


# direct methods
.method public constructor <init>(Lcom/samsung/appcessory/session/SAPSessionManager;Lcom/samsung/appcessory/session/SAPSession;J)V
    .locals 1
    .param p2, "session"    # Lcom/samsung/appcessory/session/SAPSession;
    .param p3, "id"    # J

    .prologue
    .line 392
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->this$0:Lcom/samsung/appcessory/session/SAPSessionManager;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 390
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    .line 393
    iput-object p2, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    .line 394
    iput-wide p3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->accId:J

    .line 395
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 397
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    if-nez v1, :cond_0

    .line 398
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    const-string v2, "Invalid session in Timer thread"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :goto_0
    return-void

    .line 401
    :cond_0
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Timer Expired for sId=>"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    iget-wide v3, v3, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    invoke-virtual {v1}, Lcom/samsung/appcessory/session/SAPSession;->getTimer()Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;

    move-result-object v1

    if-nez v1, :cond_1

    .line 404
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    const-string v2, "Timer is Already Stopped return"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 407
    :cond_1
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/appcessory/session/SAPSession;->setTimer(Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;)V

    .line 409
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    iget-object v1, v1, Lcom/samsung/appcessory/session/SAPSession;->_state:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    sget-object v2, Lcom/samsung/appcessory/session/SAPSession$SessionState;->OPENING_SESSION:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    if-ne v1, v2, :cond_4

    .line 410
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Opening session failed for Session ID=>"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    iget-wide v3, v3, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    iget-wide v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->accId:J

    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    iget-wide v3, v3, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v0

    .line 412
    .local v0, "temp":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v0, :cond_3

    .line 413
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    invoke-virtual {v1}, Lcom/samsung/appcessory/session/SAPSession;->recycle()V

    .line 414
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->this$0:Lcom/samsung/appcessory/session/SAPSessionManager;

    iget-object v1, v1, Lcom/samsung/appcessory/session/SAPSessionManager;->genericEventListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    if-eqz v1, :cond_2

    .line 415
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->this$0:Lcom/samsung/appcessory/session/SAPSessionManager;

    iget-object v1, v1, Lcom/samsung/appcessory/session/SAPSessionManager;->genericEventListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iget-wide v3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->accId:J

    const/16 v5, 0x6b

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V

    .line 422
    :goto_1
    iget-wide v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->accId:J

    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    invoke-static {v1, v2, v3}, Lcom/samsung/appcessory/session/SAPSessionManager;->removeSessionFromMap(JLcom/samsung/appcessory/session/SAPSession;)V

    goto :goto_0

    .line 417
    :cond_2
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    const-string v2, "genericEventListener = null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 420
    :cond_3
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    const-string v2, "Already Recycled...WRONG OPEN TIMEOUT check"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 424
    .end local v0    # "temp":Lcom/samsung/appcessory/session/SAPSession;
    :cond_4
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    iget-object v1, v1, Lcom/samsung/appcessory/session/SAPSession;->_state:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    sget-object v2, Lcom/samsung/appcessory/session/SAPSession$SessionState;->CLOSING_SESSION:Lcom/samsung/appcessory/session/SAPSession$SessionState;

    if-ne v1, v2, :cond_7

    .line 425
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Closing session failed for Session ID=>sID ="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    iget-wide v3, v3, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "AccID ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->accId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-wide v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->accId:J

    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    iget-wide v3, v3, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/appcessory/session/SAPSessionManager;->getSessionFromMap(JJ)Lcom/samsung/appcessory/session/SAPSession;

    move-result-object v0

    .line 427
    .restart local v0    # "temp":Lcom/samsung/appcessory/session/SAPSession;
    if-eqz v0, :cond_6

    .line 428
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    invoke-virtual {v1}, Lcom/samsung/appcessory/session/SAPSession;->recycle()V

    .line 429
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->this$0:Lcom/samsung/appcessory/session/SAPSessionManager;

    iget-object v1, v1, Lcom/samsung/appcessory/session/SAPSessionManager;->genericEventListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    if-eqz v1, :cond_5

    .line 430
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->this$0:Lcom/samsung/appcessory/session/SAPSessionManager;

    iget-object v1, v1, Lcom/samsung/appcessory/session/SAPSessionManager;->genericEventListener:Lcom/samsung/appcessory/base/IAccessoryEventListener;

    sget-object v2, Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;->ACC_UNKNOWN:Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;

    iget-wide v3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->accId:J

    const/16 v5, 0x6c

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/samsung/appcessory/base/IAccessoryEventListener;->onError(Lcom/samsung/appcessory/base/SAPBaseAccessory$SAPAccessoryType;JI)V

    .line 438
    :goto_2
    iget-wide v1, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->accId:J

    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    invoke-static {v1, v2, v3}, Lcom/samsung/appcessory/session/SAPSessionManager;->removeSessionFromMap(JLcom/samsung/appcessory/session/SAPSession;)V

    goto/16 :goto_0

    .line 432
    :cond_5
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    const-string v2, "genericEventListener = null"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 435
    :cond_6
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    const-string v2, "Already Recycled...WRONG TIMEOUT check"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 442
    .end local v0    # "temp":Lcom/samsung/appcessory/session/SAPSession;
    :cond_7
    const-string v1, "SAP/SAPSessionManager/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Successfully Executed PMM message for session ID"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSessionManager$PMMMessageTimer;->s:Lcom/samsung/appcessory/session/SAPSession;

    iget-wide v3, v3, Lcom/samsung/appcessory/session/SAPSession;->_id:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
