.class Lcom/samsung/appcessory/session/SAPSession$SAPDispatchTask;
.super Ljava/lang/Object;
.source "SAPSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/appcessory/session/SAPSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SAPDispatchTask"
.end annotation


# instance fields
.field public _accessoryId:J

.field public _msg:Lcom/samsung/appcessory/base/SAPMessage;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 313
    invoke-static {}, Lcom/samsung/appcessory/session/SAPSessionManager;->getConnectionManager()Lcom/samsung/appcessory/transport/SAPConnectivityManager;

    move-result-object v0

    .line 314
    .local v0, "mgr":Lcom/samsung/appcessory/transport/SAPConnectivityManager;
    if-eqz v0, :cond_0

    .line 315
    iget-wide v1, p0, Lcom/samsung/appcessory/session/SAPSession$SAPDispatchTask;->_accessoryId:J

    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPSession$SAPDispatchTask;->_msg:Lcom/samsung/appcessory/base/SAPMessage;

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/appcessory/transport/SAPConnectivityManager;->sendToAccessory(JLcom/samsung/appcessory/base/SAPMessage;)V

    .line 317
    :cond_0
    return-void
.end method
