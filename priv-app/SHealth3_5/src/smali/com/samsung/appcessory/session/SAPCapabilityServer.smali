.class public Lcom/samsung/appcessory/session/SAPCapabilityServer;
.super Ljava/lang/Object;
.source "SAPCapabilityServer.java"


# static fields
.field public static final CALENDAR_SRV_IDEN:I = 0x20006

.field public static final CALENDAR_SRV_URI:Ljava/lang/String; = "/System/Calendars"

.field public static final CALL_SRV_IDEN:I = 0x20007

.field public static final CALL_SRV_URI:Ljava/lang/String; = "/System/Call"

.field public static final CAPEX_MODE_MO:I = 0x0

.field public static final CAPEX_MODE_MT:I = 0x1

.field public static final DEST_SETTINGS_SRV_IDEN:I = 0x20004

.field public static final DEST_SETTINGS_SRV_URI:Ljava/lang/String; = "/System/Destination_Setting"

.field public static final LOCATION_SRV:Ljava/lang/String; = "com.samsung.location"

.field public static final LOCATION_SRV_IDEN:I = 0x20004

.field public static final LOCATION_SRV_URI:Ljava/lang/String; = "/System/Location"

.field public static final MESSAGES_SRV_IDEN:I = 0x20008

.field public static final MESSAGES_SRV_URI:Ljava/lang/String; = "/System/Messages"

.field public static final MESSAGING_SRV_IDEN:I = 0x20005

.field public static final MESSAGING_SRV_URI:Ljava/lang/String; = "/System/Messaging"

.field private static final NEG_FULL:B = 0x0t

.field private static final NEG_INCOMPATIBLE:B = 0x2t

.field private static final NEG_PARTIAL:B = 0x1t

.field public static final NOTIFICATION_SRV:Ljava/lang/String; = "com.samsung.notification"

.field public static final NOTIFICATION_SRV_URI:Ljava/lang/String; = "/System/Notification"

.field public static final S_BAND_SRV_IDEN:I = 0x30007

.field private static final S_BAND_SRV_URI:Ljava/lang/String; = "/Privilege/S-Band"

.field private static final TAG:Ljava/lang/String; = "SAP/SAPCapabilityServer/29Dec2014"


# instance fields
.field private accSrvList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private capexMode:I

.field private curProtoVer:Ljava/lang/String;

.field private curSwVer:Ljava/lang/String;

.field private msgHndlr:Lcom/samsung/appcessory/base/SAPMessageHandler;

.field private serviceUris:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private svcDir:Lcom/samsung/appcessory/server/SAPServiceDirectory;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->serviceUris:Ljava/util/List;

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->setAccSrvList(Ljava/util/HashMap;)V

    .line 79
    new-instance v0, Lcom/samsung/appcessory/base/SAPMessageHandler;

    invoke-direct {v0}, Lcom/samsung/appcessory/base/SAPMessageHandler;-><init>()V

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->msgHndlr:Lcom/samsung/appcessory/base/SAPMessageHandler;

    .line 80
    const-string v0, "1.1"

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->curSwVer:Ljava/lang/String;

    .line 81
    const-string v0, "1.1"

    iput-object v0, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->curProtoVer:Ljava/lang/String;

    .line 82
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->setCapexMode(I)V

    .line 83
    return-void
.end method

.method private cacheRmtServices(Ljava/lang/Long;Lcom/samsung/appcessory/protocol/SAPServiceParams;)Z
    .locals 2
    .param p1, "accessoryId"    # Ljava/lang/Long;
    .param p2, "param"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->getAccSrvList()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_services:Ljava/util/List;

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    const/4 v0, 0x1

    return v0
.end method

.method private chkCompatibility(Lcom/samsung/appcessory/protocol/SAPServiceParams;)B
    .locals 7
    .param p1, "param"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 166
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p1, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    aget-byte v4, v4, v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    aget-byte v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 167
    .local v0, "protoVer":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->curProtoVer:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 168
    const/4 v1, 0x2

    .line 169
    .local v1, "rspStatus":B
    const-string v3, "SAP/SAPCapabilityServer/29Dec2014"

    const-string v4, "Incompatible device"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :goto_0
    return v1

    .line 171
    .end local v1    # "rspStatus":B
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p1, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-byte v4, v4, v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-byte v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 172
    .local v2, "swVer":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->curSwVer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 173
    const/4 v1, 0x1

    .line 174
    .restart local v1    # "rspStatus":B
    const-string v3, "SAP/SAPCapabilityServer/29Dec2014"

    const-string v4, "Partial compatible device"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 176
    .end local v1    # "rspStatus":B
    :cond_1
    const/4 v1, 0x0

    .line 177
    .restart local v1    # "rspStatus":B
    const-string v3, "SAP/SAPCapabilityServer/29Dec2014"

    const-string v4, "compatible device"

    invoke-static {v3, v4}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static convertToServiceProfileIdentifier(Ljava/lang/String;)I
    .locals 4
    .param p0, "serviceProfile"    # Ljava/lang/String;

    .prologue
    .line 218
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "convertToServiceProfileIdentifier enter service profile = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 219
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 218
    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v0, 0x0

    .line 221
    .local v0, "serviceProfileIdentifier":I
    const-string v1, "/System/Location"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 222
    const-string v1, "com.samsung.location"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 223
    :cond_0
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier  LOCATION_SRV_URI"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    const v0, 0x20004

    .line 246
    :goto_0
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string v2, "convertToServiceProfileIdentifier exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    return v0

    .line 225
    :cond_1
    const-string v1, "/System/Destination_Setting"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 226
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier  DEST_SETTINGS_SRV_URI"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const v0, 0x20004

    .line 228
    goto :goto_0

    :cond_2
    const-string v1, "/System/Messaging"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 229
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier  MESSAGING_SRV_URI"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const v0, 0x20005

    .line 231
    goto :goto_0

    :cond_3
    const-string v1, "/System/Calendars"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 232
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier  CALENDAR_SRV_URI"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    const v0, 0x20006

    .line 234
    goto :goto_0

    :cond_4
    const-string v1, "/System/Call"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 235
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier  CALL_SRV_URI"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    const v0, 0x20007

    .line 237
    goto :goto_0

    :cond_5
    const-string v1, "/System/Messages"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 238
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier  MESSAGES_SRV_URI"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    const v0, 0x20008

    .line 240
    goto :goto_0

    :cond_6
    const-string v1, "/Privilege/S-Band"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 241
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier  S_BAND_SRV_URI"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    const v0, 0x30007

    .line 243
    goto :goto_0

    .line 244
    :cond_7
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown service profile "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static convertToServiceProfileIdentifier(I)Ljava/lang/String;
    .locals 5
    .param p0, "serviceProfileId"    # I

    .prologue
    const v4, 0x20004

    .line 251
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    .line 252
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "convertToServiceProfileIdentifier enter service profile id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 253
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 252
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 251
    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    const/4 v0, 0x0

    .line 255
    .local v0, "serviceProfile":Ljava/lang/String;
    if-ne p0, v4, :cond_0

    .line 256
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier id LOCATION_SRV_IDEN"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    const-string v0, "/System/Location"

    .line 279
    :goto_0
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string v2, "convertToServiceProfileIdentifier exit"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    return-object v0

    .line 258
    :cond_0
    if-ne p0, v4, :cond_1

    .line 259
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier id DEST_SETTINGS_SRV_IDEN"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const-string v0, "/System/Destination_Setting"

    .line 261
    goto :goto_0

    :cond_1
    const v1, 0x20005

    if-ne p0, v1, :cond_2

    .line 262
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier id MESSAGING_SRV_IDEN"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const-string v0, "/System/Messaging"

    .line 264
    goto :goto_0

    :cond_2
    const v1, 0x20006

    if-ne p0, v1, :cond_3

    .line 265
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier id CALENDAR_SRV_IDEN"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    const-string v0, "/System/Calendars"

    .line 267
    goto :goto_0

    :cond_3
    const v1, 0x20007

    if-ne p0, v1, :cond_4

    .line 268
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier id CALL_SRV_IDEN"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    const-string v0, "/System/Call"

    .line 270
    goto :goto_0

    :cond_4
    const v1, 0x20008

    if-ne p0, v1, :cond_5

    .line 271
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier id MESSAGES_SRV_IDEN"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const-string v0, "/System/Messages"

    .line 273
    goto :goto_0

    :cond_5
    const v1, 0x30007

    if-ne p0, v1, :cond_6

    .line 274
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    const-string/jumbo v2, "profile identifier id S_BAND_SRV_IDEN"

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    const-string v0, "/Privilege/S-Band"

    .line 276
    goto :goto_0

    .line 277
    :cond_6
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown service profile id"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private convertToServiceUris(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "services":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "SAP/SAPCapabilityServer/29Dec2014"

    .line 87
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "convertToServiceUris enter services count = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 88
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 87
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 102
    const-string v2, "SAP/SAPCapabilityServer/29Dec2014"

    const-string v3, "convertToServiceUris exit"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v2, 0x1

    return v2

    .line 90
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 91
    .local v1, "service":Ljava/lang/String;
    const-string v2, "SAP/SAPCapabilityServer/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Adding service "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const-string v2, "com.samsung.location"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 93
    const-string v2, "SAP/SAPCapabilityServer/29Dec2014"

    const-string v3, "adding  LOCATION_SRV"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v2, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->serviceUris:Ljava/util/List;

    const-string v3, "/System/Location"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_1
    const-string v2, "com.samsung.notification"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    const-string v2, "SAP/SAPCapabilityServer/29Dec2014"

    const-string v3, "adding  NOTIFICATION_SRV"

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v2, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->serviceUris:Ljava/util/List;

    const-string v3, "/System/Notification"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 99
    :cond_2
    const-string v2, "SAP/SAPCapabilityServer/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown service "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public getAccSrvList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->accSrvList:Ljava/util/HashMap;

    return-object v0
.end method

.method public getCapexMode()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->capexMode:I

    return v0
.end method

.method public getMsgHndlr()Lcom/samsung/appcessory/base/SAPMessageHandler;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->msgHndlr:Lcom/samsung/appcessory/base/SAPMessageHandler;

    return-object v0
.end method

.method public getRmtServices(Ljava/lang/Long;)Ljava/util/List;
    .locals 1
    .param p1, "accessoryId"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->getAccSrvList()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getSvcDir()Lcom/samsung/appcessory/server/SAPServiceDirectory;
    .locals 2

    .prologue
    .line 284
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->svcDir:Lcom/samsung/appcessory/server/SAPServiceDirectory;

    if-nez v1, :cond_0

    .line 286
    :try_start_0
    new-instance v1, Lcom/samsung/appcessory/server/SAPServiceDirectory;

    invoke-direct {v1}, Lcom/samsung/appcessory/server/SAPServiceDirectory;-><init>()V

    iput-object v1, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->svcDir:Lcom/samsung/appcessory/server/SAPServiceDirectory;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->svcDir:Lcom/samsung/appcessory/server/SAPServiceDirectory;

    return-object v1

    .line 287
    :catch_0
    move-exception v0

    .line 289
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public processCapabilityReq(Ljava/lang/Long;Lcom/samsung/appcessory/protocol/SAPServiceParams;)Z
    .locals 6
    .param p1, "accessoryId"    # Ljava/lang/Long;
    .param p2, "param"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    const/4 v5, 0x1

    .line 129
    invoke-direct {p0, p2}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->chkCompatibility(Lcom/samsung/appcessory/protocol/SAPServiceParams;)B

    move-result v1

    .line 130
    .local v1, "rspStatus":B
    new-instance v0, Lcom/samsung/appcessory/session/SAPCapexMsg;

    iget-object v2, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->curSwVer:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->curProtoVer:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/appcessory/session/SAPCapexMsg;-><init>(Ljava/lang/String;Ljava/lang/String;B)V

    .line 131
    .local v0, "capexRsp":Lcom/samsung/appcessory/session/SAPCapexMsg;
    const-string v2, "SAP/SAPCapabilityServer/29Dec2014"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Send capex response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-virtual {p0}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->getMsgHndlr()Lcom/samsung/appcessory/base/SAPMessageHandler;

    move-result-object v2

    invoke-virtual {v2, p1, v0}, Lcom/samsung/appcessory/base/SAPMessageHandler;->sendCapabilityRsp(Ljava/lang/Long;Lcom/samsung/appcessory/session/SAPCapexMsg;)Z

    .line 134
    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 135
    invoke-direct {p0, p1, p2}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->cacheRmtServices(Ljava/lang/Long;Lcom/samsung/appcessory/protocol/SAPServiceParams;)Z

    .line 141
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->getCapexMode()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 142
    invoke-virtual {p0, p1}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->sendCapability(Ljava/lang/Long;)Z

    .line 145
    :cond_1
    return v5
.end method

.method public processCapabilityRsp(Ljava/lang/Long;Lcom/samsung/appcessory/protocol/SAPServiceParams;)Z
    .locals 6
    .param p1, "accessoryId"    # Ljava/lang/Long;
    .param p2, "param"    # Lcom/samsung/appcessory/protocol/SAPServiceParams;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 149
    iget-byte v0, p2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_capexStatus:B

    .line 150
    .local v0, "rspStatus":B
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 151
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Device "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not compatible swVer = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 152
    iget-object v3, p2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-byte v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-byte v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 153
    const-string/jumbo v3, "protocolVersions = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_protoVersion:[B

    invoke-static {v3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 151
    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :goto_0
    return v4

    .line 155
    :cond_0
    if-ne v0, v4, :cond_1

    .line 156
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Device "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " partial compatible swVer = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 157
    iget-object v3, p2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-byte v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/samsung/appcessory/protocol/SAPServiceParams;->_swVersion:[B

    aget-byte v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 156
    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 159
    :cond_1
    const-string v1, "SAP/SAPCapabilityServer/29Dec2014"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Device "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " compatible"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/appcessory/utils/config/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendCapability(Ljava/lang/Long;)Z
    .locals 6
    .param p1, "accessoryId"    # Ljava/lang/Long;

    .prologue
    .line 108
    const/4 v2, 0x0

    .line 110
    .local v2, "services":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->getSvcDir()Lcom/samsung/appcessory/server/SAPServiceDirectory;

    move-result-object v3

    .line 111
    .local v3, "svcDir":Lcom/samsung/appcessory/server/SAPServiceDirectory;
    invoke-virtual {v3}, Lcom/samsung/appcessory/server/SAPServiceDirectory;->getServiceList()Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 118
    .end local v3    # "svcDir":Lcom/samsung/appcessory/server/SAPServiceDirectory;
    :goto_0
    new-instance v0, Lcom/samsung/appcessory/session/SAPCapexMsg;

    iget-object v4, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->curSwVer:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->curProtoVer:Ljava/lang/String;

    invoke-direct {v0, v4, v5, v2}, Lcom/samsung/appcessory/session/SAPCapexMsg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 120
    .local v0, "capexMsg":Lcom/samsung/appcessory/session/SAPCapexMsg;
    invoke-virtual {p0}, Lcom/samsung/appcessory/session/SAPCapabilityServer;->getMsgHndlr()Lcom/samsung/appcessory/base/SAPMessageHandler;

    move-result-object v4

    invoke-virtual {v4, p1, v0}, Lcom/samsung/appcessory/base/SAPMessageHandler;->sendCapability(Ljava/lang/Long;Lcom/samsung/appcessory/session/SAPCapexMsg;)Z

    .line 122
    const/4 v4, 0x1

    return v4

    .line 112
    .end local v0    # "capexMsg":Lcom/samsung/appcessory/session/SAPCapexMsg;
    :catch_0
    move-exception v1

    .line 113
    .local v1, "ioe":Ljava/io/IOException;
    const-string v4, "SAP/SAPCapabilityServer/29Dec2014"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/appcessory/utils/config/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setAccSrvList(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 206
    .local p1, "accSrvList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/util/List<Ljava/lang/String;>;>;"
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->accSrvList:Ljava/util/HashMap;

    .line 207
    return-void
.end method

.method public setCapexMode(I)V
    .locals 0
    .param p1, "capexMode"    # I

    .prologue
    .line 214
    iput p1, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->capexMode:I

    .line 215
    return-void
.end method

.method public setMsgHndlr(Lcom/samsung/appcessory/base/SAPMessageHandler;)V
    .locals 0
    .param p1, "_msgHndlr"    # Lcom/samsung/appcessory/base/SAPMessageHandler;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->msgHndlr:Lcom/samsung/appcessory/base/SAPMessageHandler;

    .line 199
    return-void
.end method

.method public setSvcDir(Lcom/samsung/appcessory/server/SAPServiceDirectory;)V
    .locals 0
    .param p1, "svcDir"    # Lcom/samsung/appcessory/server/SAPServiceDirectory;

    .prologue
    .line 296
    iput-object p1, p0, Lcom/samsung/appcessory/session/SAPCapabilityServer;->svcDir:Lcom/samsung/appcessory/server/SAPServiceDirectory;

    .line 297
    return-void
.end method
