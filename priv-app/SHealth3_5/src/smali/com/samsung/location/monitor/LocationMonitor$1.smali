.class Lcom/samsung/location/monitor/LocationMonitor$1;
.super Ljava/lang/Object;
.source "LocationMonitor.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/location/monitor/LocationMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/location/monitor/LocationMonitor;


# direct methods
.method constructor <init>(Lcom/samsung/location/monitor/LocationMonitor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 4
    .param p1, "loc"    # Landroid/location/Location;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$0(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorLog;

    move-result-object v0

    const-string v1, "got from G"

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 253
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setTime(J)V

    .line 254
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$1(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    move-result-object v0

    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->TRACK_ONLY:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-ne v0, v1, :cond_0

    .line 255
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$2(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorResult;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    .line 256
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$2(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorResult;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->latitude:D

    .line 257
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$2(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorResult;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->longitude:D

    .line 258
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$2(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorResult;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    iput v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->accuracy:F

    .line 259
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$2(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorResult;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->altitude:F

    .line 260
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$2(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorResult;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v1

    iput v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    .line 261
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$3(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;
    invoke-static {v1}, Lcom/samsung/location/monitor/LocationMonitor;->access$2(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorCallback;->onDataChanged(Lcom/samsung/location/monitor/LocationMonitorResult;)V

    .line 265
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$4(Lcom/samsung/location/monitor/LocationMonitor;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/location/monitor/LocationMonitor;->access$4(Lcom/samsung/location/monitor/LocationMonitor;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x5

    new-instance v3, Landroid/location/Location;

    invoke-direct {v3, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 268
    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$1(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    move-result-object v0

    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->GPS_ONLY:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$1(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    move-result-object v0

    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->TRACK_ONLY:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-ne v0, v1, :cond_1

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$5(Lcom/samsung/location/monitor/LocationMonitor;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    invoke-virtual {v0}, Lcom/samsung/location/monitor/LocationMonitor;->stopMonitoring()V

    .line 272
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$1;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$3(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/location/monitor/LocationMonitorCallback;->onMonitoringUnavailable()V

    .line 276
    :cond_1
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 279
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/os/Bundle;

    .prologue
    .line 282
    return-void
.end method
