.class Lcom/samsung/location/monitor/EnginePedometer$1;
.super Ljava/lang/Object;
.source "EnginePedometer.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/location/monitor/EnginePedometer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/location/monitor/EnginePedometer;


# direct methods
.method constructor <init>(Lcom/samsung/location/monitor/EnginePedometer;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 98
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 12
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 101
    iget-object v5, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v5}, Landroid/hardware/Sensor;->getType()I

    move-result v5

    if-ne v5, v9, :cond_3

    .line 102
    const/4 v0, 0x0

    .line 103
    .local v0, "samplingtime":I
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mPrevAcclTime:J
    invoke-static {v5}, Lcom/samsung/location/monitor/EnginePedometer;->access$0(Lcom/samsung/location/monitor/EnginePedometer;)J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_4

    .line 104
    const/16 v0, 0x14

    .line 108
    :goto_0
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-static {v5, v6, v7}, Lcom/samsung/location/monitor/EnginePedometer;->access$1(Lcom/samsung/location/monitor/EnginePedometer;J)V

    .line 109
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mAccl:[F
    invoke-static {v5}, Lcom/samsung/location/monitor/EnginePedometer;->access$2(Lcom/samsung/location/monitor/EnginePedometer;)[F

    move-result-object v5

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v11

    aput v6, v5, v11

    .line 110
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mAccl:[F
    invoke-static {v5}, Lcom/samsung/location/monitor/EnginePedometer;->access$2(Lcom/samsung/location/monitor/EnginePedometer;)[F

    move-result-object v5

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v9

    aput v6, v5, v9

    .line 111
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mAccl:[F
    invoke-static {v5}, Lcom/samsung/location/monitor/EnginePedometer;->access$2(Lcom/samsung/location/monitor/EnginePedometer;)[F

    move-result-object v5

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v10

    aput v6, v5, v10

    .line 112
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->pedometerlib:Lcom/sec/pedometer/Pedometer;
    invoke-static {v5}, Lcom/samsung/location/monitor/EnginePedometer;->access$3(Lcom/samsung/location/monitor/EnginePedometer;)Lcom/sec/pedometer/Pedometer;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mAccl:[F
    invoke-static {v6}, Lcom/samsung/location/monitor/EnginePedometer;->access$2(Lcom/samsung/location/monitor/EnginePedometer;)[F

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mBarometerData:F
    invoke-static {v7}, Lcom/samsung/location/monitor/EnginePedometer;->access$4(Lcom/samsung/location/monitor/EnginePedometer;)F

    move-result v7

    invoke-virtual {v5, v6, v7, v0}, Lcom/sec/pedometer/Pedometer;->DetectStep([FFI)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 113
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->pedometerlib:Lcom/sec/pedometer/Pedometer;
    invoke-static {v5}, Lcom/samsung/location/monitor/EnginePedometer;->access$3(Lcom/samsung/location/monitor/EnginePedometer;)Lcom/sec/pedometer/Pedometer;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    iget-object v6, v6, Lcom/samsung/location/monitor/EnginePedometer;->mStepinfo:Lcom/sec/pedometer/StepInfo;

    invoke-virtual {v5, v6}, Lcom/sec/pedometer/Pedometer;->GetStepInfo(Lcom/sec/pedometer/StepInfo;)V

    .line 114
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    iget-object v5, v5, Lcom/samsung/location/monitor/EnginePedometer;->mStepinfo:Lcom/sec/pedometer/StepInfo;

    invoke-virtual {v5}, Lcom/sec/pedometer/StepInfo;->getStepCount()J

    move-result-wide v1

    .line 115
    .local v1, "tempCount":J
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    iget-object v5, v5, Lcom/samsung/location/monitor/EnginePedometer;->mStepinfo:Lcom/sec/pedometer/StepInfo;

    invoke-virtual {v5}, Lcom/sec/pedometer/StepInfo;->getTotalStepLength()F

    move-result v3

    .line 116
    .local v3, "tempDist":F
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    iget-object v5, v5, Lcom/samsung/location/monitor/EnginePedometer;->mStepinfo:Lcom/sec/pedometer/StepInfo;

    invoke-virtual {v5}, Lcom/sec/pedometer/StepInfo;->getStepSpeed()F

    move-result v4

    .line 117
    .local v4, "tempSpeed":F
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mLastCount:J
    invoke-static {v5}, Lcom/samsung/location/monitor/EnginePedometer;->access$5(Lcom/samsung/location/monitor/EnginePedometer;)J

    move-result-wide v5

    cmp-long v5, v1, v5

    if-gez v5, :cond_0

    .line 118
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mCallback:Lcom/samsung/location/monitor/EnginePedometer$Callback;
    invoke-static {v5}, Lcom/samsung/location/monitor/EnginePedometer;->access$6(Lcom/samsung/location/monitor/EnginePedometer;)Lcom/samsung/location/monitor/EnginePedometer$Callback;

    move-result-object v5

    invoke-interface {v5, v9}, Lcom/samsung/location/monitor/EnginePedometer$Callback;->onDataInvalid(I)V

    .line 120
    :cond_0
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mLastDistance:F
    invoke-static {v5}, Lcom/samsung/location/monitor/EnginePedometer;->access$7(Lcom/samsung/location/monitor/EnginePedometer;)F

    move-result v5

    cmpg-float v5, v3, v5

    if-gez v5, :cond_1

    .line 121
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mCallback:Lcom/samsung/location/monitor/EnginePedometer$Callback;
    invoke-static {v5}, Lcom/samsung/location/monitor/EnginePedometer;->access$6(Lcom/samsung/location/monitor/EnginePedometer;)Lcom/samsung/location/monitor/EnginePedometer$Callback;

    move-result-object v5

    invoke-interface {v5, v10}, Lcom/samsung/location/monitor/EnginePedometer$Callback;->onDataInvalid(I)V

    .line 123
    :cond_1
    const/4 v5, 0x0

    cmpg-float v5, v4, v5

    if-gez v5, :cond_2

    .line 124
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mCallback:Lcom/samsung/location/monitor/EnginePedometer$Callback;
    invoke-static {v5}, Lcom/samsung/location/monitor/EnginePedometer;->access$6(Lcom/samsung/location/monitor/EnginePedometer;)Lcom/samsung/location/monitor/EnginePedometer$Callback;

    move-result-object v5

    const/4 v6, 0x4

    invoke-interface {v5, v6}, Lcom/samsung/location/monitor/EnginePedometer$Callback;->onDataInvalid(I)V

    .line 126
    :cond_2
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/samsung/location/monitor/EnginePedometer;->access$8(Lcom/samsung/location/monitor/EnginePedometer;J)V

    .line 127
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    invoke-static {v5, v1, v2}, Lcom/samsung/location/monitor/EnginePedometer;->access$9(Lcom/samsung/location/monitor/EnginePedometer;J)V

    .line 128
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    invoke-static {v5, v3}, Lcom/samsung/location/monitor/EnginePedometer;->access$10(Lcom/samsung/location/monitor/EnginePedometer;F)V

    .line 129
    iget-object v5, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    invoke-static {v5, v4}, Lcom/samsung/location/monitor/EnginePedometer;->access$11(Lcom/samsung/location/monitor/EnginePedometer;F)V

    .line 132
    .end local v0    # "samplingtime":I
    .end local v1    # "tempCount":J
    .end local v3    # "tempDist":F
    .end local v4    # "tempSpeed":F
    :cond_3
    return-void

    .line 106
    .restart local v0    # "samplingtime":I
    :cond_4
    iget-wide v5, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-object v7, p0, Lcom/samsung/location/monitor/EnginePedometer$1;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mPrevAcclTime:J
    invoke-static {v7}, Lcom/samsung/location/monitor/EnginePedometer;->access$0(Lcom/samsung/location/monitor/EnginePedometer;)J

    move-result-wide v7

    sub-long/2addr v5, v7

    const-wide/32 v7, 0xf4240

    div-long/2addr v5, v7

    long-to-int v0, v5

    goto/16 :goto_0
.end method
