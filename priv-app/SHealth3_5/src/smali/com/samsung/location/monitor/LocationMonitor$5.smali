.class Lcom/samsung/location/monitor/LocationMonitor$5;
.super Landroid/os/Handler;
.source "LocationMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/location/monitor/LocationMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/location/monitor/LocationMonitor;


# direct methods
.method constructor <init>(Lcom/samsung/location/monitor/LocationMonitor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    .line 337
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v2, 0x8

    .line 340
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 341
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    # invokes: Lcom/samsung/location/monitor/LocationMonitor;->handleStart(Landroid/os/Bundle;)V
    invoke-static {v1, v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$11(Lcom/samsung/location/monitor/LocationMonitor;Landroid/os/Bundle;)V

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 343
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # invokes: Lcom/samsung/location/monitor/LocationMonitor;->handleStop()V
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$12(Lcom/samsung/location/monitor/LocationMonitor;)V

    goto :goto_0

    .line 344
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 345
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # invokes: Lcom/samsung/location/monitor/LocationMonitor;->handlePause()V
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$13(Lcom/samsung/location/monitor/LocationMonitor;)V

    goto :goto_0

    .line 346
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 347
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # invokes: Lcom/samsung/location/monitor/LocationMonitor;->handleResume()V
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$14(Lcom/samsung/location/monitor/LocationMonitor;)V

    goto :goto_0

    .line 348
    :cond_4
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_5

    .line 349
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$1(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    move-result-object v0

    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->NONE:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-eq v0, v1, :cond_0

    .line 350
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    # invokes: Lcom/samsung/location/monitor/LocationMonitor;->handleGPSData(Landroid/location/Location;)V
    invoke-static {v1, v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$15(Lcom/samsung/location/monitor/LocationMonitor;Landroid/location/Location;)V

    goto :goto_0

    .line 352
    :cond_5
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_6

    .line 353
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$1(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    move-result-object v0

    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->NONE:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-eq v0, v1, :cond_0

    .line 354
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    # invokes: Lcom/samsung/location/monitor/LocationMonitor;->handleBarometerData(Landroid/os/Bundle;)V
    invoke-static {v1, v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$16(Lcom/samsung/location/monitor/LocationMonitor;Landroid/os/Bundle;)V

    goto :goto_0

    .line 356
    :cond_6
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_7

    .line 357
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$1(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    move-result-object v0

    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->NONE:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-eq v0, v1, :cond_0

    .line 358
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/location/monitor/LocationMonitorResult;

    # invokes: Lcom/samsung/location/monitor/LocationMonitor;->handlePedometerData(Lcom/samsung/location/monitor/LocationMonitorResult;)V
    invoke-static {v1, v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$17(Lcom/samsung/location/monitor/LocationMonitor;Lcom/samsung/location/monitor/LocationMonitorResult;)V

    goto :goto_0

    .line 360
    :cond_7
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_0

    .line 361
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$1(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    move-result-object v0

    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->NONE:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-eq v0, v1, :cond_0

    .line 362
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$4(Lcom/samsung/location/monitor/LocationMonitor;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/location/monitor/LocationMonitor;->access$4(Lcom/samsung/location/monitor/LocationMonitor;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 363
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$5;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # invokes: Lcom/samsung/location/monitor/LocationMonitor;->generateCallback()V
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$18(Lcom/samsung/location/monitor/LocationMonitor;)V

    goto/16 :goto_0
.end method
