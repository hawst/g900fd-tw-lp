.class public Lcom/samsung/location/monitor/LocationMonitor;
.super Ljava/lang/Object;
.source "LocationMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;
    }
.end annotation


# static fields
.field private static final ALTITUDE_MAX_THRESHOLD:I = 0x2710

.field private static final ALTITUDE_MIN_THRESHOLD:I = -0x3e8

.field private static final DISTANCE_THRESHOLD:I = 0xf

.field private static final GPS_SHADE_TIMEOUT:J = 0xbb8L

.field private static final GPS_WINDOW:I = 0x4

.field private static final INCLINATION_THRESHOLD:F = 0.04f

.field private static final MAX_SPEED_SMOOTHING_WINDOW:I = 0xf

.field private static final MET_CYCLE:F = 3.41E-4f

.field private static final MET_DECLINE_WEIGHT_CYCLE:F = 0.5f

.field private static final MET_DECLINE_WEIGHT_FOOT:F = 0.84f

.field private static final MET_DECLINE_WEIGHT_HIKING:F = 4.593E-4f

.field private static final MET_HIKING:F = 9.11E-4f

.field private static final MET_INCLINE_WEIGHT_CYCLE:F = 2.0f

.field private static final MET_INCLINE_WEIGHT_FOOT:F = 1.16f

.field private static final MET_INCLINE_WEIGHT_HIKING:F = 0.0039524f

.field private static final MET_RUN:F = 0.001041f

.field private static final MET_WALK:F = 6.89E-4f

.field private static final MSG_BAROMETER_DATA:I = 0x6

.field private static final MSG_CALLBACK_GENERATE:I = 0x8

.field private static final MSG_GPS_DATA:I = 0x5

.field private static final MSG_PAUSE:I = 0x3

.field private static final MSG_PEDOMETER_DATA:I = 0x7

.field private static final MSG_RESUME:I = 0x4

.field private static final MSG_START:I = 0x1

.field private static final MSG_STOP:I = 0x2

.field private static final NO_MOVE_THRESHOLD:I = 0x3

.field private static final PRESSURE_E_CON:I = 0x7

.field private static final PRESSURE_E_PARSE:I = 0x8

.field private static final PRESSURE_E_PSERVICE:I = 0x4

.field private static final PRESSURE_E_TIMEOUT:I = 0x6

.field private static final PRESSURE_E_UNKNOWN:I = 0xa

.field private static final PRESSURE_E_URL:I = 0x5

.field private static final PRESSURE_E_VALUE:I = 0x9

.field private static final PRESSURE_NONE:I = 0x1

.field private static final PRESSURE_SUCCESS:I = 0x3

.field private static final PRESSURE_TRYING:I = 0x2

.field public static final PSERVICE_ACCUWEATHER:I = 0xd

.field public static final PSERVICE_NONE:I = 0xb

.field public static final PSERVICE_WEATHERNEWS:I = 0xc

.field private static final SPEED_SMOOTHING_WINDOW:I = 0x3

.field private static final SPEED_THRESHOLD_CYCLE:F = 25.0f

.field private static final SPEED_THRESHOLD_HIKING:F = 2.5f

.field private static final SPEED_THRESHOLD_RUN:F = 7.5f

.field private static final SPEED_THRESHOLD_WALK:F = 2.5f

.field public static final TYPE_CYCLE:I = 0x3

.field public static final TYPE_HIKING:I = 0x4

.field public static final TYPE_RUN:I = 0x2

.field public static final TYPE_WALK:I = 0x1


# instance fields
.field private ep:Lcom/samsung/location/monitor/EnginePedometer;

.field private isBarometerSupported:Z

.field private isGpsDisabled:Z

.field private isMonitoring:Z

.field private isMoved:Z

.field private isPaused:Z

.field private isPedometerSupported:Z

.field private isResumeMonitoringCalled:Z

.field private isStartMonitoringCalled:Z

.field private isWearableConnected:Z

.field private lm:Landroid/location/LocationManager;

.field private lml:Lcom/samsung/location/monitor/LocationMonitorLog;

.field private mAverageCount:I

.field private mAverageDecline:F

.field private mAverageIncline:F

.field private mAverageSpeed:F

.field private mBasePressure:F

.field private mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

.field private mCallbackPedometer:Lcom/samsung/location/monitor/EnginePedometer$Callback;

.field private mCurrentLoc:Landroid/location/Location;

.field private mCurrentPressure:F

.field private mDeclineDistance:F

.field private mDeclineTime:J

.field private mFlatDistance:F

.field private mFlatTime:J

.field private mGpsIndex:I

.field private mGpsSpeed:F

.field private mHandler:Landroid/os/Handler;

.field private mInclineDistance:F

.field private mInclineTime:J

.field private mLastCallbackTime:J

.field private mLastLocToCalc:Landroid/location/Location;

.field private mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

.field private mLocListener:Landroid/location/LocationListener;

.field private mMaxAltitude:F

.field private mMaxIndex:I

.field private mMaxSpeed:F

.field private mMaxSpeedHistory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mMinAltitude:F

.field private mPServiceType:I

.field private mPedoValidity:I

.field private mPedometerSpeed:F

.field private mPressureCallback:Lcom/samsung/location/monitor/PressureTask$Callback;

.field private mPressureStatus:I

.field private mRelativeAltitudeHistory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mRelativeTimeHistory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mSectionDistance:F

.field private mSectionInitialBaroAlti:F

.field private mSectionInitialTime:J

.field private mSensorListener:Landroid/hardware/SensorEventListener;

.field private mSpeedHistory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

.field private mTotalDistance:F

.field private mType:I

.field private mUserHeight:F

.field private mUserWeight:F

.field private noMoveCount:I

.field private pt:Lcom/samsung/location/monitor/PressureTask;

.field private sm:Landroid/hardware/SensorManager;

.field private wl:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/location/monitor/LocationMonitorCallback;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callback"    # Lcom/samsung/location/monitor/LocationMonitorCallback;

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    .line 79
    const/16 v0, 0xb

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPServiceType:I

    .line 80
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mUserWeight:F

    .line 81
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mUserHeight:F

    .line 82
    iput-boolean v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->isWearableConnected:Z

    .line 85
    sget-object v0, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->NONE:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    .line 86
    iput v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    .line 88
    iput-boolean v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    .line 89
    iput-boolean v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPaused:Z

    .line 90
    iput-boolean v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->isStartMonitoringCalled:Z

    .line 91
    iput-boolean v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->isResumeMonitoringCalled:Z

    .line 92
    iput-boolean v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->isGpsDisabled:Z

    .line 93
    iput-boolean v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMoved:Z

    .line 94
    iput v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mGpsIndex:I

    .line 95
    iput v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxIndex:I

    .line 96
    iput v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->noMoveCount:I

    .line 97
    iput v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageCount:I

    .line 98
    iput v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPedoValidity:I

    .line 99
    iput-wide v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastCallbackTime:J

    .line 100
    iput-wide v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialTime:J

    .line 101
    iput-wide v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineTime:J

    .line 102
    iput-wide v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineTime:J

    .line 103
    iput-wide v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    .line 104
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mGpsSpeed:F

    .line 105
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPedometerSpeed:F

    .line 106
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mBasePressure:F

    .line 107
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentPressure:F

    .line 108
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialBaroAlti:F

    .line 109
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    .line 110
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    .line 111
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineDistance:F

    .line 112
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineDistance:F

    .line 113
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    .line 114
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxAltitude:F

    .line 115
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMinAltitude:F

    .line 116
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeed:F

    .line 117
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageSpeed:F

    .line 118
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageIncline:F

    .line 119
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageDecline:F

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeTimeHistory:Ljava/util/ArrayList;

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeAltitudeHistory:Ljava/util/ArrayList;

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSpeedHistory:Ljava/util/ArrayList;

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeedHistory:Ljava/util/ArrayList;

    .line 124
    iput-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    .line 125
    iput-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    .line 126
    new-instance v0, Lcom/samsung/location/monitor/LocationMonitorResult;

    invoke-direct {v0}, Lcom/samsung/location/monitor/LocationMonitorResult;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    .line 249
    new-instance v0, Lcom/samsung/location/monitor/LocationMonitor$1;

    invoke-direct {v0, p0}, Lcom/samsung/location/monitor/LocationMonitor$1;-><init>(Lcom/samsung/location/monitor/LocationMonitor;)V

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLocListener:Landroid/location/LocationListener;

    .line 285
    new-instance v0, Lcom/samsung/location/monitor/LocationMonitor$2;

    invoke-direct {v0, p0}, Lcom/samsung/location/monitor/LocationMonitor$2;-><init>(Lcom/samsung/location/monitor/LocationMonitor;)V

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSensorListener:Landroid/hardware/SensorEventListener;

    .line 299
    new-instance v0, Lcom/samsung/location/monitor/LocationMonitor$3;

    invoke-direct {v0, p0}, Lcom/samsung/location/monitor/LocationMonitor$3;-><init>(Lcom/samsung/location/monitor/LocationMonitor;)V

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallbackPedometer:Lcom/samsung/location/monitor/EnginePedometer$Callback;

    .line 311
    new-instance v0, Lcom/samsung/location/monitor/LocationMonitor$4;

    invoke-direct {v0, p0}, Lcom/samsung/location/monitor/LocationMonitor$4;-><init>(Lcom/samsung/location/monitor/LocationMonitor;)V

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureCallback:Lcom/samsung/location/monitor/PressureTask$Callback;

    .line 337
    new-instance v0, Lcom/samsung/location/monitor/LocationMonitor$5;

    invoke-direct {v0, p0}, Lcom/samsung/location/monitor/LocationMonitor$5;-><init>(Lcom/samsung/location/monitor/LocationMonitor;)V

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    .line 129
    iput-object p2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    .line 130
    invoke-static {}, Lcom/samsung/location/monitor/LocationMonitorLog;->getInstance()Lcom/samsung/location/monitor/LocationMonitorLog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    .line 131
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lm:Landroid/location/LocationManager;

    .line 132
    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->sm:Landroid/hardware/SensorManager;

    .line 133
    const-string/jumbo v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v3, "EnginePedometer"

    invoke-virtual {v0, v1, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->wl:Landroid/os/PowerManager$WakeLock;

    .line 134
    new-instance v0, Lcom/samsung/location/monitor/EnginePedometer;

    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallbackPedometer:Lcom/samsung/location/monitor/EnginePedometer$Callback;

    invoke-direct {v0, p1, v3}, Lcom/samsung/location/monitor/EnginePedometer;-><init>(Landroid/content/Context;Lcom/samsung/location/monitor/EnginePedometer$Callback;)V

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->ep:Lcom/samsung/location/monitor/EnginePedometer;

    .line 135
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->sm:Landroid/hardware/SensorManager;

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isBarometerSupported:Z

    .line 136
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->ep:Lcom/samsung/location/monitor/EnginePedometer;

    invoke-virtual {v0}, Lcom/samsung/location/monitor/EnginePedometer;->isPedometerSupported()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPedometerSupported:Z

    .line 137
    return-void

    :cond_0
    move v0, v2

    .line 135
    goto :goto_0
.end method

.method private _startMonitoring(IZFFILcom/samsung/location/monitor/LocationMonitorResult;)V
    .locals 8
    .param p1, "type"    # I
    .param p2, "wearableConnected"    # Z
    .param p3, "userHeight"    # F
    .param p4, "userWeight"    # F
    .param p5, "pserviceType"    # I
    .param p6, "lastResult"    # Lcom/samsung/location/monitor/LocationMonitorResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 151
    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/location/monitor/LocationMonitorLog;->initialize(I)V

    .line 152
    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "start request from "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    invoke-virtual {v7}, Ljava/lang/Object;->hashCode()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 153
    const/4 v5, 0x1

    if-eq p1, v5, :cond_0

    const/4 v5, 0x2

    if-eq p1, v5, :cond_0

    const/4 v5, 0x3

    if-eq p1, v5, :cond_0

    const/4 v5, 0x4

    if-eq p1, v5, :cond_0

    .line 154
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "type parameter is incorrect!"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 156
    :cond_0
    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->lm:Landroid/location/LocationManager;

    const-string v6, "gps"

    invoke-virtual {v5, v6}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v5, 0x3

    if-eq p1, v5, :cond_1

    const/4 v5, 0x4

    if-ne p1, v5, :cond_2

    .line 157
    :cond_1
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    const-string v6, "GPS is off"

    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 159
    :cond_2
    const/16 v5, 0xc

    if-eq p5, v5, :cond_3

    const/16 v5, 0xd

    if-eq p5, v5, :cond_3

    const/16 v5, 0xb

    if-eq p5, v5, :cond_3

    .line 160
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "pserviceType is incorrect!"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 162
    :cond_3
    iget-boolean v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPaused:Z

    if-eqz v5, :cond_4

    .line 163
    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string v6, "already started & paused"

    invoke-virtual {v5, v6}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 201
    :goto_0
    return-void

    .line 166
    :cond_4
    iget-boolean v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    if-eqz v5, :cond_5

    .line 167
    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string v6, "already started"

    invoke-virtual {v5, v6}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    goto :goto_0

    .line 171
    :cond_5
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    .line 172
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPaused:Z

    .line 173
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 174
    .local v2, "param":Landroid/os/Bundle;
    const-string/jumbo v5, "type"

    invoke-virtual {v2, v5, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 175
    const-string/jumbo v5, "pType"

    invoke-virtual {v2, v5, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 176
    const-string/jumbo v5, "wc"

    invoke-virtual {v2, v5, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 177
    const-string/jumbo v5, "wei"

    invoke-virtual {v2, v5, p4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 178
    const-string v5, "hei"

    invoke-virtual {v2, v5, p3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 180
    if-nez p6, :cond_6

    .line 181
    const-string/jumbo v5, "restart"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 200
    :goto_1
    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x1

    invoke-virtual {v6, v7, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 183
    :cond_6
    const-string/jumbo v5, "restart"

    const/4 v6, 0x1

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 184
    const/4 v5, 0x4

    new-array v1, v5, [F

    const/4 v5, 0x0

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    aput v6, v1, v5

    const/4 v5, 0x1

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    aput v6, v1, v5

    const/4 v5, 0x2

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    aput v6, v1, v5

    const/4 v5, 0x3

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    aput v6, v1, v5

    .line 185
    .local v1, "dist":[F
    const-string v5, "dist"

    invoke-virtual {v2, v5, v1}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 186
    const/4 v5, 0x3

    new-array v4, v5, [J

    const/4 v5, 0x0

    iget-wide v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineTime:J

    aput-wide v6, v4, v5

    const/4 v5, 0x1

    iget-wide v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->declineTime:J

    aput-wide v6, v4, v5

    const/4 v5, 0x2

    iget-wide v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    aput-wide v6, v4, v5

    .line 187
    .local v4, "time":[J
    const-string/jumbo v5, "time"

    invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 188
    const/4 v5, 0x2

    new-array v0, v5, [F

    const/4 v5, 0x0

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->maxAltitude:F

    aput v6, v0, v5

    const/4 v5, 0x1

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->minAltitude:F

    aput v6, v0, v5

    .line 189
    .local v0, "alti":[F
    const-string v5, "alti"

    invoke-virtual {v2, v5, v0}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 190
    const/4 v5, 0x2

    new-array v3, v5, [F

    const/4 v5, 0x0

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->maxSpeed:F

    aput v6, v3, v5

    const/4 v5, 0x1

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->averageSpeed:F

    aput v6, v3, v5

    .line 191
    .local v3, "speed":[F
    const-string/jumbo v5, "speed"

    invoke-virtual {v2, v5, v3}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 192
    const-string/jumbo v5, "step"

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->stepCount:I

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 193
    const-string/jumbo v5, "rd1"

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData1:I

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    const-string/jumbo v5, "rd2"

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData2:I

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 195
    const-string/jumbo v5, "rd3"

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData3:F

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 196
    const-string/jumbo v5, "rd4"

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData4:F

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 197
    const-string/jumbo v5, "rd5"

    iget v6, p6, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData5:F

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    goto/16 :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorLog;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/location/monitor/LocationMonitor;I)V
    .locals 0

    .prologue
    .line 86
    iput p1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/location/monitor/LocationMonitor;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 369
    invoke-direct {p0, p1}, Lcom/samsung/location/monitor/LocationMonitor;->handleStart(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/location/monitor/LocationMonitor;)V
    .locals 0

    .prologue
    .line 430
    invoke-direct {p0}, Lcom/samsung/location/monitor/LocationMonitor;->handleStop()V

    return-void
.end method

.method static synthetic access$13(Lcom/samsung/location/monitor/LocationMonitor;)V
    .locals 0

    .prologue
    .line 436
    invoke-direct {p0}, Lcom/samsung/location/monitor/LocationMonitor;->handlePause()V

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/location/monitor/LocationMonitor;)V
    .locals 0

    .prologue
    .line 453
    invoke-direct {p0}, Lcom/samsung/location/monitor/LocationMonitor;->handleResume()V

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/location/monitor/LocationMonitor;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 521
    invoke-direct {p0, p1}, Lcom/samsung/location/monitor/LocationMonitor;->handleGPSData(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$16(Lcom/samsung/location/monitor/LocationMonitor;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 588
    invoke-direct {p0, p1}, Lcom/samsung/location/monitor/LocationMonitor;->handleBarometerData(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/location/monitor/LocationMonitor;Lcom/samsung/location/monitor/LocationMonitorResult;)V
    .locals 0

    .prologue
    .line 596
    invoke-direct {p0, p1}, Lcom/samsung/location/monitor/LocationMonitor;->handlePedometerData(Lcom/samsung/location/monitor/LocationMonitorResult;)V

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/location/monitor/LocationMonitor;)V
    .locals 0

    .prologue
    .line 635
    invoke-direct {p0}, Lcom/samsung/location/monitor/LocationMonitor;->generateCallback()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorResult;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorCallback;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/location/monitor/LocationMonitor;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/location/monitor/LocationMonitor;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/location/monitor/LocationMonitor;)J
    .locals 2

    .prologue
    .line 100
    iget-wide v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialTime:J

    return-wide v0
.end method

.method static synthetic access$7(Lcom/samsung/location/monitor/LocationMonitor;)I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPedoValidity:I

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/location/monitor/LocationMonitor;I)V
    .locals 0

    .prologue
    .line 98
    iput p1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPedoValidity:I

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/location/monitor/LocationMonitor;F)V
    .locals 0

    .prologue
    .line 106
    iput p1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mBasePressure:F

    return-void
.end method

.method private calcMaxSpeed()F
    .locals 5

    .prologue
    const/16 v4, 0xf

    .line 887
    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeedHistory:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v3, v3, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 888
    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeedHistory:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v4, :cond_0

    .line 889
    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeedHistory:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 891
    :cond_0
    const/4 v1, 0x0

    .line 892
    .local v1, "smoothedMaxSpeed":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeedHistory:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 895
    iget v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxIndex:I

    .line 896
    iget v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxIndex:I

    if-ge v2, v4, :cond_2

    .line 897
    const/4 v1, 0x0

    .line 901
    :goto_1
    return v1

    .line 893
    :cond_1
    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeedHistory:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    add-float/2addr v1, v2

    .line 892
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 899
    :cond_2
    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeedHistory:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    goto :goto_1
.end method

.method private calculate3D(J)F
    .locals 9
    .param p1, "currentTime"    # J

    .prologue
    .line 719
    iget-wide v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialTime:J

    sub-long v4, p1, v6

    .line 720
    .local v4, "timeDiff":J
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeTimeHistory:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeAltitudeHistory:Ljava/util/ArrayList;

    invoke-direct {p0, v6, v7}, Lcom/samsung/location/monitor/LocationMonitor;->getInclinationTY(Ljava/util/ArrayList;Ljava/util/ArrayList;)F

    move-result v1

    .line 721
    .local v1, "inclinationTY":F
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeTimeHistory:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeTimeHistory:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    mul-float v3, v1, v6

    .line 722
    .local v3, "sectionAltitude":F
    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    .line 723
    .local v0, "distance3D":F
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_0

    .line 724
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    iget v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    mul-float/2addr v6, v7

    mul-float v7, v3, v3

    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v0, v6

    .line 725
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    iget v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    sub-float v7, v0, v7

    add-float/2addr v6, v7

    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    .line 727
    :cond_0
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    div-float v2, v3, v6

    .line 728
    .local v2, "inclinationXY":F
    const v6, 0x3d23d70a    # 0.04f

    cmpl-float v6, v2, v6

    if-ltz v6, :cond_1

    .line 729
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageIncline:F

    iget-wide v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineTime:J

    long-to-float v7, v7

    mul-float/2addr v6, v7

    long-to-float v7, v4

    mul-float/2addr v7, v2

    add-float/2addr v6, v7

    iget-wide v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineTime:J

    add-long/2addr v7, v4

    long-to-float v7, v7

    div-float/2addr v6, v7

    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageIncline:F

    .line 730
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineDistance:F

    add-float/2addr v6, v0

    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineDistance:F

    .line 731
    iget-wide v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineTime:J

    add-long/2addr v6, v4

    iput-wide v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineTime:J

    .line 740
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/location/monitor/LocationMonitor;->initializeSectionParameter(J)V

    .line 741
    return v0

    .line 732
    :cond_1
    const v6, -0x42dc28f6    # -0.04f

    cmpg-float v6, v2, v6

    if-gtz v6, :cond_2

    .line 733
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageDecline:F

    iget-wide v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineTime:J

    long-to-float v7, v7

    mul-float/2addr v6, v7

    long-to-float v7, v4

    mul-float/2addr v7, v2

    add-float/2addr v6, v7

    iget-wide v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineTime:J

    add-long/2addr v7, v4

    long-to-float v7, v7

    div-float/2addr v6, v7

    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageDecline:F

    .line 734
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineDistance:F

    add-float/2addr v6, v0

    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineDistance:F

    .line 735
    iget-wide v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineTime:J

    add-long/2addr v6, v4

    iput-wide v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineTime:J

    goto :goto_0

    .line 737
    :cond_2
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    add-float/2addr v6, v0

    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    .line 738
    iget-wide v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    add-long/2addr v6, v4

    iput-wide v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    goto :goto_0
.end method

.method private calculateExtraData()V
    .locals 15

    .prologue
    const/high16 v14, 0x447a0000    # 1000.0f

    const/high16 v13, 0x42700000    # 60.0f

    const/4 v12, 0x3

    const v11, 0x3a349e02    # 6.89E-4f

    const/4 v10, 0x0

    .line 766
    iget-boolean v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->isBarometerSupported:Z

    if-eqz v8, :cond_4

    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentPressure:F

    cmpl-float v8, v8, v10

    if-eqz v8, :cond_4

    .line 767
    const/4 v0, 0x0

    .line 768
    .local v0, "currentAltitude":F
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    if-ne v8, v12, :cond_a

    .line 769
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mBasePressure:F

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentPressure:F

    invoke-static {v8, v9}, Landroid/hardware/SensorManager;->getAltitude(FF)F

    move-result v0

    .line 773
    :cond_0
    :goto_0
    const v8, 0x461c4000    # 10000.0f

    cmpl-float v8, v0, v8

    if-lez v8, :cond_1

    .line 774
    const v0, 0x461c4000    # 10000.0f

    .line 776
    :cond_1
    const/high16 v8, -0x3b860000    # -1000.0f

    cmpg-float v8, v0, v8

    if-gez v8, :cond_2

    .line 777
    const/high16 v0, -0x3b860000    # -1000.0f

    .line 779
    :cond_2
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iput v0, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->altitude:F

    .line 780
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxAltitude:F

    cmpl-float v8, v8, v10

    if-nez v8, :cond_b

    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMinAltitude:F

    cmpl-float v8, v8, v10

    if-nez v8, :cond_b

    .line 781
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxAltitude:F

    .line 782
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxAltitude:F

    iput v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMinAltitude:F

    .line 791
    :cond_3
    :goto_1
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxAltitude:F

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->maxAltitude:F

    .line 792
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMinAltitude:F

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->minAltitude:F

    .line 796
    .end local v0    # "currentAltitude":F
    :cond_4
    const/high16 v5, 0x40200000    # 2.5f

    .line 797
    .local v5, "limitSpeed":F
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_d

    .line 798
    const/high16 v5, 0x40f00000    # 7.5f

    .line 804
    :cond_5
    :goto_2
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v8, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    cmpl-float v8, v8, v5

    if-lez v8, :cond_6

    .line 805
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iput v5, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    .line 807
    :cond_6
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSpeedHistory:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 808
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSpeedHistory:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-le v8, v12, :cond_7

    .line 809
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSpeedHistory:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 811
    :cond_7
    const/4 v7, 0x0

    .line 812
    .local v7, "smoothedSpeed":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSpeedHistory:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v3, v8, :cond_f

    .line 815
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSpeedHistory:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    .line 816
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iput v7, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    .line 819
    invoke-direct {p0}, Lcom/samsung/location/monitor/LocationMonitor;->calcMaxSpeed()F

    move-result v6

    .line 820
    .local v6, "smoothMaxSpeed":F
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeed:F

    cmpg-float v8, v8, v6

    if-gez v8, :cond_8

    .line 821
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeed:F

    .line 823
    :cond_8
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeed:F

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->maxSpeed:F

    .line 826
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageCount:I

    .line 827
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageCount:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_10

    .line 828
    iput v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageSpeed:F

    .line 832
    :goto_4
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageSpeed:F

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->averageSpeed:F

    .line 835
    cmpl-float v8, v7, v10

    if-nez v8, :cond_11

    .line 836
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iput v10, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->pace:F

    .line 840
    :goto_5
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeed:F

    cmpl-float v8, v8, v10

    if-nez v8, :cond_12

    .line 841
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iput v10, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->maxPace:F

    .line 845
    :goto_6
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v8, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->averageSpeed:F

    cmpl-float v8, v8, v10

    if-nez v8, :cond_13

    .line 846
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iput v10, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->averagePace:F

    .line 852
    :goto_7
    const/4 v4, 0x0

    .line 853
    .local v4, "inclineCal":F
    const/4 v2, 0x0

    .line 854
    .local v2, "flatCal":F
    const/4 v1, 0x0

    .line 855
    .local v1, "declineCal":F
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_14

    .line 856
    const v8, 0x3a518416

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    mul-float v4, v8, v9

    .line 857
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v8, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    sub-float/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    sub-float/2addr v8, v9

    mul-float v2, v11, v8

    .line 858
    const v8, 0x3a17b7ed    # 5.7876E-4f

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    mul-float v1, v8, v9

    .line 876
    :cond_9
    :goto_8
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    add-float v9, v4, v2

    add-float/2addr v9, v1

    iget v10, p0, Lcom/samsung/location/monitor/LocationMonitor;->mUserWeight:F

    mul-float/2addr v9, v10

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->consumedCalorie:F

    .line 879
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageCount:I

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData1:I

    .line 880
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData2:I

    .line 881
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageIncline:F

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData3:F

    .line 882
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageDecline:F

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData4:F

    .line 883
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mBasePressure:F

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData5:F

    .line 884
    return-void

    .line 770
    .end local v1    # "declineCal":F
    .end local v2    # "flatCal":F
    .end local v3    # "i":I
    .end local v4    # "inclineCal":F
    .end local v5    # "limitSpeed":F
    .end local v6    # "smoothMaxSpeed":F
    .end local v7    # "smoothedSpeed":F
    .restart local v0    # "currentAltitude":F
    :cond_a
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    const/4 v9, 0x1

    if-eq v8, v9, :cond_0

    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    const/4 v9, 0x2

    if-eq v8, v9, :cond_0

    .line 771
    const v8, 0x447d5000    # 1013.25f

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentPressure:F

    invoke-static {v8, v9}, Landroid/hardware/SensorManager;->getAltitude(FF)F

    move-result v0

    goto/16 :goto_0

    .line 784
    :cond_b
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxAltitude:F

    cmpg-float v8, v8, v0

    if-gez v8, :cond_c

    .line 785
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxAltitude:F

    .line 787
    :cond_c
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMinAltitude:F

    cmpl-float v8, v8, v0

    if-lez v8, :cond_3

    .line 788
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMinAltitude:F

    goto/16 :goto_1

    .line 799
    .end local v0    # "currentAltitude":F
    .restart local v5    # "limitSpeed":F
    :cond_d
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    if-ne v8, v12, :cond_e

    .line 800
    const/high16 v5, 0x41c80000    # 25.0f

    .line 801
    goto/16 :goto_2

    :cond_e
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v9, 0x4

    if-ne v8, v9, :cond_5

    .line 802
    const/high16 v5, 0x40200000    # 2.5f

    goto/16 :goto_2

    .line 813
    .restart local v3    # "i":I
    .restart local v7    # "smoothedSpeed":F
    :cond_f
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSpeedHistory:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    add-float/2addr v7, v8

    .line 812
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 830
    .restart local v6    # "smoothMaxSpeed":F
    :cond_10
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageSpeed:F

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageCount:I

    add-int/lit8 v9, v9, -0x1

    int-to-float v9, v9

    mul-float/2addr v8, v9

    add-float/2addr v8, v7

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageCount:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    iput v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageSpeed:F

    goto/16 :goto_4

    .line 838
    :cond_11
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    mul-float v9, v7, v13

    div-float v9, v14, v9

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->pace:F

    goto/16 :goto_5

    .line 843
    :cond_12
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeed:F

    mul-float/2addr v9, v13

    div-float v9, v14, v9

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->maxPace:F

    goto/16 :goto_6

    .line 848
    :cond_13
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->averageSpeed:F

    mul-float/2addr v9, v13

    div-float v9, v14, v9

    iput v9, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->averagePace:F

    goto/16 :goto_7

    .line 859
    .restart local v1    # "declineCal":F
    .restart local v2    # "flatCal":F
    .restart local v4    # "inclineCal":F
    :cond_14
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_15

    .line 860
    const v8, 0x3a9e46fd

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    mul-float v4, v8, v9

    .line 861
    const v8, 0x3a88722a    # 0.001041f

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    iget-object v10, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v10, v10, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    sub-float/2addr v9, v10

    iget-object v10, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v10, v10, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    sub-float/2addr v9, v10

    mul-float v2, v8, v9

    .line 862
    const v8, 0x3a653aac

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    mul-float v1, v8, v9

    .line 863
    goto/16 :goto_8

    :cond_15
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    if-ne v8, v12, :cond_16

    .line 864
    const v8, 0x3a32c83f    # 6.82E-4f

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    mul-float v4, v8, v9

    .line 865
    const v8, 0x39b2c83f    # 3.41E-4f

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    iget-object v10, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v10, v10, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    sub-float/2addr v9, v10

    iget-object v10, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v10, v10, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    sub-float/2addr v9, v10

    mul-float v2, v8, v9

    .line 866
    const v8, 0x3932c83f    # 1.705E-4f

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    mul-float v1, v8, v9

    .line 867
    goto/16 :goto_8

    :cond_16
    iget v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v9, 0x4

    if-ne v8, v9, :cond_9

    .line 868
    iget-boolean v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->isBarometerSupported:Z

    if-eqz v8, :cond_17

    .line 869
    const v8, 0x3b818322

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageIncline:F

    mul-float/2addr v8, v9

    add-float/2addr v8, v11

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    mul-float v4, v8, v9

    .line 870
    iget-object v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v8, v8, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    sub-float/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    sub-float/2addr v8, v9

    mul-float v2, v11, v8

    .line 871
    const v8, 0x39f0ce34    # 4.593E-4f

    iget v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageDecline:F

    mul-float/2addr v8, v9

    sub-float v8, v11, v8

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    mul-float v1, v8, v9

    .line 872
    goto/16 :goto_8

    .line 873
    :cond_17
    const v8, 0x3a6ed02d    # 9.11E-4f

    iget-object v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v9, v9, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    mul-float v2, v8, v9

    goto/16 :goto_8
.end method

.method private calculateGpsData()V
    .locals 9

    .prologue
    const/high16 v8, 0x447a0000    # 1000.0f

    .line 675
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    invoke-virtual {v6}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    .line 676
    .local v0, "currentTime":J
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    invoke-virtual {v6}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    .line 677
    .local v3, "lastTime":J
    sub-long v6, v0, v3

    long-to-float v6, v6

    div-float v5, v6, v8

    .line 678
    .local v5, "timeDiff":F
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    iget-object v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    invoke-virtual {v6, v7}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v6

    invoke-direct {p0, v6, v5}, Lcom/samsung/location/monitor/LocationMonitor;->getFilteredDistance(FF)F

    move-result v2

    .line 679
    .local v2, "dist":F
    iget-boolean v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMoved:Z

    if-eqz v6, :cond_0

    .line 680
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    add-float/2addr v6, v2

    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    .line 681
    iget-boolean v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->isBarometerSupported:Z

    if-eqz v6, :cond_1

    .line 682
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    add-float/2addr v6, v2

    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    .line 683
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    const/high16 v7, 0x41700000    # 15.0f

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_0

    .line 684
    invoke-direct {p0, v0, v1}, Lcom/samsung/location/monitor/LocationMonitor;->calculate3D(J)F

    move-result v2

    .line 691
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    iput v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    .line 692
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineDistance:F

    iput v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    .line 693
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineDistance:F

    iput v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    .line 694
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    iput v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    .line 695
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineTime:J

    iput-wide v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineTime:J

    .line 696
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineTime:J

    iput-wide v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->declineTime:J

    .line 697
    iget-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    iput-wide v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    .line 698
    return-void

    .line 687
    :cond_1
    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    add-float/2addr v6, v2

    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    .line 688
    iget-wide v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    long-to-float v6, v6

    mul-float v7, v5, v8

    add-float/2addr v6, v7

    float-to-long v6, v6

    iput-wide v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    goto :goto_0
.end method

.method private determineMoved()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x3

    .line 905
    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mGpsSpeed:F

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPedometerSpeed:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    .line 906
    :cond_0
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->noMoveCount:I

    .line 913
    :cond_1
    :goto_0
    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->noMoveCount:I

    if-ne v0, v1, :cond_3

    .line 914
    iput-boolean v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMoved:Z

    .line 918
    :goto_1
    return-void

    .line 908
    :cond_2
    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->noMoveCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->noMoveCount:I

    .line 909
    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->noMoveCount:I

    if-le v0, v1, :cond_1

    .line 910
    iput v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->noMoveCount:I

    goto :goto_0

    .line 916
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMoved:Z

    goto :goto_1
.end method

.method private generateCallback()V
    .locals 22

    .prologue
    .line 636
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v1, v1, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 672
    :goto_0
    return-void

    .line 639
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    sget-object v2, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->TRACK_ONLY:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-ne v1, v2, :cond_1

    .line 640
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    invoke-interface {v1, v2}, Lcom/samsung/location/monitor/LocationMonitorCallback;->onDataChanged(Lcom/samsung/location/monitor/LocationMonitorResult;)V

    goto :goto_0

    .line 643
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v3, v3, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    sub-long v19, v1, v3

    .line 644
    .local v19, "shadeTime":J
    const-wide/16 v1, 0xbb8

    cmp-long v1, v19, v1

    if-lez v1, :cond_2

    .line 645
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->isGpsDisabled:Z

    .line 646
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->isPedometerSupported:Z

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    sget-object v2, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->ALL_GPS_OK:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-ne v1, v2, :cond_2

    .line 647
    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->ALL_GPS_NOK:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    .line 648
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    invoke-virtual {v2, v3}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    long-to-float v3, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/samsung/location/monitor/LocationMonitor;->getFilteredDistance(FF)F

    move-result v2

    add-float v16, v1, v2

    .line 649
    .local v16, "missedDistance":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialTime:J

    sub-long v17, v1, v3

    .line 650
    .local v17, "missedTime":J
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    add-float v1, v1, v16

    move-object/from16 v0, p0

    iput v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    .line 651
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    add-float v1, v1, v16

    move-object/from16 v0, p0

    iput v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    .line 652
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    add-long v1, v1, v17

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    .line 653
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->ep:Lcom/samsung/location/monitor/EnginePedometer;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/location/monitor/EnginePedometer;->startShade(FFJ)V

    .line 654
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 657
    .end local v16    # "missedDistance":F
    .end local v17    # "missedTime":J
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v1, v1, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastCallbackTime:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_3

    .line 658
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string/jumbo v2, "no new fix from engines"

    invoke-virtual {v1, v2}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 661
    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mGpsIndex:I

    const/4 v2, 0x4

    if-lt v1, v2, :cond_4

    .line 662
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mGpsIndex:I

    .line 663
    invoke-direct/range {p0 .. p0}, Lcom/samsung/location/monitor/LocationMonitor;->calculateGpsData()V

    .line 664
    new-instance v1, Landroid/location/Location;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    .line 666
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/samsung/location/monitor/LocationMonitor;->calculateExtraData()V

    .line 667
    invoke-direct/range {p0 .. p0}, Lcom/samsung/location/monitor/LocationMonitor;->determineMoved()V

    .line 668
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/location/monitor/LocationMonitor;->mUserWeight:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/location/monitor/LocationMonitor;->mPServiceType:I

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/samsung/location/monitor/LocationMonitor;->isWearableConnected:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/samsung/location/monitor/LocationMonitor;->isBarometerSupported:Z

    .line 669
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/samsung/location/monitor/LocationMonitor;->isPedometerSupported:Z

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageIncline:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageDecline:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/location/monitor/LocationMonitor;->mPedoValidity:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/location/monitor/LocationMonitor;->mBasePressure:F

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v21, ","

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 668
    invoke-virtual/range {v1 .. v15}, Lcom/samsung/location/monitor/LocationMonitorLog;->resultLog(Lcom/samsung/location/monitor/LocationMonitorResult;IIFIZZZFFIIFLjava/lang/String;)V

    .line 670
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    new-instance v2, Lcom/samsung/location/monitor/LocationMonitorResult;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    invoke-direct {v2, v3}, Lcom/samsung/location/monitor/LocationMonitorResult;-><init>(Lcom/samsung/location/monitor/LocationMonitorResult;)V

    invoke-interface {v1, v2}, Lcom/samsung/location/monitor/LocationMonitorCallback;->onDataChanged(Lcom/samsung/location/monitor/LocationMonitorResult;)V

    .line 671
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v1, v1, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/samsung/location/monitor/LocationMonitor;->mLastCallbackTime:J

    goto/16 :goto_0
.end method

.method private getFilteredDistance(FF)F
    .locals 4
    .param p1, "rawDistance"    # F
    .param p2, "timeDiff"    # F

    .prologue
    const/high16 v3, 0x40200000    # 2.5f

    .line 701
    const/4 v0, 0x0

    .line 702
    .local v0, "maxDist":F
    iget v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 703
    mul-float v0, v3, p2

    .line 711
    :cond_0
    :goto_0
    cmpl-float v1, p1, v0

    if-lez v1, :cond_4

    .line 714
    .end local v0    # "maxDist":F
    :goto_1
    return v0

    .line 704
    .restart local v0    # "maxDist":F
    :cond_1
    iget v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 705
    const/high16 v1, 0x40f00000    # 7.5f

    mul-float v0, v1, p2

    .line 706
    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 707
    const/high16 v1, 0x41c80000    # 25.0f

    mul-float v0, v1, p2

    .line 708
    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 709
    mul-float v0, v3, p2

    goto :goto_0

    :cond_4
    move v0, p1

    .line 714
    goto :goto_1
.end method

.method private getInclinationTY(Ljava/util/ArrayList;Ljava/util/ArrayList;)F
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;)F"
        }
    .end annotation

    .prologue
    .local p1, "horizontal":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    .local p2, "vertical":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    const/4 v8, 0x0

    .line 745
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 746
    .local v2, "n":I
    const/4 v3, 0x0

    .line 747
    .local v3, "sum_t":F
    const/4 v6, 0x0

    .line 748
    .local v6, "sum_y":F
    const/4 v5, 0x0

    .line 749
    .local v5, "sum_ty":F
    const/4 v4, 0x0

    .line 750
    .local v4, "sum_t2":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 756
    int-to-float v7, v2

    mul-float/2addr v7, v4

    mul-float v9, v3, v3

    sub-float v0, v7, v9

    .line 757
    .local v0, "denominator":F
    cmpl-float v7, v0, v8

    if-nez v7, :cond_1

    move v7, v8

    .line 760
    :goto_1
    return v7

    .line 751
    .end local v0    # "denominator":F
    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    add-float/2addr v3, v7

    .line 752
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    add-float/2addr v6, v7

    .line 753
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v9

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    mul-float/2addr v7, v9

    add-float/2addr v4, v7

    .line 754
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v9

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    mul-float/2addr v7, v9

    add-float/2addr v5, v7

    .line 750
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 760
    .restart local v0    # "denominator":F
    :cond_1
    int-to-float v7, v2

    mul-float/2addr v7, v5

    mul-float v8, v3, v6

    sub-float/2addr v7, v8

    div-float/2addr v7, v0

    goto :goto_1
.end method

.method private handleBarometerData(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 589
    const-string/jumbo v0, "pressure"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentPressure:F

    .line 590
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isGpsDisabled:Z

    if-nez v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeTimeHistory:Ljava/util/ArrayList;

    const-string/jumbo v1, "timeDiff"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeAltitudeHistory:Ljava/util/ArrayList;

    const v1, 0x447d5000    # 1013.25f

    iget v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentPressure:F

    invoke-static {v1, v2}, Landroid/hardware/SensorManager;->getAltitude(FF)F

    move-result v1

    iget v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialBaroAlti:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 594
    :cond_0
    return-void
.end method

.method private handleGPSData(Landroid/location/Location;)V
    .locals 8
    .param p1, "currentLoc"    # Landroid/location/Location;

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 522
    iget-boolean v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->isBarometerSupported:Z

    if-eqz v1, :cond_3

    .line 523
    iget v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentPressure:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    .line 576
    :cond_0
    :goto_0
    return-void

    .line 526
    :cond_1
    iget v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    if-ne v1, v6, :cond_2

    .line 527
    iput v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    .line 528
    new-instance v1, Lcom/samsung/location/monitor/PressureTask;

    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureCallback:Lcom/samsung/location/monitor/PressureTask$Callback;

    invoke-direct {v1, v2}, Lcom/samsung/location/monitor/PressureTask;-><init>(Lcom/samsung/location/monitor/PressureTask$Callback;)V

    iput-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->pt:Lcom/samsung/location/monitor/PressureTask;

    .line 529
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->pt:Lcom/samsung/location/monitor/PressureTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPServiceType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Lcom/samsung/location/monitor/PressureTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 531
    :cond_2
    iget v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    if-eq v1, v7, :cond_0

    .line 536
    :cond_3
    const/4 v0, 0x0

    .line 537
    .local v0, "fromShade":Z
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    sget-object v2, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->ALL_GPS_NOK:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-ne v1, v2, :cond_4

    .line 538
    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->ALL_GPS_OK:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    iput-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    .line 540
    :cond_4
    iget-boolean v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->isGpsDisabled:Z

    if-eqz v1, :cond_6

    .line 541
    iput-boolean v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->isGpsDisabled:Z

    .line 542
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    if-nez v1, :cond_9

    .line 543
    iget-boolean v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->isStartMonitoringCalled:Z

    if-nez v1, :cond_5

    .line 544
    iput-boolean v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->isStartMonitoringCalled:Z

    .line 545
    iget-boolean v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    if-eqz v1, :cond_5

    .line 546
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string/jumbo v2, "onMonitoringStarted"

    invoke-virtual {v1, v2}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 547
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    invoke-interface {v1}, Lcom/samsung/location/monitor/LocationMonitorCallback;->onMonitoringStarted()V

    .line 550
    :cond_5
    iget-boolean v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->isResumeMonitoringCalled:Z

    if-nez v1, :cond_6

    .line 551
    iput-boolean v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->isResumeMonitoringCalled:Z

    .line 552
    iget-boolean v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    if-eqz v1, :cond_6

    .line 553
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string/jumbo v2, "onMonitoringResumed"

    invoke-virtual {v1, v2}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 554
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    invoke-interface {v1}, Lcom/samsung/location/monitor/LocationMonitorCallback;->onMonitoringResumed()V

    .line 562
    :cond_6
    :goto_1
    iput-object p1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    .line 563
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    .line 564
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/location/monitor/LocationMonitorResult;->latitude:D

    .line 565
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/location/monitor/LocationMonitorResult;->longitude:D

    .line 566
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    iput v2, v1, Lcom/samsung/location/monitor/LocationMonitorResult;->accuracy:F

    .line 567
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, v1, Lcom/samsung/location/monitor/LocationMonitorResult;->altitude:F

    .line 568
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v2

    iput v2, v1, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    .line 569
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v1

    iput v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mGpsSpeed:F

    .line 570
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    if-eqz v1, :cond_7

    if-eqz v0, :cond_8

    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    sget-object v2, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->ALL_GPS_OK:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-ne v1, v2, :cond_8

    .line 571
    :cond_7
    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mGpsIndex:I

    .line 572
    iput-object p1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    .line 573
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/samsung/location/monitor/LocationMonitor;->initializeSectionParameter(J)V

    .line 575
    :cond_8
    iget v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mGpsIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mGpsIndex:I

    goto/16 :goto_0

    .line 558
    :cond_9
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private handlePause()V
    .locals 9

    .prologue
    .line 437
    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    if-eqz v3, :cond_1

    .line 438
    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    sget-object v4, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->ALL_GPS_OK:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    sget-object v4, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->GPS_ONLY:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-ne v3, v4, :cond_1

    .line 439
    :cond_0
    iget v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    iget-object v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    invoke-virtual {v4, v5}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v4

    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    iget-object v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    invoke-virtual {v7}, Landroid/location/Location;->getTime()J

    move-result-wide v7

    sub-long/2addr v5, v7

    long-to-float v5, v5

    invoke-direct {p0, v4, v5}, Lcom/samsung/location/monitor/LocationMonitor;->getFilteredDistance(FF)F

    move-result v4

    add-float v0, v3, v4

    .line 440
    .local v0, "missedDistance":F
    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialTime:J

    sub-long v1, v3, v5

    .line 441
    .local v1, "missedTime":J
    iget v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    add-float/2addr v3, v0

    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    .line 442
    iget v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    add-float/2addr v3, v0

    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    .line 443
    iget-wide v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    .line 444
    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    iput v4, v3, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    .line 445
    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    iput v4, v3, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    .line 446
    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    iput-wide v4, v3, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    .line 449
    .end local v0    # "missedDistance":F
    .end local v1    # "missedTime":J
    :cond_1
    invoke-direct {p0}, Lcom/samsung/location/monitor/LocationMonitor;->stop()V

    .line 450
    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string v4, "LocationMonitor paused"

    invoke-virtual {v3, v4}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 451
    return-void
.end method

.method private handlePedometerData(Lcom/samsung/location/monitor/LocationMonitorResult;)V
    .locals 3
    .param p1, "result"    # Lcom/samsung/location/monitor/LocationMonitorResult;

    .prologue
    const/4 v2, 0x1

    .line 597
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->GPS_ONLY:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-ne v0, v1, :cond_0

    .line 598
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPedometerSpeed:F

    .line 633
    :goto_0
    return-void

    .line 602
    :cond_0
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    if-nez v0, :cond_2

    .line 603
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isStartMonitoringCalled:Z

    if-nez v0, :cond_1

    .line 604
    iput-boolean v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->isStartMonitoringCalled:Z

    .line 605
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    if-eqz v0, :cond_1

    .line 606
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string/jumbo v1, "onMonitoringStarted"

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 607
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    invoke-interface {v0}, Lcom/samsung/location/monitor/LocationMonitorCallback;->onMonitoringStarted()V

    .line 610
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isResumeMonitoringCalled:Z

    if-nez v0, :cond_2

    .line 611
    iput-boolean v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->isResumeMonitoringCalled:Z

    .line 612
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    if-eqz v0, :cond_2

    .line 613
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string/jumbo v1, "onMonitoringResumed"

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 614
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    invoke-interface {v0}, Lcom/samsung/location/monitor/LocationMonitorCallback;->onMonitoringResumed()V

    .line 619
    :cond_2
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->ALL_GPS_NOK:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-ne v0, v1, :cond_3

    .line 620
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    iput-wide v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    .line 621
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->latitude:D

    iput-wide v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->latitude:D

    .line 622
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->longitude:D

    iput-wide v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->longitude:D

    .line 623
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    iput v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    .line 624
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    iput v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    .line 625
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    iput v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    .line 626
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    iput-wide v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    .line 627
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    .line 628
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    .line 629
    iget-wide v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    iput-wide v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    .line 631
    :cond_3
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPedometerSpeed:F

    .line 632
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v1, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->stepCount:I

    iput v1, v0, Lcom/samsung/location/monitor/LocationMonitorResult;->stepCount:I

    goto :goto_0
.end method

.method private handleResume()V
    .locals 3

    .prologue
    .line 454
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/location/monitor/LocationMonitor;->start(Z)V

    .line 455
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LocationMonitor resumed : type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/WC="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->isWearableConnected:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/PT="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPServiceType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 456
    return-void
.end method

.method private handleStart(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "param"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x2

    const-wide/16 v9, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 370
    const-string/jumbo v5, "type"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    .line 371
    const-string/jumbo v5, "wei"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v5

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mUserWeight:F

    .line 372
    const-string v5, "hei"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v5

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mUserHeight:F

    .line 373
    const-string/jumbo v5, "pType"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPServiceType:I

    .line 374
    const-string/jumbo v5, "wc"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->isWearableConnected:Z

    .line 375
    iput v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    .line 376
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mBasePressure:F

    .line 377
    iput v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPedoValidity:I

    .line 378
    iput-wide v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastCallbackTime:J

    .line 379
    new-instance v5, Lcom/samsung/location/monitor/LocationMonitorResult;

    invoke-direct {v5}, Lcom/samsung/location/monitor/LocationMonitorResult;-><init>()V

    iput-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    .line 380
    const-string/jumbo v5, "restart"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 381
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    .line 382
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineDistance:F

    .line 383
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineDistance:F

    .line 384
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    .line 385
    iput-wide v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineTime:J

    .line 386
    iput-wide v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineTime:J

    .line 387
    iput-wide v9, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    .line 388
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxAltitude:F

    .line 389
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMinAltitude:F

    .line 390
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeed:F

    .line 391
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageSpeed:F

    .line 392
    iput v8, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageCount:I

    .line 393
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageIncline:F

    .line 394
    iput v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageDecline:F

    .line 426
    :goto_0
    invoke-direct {p0, v8}, Lcom/samsung/location/monitor/LocationMonitor;->start(Z)V

    .line 427
    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "LocationMonitor started : type="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/WC="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->isWearableConnected:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/PT="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPServiceType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 428
    return-void

    .line 396
    :cond_0
    const-string v5, "dist"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v1

    .line 397
    .local v1, "dist":[F
    aget v5, v1, v8

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    .line 398
    aget v5, v1, v7

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineDistance:F

    .line 399
    aget v5, v1, v11

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineDistance:F

    .line 400
    const/4 v5, 0x3

    aget v5, v1, v5

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    .line 401
    const-string/jumbo v5, "time"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v4

    .line 402
    .local v4, "time":[J
    aget-wide v5, v4, v8

    iput-wide v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mInclineTime:J

    .line 403
    aget-wide v5, v4, v7

    iput-wide v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mDeclineTime:J

    .line 404
    aget-wide v5, v4, v11

    iput-wide v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    .line 405
    const-string v5, "alti"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v0

    .line 406
    .local v0, "alti":[F
    aget v5, v0, v8

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxAltitude:F

    .line 407
    aget v5, v0, v7

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMinAltitude:F

    .line 408
    const-string/jumbo v5, "speed"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v3

    .line 409
    .local v3, "speed":[F
    aget v5, v3, v8

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeed:F

    .line 410
    aget v5, v3, v7

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageSpeed:F

    .line 411
    const-string/jumbo v5, "rd1"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageCount:I

    .line 412
    const-string/jumbo v5, "rd2"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 413
    .local v2, "lastPressureStatus":I
    if-eq v2, v7, :cond_1

    if-eq v2, v11, :cond_1

    .line 414
    iput v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPressureStatus:I

    .line 415
    const/4 v5, 0x3

    if-ne v2, v5, :cond_1

    .line 416
    const-string/jumbo v5, "rd5"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v5

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mBasePressure:F

    .line 419
    :cond_1
    const-string/jumbo v5, "rd3"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v5

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageIncline:F

    .line 420
    const-string/jumbo v5, "rd4"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v5

    iput v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mAverageDecline:F

    .line 421
    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    const-string/jumbo v6, "step"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/samsung/location/monitor/LocationMonitorResult;->stepCount:I

    .line 422
    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mTotalDistance:F

    iput v6, v5, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    .line 423
    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatDistance:F

    iput v6, v5, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    .line 424
    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget-wide v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mFlatTime:J

    iput-wide v6, v5, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    goto/16 :goto_0
.end method

.method private handleStop()V
    .locals 2

    .prologue
    .line 431
    invoke-direct {p0}, Lcom/samsung/location/monitor/LocationMonitor;->stop()V

    .line 432
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string v1, "LocationMonitor stopped"

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 433
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 434
    return-void
.end method

.method private initializeSectionParameter(J)V
    .locals 3
    .param p1, "currentTime"    # J

    .prologue
    const/4 v2, 0x0

    .line 579
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeTimeHistory:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 580
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeTimeHistory:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 581
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeAltitudeHistory:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 582
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeAltitudeHistory:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    iput v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    .line 584
    iput-wide p1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialTime:J

    .line 585
    const v0, 0x447d5000    # 1013.25f

    iget v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentPressure:F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getAltitude(FF)F

    move-result v0

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialBaroAlti:F

    .line 586
    return-void
.end method

.method private start(Z)V
    .locals 7
    .param p1, "isResume"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 459
    iput-boolean p1, p0, Lcom/samsung/location/monitor/LocationMonitor;->isStartMonitoringCalled:Z

    .line 460
    if-eqz p1, :cond_5

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isResumeMonitoringCalled:Z

    .line 461
    iput-boolean v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->isGpsDisabled:Z

    .line 462
    iput-boolean v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMoved:Z

    .line 463
    iput v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mGpsIndex:I

    .line 464
    iput v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxIndex:I

    .line 465
    iput v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mGpsSpeed:F

    .line 466
    iput v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mPedometerSpeed:F

    .line 467
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialTime:J

    .line 468
    iput v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentPressure:F

    .line 469
    iput v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialBaroAlti:F

    .line 470
    iput v4, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSectionDistance:F

    .line 471
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mMaxSpeedHistory:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 472
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeTimeHistory:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 473
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mRelativeAltitudeHistory:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 474
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSpeedHistory:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 475
    iput-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCurrentLoc:Landroid/location/Location;

    .line 476
    iput-object v6, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastLocToCalc:Landroid/location/Location;

    .line 478
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 481
    :cond_0
    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    if-ne v0, v5, :cond_6

    .line 482
    :cond_1
    sget-object v0, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->GPS_ONLY:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    .line 483
    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    if-ne v0, v5, :cond_2

    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPedometerSupported:Z

    if-eqz v0, :cond_2

    .line 484
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->ep:Lcom/samsung/location/monitor/EnginePedometer;

    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mUserHeight:F

    iget v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mUserWeight:F

    invoke-virtual {v0, v2, v3, v5}, Lcom/samsung/location/monitor/EnginePedometer;->startEngine(Lcom/samsung/location/monitor/LocationMonitorResult;FF)V

    .line 494
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    sget-object v2, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->TRACK_ONLY:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-eq v0, v2, :cond_3

    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isBarometerSupported:Z

    if-eqz v0, :cond_3

    .line 495
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->sm:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->sm:Landroid/hardware/SensorManager;

    const/4 v5, 0x6

    invoke-virtual {v3, v5}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 497
    :cond_3
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lm:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    iget-object v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 498
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->TRACK_ONLY:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-eq v0, v1, :cond_4

    .line 499
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 501
    :cond_4
    return-void

    :cond_5
    move v0, v2

    .line 460
    goto/16 :goto_0

    .line 486
    :cond_6
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isWearableConnected:Z

    if-eqz v0, :cond_7

    .line 487
    sget-object v0, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->TRACK_ONLY:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    goto :goto_1

    .line 489
    :cond_7
    sget-object v0, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->ALL_GPS_NOK:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    .line 490
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPedometerSupported:Z

    if-eqz v0, :cond_2

    .line 491
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->ep:Lcom/samsung/location/monitor/EnginePedometer;

    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLastResult:Lcom/samsung/location/monitor/LocationMonitorResult;

    iget v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->mUserHeight:F

    iget v5, p0, Lcom/samsung/location/monitor/LocationMonitor;->mUserWeight:F

    invoke-virtual {v0, v2, v3, v5}, Lcom/samsung/location/monitor/EnginePedometer;->startEngine(Lcom/samsung/location/monitor/LocationMonitorResult;FF)V

    goto :goto_1
.end method

.method private stop()V
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lm:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 505
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    sget-object v1, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->TRACK_ONLY:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isBarometerSupported:Z

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->sm:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 508
    :cond_0
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->ep:Lcom/samsung/location/monitor/EnginePedometer;

    invoke-virtual {v0}, Lcom/samsung/location/monitor/EnginePedometer;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 509
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->ep:Lcom/samsung/location/monitor/EnginePedometer;

    invoke-virtual {v0}, Lcom/samsung/location/monitor/EnginePedometer;->stopEngine()V

    .line 511
    :cond_1
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 512
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 513
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 514
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 515
    sget-object v0, Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;->NONE:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    iput-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mStatus:Lcom/samsung/location/monitor/LocationMonitor$MonitoringStatus;

    .line 516
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 517
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 519
    :cond_2
    return-void
.end method


# virtual methods
.method public isMonitoring()Z
    .locals 1

    .prologue
    .line 242
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    return v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPaused:Z

    return v0
.end method

.method public pauseMonitoring()V
    .locals 3

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "pause request from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 216
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    if-nez v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string v1, "Wrong pause request : already stopped"

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 226
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPaused:Z

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string v1, "Wrong pause request : already paused"

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    goto :goto_0

    .line 224
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPaused:Z

    .line 225
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public restartMonitoring(IZFFILcom/samsung/location/monitor/LocationMonitorResult;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "wearableConnected"    # Z
    .param p3, "userHeight"    # F
    .param p4, "userWeight"    # F
    .param p5, "pserviceType"    # I
    .param p6, "lastResult"    # Lcom/samsung/location/monitor/LocationMonitorResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 144
    if-nez p6, :cond_0

    .line 145
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "continueMonitoring : lastResult is null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_0
    invoke-direct/range {p0 .. p6}, Lcom/samsung/location/monitor/LocationMonitor;->_startMonitoring(IZFFILcom/samsung/location/monitor/LocationMonitorResult;)V

    .line 148
    return-void
.end method

.method public resumeMonitoring()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 229
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "resume request from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lm:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mType:I

    if-ne v0, v3, :cond_1

    .line 231
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "GPS is off"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPaused:Z

    if-nez v0, :cond_2

    .line 234
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string v1, "Wrong resume request : not paused"

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 239
    :goto_0
    return-void

    .line 237
    :cond_2
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 238
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPaused:Z

    goto :goto_0
.end method

.method public startMonitoring(IZFFI)V
    .locals 7
    .param p1, "type"    # I
    .param p2, "wearableConnected"    # Z
    .param p3, "userHeight"    # F
    .param p4, "userWeight"    # F
    .param p5, "pserviceType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 140
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/location/monitor/LocationMonitor;->_startMonitoring(IZFFILcom/samsung/location/monitor/LocationMonitorResult;)V

    .line 141
    return-void
.end method

.method public stopMonitoring()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 204
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "stop request from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/location/monitor/LocationMonitor;->mCallback:Lcom/samsung/location/monitor/LocationMonitorCallback;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 205
    iget-boolean v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;

    const-string v1, "already stopped"

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 212
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 210
    iput-boolean v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->isMonitoring:Z

    .line 211
    iput-boolean v3, p0, Lcom/samsung/location/monitor/LocationMonitor;->isPaused:Z

    goto :goto_0
.end method
