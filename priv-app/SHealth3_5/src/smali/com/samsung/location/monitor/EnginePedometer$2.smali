.class Lcom/samsung/location/monitor/EnginePedometer$2;
.super Landroid/os/Handler;
.source "EnginePedometer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/location/monitor/EnginePedometer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/location/monitor/EnginePedometer;


# direct methods
.method constructor <init>(Lcom/samsung/location/monitor/EnginePedometer;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/location/monitor/EnginePedometer$2;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    .line 135
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 138
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer$2;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->isRunning:Z
    invoke-static {v0}, Lcom/samsung/location/monitor/EnginePedometer;->access$12(Lcom/samsung/location/monitor/EnginePedometer;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_0

    .line 142
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer$2;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # invokes: Lcom/samsung/location/monitor/EnginePedometer;->handlePedometerInfo()V
    invoke-static {v0}, Lcom/samsung/location/monitor/EnginePedometer;->access$13(Lcom/samsung/location/monitor/EnginePedometer;)V

    .line 143
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer$2;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/location/monitor/EnginePedometer;->access$14(Lcom/samsung/location/monitor/EnginePedometer;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/monitor/EnginePedometer$2;->this$0:Lcom/samsung/location/monitor/EnginePedometer;

    # getter for: Lcom/samsung/location/monitor/EnginePedometer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/location/monitor/EnginePedometer;->access$14(Lcom/samsung/location/monitor/EnginePedometer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method
