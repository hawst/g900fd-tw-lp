.class Lcom/samsung/location/monitor/LocationMonitor$2;
.super Ljava/lang/Object;
.source "LocationMonitor.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/location/monitor/LocationMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/location/monitor/LocationMonitor;


# direct methods
.method constructor <init>(Lcom/samsung/location/monitor/LocationMonitor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/location/monitor/LocationMonitor$2;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 288
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 291
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 292
    .local v0, "data":Landroid/os/Bundle;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 293
    .local v1, "timeStamp":J
    const-string/jumbo v3, "timeDiff"

    iget-object v4, p0, Lcom/samsung/location/monitor/LocationMonitor$2;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mSectionInitialTime:J
    invoke-static {v4}, Lcom/samsung/location/monitor/LocationMonitor;->access$6(Lcom/samsung/location/monitor/LocationMonitor;)J

    move-result-wide v4

    sub-long v4, v1, v4

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    double-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 294
    const-string/jumbo v3, "pressure"

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 295
    iget-object v3, p0, Lcom/samsung/location/monitor/LocationMonitor$2;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/location/monitor/LocationMonitor;->access$4(Lcom/samsung/location/monitor/LocationMonitor;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/location/monitor/LocationMonitor$2;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/location/monitor/LocationMonitor;->access$4(Lcom/samsung/location/monitor/LocationMonitor;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {v4, v5, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 296
    return-void
.end method
