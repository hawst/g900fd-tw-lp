.class public Lcom/samsung/location/monitor/LocationMonitorResult;
.super Ljava/lang/Object;
.source "LocationMonitorResult.java"


# instance fields
.field public accuracy:F

.field public altitude:F

.field public averagePace:F

.field public averageSpeed:F

.field public consumedCalorie:F

.field public declineDistance:F

.field public declineTime:J

.field public flatDistance:F

.field public flatTime:J

.field public inclineDistance:F

.field public inclineTime:J

.field public latitude:D

.field public longitude:D

.field public maxAltitude:F

.field public maxPace:F

.field public maxSpeed:F

.field public minAltitude:F

.field public pace:F

.field public restartData1:I

.field public restartData2:I

.field public restartData3:F

.field public restartData4:F

.field public restartData5:F

.field public speed:F

.field public stepCount:I

.field public timeStamp:J

.field public totalDistance:F


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-wide v1, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    .line 5
    iput-wide v4, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->latitude:D

    .line 6
    iput-wide v4, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->longitude:D

    .line 7
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->accuracy:F

    .line 8
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    .line 9
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    .line 10
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    .line 11
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    .line 12
    iput-wide v1, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineTime:J

    .line 13
    iput-wide v1, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->declineTime:J

    .line 14
    iput-wide v1, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    .line 15
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->altitude:F

    .line 16
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->maxAltitude:F

    .line 17
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->minAltitude:F

    .line 18
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    .line 19
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->maxSpeed:F

    .line 20
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->averageSpeed:F

    .line 21
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->pace:F

    .line 22
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->maxPace:F

    .line 23
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->averagePace:F

    .line 24
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->consumedCalorie:F

    .line 25
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->stepCount:I

    .line 26
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData1:I

    .line 27
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData2:I

    .line 28
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData3:F

    .line 29
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData4:F

    .line 30
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData5:F

    .line 33
    return-void
.end method

.method public constructor <init>(Lcom/samsung/location/monitor/LocationMonitorResult;)V
    .locals 6
    .param p1, "lmr"    # Lcom/samsung/location/monitor/LocationMonitorResult;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-wide v1, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    .line 5
    iput-wide v4, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->latitude:D

    .line 6
    iput-wide v4, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->longitude:D

    .line 7
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->accuracy:F

    .line 8
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    .line 9
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    .line 10
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    .line 11
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    .line 12
    iput-wide v1, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineTime:J

    .line 13
    iput-wide v1, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->declineTime:J

    .line 14
    iput-wide v1, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    .line 15
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->altitude:F

    .line 16
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->maxAltitude:F

    .line 17
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->minAltitude:F

    .line 18
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    .line 19
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->maxSpeed:F

    .line 20
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->averageSpeed:F

    .line 21
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->pace:F

    .line 22
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->maxPace:F

    .line 23
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->averagePace:F

    .line 24
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->consumedCalorie:F

    .line 25
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->stepCount:I

    .line 26
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData1:I

    .line 27
    iput v3, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData2:I

    .line 28
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData3:F

    .line 29
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData4:F

    .line 30
    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData5:F

    .line 36
    iget-wide v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    iput-wide v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    .line 37
    iget-wide v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->latitude:D

    iput-wide v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->latitude:D

    .line 38
    iget-wide v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->longitude:D

    iput-wide v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->longitude:D

    .line 39
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->accuracy:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->accuracy:F

    .line 40
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    .line 41
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineDistance:F

    .line 42
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->declineDistance:F

    .line 43
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    .line 44
    iget-wide v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineTime:J

    iput-wide v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->inclineTime:J

    .line 45
    iget-wide v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->declineTime:J

    iput-wide v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->declineTime:J

    .line 46
    iget-wide v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    iput-wide v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    .line 47
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->altitude:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->altitude:F

    .line 48
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->maxAltitude:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->maxAltitude:F

    .line 49
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->minAltitude:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->minAltitude:F

    .line 50
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    .line 51
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->maxSpeed:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->maxSpeed:F

    .line 52
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->averageSpeed:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->averageSpeed:F

    .line 53
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->pace:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->pace:F

    .line 54
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->maxPace:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->maxPace:F

    .line 55
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->averagePace:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->averagePace:F

    .line 56
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->consumedCalorie:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->consumedCalorie:F

    .line 57
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->stepCount:I

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->stepCount:I

    .line 58
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData1:I

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData1:I

    .line 59
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData2:I

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData2:I

    .line 60
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData3:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData3:F

    .line 61
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData4:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData4:F

    .line 62
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData5:F

    iput v0, p0, Lcom/samsung/location/monitor/LocationMonitorResult;->restartData5:F

    .line 63
    return-void
.end method
