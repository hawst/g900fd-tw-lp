.class public Lcom/samsung/location/monitor/PressureTask;
.super Landroid/os/AsyncTask;
.source "PressureTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/location/monitor/PressureTask$Callback;,
        Lcom/samsung/location/monitor/PressureTask$InvalidResultException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "[",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# static fields
.field private static final ALTITUDE_TASK_TIMEOUT:I = 0x5dc


# instance fields
.field private mCallback:Lcom/samsung/location/monitor/PressureTask$Callback;


# direct methods
.method public constructor <init>(Lcom/samsung/location/monitor/PressureTask$Callback;)V
    .locals 1
    .param p1, "callback"    # Lcom/samsung/location/monitor/PressureTask$Callback;

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/location/monitor/PressureTask;->mCallback:Lcom/samsung/location/monitor/PressureTask$Callback;

    .line 28
    iput-object p1, p0, Lcom/samsung/location/monitor/PressureTask;->mCallback:Lcom/samsung/location/monitor/PressureTask$Callback;

    .line 29
    return-void
.end method

.method private doAccuWeather(DD)Ljava/lang/Float;
    .locals 17
    .param p1, "lat"    # D
    .param p3, "lng"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 107
    const/4 v3, 0x0

    .line 108
    .local v3, "basePressure":Ljava/lang/Float;
    new-instance v12, Ljava/net/URL;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "https://api.accuweather.com/locations/v1/cities/geoposition/search.json?q="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-wide/from16 v0, p3

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "&apikey=0460650BB2524F84BAECAA9381D79EFC&language=en"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v12, v14}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 109
    .local v12, "url4Key":Ljava/net/URL;
    invoke-virtual {v12}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljavax/net/ssl/HttpsURLConnection;

    .line 110
    .local v5, "conn4Key":Ljavax/net/ssl/HttpsURLConnection;
    const/4 v9, 0x0

    .line 112
    .local v9, "key":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 113
    const-string v14, "TLS"

    invoke-static {v14}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v7

    .line 114
    .local v7, "context":Ljavax/net/ssl/SSLContext;
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v7, v14, v15, v0}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 115
    invoke-virtual {v7}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v14

    invoke-virtual {v5, v14}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 116
    const/16 v14, 0x5dc

    invoke-virtual {v5, v14}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    .line 117
    const/16 v14, 0x5dc

    invoke-virtual {v5, v14}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V

    .line 118
    const/4 v14, 0x0

    invoke-virtual {v5, v14}, Ljavax/net/ssl/HttpsURLConnection;->setUseCaches(Z)V

    .line 119
    const/4 v14, 0x1

    invoke-virtual {v5, v14}, Ljavax/net/ssl/HttpsURLConnection;->setInstanceFollowRedirects(Z)V

    .line 120
    invoke-virtual {v5}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 122
    invoke-virtual {v5}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v14

    const/16 v15, 0xc8

    if-ne v14, v15, :cond_0

    .line 123
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8}, Ljava/lang/String;-><init>()V

    .line 124
    .local v8, "json":Ljava/lang/String;
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v14, Ljava/io/InputStreamReader;

    invoke-virtual {v5}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v14}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 125
    .local v4, "br":Ljava/io/BufferedReader;
    const/4 v10, 0x0

    .line 126
    .local v10, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_2

    .line 129
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 130
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11, v8}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 131
    .local v11, "obj":Lorg/json/JSONObject;
    const-string v14, "Key"

    invoke-virtual {v11, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 135
    .end local v4    # "br":Ljava/io/BufferedReader;
    .end local v7    # "context":Ljavax/net/ssl/SSLContext;
    .end local v8    # "json":Ljava/lang/String;
    .end local v10    # "line":Ljava/lang/String;
    .end local v11    # "obj":Lorg/json/JSONObject;
    :cond_0
    if-eqz v9, :cond_4

    .line 136
    new-instance v13, Ljava/net/URL;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "https://api.accuweather.com/currentconditions/v1/"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ".json?apikey=0460650BB2524F84BAECAA9381D79EFC&language=en&details=true&getPhotos=false&metric=true"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 137
    .local v13, "url4Pressure":Ljava/net/URL;
    invoke-virtual {v13}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v6

    check-cast v6, Ljavax/net/ssl/HttpsURLConnection;

    .line 139
    .local v6, "conn4Pressure":Ljavax/net/ssl/HttpsURLConnection;
    if-eqz v6, :cond_5

    .line 140
    const-string v14, "TLS"

    invoke-static {v14}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v7

    .line 141
    .restart local v7    # "context":Ljavax/net/ssl/SSLContext;
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v7, v14, v15, v0}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 142
    invoke-virtual {v7}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 143
    const/16 v14, 0x5dc

    invoke-virtual {v6, v14}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    .line 144
    const/16 v14, 0x5dc

    invoke-virtual {v6, v14}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V

    .line 145
    const/4 v14, 0x0

    invoke-virtual {v6, v14}, Ljavax/net/ssl/HttpsURLConnection;->setUseCaches(Z)V

    .line 146
    const/4 v14, 0x1

    invoke-virtual {v6, v14}, Ljavax/net/ssl/HttpsURLConnection;->setInstanceFollowRedirects(Z)V

    .line 147
    invoke-virtual {v6}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 149
    invoke-virtual {v6}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v14

    const/16 v15, 0xc8

    if-ne v14, v15, :cond_5

    .line 150
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8}, Ljava/lang/String;-><init>()V

    .line 151
    .restart local v8    # "json":Ljava/lang/String;
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v14, Ljava/io/InputStreamReader;

    invoke-virtual {v6}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v14}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 152
    .restart local v4    # "br":Ljava/io/BufferedReader;
    const/4 v10, 0x0

    .line 153
    .restart local v10    # "line":Ljava/lang/String;
    :goto_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_3

    .line 156
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 157
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v8}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 158
    .local v2, "array":Lorg/json/JSONArray;
    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v14

    const-string v15, "Pressure"

    invoke-virtual {v14, v15}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    const-string v15, "Metric"

    invoke-virtual {v14, v15}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    const-string v15, "Value"

    invoke-virtual {v14, v15}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v14

    double-to-float v14, v14

    invoke-static {v14}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 159
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v14

    const/4 v15, 0x0

    cmpg-float v14, v14, v15

    if-lez v14, :cond_1

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v14

    const v15, 0x44bb8000    # 1500.0f

    cmpl-float v14, v14, v15

    if-lez v14, :cond_5

    .line 160
    :cond_1
    new-instance v14, Lcom/samsung/location/monitor/PressureTask$InvalidResultException;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v15}, Lcom/samsung/location/monitor/PressureTask$InvalidResultException;-><init>(Lcom/samsung/location/monitor/PressureTask;Lcom/samsung/location/monitor/PressureTask$InvalidResultException;)V

    throw v14

    .line 127
    .end local v2    # "array":Lorg/json/JSONArray;
    .end local v6    # "conn4Pressure":Ljavax/net/ssl/HttpsURLConnection;
    .end local v13    # "url4Pressure":Ljava/net/URL;
    :cond_2
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 154
    .restart local v6    # "conn4Pressure":Ljavax/net/ssl/HttpsURLConnection;
    .restart local v13    # "url4Pressure":Ljava/net/URL;
    :cond_3
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 165
    .end local v4    # "br":Ljava/io/BufferedReader;
    .end local v6    # "conn4Pressure":Ljavax/net/ssl/HttpsURLConnection;
    .end local v7    # "context":Ljavax/net/ssl/SSLContext;
    .end local v8    # "json":Ljava/lang/String;
    .end local v10    # "line":Ljava/lang/String;
    .end local v13    # "url4Pressure":Ljava/net/URL;
    :cond_4
    new-instance v14, Lcom/samsung/location/monitor/PressureTask$InvalidResultException;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v15}, Lcom/samsung/location/monitor/PressureTask$InvalidResultException;-><init>(Lcom/samsung/location/monitor/PressureTask;Lcom/samsung/location/monitor/PressureTask$InvalidResultException;)V

    throw v14

    .line 168
    .restart local v6    # "conn4Pressure":Ljavax/net/ssl/HttpsURLConnection;
    .restart local v13    # "url4Pressure":Ljava/net/URL;
    :cond_5
    if-nez v3, :cond_6

    .line 169
    new-instance v14, Ljava/io/IOException;

    const-string v15, "conn is null or NOK"

    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 171
    :cond_6
    return-object v3
.end method

.method private doWeatherNews(DD)Ljava/lang/Float;
    .locals 10
    .param p1, "lat"    # D
    .param p3, "lng"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    const/4 v0, 0x0

    .line 65
    .local v0, "basePressure":Ljava/lang/Float;
    new-instance v5, Ljava/net/URL;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "https://galaxy.wni.com/api/weather.cgi?lat="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&lon="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 66
    .local v5, "url":Ljava/net/URL;
    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljavax/net/ssl/HttpsURLConnection;

    .line 68
    .local v2, "conn":Ljavax/net/ssl/HttpsURLConnection;
    if-eqz v2, :cond_1

    .line 69
    const-string v7, "TLS"

    invoke-static {v7}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v3

    .line 70
    .local v3, "context":Ljavax/net/ssl/SSLContext;
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v3, v7, v8, v9}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 71
    invoke-virtual {v3}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 72
    const/16 v7, 0x5dc

    invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    .line 73
    const/16 v7, 0x5dc

    invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V

    .line 74
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setUseCaches(Z)V

    .line 75
    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setInstanceFollowRedirects(Z)V

    .line 76
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 78
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v7

    const/16 v8, 0xc8

    if-ne v7, v8, :cond_1

    .line 79
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 80
    .local v1, "br":Ljava/io/BufferedReader;
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v7

    invoke-virtual {v7}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    .line 81
    .local v6, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v6, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 82
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    .line 83
    .local v4, "eventType":I
    :goto_0
    const/4 v7, 0x1

    if-ne v4, v7, :cond_2

    .line 96
    :cond_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 100
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v3    # "context":Ljavax/net/ssl/SSLContext;
    .end local v4    # "eventType":I
    .end local v6    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :cond_1
    if-nez v0, :cond_5

    .line 101
    new-instance v7, Ljava/io/IOException;

    const-string v8, "conn is null or NOK"

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 84
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "context":Ljavax/net/ssl/SSLContext;
    .restart local v4    # "eventType":I
    .restart local v6    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :cond_2
    const/4 v7, 0x2

    if-ne v4, v7, :cond_4

    .line 85
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "press"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 86
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 87
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v7

    const/4 v8, 0x0

    cmpg-float v7, v7, v8

    if-lez v7, :cond_3

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v7

    const v8, 0x44bb8000    # 1500.0f

    cmpl-float v7, v7, v8

    if-lez v7, :cond_0

    .line 89
    :cond_3
    new-instance v7, Lcom/samsung/location/monitor/PressureTask$InvalidResultException;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/samsung/location/monitor/PressureTask$InvalidResultException;-><init>(Lcom/samsung/location/monitor/PressureTask;Lcom/samsung/location/monitor/PressureTask$InvalidResultException;)V

    throw v7

    .line 94
    :cond_4
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    goto :goto_0

    .line 103
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v3    # "context":Ljavax/net/ssl/SSLContext;
    .end local v4    # "eventType":I
    .end local v6    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :cond_5
    return-object v0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/samsung/location/monitor/PressureTask;->doInBackground([Ljava/lang/Object;)[Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)[Ljava/lang/Float;
    .locals 12
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    const/4 v7, 0x2

    const/high16 v11, 0x40a00000    # 5.0f

    const/4 v10, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 33
    new-array v0, v7, [Ljava/lang/Float;

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v0, v9

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v0, v8

    .line 35
    .local v0, "basePressure":[Ljava/lang/Float;
    const/4 v7, 0x0

    :try_start_0
    aget-object v7, p1, v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 36
    .local v6, "pserviceType":I
    const/4 v7, 0x1

    aget-object v7, p1, v7

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 37
    .local v2, "lat":D
    const/4 v7, 0x2

    aget-object v7, p1, v7

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 38
    .local v4, "lng":D
    const/16 v7, 0xc

    if-ne v6, v7, :cond_0

    .line 39
    const/4 v7, 0x1

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/location/monitor/PressureTask;->doWeatherNews(DD)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v0, v7

    .line 60
    .end local v2    # "lat":D
    .end local v4    # "lng":D
    .end local v6    # "pserviceType":I
    :goto_0
    return-object v0

    .line 40
    .restart local v2    # "lat":D
    .restart local v4    # "lng":D
    .restart local v6    # "pserviceType":I
    :cond_0
    const/16 v7, 0xd

    if-ne v6, v7, :cond_1

    .line 41
    const/4 v7, 0x1

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/location/monitor/PressureTask;->doAccuWeather(DD)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v0, v7
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/location/monitor/PressureTask$InvalidResultException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    goto :goto_0

    .line 45
    .end local v2    # "lat":D
    .end local v4    # "lng":D
    .end local v6    # "pserviceType":I
    :catch_0
    move-exception v1

    .line 46
    .local v1, "e":Ljava/net/MalformedURLException;
    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v0, v9

    goto :goto_0

    .line 43
    .end local v1    # "e":Ljava/net/MalformedURLException;
    .restart local v2    # "lat":D
    .restart local v4    # "lng":D
    .restart local v6    # "pserviceType":I
    :cond_1
    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    :try_start_1
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v0, v7
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/samsung/location/monitor/PressureTask$InvalidResultException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    goto :goto_0

    .line 47
    .end local v2    # "lat":D
    .end local v4    # "lng":D
    .end local v6    # "pserviceType":I
    :catch_1
    move-exception v1

    .line 48
    .local v1, "e":Ljava/net/SocketTimeoutException;
    const/high16 v7, 0x40400000    # 3.0f

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v0, v9

    goto :goto_0

    .line 49
    .end local v1    # "e":Ljava/net/SocketTimeoutException;
    :catch_2
    move-exception v1

    .line 50
    .local v1, "e":Ljava/io/IOException;
    const/high16 v7, 0x40800000    # 4.0f

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v0, v9

    goto :goto_0

    .line 51
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 52
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v0, v9

    goto :goto_0

    .line 53
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_4
    move-exception v1

    .line 54
    .local v1, "e":Lorg/json/JSONException;
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v0, v9

    goto :goto_0

    .line 55
    .end local v1    # "e":Lorg/json/JSONException;
    :catch_5
    move-exception v1

    .line 56
    .local v1, "e":Lcom/samsung/location/monitor/PressureTask$InvalidResultException;
    const/high16 v7, 0x40c00000    # 6.0f

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v0, v9

    goto :goto_0

    .line 57
    .end local v1    # "e":Lcom/samsung/location/monitor/PressureTask$InvalidResultException;
    :catch_6
    move-exception v1

    .line 58
    .local v1, "e":Ljava/lang/Exception;
    const/high16 v7, 0x40e00000    # 7.0f

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v0, v9

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Float;

    invoke-virtual {p0, p1}, Lcom/samsung/location/monitor/PressureTask;->onPostExecute([Ljava/lang/Float;)V

    return-void
.end method

.method protected onPostExecute([Ljava/lang/Float;)V
    .locals 1
    .param p1, "basePressure"    # [Ljava/lang/Float;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/location/monitor/PressureTask;->mCallback:Lcom/samsung/location/monitor/PressureTask$Callback;

    invoke-interface {v0, p1}, Lcom/samsung/location/monitor/PressureTask$Callback;->onResult([Ljava/lang/Float;)V

    .line 177
    return-void
.end method
