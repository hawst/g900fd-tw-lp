.class public Lcom/samsung/location/monitor/EnginePedometer;
.super Ljava/lang/Object;
.source "EnginePedometer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/location/monitor/EnginePedometer$Callback;
    }
.end annotation


# static fields
.field private static final MSG_PEDOMETER_READ:I = 0x1

.field private static final NO_DETECTION_TIME:I = 0x5dc


# instance fields
.field private isPedometerSupported:Z

.field private isRunning:Z

.field private mAccl:[F

.field private mBarometerData:F

.field private mCallback:Lcom/samsung/location/monitor/EnginePedometer$Callback;

.field private mFlatDistanceFromGPS:F

.field private mFlatTimeFromGPS:J

.field private mHandler:Landroid/os/Handler;

.field private mInitialStepCount:J

.field private mLastCount:J

.field private mLastDistance:F

.field private mLastSpeed:F

.field private mPrevAcclTime:J

.field private mPrevDetectionTime:J

.field private mPrevStepCount:I

.field private mSectionInitalDistance:F

.field private mSectionInitialTime:J

.field private mSensorListener:Landroid/hardware/SensorEventListener;

.field mStepinfo:Lcom/sec/pedometer/StepInfo;

.field private mTotalDistanceFromGPS:F

.field private pedometerlib:Lcom/sec/pedometer/Pedometer;

.field private sm:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/location/monitor/EnginePedometer$Callback;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callback"    # Lcom/samsung/location/monitor/EnginePedometer$Callback;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    const-wide/16 v3, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-boolean v2, p0, Lcom/samsung/location/monitor/EnginePedometer;->isPedometerSupported:Z

    .line 19
    iput-boolean v2, p0, Lcom/samsung/location/monitor/EnginePedometer;->isRunning:Z

    .line 20
    iput-wide v3, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastCount:J

    .line 21
    iput v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastDistance:F

    .line 22
    iput v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastSpeed:F

    .line 23
    iput-wide v3, p0, Lcom/samsung/location/monitor/EnginePedometer;->mInitialStepCount:J

    .line 24
    iput v2, p0, Lcom/samsung/location/monitor/EnginePedometer;->mPrevStepCount:I

    .line 25
    iput-wide v3, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitialTime:J

    .line 26
    iput v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitalDistance:F

    .line 27
    iput v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mTotalDistanceFromGPS:F

    .line 28
    iput v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mFlatDistanceFromGPS:F

    .line 29
    iput-wide v3, p0, Lcom/samsung/location/monitor/EnginePedometer;->mFlatTimeFromGPS:J

    .line 30
    const v0, 0x447d5000    # 1013.25f

    iput v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mBarometerData:F

    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mAccl:[F

    .line 32
    iput-wide v3, p0, Lcom/samsung/location/monitor/EnginePedometer;->mPrevAcclTime:J

    .line 33
    iput-wide v3, p0, Lcom/samsung/location/monitor/EnginePedometer;->mPrevDetectionTime:J

    .line 34
    invoke-static {}, Lcom/sec/pedometer/Pedometer;->getInstance()Lcom/sec/pedometer/Pedometer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->pedometerlib:Lcom/sec/pedometer/Pedometer;

    .line 35
    new-instance v0, Lcom/sec/pedometer/StepInfo;

    invoke-direct {v0}, Lcom/sec/pedometer/StepInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mStepinfo:Lcom/sec/pedometer/StepInfo;

    .line 95
    new-instance v0, Lcom/samsung/location/monitor/EnginePedometer$1;

    invoke-direct {v0, p0}, Lcom/samsung/location/monitor/EnginePedometer$1;-><init>(Lcom/samsung/location/monitor/EnginePedometer;)V

    iput-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSensorListener:Landroid/hardware/SensorEventListener;

    .line 135
    new-instance v0, Lcom/samsung/location/monitor/EnginePedometer$2;

    invoke-direct {v0, p0}, Lcom/samsung/location/monitor/EnginePedometer$2;-><init>(Lcom/samsung/location/monitor/EnginePedometer;)V

    iput-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mHandler:Landroid/os/Handler;

    .line 38
    iput-object p2, p0, Lcom/samsung/location/monitor/EnginePedometer;->mCallback:Lcom/samsung/location/monitor/EnginePedometer$Callback;

    .line 39
    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->sm:Landroid/hardware/SensorManager;

    .line 40
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->sm:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->isPedometerSupported:Z

    .line 41
    return-void

    :cond_0
    move v0, v2

    .line 40
    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/location/monitor/EnginePedometer;)J
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mPrevAcclTime:J

    return-wide v0
.end method

.method static synthetic access$1(Lcom/samsung/location/monitor/EnginePedometer;J)V
    .locals 0

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/samsung/location/monitor/EnginePedometer;->mPrevAcclTime:J

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/location/monitor/EnginePedometer;F)V
    .locals 0

    .prologue
    .line 21
    iput p1, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastDistance:F

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/location/monitor/EnginePedometer;F)V
    .locals 0

    .prologue
    .line 22
    iput p1, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastSpeed:F

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/location/monitor/EnginePedometer;)Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->isRunning:Z

    return v0
.end method

.method static synthetic access$13(Lcom/samsung/location/monitor/EnginePedometer;)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/samsung/location/monitor/EnginePedometer;->handlePedometerInfo()V

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/location/monitor/EnginePedometer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/location/monitor/EnginePedometer;)[F
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mAccl:[F

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/location/monitor/EnginePedometer;)Lcom/sec/pedometer/Pedometer;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->pedometerlib:Lcom/sec/pedometer/Pedometer;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/location/monitor/EnginePedometer;)F
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mBarometerData:F

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/location/monitor/EnginePedometer;)J
    .locals 2

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastCount:J

    return-wide v0
.end method

.method static synthetic access$6(Lcom/samsung/location/monitor/EnginePedometer;)Lcom/samsung/location/monitor/EnginePedometer$Callback;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mCallback:Lcom/samsung/location/monitor/EnginePedometer$Callback;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/location/monitor/EnginePedometer;)F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastDistance:F

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/location/monitor/EnginePedometer;J)V
    .locals 0

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/samsung/location/monitor/EnginePedometer;->mPrevDetectionTime:J

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/location/monitor/EnginePedometer;J)V
    .locals 0

    .prologue
    .line 20
    iput-wide p1, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastCount:J

    return-void
.end method

.method private handlePedometerInfo()V
    .locals 15

    .prologue
    const-wide/16 v13, 0x0

    const-wide/high16 v11, 0x4069000000000000L    # 200.0

    .line 149
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 150
    .local v4, "currentTime":J
    iget-wide v2, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastCount:J

    .line 151
    .local v2, "currentStepCount":J
    iget v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastDistance:F

    .line 152
    .local v0, "currentDistance":F
    iget v1, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastSpeed:F

    .line 153
    .local v1, "currentSpeed":F
    iget-wide v7, p0, Lcom/samsung/location/monitor/EnginePedometer;->mPrevDetectionTime:J

    sub-long v7, v4, v7

    const-wide/16 v9, 0x5dc

    cmp-long v7, v7, v9

    if-lez v7, :cond_0

    .line 154
    const/4 v1, 0x0

    .line 156
    :cond_0
    iget-wide v7, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitialTime:J

    cmp-long v7, v7, v13

    if-nez v7, :cond_1

    .line 157
    iput-wide v4, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitialTime:J

    .line 159
    :cond_1
    iget v7, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitalDistance:F

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-nez v7, :cond_2

    .line 160
    iput v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitalDistance:F

    .line 162
    :cond_2
    iget-wide v7, p0, Lcom/samsung/location/monitor/EnginePedometer;->mInitialStepCount:J

    cmp-long v7, v7, v13

    if-nez v7, :cond_3

    .line 163
    iput-wide v2, p0, Lcom/samsung/location/monitor/EnginePedometer;->mInitialStepCount:J

    .line 166
    :cond_3
    new-instance v6, Lcom/samsung/location/monitor/LocationMonitorResult;

    invoke-direct {v6}, Lcom/samsung/location/monitor/LocationMonitorResult;-><init>()V

    .line 167
    .local v6, "result":Lcom/samsung/location/monitor/LocationMonitorResult;
    iput-wide v4, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->timeStamp:J

    .line 168
    iput-wide v11, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->latitude:D

    .line 169
    iput-wide v11, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->longitude:D

    .line 170
    const v7, 0x40666666    # 3.6f

    div-float v7, v1, v7

    iput v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->speed:F

    .line 171
    iget v7, p0, Lcom/samsung/location/monitor/EnginePedometer;->mTotalDistanceFromGPS:F

    add-float/2addr v7, v0

    iget v8, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitalDistance:F

    sub-float/2addr v7, v8

    iput v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    .line 172
    iget v7, p0, Lcom/samsung/location/monitor/EnginePedometer;->mFlatDistanceFromGPS:F

    add-float/2addr v7, v0

    iget v8, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitalDistance:F

    sub-float/2addr v7, v8

    iput v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    .line 173
    iget-wide v7, p0, Lcom/samsung/location/monitor/EnginePedometer;->mFlatTimeFromGPS:J

    add-long/2addr v7, v4

    iget-wide v9, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitialTime:J

    sub-long/2addr v7, v9

    iput-wide v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    .line 174
    iget-wide v7, p0, Lcom/samsung/location/monitor/EnginePedometer;->mInitialStepCount:J

    sub-long v7, v2, v7

    long-to-int v7, v7

    iget v8, p0, Lcom/samsung/location/monitor/EnginePedometer;->mPrevStepCount:I

    add-int/2addr v7, v8

    iput v7, v6, Lcom/samsung/location/monitor/LocationMonitorResult;->stepCount:I

    .line 176
    iget-object v7, p0, Lcom/samsung/location/monitor/EnginePedometer;->mCallback:Lcom/samsung/location/monitor/EnginePedometer$Callback;

    invoke-interface {v7, v6}, Lcom/samsung/location/monitor/EnginePedometer$Callback;->onDataChanged(Lcom/samsung/location/monitor/LocationMonitorResult;)V

    .line 177
    return-void
.end method

.method private initializeParameter(Lcom/samsung/location/monitor/LocationMonitorResult;)V
    .locals 3
    .param p1, "prevResult"    # Lcom/samsung/location/monitor/LocationMonitorResult;

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 81
    iput-wide v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastCount:J

    .line 82
    iput v2, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastDistance:F

    .line 83
    iput v2, p0, Lcom/samsung/location/monitor/EnginePedometer;->mLastSpeed:F

    .line 84
    iput-wide v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitialTime:J

    .line 85
    iput v2, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitalDistance:F

    .line 86
    iput-wide v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mInitialStepCount:J

    .line 87
    iput-wide v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mPrevAcclTime:J

    .line 88
    iput-wide v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mPrevDetectionTime:J

    .line 89
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->stepCount:I

    iput v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mPrevStepCount:I

    .line 90
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->totalDistance:F

    iput v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mTotalDistanceFromGPS:F

    .line 91
    iget v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->flatDistance:F

    iput v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mFlatDistanceFromGPS:F

    .line 92
    iget-wide v0, p1, Lcom/samsung/location/monitor/LocationMonitorResult;->flatTime:J

    iput-wide v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mFlatTimeFromGPS:J

    .line 93
    return-void
.end method


# virtual methods
.method public isPedometerSupported()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->isPedometerSupported:Z

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->isRunning:Z

    return v0
.end method

.method public startEngine(Lcom/samsung/location/monitor/LocationMonitorResult;FF)V
    .locals 5
    .param p1, "prevResult"    # Lcom/samsung/location/monitor/LocationMonitorResult;
    .param p2, "userHeight"    # F
    .param p3, "userWeight"    # F

    .prologue
    const/4 v4, 0x1

    .line 48
    iget-boolean v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->isPedometerSupported:Z

    if-nez v0, :cond_0

    .line 56
    :goto_0
    return-void

    .line 51
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/location/monitor/EnginePedometer;->initializeParameter(Lcom/samsung/location/monitor/LocationMonitorResult;)V

    .line 52
    iput-boolean v4, p0, Lcom/samsung/location/monitor/EnginePedometer;->isRunning:Z

    .line 53
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->pedometerlib:Lcom/sec/pedometer/Pedometer;

    new-instance v1, Lcom/sec/pedometer/UserInfo;

    invoke-direct {v1, p2, p3, v4}, Lcom/sec/pedometer/UserInfo;-><init>(FFI)V

    invoke-virtual {v0, v1}, Lcom/sec/pedometer/Pedometer;->Init(Lcom/sec/pedometer/UserInfo;)V

    .line 54
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->sm:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/samsung/location/monitor/EnginePedometer;->sm:Landroid/hardware/SensorManager;

    invoke-virtual {v2, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    const/16 v3, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 55
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/location/monitor/EnginePedometer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public startShade(FFJ)V
    .locals 2
    .param p1, "lastTotalDist"    # F
    .param p2, "lastFlatDist"    # F
    .param p3, "lastFlatTime"    # J

    .prologue
    .line 73
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitialTime:J

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSectionInitalDistance:F

    .line 75
    iput p1, p0, Lcom/samsung/location/monitor/EnginePedometer;->mTotalDistanceFromGPS:F

    .line 76
    iput p2, p0, Lcom/samsung/location/monitor/EnginePedometer;->mFlatDistanceFromGPS:F

    .line 77
    iput-wide p3, p0, Lcom/samsung/location/monitor/EnginePedometer;->mFlatTimeFromGPS:J

    .line 78
    return-void
.end method

.method public stopEngine()V
    .locals 2

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->isPedometerSupported:Z

    if-nez v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->sm:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/location/monitor/EnginePedometer;->mSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 63
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->pedometerlib:Lcom/sec/pedometer/Pedometer;

    invoke-virtual {v0}, Lcom/sec/pedometer/Pedometer;->Finalize()V

    .line 64
    iget-object v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/monitor/EnginePedometer;->isRunning:Z

    goto :goto_0
.end method
