.class Lcom/samsung/location/monitor/LocationMonitor$4;
.super Ljava/lang/Object;
.source "LocationMonitor.java"

# interfaces
.implements Lcom/samsung/location/monitor/PressureTask$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/location/monitor/LocationMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/location/monitor/LocationMonitor;


# direct methods
.method constructor <init>(Lcom/samsung/location/monitor/LocationMonitor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/location/monitor/LocationMonitor$4;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult([Ljava/lang/Float;)V
    .locals 3
    .param p1, "basePressure"    # [Ljava/lang/Float;

    .prologue
    .line 314
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 315
    .local v0, "errorCode":F
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$4;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    const/4 v2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-static {v1, v2}, Lcom/samsung/location/monitor/LocationMonitor;->access$9(Lcom/samsung/location/monitor/LocationMonitor;F)V

    .line 316
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    .line 317
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$4;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/samsung/location/monitor/LocationMonitor;->access$10(Lcom/samsung/location/monitor/LocationMonitor;I)V

    .line 333
    :goto_0
    return-void

    .line 318
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_1

    .line 319
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$4;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/samsung/location/monitor/LocationMonitor;->access$10(Lcom/samsung/location/monitor/LocationMonitor;I)V

    goto :goto_0

    .line 320
    :cond_1
    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_2

    .line 321
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$4;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/samsung/location/monitor/LocationMonitor;->access$10(Lcom/samsung/location/monitor/LocationMonitor;I)V

    goto :goto_0

    .line 322
    :cond_2
    const/high16 v1, 0x40400000    # 3.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_3

    .line 323
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$4;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/samsung/location/monitor/LocationMonitor;->access$10(Lcom/samsung/location/monitor/LocationMonitor;I)V

    goto :goto_0

    .line 324
    :cond_3
    const/high16 v1, 0x40800000    # 4.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_4

    .line 325
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$4;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    const/4 v2, 0x7

    invoke-static {v1, v2}, Lcom/samsung/location/monitor/LocationMonitor;->access$10(Lcom/samsung/location/monitor/LocationMonitor;I)V

    goto :goto_0

    .line 326
    :cond_4
    const/high16 v1, 0x40a00000    # 5.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_5

    .line 327
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$4;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lcom/samsung/location/monitor/LocationMonitor;->access$10(Lcom/samsung/location/monitor/LocationMonitor;I)V

    goto :goto_0

    .line 328
    :cond_5
    const/high16 v1, 0x40c00000    # 6.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_6

    .line 329
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$4;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    const/16 v2, 0x9

    invoke-static {v1, v2}, Lcom/samsung/location/monitor/LocationMonitor;->access$10(Lcom/samsung/location/monitor/LocationMonitor;I)V

    goto :goto_0

    .line 331
    :cond_6
    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$4;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lcom/samsung/location/monitor/LocationMonitor;->access$10(Lcom/samsung/location/monitor/LocationMonitor;I)V

    goto :goto_0
.end method
