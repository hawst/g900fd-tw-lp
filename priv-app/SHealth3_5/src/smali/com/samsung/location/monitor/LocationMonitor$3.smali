.class Lcom/samsung/location/monitor/LocationMonitor$3;
.super Ljava/lang/Object;
.source "LocationMonitor.java"

# interfaces
.implements Lcom/samsung/location/monitor/EnginePedometer$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/location/monitor/LocationMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/location/monitor/LocationMonitor;


# direct methods
.method constructor <init>(Lcom/samsung/location/monitor/LocationMonitor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/location/monitor/LocationMonitor$3;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    .line 299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/samsung/location/monitor/LocationMonitorResult;)V
    .locals 3
    .param p1, "result"    # Lcom/samsung/location/monitor/LocationMonitorResult;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$3;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->lml:Lcom/samsung/location/monitor/LocationMonitorLog;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$0(Lcom/samsung/location/monitor/LocationMonitor;)Lcom/samsung/location/monitor/LocationMonitorLog;

    move-result-object v0

    const-string v1, "got from P"

    invoke-virtual {v0, v1}, Lcom/samsung/location/monitor/LocationMonitorLog;->generalLog(Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$3;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$4(Lcom/samsung/location/monitor/LocationMonitor;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/monitor/LocationMonitor$3;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/location/monitor/LocationMonitor;->access$4(Lcom/samsung/location/monitor/LocationMonitor;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 304
    return-void
.end method

.method public onDataInvalid(I)V
    .locals 2
    .param p1, "data"    # I

    .prologue
    .line 307
    iget-object v0, p0, Lcom/samsung/location/monitor/LocationMonitor$3;->this$0:Lcom/samsung/location/monitor/LocationMonitor;

    # getter for: Lcom/samsung/location/monitor/LocationMonitor;->mPedoValidity:I
    invoke-static {v0}, Lcom/samsung/location/monitor/LocationMonitor;->access$7(Lcom/samsung/location/monitor/LocationMonitor;)I

    move-result v1

    or-int/2addr v1, p1

    invoke-static {v0, v1}, Lcom/samsung/location/monitor/LocationMonitor;->access$8(Lcom/samsung/location/monitor/LocationMonitor;I)V

    .line 308
    return-void
.end method
